package com.vedantu.subscription.dao;


import static com.vedantu.subscription.entities.mongo.Enrollment.Constants.BATCH_CHANGE_TIME;
import static com.vedantu.subscription.entities.mongo.Enrollment.Constants.BATCH_ID;
import static com.vedantu.subscription.entities.mongo.Enrollment.Constants.COURSE_ID;
import static com.vedantu.subscription.entities.mongo.Enrollment.Constants.ENTITY_TITLE;
import static com.vedantu.subscription.entities.mongo.Enrollment.Constants.ENTITY_TYPE;
import static com.vedantu.subscription.entities.mongo.Enrollment.Constants.ROLE;
import static com.vedantu.subscription.entities.mongo.Enrollment.Constants.SECTION_ID;
import static com.vedantu.subscription.entities.mongo.Enrollment.Constants.SECTION_STATE;
import static com.vedantu.subscription.entities.mongo.Enrollment.Constants.STATE;
import static com.vedantu.subscription.entities.mongo.Enrollment.Constants.STATUS;
import static com.vedantu.subscription.entities.mongo.Enrollment.Constants.USER_ID;
import static com.vedantu.util.dbentities.mongo.AbstractMongoEntity.Constants._ID;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.vedantu.util.dbentities.mongo.AbstractMongoEntity;
import org.apache.logging.log4j.Logger;
import org.bson.Document;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationOperation;
import org.springframework.data.mongodb.core.aggregation.AggregationOptions;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.vedantu.User.Role;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.onetofew.enums.EnrollmentState;
import com.vedantu.onetofew.enums.EntityStatus;
import com.vedantu.session.pojo.EntityType;
import com.vedantu.subscription.entities.mongo.Enrollment;
import com.vedantu.subscription.enums.EnrollmentType;
import com.vedantu.subscription.managers.AwsSQSManager;
import com.vedantu.subscription.request.UserPremiumSubscriptionEnrollmentReq;
import com.vedantu.subscription.request.section.BatchSectionReq;
import com.vedantu.subscription.viewobject.response.section.SectionEnrollmentAggregationRes;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.dbentitybeans.mongo.EntityState;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import com.vedantu.util.enums.SQSMessageType;
import com.vedantu.util.enums.SQSQueue;
import com.vedantu.util.request.ActiveUserIdsByBatchIdsReq;
import com.vedantu.util.request.GetEnrollmentsReq;
import com.vedantu.util.request.GetUserEnrollmentsReq;

@Service
public class EnrollmentDAO extends AbstractMongoDAO {

    @Autowired
    private MongoClientFactory mongoClientFactory;

    @Autowired
    private LogFactory logfactory;

    @Autowired
    private AwsSQSManager awsSQSManager;

    @Autowired
    private SectionDAO sectionDAO;

    private final Gson gson = new Gson();

    @SuppressWarnings("static-access")
    private Logger logger = logfactory.getLogger(EnrollmentDAO.class);

    public EnrollmentDAO() {
        super();
    }

    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }

    public void create(Enrollment p, Long callingUserId) {
        String callingUserIdString = null;
        if (callingUserId != null) {
            callingUserIdString = callingUserId.toString();
        }
        boolean newCP = StringUtils.isEmpty(p.getId());
        if (p != null) {
            saveEntity(p, callingUserIdString);
        }
        if (Role.STUDENT.equals(p.getRole())) {
            if (newCP) {
                awsSQSManager.sendToSQS(SQSQueue.ENROLLMENT_CREATED_LEADSQUARED_QUEUE, SQSMessageType.ENROLLMENT_CREATED, gson.toJson(p), p.getUserId() + "_ENRO");
            } else {
                awsSQSManager.sendToSQS(SQSQueue.ENROLLMENT_UPDATED_LEADSQUARED_QUEUE, SQSMessageType.ENROLLMENT_UPDATED, gson.toJson(p), p.getUserId() + "_ENRO");
            }
        }
        if(p.getStatus().equals(EntityStatus.ENDED) && Objects.nonNull(p.getSectionId())){
            sectionDAO.incrementVacantSeatsForSections(Collections.singletonList(p.getSectionId()),1);
        }

    }

    public void insertAll(List<Enrollment> p, Long callingUserId) {
        if (p != null) {
            String callingUserIdString = null;
            if (callingUserId != null) {
                callingUserIdString = callingUserId.toString();
            }
            insertAllEntities(p, Enrollment.class.getSimpleName(), callingUserIdString);
            sendToSqsList(p);

            for(Enrollment e:p){
                StringBuilder stringBuilder = new StringBuilder();
                if(!e.getStatus().equals(EntityStatus.ENDED) && Objects.isNull(e.getSectionId()))
                    stringBuilder.append(e.getId());
                logger.warn("NOTE: still enrollments happening for direct OTM bundle purchase {}",stringBuilder);

            }
            List<String> sectionIds = p.stream().filter(e -> Objects.nonNull(e.getSectionId()) && e.getStatus().equals(EntityStatus.ENDED)).map(Enrollment::getSectionId).collect(Collectors.toList());
            sectionDAO.incrementVacantSeatsForSections(sectionIds,1);
        }
    }

    public Enrollment getById(String id) {
        Enrollment enrollment = null;
        if (!StringUtils.isEmpty(id)) {
            enrollment = getEntityById(id, Enrollment.class);
        }
        return enrollment;
    }

    public void save(Enrollment p) {
        if (p != null) {
            boolean newCP = StringUtils.isEmpty(p.getId());
            saveEntity(p);
            if (Role.STUDENT.equals(p.getRole())) {
                if (newCP) {
                    awsSQSManager.triggerAsyncSQS(SQSQueue.ENROLLMENT_CREATED_LEADSQUARED_QUEUE, SQSMessageType.ENROLLMENT_CREATED, gson.toJson(p), p.getUserId() + "_ENRO");

                } else {
                    awsSQSManager.triggerAsyncSQS(SQSQueue.ENROLLMENT_UPDATED_LEADSQUARED_QUEUE, SQSMessageType.ENROLLMENT_UPDATED, gson.toJson(p), p.getUserId() + "_ENRO");
                }
            }

            if(p.getStatus().equals(EntityStatus.ENDED) && Objects.nonNull(p.getSectionId())){
                sectionDAO.incrementVacantSeatsForSections(Collections.singletonList(p.getSectionId()),1);
            }
        }
    }

    public List<Enrollment> getByIdsWithProjection(List<String> ids, List<String> includeFields, int start, int size) {
        Query q = new Query();
        q.addCriteria(Criteria.where(Enrollment.Constants._ID).in(ids));
        q.with(Sort.by(Sort.Direction.ASC, Enrollment.Constants._ID));
        setFetchParameters(q, start, size);
        return runQuery(q, Enrollment.class, includeFields);
    }


    public Enrollment getEnrollmentByUserIdAndBatch(Long userId, String batchId, EntityStatus status) {

        Query query = new Query();
        query.addCriteria(Criteria.where(Enrollment.Constants.BATCH_ID).is(batchId));
        query.addCriteria(Criteria.where(Enrollment.Constants.USER_ID).is(userId.toString()));
        if (status != null) {
            query.addCriteria(Criteria.where(Enrollment.Constants.STATUS).is(EntityStatus.ACTIVE));
        }

        return findOne(query, Enrollment.class);

    }
    public List<Enrollment> getEnrollmentsOfBatch(List<String> batchIds) {
        return getEnrollmentsOfBatch(batchIds,0,0,false);
    }

    public List<Enrollment> getEnrollmentsOfBatch(List<String> batchIds,int start,int size,boolean fetchParameteresRequired) {
        Query query = new Query();
        query.addCriteria(Criteria.where(Enrollment.Constants.BATCH_ID).in(batchIds));
        query.with(Sort.by(Sort.Direction.DESC, Enrollment.Constants.LAST_UPDATED));
        if(fetchParameteresRequired) {
            setFetchParameters(query, start, size);
        }
        return runQuery(query, Enrollment.class);
    }

    public List<Enrollment> getEnrollmentsForBatches(List<String> batchIds) {
        return getEnrollmentsForBatches(batchIds,0,0,false);
    }

    public List<Enrollment> getEnrollmentsForBatches(List<String> batchIds,int start,int size,boolean fetchParameteresRequired) {

        Query query = new Query();
        query.addCriteria(Criteria.where(Enrollment.Constants.BATCH_ID).in(batchIds));
        if(fetchParameteresRequired) {
            setFetchParameters(query, start, size);
        }
        logger.info("query : {}",query);
        return runQuery(query, Enrollment.class);

    }

    public List<Enrollment> getStudentEnrollmentsForBatches(List<String> batchIds) {

        Query query = new Query();
        query.addCriteria(Criteria.where(Enrollment.Constants.BATCH_ID).in(batchIds));
        query.addCriteria(Criteria.where(Enrollment.Constants.ROLE).is(Role.STUDENT));
        return runQuery(query, Enrollment.class);

    }

    public List<Enrollment> getStudentEnrollmentsForBatches(List<String> batchIds, int start, int size) {

        Query query = new Query();
        query.addCriteria(Criteria.where(Enrollment.Constants.BATCH_ID).in(batchIds));
        query.addCriteria(Criteria.where(Enrollment.Constants.ROLE).is(Role.STUDENT));
        setFetchParameters(query, start, size);
        return runQuery(query, Enrollment.class);

    }

    public List<Enrollment> getEnrollmentByCourseAndUser(String courseId, String userId,
                                                         List<EntityStatus> statuses) {
        Query query = new Query();
        query.addCriteria(Criteria.where(Enrollment.Constants.USER_ID).is(userId));
        query.addCriteria(Criteria.where(Enrollment.Constants.COURSE_ID).is(courseId));
        if (ArrayUtils.isNotEmpty(statuses)) {
            query.addCriteria(Criteria.where(Enrollment.Constants.STATUS).in(statuses));
        }
        return runQuery(query, Enrollment.class);

    }

    public List<Enrollment> getEnrollmentByBatchAndUser(String batchId, String userId,
                                                        List<EntityStatus> statuses) {
        Query query = new Query();
        query.addCriteria(Criteria.where(Enrollment.Constants.USER_ID).is(userId));
        query.addCriteria(Criteria.where(Enrollment.Constants.BATCH_ID).is(batchId));
        if (ArrayUtils.isNotEmpty(statuses)) {
            query.addCriteria(Criteria.where(Enrollment.Constants.STATUS).in(statuses));
        }
        return runQuery(query, Enrollment.class);

    }

    public void updateStatus(List<String> enrollmentIds) {
        Query query = new Query();
        Update update = new Update();
        query.addCriteria(Criteria.where(Enrollment.Constants._ID).in(enrollmentIds));
        update.set(Enrollment.Constants.STATE, EnrollmentState.REGULAR);
        updateMulti(query, update, Enrollment.class);
    }

    public List<Enrollment> getEnrollments(GetEnrollmentsReq req) {
        Query query = new Query();
        logger.info(req);

        if (req.getUserId() != null) {
            query.addCriteria(Criteria.where(Enrollment.Constants.USER_ID).is(req.getUserId().toString()));
        }

        if (req.getRole() != null) {
            query.addCriteria(Criteria.where(Enrollment.Constants.ROLE).is(req.getRole()));
        }

        if (ArrayUtils.isNotEmpty(req.getStatuses())) {
            query.addCriteria(Criteria.where(Enrollment.Constants.STATUS).in(req.getStatuses()));
        } else if (req.getStatus() != null) {
            query.addCriteria(Criteria.where(Enrollment.Constants.STATUS).is(req.getStatus()));
        }

        if (req.getState() != null) {
            query.addCriteria(Criteria.where(Enrollment.Constants.STATE).is(req.getState()));
        }

        if (ArrayUtils.isNotEmpty(req.getEnrollmentIds())) {
            query.addCriteria(Criteria.where(Enrollment.Constants._ID).in(req.getEnrollmentIds()));
        }

        if (!StringUtils.isEmpty(req.getBatchId())) {
            query.addCriteria(Criteria.where(Enrollment.Constants.BATCH_ID).is(req.getBatchId()));
        } else if (req.getBatchIdExists() != null) {
            query.addCriteria(Criteria.where(Enrollment.Constants.BATCH_ID).exists(req.getBatchIdExists()));
        } else if (!ArrayUtils.isEmpty(req.getMultipleBatchIds())) {
            query.addCriteria(Criteria.where(Enrollment.Constants.BATCH_ID).in(req.getMultipleBatchIds()));
        }

        if (req.getEntityType() != null) {
            query.addCriteria(Criteria.where(Enrollment.Constants.ENTITY_TYPE).is(req.getEntityType()));
        }
        if (req.getLastUpdated() != null) {
            query.addCriteria(Criteria.where(Enrollment.Constants.LAST_UPDATED).gt(req.getLastUpdated()));
        }
        if (!StringUtils.isEmpty(req.getEntityId())) {
            query.addCriteria(Criteria.where(Enrollment.Constants.ENTITY_ID).is(req.getEntityId()));
        }

        if(req.getRemoveDeEnrolled() != null  &&  req.getRemoveDeEnrolled() == true ){
            query.addCriteria(Criteria.where(Enrollment.Constants.IS_DEENROLLED).exists(false));
        }

        if ((req.getFromTime() != null) || (req.getTillTime() != null)) {
            Criteria criteria = null;
            if (req.getFromTime() != null) {
                criteria = Criteria.where(Enrollment.Constants.CREATION_TIME).gte(req.getFromTime());
            }

            if (req.getTillTime() != null) {
                if (criteria != null) {
                    criteria = criteria
                            .andOperator(Criteria.where(Enrollment.Constants.CREATION_TIME).lt(req.getTillTime()));
                } else {
                    criteria = Criteria.where(Enrollment.Constants.CREATION_TIME).lt(req.getTillTime());
                }
            }
            query.addCriteria(criteria);
        }

        if(ArrayUtils.isNotEmpty(req.getIncludeFields())){
            req.getIncludeFields().forEach(field->query.fields().include(field));
        }

        if(ArrayUtils.isNotEmpty(req.getExcludeFields())){
            req.getExcludeFields().forEach(field->query.fields().exclude(field));
        }

        Integer start = req.getStart() == null ? 0 : req.getStart();
        query.skip(start);

        Integer size = req.getSize() == null ? 20 : req.getSize();
        query.limit(size);
        query.with(Sort.by(Sort.Direction.DESC, Enrollment.Constants._ID));
        return runQuery(query, Enrollment.class);
    }

    public List<Enrollment> getEnrollmentsDataForSessionStrip(GetEnrollmentsReq req) {
        Query query = new Query();

        query.addCriteria(Criteria.where(Enrollment.Constants.USER_ID).is(req.getUserId().toString()));
        query.addCriteria(Criteria.where(Enrollment.Constants.STATUS).is(EntityStatus.ACTIVE));

        query.fields().include(Enrollment.Constants.ENTITY_TITLE);
        query.fields().include(Enrollment.Constants.COURSE_ID);
        query.fields().include(Enrollment.Constants.BATCH_ID);

        return runQuery(query, Enrollment.class);
    }

    @Deprecated
    public List<Enrollment> getActiveStudentEnrollmentsByBatchIds(List<String> batchIds) {
        return getActiveStudentEnrollmentsByBatchIds(batchIds,null,null);
    }
    @Deprecated
    public List<Enrollment> getActiveStudentEnrollmentsByBatchIds(List<String> batchIds,List<String> enrollmentDataIncludeField,List<String> enrollmentDataExcludeField) {
        Query query = new Query();
        query.addCriteria(Criteria.where(Enrollment.Constants.ROLE).is(Role.STUDENT));
        query.addCriteria(Criteria.where(Enrollment.Constants.STATUS).is(EntityStatus.ACTIVE));
        query.addCriteria(Criteria.where(Enrollment.Constants.BATCH_ID).in(batchIds));

        if(ArrayUtils.isNotEmpty(enrollmentDataIncludeField)){
            enrollmentDataIncludeField.forEach(field->query.fields().include(field));
        }

        if(ArrayUtils.isNotEmpty(enrollmentDataExcludeField)){
            enrollmentDataExcludeField.forEach(field->query.fields().exclude(field));
        }

        query.skip(0);
        query.limit(EnrollmentDAO.MAX_ALLOWED_FETCH_SIZE);
        List<Enrollment> enrollments = runQuery(query, Enrollment.class);

        return enrollments;
    }

    public List<Enrollment> getUserIdEnrollmentBatchMap(List<String> userIds, List<String> batchIds) {
        Query query = new Query();
        query.addCriteria(Criteria.where(Enrollment.Constants.ROLE).is(Role.STUDENT));
        query.addCriteria(Criteria.where(Enrollment.Constants.STATUS).is(EntityStatus.ACTIVE));
        query.addCriteria(Criteria.where(Enrollment.Constants.BATCH_ID).in(batchIds));
        query.addCriteria(Criteria.where(Enrollment.Constants.USER_ID).in(userIds));
        List<Enrollment> enrollments = runQuery(query, Enrollment.class);
        return enrollments;
    }

    public List<Enrollment> getEnrollmentForEntityId(String userId, String entityId) {
        Query query = new Query();
        query.addCriteria(Criteria.where(Enrollment.Constants.ROLE).is(Role.STUDENT));
        query.addCriteria(Criteria.where(Enrollment.Constants.STATUS).is(EntityStatus.ACTIVE));
        query.addCriteria(Criteria.where(Enrollment.Constants.BATCH_ID).exists(true));
        query.addCriteria(Criteria.where(Enrollment.Constants.ENTITY_ID).is(entityId));
        query.addCriteria(Criteria.where(Enrollment.Constants.USER_ID).is(userId));
        List<Enrollment> enrollments = runQuery(query, Enrollment.class);
        return enrollments;
    }

    public void sendToSqsList(List<Enrollment> enrollments) {
        if (ArrayUtils.isNotEmpty(enrollments)) {
            int count = 1;
            for (Enrollment enrollment : enrollments) {
                if (Role.STUDENT.equals(enrollment.getRole())) {
                    awsSQSManager.triggerAsyncSQS(SQSQueue.ENROLLMENT_CREATED_LEADSQUARED_QUEUE, SQSMessageType.ENROLLMENT_CREATED, gson.toJson(enrollment), enrollment.getUserId() + "_ENRO_" + count);

                }

            }
        }
    }

    public List<Enrollment> getEnrollmentsForUserId(String userId, int start, int size) {

        Query query = new Query();

        query.addCriteria(Criteria.where(Enrollment.Constants.USER_ID).is(userId));
        query.addCriteria(Criteria.where(Enrollment.Constants.STATUS).is(EntityStatus.ACTIVE));
        setFetchParameters(query, start, size);

        return runQuery(query, Enrollment.class);

    }

    public List<Enrollment> getEnrollmentsForUserId(String userId) {

        Query query = new Query();

        query.addCriteria(Criteria.where(Enrollment.Constants.USER_ID).is(userId));
        query.addCriteria(Criteria.where(Enrollment.Constants.STATUS).in(EntityStatus.ACTIVE,EntityStatus.INACTIVE));

        return runQuery(query, Enrollment.class);

    }

    public List<Enrollment> getEnrollmentsForUserId(List<String> userIds) {

        Query query = new Query();

        query.addCriteria(Criteria.where(Enrollment.Constants.USER_ID).in(userIds));
        query.addCriteria(Criteria.where(Enrollment.Constants.STATUS).in(EntityStatus.ACTIVE,EntityStatus.INACTIVE));

        return runQuery(query, Enrollment.class);

    }

    public List<Enrollment> getAllUserEnrollments(String userId, int limit, List<String> includeFields) {

        Query query = new Query();

        query.addCriteria(Criteria.where(Enrollment.Constants.USER_ID).is(userId));
        query.limit(50);
        query.with(Sort.by(Sort.Direction.DESC, _ID));

        return runQuery(query, Enrollment.class, includeFields);

    }

    public List<Enrollment> getEnrollmentByCoursesAndUser(List<String> courseIds, String userId,
                                                          List<EntityStatus> statuses) {
        Query query = new Query();
        query.addCriteria(Criteria.where(Enrollment.Constants.USER_ID).is(userId));
        query.addCriteria(Criteria.where(Enrollment.Constants.COURSE_ID).in(courseIds));
        if (ArrayUtils.isNotEmpty(statuses)) {
            query.addCriteria(Criteria.where(Enrollment.Constants.STATUS).in(statuses));
        }
        return runQuery(query, Enrollment.class);

    }

    public List<Enrollment> getEnrollmentByBatchAndUser(List<String> batchIds, String userId,
                                                        List<EntityStatus> statuses) {
        Query query = new Query();
        query.addCriteria(Criteria.where(Enrollment.Constants.USER_ID).is(userId));
        query.addCriteria(Criteria.where(Enrollment.Constants.BATCH_ID).in(batchIds));
        if (ArrayUtils.isNotEmpty(statuses)) {
            query.addCriteria(Criteria.where(Enrollment.Constants.STATUS).in(statuses));
        }
        return runQuery(query, Enrollment.class);

    }

    public long getActiveEnrollmentCountForUserInBatchIds(List<String> batchIds, String userId) {
        Query query = new Query();
        query.addCriteria(Criteria.where(Enrollment.Constants.USER_ID).is(userId));
        query.addCriteria(Criteria.where(Enrollment.Constants.BATCH_ID).in(batchIds));
        query.addCriteria(Criteria.where(Enrollment.Constants.STATUS).is(EntityStatus.ACTIVE));
        return queryCount(query, Enrollment.class);
    }

    public List<Enrollment> getActiveEnrollmentsForPurchaseContextId(String purchaseContextId,
                                                                     String otmBundleId,
                                                                     int start, int size) {
        Query query = new Query();
        query.addCriteria(Criteria.where(Enrollment.Constants.PURCHASE_CONTEXT_ID).is(purchaseContextId));
        if (StringUtils.isNotEmpty(otmBundleId)) {
            query.addCriteria(Criteria.where(Enrollment.Constants.ENTITY_ID).is(otmBundleId));
        }
        query.addCriteria(Criteria.where(Enrollment.Constants.STATUS).is(EntityStatus.ACTIVE));
        setFetchParameters(query, start, size);
        return runQuery(query, Enrollment.class);

    }

    public List<Enrollment> getEnrollmentsForPurchaseContextIdForUserId(String purchaseContextId, String userId) {
        Query query = new Query();
        query.addCriteria(Criteria.where(Enrollment.Constants.USER_ID).is(userId));
        query.addCriteria(Criteria.where(Enrollment.Constants.PURCHASE_CONTEXT_ID).is(purchaseContextId));
        query.addCriteria(Criteria.where(Enrollment.Constants.PURCHASE_CONTEXT_ENROLLMENT_TYPE).ne(EnrollmentType.UPSELL));
        query.addCriteria(Criteria.where(Enrollment.Constants.STATUS).in(Arrays.asList(EntityStatus.ACTIVE, EntityStatus.INACTIVE)));

        return runQuery(query, Enrollment.class);

    }

    public List<Enrollment> getEnrollmentsForEntities(String userId, EntityType entityType, String entityId,
                                                      List<EntityStatus> statuses, boolean doNotFetchEnrollmentsWithPurchaseContext) {
        Query query = new Query();

        query.addCriteria(Criteria.where(Enrollment.Constants.USER_ID).is(userId));

        if (ArrayUtils.isNotEmpty(statuses)) {
            query.addCriteria(Criteria.where(Enrollment.Constants.STATUS).in(statuses));
        }

        query.addCriteria(Criteria.where(Enrollment.Constants.ENTITY_TYPE).is(entityType));
        query.addCriteria(Criteria.where(Enrollment.Constants.ENTITY_ID).is(entityId));

        if (doNotFetchEnrollmentsWithPurchaseContext) {
            query.addCriteria(Criteria.where(Enrollment.Constants.PURCHASE_CONTEXT_ID).exists(false));
        }

        query.with(Sort.by(Sort.Direction.DESC, Enrollment.Constants.CREATION_TIME));
        return runQuery(query, Enrollment.class);
    }
    public void updateMultiple(Query query,Update update){
        updateMulti(query,update, Enrollment.class);
    }

    public List<Enrollment> getActiveAndInactiveStudentEnrollmentsByBatchIds(List<String> batchIds) {
        Query query = new Query();
        query.addCriteria(Criteria.where(Enrollment.Constants.ROLE).is(Role.STUDENT));
        query.addCriteria(Criteria.where(Enrollment.Constants.STATUS).in(Arrays.asList(
                EntityStatus.ACTIVE,EntityStatus.INACTIVE
        )));
        query.addCriteria(Criteria.where(Enrollment.Constants.BATCH_ID).in(batchIds));
        query.limit(EnrollmentDAO.MAX_ALLOWED_FETCH_SIZE);
        List<Enrollment> enrollments = runQuery(query, Enrollment.class);
//        if (enrollments.size() > EnrollmentDAO.MAX_ALLOWED_FETCH_SIZE) {
//            logger.error("more than MAX_ALLOWED_FETCH_SIZE found for getActiveStudentEnrollmentsByBatchIds ");
//        }
        return enrollments;
    }

    public List<Enrollment> getBatchIdsOfActiveEnrollmentsOfUser(GetUserEnrollmentsReq req) {
        Query query = new Query();

        query.addCriteria(Criteria.where(Enrollment.Constants.USER_ID).is(req.getUserId().toString()));
        query.addCriteria(Criteria.where(Enrollment.Constants.STATUS).is(EntityStatus.ACTIVE));
        if(req.getIncludeCourseId()){
            query.fields().include(Enrollment.Constants.COURSE_ID);
        }
        query.fields().include(Enrollment.Constants.BATCH_ID);
        query.fields().exclude(Enrollment.Constants._ID);
        return runQuery(query, Enrollment.class);

    }


    public List<Enrollment> getAllBatchIdsOfUser(String userId) {
        Query query = new Query();
        query.addCriteria(Criteria.where(Enrollment.Constants.USER_ID).is(userId));
        query.fields().include(Enrollment.Constants.BATCH_ID);
        query.fields().include(Enrollment.Constants.BATCH_CHANGE_TIME);

        List<Enrollment> enrollments = runQuery(query, Enrollment.class);
        return enrollments;
    }



    public List<Enrollment> getActiveUserIdsByBatchIds(ActiveUserIdsByBatchIdsReq req) {
        Query query = new Query();
        query.addCriteria(Criteria.where(Enrollment.Constants.ROLE).is(Role.STUDENT));
        query.addCriteria(Criteria.where(Enrollment.Constants.STATUS).is(EntityStatus.ACTIVE));
        query.addCriteria(Criteria.where(Enrollment.Constants.BATCH_ID).in(req.getBatchIds()));
        query.fields().include(Enrollment.Constants.BATCH_ID);
        query.fields().include(Enrollment.Constants.USER_ID);
        query.fields().include(Enrollment.Constants.BATCH_CHANGE_TIME);
        query.fields().include(Enrollment.Constants.SECTION_ID);
        query.skip(req.getStart());
        if(req.getSize() == 0 ) {
            query.limit(EnrollmentDAO.MAX_ALLOWED_FETCH_SIZE);
        }
        else{
            query.limit(req.getSize());
        }
        List<Enrollment> enrollments = runQuery(query, Enrollment.class);
//        if (enrollments.size() >= EnrollmentDAO.MAX_ALLOWED_FETCH_SIZE) {
//            logger.error("more than MAX_ALLOWED_FETCH_SIZE found for getActiveUserIdsByBatchIds  for query "  + query);
//        }
        return enrollments;
    }

    // needs an index batchId_1__id_1
    public List<Enrollment> getActiveStudentsInBatches(List<String> batchIds, String fetchAfterId, int limit) {
        Query q = new Query();

        q.addCriteria(Criteria.where(ROLE).is(Role.STUDENT));
        q.addCriteria(Criteria.where(STATUS).is(EntityStatus.ACTIVE));
        q.addCriteria(Criteria.where(BATCH_ID).in(batchIds));
        // fetch params
        if (StringUtils.isNotEmpty(fetchAfterId)) {
            q.addCriteria(Criteria.where(_ID).gt(new ObjectId(fetchAfterId)));
        }
        q.limit(limit);
        q.with(Sort.by(Sort.Direction.ASC, BATCH_ID, _ID));

        // include fields
        q.fields().include(BATCH_ID);
        q.fields().include(USER_ID);
        q.fields().include(BATCH_CHANGE_TIME);
        q.fields().include(SECTION_ID);
        q.fields().include(SECTION_STATE);
        return runQuery(q, Enrollment.class);
    }

    public long getEnrollmentCountInCourse(String courseId) {
        if(StringUtils.isEmpty(courseId)){
            return 0;
        }
        Query query = new Query();
        query.addCriteria(Criteria.where(Enrollment.Constants.COURSE_ID).is(courseId));
        query.addCriteria(Criteria.where(Enrollment.Constants.STATUS).is(EntityStatus.ACTIVE));
        return queryCount(query, Enrollment.class);
    }

    public long getEnrollmentCountInBatch(String batchId) {
        if(StringUtils.isEmpty(batchId)){
            return 0;
        }
        Query query = new Query();
        query.addCriteria(Criteria.where(Enrollment.Constants.BATCH_ID).is(batchId));
        query.addCriteria(Criteria.where(Enrollment.Constants.STATUS).is(EntityStatus.ACTIVE));
        return queryCount(query, Enrollment.class);
    }


    public long getActiveAndInactiveEnrollmentCountByUserIdAndCourseIds(String userId, List<String> courseIds) throws BadRequestException {

        if(StringUtils.isEmpty(userId) || ArrayUtils.isEmpty(courseIds)){
            throw new BadRequestException(ErrorCode.INVALID_PARAMETER,"userId and courseIds are mandatory fields and " +
                    "cann't be null with data userId - "+userId+" and courseIds - "+courseIds);
        }

        Query query=new Query();
        query.addCriteria(Criteria.where(Enrollment.Constants.USER_ID).is(userId));
        query.addCriteria(Criteria.where(Enrollment.Constants.COURSE_ID).in(courseIds));
        query.addCriteria(Criteria.where(Enrollment.Constants.STATUS).in(EntityStatus.ACTIVE,EntityStatus.INACTIVE));

        query.fields().include(Enrollment.Constants._ID);

        return queryCount(query,Enrollment.class);
    }

    public List<Enrollment> getEnrolledStudentsInBatch(String batchId, List<String> includeFields, int start, int size) {
        Query query = new Query();
        query.addCriteria(Criteria.where(Enrollment.Constants.BATCH_ID).is(batchId));
        query.addCriteria(Criteria.where(Enrollment.Constants.ROLE).is(Role.STUDENT));
        query.addCriteria(Criteria.where(Enrollment.Constants.STATUS).is(EntityStatus.ACTIVE));
        query.fields().include(Enrollment.Constants.USER_ID)
                .include(Enrollment.Constants.BATCH_ID)
                .include(Enrollment.Constants.ID);
        setFetchParameters(query, start, size);
        query.with(Sort.by(Sort.Direction.ASC, Enrollment.Constants.BATCH_ID));
        return runQuery(query, Enrollment.class, includeFields);
    }

    public List<Enrollment> getUserActiveAndInactiveEnrollment(Long userId, Integer start, Integer size,List<String> includeFields) throws BadRequestException {

        logger.info("getUserActiveAndInactiveEnrollment - userId - {} - start - {} size - {}",userId,start,size);

        if(Objects.isNull(userId) || Objects.isNull(start) || Objects.isNull(size)){
            throw new BadRequestException(ErrorCode.INVALID_PARAMETER,"Some of the parameters are empty");
        }

        Query query=new Query();
        query.addCriteria(Criteria.where(Enrollment.Constants.USER_ID).is(Long.toString(userId)));
        query.addCriteria(Criteria.where(Enrollment.Constants.STATUS).in(EntityStatus.ACTIVE,EntityStatus.INACTIVE));

        query.skip(start);
        query.limit(size);

        Optional.ofNullable(includeFields).map(Collection::stream).orElseGet(Stream::empty).forEach(query.fields()::include);

        logger.info("getUserActiveAndInactiveEnrollment - query {}",query);
        return runQuery(query,Enrollment.class);
    }

    public List<Enrollment> getNonEndedEnrolledBatchesForStudent(String userId, Integer start, Integer size) {

        Query query = new Query();
        query.addCriteria(Criteria.where(Enrollment.Constants.ENTITY_STATE).is(EntityState.ACTIVE));
        query.addCriteria(Criteria.where(Enrollment.Constants.USER_ID).is(userId));
        query.addCriteria(Criteria.where(Enrollment.Constants.BATCH_ID).exists(true));
        query.addCriteria(Criteria.where(Enrollment.Constants.STATUS).ne(EntityStatus.ENDED));
        query.with(Sort.by(Sort.Direction.DESC, Enrollment.Constants.CREATION_TIME));
        setFetchParameters(query, start, size);
        return runQuery(query, Enrollment.class);
    }

    public List<Enrollment> getActiveStudentEnrollmentsUserIdsByBatchIds(List<String> batchIds, List<String> includeFields) {
        Query query = new Query();
        query.addCriteria(Criteria.where(Enrollment.Constants.ROLE).is(Role.STUDENT));
        query.addCriteria(Criteria.where(Enrollment.Constants.STATUS).is(EntityStatus.ACTIVE));
        query.addCriteria(Criteria.where(Enrollment.Constants.BATCH_ID).in(batchIds));
        query.limit(EnrollmentDAO.MAX_ALLOWED_FETCH_SIZE);
        if (ArrayUtils.isNotEmpty(includeFields)) {
            includeFields.forEach(field -> query.fields().include(field));
        }
        return runQuery(query, Enrollment.class);

    }

    public List<SectionEnrollmentAggregationRes> getSectionIdEnrollmentCountForSections(String batchId, List<EnrollmentState> enrollmentState) {
        if(batchId.isEmpty() || Objects.isNull(enrollmentState))
            return new ArrayList<>();

        List<SectionEnrollmentAggregationRes> response = new ArrayList<>();
        List<AggregationOperation> aggregationOperation = new ArrayList<>();
        aggregationOperation.add(Aggregation.match(Criteria.where(Enrollment.Constants.BATCH_ID).is(batchId)));
        aggregationOperation.add(Aggregation.match(Criteria.where(SECTION_STATE).in(enrollmentState)));
        aggregationOperation.add(Aggregation.match(Criteria.where(STATUS).ne(EntityStatus.ENDED)));
        aggregationOperation.add(Aggregation.group(Enrollment.Constants.SECTION_ID).count().as("enrollmentsCount"));
        aggregationOperation.add(Aggregation.sort(Sort.Direction.ASC,"enrollmentsCount"));

        AggregationOptions aggregationOptions = Aggregation.newAggregationOptions().cursor(new Document()).build();
        Aggregation aggregation = Aggregation.newAggregation(aggregationOperation).withOptions(aggregationOptions);

        logger.info("\n-------------------------- section-aggregation-query----------------------\n" + aggregation + "\n");

        AggregationResults<SectionEnrollmentAggregationRes> results = getMongoOperations().aggregate(aggregation,
                Enrollment.class, SectionEnrollmentAggregationRes.class);

        response = results.getMappedResults();
        return response;
    }

    // index used for winning plan -- userId
    public List<Enrollment> getSectionEnrollmentsForUserIdBatchId(String batchId, String userId, Integer skip, Integer limit ) {
        Query query = new Query();
        query.addCriteria(Criteria.where(Enrollment.Constants.BATCH_ID).is(batchId));
        query.addCriteria(Criteria.where(Enrollment.Constants.USER_ID).is(userId));
        query.addCriteria(Criteria.where(Enrollment.Constants.SECTION_ID).exists(true));
        query.addCriteria(Criteria.where(SECTION_STATE).exists(true));
        query.addCriteria(Criteria.where(STATUS).ne(EntityStatus.ENDED));
        query.with(Sort.by(Sort.Direction.ASC, Enrollment.Constants._ID));
        query.fields().include(Enrollment.Constants.SECTION_ID);
        query.fields().include(SECTION_STATE);

        setFetchParameters(query,skip,limit);

        logger.info("query {}",query);
        return runQuery(query,Enrollment.class);
    }

    // index used in winning plan -- _id
    public List<Enrollment> getSectionEnrollmentsForBatchIdSectionId(String batchId, String sectionId, List<EnrollmentState> enrollmentStates, Integer skip, Integer limit){
        Query query = new Query();

        if(Objects.nonNull(batchId))
            query.addCriteria(Criteria.where(Enrollment.Constants.BATCH_ID).is(batchId));

        if(Objects.nonNull(sectionId))
            query.addCriteria(Criteria.where(Enrollment.Constants.SECTION_ID).is(sectionId));
        else
            query.addCriteria(Criteria.where(Enrollment.Constants.SECTION_ID).exists(true));

        if(ArrayUtils.isNotEmpty(enrollmentStates))
            query.addCriteria(Criteria.where(SECTION_STATE).in(enrollmentStates));

        query.addCriteria(Criteria.where(STATUS).ne(EntityStatus.ENDED));
        query.with(Sort.by(Sort.Direction.ASC, Enrollment.Constants._ID));
        query.fields().include(Enrollment.Constants.SECTION_ID);
        query.fields().include(Enrollment.Constants.USER_ID);
        query.fields().include(SECTION_STATE);


        setFetchParameters(query,skip,limit);
        logger.info("query {}",query);
        return runQuery(query,Enrollment.class);
    }

    // index used in wining plan -- userId
    public List<Enrollment> getSectionEnrollmentsForStudentIds(String primaryBatchId, List<String> studentIds, List<EnrollmentState> enrollmentStates, Integer skip, Integer limit) {
        if( primaryBatchId == null || primaryBatchId.isEmpty() || studentIds == null || studentIds.isEmpty())
            return new ArrayList<>();

        Query query = new Query();
        query.addCriteria(Criteria.where(BATCH_ID).is(primaryBatchId));
        query.addCriteria(Criteria.where(USER_ID).in(studentIds));
        query.addCriteria(Criteria.where(STATUS).ne(EntityStatus.ENDED));
        query.addCriteria(Criteria.where(SECTION_ID).exists(true));
        query.with(Sort.by(Sort.Direction.ASC, Enrollment.Constants._ID));
        if(ArrayUtils.isNotEmpty(enrollmentStates))
            query.addCriteria(Criteria.where(SECTION_STATE).in(enrollmentStates));

        setFetchParameters(query,skip,limit);
        logger.info("query {}",query);
        return runQuery(query,Enrollment.class);
    }

    // index used in winning plan -- userId
    // node team using this
    public Enrollment getEnrollmentsForUserIdInBatchIds(List<String> batchIds, String userId) {
        if(userId.isEmpty() || batchIds.isEmpty())
            return null;

        Query query = new Query();
        query.addCriteria(Criteria.where(BATCH_ID).in(batchIds));
        query.addCriteria(Criteria.where(USER_ID).is(userId));
        query.addCriteria(Criteria.where(STATUS).is(EntityStatus.ACTIVE));
//        query.addCriteria(Criteria.where(SECTION_STATE).exists(true));
        query.fields().include(USER_ID);
        query.fields().include(BATCH_ID);
        query.fields().include(SECTION_ID);

        query.limit(1);
        logger.info("query {}",query);
        return findOne(query,Enrollment.class);
    }


    public void unsetSectionFromEnrollment(String enrollmentId){
        Query _q = new Query();
        _q.addCriteria(Criteria.where(Enrollment.Constants._ID).is(new ObjectId(enrollmentId)));
        Update update = new Update();
        update.unset(Enrollment.Constants.SECTION_ID);
        update.unset(Enrollment.Constants.SECTION_STATE);
        updateFirst(_q,update,Enrollment.class);
    }

    public void updateSingle(Query query, Update update){
        updateFirst(query,update,Enrollment.class);

    }
    
    public List<Enrollment> getAppEnrollmentsOfUser(UserPremiumSubscriptionEnrollmentReq req) {

    	logger.info("getActiveEnrollmentsOfUserForApp - userId - {} - start - {} size - {}",req.getUserId(),req.getStart(),req.getSize());
    	Query query = new Query();
    	query.addCriteria(Criteria.where(Enrollment.Constants.USER_ID).is(req.getUserId().toString()));
    	query.addCriteria(Criteria.where(Enrollment.Constants.STATUS).in(EntityStatus.ACTIVE, EntityStatus.INACTIVE));
    	if(ArrayUtils.isNotEmpty(req.getBatchIds())) {
    		query.addCriteria(Criteria.where(Enrollment.Constants.BATCH_ID).in(req.getBatchIds()));
    	}
    	if(ArrayUtils.isNotEmpty(req.getBundleIds())) {
    		query.addCriteria(Criteria.where(Enrollment.Constants.PURCHASE_CONTEXT_ID).in(req.getBundleIds()));
    	}
    	if(req.isIncludeCourseId()){
    		query.fields().include(Enrollment.Constants.COURSE_ID);
    	}
    	query.fields().include(Enrollment.Constants.BATCH_ID);
    	query.fields().include(Enrollment.Constants.PURCHASE_CONTEXT_ID);
    	query.fields().include(Enrollment.Constants.STATE);
    	query.fields().include(Enrollment.Constants.STATUS);
    	query.fields().exclude(Enrollment.Constants._ID);
    	setFetchParameters(query, req.getStart(), req.getSize());
    	query.with(Sort.by(Sort.Direction.DESC, Enrollment.Constants.CREATION_TIME));
    	logger.info("getActiveEnrollmentsOfUserForApp - query {}",query);
    	return runQuery(query, Enrollment.class);

    }

    public void setSectionToEnrollment(String enrollmentId, String sectionId,
                                       EnrollmentState sectionState){
        Query query = new Query();
        query.addCriteria(Criteria.where(Enrollment.Constants._ID).is(new ObjectId(enrollmentId)));

        Update update = new Update();
        update.set(Enrollment.Constants.SECTION_ID,sectionId);
        update.set(Enrollment.Constants.SECTION_STATE,sectionState);
        updateFirst(query,update,Enrollment.class);
    }

    public List<Enrollment> getByIdsWithoutProjection(List<String> ids, int start, int size) {
        Query q = new Query();
        q.addCriteria(Criteria.where(Enrollment.Constants._ID).in(ids));
        setFetchParameters(q, start, size);
        return runQuery(q, Enrollment.class);
    }

    // partial index - on partial filter expression role : STUDENT and entityType : OTF_COURSE - directly added on db
    public List<Enrollment> getCourseEnrollments(String courseId, List<EntityState> status, String fetchAfterId,
                                                 List<String> includeFields, int limit) {
        Query query = new Query();

        // Equality
        query.addCriteria(Criteria.where(COURSE_ID).is(courseId));
        query.addCriteria(Criteria.where(STATUS).in(status));
        query.addCriteria(Criteria.where(ROLE).is(Role.STUDENT));
        query.addCriteria(Criteria.where(ENTITY_TYPE).is(EntityType.OTF_COURSE));

        // Sort
        query.with(Sort.by(Sort.Direction.ASC, Enrollment.Constants._ID));

        // Range
        if (StringUtils.isNotEmpty(fetchAfterId)) {
            query.addCriteria(Criteria.where(_ID).gt(new ObjectId(fetchAfterId)));
        } else {
            query.addCriteria(Criteria.where(_ID).gt(AbstractMongoDAO.MIN_KEY_OBJECT_ID));
        }

        query.limit(limit);
        includeFieldsForProjection(query, includeFields);

        logger.info("Fetch course enrolments query : {}", query);
        return runQuery(query, Enrollment.class);
    }

    public void updateEntityTitle(List<String> enrollmentIds, String entityTitle) {
        logger.info("updating entity title for enrollments");
        updateMulti(new Query(Criteria.where(_ID).in(enrollmentIds)),
                new Update().set(ENTITY_TITLE, entityTitle), Enrollment.class);
    }

}
