package com.vedantu.subscription.dao;

import com.mongodb.*;
import com.vedantu.session.pojo.EntityType;
import com.vedantu.subscription.entities.mongo.Bundle;
import com.vedantu.subscription.entities.mongo.BundleVideo;
import com.vedantu.subscription.entities.mongo.VideoEngagement;
import com.vedantu.subscription.request.GetBundlesReq;
import com.vedantu.subscription.viewobject.request.VideoEngagementReq;
import com.vedantu.subscription.viewobject.response.BundleVideoFilterRes;
import com.vedantu.util.CollectionUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import org.apache.logging.log4j.Logger;
import org.bson.Document;
import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationOperation;
import org.springframework.data.mongodb.core.aggregation.AggregationOptions;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
/**
 * Created by darshit on 18/8/19.
 */
@Service
public class VideoEngagementDAO extends AbstractMongoDAO {

	@Autowired
	private LogFactory logFactory;

	private final Logger logger = logFactory.getLogger(VideoEngagementDAO.class);

	@Autowired
	private MongoClientFactory mongoClientFactory;

	public VideoEngagementDAO() {
		super();
	}

	@Override
	protected MongoOperations getMongoOperations() {
		return mongoClientFactory.getMongoOperations();
	}

	public void save(VideoEngagement p, Long callingUserId) {
		// try {
		if (p != null) {
			String callingUserIdString = null;
			if (callingUserId != null) {
				callingUserIdString = callingUserId.toString();
			}
			logger.info("Saving into db" + p);
			saveEntity(p, callingUserIdString);
		}
	}


	public List<VideoEngagement> getEngagementByReq(VideoEngagementReq req) {
		Query query = new Query();
		if (req.getUserId() != null) {
			query.addCriteria(Criteria.where(VideoEngagement.Constants.USERID).is(req.getUserId()));
		}
		if (req.getBundleEnrollmentId() != null) {
			query.addCriteria(Criteria.where(VideoEngagement.Constants.BUNDLEENROLLMENTID).is(req.getBundleEnrollmentId()));
		}
		if (req.getVideoId() != null) {
			query.addCriteria(Criteria.where(VideoEngagement.Constants.VIDEOID).is(req.getVideoId()));
		}
		logger.info("VideoEngagement query: " + query);
		return runQuery(query, VideoEngagement.class);
	}


}
