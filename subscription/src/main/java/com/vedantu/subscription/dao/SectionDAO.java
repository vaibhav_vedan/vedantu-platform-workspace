package com.vedantu.subscription.dao;

import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.VRuntimeException;
import com.vedantu.onetofew.enums.EnrollmentState;
import com.vedantu.subscription.entities.mongo.Enrollment;
import com.vedantu.subscription.entities.mongo.Section;
import com.vedantu.subscription.response.section.BatchSectionAggregationRes;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.dbentitybeans.mongo.EntityState;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.Logger;
import org.bson.Document;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.FindAndModifyOptions;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationOperation;
import org.springframework.data.mongodb.core.aggregation.AggregationOptions;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.query.Collation;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Stream;

@Service
public class SectionDAO extends AbstractMongoDAO {


    @Autowired
    private MongoClientFactory mongoClientFactory;

    @Autowired
    private LogFactory logFactory;

    private Logger logger = logFactory.getLogger(SectionDAO.class);

    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }

    public SectionDAO(){
        super();
    }

    public void save(Section s) {
        try {
            if (s != null) {
                saveEntity(s);
            }
        } catch (Exception ex) {
            // throw Exception
            logger.error("Section save failed for: " + (s.getId()) + ", error: " + ex.getMessage());
        }
    }

    public Section getById(String sectionId){
        Section section = null;

        try {

            if(!StringUtils.isEmpty(sectionId)){
                section = getEntityById(sectionId,Section.class);
            }
        }
        catch(Exception e){
            logger.error("Error fetching section with id: " + sectionId);
        }

        return section;
    }

    public void updateAll(List<Section> s) {
        try {
            if (s != null) {
                insertAllEntities(s, Section.class.getSimpleName());
            }
        } catch (Exception ex) {
            // throw Exception;
            logger.info("Section updateAll failed error: "+ ex.getMessage());
            throw new VRuntimeException(ErrorCode.DUPLICATE_SECTION_NAME,"A section with same name already exists for this batch !", Level.ERROR);
        }
    }

    // index used for winning plan -- batchId-sectionName
    public List<Section> getSectionsForBatchId(String batchId, Integer skip, Integer limit, List<EnrollmentState>enrollmentStateList){
        List<Section> sections = null;
        Query query = new Query();
        query.addCriteria(Criteria.where(Section.Constants.BATCH_ID).is(batchId));
        query.addCriteria(Criteria.where(Section.Constants.ENTITY_STATE).ne((EntityState.DELETED)));

        if(ArrayUtils.isNotEmpty(enrollmentStateList))
            query.addCriteria(Criteria.where(Section.Constants.SECTION_TYPE).in(enrollmentStateList));

        query.with(Sort.by(Sort.Direction.ASC, Section.Constants._ID));

        setFetchParameters(query,skip,limit);
        logger.info("query {}",query);
        sections = runQuery(query,Section.class);
        return sections;
    }

    // for testing purpose, not used any where as of now
    public List<Section> getDeletedSectionsForBatchId(String batchId) {
        if(Objects.isNull(batchId) || batchId.isEmpty())
            return new ArrayList<>();
        Query query = new Query();
        query.addCriteria(Criteria.where(Section.Constants.BATCH_ID).is(batchId));
        query.addCriteria(Criteria.where(Section.Constants.ENTITY_STATE).in(Arrays.asList(EntityState.DELETED)));
        query.with(Sort.by(Sort.Direction.ASC, Section.Constants._ID));
        return runQuery(query,Section.class);
    }

    // uses default index -- _id
    public List<Section> getSectionsById(List<String> sectionIds, List<String> includeFields){
        if(sectionIds == null || sectionIds.isEmpty())
            return new ArrayList<>();
        Query query = new Query();
        query.addCriteria(Criteria.where(Section.Constants._ID).in(sectionIds));
        query.addCriteria(Criteria.where(Section.Constants.ENTITY_STATE).ne(EntityState.DELETED));
        query.with(Sort.by(Sort.Direction.ASC, Section.Constants._ID));

        if(Objects.nonNull(includeFields)){
            for(String field:includeFields){
                query.fields().include(field);
            }
        }
        return runQuery(query,Section.class);
    }

    // index used for winning plan -- batchId-sectionName
    public List<Section> getAllSectionsForBatchId(String batchId, Integer skip, Integer limit, List<EnrollmentState>enrollmentStateList){
        List<Section> sections = null;
        Query query = new Query();
        query.addCriteria(Criteria.where(Section.Constants.BATCH_ID).is(batchId));

        if(ArrayUtils.isNotEmpty(enrollmentStateList))
            query.addCriteria(Criteria.where(Section.Constants.SECTION_TYPE).in(enrollmentStateList));

        query.with(Sort.by(Sort.Direction.ASC, Section.Constants.SECTION_NAME));
        query.collation(Collation.of(Locale.ENGLISH).numericOrdering(true));
        setFetchParameters(query,skip,limit);
        logger.info("query {}",query);
        sections = runQuery(query,Section.class);
        return sections;
    }

    public List<Section> getSectionsForBatchIds(List<String> batchIds,Integer skip, Integer limit) {
        Query query = new Query();
        query.addCriteria(Criteria.where(Section.Constants.BATCH_ID).in(batchIds));
        query.addCriteria(Criteria.where(Section.Constants.ENTITY_STATE).ne((EntityState.DELETED)));
        query.fields().include(Section.Constants._ID);
        query.fields().include(Section.Constants.BATCH_ID);
        query.fields().include(Section.Constants.TEACHER_IDS);
        query.fields().include(Section.Constants.SECTION_NAME);
        query.fields().include(Section.Constants.SECTION_TYPE);
        query.with(Sort.by(Sort.Direction.DESC, Section.Constants._ID));
        setFetchParameters(query,skip,limit);
        return runQuery(query,Section.class);
    }

    public Section getSectionsForBatchIdsUserId(List<String> batchIds, String userId) {
        Query query = new Query();
        query.addCriteria(Criteria.where(Section.Constants.BATCH_ID).in(batchIds));
        query.addCriteria(Criteria.where(Section.Constants.TEACHER_IDS).is(userId));
        query.addCriteria(Criteria.where(Section.Constants.ENTITY_STATE).ne((EntityState.DELETED)));
        query.fields().include(Section.Constants._ID);
        query.fields().include(Section.Constants.BATCH_ID);
        query.fields().include(Section.Constants.TEACHER_IDS);
        return findOne(query,Section.class);
    }

    public List<Section> getSectionsInfoForTAs(List<String> batchIds, List<String> taIds, Integer skip, Integer limit , List<String> includeFields){
        Query query = new Query();
        query.addCriteria(Criteria.where(Section.Constants.BATCH_ID).in(batchIds));
        query.addCriteria(Criteria.where(Section.Constants.ENTITY_STATE).ne((EntityState.DELETED)));
        query.addCriteria(Criteria.where(Section.Constants.TEACHER_IDS).in(taIds));
        query.with(Sort.by(Sort.Direction.DESC, Section.Constants._ID));
        for(String field:includeFields)
            query.fields().include(field);
        
        setFetchParameters(query,skip,limit);
        return runQuery(query,Section.class);
    }

    public List<BatchSectionAggregationRes> getSectionCountForBatchIds(List<String>batchIds){
        List<BatchSectionAggregationRes> response = new ArrayList<>();
        List<AggregationOperation> aggregationOperation = new ArrayList<>();
        aggregationOperation.add(Aggregation.match(Criteria.where(Enrollment.Constants.BATCH_ID).in(batchIds)));
        aggregationOperation.add(Aggregation.group(Enrollment.Constants.BATCH_ID).count().as("sectionsCount"));
        aggregationOperation.add(Aggregation.sort(Sort.Direction.ASC,"sectionsCount"));

        AggregationOptions aggregationOptions = Aggregation.newAggregationOptions().cursor(new Document()).build();
        Aggregation aggregation = Aggregation.newAggregation(aggregationOperation).withOptions(aggregationOptions);

        logger.info("\n-------------------------- section-aggregation-query----------------------\n" + aggregation + "\n");

        AggregationResults<BatchSectionAggregationRes> results = getMongoOperations().aggregate(aggregation,
                Section.class, BatchSectionAggregationRes.class);

        response = results.getMappedResults();
        return response;
    }

    public Long getSectionCountForBatch(String batchId){
        Query query = new Query();
        query.addCriteria(Criteria.where(Section.Constants.BATCH_ID).is(batchId));
        query.addCriteria(Criteria.where(Section.Constants.ENTITY_STATE).ne(EntityState.DELETED));
        return queryCount(query,Section.class);
    }

    public Section getVacantSection(String batchId, EnrollmentState sectionType) {
        Section section = findAndModifyEntity(
                Query.query(Criteria.where(Section.Constants.BATCH_ID).is(batchId)
                        .and(Section.Constants.ENTITY_STATE).ne(EntityState.DELETED)
                        .and(Section.Constants.SEATS_VACANT).gt(0)
                        .and(Section.Constants.SECTION_TYPE).is(sectionType))
                        .with(Sort.by(Sort.Direction.DESC,Section.Constants.SEATS_VACANT)),
                new Update().inc(Section.Constants.SEATS_VACANT, -1),
                        FindAndModifyOptions.options().returnNew(true),
                        Section.class);
        return section;
    }

    public void incrementVacantSeatsForSections(List<String> sectionIds, long increaseBy){
        Query query = new Query();
        query.addCriteria(Criteria.where(Section.Constants._ID).in(new ArrayList<>(sectionIds)));

        Update update = new Update();
        update.inc(Section.Constants.SEATS_VACANT,increaseBy);
        updateMulti(query,update,Section.class);
    }

    public Section getSectionForTeacher(String userId, List<String> batchIds, List<String> includeFields){
        Query query = new Query();
        query.addCriteria(Criteria.where(Section.Constants.TEACHER_IDS).in(userId));
        query.addCriteria(Criteria.where(Section.Constants.BATCH_ID).in(batchIds));
        query.addCriteria(Criteria.where(Section.Constants.ENTITY_STATE).is(EntityState.ACTIVE));
        Optional.ofNullable(includeFields).map(Collection::stream).orElseGet(Stream::empty).forEach(query.fields()::include);
        return findOne(query,Section.class);
    }
}
