package com.vedantu.subscription.dao;

import java.util.List;

import com.vedantu.dbs.sessionfactory.SqlSessionFactory;
import com.vedantu.util.dbutils.*;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vedantu.subscription.entities.sql.SubscriptionDetails;
import com.vedantu.util.LogFactory;

@Service
public class SubscriptionDetailsDAO extends com.vedantu.util.dbutils.AbstractSqlDAO {
	
	@Autowired
	private LogFactory logFactory;
	
	@SuppressWarnings("static-access")
	private Logger logger = logFactory.getLogger(SubscriptionDetailsDAO.class);

	public SubscriptionDetailsDAO() {
		super();
	}

	@Autowired
	SqlSessionFactory sqlSessionFactory;


	@Override
	protected SessionFactory getSessionFactory() {
		return sqlSessionFactory.getSessionFactory();
	}


	public void create(SubscriptionDetails subscriptionDetails, Session session) {
		create(subscriptionDetails, session, null);
	}

	public void create(SubscriptionDetails subscriptionDetails, Session session, String callingUserId) {
		try {
			if (subscriptionDetails != null) {
				saveEntity(subscriptionDetails, session, callingUserId);
			}
		} catch (Exception ex) {
			logger.error("Create SubscriptionDetails: "+ex.getMessage());
		}
	}

	public SubscriptionDetails getById(Long id, Boolean enabled, Session session) {
		SubscriptionDetails subscriptionDetails = null;
		try {
			if (id != null) {
				subscriptionDetails = (SubscriptionDetails) getEntityById(id, enabled, SubscriptionDetails.class, session);
			}
		} catch (Exception ex) {
			logger.error("getById SubscriptionDetails: "+ex.getMessage());
			subscriptionDetails = null;
		}
		return subscriptionDetails;
	}


	public void update(SubscriptionDetails p, Session session) {
		update(p, session, null);
	}

	public void update(SubscriptionDetails p, Session session, String callingUserId) {
		try {
			if (p != null) {
				updateEntity(p, session, callingUserId);
			}
		} catch (Exception ex) {
			logger.error("update SubscriptionDetails: "+ex.getMessage());
		}
	}


	public void updateAll(List<SubscriptionDetails> p, Session session) {
		updateAll(p, session, null);
	}

	public void updateAll(List<SubscriptionDetails> p, Session session, String callingUserId) {
		try {
			if (p != null) {
				updateAllEntities(p, session, callingUserId);
			}
		} catch (Exception ex) {
			logger.error("updateAll SubscriptionDetails: "+ex.getMessage());
		}
	}
}
