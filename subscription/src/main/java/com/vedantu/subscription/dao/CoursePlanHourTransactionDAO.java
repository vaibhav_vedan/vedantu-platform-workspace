package com.vedantu.subscription.dao;

import com.vedantu.dbs.sessionfactory.SqlSessionFactory;
import com.vedantu.subscription.entities.sql.CoursePlanHourTransaction;
import com.vedantu.util.LogFactory;
import com.vedantu.util.dbutils.AbstractSqlDAO;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CoursePlanHourTransactionDAO extends AbstractSqlDAO {

	@Autowired
	private LogFactory logFactory;

	@SuppressWarnings("static-access")
	private Logger logger = logFactory.getLogger(CoursePlanHourTransactionDAO.class);

	public CoursePlanHourTransactionDAO() {
		super();
	}




	@Autowired
	SqlSessionFactory sqlSessionFactory;


	@Override
	protected SessionFactory getSessionFactory() {
		return sqlSessionFactory.getSessionFactory();
	}



	public void updateAll(List<CoursePlanHourTransaction> p, Session session) {
		updateAll(p, session, null);
	}


	public void updateAll(List<CoursePlanHourTransaction> p, Session session, String callingUserId) {
		try {
			if (p != null) {
				updateAllEntities(p, session, callingUserId);
			}
		} catch (Exception ex) {
			logger.error("updateAll HourTransaction: "+ex.getMessage());
		}
	}
}
