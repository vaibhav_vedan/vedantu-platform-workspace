package com.vedantu.subscription.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.vedantu.subscription.entities.mongo.StudentSlotPreference;
import com.vedantu.util.dbutils.AbstractMongoDAO;

@Service
public class StudentSlotPreferenceDAO extends AbstractMongoDAO {
        @Autowired
        private MongoClientFactory mongoClientFactory;    


        public StudentSlotPreferenceDAO() {
            super();
        }

        @Override
        protected MongoOperations getMongoOperations() {
            return mongoClientFactory.getMongoOperations();
        }

	public void create(StudentSlotPreference p) throws Exception {
		if (p != null) {
			saveEntity(p);
		}
	}

	public StudentSlotPreference getById(String id) {
		StudentSlotPreference schedule = null;
		try {
			if (!StringUtils.isEmpty(id)) {
				schedule = (StudentSlotPreference) getEntityById(id, StudentSlotPreference.class);
			}
		} catch (Exception ex) {
			// log Exception
			schedule = null;
		}
		return schedule;
	}

	public void save(StudentSlotPreference p) {
		if (p != null) {
			saveEntity(p);
		}
	}

	public int deleteById(String id) {
		int result = 0;
		try {
			result = deleteEntityById(id, StudentSlotPreference.class);
		} catch (Exception ex) {
			// throw Exception;
		}

		return result;
	}
	
}