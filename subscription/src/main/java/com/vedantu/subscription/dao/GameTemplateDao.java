package com.vedantu.subscription.dao;

import com.google.gson.Gson;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.InternalServerErrorException;
import com.vedantu.exception.VRuntimeException;
import com.vedantu.subscription.entities.mongo.GameJourney;
import com.vedantu.subscription.entities.mongo.GameTemplate;
import com.vedantu.subscription.util.RedisDAO;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.dbentitybeans.mongo.EntityState;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * @author mano
 */
@Service
public class GameTemplateDao extends AbstractMongoDAO {

    private final Logger logger = LogFactory.getLogger(AIOGroupDAO.class);

    @Autowired
    private MongoClientFactory mongoClientFactory;

    @Autowired
    private RedisDAO redisDAO;

    private final String GAME_TEMPLATE_ACTIVE_KEY = "GAME_TEMPLATE_ACTIVE";
    private final String GAME_TEMPLATE_KEY = "GAME_TEMPLATE_";
    private final Gson gson = new Gson();

    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }

    public void save(GameTemplate template) throws InternalServerErrorException {
        logger.info("GAME TEMPLATE: " + template);
        template.verify();
        updatePrevGameToInactive();
        saveEntity(template);
        redisDAO.set(GAME_TEMPLATE_ACTIVE_KEY, template.getId());
        redisDAO.set(GAME_TEMPLATE_KEY + template.getId(), gson.toJson(template));
    }

    private void updatePrevGameToInactive() {
        Query query = new Query();
        query.addCriteria(Criteria.where(GameTemplate.Constants.ENTITY_STATE).is(EntityState.ACTIVE));
        Update update = new Update();
        update.set(GameTemplate.Constants.ENTITY_STATE, EntityState.INACTIVE);
        updateMulti(query, update, GameTemplate.class);
    }

    public GameTemplate getActiveGameTemplate() throws BadRequestException, InternalServerErrorException {
        String id = redisDAO.get(GAME_TEMPLATE_ACTIVE_KEY);
        if (id != null) {
            String templateJson = redisDAO.get(GAME_TEMPLATE_KEY + id);
            if (templateJson != null) {
                return gson.fromJson(templateJson, GameTemplate.class);
            }
        }
        GameTemplate activeTemplate = getActiveTemplate();
        redisDAO.set(GAME_TEMPLATE_ACTIVE_KEY, activeTemplate.getId());
        redisDAO.set(GAME_TEMPLATE_KEY + activeTemplate.getId(), gson.toJson(activeTemplate));
        return activeTemplate;
    }

    private GameTemplate getActiveTemplate() {
        Query query = new Query();
        query.addCriteria(Criteria.where(GameTemplate.Constants.ENTITY_STATE).is(EntityState.ACTIVE));
        query.with(Sort.by(Sort.Direction.DESC, GameTemplate.Constants.LAST_UPDATED));
        return findOne(query, GameTemplate.class);
    }

    public String getActiveGameTemplateId() throws BadRequestException, InternalServerErrorException {
        String id = redisDAO.get(GAME_TEMPLATE_ACTIVE_KEY);
        if (id == null) {
            GameTemplate activeTemplate = getActiveTemplate();
            if(null != activeTemplate && null != activeTemplate.getId())
            redisDAO.set(GAME_TEMPLATE_ACTIVE_KEY, activeTemplate.getId());
            return activeTemplate.getId();
        }
        return id;
    }
}
