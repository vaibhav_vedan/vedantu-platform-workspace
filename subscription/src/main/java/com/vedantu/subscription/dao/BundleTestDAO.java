package com.vedantu.subscription.dao;

import com.vedantu.exception.NotFoundException;
import com.vedantu.exception.VException;
import com.vedantu.subscription.entities.mongo.Bundle;
import com.vedantu.subscription.entities.mongo.BundleTest;
import com.vedantu.subscription.entities.mongo.BundleVideo;
import com.vedantu.subscription.pojo.TestContentData;
import com.vedantu.subscription.viewobject.request.GetBundleTestVideoReq;
import com.vedantu.subscription.viewobject.response.BundleTestFilterRes;
import com.vedantu.subscription.viewobject.response.BundleTestRes;
import com.vedantu.util.LogFactory;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationOperation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import org.springframework.data.mongodb.core.aggregation.AggregationOptions;
import org.bson.Document;

@Service
public class BundleTestDAO extends AbstractMongoDAO {

    @Autowired
    private LogFactory logFactory;

    private final Logger logger = logFactory.getLogger(BundleTest.class);


    @Autowired
    private MongoClientFactory mongoClientFactory;

    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }

    public void save(BundleTest p) {
        // try {
        if (p != null) {
            logger.info("Saving into db" + p);
            saveEntity(p);
        }
    }

    public List<BundleTest> getTest(GetBundleTestVideoReq req) {

        Query query = new Query();
        if (req.getGrade() != null) {
            query.addCriteria(Criteria.where(BundleVideo.Constants.GRADE).is(req.getGrade()));
        }
        if (req.getTitle() != null) {
            int length = req.getTitle().length();
            int index = length < 30 ? length : 30;
            query.addCriteria(Criteria.where(BundleVideo.Constants.CHILDREN_TITLE).regex(req.getTitle().substring(0, index), "i"));

        }
        if (req.getSubject() != null) {
            query.addCriteria(Criteria.where(BundleVideo.Constants.TITLE).is(req.getSubject()));
        }
        return runQuery(query, BundleTest.class);
    }

    public  BundleTestRes getTestByBundle(GetBundleTestVideoReq bundleVideo) throws NotFoundException, VException {

        Query query = new Query();
        List<AggregationOperation> aggregationOperation = new ArrayList<AggregationOperation>();
        if(bundleVideo.getBundleIds() != null && !bundleVideo.getBundleIds().isEmpty() ) {
            aggregationOperation.add(Aggregation.match(Criteria.where(Bundle.Constants._ID).in(bundleVideo.getBundleIds())));
        }
        else {
            aggregationOperation.add(Aggregation.match(Criteria.where(Bundle.Constants._ID).is(bundleVideo.getBundleId())));
        }
        aggregationOperation.add(Aggregation.unwind(Bundle.Constants.TESTS_CHILDREN));

        if (bundleVideo.getGrade() != null) {

            aggregationOperation.add(Aggregation.match(Criteria.where(Bundle.Constants.TEST_CHILDREN_GRADE).is(bundleVideo.getGrade())));

        }
        if ( bundleVideo.getSubject() != null) {
            aggregationOperation.add(Aggregation.match(Criteria.where(Bundle.Constants.TEST_CHILDREN_SUBJECT).is(bundleVideo.getSubject())));

        }

        AggregationOptions aggregationOptions=Aggregation.newAggregationOptions().cursor(new Document()).build();
        Aggregation aggregation = Aggregation.newAggregation(aggregationOperation).withOptions(aggregationOptions);


        logger.info ("\n--------------------------aggregation query---------------------------------\n" + aggregation+"\n");
        AggregationResults<BundleTestFilterRes> results = getMongoOperations().aggregate(aggregation,
                Bundle.class, BundleTestFilterRes.class);
        List<BundleTestFilterRes> bundles = results.getMappedResults();
        TestContentData testContentData = new  TestContentData();
        testContentData.setChildren(new ArrayList< TestContentData>());
        if (bundles != null && !bundles.isEmpty()) {
            return bundles.get(0).getTests();
        } else {
            return null;
        }
    }

    public  List<BundleTestFilterRes> getTestDetailsByBundles(GetBundleTestVideoReq bundleVideo) throws NotFoundException, VException {

        Query query = new Query();
        List<AggregationOperation> aggregationOperation = new ArrayList<>();

        aggregationOperation.add(Aggregation.match(Criteria.where(Bundle.Constants._ID).in(bundleVideo.getBundleIds())));
        aggregationOperation.add(Aggregation.unwind(Bundle.Constants.TESTS_CHILDREN));

        if (bundleVideo.getGrade() != null) {
            aggregationOperation.add(Aggregation.match(Criteria.where(Bundle.Constants.TEST_CHILDREN_GRADE).is(bundleVideo.getGrade())));
        }
        if ( bundleVideo.getSubject() != null) {
            aggregationOperation.add(Aggregation.match(Criteria.where(Bundle.Constants.TEST_CHILDREN_SUBJECT).is(bundleVideo.getSubject())));
        }

        AggregationOptions aggregationOptions=Aggregation.newAggregationOptions().cursor(new Document()).build();
        Aggregation aggregation = Aggregation.newAggregation(aggregationOperation).withOptions(aggregationOptions);


        logger.info ("\n--------------------------aggregation query---------------------------------\n" + aggregation+"\n");
        AggregationResults<BundleTestFilterRes> results = getMongoOperations().aggregate(aggregation,
                Bundle.class, BundleTestFilterRes.class);
        List<BundleTestFilterRes> bundles = results.getMappedResults();
        return bundles;
        /*TestContentData testContentData = new  TestContentData();
        testContentData.setChildren(new ArrayList< TestContentData>());
        if (bundles != null && !bundles.isEmpty()) {
            logger.info("\n\n#################################################################\n\n");
            for(BundleTestFilterRes  bundle:  bundles){
                //BundleTestRes bundle =bundle.getTests()
                //      bundle.
                logger.info("id - "+bundle.getId()+"\n\n");
                testContentData.getChildren().add(bundle.getTests().getChildren());
            }
            logger.info("##################################################################################");
            return testContentData;
        } else {
            return null;
        }*/
    }
}
