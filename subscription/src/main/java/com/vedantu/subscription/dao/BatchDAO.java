package com.vedantu.subscription.dao;

import com.vedantu.User.Role;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.onetofew.enums.CourseTerm;
import com.vedantu.onetofew.pojo.LastUpdatedUsers;
import com.vedantu.session.pojo.VisibilityState;
import com.vedantu.subscription.entities.mongo.Batch;
import com.vedantu.subscription.entities.mongo.Course;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.CollectionUtils;
import com.vedantu.util.DateTimeUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.dbentitybeans.mongo.EntityState;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class BatchDAO extends AbstractMongoDAO {

    @Autowired
    private MongoClientFactory mongoClientFactory;

    @Autowired
    private LogFactory logFactory;

    private Logger logger = logFactory.getLogger(BatchDAO.class);

    public BatchDAO() {
        super();
    }

    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }

    public Batch getById(String id) {
        Batch batch = null;
        try {
            if (!StringUtils.isEmpty(id)) {
                batch = (Batch) getEntityById(id, Batch.class);
            }
        } catch (Exception ex) {
            // log Exception
            batch = null;
        }
        return batch;
    }

    public void save(Batch p) {
        try {
            if (p != null) {
                saveEntity(p);
            }
        } catch (Exception ex) {
            // throw Exception;
            logger.error("Batch save failed for: " + (p.getId() != null ? p.getId() : null) + ", error: " + ex.getMessage());
        }
    }

    public Batch getById(String id, List<String> includeFields) {
        return (Batch) getEntityById(id, Batch.class, includeFields);
    }

    public void updateAll(List<Batch> p) {
        try {
            if (p != null) {
                insertAllEntities(p, Batch.class.getSimpleName());
            }
        } catch (Exception ex) {
            // throw Exception;
            logger.error("Batch updateall failed error: "+ ex.getMessage());
        }
    }

    public int deleteById(String id) {
        int result = 0;
        try {
            result = deleteEntityById(id, Batch.class);
        } catch (Exception ex) {
            // throw Exception;
        }

        return result;
    }

    //TODO session partners for teacher is commented as it can result in populating almost 30k entries on teacher side
    public Set<String> getSessionPartners(String userId, Role role) {
        Set<String> result = new LinkedHashSet<>();
        if (Role.TEACHER.equals(role)) {
            return result;
        }

        Query query = new Query();

        List<Sort.Order> orderList = new ArrayList<>();
        orderList.add(new Sort.Order(Sort.Direction.DESC, Batch.Constants.START_TIME));
        query.with(Sort.by(orderList));
        query.limit(2000);
        if (Role.STUDENT.equals(role)) {
            query.fields().include(Batch.Constants.TEACHER_IDS);
        }

        query.addCriteria(Criteria.where(Batch.Constants.TEACHER_IDS).in(userId));
        List<Batch> batches = runQuery(query, Batch.class);
        int maxResults = 200;
        if (batches != null) {
            for (Batch batch : batches) {
                if (batch != null) {
//                    if (Role.TEACHER.equals(role) && batch.getEnrolledStudents() != null) {
//                        result.addAll(batch.getEnrolledStudents());
//                    }
                    if (Role.STUDENT.equals(role) && batch.getTeacherIds() != null) {
                        result.addAll(batch.getTeacherIds());
                    }
                }
                if (result.size() > (maxResults + 1)) {// +1 for callinguserid
                    break;
                }
            }
        }
        result.remove(userId);
        return result;
    }

    public Set<String> getLastUpdatedBatches(long startTime, long endTime) {
        Query query = new Query();
        query.addCriteria(Criteria.where(Batch.Constants.LAST_UPDATED).gte(startTime).lte(endTime));
        query.fields().include(Batch.Constants.ID);

        List<Batch> batches = runQuery(query, Batch.class);
        Set<String> batchIds = new HashSet<>();
        if (!CollectionUtils.isEmpty(batches)) {
            batches.forEach(b -> batchIds.add(b.getId()));
        }
        return batchIds;
    }

    public List<Batch> getBatchByIds(List<String> batchIds) {
        Query query = new Query();
        query.addCriteria(Criteria.where(Batch.Constants._ID).in(batchIds));
        return runQuery(query, Batch.class);
    }

    public List<Batch> getBatchByGroupName(String groupName, Integer start, Integer limit) {
        Query query = new Query();
        List<Batch> batchList = new ArrayList<>();
        if (groupName != null) {
            query.addCriteria(Criteria.where(Batch.Constants.GROUP_NAME).is(groupName));
            query.addCriteria(Criteria.where(Batch.Constants.ENTITY_STATE).is(EntityState.ACTIVE));            
            if (start != null && limit != null) {
                setFetchParameters(query, start, limit);
            }
            batchList = runQuery(query, Batch.class);
        } else {
            logger.error("groupName not provided, getBatchByGroupName ");
        }
        return batchList;
    }    
    
    public List<Batch> getBatchByGroupNames(List<String> groupNames, Integer start, Integer limit) {
        Query query = new Query();
        List<Batch> batchList = new ArrayList<>();
        if (ArrayUtils.isNotEmpty(groupNames)) {
            query.addCriteria(Criteria.where(Batch.Constants.GROUP_NAME).in(groupNames));
            query.addCriteria(Criteria.where(Batch.Constants.ENTITY_STATE).is(EntityState.ACTIVE));
            if (start != null && limit != null) {
                setFetchParameters(query, start, limit);
            }
            batchList = runQuery(query, Batch.class);
        } else {
            logger.error("groupName not provided, getBatchByGroupName ");
        }
        return batchList;
    }
    
    public List<Batch> getBatchByGroupNameWithDifferentAMId(String groupName, String amId) {
        Query query = new Query();
        List<Batch> batchList = new ArrayList<>();
        if (groupName != null) {
            query.addCriteria(Criteria.where(Batch.Constants.GROUP_NAME).is(groupName));
            query.addCriteria(Criteria.where(Batch.Constants.AM_ID).exists(true).ne(amId));
            batchList = runQuery(query, Batch.class);
        } else {
            logger.error("groupName not provided, getBatchByGroupName ");
        }
        return batchList;
    }    
    
    public List<Batch> getBatchByGroupNameAndAMId(String amId, String groupName, Integer start, Integer limit) {
        Query query = new Query();
        List<Batch> batchList = new ArrayList<>();
        if (groupName != null) {
            query.addCriteria(Criteria.where(Batch.Constants.GROUP_NAME).is(groupName));
            if(amId != null){
                query.addCriteria(Criteria.where(Batch.Constants.AM_ID).is(amId));
            }
            if (start != null && limit != null) {
                setFetchParameters(query, start, limit);
            }
            batchList = runQuery(query, Batch.class);
        } else {
            logger.error("groupName not provided, getBatchByGroupName ");
        }
        return batchList;
    }    
    
    public List<Batch>  getActiveBatches(){
        Query query = new Query();
        query.addCriteria(Criteria.where(Batch.Constants.ENTITY_STATE).is(EntityState.ACTIVE));
        return runQuery(query, Batch.class);
    }
    
    public List<Batch> getEndedBatches(Long time){
        Query query = new Query();
        Long beforeTime = time - 2*DateTimeUtils.MILLIS_PER_DAY;
        query.addCriteria(Criteria.where(Batch.Constants.END_TIME).lte(time).gte(beforeTime));
        
        return runQuery(query, Batch.class);
        
    }
    
    public List<Batch> getContentOnlyBatch(){
        Query query = new Query();
        Long currentTime = System.currentTimeMillis();
        query.addCriteria(Criteria.where("contentBatch").is(true));
        query.addCriteria(Criteria.where(Batch.Constants.ENTITY_STATE).is(EntityState.ACTIVE));
        query.addCriteria(Criteria.where(Batch.Constants.END_TIME).gte(currentTime));
        return runQuery(query, Batch.class);
    }

    public List<Batch> getBatchesCreatedBetween(Long startTime, Long endTime){
        Query query = new Query();
        query.addCriteria(Criteria.where(Batch.Constants.CREATION_TIME).gte(startTime).lt(endTime));
        query.addCriteria(Criteria.where(Batch.Constants.ENTITY_STATE).is(EntityState.ACTIVE));
        return runQuery(query, Batch.class);
    }
    
    public List<Batch> getBatches(List<String> ids, List<String> includeFields) {

        Query query = new Query();
        if (ArrayUtils.isNotEmpty(ids)) {
            query.addCriteria(Criteria.where(Course.Constants.ID).in(ids));
        }
        if (ArrayUtils.isNotEmpty(includeFields)) {
            includeFields.forEach((fieldName) -> {
                query.fields().include(fieldName);
            });
        }
        return runQuery(query, Batch.class);
    }

    public List<Batch> getModularBatch(int start, int size){
        Query query = new Query();
        query.addCriteria(Criteria.where(Batch.Constants.MODULAR_BATCH).is(true));
        query.addCriteria(Criteria.where(Batch.Constants.ENTITY_STATE).is(EntityState.ACTIVE));
        
        setFetchParameters(query, start, size);
        return runQuery(query, Batch.class);
        
    }
    
    public List<Batch> getBatchForAM(String amId){
        Query query = new Query();
        query.addCriteria(Criteria.where(Batch.Constants.AM_ID).is(amId));
        query.addCriteria(Criteria.where(Batch.Constants.ENTITY_STATE).is(EntityState.ACTIVE));
        
        return runQuery(query, Batch.class);
    }

    public List<Batch> getBatchByIds(List<String> batchIds,List<String> includeFields,List<String> excludeFields){

        Query query = new Query();
        query.addCriteria(Criteria.where(Batch.Constants.ID).in(batchIds));

        if(ArrayUtils.isNotEmpty(includeFields)){
            includeFields.forEach(field->query.fields().include(field));
        }

        if(ArrayUtils.isNotEmpty(excludeFields)){
            excludeFields.forEach(field->query.fields().exclude(field));
        }

        logger.info("getBatchByIds - query"+query);
        return runQuery(query, Batch.class);
    }

    public List<Batch> getBatchesForSubscription(String courseId,Boolean isVassist, Integer start, Integer size) {
        Query query = new Query();
        query.addCriteria( Criteria.where( Batch.Constants.COURSE_ID ).is( courseId ) );
        query.addCriteria( Criteria.where( Batch.Constants.VISIBILITY_STATE ).is( VisibilityState.VISIBLE ) );
        if(isVassist != null && isVassist ){
            query.addCriteria( Criteria.where( Batch.Constants.SEARCH_TERMS ).nin(Arrays.asList( CourseTerm.LONG_TERM,CourseTerm.SHORT_TERM)) );
    //        query.addCriteria( Criteria.where( Batch.Constants.SEARCH_TERMS ).ne(CourseTerm.LONG_TERM) );

        }
        query.fields().include( Batch.Constants.RECORDED_VIDEO );
        query.fields().include( Batch.Constants.START_TIME );
        query.fields().include( Batch.Constants.END_TIME );
        query.fields().include( Batch.Constants.SEARCH_TERMS );
        query.fields().include( Batch.Constants.START_TIME );

        start = (start != null) ? start : 0;
        size = (size != null) ? size : 20;
        if(MAX_ALLOWED_FETCH_SIZE < size) {
            logger.error( "getCourseForSubscription crossed MAX_ALLOWED_FETCH_SIZE "+ size );
        }
        query.skip(start);
        query.limit(size);
        logger.info( "getBatchesForSubscription query {}",query );
        return runQuery(query, Batch.class);
    }

    public Batch getBatchWithExcludedFields(String id, List<String> excludedFields) {
        Query query=new Query();
        query.addCriteria(Criteria.where(Batch.Constants._ID).is(id));

        if(ArrayUtils.isNotEmpty(excludedFields)){
            excludedFields.forEach(query.fields()::exclude);
        }

        return findOne(query,Batch.class);
    }

    public List<String> getDistinctCourseIdsFromBatchIds(List<String> batchIds) throws BadRequestException {

        if(ArrayUtils.isEmpty(batchIds)){
            throw new BadRequestException(ErrorCode.INVALID_PARAMETER,"batchIds can'be be null or empty");
        }

        if(batchIds.size()>3000){
            throw new BadRequestException(ErrorCode.LIST_SIZE_EXCEEDED,"Please keep the list size within 3000 for best result in method `getDistinctCourseIdsFromBatchIds`", Level.ERROR);
        }

        Query query=new Query();
        query.addCriteria(Criteria.where(Batch.Constants._ID).in(batchIds));

        query.fields().include(Batch.Constants.COURSE_ID);

        query.fields().exclude(Batch.Constants._ID);

        return Optional.ofNullable(runQuery(query,Batch.class))
                        .map(Collection::stream)
                        .orElseGet(Stream::empty)
                        .map(Batch::getCourseId)
                        .distinct()
                        .collect(Collectors.toList());
    }

    public List<String> getAllDistinctCourseIdsFromBatchIds(List<String> ids) {

        if(ArrayUtils.isEmpty(ids)){
            return new ArrayList<>();
        }

        Query query=new Query();
        query.addCriteria(Criteria.where(Batch.Constants._ID).in(ids));
        query.fields().include(Batch.Constants.COURSE_ID);
        query.fields().exclude(Batch.Constants.ID);

        return Optional.ofNullable(runQuery(query,Batch.class))
                .map(Collection::stream)
                .orElseGet(Stream::empty)
                .filter(Objects::nonNull)
                .map(Batch::getCourseId)
                .distinct()
                .collect(Collectors.toList());
    }

    public List<String> getAllDistinctOrgIdsFromBatchIds(List<String> batchIds) {
        if(ArrayUtils.isEmpty(batchIds)){
            return new ArrayList<>();
        }
        Query query=new Query();
        query.addCriteria(Criteria.where(Batch.Constants._ID).in(batchIds));
        query.fields().include(Batch.Constants.ORG_ID);
        query.fields().exclude(Batch.Constants._ID);
        logger.info("getAllDistinctOrgIdsFromBatchIds - query - {}",query);
        return Optional.ofNullable(runQuery(query,Batch.class))
                        .map(Collection::stream)
                        .orElseGet(Stream::empty)
                        .map(Batch::getOrgId)
                        .distinct()
                        .peek(logger::info)
                        .collect(Collectors.toList());
    }

    public List<Batch> getCourseIdsForBatchIds(List<String> ids) {
        if(ArrayUtils.isEmpty(ids) && ids.size()>1000){
            logger.error("It is not recommended to give 1000 batches for it, need to rectify the same.");
        }
        if(ArrayUtils.isEmpty(ids)){
            return new ArrayList<>();
        }
        Query query=new Query();
        query.addCriteria(Criteria.where(Batch.Constants._ID).in(ids));
        query.addCriteria(Criteria.where(Batch.Constants.ENTITY_STATE).is(EntityState.ACTIVE));

        query.fields().include(Batch.Constants.COURSE_ID);

       return runQuery(query,Batch.class);
    }

    public Long getSectionEnabledBatchesCount(List<String> batchIds) {
        Query query = new Query();
        query.addCriteria(Criteria.where(Batch.Constants._ID).in(batchIds));
        query.addCriteria(Criteria.where(Batch.Constants.HAS_SECTIONS).is(true));
        return queryCount(query,Batch.class);
    }
}
