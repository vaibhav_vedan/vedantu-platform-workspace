package com.vedantu.subscription.dao;

import com.vedantu.session.pojo.VisibilityState;
import java.util.List;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import com.vedantu.subscription.entities.mongo.BundlePackage;
import com.vedantu.subscription.pojo.EntityInfo;
import com.vedantu.subscription.request.GetBundlePackagesReq;
import com.vedantu.util.CollectionUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import org.springframework.util.StringUtils;

/**
 * Created by somil on 10/05/17.
 */
@Service
public class BundlePackageDAO extends AbstractMongoDAO {

    @Autowired
    private LogFactory logFactory;

    private final Logger logger = logFactory.getLogger(BundlePackageDAO.class);

    @Autowired
    private MongoClientFactory mongoClientFactory;

    public BundlePackageDAO() {
        super();
    }

    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }

    public void save(BundlePackage p, Long callingUserId) {
        // try {
        if (p != null) {
            String callingUserIdString = null;
            if (callingUserId != null) {
                callingUserIdString = callingUserId.toString();
            }
            logger.info("Saving into db" + p);
            saveEntity(p, callingUserIdString);
        }
    }

    public BundlePackage getBundlePackageById(String id) {
        return getEntityById(id, BundlePackage.class);
    }

    public List<BundlePackage> getBundlePackages(GetBundlePackagesReq req) {
        Query query = new Query();
        setFetchParameters(query, req);
        if (!CollectionUtils.isEmpty(req.getBundleIds())) {
            query.addCriteria(Criteria.where(BundlePackage.Constants.ENTITIES)
                    .elemMatch(Criteria.where(EntityInfo.Constants.ENTITY_ID).in(req.getBundleIds())));
        }
        if (!CollectionUtils.isEmpty(req.getTags())) {
            query.addCriteria(Criteria.where(BundlePackage.Constants.TAGS).in(req.getTags()));
        }
        query.with(Sort.by(Sort.Direction.DESC, AbstractMongoStringIdEntity.Constants.LAST_UPDATED));
        return runQuery(query, BundlePackage.class);
    }

    public List<BundlePackage> getBundlePackagesForEntity(GetBundlePackagesReq req) {
        Query query = new Query();
        setFetchParameters(query, req);
        if (!CollectionUtils.isEmpty(req.getEntityId())) {
            query.addCriteria(Criteria.where(BundlePackage.Constants.ENTITIES)
                    .elemMatch(Criteria.where(EntityInfo.Constants.ENTITY_ID).in(req.getEntityId())));
        }

        query.with(Sort.by(Sort.Direction.DESC, AbstractMongoStringIdEntity.Constants.LAST_UPDATED));
        return runQuery(query, BundlePackage.class);
    }

    public List<BundlePackage> getFutureBundlePackages(String board, String grade, int start, int size) {
        Long currentTime = System.currentTimeMillis();
        Query query = new Query();
        Integer _grade = null;
        try {
            _grade = Integer.parseInt(grade);
        } catch (Exception e) {
            //swallow
        }
        query.addCriteria(Criteria.where(BundlePackage.Constants.GRADE).is(_grade));
        if (!StringUtils.isEmpty(board)) {
            //TODO why comparing target with board
            query.addCriteria(Criteria.where(BundlePackage.Constants.TARGETS).is(board));
        }
        query.addCriteria(Criteria.where(BundlePackage.Constants.VISIBILITY_STATE).is(VisibilityState.VISIBLE));
        query.addCriteria(Criteria.where(BundlePackage.Constants.START_TIME).gte(currentTime));
        setFetchParameters(query, start, size);
        return runQuery(query, BundlePackage.class);
    }

    public List<BundlePackage> getPastBundlePackages(String board, String grade, int start, int size) {
        Long currentTime = System.currentTimeMillis();
        Integer _grade = null;
        try {
            _grade = Integer.parseInt(grade);
        } catch (Exception e) {
            //swallow
        }
        Query query = new Query();
        query.addCriteria(Criteria.where(BundlePackage.Constants.GRADE).is(_grade));
        if (!StringUtils.isEmpty(board)) {
            //TODO why comparing target with board
            query.addCriteria(Criteria.where(BundlePackage.Constants.TARGETS).is(board));
        }
        query.addCriteria(Criteria.where(BundlePackage.Constants.VISIBILITY_STATE).is(VisibilityState.VISIBLE));
        query.addCriteria(Criteria.where(BundlePackage.Constants.START_TIME).lte(currentTime));
        setFetchParameters(query, start, size);
        return runQuery(query, BundlePackage.class);
    }

}
