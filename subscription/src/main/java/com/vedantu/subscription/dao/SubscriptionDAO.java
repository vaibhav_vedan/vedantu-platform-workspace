package com.vedantu.subscription.dao;

import java.util.List;

import com.vedantu.dbs.sessionfactory.SqlSessionFactory;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vedantu.subscription.entities.sql.Subscription;
import com.vedantu.util.LogFactory;
import java.util.ArrayList;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

@Service
public class SubscriptionDAO extends com.vedantu.util.dbutils.AbstractSqlDAO {

	@Autowired
	private LogFactory logFactory;

	@SuppressWarnings("static-access")
	private Logger logger = logFactory.getLogger(SubscriptionDAO.class);

	private static final int IN_LIMIT = 200;

	public SubscriptionDAO() {
		super();
	}




	@Autowired
	SqlSessionFactory sqlSessionFactory;


	@Override
	protected SessionFactory getSessionFactory() {
		return sqlSessionFactory.getSessionFactory();
	}


	public Subscription addOrUpdateSubscription(Subscription subscription, Session session) {
		return addOrUpdateSubscription(subscription, session, null);
	}


	public Subscription addOrUpdateSubscription(Subscription subscription, Session session, Long callingUserId) {

		Long id = subscription.getId();
		String callingUserIdString = null;
		if(callingUserId!=null)
			callingUserIdString = callingUserId.toString();

		if (id == null) {
			create(subscription, session, callingUserIdString);
			return subscription;
		} else {
			if (subscription != null) {
				update(subscription, session, callingUserIdString);
			}
		}
		return subscription;
	}

	private void create(Subscription subscription, Session session, String callingUserId) {
		try {
			if (subscription != null) {
				logger.info("create subscription: " + subscription.toString());
				saveEntity(subscription, session, callingUserId);
			}
		} catch (Exception ex) {
			logger.error("Create Subscription: " + ex.getMessage());
		}
	}

	public Subscription getById(Long id, Session session) {
		Subscription subscription = null;
		try {
			if (id != null) {
				subscription = (Subscription) getEntityById(id, null, Subscription.class, session);
			}
		} catch (Exception ex) {
			logger.error("getById Subscription: " + ex.getMessage());
			subscription = null;
		}
		return subscription;
	}

	public List<Subscription> getByIds(List<Long> ids, Session session) {
		List<Subscription> subscriptions = new ArrayList<>();
		int listSize = ids.size();

		for (int i = 0; i < listSize; i += IN_LIMIT) {
			List subList;
			if (listSize > i + IN_LIMIT) {
				subList = ids.subList(i, (i + IN_LIMIT));
			} else {
				subList = ids.subList(i, listSize);
			}
			Criteria cr = session.createCriteria(Subscription.class);
			cr.add(Restrictions.in("id", subList));
			subscriptions.addAll(runQuery(session, cr, Subscription.class));
		}

		return subscriptions;
	}

	public Subscription getById(Long id, Boolean enabled, Session session) {
		Subscription subscription = null;
		try {
			if (id != null) {
				subscription = (Subscription) getEntityById(id, enabled, Subscription.class, session);
			}
		} catch (Exception ex) {
			logger.error("getById Subscription: " + ex.getMessage());
			subscription = null;
		}
		return subscription;
	}

	private void update(Subscription p, Session session, String callingUserId) {
		try {
			if (p != null) {
				logger.info("update subscription: " + p.toString());
				updateEntity(p, session, callingUserId);
			}
		} catch (Exception ex) {
			logger.error("Update Subscription: " + ex.getMessage());
		}
	}

	public void updateAll(List<Subscription> p, Session session) {
		updateAll(p, session, null);
	}


	public void updateAll(List<Subscription> p, Session session, Long callingUserId) {
		try {
			String callingUserIdString = null;
			if(callingUserId!=null)
				callingUserIdString = callingUserId.toString();
			if (p != null) {
				saveAllEntities(p, session, callingUserIdString);
			}
		} catch (Exception ex) {
			logger.error("updateAll Subscription: " + ex.getMessage());
		}
	}


}
