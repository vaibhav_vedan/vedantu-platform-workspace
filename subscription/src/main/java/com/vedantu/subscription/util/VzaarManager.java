package com.vedantu.subscription.util;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.vedantu.exception.*;
import com.vedantu.subscription.pojo.VideoDetails;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.WebUtils;
import org.apache.logging.log4j.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


/**
 * Created by somil on 09/05/17.
 */
@Service
public class VzaarManager {

    private static final Gson gson = new Gson();

    private static final String BASE_URL = "https://api.vzaar.com/api/v2/videos/";

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(VzaarManager.class);

    private String clientId = ConfigUtils.INSTANCE.getStringValue("vzaar.clientId");
    private String authToken = ConfigUtils.INSTANCE.getStringValue("vzaar.authToken");


    public VideoDetails getVideoDetails(Long id) throws VException {
        Client client = WebUtils.INSTANCE.getClient();
        String serverUrl = BASE_URL + id;
        logger.info("serverUrl: "+serverUrl);
        WebResource.Builder webResource = client.resource(serverUrl).getRequestBuilder();

        logger.info("clientId: "+clientId+ ", X-Auth-Token"+authToken);
        webResource = webResource.header("X-Client-Id", clientId).header("X-Auth-Token", authToken);
        ClientResponse resp = webResource.get(ClientResponse.class);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        logger.info("Response : "+ jsonString);
        JsonParser parser = new JsonParser();
        JsonObject object = parser.parse(jsonString).getAsJsonObject();
        String data = object.getAsJsonObject("data").toString();
        logger.info("data: "+data);
        VideoDetails videoDetails = new Gson().fromJson(data, VideoDetails.class);
        return videoDetails;
    }



}
