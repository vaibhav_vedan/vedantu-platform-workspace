package com.vedantu.subscription.util;

import com.vedantu.util.pojo.Pair;

import java.util.Comparator;

public class The_Comparator implements Comparator<Pair<Long,String>> {
    @Override
    public int compare(Pair<Long, String> p1, Pair<Long, String> p2) {
        long s1 = p1.getKey();
        long s2 = p2.getKey();

        if(s1 < s2)
            return 1;
        else if(s1 > s2)
                return -1;
        return 0;
    }
}
