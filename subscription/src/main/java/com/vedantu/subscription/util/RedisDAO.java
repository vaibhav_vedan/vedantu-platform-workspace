package com.vedantu.subscription.util;

import com.google.common.collect.Lists;
import com.google.gson.Gson;
import com.vedantu.exception.InternalServerErrorException;
import com.vedantu.onetofew.pojo.BatchBasicInfo;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import org.apache.commons.collections.CollectionUtils;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;

import com.vedantu.util.redis.AbstractRedisDAO;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class RedisDAO extends AbstractRedisDAO{

    private Logger logger = LogFactory.getLogger(RedisDAO.class);
    private static final int MAX_FETCH_KEYS_PAGE_SIZE = 400;
    public static String env = ConfigUtils.INSTANCE.getStringValue("environment");

    public static String SCRIPT_SEATS_AVAILABLE_FOR_BATCH = ""
            + "local exists = redis.call(\"get\", KEYS[1]) "
            + "if (exists == false) then "
            + "  return nil "
            + "else local result = redis.call(\"decr\", KEYS[1]) "
            + "return result "
            + "end";

    private Gson gson = new Gson();

    public RedisDAO() {
    }

    public Map<String, BatchBasicInfo> getCachedBatchBasicInfo(List<String> ids) {

        Map<String, BatchBasicInfo> batchMap = new HashMap<>();
        if (CollectionUtils.isEmpty(ids)) {
            return batchMap;
        }

        // gather keys for given batchIds
        final List<String> keys = ids.stream()
                .filter(StringUtils::isNotEmpty)
                .map(this::getBatchRedisKey)
                .distinct()
                .collect(Collectors.toList());

        // fetch in partitions of size 250 each - mget
        List<List<String>> keyPartitions = Lists.partition(keys, MAX_FETCH_KEYS_PAGE_SIZE);
        Map<String, String> cachedBatchBasicInfo = new HashMap<>();
        for (List<String> part : keyPartitions) {
            try {
                Map<String, String> fetchedPart = getValuesForKeys(part);
                cachedBatchBasicInfo.putAll(fetchedPart);
            } catch (InternalServerErrorException e) {
                logger.warn("Error while fetching cached batch basic info : " + e.getMessage());
            }
        }

        if (cachedBatchBasicInfo.isEmpty()) {
            return batchMap;
        }

        ids.stream().filter(StringUtils::isNotEmpty).forEach(batchId -> {
            String batchInfoStr = cachedBatchBasicInfo.get(getBatchRedisKey(batchId));
            if (StringUtils.isEmpty(batchInfoStr)) {
                return;
            }
            batchMap.putIfAbsent(batchId, gson.fromJson(batchInfoStr, BatchBasicInfo.class));
        });

        return batchMap;
    }

    public String getBatchRedisKey(String batchId) {
        return "BATCH_BASIC_INFO_" + batchId;
    }

    public String getCourseRedisKey(String courseId) {
        return "COURSE_BASIC_INFO_" + courseId;
    }

}
