#!/bin/bash
PLATFORM_PATH=/home/vedantu/vedantu-platform-workspace
TOMCAT_PATH=/opt/tomcat/apache-tomcat-9.0.24/webapps/

# building utils
cd $PLATFORM_PATH/utils;
mvn clean install && cd ..

# #building platform
#(cd $PLATFORM_PATH/platform
#mvn clean package -P local
#sudo cp target/*.war $TOMCAT_PATH)

# #building user
#(cd $PLATFORM_PATH/user
#mvn clean package -P local
#sudo cp target/*.war $TOMCAT_PATH)

##building subscription
#(cd $PLATFORM_PATH/subscription
#mvn clean package -P local
#sudo cp target/*.war $TOMCAT_PATH)
#
##building scheduling
#(cd $PLATFORM_PATH/scheduling
#mvn clean package -P local
#sudo cp target/*.war $TOMCAT_PATH)
#
## building dinero
#(cd $PLATFORM_PATH/dinero
#mvn clean package -P local
#sudo cp target/*.war $TOMCAT_PATH)

#building lms
#(cd $PLATFORM_PATH/lms
#mvn clean package -P local
#sudo cp target/*.war $TOMCAT_PATH)
#
#building notification-centre
(cd $PLATFORM_PATH/notification-centre
mvn clean package -P local
sudo cp target/*.war $TOMCAT_PATH)

##building vedantudata
#(cd $PLATFORM_PATH/vedantudata
#mvn clean package -P local
#sudo cp target/*.war $TOMCAT_PATH)
#
##building notification-centre
#(cd $PLATFORM_PATH/notification-centre
#mvn clean package -P local
#sudo cp target/*.war $TOMCAT_PATH)
