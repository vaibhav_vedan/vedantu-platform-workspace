package com.vedantu.vedantudata.dao;

import com.vedantu.util.LogFactory;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import com.vedantu.vedantudata.dao.serializers.MongoClientFactory;
import com.vedantu.vedantudata.entities.salesconversion.UserSessionDetails;
import io.jsonwebtoken.lang.Assert;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ScVglanceDao extends AbstractMongoDAO {

    @Autowired
    private MongoClientFactory mongoClientFactory;

    @Autowired
    private LogFactory logFactory;

    private final Logger logger = logFactory.getLogger(ScVglanceDao.class);

    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }


    public void addUserSessionDetails(UserSessionDetails userSessionDetails) {
        try {
            Assert.notNull(userSessionDetails);
            saveEntity(userSessionDetails);
        } catch (Exception ex) {
            throw new RuntimeException("UserSessionDetailsError : Error while add/update operations.. " + userSessionDetails.toString(), ex);
        }
    }

    public UserSessionDetails getUserSessionDetailsByUserDate(Long userId, long time) {
        Query query = new Query();
        Criteria criteria = new Criteria();
        criteria.andOperator(Criteria.where(UserSessionDetails.Constants.USER_ID).is(userId), Criteria.where(UserSessionDetails.Constants.DATE).is(time));
        query.addCriteria(criteria);
        logger.info("getUserSessionDetailsByUserDate query: {}", query);
        return findOne(query, UserSessionDetails.class);
    }

    private void manageFieldsInclude(Query q, List<String> keySet, boolean isInclude) {
        if (keySet != null && !keySet.isEmpty()) {
            if (isInclude)
                keySet.parallelStream().forEach(includeKey -> q.fields().include(includeKey));
            else
                keySet.parallelStream().forEach(includeKey -> q.fields().exclude(includeKey));
        }
    }

    public List<UserSessionDetails> getUserSessionDetailsByDate(Long userId, Long startTime, List<String> requiredFields, boolean isInclude) {
        Query query = new Query();
        query.addCriteria(Criteria.where(UserSessionDetails.Constants.USER_ID).is(userId));
        Criteria criteria = new Criteria();
        criteria.andOperator(Criteria.where(UserSessionDetails.Constants.DATE).gte(startTime), Criteria.where(UserSessionDetails.Constants.DATE).lt(System.currentTimeMillis()));
        query.addCriteria(criteria);
        manageFieldsInclude(query, requiredFields, isInclude);
        logger.info("getUserSessionDetailsByDate query: {}", query);
        return runQuery(query, UserSessionDetails.class);
    }
}
