package com.vedantu.vedantudata.dao;

import com.vedantu.util.LogFactory;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import com.vedantu.vedantudata.dao.serializers.MongoClientFactory;
import com.vedantu.vedantudata.entities.SlashRTCActivityDetails;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.util.List;

@Service
public class SlashRTCDialerDao extends AbstractMongoDAO {

    @Autowired
    private MongoClientFactory mongoClientFactory;

    @Autowired
    private LogFactory logFactory;

    private final Logger logger = logFactory.getLogger(SlashRTCDialerDao.class);

    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }

    public void creatActivity(SlashRTCActivityDetails slashRTCActivityDetails) {
        try {
            Assert.notNull(slashRTCActivityDetails);
            saveEntity(slashRTCActivityDetails);
        } catch (Exception ex) {
            throw new RuntimeException(
                    "SlashRTCActivityDetailsCreate : Error adding the slashRTC activity data " + slashRTCActivityDetails.toString(), ex);
        }
    }

    public void updateActivity(SlashRTCActivityDetails SlashRTCActivityDetails) {

        saveEntity(SlashRTCActivityDetails);
    }


    public SlashRTCActivityDetails getSlashRTCActivityById(String callSessionId) {
        Query q = new Query();
        q.addCriteria(Criteria.where(SlashRTCActivityDetails.Constants.CDRID).is(callSessionId));

        logger.info("Logger info: " + q);
        List<SlashRTCActivityDetails> results = runQuery(q, SlashRTCActivityDetails.class);
        if (results != null && !results.isEmpty()) {
            return results.get(0);
        }
        return null;

    }

    public List<SlashRTCActivityDetails> getSlashRTCActivityDetails(Long fromTime, Long thruTime) {
        Query q = new Query();

        Criteria andCriteria = new Criteria().andOperator(Criteria.where(SlashRTCActivityDetails.Constants.START_TIME).gte(fromTime),
                Criteria.where(SlashRTCActivityDetails.Constants.START_TIME).lte(thruTime));
        q.addCriteria(andCriteria);
        q.with(Sort.by(Sort.Direction.DESC, "creationTime"));

        logger.info("Logger info: " + q);
        return runQuery(q, SlashRTCActivityDetails.class, SlashRTCActivityDetails.class.getName());

    }


    public void creatActivities(List<SlashRTCActivityDetails> SlashRTCActivityDetailsList) {

        if (SlashRTCActivityDetailsList.isEmpty())
            return;

        insertAllEntities(SlashRTCActivityDetailsList, SlashRTCActivityDetails.class.getSimpleName());
    }


    public void updateEntities(List<SlashRTCActivityDetails> SlashRTCActivityDetailsList) {
        if (SlashRTCActivityDetailsList == null) {
            return;
        }

        for (SlashRTCActivityDetails SlashRTCActivityDetails : SlashRTCActivityDetailsList) {
            saveEntity(SlashRTCActivityDetails);
        }
    }


}
