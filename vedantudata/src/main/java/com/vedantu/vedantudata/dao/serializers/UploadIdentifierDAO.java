package com.vedantu.vedantudata.dao.serializers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.vedantu.util.dbutils.AbstractMongoDAO;
import com.vedantu.vedantudata.entities.UploadIdentifier;

@Service
public class UploadIdentifierDAO extends AbstractMongoDAO {

	@Autowired
	private MongoClientFactory mongoClientFactory;

	public UploadIdentifierDAO() {
		super();
	}

	@Override
	protected MongoOperations getMongoOperations() {
		return mongoClientFactory.getMongoOperations();
	}

	public void create(UploadIdentifier p) {
		if (p != null) {
			saveEntity(p);
		}
	}

	public UploadIdentifier getById(String id) {
		UploadIdentifier p = null;
		try {
			if (!StringUtils.isEmpty(id)) {
				p = (UploadIdentifier) getEntityById(id, UploadIdentifier.class);
			}
		} catch (Exception ex) {
			// log Exception
			p = null;
		}
		return p;
	}

	public void save(UploadIdentifier p) throws Exception {
		if (p != null) {
			saveEntity(p);
		}
	}

	public int deleteById(String id) {
		int result = 0;
		try {
			result = deleteEntityById(id, UploadIdentifier.class);
		} catch (Exception ex) {
			// throw Exception;
		}

		return result;
	}

}
