package com.vedantu.vedantudata.dao.serializers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.vedantu.util.dbutils.AbstractMongoDAO;
import com.vedantu.vedantudata.entities.UserIdData;

@Service
public class UserIdDAO extends AbstractMongoDAO {

	@Autowired
	private MongoClientFactory mongoClientFactory;

	public UserIdDAO() {
		super();
	}

	@Override
	protected MongoOperations getMongoOperations() {
		return mongoClientFactory.getMongoOperations();
	}

	public void create(UserIdData p) throws Exception {
		if (p != null) {
			saveEntity(p);
		}
	}

	public UserIdData getById(String id) {
		UserIdData p = null;
		try {
			if (!StringUtils.isEmpty(id)) {
				p = (UserIdData) getEntityById(id, UserIdData.class);
			}
		} catch (Exception ex) {
			// log Exception
			p = null;
		}
		return p;
	}

	public void save(UserIdData p) throws Exception {
		if (p != null) {
			saveEntity(p);
		}
	}

	public int deleteById(String id) {
		int result = 0;
		try {
			result = deleteEntityById(id, UserIdData.class);
		} catch (Exception ex) {
			// throw Exception;
		}

		return result;
	}

}
