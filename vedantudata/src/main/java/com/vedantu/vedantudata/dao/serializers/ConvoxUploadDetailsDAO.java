package com.vedantu.vedantudata.dao.serializers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.vedantu.util.dbutils.AbstractMongoDAO;
import com.vedantu.vedantudata.entities.ConvoxUploadDetails;

@Service
public class ConvoxUploadDetailsDAO extends AbstractMongoDAO {

	@Autowired
	private MongoClientFactory mongoClientFactory;

	public ConvoxUploadDetailsDAO() {
		super();
	}

	@Override
	protected MongoOperations getMongoOperations() {
		return mongoClientFactory.getMongoOperations();
	}

	public void create(ConvoxUploadDetails p) {
		if (p != null) {
			saveEntity(p);
		}
	}

	public ConvoxUploadDetails getById(String id) {
		ConvoxUploadDetails p = null;
		try {
			if (!StringUtils.isEmpty(id)) {
				p = (ConvoxUploadDetails) getEntityById(id, ConvoxUploadDetails.class);
			}
		} catch (Exception ex) {
			// log Exception
			p = null;
		}
		return p;
	}

	public void save(ConvoxUploadDetails p) throws Exception {
		if (p != null) {
			saveEntity(p);
		}
	}

	public int deleteById(String id) {
		int result = 0;
		try {
			result = deleteEntityById(id, ConvoxUploadDetails.class);
		} catch (Exception ex) {
			// throw Exception;
		}

		return result;
	}

}
