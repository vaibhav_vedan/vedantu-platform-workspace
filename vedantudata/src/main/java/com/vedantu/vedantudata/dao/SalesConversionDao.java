package com.vedantu.vedantudata.dao;

import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.NotFoundException;
import com.vedantu.util.*;
import com.vedantu.util.dbentitybeans.mongo.EntityState;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import com.vedantu.util.security.HttpSessionData;
import com.vedantu.util.security.HttpSessionUtils;
import com.vedantu.vedantudata.dao.serializers.MongoClientFactory;
import com.vedantu.vedantudata.entities.salesconversion.*;
import com.vedantu.vedantudata.enums.salesconversion.AgentDesignation;
import com.vedantu.vedantudata.enums.salesconversion.AgentDesignationType;
import com.vedantu.vedantudata.enums.salesconversion.SalesTeam;
import com.vedantu.vedantudata.pojos.request.*;
import com.vedantu.vedantudata.pojos.response.FOSCentreFilterRes;
import com.vedantu.vedantudata.pojos.response.SalesAgentMetadataRes;
import com.vedantu.vedantudata.utils.CommonUtils;
import io.jsonwebtoken.lang.Assert;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.text.ParseException;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class SalesConversionDao extends AbstractMongoDAO {

    @Autowired
    private MongoClientFactory mongoClientFactory;

    @Autowired
    private LogFactory logFactory;

    @Autowired
    private CustomValidator validator;

    @Autowired
    private HttpSessionUtils sessionUtils;

    private final Logger logger = logFactory.getLogger(SalesConversionDao.class);


    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }

    public void addFOSTeamMandays(List<FOSMandaysInfo> fosMandaysInfoList, long loggedInUserId) {
        try {
            insertAllEntities(fosMandaysInfoList, FOSMandaysInfo.class.getSimpleName(), String.valueOf(loggedInUserId));
        } catch (Exception ex) {
            throw new RuntimeException(
                    "FOSMandaysInfoCreate : Error while adding FOSTeam Mandays data " + fosMandaysInfoList, ex);
        }
    }

    public Boolean addCentreDetails(CentreInfo centreInfo, long loggedInUserId) {
        try {
            Assert.notNull(centreInfo);
            saveEntity(centreInfo, String.valueOf(loggedInUserId));
            return true;
        } catch (Exception ex) {
            throw new RuntimeException(
                    "CentreInfo : Error while adding CentreInfo data " + centreInfo, ex);
        }
    }

    public List<CentreInfo> getCentreDetailsByName(Set<String> centreList, SalesTeam team) {
        if (CollectionUtils.isEmpty(centreList)) {
            return null;
        }

        Query query = new Query();
        Criteria criteria = new Criteria();
        if (team == null) {
            criteria = Criteria.where(CentreInfo.Constants.CENTRE_NAME).in(centreList);
        } else {
            criteria = criteria.andOperator(Criteria.where(CentreInfo.Constants.TEAM).is(team), Criteria.where(CentreInfo.Constants.CENTRE_NAME).in(centreList));
        }
        query.addCriteria(criteria);
        return runQuery(query, CentreInfo.class);
    }

    public List<CentreInfo> getCentreDetailsByName(String centreName) {
        if (StringUtils.isEmpty(centreName)) {
            return null;
        }
        Query query = new Query();
        Criteria criteria = Criteria.where(CentreInfo.Constants.CENTRE_NAME).is(centreName);
        query.addCriteria(criteria);
        return runQuery(query, CentreInfo.class);
    }

    public void updateCentreName(CentreInfo centreInfo, long loggedInUserId) {
        saveEntity(centreInfo, String.valueOf(loggedInUserId));
    }

    public void addTargetInput(List<TargetInput> targetInputs) throws BadRequestException {
        try {
            insertAllEntities(targetInputs, TargetInput.class.getSimpleName());
        } catch (Exception ex) {
            throw new RuntimeException("Target Info : Error while by adding Targetnput details " + targetInputs, ex);
        }

    }

    public void addISTeamMandays(List<ISMandaysInfo> isMandaysInfos, long loggedInUserId) {
        try {
            insertAllEntities(isMandaysInfos, ISMandaysInfo.class.getSimpleName(), String.valueOf(loggedInUserId));
        } catch (Exception ex) {
            throw new RuntimeException(
                    "ISMandaysInfoCreate : Error while adding ISTeam Mandays data " + isMandaysInfos, ex);
        }
    }

    public Query getSalesAgentMetadataQuery(SalesAgentMetadataReq req) {
        Query query = new Query();
        query.addCriteria(Criteria.where(SalesAgentInfo.Constants.ENTITY_STATE).is(EntityState.ACTIVE));
        query.addCriteria(Criteria.where(SalesAgentInfo.Constants.TEAM).is(req.getTeam()));

        if (SalesTeam.FOS.equals(req.getTeam())) {

            if (StringUtils.isNotEmpty(req.getCentreId())) {
                query.addCriteria(Criteria.where(SalesAgentInfo.Constants.CENTRE_ID).is(req.getCentreId()));
            } else if (StringUtils.isNotEmpty(req.getZhId()) || StringUtils.isNotEmpty(req.getRmId()) ||
                    StringUtils.isNotEmpty(req.getCmId())) {
                Query centreQuery = new Query();
                Criteria criteria = new Criteria();
                if (StringUtils.isNotEmpty(req.getZhId())) {
                    criteria = Criteria.where(CentreInfo.Constants.ZH).is(req.getZhId());
                }
                if (StringUtils.isNotEmpty(req.getCmId())) {
                    criteria = Criteria.where(CentreInfo.Constants.CM).is(req.getCmId());
                }
                if (StringUtils.isNotEmpty(req.getRmId())) {
                    criteria = Criteria.where(CentreInfo.Constants.RM).is(req.getRmId());
                }
                if (criteria != null) {
                    centreQuery.addCriteria(criteria);
                    List<CentreInfo> centreInfos = runQuery(centreQuery, CentreInfo.class);
                    if (CollectionUtils.isNotEmpty(centreInfos)) {
                        query.addCriteria(Criteria.where(SalesAgentInfo.Constants.CENTRE_ID).in(centreInfos.stream().map(CentreInfo::getId).collect(Collectors.toList())));
                    }
                }
            }
        } else if (SalesTeam.IS.equals(req.getTeam())) {
            if (StringUtils.isNotEmpty(req.getIsFMId())) {
                query.addCriteria(Criteria.where(SalesAgentInfo.Constants.IS_FM_ID).is(req.getIsFMId()));
            }
            if (StringUtils.isNotEmpty(req.getIsRMId())) {
                query.addCriteria(Criteria.where(SalesAgentInfo.Constants.IS_RM_ID).is(req.getIsRMId()));
            }
        }
        if (StringUtils.isNotEmpty(req.getTlId())) {
            query.addCriteria(Criteria.where(SalesAgentInfo.Constants.TL_ID).is(req.getTlId()));
        }
        if (req.getStatus() != null) {
            query.addCriteria(Criteria.where(SalesAgentInfo.Constants.STATUS).is(req.getStatus()));
        }
        if (req.getDesignation() != null) {
            query.addCriteria(Criteria.where(SalesAgentInfo.Constants.DESIGNATION).is(req.getDesignation()));
        }
        if ((req.getDateOfJoiningFrom() != null) || (req.getDateOfJoiningTo() != null)) {
            Criteria criteria = null;
            if (req.getDateOfJoiningFrom() != null) {
                criteria = Criteria.where(SalesAgentInfo.Constants.DATE_OF_JOINING).gte(req.getDateOfJoiningFrom());
            }
            if (req.getDateOfJoiningTo() != null) {
                if (criteria != null) {
                    criteria = criteria
                            .andOperator(Criteria.where(SalesAgentInfo.Constants.DATE_OF_JOINING).lt(req.getDateOfJoiningTo()));
                } else {
                    criteria = Criteria.where(SalesAgentInfo.Constants.DATE_OF_JOINING).lt(req.getDateOfJoiningTo());
                }
            }
            query.addCriteria(criteria);
        }

        if ((req.getDateOfLeavingFrom() != null) || (req.getDateOfLeavingTo() != null)) {
            Criteria criteria = null;
            if (req.getDateOfLeavingFrom() != null) {
                criteria = Criteria.where(SalesAgentInfo.Constants.DATE_OF_LEAVING).gte(req.getDateOfLeavingFrom());
            }
            if (req.getDateOfLeavingTo() != null) {
                if (criteria != null) {
                    criteria = criteria
                            .andOperator(Criteria.where(SalesAgentInfo.Constants.DATE_OF_LEAVING).lt(req.getDateOfLeavingTo()));
                } else {
                    criteria = Criteria.where(SalesAgentInfo.Constants.DATE_OF_LEAVING).lt(req.getDateOfLeavingTo());
                }
            }
            query.addCriteria(criteria);
        }
        return query;
    }

    public SalesAgentMetadataRes getSalesAgentMetadata(SalesAgentMetadataReq req, Set<String> requiredFields, boolean isInclude) {

        SalesAgentMetadataRes salesAgentMetadataRes = new SalesAgentMetadataRes();
        Query query = getSalesAgentMetadataQuery(req);


        manageFieldsInclude(query, requiredFields, isInclude);

        query.with(Sort.by(Sort.Direction.DESC, SalesAgentInfo.Constants.LAST_UPDATED));
        List<SalesAgentInfo> salesAgentInfoCount = runQuery(query, SalesAgentInfo.class);
        if (CollectionUtils.isNotEmpty(salesAgentInfoCount)) {
            salesAgentMetadataRes.setSalesAgentInfoCount(salesAgentInfoCount.size());
        }
        setFetchParameters(query, req.getStart(), req.getSize());
        logger.info(query);
        List<SalesAgentInfo> salesAgentInfo = runQuery(query, SalesAgentInfo.class);
        salesAgentMetadataRes.setSalesAgentInfoList(salesAgentInfo);
        return salesAgentMetadataRes;
    }

    private void manageFieldsInclude(Query q, Set<String> keySet, boolean isInclude) {
        if (keySet != null && !keySet.isEmpty()) {
            if (isInclude)
                keySet.parallelStream().forEach(includeKey -> q.fields().include(includeKey));
            else
                keySet.parallelStream().forEach(includeKey -> q.fields().exclude(includeKey));
        }
    }

    public SalesAgentInfo getSalesAgentInfoById(String agentId) {
        return getEntityById(agentId, SalesAgentInfo.class);
    }

    public Boolean addSalesAgentData(SalesAgentInfo salesAgentInfo, long callingUserId) {

        try {
            Assert.notNull(salesAgentInfo);
            saveEntity(salesAgentInfo, String.valueOf(callingUserId));
            return true;
        } catch (Exception ex) {
            throw new RuntimeException("SalesAgentInfoError : Error updating the Sales Agent info " + salesAgentInfo.toString(), ex);
        }
    }

    public CentreInfo getCentreInfoById(String centreId) {
        return getEntityById(centreId, CentreInfo.class);
    }

    public List<SalesAgentInfo> getSalesAgentInfoByIds(Set<String> agentList, Set<String> requiredFields, boolean isInclude) {
        Query query = new Query();
        query.addCriteria(Criteria.where(SalesAgentInfo.Constants._ID).in(agentList));
        manageFieldsInclude(query, requiredFields, isInclude);
        return runQuery(query, SalesAgentInfo.class);
    }

    public List<SalesAgentInfo> getSalesAgentFilterValue(SalesAgentFilterValueReq salesAgentFilterValueReq, Set<String> requiredFields, boolean isInclude) {
        Query query = new Query();
        Criteria andCriteria = new Criteria();
        andCriteria.andOperator(Criteria.where(SalesAgentInfo.Constants.DESIGNATION_TYPE).is(salesAgentFilterValueReq.getDesignationType()),
                Criteria.where(SalesAgentInfo.Constants.TEAM).is(salesAgentFilterValueReq.getTeam()));
        query.addCriteria(andCriteria);
        manageFieldsInclude(query, requiredFields, isInclude);
        setFetchParameters(query, salesAgentFilterValueReq.getStart(), salesAgentFilterValueReq.getSize());
        return runQuery(query, SalesAgentInfo.class);
    }

    public Boolean addSalesDataToHistory(SalesAgentInfoHistory salesAgentInfoHistory, long callingUserId) {
        try {
            Assert.notNull(salesAgentInfoHistory);
            saveEntity(salesAgentInfoHistory, String.valueOf(callingUserId));
            return true;
        } catch (Exception ex) {
            throw new RuntimeException("salesAgentInfoHistoryError : Error updating the Sales Agent info History " + salesAgentInfoHistory.toString(), ex);
        }

    }

    public Boolean addCentreInfoToHistory(CentreInfoHistory centreInfoHistory, long loggedInUserId) {
        try {
            Assert.notNull(centreInfoHistory);
            saveEntity(centreInfoHistory, String.valueOf(loggedInUserId));
            return true;
        } catch (Exception ex) {
            throw new RuntimeException("centreInfoHistoryError : Error updating the Centre info History " + centreInfoHistory.toString(), ex);
        }
    }

    public List<CentreInfo> getFOSCentreFilter(FOSCentreFilterReq fosCentreFilterReq, Set<String> requiredFields, boolean isInclude) {

        Query query = new Query();
        query.addCriteria(Criteria.where(SalesAgentInfo.Constants.ENTITY_STATE).is(EntityState.ACTIVE));
        Criteria criteria = new Criteria();
        if (StringUtils.isNotEmpty(fosCentreFilterReq.getZhId())) {
            criteria = Criteria.where(CentreInfo.Constants.ZH).is(fosCentreFilterReq.getZhId());
        }
        if (StringUtils.isNotEmpty(fosCentreFilterReq.getRmId())) {
            criteria = Criteria.where(CentreInfo.Constants.RM).is(fosCentreFilterReq.getRmId());
        }
        if (StringUtils.isNotEmpty(fosCentreFilterReq.getCmId())) {
            criteria = Criteria.where(CentreInfo.Constants.CM).is(fosCentreFilterReq.getCmId());
        }
        manageFieldsInclude(query, requiredFields, isInclude);
        if (criteria != null) {
            query.addCriteria(criteria);
        }
        return runQuery(query, CentreInfo.class);
    }

    public List<SalesAgentInfo> getISFilterByDesignation(String isFMId, Set<String> requiredFields, boolean isInclude) {
        Query query = new Query();
        Criteria criteria = new Criteria();
        criteria.andOperator(Criteria.where(SalesAgentInfo.Constants.DESIGNATION).is(AgentDesignation.REVENUE_MANAGER), Criteria.where(SalesAgentInfo.Constants.IS_FM_ID).is(isFMId));
        manageFieldsInclude(query, requiredFields, isInclude);
        query.addCriteria(criteria);
        return runQuery(query, SalesAgentInfo.class);
    }

    public List<CentreInfo> getCentreInfoByIds(Set<String> centreIds, Set<String> requiredCentreFields, boolean isInclude) {
        Query query = new Query();
        query.addCriteria(Criteria.where(CentreInfo.Constants._ID).in(centreIds));
        manageFieldsInclude(query, requiredCentreFields, isInclude);
        return runQuery(query, CentreInfo.class);
    }

    public FOSCentreFilterRes getCentreInfos(Integer start, Integer size) {
        FOSCentreFilterRes fosCentreFilterRes = new FOSCentreFilterRes();
        ;
        Query query = new Query();
        query.with(Sort.by(Sort.Direction.ASC, CentreInfo.Constants.CENTRE_NAME));
        List<CentreInfo> centreInfoCount = runQuery(query, CentreInfo.class);
        if (CollectionUtils.isNotEmpty(centreInfoCount)) {
            fosCentreFilterRes.setCountOfAllCentres(centreInfoCount.size());
        }
        setFetchParameters(query, start, size);
        fosCentreFilterRes.setCentreInfos(runQuery(query, CentreInfo.class));
        return fosCentreFilterRes;
    }

    public List<SalesAgentInfo> getTeamLeadByCentre(String centreId, Set<String> requiredFields, boolean isInclude) {
        Query query = new Query();
        Criteria criteria = new Criteria();
        criteria.andOperator(Criteria.where(SalesAgentInfo.Constants.CENTRE_ID).is(centreId), Criteria.where(SalesAgentInfo.Constants.DESIGNATION_TYPE).is(AgentDesignationType.TL));
        manageFieldsInclude(query, requiredFields, isInclude);
        query.addCriteria(criteria);
        return runQuery(query, SalesAgentInfo.class);
    }


    public List<SalesAgentInfo> checkSAUniqueValue(CheckSAUniqueValueReq checkSAUniqueValueReq, Set<String> requiredFields, boolean isInclude) {
        Query query = new Query();
        if (StringUtils.isNotEmpty(checkSAUniqueValueReq.getEmail())) {
            query.addCriteria(Criteria.where(SalesAgentInfo.Constants.EMAIL).is(checkSAUniqueValueReq.getEmail()));
        } else if (StringUtils.isNotEmpty(checkSAUniqueValueReq.getEmployeeId())) {
            query.addCriteria(Criteria.where(SalesAgentInfo.Constants.EMPLOYEE_ID).is(checkSAUniqueValueReq.getEmployeeId()));
        }

        manageFieldsInclude(query, requiredFields, isInclude);
        return runQuery(query, SalesAgentInfo.class);
    }

    public List<SalesAgentInfo> getSARegex(GetAllSalesAgentReq getAllSalesAgentReq, Set<String> requiredFields, boolean isInclude) {
        Query query = new Query();
        query.addCriteria(Criteria.where(SalesAgentInfo.Constants.TEAM).is(getAllSalesAgentReq.getTeam()));
        if (getAllSalesAgentReq.getDesignationType() != null) {
            query.addCriteria(Criteria.where(SalesAgentInfo.Constants.DESIGNATION_TYPE).is(getAllSalesAgentReq.getDesignationType()));
        }
        if (StringUtils.isNotEmpty(getAllSalesAgentReq.getQuery())) {
            String queryString = getAllSalesAgentReq.getQuery().replaceAll("\\*", "");
            if (StringUtils.isNotEmpty(queryString)) {
                Criteria criteria = new Criteria();
                criteria.orOperator(Criteria.where(SalesAgentInfo.Constants.EMAIL).regex("^" + queryString, "m"),
                        Criteria.where(SalesAgentInfo.Constants.EMPLOYEE_ID).regex("^" + queryString, "m"));
                query.addCriteria(criteria);
            }
        }
        manageFieldsInclude(query, requiredFields, isInclude);
        setFetchParameters(query, 0, 20);
        logger.info(query);
        return runQuery(query, SalesAgentInfo.class);
    }

    public SalesAgentInfo getSalesAgentInfoByEmployeeId(String employeeId) throws NotFoundException {
        Query query = new Query();
        query.addCriteria(Criteria.where(SalesAgentInfo.Constants.EMPLOYEE_ID).is(employeeId));
        List<SalesAgentInfo> salesAgentInfos = runQuery(query, SalesAgentInfo.class);
        if (ArrayUtils.isNotEmpty(salesAgentInfos) && null != salesAgentInfos) {
            return salesAgentInfos.get(0);
        }
        return null;
    }

    public List<SalesAgentInfo> getSalesAgentInfosByEmployeeIds(Set<String> employeeIds) {
        Query query = new Query();
        query.addCriteria(Criteria.where(SalesAgentInfo.Constants.EMPLOYEE_ID).in(employeeIds));
        return runQuery(query, SalesAgentInfo.class);
    }

    public List<CentreInfo> getCentreIdsByNames(Set<String> centreNames) {
        Query query = new Query();
        query.addCriteria(Criteria.where(CentreInfo.Constants.CENTRE_NAME).in(centreNames));
        return runQuery(query, CentreInfo.class);
    }

    public void addAgentAttendance(List<SalesAgentAttendanceInfo> salesAgentAttendanceInfos, Long callingUserId) throws BadRequestException {
        try {
            insertAllEntities(salesAgentAttendanceInfos, SalesAgentAttendanceInfo.class.getSimpleName(), String.valueOf(callingUserId));
        } catch (Exception ex) {
            throw new RuntimeException("SalesAgentAttendanceInfo : Error while by adding SalesAgentAttendanceInfo details " + salesAgentAttendanceInfos, ex);
        }

    }

    public List<SalesAgentInfo> getSalesAgentInfoByEmployeeIds(Set<String> employeeIds) {
        logger.info("***employeeIds***" + employeeIds);
        Query query = new Query();
        query.addCriteria(Criteria.where(SalesAgentInfo.Constants.EMPLOYEE_ID).in(employeeIds));
        return runQuery(query, SalesAgentInfo.class);
    }

    public SalesAgentAttendanceInfo getSAAttendanceInfo(String id, String date) throws ParseException {
        Query query = new Query();
        query.addCriteria(Criteria.where(SalesAgentAttendanceInfo.Constants.AGENT_ID).is(id));
        query.addCriteria(Criteria.where(SalesAgentAttendanceInfo.Constants.DATE).is(CommonUtils.parseTime(date)));
        List<SalesAgentAttendanceInfo> salesAgentAttendanceInfos = runQuery(query, SalesAgentAttendanceInfo.class);
        if (ArrayUtils.isNotEmpty(salesAgentAttendanceInfos)) {
            return salesAgentAttendanceInfos.get(0);
        }
        return null;
    }

    public void updateSAAttendanceInfo(AgentAttendancePojo attendancePojo) throws ParseException {
        Query query = new Query();
        query.addCriteria(Criteria.where(SalesAgentAttendanceInfo.Constants.AGENT_ID).is(attendancePojo.getAgentId()));
        query.addCriteria(Criteria.where(SalesAgentAttendanceInfo.Constants.DATE).is(CommonUtils.parseTime(attendancePojo.getDate())));
        Update update = new Update();
        if (StringUtils.isNotEmpty(attendancePojo.getAttendanceStatus())){
            update.set(SalesAgentAttendanceInfo.Constants.ATTENDANCE_STATUS, attendancePojo.getAttendanceStatus());
            updateFirst(query, update, SalesAgentAttendanceInfo.class);
            return;
        }
        deleteEntities(query,SalesAgentAttendanceInfo.class);
    }

    public void addAgentAttendanceToHistory(SalesAgentAttendanceInfoHistory salesAgentAttendanceInfoHistory) {
        HttpSessionData sessionData = sessionUtils.getCurrentSessionData();
        saveEntity(salesAgentAttendanceInfoHistory, sessionData.getUserId().toString());

    }

    public List<SalesAgentAttendanceInfo> getAgentAttendanceData(List<String> agentIds, SalesAgentAttendanceDataReq salesAgentAttendanceDataReq) {
        Query query = new Query();
        query.addCriteria(Criteria.where(SalesAgentAttendanceInfo.Constants.AGENT_ID).in(agentIds));
        Long attendanceFrom = salesAgentAttendanceDataReq.getAttendanceFrom();
        Long attendanceTo = salesAgentAttendanceDataReq.getAttendanceTo();
        if ((attendanceFrom != null) || (attendanceTo != null)) {
            Criteria criteria = new Criteria();
            if (attendanceFrom != null && attendanceTo != null) {
                criteria.andOperator(Criteria.where(SalesAgentAttendanceInfo.Constants.DATE).gte(attendanceFrom),
                        Criteria.where(SalesAgentAttendanceInfo.Constants.DATE).lt(attendanceTo));
            } else if (attendanceFrom != null && attendanceTo == null) {
                //considering default 30 days
                LocalDateTime localDateTimeFrom = Instant.ofEpochMilli(attendanceFrom)
                        .atZone(ZoneId.systemDefault())
                        .toLocalDateTime();
                Long difference = System.currentTimeMillis() - attendanceFrom;
                Long days = difference / (1000 * 60 * 60 * 24);
                LocalDateTime localDateTimeTo = localDateTimeFrom.plusDays(days);
                criteria.andOperator(Criteria.where(SalesAgentAttendanceInfo.Constants.DATE).gte(attendanceFrom),
                        Criteria.where(SalesAgentAttendanceInfo.Constants.DATE).lt(Timestamp.valueOf(localDateTimeTo).getTime()));
            } else {
                LocalDateTime localDateTimeTo = Instant.ofEpochMilli(attendanceTo)
                        .atZone(ZoneId.systemDefault())
                        .toLocalDateTime();
                LocalDateTime localDateTimeFrom = localDateTimeTo.minusDays(31);
                criteria.andOperator(Criteria.where(SalesAgentAttendanceInfo.Constants.DATE).gte(Timestamp.valueOf(localDateTimeFrom).getTime()),
                        Criteria.where(SalesAgentAttendanceInfo.Constants.DATE).lt(attendanceTo));
            }
            query.addCriteria(criteria);
        } else {
            LocalDateTime localDateTimeTo = Instant.ofEpochMilli(System.currentTimeMillis())
                    .atZone(ZoneId.systemDefault())
                    .toLocalDateTime();
            LocalDateTime localDateTimeFrom = localDateTimeTo.minusDays(31);
            Criteria andCriteria = new Criteria();
            andCriteria.andOperator(Criteria.where(SalesAgentAttendanceInfo.Constants.DATE).gte(Timestamp.valueOf(localDateTimeFrom).getTime()),
                    Criteria.where(SalesAgentAttendanceInfo.Constants.DATE).lt(Timestamp.valueOf(localDateTimeTo).getTime()));
            query.addCriteria(andCriteria);
        }
        query.with(Sort.by(Sort.Direction.DESC, SalesAgentAttendanceInfo.Constants.LAST_UPDATED));
        logger.info(query);
        return runQuery(query, SalesAgentAttendanceInfo.class);
    }

    public String getAgentIdByEmployeeId(String employeeId) {
        Query query = new Query();
        query.addCriteria(Criteria.where(SalesAgentInfo.Constants.EMPLOYEE_ID).is(employeeId));
        List<SalesAgentInfo> salesAgentInfos = runQuery(query, SalesAgentInfo.class);
        if (ArrayUtils.isNotEmpty(salesAgentInfos)) {
            String agentId = salesAgentInfos.get(0).getId();
            return agentId;
        }
        return null;
    }
    public void editAgentAttendance(SalesAgentAttendanceInfo salesAgentAttendanceInfo,Long callingUserId){
        try{
            saveEntity(salesAgentAttendanceInfo,String.valueOf(callingUserId));
        }catch (Exception ex){
            throw new RuntimeException("Error while updateing sales agent info : " + salesAgentAttendanceInfo,ex);
        }
    }
}
