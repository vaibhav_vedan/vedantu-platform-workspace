package com.vedantu.vedantudata.dao.serializers;

import java.util.List;

import com.vedantu.exception.DuplicateEntryException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.util.LogFactory;
import com.vedantu.vedantudata.managers.leadsquared.LeadsquaredUserManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.vedantu.util.dbutils.AbstractMongoDAO;
import com.vedantu.vedantudata.entities.leadsquared.LeadsquaredUser;

@Service
public class LeadsquaredUserDAO extends AbstractMongoDAO {
	@Autowired
	private MongoClientFactory mongoClientFactory;

	public LeadsquaredUserDAO() {
		super();
	}

	@Override
	protected MongoOperations getMongoOperations() {
		return mongoClientFactory.getMongoOperations();
	}

	@Autowired
	private LogFactory logFactory;

	@SuppressWarnings("static-access")
	private Logger logger = logFactory.getLogger(LeadsquaredUserDAO.class);

	public void create(LeadsquaredUser p) throws Exception {
		if (p != null) {
			saveEntity(p);
		}
	}

	public LeadsquaredUser getById(String id) {
		LeadsquaredUser p = null;
		try {
			if (!StringUtils.isEmpty(id)) {
				p = (LeadsquaredUser) getEntityById(id, LeadsquaredUser.class);
			}
		} catch (Exception ex) {
			// log Exception
			p = null;
		}
		return p;
	}

	public List<LeadsquaredUser> getByUserIds(List<String> leadsquaredIds) {
		Query query = new Query();
		query.addCriteria(Criteria.where(LeadsquaredUser.Constants.LEAD_SQUARED_USER_ID).in(leadsquaredIds));
		return runQuery(query, LeadsquaredUser.class);
	}

	public void save(LeadsquaredUser p) throws Exception {
		if (p != null) {
			saveEntity(p);
		}
	}

	public int deleteById(String id) {
		int result = 0;
		try {
			result = deleteEntityById(id, LeadsquaredUser.class);
		} catch (Exception ex) {
			// throw Exception;
		}

		return result;
	}

	public void deleteByEmais(List<String> emailIdList){
		try {
			Query query = new Query(Criteria.where(LeadsquaredUser.Constants.EMAIL).in(emailIdList));
			deleteEntities(query,LeadsquaredUser.class);
		}
		catch (Exception ex){

		}
	}

	public void insertLeadSquaredUserList(List<LeadsquaredUser> leadsquaredUserList) {
		try{
			insertAllEntities(leadsquaredUserList,"LeadsquaredUser");
		}
		catch (Exception ex){
		}
	}
}
