package com.vedantu.vedantudata.dao;

import com.vedantu.util.LogFactory;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import com.vedantu.vedantudata.dao.serializers.MongoClientFactory;
import com.vedantu.vedantudata.entities.leadsquared.AssignedLeads;
import com.vedantu.vedantudata.entities.leadsquared.LeadActualAssignment;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LeadAutoAssignmentDAO extends AbstractMongoDAO {

    @Autowired
    private MongoClientFactory mongoClientFactory;

    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(LeadAutoAssignmentDAO.class);

    public void pushDataToDb(AssignedLeads assignedLeads) {
        try {
            saveEntity(assignedLeads, AssignedLeads.class.getSimpleName());
        } catch (Exception ex) {
            throw new RuntimeException(
                    "Error while pushing assigned data " + assignedLeads , ex);
        }
    }

    public void pushLeadActualAssignmentDateToDB(List<LeadActualAssignment> leadActualAssignments) {
        try {
            insertAllEntities(leadActualAssignments, LeadActualAssignment.class.getSimpleName());
        } catch (Exception ex) {
            throw new RuntimeException(
                    "Error while pushing leadActualAssignments data "  , ex);
        }
    }

}
