package com.vedantu.vedantudata.dao;

import com.vedantu.util.LogFactory;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import com.vedantu.vedantudata.dao.serializers.MongoClientFactory;
import com.vedantu.vedantudata.entities.ActivityDetails;
import com.vedantu.vedantudata.entities.DemoOTPValidation;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DemoOTPValidationDao  extends AbstractMongoDAO {

    @Autowired
    private MongoClientFactory mongoClientFactory;

    @Autowired
    private LogFactory logFactory;

    private final Logger logger = logFactory.getLogger(ActivityDao.class);

    @Override
    protected MongoOperations getMongoOperations() {

        return mongoClientFactory.getMongoOperations();
    }

    public void save(String prospectActivityId, String leadEmail, String phoneNumber, String otp, Long otpExpiryTime) {
        DemoOTPValidation demoOTPValidation = new DemoOTPValidation();
        demoOTPValidation.setPhone(phoneNumber);
        demoOTPValidation.setEmailAddress(leadEmail);
        demoOTPValidation.setOTP(otp);
        demoOTPValidation.setExpiryTime(otpExpiryTime);
        demoOTPValidation.setProspectActivityId(prospectActivityId);

        saveEntity(demoOTPValidation);
    }

    public DemoOTPValidation getLatestActiveOtpDetails(String leadEmail) {
        Query q = new Query();
        q.addCriteria(Criteria.where(DemoOTPValidation.Constants.EMAIL_ADDRESS).is(leadEmail));
        q.addCriteria(Criteria.where(DemoOTPValidation.Constants.EXPIRY_TIME).gt(System.currentTimeMillis()));
        q.limit(1);
        q.with(Sort.by(Sort.Direction.DESC, "creationTime"));

        logger.info("Logger info: " + q);
        List<DemoOTPValidation> results = runQuery(q, DemoOTPValidation.class);
        if (results != null && !results.isEmpty()) {
            return results.get(0);
        }
        return null;

    }
}
