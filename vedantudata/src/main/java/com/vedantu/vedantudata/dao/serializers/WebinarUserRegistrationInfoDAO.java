package com.vedantu.vedantudata.dao.serializers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import com.vedantu.util.dbutils.AbstractMongoDAO;
import com.vedantu.vedantudata.entities.WebinarUserRegistrationInfo;
import org.springframework.data.domain.Sort;

@Service
public class WebinarUserRegistrationInfoDAO extends AbstractMongoDAO {

    @Autowired
    private MongoClientFactory mongoClientFactory;

    public WebinarUserRegistrationInfoDAO() {
        super();
    }

    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }

    public void create(WebinarUserRegistrationInfo p) {
        if (p != null) {
            saveEntity(p);
        }
    }

    public WebinarUserRegistrationInfo getById(String id) {
        WebinarUserRegistrationInfo p = null;
        try {
            if (!StringUtils.isEmpty(id)) {
                p = (WebinarUserRegistrationInfo) getEntityById(id, WebinarUserRegistrationInfo.class);
            }
        } catch (Exception ex) {
            // log Exception
            p = null;
        }
        return p;
    }

    public void save(WebinarUserRegistrationInfo p) throws Exception {
        if (p != null) {
            saveEntity(p);
        }
    }

    public WebinarUserRegistrationInfo getWebinarUserInfo(String email, String traningId) {
        Query query = new Query();
        query.addCriteria(Criteria.where(WebinarUserRegistrationInfo.Constants.EMAIL_ID).is(email));
        query.addCriteria(Criteria.where(WebinarUserRegistrationInfo.Constants.TRAINING_ID).is(traningId));

        List<WebinarUserRegistrationInfo> results = runQuery(query, WebinarUserRegistrationInfo.class);

        WebinarUserRegistrationInfo webinarUserRegistrationInfo = null;
        if (!CollectionUtils.isEmpty(results)) {
            if (results.size() > 1) {
                // TODO : delete one of the entry
            }
            webinarUserRegistrationInfo = results.get(0);
        }

        return webinarUserRegistrationInfo;
    }

    public int deleteById(String id) {
        int result = 0;
        try {
            result = deleteEntityById(id, WebinarUserRegistrationInfo.class);
        } catch (Exception ex) {
            // throw Exception;
        }

        return result;
    }

    public List<WebinarUserRegistrationInfo> getWebinarUserRegistrationInfos(String traningId, Integer start, Integer size) {
        Query query = new Query();
        query.addCriteria(Criteria.where(WebinarUserRegistrationInfo.Constants.TRAINING_ID).is(traningId));
        query.with(Sort.by(Sort.Direction.ASC, WebinarUserRegistrationInfo.Constants.CREATION_TIME));
        setFetchParameters(query, start, size);
        return runQuery(query, WebinarUserRegistrationInfo.class);
    }

    public Long getWebinarUserRegistrationInfosCount(String traningId) {
        Query query = new Query();
        query.addCriteria(Criteria.where(WebinarUserRegistrationInfo.Constants.TRAINING_ID).is(traningId));
        return queryCount(query, WebinarUserRegistrationInfo.class);
    }

}
