package com.vedantu.vedantudata.dao;

import com.vedantu.util.LogFactory;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import com.vedantu.vedantudata.dao.serializers.MongoClientFactory;
import com.vedantu.vedantudata.entities.ActivityDetails;
import com.vedantu.vedantudata.pojos.AggregatedActivityDetails;
import com.vedantu.vedantudata.utils.ActivityEventConstants;
import org.apache.logging.log4j.Logger;
import org.bson.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationOptions;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class ActivityDao extends AbstractMongoDAO {

    @Autowired
    private MongoClientFactory mongoClientFactory;

    @Autowired
    private LogFactory logFactory;

    private final Logger logger = logFactory.getLogger(ActivityDao.class);

    @Override
    protected MongoOperations getMongoOperations() {

        return mongoClientFactory.getMongoOperations();
    }

    public void creatActivity(ActivityDetails activityDetails) {

        saveEntity(activityDetails);
    }

    public void updateActivity(ActivityDetails activityDetails) {

        saveEntity(activityDetails);
    }

    public ActivityDetails getLastActivity(Long userId, int activityCode) {
        Query q = new Query();
        q.addCriteria(Criteria.where(ActivityDetails.Constants.LEAD_VEDANTU_ID).is(userId));
        q.addCriteria(Criteria.where(ActivityDetails.Constants.ACTIVITY_EVENT).is(activityCode));
        q.limit(1);
        q.with(Sort.by(Sort.Direction.DESC, "creationTime"));

        logger.info("Logger info: " + q);
        List<ActivityDetails> results = runQuery(q, ActivityDetails.class);
        if (results != null && !results.isEmpty()) {
            return results.get(0);
        }
        return null;

    }

    public ActivityDetails getLastActivity(int activityCode) {
        Query q = new Query();
        q.addCriteria(Criteria.where(ActivityDetails.Constants.ACTIVITY_EVENT).is(activityCode));
        q.limit(1);
        q.with(Sort.by(Sort.Direction.DESC, "creationTime"));

        logger.info("Logger info: " + q);
        List<ActivityDetails> results = runQuery(q, ActivityDetails.class);
        if (results != null && !results.isEmpty()) {
            return results.get(0);
        }
        return null;

    }

    public ActivityDetails getLastActivity(String email, int activityCode) {
        Query q = new Query();
        q.addCriteria(Criteria.where(ActivityDetails.Constants.LEAD_EMAIL_ADDRESS).is(email));
        q.addCriteria(Criteria.where(ActivityDetails.Constants.ACTIVITY_EVENT).is(activityCode));
        q.limit(1);
        q.with(Sort.by(Sort.Direction.DESC, "creationTime"));

        logger.info("Logger info: " + q);
        List<ActivityDetails> results = runQuery(q, ActivityDetails.class);
        if (results != null && !results.isEmpty()) {
            return results.get(0);
        }
        return null;

    }

    public ActivityDetails getActivityById(String prospectActivityId) {
        Query q = new Query();
        q.addCriteria(Criteria.where(ActivityDetails.Constants.PROSPET_ACTIVITY_ID).is(prospectActivityId));

        logger.info("Logger info: " + q);
        List<ActivityDetails> results = runQuery(q, ActivityDetails.class);
        if (results != null && !results.isEmpty()) {
            return results.get(0);
        }
        return null;

    }

    public List<ActivityDetails> getActivitydetails(List<Integer> activityCodes, Long fromTime, Long thruTime) {
        Query q = new Query();

        Criteria andCriteria = new Criteria().andOperator(Criteria.where(ActivityDetails.Constants.CREATION_TIME).gte(fromTime),
                Criteria.where(ActivityDetails.Constants.CREATION_TIME).lte(thruTime));
        q.addCriteria(andCriteria);
        q.addCriteria(Criteria.where(ActivityDetails.Constants.ACTIVITY_EVENT).in(activityCodes));
        q.with(Sort.by(Sort.Direction.DESC, "creationTime"));

        logger.info("Logger info: " + q);
        return runQuery(q, ActivityDetails.class, ActivityDetails.class.getName());

    }


    public void creatActivities(List<ActivityDetails> activityDetailsList) {

        if(activityDetailsList.isEmpty())
            return;

        insertAllEntities(activityDetailsList, ActivityDetails.class.getSimpleName());
    }

    public boolean checkForFreshdeskActivity(Long studentId, Integer eventCode, Long ticketId) {
        List<ActivityDetails> activityDetails = getFreshDeskActivityDetails(studentId, eventCode, ticketId);
        return !activityDetails.isEmpty();
    }

    public List<ActivityDetails> getFreshDeskActivityDetails(Long studentId, Integer eventCode, Long ticketId) {
        Query query = new Query();
        query.addCriteria(Criteria.where(ActivityDetails.Constants.FRESHDESK_TICKET_ID).is(ticketId))
                .addCriteria(Criteria.where(ActivityDetails.Constants.LEAD_VEDANTU_ID).is(studentId))
                .addCriteria(Criteria.where(ActivityDetails.Constants.ACTIVITY_EVENT).is(eventCode));
        List<ActivityDetails> activityDetails = runQuery(query, ActivityDetails.class);
        return activityDetails == null ? new ArrayList<>() : activityDetails;
    }

    public List<ActivityDetails> getFreshdeskActivities(List<Long> ticketIds) {
        Query query = new Query();
        query.addCriteria(Criteria.where(ActivityDetails.Constants.FRESHDESK_TICKET_ID).in(ticketIds));
        return runQuery(query, ActivityDetails.class);
    }

    public void updateEntities(List<ActivityDetails> activityDetailsList) {
        if (activityDetailsList == null) {
            return;
        }

        for (ActivityDetails activityDetails : activityDetailsList) {
            saveEntity(activityDetails);
        }
    }

    public Map<String,Integer> getAcadMentorPendingActivityCount(List<String> amEmailList){
        List<Criteria> criterias = new ArrayList<>();
        criterias.add(Criteria.where(ActivityDetails.Constants.ACTIVITY_OWNER_EMAIL).in(amEmailList));

        Long fromtime = System.currentTimeMillis() - ActivityEventConstants.MILLI_DAY;
        Criteria andCriteria = new Criteria().andOperator(Criteria.where(ActivityDetails.Constants.CREATION_TIME).gte(fromtime),
                Criteria.where(ActivityDetails.Constants.CREATION_TIME).lte(System.currentTimeMillis()));
        criterias.add(andCriteria);

        AggregationOptions aggregationOptions= Aggregation.newAggregationOptions().cursor(new Document()).build();
        Aggregation aggregation = Aggregation.newAggregation(
                Aggregation.match(new Criteria().andOperator(criterias.toArray(new Criteria[criterias.size()]))),
                Aggregation.group(ActivityDetails.Constants.ACTIVITY_OWNER_EMAIL).count().as("count")).withOptions(aggregationOptions);

        AggregationResults<AggregatedActivityDetails> result =getMongoOperations().aggregate(aggregation,ActivityDetails.class.getSimpleName(), AggregatedActivityDetails.class);

        Map<String, Integer> countMap = new HashMap<>();

        for(AggregatedActivityDetails aggregatedActivityDetails : result){
            countMap.put(aggregatedActivityDetails.getId(), aggregatedActivityDetails.getCount());
        }

        return countMap;
    }

    public List<ActivityDetails> getOrderNotPaidActivityByOrderIds(int eventNotPaidOrders, List<String> orderIds) {
        Query query = new Query();
        query.addCriteria(Criteria.where(ActivityDetails.Constants.ACTIVITY_EVENT).is(eventNotPaidOrders));
        query.addCriteria(Criteria.where(ActivityDetails.Constants.ACTIVITY_ACTIVITY_NOTE).in(orderIds));
        return runQuery(query, ActivityDetails.class);
    }
}
