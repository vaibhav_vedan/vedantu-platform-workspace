package com.vedantu.vedantudata.dao;

import com.vedantu.util.LogFactory;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import com.vedantu.vedantudata.dao.serializers.MongoClientFactory;
import com.vedantu.vedantudata.pojos.freshdesk.FreshdeskContact;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FreshdeskContactDao extends AbstractMongoDAO {

    @Autowired
    private MongoClientFactory mongoClientFactory;

    @Autowired
    private LogFactory logFactory;

    private final Logger logger = LogFactory.getLogger(ActivityDao.class);

    @Override
    protected MongoOperations getMongoOperations() {

        return mongoClientFactory.getMongoOperations();
    }
    public void save(FreshdeskContact contact) {

        saveEntity(contact);
    }

    public List<FreshdeskContact> getFreshDeskContacts(List<Long> ids, FreshdeskContact.Type type) {
        Query query = new Query();
        query.addCriteria(Criteria.where(FreshdeskContact.Constants.FRESHDESK_ID).in(ids));
        query.addCriteria(Criteria.where(FreshdeskContact.Constants.CONTACT_TYPE).is(type));
        logger.info(query);
        return runQuery(query, FreshdeskContact.class);
    }
}
