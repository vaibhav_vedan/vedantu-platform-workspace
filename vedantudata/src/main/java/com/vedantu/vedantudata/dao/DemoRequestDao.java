package com.vedantu.vedantudata.dao;

import com.vedantu.util.CollectionUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import com.vedantu.vedantudata.dao.serializers.MongoClientFactory;
import com.vedantu.vedantudata.entities.DemoOTPValidation;
import com.vedantu.vedantudata.enums.DemoRequestType;
import com.vedantu.vedantudata.request.DemoRequest;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DemoRequestDao extends AbstractMongoDAO {

    @Autowired
    private MongoClientFactory mongoClientFactory;

    @Autowired
    private LogFactory logFactory;

    private final Logger logger = logFactory.getLogger(DemoRequestDao.class);

    @Override
    protected MongoOperations getMongoOperations() {

        return mongoClientFactory.getMongoOperations();
    }

    public void registerDemo(DemoRequest demoRequest){

        try {
            saveEntity(demoRequest);
        }catch (Exception e){
            logger.error("Error persisting demo request", e);
        }

    }

    public DemoRequest getRegisteredDemo(Long userId, DemoRequestType demoRequestType){

        try {
            Query q = new Query();
            q.addCriteria(Criteria.where(DemoRequest.Constants.USER_ID).is(userId));
            q.addCriteria(Criteria.where(DemoRequest.Constants.DEMO_REQUEST_TYPE).is(demoRequestType));

            logger.info("Logger info: " + q);
            List<DemoRequest> results = runQuery(q, DemoRequest.class);

            if(CollectionUtils.isNotEmpty(results))
                return results.get(0);

        }catch (Exception e){
            logger.error("Error fetching demo request", e);
        }

        return null;

    }

}
