package com.vedantu.vedantudata.dao;

import com.vedantu.util.LogFactory;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import com.vedantu.vedantudata.dao.serializers.MongoClientFactory;
import com.vedantu.vedantudata.entities.ActivityDetails;
import com.vedantu.vedantudata.entities.EmailMappingLS;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.regex.Pattern;

@Service
public class EmailMappingLSDao extends AbstractMongoDAO {

    @Autowired
    private MongoClientFactory mongoClientFactory;

    @Autowired
    private LogFactory logFactory;

    private final Logger logger = logFactory.getLogger(EmailMappingLSDao.class);

    @Override
    protected MongoOperations getMongoOperations() {

        return mongoClientFactory.getMongoOperations();
    }

    public EmailMappingLS getLsEmail(String vedantuEmail) {
        if (vedantuEmail == null) {
            return null;
        }
        Query q = new Query();
        q.addCriteria(new Criteria().orOperator(
                Criteria.where(EmailMappingLS.Constants.VEDANTU_EMAIL).regex(Pattern.compile(Pattern.quote(vedantuEmail), Pattern.CASE_INSENSITIVE)),
                Criteria.where(EmailMappingLS.Constants.LS_EMAIL).regex(Pattern.compile(Pattern.quote(vedantuEmail), Pattern.CASE_INSENSITIVE)))
        );

        logger.info("Logger info: " + q);
        List<EmailMappingLS> results = runQuery(q, EmailMappingLS.class);
        if (results != null && !results.isEmpty()) {
            return results.get(0);
        }
        return null;

    }
}
