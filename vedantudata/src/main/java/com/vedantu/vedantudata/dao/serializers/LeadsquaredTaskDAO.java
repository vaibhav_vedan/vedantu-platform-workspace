package com.vedantu.vedantudata.dao.serializers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.vedantu.util.dbutils.AbstractMongoDAO;
import com.vedantu.vedantudata.entities.leadsquared.LeadTask;

@Service
public class LeadsquaredTaskDAO extends AbstractMongoDAO {

	@Autowired
	private MongoClientFactory mongoClientFactory;

	public LeadsquaredTaskDAO() {
		super();
	}

	@Override
	protected MongoOperations getMongoOperations() {
		return mongoClientFactory.getMongoOperations();
	}

	public void create(LeadTask p) {
		if (p != null) {
			saveEntity(p);
		}
	}

	public LeadTask getById(String id) {
		LeadTask p = null;
		try {
			if (!StringUtils.isEmpty(id)) {
				p = (LeadTask) getEntityById(id, LeadTask.class);
			}
		} catch (Exception ex) {
			// log Exception
			p = null;
		}
		return p;
	}

	public void save(LeadTask p) throws Exception {
		if (p != null) {
			saveEntity(p);
		}
	}

	public int deleteById(String id) {
		int result = 0;
		try {
			result = deleteEntityById(id, LeadTask.class);
		} catch (Exception ex) {
			// throw Exception;
		}

		return result;
	}

}
