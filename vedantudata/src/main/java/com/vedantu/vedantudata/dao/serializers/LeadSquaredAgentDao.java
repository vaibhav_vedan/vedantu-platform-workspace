package com.vedantu.vedantudata.dao.serializers;

import com.vedantu.exception.BadRequestException;
import com.vedantu.util.LogFactory;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import com.vedantu.vedantudata.entities.leadsquared.LeadSquaredAgent;
import com.vedantu.vedantudata.entities.salesconversion.CentreInfo;
import com.vedantu.vedantudata.entities.salesconversion.SalesAgentInfo;
import com.vedantu.vedantudata.entities.salesconversion.TargetInput;
import io.jsonwebtoken.lang.Assert;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
public class LeadSquaredAgentDao extends AbstractMongoDAO {

    @Autowired
    private MongoClientFactory mongoClientFactory;

    @Autowired
    private LogFactory logFactory;

    private final Logger logger = logFactory.getLogger(LeadSquaredAgentDao.class);

    static final String collectionName="LeadSquaredAgent";


    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }


    public List<LeadSquaredAgent> findLeadSquaredAgentByUserId(String userId)
    {
        Query query=new Query();
        query.addCriteria(Criteria.where("userid").is(userId));
        return runQuery(query,LeadSquaredAgent.class,collectionName);
    }

    public void saveLeadSquaredAgent(LeadSquaredAgent agent)
    {

        if (agent != null) {
            saveEntity(agent);
        }


    }
    public void insertLeadSquaredAgent(LeadSquaredAgent agent)
    {
        if (agent != null) {
            insertAllEntities(Arrays.asList(agent),collectionName);
        }

    }

    public List<LeadSquaredAgent> getAgentByUserIds(List<String> userIds, List<String> includeSet) {
        Query query = new Query();
        query.addCriteria(Criteria.where(LeadSquaredAgent.Constants.USER_ID).in(userIds));
        logger.info("query: {}", query);
        return runQuery(query, LeadSquaredAgent.class, includeSet);
    }

    public List<LeadSquaredAgent> getAgentByEmailIds(List<String> emailIds) {
        Query query = new Query();
        query.addCriteria(Criteria.where(LeadSquaredAgent.Constants.EMAIL_ADDRESS).in(emailIds));
        logger.info("query: {}", query);
        return runQuery(query, LeadSquaredAgent.class);
    }

    public void addAgentsEmailForIntermittentScreen(LeadSquaredAgent leadSquaredAgent) throws BadRequestException {
        try {
            Assert.notNull(leadSquaredAgent);
            saveEntity(leadSquaredAgent);
        } catch (Exception ex) {
            throw new RuntimeException("LeadSquaredAgents Info : Error while by adding LeadSquaredAgent details " + ex);
        }
    }

    public List<LeadSquaredAgent> getIntermittentScreenAllowedAgent(String emailId) {
        Query query = new Query();
        Criteria andCriteria = new Criteria();
        andCriteria.andOperator(Criteria.where(LeadSquaredAgent.Constants.EMAIL_ADDRESS).is(emailId),
                Criteria.where(LeadSquaredAgent.Constants.IS_INTERMITTENT_SCREEN_ALLOWED).is(true));
        query.addCriteria(andCriteria);
        logger.info("query: {}", query);
        return runQuery(query, LeadSquaredAgent.class);
    }
}
