package com.vedantu.vedantudata.dao;

import com.vedantu.util.LogFactory;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import com.vedantu.vedantudata.dao.serializers.MongoClientFactory;
import com.vedantu.vedantudata.entities.leadsquared.LeadSquaredAgent;
import com.vedantu.vedantudata.request.leadsquared.SalesWaveSessionJoinedLSRequest;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

@Service
public class SalesWaveSessionJoinedLSRequestDao extends AbstractMongoDAO {


    @Autowired
    private MongoClientFactory mongoClientFactory;

    @Autowired
    private LogFactory logFactory;

    private final Logger logger = logFactory.getLogger(SalesWaveSessionJoinedLSRequestDao.class);

    static final String collectionName = "SalesWaveSessionJoinedLSRequest";

    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }

    public void saveSalesWaveSessionJoinedLSRequest(SalesWaveSessionJoinedLSRequest salesWaveSessionJoinedLSRequest) {

        if (Objects.nonNull(salesWaveSessionJoinedLSRequest)) {
            saveEntity(salesWaveSessionJoinedLSRequest);
        }


    }

    public List<SalesWaveSessionJoinedLSRequest> findSalesWaveSessionDataBySessionId(String sessionId) {
        Query query = new Query();
        query.addCriteria(Criteria.where("sessionId").is(sessionId));
        logger.info("query: {}", query);
        return runQuery(query,SalesWaveSessionJoinedLSRequest.class);
    }


}
