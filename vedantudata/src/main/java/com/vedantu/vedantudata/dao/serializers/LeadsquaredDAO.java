package com.vedantu.vedantudata.dao.serializers;

import com.vedantu.util.dbentities.mongo.AbstractMongoEntity;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import com.vedantu.vedantudata.entities.LeadVcCode;
import com.vedantu.vedantudata.entities.leadsquared.LeadActivity;
import com.vedantu.vedantudata.entities.leadsquared.LeadDetails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;

@Service
public class LeadsquaredDAO extends AbstractMongoDAO {
	@Autowired
	private MongoClientFactory mongoClientFactory;

	public LeadsquaredDAO() {
		super();
	}

	@Override
	protected MongoOperations getMongoOperations() {
		return mongoClientFactory.getMongoOperations();
	}

	public void createLead(LeadDetails p) throws Exception {
		if (p != null) {
			saveEntity(p);
		}
	}

	public void createLeadActivity(LeadActivity p) throws Exception {
		if (p != null) {
			saveEntity(p);
		}
	}
	public void createVcCode(LeadVcCode p) throws Exception{
		if (p != null) {
			saveEntity(p);
		}
	}

	public LeadDetails getLeadById(String id) {
		LeadDetails p = null;
		try {
			if (!StringUtils.isEmpty(id)) {
				p = (LeadDetails) getEntityById(id, LeadDetails.class);
			}
		} catch (Exception ex) {
			// log Exception
			p = null;
		}
		return p;
	}

	public LeadActivity getLeadActivityById(String id) {
		LeadActivity p = null;
		try {
			if (!StringUtils.isEmpty(id)) {
				p = (LeadActivity) getEntityById(id, LeadActivity.class);
			}
		} catch (Exception ex) {
			// log Exception
			p = null;
		}
		return p;
	}

	@Override
	public <T extends AbstractMongoEntity> List<T> runQuery(Query query, Class<T> clazz) {
		return getMongoOperations().find(query, clazz, clazz.getSimpleName());
	}
}
