package com.vedantu.vedantudata.dao.serializers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.vedantu.util.dbutils.AbstractMongoDAO;
import com.vedantu.vedantudata.entities.UserEventData;

@Service
public class UserEventDAO extends AbstractMongoDAO {

	@Autowired
	private MongoClientFactory mongoClientFactory;

	public UserEventDAO() {
		super();
	}

	@Override
	protected MongoOperations getMongoOperations() {
		return mongoClientFactory.getMongoOperations();
	}

	public void create(UserEventData p) {
		if (p != null) {
			saveEntity(p);
		}
	}

	public UserEventData getById(String id) {
		UserEventData p = null;
		try {
			if (!StringUtils.isEmpty(id)) {
				p = (UserEventData) getEntityById(id, UserEventData.class);
			}
		} catch (Exception ex) {
			// log Exception
			p = null;
		}
		return p;
	}

	public void save(UserEventData p) {
		if (p != null) {
			saveEntity(p);
		}
	}

	public int deleteById(String id) {
		int result = 0;
		try {
			result = deleteEntityById(id, UserEventData.class);
		} catch (Exception ex) {
			// throw Exception;
		}

		return result;
	}

}
