package com.vedantu.vedantudata.dao.postgresRO;

import com.vedantu.util.LogFactory;
import com.vedantu.vedantudata.config.HibernateConfig;
import com.vedantu.vedantudata.enums.salesconversion.SalesTeam;
import com.vedantu.vedantudata.pojos.CustomLeadSquaredAgent;
import org.apache.logging.log4j.Logger;
import org.hibernate.*;
import org.hibernate.transform.Transformers;
import org.hibernate.type.StandardBasicTypes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class CustomLeadSquaredAgentDao {

    @Autowired
    private HibernateConfig hibernateConfig;

    @Autowired
    private LogFactory logFactory;

    private final Logger logger = logFactory.getLogger(CustomLeadSquaredAgentDao.class);


    public List<CustomLeadSquaredAgent> getLeadSquaredAgentsFromDB(StringBuilder sb, SalesTeam salesTeam) {

        Session session = hibernateConfig.getSessionFactoryUtils().getCurrentSession();
        List agents = null;
        Transaction tx = session.beginTransaction();
        try {
            SQLQuery query = session.createSQLQuery(sb.toString());
            if(salesTeam.equals(SalesTeam.FOS))
            {
                logger.info("For FOS query adding l4-->");
               query= query.addScalar("l5userid", StandardBasicTypes.STRING)
                    .addScalar("l5designation", StandardBasicTypes.STRING);
            }
                   agents= query.addScalar("createdon", StandardBasicTypes.STRING)
                    .addScalar("userid", StandardBasicTypes.STRING)
                    .addScalar("manageruserid", StandardBasicTypes.STRING)
                    .addScalar("l1userid", StandardBasicTypes.STRING)
                    .addScalar("l1designation", StandardBasicTypes.STRING)
                    .addScalar("l2userid", StandardBasicTypes.STRING)
                    .addScalar("l2designation", StandardBasicTypes.STRING)
                    .addScalar("l3userid", StandardBasicTypes.STRING)
                    .addScalar("l3designation", StandardBasicTypes.STRING)
                    .addScalar("l4userid", StandardBasicTypes.STRING)
                    .addScalar("l4designation", StandardBasicTypes.STRING)
                    .addScalar("isemailsender", StandardBasicTypes.INTEGER)
                    .addScalar("usertype", StandardBasicTypes.INTEGER)
                    .addScalar("isphonecallagent", StandardBasicTypes.INTEGER)
                    .addScalar("modifyallleadsofgroup", StandardBasicTypes.INTEGER)
                    .addScalar("viewallleadsofgroup", StandardBasicTypes.INTEGER)
                    .addScalar("isagencyuser", StandardBasicTypes.INTEGER)
                    .addScalar("isadministrator", StandardBasicTypes.INTEGER)
                    .addScalar("isbillinguser", StandardBasicTypes.INTEGER)
                    .addScalar("ischeckedin", StandardBasicTypes.INTEGER)
                    .addScalar("ischeckinenabled", StandardBasicTypes.INTEGER)
                    .addScalar("autouserid", StandardBasicTypes.INTEGER)
                    .addScalar("isdefaultowner", StandardBasicTypes.INTEGER)
                    .addScalar("statuscode", StandardBasicTypes.INTEGER)
                    .addScalar("statusreason", StandardBasicTypes.INTEGER)
                    .addScalar("deletionstatuscode", StandardBasicTypes.INTEGER)
                    .addScalar("mx_custom_1", StandardBasicTypes.STRING)
                    .addScalar("mx_custom_2", StandardBasicTypes.STRING)
                    .addScalar("mx_custom_3", StandardBasicTypes.STRING)
                    .addScalar("mx_custom_4", StandardBasicTypes.STRING)
                    .addScalar("mx_custom_5", StandardBasicTypes.STRING)
                    .addScalar("mx_custom_6", StandardBasicTypes.STRING)
                    .addScalar("mx_custom_7", StandardBasicTypes.STRING)
                    .addScalar("mx_custom_8", StandardBasicTypes.STRING)
                    .addScalar("mx_custom_9", StandardBasicTypes.STRING)
                    .addScalar("mx_custom_10", StandardBasicTypes.STRING)
                    .addScalar("modifiedon", StandardBasicTypes.STRING)
                    .addScalar("lastcheckedon", StandardBasicTypes.STRING)
                    .addScalar("lastname", StandardBasicTypes.STRING)
                    .addScalar("middlename", StandardBasicTypes.STRING)
                    .addScalar("emailaddress", StandardBasicTypes.STRING)
                    .addScalar("firstname", StandardBasicTypes.STRING)
                    .addScalar("password", StandardBasicTypes.STRING)
                    .addScalar("authtoken", StandardBasicTypes.STRING)
                    .addScalar("tmp_forgotpassword", StandardBasicTypes.STRING)
                    .addScalar("sessionid", StandardBasicTypes.STRING)
                    .addScalar("timezone", StandardBasicTypes.STRING)
                    .addScalar("dateformat", StandardBasicTypes.STRING)
                    .addScalar("signature_html", StandardBasicTypes.STRING)
                    .addScalar("signature_text", StandardBasicTypes.STRING)
                    .addScalar("associatedphonenumbers", StandardBasicTypes.STRING)
                    .addScalar("phonemain", StandardBasicTypes.STRING)
                    .addScalar("phonemobile", StandardBasicTypes.STRING)
                    .addScalar("phoneothers", StandardBasicTypes.STRING)
                    .addScalar("createdby", StandardBasicTypes.STRING)
                    .addScalar("modifiedby", StandardBasicTypes.STRING)
                    .addScalar("role", StandardBasicTypes.STRING)
                    .addScalar("checkincheckouthistoryid", StandardBasicTypes.STRING)
                    .addScalar("lastcheckedipaddress", StandardBasicTypes.STRING)
                    .addScalar("teamid", StandardBasicTypes.STRING)
                    .addScalar("holidaycalendarid", StandardBasicTypes.STRING)
                    .addScalar("workdaytemplateid", StandardBasicTypes.STRING)
                    .addScalar("telephonyagentid", StandardBasicTypes.STRING)
                    .addScalar("zipcode", StandardBasicTypes.STRING)
                    .addScalar("country", StandardBasicTypes.STRING)
                    .addScalar("state", StandardBasicTypes.STRING)
                    .addScalar("city", StandardBasicTypes.STRING)
                    .addScalar("address", StandardBasicTypes.STRING)
                    .addScalar("officelocationname", StandardBasicTypes.STRING)
                    .addScalar("availabilitystatus", StandardBasicTypes.STRING)
                    .addScalar("skills", StandardBasicTypes.STRING)
                    .addScalar("salesregions", StandardBasicTypes.STRING)
                    .addScalar("department", StandardBasicTypes.STRING)
                    .addScalar("team", StandardBasicTypes.STRING)
                    .addScalar("designation", StandardBasicTypes.STRING)
                    .addScalar("photourl", StandardBasicTypes.STRING)
                    .addScalar("groups", StandardBasicTypes.STRING)
                    .setResultTransformer(Transformers.aliasToBean(CustomLeadSquaredAgent.class)).list();
            logger.info(agents);
        } catch (Exception e) {
            tx.rollback();
            session.close();
            logger.error("Unable to fetch query results" + e);
        }
        tx.commit();
        session.close();
        return agents;

    }
}
