package com.vedantu.vedantudata.dao.serializers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.vedantu.util.dbutils.AbstractMongoDAO;
import com.vedantu.vedantudata.entities.UserEmail;

@Service
public class UserEmailDAO extends AbstractMongoDAO {

	@Autowired
	private MongoClientFactory mongoClientFactory;

	public UserEmailDAO() {
		super();
	}

	@Override
	protected MongoOperations getMongoOperations() {
		return mongoClientFactory.getMongoOperations();
	}

	public void create(UserEmail p) throws Exception {
		if (p != null) {
			saveEntity(p);
		}
	}

	public UserEmail getById(String id) {
		UserEmail p = null;
		try {
			if (!StringUtils.isEmpty(id)) {
				p = (UserEmail) getEntityById(id, UserEmail.class);
			}
		} catch (Exception ex) {
			// log Exception
			p = null;
		}
		return p;
	}

	public void save(UserEmail p) throws Exception {
		if (p != null) {
			saveEntity(p);
		}
	}

	public int deleteById(String id) {
		int result = 0;
		try {
			result = deleteEntityById(id, UserEmail.class);
		} catch (Exception ex) {
			// throw Exception;
		}

		return result;
	}

}
