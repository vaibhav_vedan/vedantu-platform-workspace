package com.vedantu.vedantudata.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TimeZone;
import java.util.UUID;

import com.vedantu.util.CollectionUtils;
import com.vedantu.util.DateTimeUtils;
import com.vedantu.util.StringUtils;

public class CommonUtils {

	public static final int MILLIS_PER_SECOND = 1000;
	public static final int SECONDS_PER_MINUTE = 60;
	public static final int MINUTES_PER_HOUR = 60;
	public static final int HOURS_PER_DAY = 24;

	public static final int MILLIS_PER_MINUTE = MILLIS_PER_SECOND * SECONDS_PER_MINUTE;
	public static final int MILLIS_PER_HOUR = MILLIS_PER_MINUTE * MINUTES_PER_HOUR;
	public static final int MILLIS_PER_DAY = MILLIS_PER_HOUR * HOURS_PER_DAY;

	public static final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

	public static int getMillisPerDay() {
		return MILLIS_PER_DAY;
	}

	public static String calculateTimeStampIst(Long time) {
		if (time == null) {
			return null;
		}

		sdf.setTimeZone(TimeZone.getTimeZone(DateTimeUtils.TIME_ZONE_IN));
		return sdf.format(new Date(time));
	}

	public static String calculateTimeStampGMT(Long time) {
		if (time == null) {
			return null;
		}

		sdf.setTimeZone(TimeZone.getTimeZone(DateTimeUtils.TIME_ZONE_GMT));
		return sdf.format(new Date(time));
	}

	public static long parseTime(String timeStamp) throws ParseException {
		sdf.setTimeZone(TimeZone.getTimeZone(DateTimeUtils.TIME_ZONE_GMT));
		return sdf.parse(timeStamp).getTime();
	}

	public static long parseTimeIST(String timeStamp) throws ParseException {
		sdf.setTimeZone(TimeZone.getTimeZone(DateTimeUtils.TIME_ZONE_IN));
		return sdf.parse(timeStamp).getTime();
	}

	public static String getListString(List<String> values) {
		// String join is introduced in JDK8. Upgrade in case of error.
		return String.join(", ", values);
	}

	// Return true if timeStamp1 <= timeStamp2
	public static boolean compareTimeStamps(String timeStamp1, String timeStamp2) throws ParseException {
		// 2016-09-14 00:03:24.000

		if (!StringUtils.isEmpty(timeStamp1) && !StringUtils.isEmpty(timeStamp2)) {
			Date date1 = sdf.parse(timeStamp1);
			Date date2 = sdf.parse(timeStamp2);
			if (date1.getTime() <= date2.getTime()) {
				return true;
			}
		}

		return false;
	}

	public static Set<String> getAllElementSet(String listStr, String delimeter) {
		Set<String> results = new HashSet<>();
		if (!StringUtils.isEmpty(listStr)) {
			String[] elements = listStr.split(delimeter);
			if (elements != null && elements.length > 0) {
				for (String element : elements) {
					if (!StringUtils.isEmpty(element)) {
						results.add(element);
					}
				}
			}
		}

		return results;
	}

	public static Set<String> trimAllElements(Set<String> elements) {
		if (CollectionUtils.isEmpty(elements)) {
			return elements;
		}

		Set<String> results = new HashSet<>();
		for (String element : elements) {
			results.add(element.trim());
		}

		return results;
	}

	public static String generateRandomUUID() {
		UUID uuid = UUID.randomUUID();
		return uuid.toString();
	}

	public static String getDurationString(long durationInSeconds) {
		String durationStr = "";
		if (durationInSeconds < 0) {
			durationStr += "(-)";
			durationInSeconds *= -1;
		}
		durationStr += (durationInSeconds / 60) + ":" + (durationInSeconds % 60);
		return durationStr;
	}
	public static Long getDaysDiffernceFromMillis(Long time1,Long time2)
	{
		return (time2-time1)/MILLIS_PER_DAY;

	}
}
