package com.vedantu.vedantudata.utils;

import com.google.gson.Gson;
import com.vedantu.exception.VException;
import com.vedantu.util.*;
import com.vedantu.util.enums.SQSMessageType;
import com.vedantu.util.enums.SQSQueue;
import com.vedantu.vedantudata.dao.ActivityDao;
import com.vedantu.vedantudata.entities.ActivityDetails;
import com.vedantu.vedantudata.managers.leadsquared.LeadsquaredActivityManager;
import com.vedantu.vedantudata.pojos.ActivityDetailsPojo;
import com.vedantu.vedantudata.pojos.ActivityResponseMessage;
import com.vedantu.vedantudata.pojos.ActivityresponsepayloadPojo;
import com.vedantu.vedantudata.pojos.LeadsquaredActivityField;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.*;

@Service
public class LeadsquaredActivityHelper {

    public static List<ActivityDetails> activityDetailsStaticList = new ArrayList<>();
    public static List<ActivityDetails> updateActivityDetailsList = new ArrayList<>();

    public static Integer AM_LIMIT = 0;

    @Autowired
    private AwsSQSManager awsSQSManager;

    @Autowired
    private ActivityDao activityDao;

    @Autowired
    private LeadsquaredActivityManager leadsquaredActivityManager;

    private final Gson gson = new Gson();

    @Autowired
    private LogFactory logFactory;

    private Logger logger = logFactory.getLogger(LeadsquaredActivityHelper.class);

    public static String getLeadSquaredDate(Long date) {
        // Lead squared date format : "yyyy-MM-dd HH:mm:ss" (This is the only
        // date form Lead squared accepts)
        if (date == null || date <= 0) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        sdf.setTimeZone(TimeZone.getTimeZone(DateTimeUtils.TIME_ZONE_GMT));
        return sdf.format(new Date(date));
    }

    public static List<LeadsquaredActivityField> getSchemaValuePair(List<String> values) {
        List<LeadsquaredActivityField> fields = new ArrayList<>();
        if(ArrayUtils.isNotEmpty(values)){
            int i =1;
            for(String value : values){
                if(value != null){
                    fields.add(new LeadsquaredActivityField("mx_Custom_"+i,value));
                }
                i++;
            }
        }
        return fields;
    }

    public void queueActivityToPush(ActivityDetails activityDetails) {

        activityDetailsStaticList.add(activityDetails);

        logger.info("ACTIVITY DETAIL : " + activityDetails);
        if (activityDetailsStaticList.size() >= ActivityEventConstants.LS_ACTIVITY_CHUNK_SIZE) {

            logger.info("======activity list is full :: push it to queue:");
            triggerAllActivities();
        }
    }

    public void triggerAllActivities() {

        if (activityDetailsStaticList.isEmpty()) {
            return;
        }

        logger.info("======Pushing the activities list to QUEUE:: LS_PUSH_ACTIVITY_TRIGGER");
       awsSQSManager.sendToSQS(SQSQueue.VEDANTU_DATA_LEADSQUARED_QUEUE,
                SQSMessageType.LS_PUSH_ACTIVITY_TRIGGER, new Gson().toJson(activityDetailsStaticList), SQSMessageType.LS_PUSH_ACTIVITY_TRIGGER.name());

       activityDetailsStaticList.clear();
    }

    private void triggerNow() throws VException, InterruptedException {
        logger.info("======Recieved the request for trigger activities");

        List<ActivityDetailsPojo> activityDetailsMobilePojos = new ArrayList<>();
        List<ActivityDetails> activityDetailsMobileStaticListSub  = new ArrayList<>();

        List<ActivityDetailsPojo> activityDetailsEmailPojos = new ArrayList<>();
        List<ActivityDetails> activityDetailsEmailStaticListSub  = new ArrayList<>();

        for (ActivityDetails details : activityDetailsStaticList) {
            if ((ActivityEventConstants.EVENT_GRIEVANCE == details.getActivityEvent() || ActivityEventConstants.EVENT_OPEN_TICKETS == details.getActivityEvent())
                    && activityDao.checkForFreshdeskActivity(details.getLeadVedantuId(), details.getActivityEvent(), details.getFreshdeskTicketId())) {
                continue;
            }

            ActivityDetailsPojo activityDetailsPojo = new Gson().fromJson(new Gson().toJson(details), ActivityDetailsPojo.class);

            if(StringUtils.isNotEmpty(details.getEmailAddress())) {
                activityDetailsPojo.setSearchByValue(details.getEmailAddress());
                activityDetailsEmailPojos.add(activityDetailsPojo);
                activityDetailsEmailStaticListSub.add(details);
            }else if(StringUtils.isNotEmpty(details.getPhone())){
                activityDetailsPojo.setSearchByValue(details.getPhone());
                activityDetailsMobilePojos.add(activityDetailsPojo);
                activityDetailsMobileStaticListSub.add(details);
            }

        }

        List<ActivityDetails> activityDetailsList = new ArrayList<>();

        if(!activityDetailsEmailPojos.isEmpty()){
            logger.info("======triggering activities search by email");
            ActivityresponsepayloadPojo activityEmailresponsepayloadPojo = leadsquaredActivityManager.pushActivitiesByEmail(activityDetailsEmailPojos);
            manageResponsePayload(activityEmailresponsepayloadPojo, activityDetailsList, activityDetailsEmailStaticListSub);
        }else{
            logger.info("======Ignoring activities search by email since the list is empty");
        }

        logger.info("======persisting email activities");
        activityDao.creatActivities(activityDetailsList);

        activityDetailsList = new ArrayList<>();

        if(!activityDetailsMobilePojos.isEmpty()){
            logger.info("======triggering activities search by mobile");
            ActivityresponsepayloadPojo activityMobileresponsepayloadPojo = leadsquaredActivityManager.pushActivitiesByMobile(activityDetailsMobilePojos);
            manageResponsePayload(activityMobileresponsepayloadPojo, activityDetailsList, activityDetailsMobileStaticListSub);
        }else{
            logger.info("======Ignoring activities search by phone since the list is empty");
        }

        logger.info("======persisting phone activities");
        activityDao.creatActivities(activityDetailsList);
    }

    private void manageResponsePayload(ActivityresponsepayloadPojo activityResponsepayloadPojo, List<ActivityDetails> activityDetailsList, List<ActivityDetails> activityDetailsStaticListSub) {

        if (activityResponsepayloadPojo == null)
            return;

        logger.info("\n\nLS Response ::: {}", activityResponsepayloadPojo.toString());
        List<ActivityResponseMessage> responseMobileMessages = activityResponsepayloadPojo.getResponse();
        for (int i = 0; i < responseMobileMessages.size(); i++) {
            ActivityResponseMessage activityResponseMessage = responseMobileMessages.get(i);
            if (activityResponseMessage.isActivityCreated()) {
                String prospectActivityId = activityResponseMessage.getProspectActivityId();
                ActivityDetails activity = activityDetailsStaticListSub.get(i);
                activity.setProspectActivityId(prospectActivityId);
                activityDetailsList.add(activity);
            }
        }
    }
    public void triggerActivity(ActivityDetails details) {

        logger.info("ACTIVITY DETAIL : " + details);
        logger.info("TRIGGER DIRECT " );

        logger.info("======Pushing the activities list to QUEUE:: LS_PUSH_ACTIVITY_TRIGGER");
        awsSQSManager.sendToSQS(SQSQueue.VEDANTU_DATA_LEADSQUARED_QUEUE,
                SQSMessageType.LS_PUSH_ACTIVITY_TRIGGER, new Gson().toJson(Collections.singletonList(details)), SQSMessageType.LS_PUSH_ACTIVITY_TRIGGER.name());

    }

    public void addUpdateActivityToQueue(ActivityDetails details) {
//        updateActivityDetailsList.add(details);
//        if (updateActivityDetailsList.size() >= ActivityEventConstants.LS_ACTIVITY_CHUNK_SIZE) {
        logger.info("======Pushing the activity  to QUEUE:: LS_PUSH_UPDATE_ACTIVITY_TRIGGER");
        awsSQSManager.sendToSQS(SQSQueue.VEDANTU_DATA_LEADSQUARED_QUEUE,
                SQSMessageType.LS_PUSH_UPDATE_ACTIVITY_TRIGGER, new Gson().toJson(Collections.singletonList(details)), SQSMessageType.LS_PUSH_UPDATE_ACTIVITY_TRIGGER.name());
//        }
//        updateActivityDetailsList.clear();
    }

    public void pushToQueueForpersist(List<Map<String, Object>> tickets) {

        logger.info("======Pushing the activity  to QUEUE:: FD_PERSIST_DATA_IN_RS");
        awsSQSManager.sendToSQS(SQSQueue.VEDANTU_DATA_LEADSQUARED_QUEUE,
                SQSMessageType.FD_PERSIST_DATA_IN_RS, new Gson().toJson(tickets), SQSMessageType.FD_PERSIST_DATA_IN_RS.name());
    }

    public void freshDeskFetchRecordsTaskPushToQueue() {

        logger.info("======Pushing the activity  to QUEUE:: FD_PERSIST_DATA_IN_RS");
        awsSQSManager.sendToSQS(SQSQueue.VEDANTU_DATA_LEADSQUARED_QUEUE,
                SQSMessageType.FRESHDESK_FETCH_LAST_UPDATED_RECORDS, "{}", SQSMessageType.FRESHDESK_FETCH_LAST_UPDATED_RECORDS.name());
    }


}
