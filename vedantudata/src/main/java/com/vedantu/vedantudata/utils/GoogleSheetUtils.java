package com.vedantu.vedantudata.utils;

import java.util.ArrayList;
import java.util.List;

import com.google.api.services.sheets.v4.model.CellData;
import com.google.api.services.sheets.v4.model.ExtendedValue;
import com.google.api.services.sheets.v4.model.RowData;

public class GoogleSheetUtils {

	// Row number is excluding the header
	public static String getRangeIdentifier(int rowNumber) {
		return String.valueOf((char) ('A' + (rowNumber + 1)));
	}

	public static RowData getRowData(String[] values) {
		RowData rowData = new RowData();
		List<CellData> cellDataList = new ArrayList<>();
		for (String value : values) {
			CellData cellData = new CellData();
			ExtendedValue extendedValue = new ExtendedValue();
			extendedValue.setStringValue(value);
			cellData.setUserEnteredValue(extendedValue);
			cellDataList.add(cellData);
		}
		rowData.setValues(cellDataList);
		return rowData;
	}
}
