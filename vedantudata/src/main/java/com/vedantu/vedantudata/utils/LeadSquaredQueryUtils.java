package com.vedantu.vedantudata.utils;

import com.vedantu.vedantudata.enums.salesconversion.SalesTeam;
import org.apache.commons.beanutils.BeanUtils;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.stereotype.Service;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;

@Service
public class LeadSquaredQueryUtils {

    public StringBuilder getLeadSquaredAgentHierarichalDataQuery(SalesTeam salesTeam)
    {
        switch(salesTeam) {
            case FOS:
            StringBuilder sb = new StringBuilder();
            sb.append("select ls.*,tl.userid as l1userid,\n" +
                    "tl.designation as l1designation,stl.userid as l2userid,"
                    + "stl.designation as l2designation,ch.userid as l3userid,\n" +
                    "ch.designation as l3designation,rm.userid as l4userid,\n" +
                    "rm.designation as l4designation,\n" +
                    "zm.userid as l5userid,\n" +
                    "zm.designation as l5designation\n" +
                    "from leadsquared.user_base ls \n" +
                    "left join\n" +
                    "leadsquared.user_base tl on tl.userid=ls.manageruserid\n" +
                    "left join\n" +
                    "leadsquared.user_base stl on stl.userid=tl.manageruserid\n" +
                    "left join\n" +
                    "leadsquared.user_base ch on ch.userid=stl.manageruserid \n" +
                    "left join\n" +
                    "leadsquared.user_base rm on rm.userid=ch.manageruserid \n" +
                    "left join\n" +
                    "leadsquared.user_base zm on zm.userid=rm.manageruserid\n" +
                    "where ls.department in ('FOS') \n" +
                    "and ((ls.mx_custom_8 IS NOT NULL and ls.statuscode=1) or ls.statuscode=0)");
            return sb;
            case IS:
                 sb = new StringBuilder();
                sb.append("select ls.*,tl.userid as l1userid,\n" +
                		  "tl.designation as l1designation,stl.userid as l2userid,"
                          + "stl.designation as l2designation,ch.userid as l3userid,\n" +
                          "ch.designation as l3designation,rm.userid as l4userid,\n" +
                          "rm.designation as l4designation \n" +
                        "from leadsquared.user_base ls \n" +
                        "left join\n" +
                        "leadsquared.user_base tl on tl.userid=ls.manageruserid\n" +
                        "left join\n" +
                        "leadsquared.user_base stl on stl.userid=tl.manageruserid\n" +
                        "left join\n" +
                        "leadsquared.user_base ch on ch.userid=stl.manageruserid \n" +
                        "left join\n" +
                        "leadsquared.user_base rm on rm.userid=ch.manageruserid \n" +
                        "where ls.department in ('IS') \n" +
                        "and ((ls.mx_custom_8 IS NOT NULL and ls.statuscode=1) or ls.statuscode=0)");
                return sb;
        }
        return null;
    }


}
