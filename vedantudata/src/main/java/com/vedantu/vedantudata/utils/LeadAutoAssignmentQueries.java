package com.vedantu.vedantudata.utils;

import com.vedantu.util.ConfigUtils;


public class LeadAutoAssignmentQueries {


    public static final String queryToFetchPulledBackLeads = "select pb.grade,pb.phone,pb.prospectstage,pb.firstName,'' as lead_score,pb.prospectid,'' as lead_language,'' as assignment_type, '' as mx_core_nonCore,'' as scoreid,ub.designation,case when prospectstage in ('1a. Fresh Lead', '1b. Fresh Signup') then TRUE else FALSE end as isFreshLead,ub.manageruserid\n" +
            "from leadsquared.prospect_base pb\n" +
            "    inner join leadsquared.user_base ub on ub.userid = pb.ownerid and\n" +
            "        ub.statuscode = 1 and ub.department = 'FOS' and ub.designation in ('ACADEMIC_COUNSELLOR','SENIOR_ACADEMIC_COUNSELLOR','TELE_CALLER', 'INTERN','TEAM_LEADER','SENIOR_TEAM_LEADER','CENTER_HEAD') and pb.phone is not null and pb.phone <> ''";
    //Gets grades of all the Counsellors in System
    public static final String queryToGetAllGrades = "with w1 as (\n" +
            "    with base as (\n" +
            "        select userid, emailaddress, manageruserid, c.center_code, c.default_language, ub.department\n" +
            "        from leadsquared.user_base ub\n" +
            "                 left join leadsquared.centreattributes c\n" +
            "                           on c.center_name = ub.salesregions\n" +
            "        where department = 'FOS'\n" +
            "          and statuscode = 0\n" +
            "          and designation in ('ACADEMIC_COUNSELLOR', 'SENIOR_ACADEMIC_COUNSELLOR', 'TELE_CALLER', 'INTERN')),\n" +
            "         base1 as (\n" +
            "             select userid,\n" +
            "                    emailaddress,\n" +
            "                    case\n" +
            "                        when (language_preference_1 is null and language_preference_2 is null) then b.default_language\n" +
            "                        else language_preference_1 end                               as language_preference_1,\n" +
            "                    language_preference_2,\n" +
            "                    center_code,\n" +
            "                    manageruserid,\n" +
            "                    case when required_leads is null then 25 else required_leads end as required_leads,\n" +
            "                    case\n" +
            "                        when (grade_preference_1 is null and grade_preference_2 is null and grade_preference_3 is null)\n" +
            "                            then '12'\n" +
            "                        else grade_preference_1 end                                  as grade_preference_1,\n" +
            "                    case\n" +
            "                        when (grade_preference_1 is null and grade_preference_2 is null and grade_preference_3 is null)\n" +
            "                            then '6'\n" +
            "                        else grade_preference_2 end                                  as grade_preference_2,\n" +
            "                    grade_preference_3,\n" +
            "                    lead_type_preference_1,\n" +
            "                    lead_type_preference_2,\n" +
            "                    lead_type_preference_3,\n" +
            "                    cp.counsellor_emailid\n" +
            "             from base b\n" +
            "                      left join leadsquared.counsellor_preferences cp\n" +
            "                                on cp.counsellor_emailid = b.emailaddress\n" +
            "                      left join leadsquared.daywise_requirement dr\n" +
            "                                on (b.department = dr.team_type and cp.cohort_july = dr.cohort and\n" +
            "                                    extract(day from current_date) = dr.dayofthemonth))\n" +
            "    select *\n" +
            "    from base1\n" +
            ")\n" +
            "select distinct grade_preference_1 from w1 union select distinct grade_preference_2 from w1 union select distinct grade_preference_3 from w1";

    //Gets grades of all the Counsellors in System
    public static final String queryToGetAllLanguages = "with w1 as (\n" +
            "    with base as (\n" +
            "        select userid, emailaddress, manageruserid, c.center_code, c.default_language, ub.department\n" +
            "        from leadsquared.user_base ub\n" +
            "                 left join leadsquared.centreattributes c\n" +
            "                           on c.center_name = ub.salesregions\n" +
            "        where department = 'FOS'\n" +
            "          and statuscode = 0\n" +
            "          and designation in ('ACADEMIC_COUNSELLOR', 'SENIOR_ACADEMIC_COUNSELLOR', 'TELE_CALLER', 'INTERN')),\n" +
            "         base1 as (\n" +
            "             select userid,\n" +
            "                    emailaddress,\n" +
            "                    case\n" +
            "                        when (language_preference_1 is null and language_preference_2 is null) then b.default_language\n" +
            "                        else language_preference_1 end                               as language_preference_1,\n" +
            "                    language_preference_2,\n" +
            "                    center_code,\n" +
            "                    manageruserid,\n" +
            "                    case when required_leads is null then 25 else required_leads end as required_leads,\n" +
            "                    case\n" +
            "                        when (grade_preference_1 is null and grade_preference_2 is null and grade_preference_3 is null)\n" +
            "                            then '12'\n" +
            "                        else grade_preference_1 end                                  as grade_preference_1,\n" +
            "                    case\n" +
            "                        when (grade_preference_1 is null and grade_preference_2 is null and grade_preference_3 is null)\n" +
            "                            then '6'\n" +
            "                        else grade_preference_2 end                                  as grade_preference_2,\n" +
            "                    grade_preference_3,\n" +
            "                    lead_type_preference_1,\n" +
            "                    lead_type_preference_2,\n" +
            "                    lead_type_preference_3,\n" +
            "                    cp.counsellor_emailid\n" +
            "             from base b\n" +
            "                      left join leadsquared.counsellor_preferences cp\n" +
            "                                on cp.counsellor_emailid = b.emailaddress\n" +
            "                      left join leadsquared.daywise_requirement dr\n" +
            "                                on (b.department = dr.team_type and cp.cohort_july = dr.cohort and\n" +
            "                                    extract(day from current_date) = dr.dayofthemonth))\n" +
            "    select *\n" +
            "    from base1\n" +
            ")\n" +
            "select distinct language_preference_1 from w1 union select distinct language_preference_2 from w1;";

    //To get counsellor details, and count of fresh lead in bucket
    public static final String queryToFetchCounsellors = "with base as (\n" +
            "    select userid, emailaddress, manageruserid, c.center_code, c.default_language, ub.department\n" +
            "    from leadsquared.user_base ub\n" +
            "             left join leadsquared.centreattributes c\n" +
            "                       on c.center_name = ub.salesregions\n" +
            "    where department = 'FOS'\n" +
            "and statuscode = 0\n" +
            "and designation in ('ACADEMIC_COUNSELLOR','SENIOR_ACADEMIC_COUNSELLOR','TELE_CALLER','INTERN')),\n" +
            "     base1 as (\n" +
            "         select userid,\n" +
            "                emailaddress,\n" +
            "                case\n" +
            "                    when ((language_preference_1 is null or language_preference_1='') and (language_preference_2 is null or language_preference_2 = '')) then b.default_language\n" +
            "                    else language_preference_1 end                               as language_preference_1,\n" +
            "                language_preference_2,\n" +
            "                center_code,default_language,\n" +
            "                manageruserid,\n" +
            "                case when (required_leads is null or required_leads='0') then 21 else required_leads end as required_leads,\n" +
            "                case\n" +
            "                    when (grade_preference_1 is null and grade_preference_2 is null and grade_preference_3 is null)\n" +
            "                        then '6'\n" +
            "                    else grade_preference_1 end                                  as grade_preference_1,\n" +
            "                case\n" +
            "                    when (grade_preference_1 is null and grade_preference_2 is null and grade_preference_3 is null)\n" +
            "                        then '12'\n" +
            "                    else grade_preference_2 end                                  as grade_preference_2,\n" +
            "                grade_preference_3,\n" +
            "                lead_type_preference_1,\n" +
            "                lead_type_preference_2,\n" +
            "                lead_type_preference_3,location_preference,lower_limit_of_lead_score,upper_limit_of_lead_score,cohort_july,\n" +
            "                cp.counsellor_emailid\n" +
            "         from base b\n" +
            "                  left join leadsquared.counsellor_preferences cp\n" +
            "                            on cp.counsellor_emailid = b.emailaddress\n" +
            "                  left join leadsquared.daywise_requirement dr\n" +
            "                            on (b.department = dr.team_type and cp.cohort_july = dr.cohort and\n" +
            "                                extract(day from convert_timezone('UTC+5:30','UTC', current_date)) = dr.dayofthemonth))\n" +
            "select base1.*,count(pb.ownerid)\n" +
            "from base1\n" +
            "left join leadsquared.prospect_base pb on base1.userid = pb.ownerid\n" +
            "and pb.prospectstage = '1b. Fresh Sign up' \n" +
            "group by base1.userid,base1.location_preference,base1.emailaddress,base1.language_preference_1,base1.language_preference_2,base1.grade_preference_1,base1.grade_preference_2,base1.grade_preference_3,base1.center_code,base1.manageruserid,base1.required_leads,base1.lead_type_preference_1,base1.lead_type_preference_2,base1.lead_type_preference_3,base1.counsellor_emailid,base1.lower_limit_of_lead_score,base1.upper_limit_of_lead_score,base1.cohort_july,base1.default_language";


    private static String adminEmailsString = ConfigUtils.INSTANCE.getStringValue("auto.lead.assignment.admins").replace(",", "','");

    public static final String queryToGetAdminBucketNonStateBoardLeads = " with w1 as (\n"+
            " select lra.prospectid, lra.locationinfo_state,lra.grade,lra.scoreid, lra.lead_score,lra.firstname ,lra.lead_language,lra.studentinfo_board,lra.archived_datetime,lra.assignment_type,pb.prospectstage,pb.phone,case\n"+
            " when lra.qualityflaghqlq = 'HighQuality' then 'HQ'\n"+
            " when (lra.assignment_type is null or lra.assignment_type = '') then 'Non Core'\n"+
            " else 'Core' end as mx_core_nonCore,o.userid,o.subscriptionplanid\n"+
            " from     (select  *, rank() over (partition by prospectid order by archived_datetime desc) as rn\n"+
            "          from   analytics_dna.leadscore_result_archive_master\n"+
            "          where  assignment_type is not null\n"+
            "          and    assignment_type <> ''\n"+
            "          and    lead_score <> 0\n"+
            "          and    trunc(archived_datetime) >= dateadd(day, -15, current_date)\n"+
            "          and    studentinfo_board <> 'state board'\n"+
            "          and    lead_language is not null \n"+
            "          and    lead_language <> '' \n"+
            "          and    lead_language <> 'Other'\n"+
            "          and    grade in ('6', '7', '8', '9', '10', '11', '12','13')\n"+
            "          and    assignment_type not in ('PaidTrial', 'Mod', 'App Non-core'))lra\n"+
            " inner join  leadsquared.prospect_base pb\n"+
            " on          lra.prospectid = pb.prospectid\n"+
            " inner join  leadsquared.user_base ub\n"+
            " on          ub.userid = pb.ownerid\n"+
            " inner join  leadsquared.prospect_extensionbase pe\n"+
            " on          pe.prospectid = pb.prospectid\n"+
            " left join   (select userid, subscriptionplanid\n"+
            "             from public.orders \n"+
            "             where subscriptionplanid is not null\n"+
            "             and paymentstatus <> 'NOT_PAID'\n"+
            "             and amount <> 0) o\n"+
            " on          split_part(o.userid, '_', 2) = pb.mx_user_id\n"+
            " where       lra.prospectid not in ('{}') \n"+
            " and          lra.rn = 1\n"+
            " and         ub.emailaddress in ('admin@vedantu.com','priyanka.m@vedantu.com','simriti.goel@vedantu.com','jayesh.singh@vedantu.com','parth.patel@vedantu.com','nayeeni.manoj@vedantu.com','kanjula.venkata@vedantu.com','ketha.prashanth@vedantu.com','anshul.goswami@vedantu.com')\n"+
            " and         pb.prospectstage not in ('Duplicate Lead','Care-V-Student','FOS Demo Done','Invalid Lead','FOS Invalid Lead','6. TOS Done','6a. Sales Warm','7. Sales Won','Refund','Price Verification Done','Price Verification Not Done','Sales Form Filled','Sales Form Re-Filled','Onboarding','Enrollment Done','Live User','Installment Due','Trial Registration Done')\n"+
            " and         (pb.prospectstage in ('FOS Not Interested', 'Not Interested') and trunc(pe.mx_lastactivitydate) < dateadd(day, -30, current_date) or pb.prospectstage not in ('FOS Not Interested', 'Not Interested'))\n"+
            " and         (pe.mx_sub_stage not in ('No student','>13 Grade','Course Not Available','<6 grade','Not a Student','> 5 Attempts','Teacher Enquiry','5','No student','6','<4 Grade','>5 DNP','DNP 5','FUW > 5 Attempts','Attempt 5') or pe.mx_sub_stage is null)\n"+
            " and         len(right(phone,10)) = 10\n"+
            " and         substring(right(pb.phone,10),1,1) in ('6','7','8','9'))\n"+
            " select *\n"+
            " from w1 \n"+
            " where studentinfo_board = 'icse' and grade not in ('6','7','8') or studentinfo_board <> 'icse' \n"+
            " and subscriptionplanid is null \n"+
            " order by archived_datetime desc, lead_score desc";

    public static final String queryToGetAdminBucketStateBoardLeads = " with w1 as (\n"+
            " select lra.prospectid, lra.locationinfo_state,lra.grade,lra.scoreid, lra.lead_score,lra.firstname ,lra.lead_language,lra.studentinfo_board,lra.archived_datetime,lra.assignment_type,pb.prospectstage,pb.phone,case\n"+
            " when lra.qualityflaghqlq = 'HighQuality' then 'HQ'\n"+
            " when (lra.assignment_type is null or lra.assignment_type = '') then 'Non Core'\n"+
            " else 'Core' end as mx_core_nonCore,o.userid,o.subscriptionplanid\n"+
            " from     (select  *, rank() over (partition by prospectid order by archived_datetime desc) as rn\n"+
            "          from   analytics_dna.leadscore_result_archive_master\n"+
            "          where  assignment_type is not null\n"+
            "          and    assignment_type <> ''\n"+
            "          and    lead_score <> 0\n"+
            "          and    trunc(archived_datetime) >= dateadd(day, -15, current_date)\n"+
            "          and    studentinfo_board = 'state board'\n"+
            "          and    lead_language is not null \n"+
            "          and    lead_language <> '' \n"+
            "          and    lead_language <> 'Other'\n"+
            "          and    grade in ('11', '12', '13')\n"+
            "          and    assignment_type not in ('PaidTrial', 'Mod', 'App Non-core'))lra\n"+
            " inner join  leadsquared.prospect_base pb\n"+
            " on          lra.prospectid = pb.prospectid\n"+
            " inner join  leadsquared.user_base ub\n"+
            " on          ub.userid = pb.ownerid\n"+
            " inner join  leadsquared.prospect_extensionbase pe\n"+
            " on          pe.prospectid = pb.prospectid\n"+
            " left join   (select userid, subscriptionplanid\n"+
            "             from public.orders \n"+
            "             where subscriptionplanid is not null\n"+
            "             and paymentstatus <> 'NOT_PAID'\n"+
            "             and amount <> 0) o\n"+
            " on          split_part(o.userid, '_', 2) = pb.mx_user_id\n"+
            " where       lra.prospectid not in ('{}') \n"+
            " and         lra.rn = 1\n"+
            " and         ub.emailaddress in ('admin@vedantu.com','priyanka.m@vedantu.com','simriti.goel@vedantu.com','jayesh.singh@vedantu.com','parth.patel@vedantu.com','nayeeni.manoj@vedantu.com','kanjula.venkata@vedantu.com','ketha.prashanth@vedantu.com','anshul.goswami@vedantu.com')\n"+
            " and         pb.prospectstage not in ('Duplicate Lead','Care-V-Student','FOS Demo Done','Invalid Lead','FOS Invalid Lead','6. TOS Done','6a. Sales Warm','7. Sales Won','Refund','Price Verification Done','Price Verification Not Done','Sales Form Filled','Sales Form Re-Filled','Onboarding','Enrollment Done','Live User','Installment Due','Trial Registration Done')\n"+
            " and         (pb.prospectstage in ('FOS Not Interested', 'Not Interested') and trunc(pe.mx_lastactivitydate) < dateadd(day, -30, current_date) or pb.prospectstage not in ('FOS Not Interested', 'Not Interested'))\n"+
            " and         (pe.mx_sub_stage not in ('No student','>13 Grade','Course Not Available','<6 grade','Not a Student','> 5 Attempts','Teacher Enquiry','5','No student','6','<4 Grade','>5 DNP','DNP 5','FUW > 5 Attempts','Attempt 5') or pe.mx_sub_stage is null)\n"+
            " and         len(right(phone,10)) = 10\n"+
            " and         substring(right(pb.phone,10),1,1) in ('6','7','8','9'))\n"+
            " select *\n"+
            " from w1 \n"+
            " where subscriptionplanid is null \n"+
            " order by archived_datetime desc, lead_score desc";

    public static final String queryToFetchCounsellorBucketNonStateBoardLeads = " with w1 as (\n"+
            " select ms.prospectid,ms.locationinfo_state,ms.scoreid,ms.grade,pb.phone,pb.prospectstage,ms.firstname,ms.lead_language,ms.studentinfo_board,ms.lead_score,ms.assignment_type,pb.ownerid,ms.archived_datetime,o.userid,o.subscriptionplanid, case\n"+
            "             when ms.qualityflaghqlq = 'HighQuality' then 'HQ'\n"+
            "             when (ms.assignment_type is null or ms.assignment_type = '') then 'Non Core'\n"+
            "             else 'Core' end as mx_core_nonCore\n"+
            " from        (select  *, rank() over (partition by prospectid order by archived_datetime desc) as rn\n"+
            "              from   analytics_dna.leadscore_result_archive_master\n"+
            "              where  assignment_type is not null\n"+
            "              and    assignment_type <> ''\n"+
            "              and    lead_score <> 0\n"+
            "              and    trunc(archived_datetime) >= dateadd(day, -15, current_date)\n"+
            "              and    studentinfo_board <> 'state board'\n"+
            "              and    lead_language is not null \n"+
            "              and    lead_language <> '' \n"+
            "              and    lead_language <> 'Other'\n"+
            "              and    grade in ('6', '7', '8', '9', '10', '11', '12','13')\n"+
            "              and    assignment_type not in ('PaidTrial', 'Mod', 'App Non-core')) ms\n"+
            " inner join  leadsquared.prospect_base pb\n"+
            " on          ms.prospectid = pb.prospectid\n"+
            " inner join  leadsquared.user_base ub\n"+
            " on          ub.userid = pb.ownerid\n"+
            " inner join  leadsquared.prospect_extensionbase pe\n"+
            " on          pe.prospectid = pb.prospectid\n"+
            " left join   (select userid, subscriptionplanid\n"+
            "             from public.orders \n"+
            "             where subscriptionplanid is not null\n"+
            "             and paymentstatus <> 'NOT_PAID'\n"+
            "             and amount <> 0) o\n"+
            " on          split_part(o.userid, '_', 2) = pb.mx_user_id\n"+
            " inner join  (select leadid, createdon, duedate, RANK() OVER (PARTITION BY leadid ORDER BY createdon DESC) as latest_status from leadsquared.tasks) t\n"+
            " on          t.leadid = pb.prospectid\n"+
            " where       ms.prospectid not in ('{}') \n"+
            " and         rn = 1\n"+
            " and         latest_status = 1\n"+
            " and         ub.designation in ('ACADEMIC_COUNSELLOR','SENIOR_ACADEMIC_COUNSELLOR','TELE_CALLER','INTERN')\n"+
            " and         ub.statuscode = 0\n"+
            " and         trunc(t.duedate) not between current_date and dateadd(day,30,current_date)\n"+
            " and         ((pb.prospectstage in ('FOS Follow Up', '1c. Follow up','1d. Follow up Warm','EL Follow-up','4a. TOS Follow Up')\n"+
            "              and trunc(pe.mx_lastactivitydate) < dateadd(day, -15, current_date))\n"+
            "             or (pb.prospectstage in ('1b. Fresh Sign up', 'FOS DNP (Did not pick)','FOS Non Contact','1e. Non Contact','Language Barrier','FOS Region')\n"+
            "              and trunc(pe.mx_lastactivitydate) < dateadd(day, -7, current_date))\n"+
            "             or (pb.prospectstage in ('FOS Not Interested', 'Not Interested','FOS Demo Done') and trunc(pe.mx_lastactivitydate) < dateadd(day, -30, current_date)))\n"+
            " and         trunc(pe.mx_last_assigned_date) < dateadd(day,-7,current_date)\n"+
            " and         (pe.mx_sub_stage not in ('No student','>13 Grade','Course Not Available','<6 grade','Not a Student','> 5 Attempts','Teacher Enquiry','5','No student','6','<4 Grade','>5 DNP','DNP 5','FUW > 5 Attempts','Attempt 5') or pe.mx_sub_stage is null)\n"+
            " and         len(right(phone,10)) = 10\n"+
            " and         substring(right(pb.phone,10),1,1) in ('6','7','8','9'))\n"+
            " select *\n"+
            " from w1\n"+
            " where  (studentinfo_board = 'icse' and grade not in ('6','7', '8')) or studentinfo_board <> 'icse' \n"+
            " and   subscriptionplanid is null\n"+
            " order by archived_datetime desc,lead_score desc";

    public static final String queryToFetchCounsellorBucketStateBoardLeads = " with w1 as (\n"+
            " select ms.prospectid,ms.locationinfo_state,ms.scoreid,ms.grade,pb.phone,pb.prospectstage,ms.firstname,ms.lead_language,ms.studentinfo_board,ms.lead_score,ms.assignment_type,pb.ownerid,ms.archived_datetime,o.userid,o.subscriptionplanid, case\n"+
            "             when ms.qualityflaghqlq = 'HighQuality' then 'HQ'\n"+
            "             when (ms.assignment_type is null or ms.assignment_type = '') then 'Non Core'\n"+
            "             else 'Core' end as mx_core_nonCore\n"+
            " from        (select  *, rank() over (partition by prospectid order by archived_datetime desc) as rn\n"+
            "              from   analytics_dna.leadscore_result_archive_master\n"+
            "              where  assignment_type is not null\n"+
            "              and    assignment_type <> ''\n"+
            "              and    lead_score <> 0\n"+
            "              and    trunc(archived_datetime) >= dateadd(day, -15, current_date)\n"+
            "              and    studentinfo_board = 'state board'\n"+
            "              and    lead_language is not null \n"+
            "              and    lead_language <> '' \n"+
            "              and    lead_language <> 'Other'\n"+
            "              and    grade in ('11', '12','13')\n"+
            "              and    assignment_type not in ('PaidTrial', 'Mod', 'App Non-core')) ms\n"+
            " inner join  leadsquared.prospect_base pb\n"+
            " on          ms.prospectid = pb.prospectid\n"+
            " inner join  leadsquared.user_base ub\n"+
            " on          ub.userid = pb.ownerid\n"+
            " inner join  leadsquared.prospect_extensionbase pe\n"+
            " on          pe.prospectid = pb.prospectid\n"+
            " left join   (select userid, subscriptionplanid\n"+
            "             from public.orders \n"+
            "             where subscriptionplanid is not null\n"+
            "             and paymentstatus <> 'NOT_PAID'\n"+
            "             and amount <> 0) o\n"+
            " on          split_part(o.userid, '_', 2) = pb.mx_user_id\n"+
            " inner join  (select leadid, createdon, duedate, RANK() OVER (PARTITION BY leadid ORDER BY createdon DESC) as latest_status from leadsquared.tasks) t\n"+
            " on          t.leadid = pb.prospectid\n"+
            " where       ms.prospectid not in ('{}') \n"+
            " and         rn = 1\n"+
            " and         latest_status = 1\n"+
            " and         ub.designation in ('ACADEMIC_COUNSELLOR','SENIOR_ACADEMIC_COUNSELLOR','TELE_CALLER','INTERN')\n"+
            " and         ub.statuscode = 0\n"+
            " and         trunc(t.duedate) not between current_date and dateadd(day,30,current_date)\n"+
            " and         ((pb.prospectstage in ('FOS Follow Up', '1c. Follow up','1d. Follow up Warm','EL Follow-up','4a. TOS Follow Up')\n"+
            "              and trunc(pe.mx_lastactivitydate) < dateadd(day, -15, current_date))\n"+
            "             or (pb.prospectstage in ('1b. Fresh Sign up', 'FOS DNP (Did not pick)','FOS Non Contact','1e. Non Contact','Language Barrier','FOS Region')\n"+
            "              and trunc(pe.mx_lastactivitydate) < dateadd(day, -7, current_date))\n"+
            "             or (pb.prospectstage in ('FOS Not Interested', 'Not Interested','FOS Demo Done') and trunc(pe.mx_lastactivitydate) < dateadd(day, -30, current_date)))\n"+
            " and         trunc(pe.mx_last_assigned_date) < dateadd(day,-7,current_date)\n"+
            " and         (pe.mx_sub_stage not in ('No student','>13 Grade','Course Not Available','<6 grade','Not a Student','> 5 Attempts','Teacher Enquiry','5','No student','6','<4 Grade','>5 DNP','DNP 5','FUW > 5 Attempts','Attempt 5') or pe.mx_sub_stage is null)\n"+
            " and         len(right(phone,10)) = 10\n"+
            " and         substring(right(pb.phone,10),1,1) in ('6','7','8','9'))\n"+
            " select *\n"+
            " from w1\n"+
            " where subscriptionplanid is null\n"+
            " order by archived_datetime desc,lead_score desc";

    public static final String queryToFetchNonCoreLeads = "with test as (\n" +
            "with w1 as (\n" +
            "with base as (\n" +
            "    select id,\n" +
            "           studentinfo_board,locationinfo_state,\n" +
            "           creationtime_istdate,\n" +
            "           firstname,\n" +
            "           case\n" +
            "               when (locationinfo_city ilike '%ahmedabad%' or locationinfo_city ilike '%gandhinagar%') then 'Ahmedabad'\n" +
            "               when (locationinfo_city ilike '%surat%') then 'Surat'\n" +
            "               when (locationinfo_state ilike '%gujarat%') then 'Gujarat'\n" +
            "               when (locationinfo_city ilike '%chennai%') then 'Chennai'\n" +
            "               when (locationinfo_city ilike '%Coimbatore%') then 'Coimbatore'\n" +
            "               when (locationinfo_state ilike '%Tamil Nadu%') then 'Tamil Nadu'\n" +
            "               when (locationinfo_city ilike '%Kolkata%' or locationinfo_city ilike '%Calcutta%') then 'Kolkata'\n" +
            "               when (locationinfo_state ilike '%Bengal%') then 'West Bengal'\n" +
            "               when (locationinfo_city ilike '%Bhopal%') then 'Bhopal'\n" +
            "               when (locationinfo_city ilike '%Indore%') then 'Indore'\n" +
            "               when (locationinfo_city ilike '%jabalpur%') then 'Jabalpur'\n" +
            "               when (locationinfo_state ilike '%Madhya%' or locationinfo_state ilike '%MP%') then 'Madhya Pradesh'\n" +
            "               when (locationinfo_city ilike '%Raipur%') then 'Raipur'\n" +
            "               when (locationinfo_state ilike '%Chattis%' or locationinfo_state ilike '%CG%') then 'Chattisgarh'\n" +
            "               when (locationinfo_city ilike '%Kochi%' or locationinfo_city ilike '%cochin%') then 'Kochi'\n" +
            "               when (locationinfo_state ilike '%kerala%' or locationinfo_state ilike '%kerela%') then 'Kerala'\n" +
            "               when (locationinfo_city ilike '%Lucknow%') then 'Lucknow'\n" +
            "               when (locationinfo_city ilike '%Agra%') then 'Agra'\n" +
            "               when (locationinfo_city ilike '%Varanasi%' or locationinfo_city ilike '%Banaras%') then 'Varanasi'\n" +
            "               when (locationinfo_city ilike '%Kanpur%') then 'Kanpur'\n" +
            "               when (locationinfo_city ilike '%meerut%') then 'Meerut'\n" +
            "               when (locationinfo_city ilike '%Allahabad%') then 'Allahabad'\n" +
            "               when (locationinfo_state ilike '%Uttar%' or locationinfo_state ilike '%UP%') then 'Uttar Pradesh'\n" +
            "               when (locationinfo_city ilike '%Chandigarh%') then 'Chandigarh'\n" +
            "               when (locationinfo_city ilike '%Ludhiana%') then 'Ludhiana'\n" +
            "               when (locationinfo_state ilike '%Punjab%') then 'Punjab'\n" +
            "               when (locationinfo_city ilike '%Guwahati%') then 'Guwahati'\n" +
            "               when (locationinfo_state ilike '%Assam%' or locationinfo_state ilike '%Mizoram%' or\n" +
            "                     locationinfo_state ilike '%Arunachal%' or locationinfo_state ilike '%Manipur%' or\n" +
            "                     locationinfo_state ilike '%Nagaland%' or locationinfo_state ilike '%Sikkim%' OR\n" +
            "                     locationinfo_state ilike '%Tripura%' OR locationinfo_state ilike '%Meghalaya%') then 'Assam'\n" +
            "               when (locationinfo_city ilike '%Hyderabad%') then 'Hyderabad'\n" +
            "               when (locationinfo_city ilike '%Vishakhapatnam%' or locationinfo_city ilike '%vizag%' or\n" +
            "                     locationinfo_city ilike '%vyzag%') then 'Vizag'\n" +
            "               when (locationinfo_state ilike '%Andhra%' or locationinfo_state ilike '%Telangana%') then 'Telangana'\n" +
            "               when (locationinfo_city ilike '%Mumbai%' or locationinfo_city ilike '%Bombay%') then 'Mumbai'\n" +
            "               when (locationinfo_city ilike '%Pune%') then 'Pune'\n" +
            "               when (locationinfo_city ilike '%Nagpur%') then 'Nagpur'\n" +
            "               when (locationinfo_state ilike '%Mahara%') then 'Maharashtra'\n" +
            "               when (locationinfo_city ilike '%Bangalore%' or locationinfo_city ilike '%Bengaluru%') then 'Bangalore'\n" +
            "               when (locationinfo_state ilike '%Karnataka%') then 'Karnataka'\n" +
            "               when (locationinfo_city ilike '%Jaipur%') then 'Jaipur'\n" +
            "               when (locationinfo_state ilike '%Rajasthan%') then 'Rajasthan'\n" +
            "               when (locationinfo_city ilike '%Ranchi%') then 'Ranchi'\n" +
            "               when (locationinfo_state ilike '%Jharkhand%') then 'Jharkhand'\n" +
            "               when (locationinfo_city ilike '%Delhi%' or locationinfo_city ilike '%Gurgaon%' or\n" +
            "                     locationinfo_city ilike '%Noida%' or locationinfo_city ilike '%greater Noida%' or\n" +
            "                     locationinfo_city ilike '%Ghaziabad%') then 'Delhi'\n" +
            "               when (locationinfo_state ilike '%Delhi%') then 'Delhi'\n" +
            "               when (locationinfo_city ilike '%Patna%') then 'Patna'\n" +
            "               when (locationinfo_state ilike '%Bihar%') then 'Bihar'\n" +
            "               when (locationinfo_city ilike '%Bhubaneshwar%') then 'Bhubaneshwar'\n" +
            "               when (locationinfo_state ilike '%Odisha%' or locationinfo_state ilike '%Orissa%') then 'Odisha'\n" +
            "               Else 'Other'\n" +
            "               End as \"Mapped_City_Or_State\"\n" +
            "    from public.user\n" +
            "    where trunc(creationtime_istdate) >= dateadd(day, -60, current_date)\n" +
            "    order by creationtime_istdate\n" +
            ")\n" +
            "select base.*,pb.prospectid,\n" +
            "           pb.grade,\n" +
            "           pb.phone,\n" +
            "            'Non Core'  as mx_core_nonCore,\n" +
            "       l.languages as lead_language,'' as assignment_type,lra.scoreid,o.userid,o.subscriptionplanid,o.bundlestatus,o.paymentstatus,lra.lead_score,lra.scoreid\n" +
            "            from base\n" +
            "inner join leadsquared.prospect_base as pb on pb.mx_user_id = split_part(base.id, '_', 2)\n" +
            "and         pb.prospectid not in ('{}') \n" +
            "        and pb.ownerid in (select ub.userid\n" +
            "                           from leadsquared.user_base ub\n" +
            "                           where emailaddress in\n" +
            "                                 ('simriti.goel@vedantu.com', 'jayesh.singh@vedantu.com', 'admin@vedantu.com', 'parth.patel@vedantu.com'))\n" +
            "        and pb.grade in ('11', '9', '10', '8','7','6', '12') \n" +
            "        and         len(right(phone,10)) = 10\n" +
            "        and         substring(right(pb.phone,10),1,1) in ('6','7','8','9')\n" +
            "        and pb.prospectstage in  ('1a. Fresh Lead', '1b. Fresh Sign up')\n" +
            "        and base.studentinfo_board in ('CBSE','cbse')\n" +
            "inner join leadsquared.languages as l on base.Mapped_City_Or_State = l.city_state\n" +
            "inner join analytics_dna.leadscore_result_archive_master lra on pb.prospectid = lra.prospectid\n" +
            "left join public.orders as o on base.id = o.userid \n" +
            "where lra.lead_score <> 0 \n" +
            "),\n" +
            "w2 as(\n" +
            "    select * ,row_number() over (partition by w1.prospectid order by w1.creationtime_istdate desc)  as rn from w1\n" +
            ")\n" +
            "select * from w2 where rn = 1 and ((w2.userid is not null and (subscriptionplanid is null or (subscriptionplanid is not null and paymentstatus <> 'PAID'))) or w2.userid is null) order by w2.creationtime_istdate desc\n" +
            ")\n" +
            "select *, case when grade = 11 then 1\n" +
            "            when grade = 9 then 2\n" +
            "            when grade = 10 then 3\n" +
            "            when grade = 8 then 4\n" +
            "            when grade = 7 then 5\n" +
            "            when grade = 6 then 6\n" +
            "            when grade = 12 then 7\n" +
            "        else null end as grade_priority,\n" +
            "        case when lead_language = 'Malayalam' then 1\n" +
            "             when lead_language = 'Tamil' then 2\n" +
            "             when lead_language = 'Kannada' then 3\n" +
            "             when lead_language = 'Telugu' then 4\n" +
            "             when lead_language = 'Bengali' then 5\n" +
            "             when lead_language = 'Marathi' then 6\n" +
            "             when lead_language = 'Assamese' then 7\n" +
            "             when lead_language = 'Gujarati' then 8\n" +
            "             when lead_language = 'Punjabi' then 9\n" +
            "             when lead_language = 'Oriya' then 10\n" +
            "             when lead_language = 'Hindi 1' then 11\n" +
            "             when lead_language = 'Hindi 2' then 12\n" +
            "        else null end as language_priority\n" +
            "from test\n" +
            "order by language_priority asc, grade_priority asc, creationtime_istdate desc, lead_score desc";

    public static final String queryToGetAllLocations = "select  distinct location_preference from leadsquared.counsellor_preferences;";

    public static final String queryToFetchParthBucketLeads = " select * from (\n"+
            " select      lra.prospectid,lra.grade,lra.scoreid,lra.lead_score,lra.firstname ,lra.lead_language,u.studentinfo_board,u.studentinfo_grade,lra.archived_datetime,lra.assignment_type,pb.prospectstage,pb.phone, u.locationinfo_state,\n"+
            "             case\n"+
            "                when (locationinfo_city ilike '%ahmedabad%' or locationinfo_city ilike '%gandhinagar%') then 'Ahmedabad'\n"+
            "                when (locationinfo_city ilike '%surat%') then 'Surat'\n"+
            "                when (locationinfo_state ilike '%gujarat%') then 'Gujarat'\n"+
            "                when (locationinfo_city ilike '%chennai%') then 'Chennai'\n"+
            "                when (locationinfo_city ilike '%Coimbatore%') then 'Coimbatore'\n"+
            "                when (locationinfo_state ilike '%Tamil Nadu%') then 'Tamil Nadu'\n"+
            "                when (locationinfo_city ilike '%Kolkata%' or locationinfo_city ilike '%Calcutta%') then 'Kolkata'\n"+
            "                when (locationinfo_state ilike '%Bengal%') then 'West Bengal'\n"+
            "                when (locationinfo_city ilike '%Bhopal%') then 'Bhopal'\n"+
            "                when (locationinfo_city ilike '%Indore%') then 'Indore'\n"+
            "                when (locationinfo_city ilike '%jabalpur%') then 'Jabalpur'\n"+
            "                when (locationinfo_state ilike '%Madhya%' or locationinfo_state ilike '%MP%') then 'Madhya Pradesh'\n"+
            "                when (locationinfo_city ilike '%Raipur%') then 'Raipur'\n"+
            "                when (locationinfo_state ilike '%Chattis%' or locationinfo_state ilike '%CG%') then 'Chattisgarh'\n"+
            "                when (locationinfo_city ilike '%Kochi%' or locationinfo_city ilike '%cochin%') then 'Kochi'\n"+
            "                when (locationinfo_state ilike '%kerala%' or locationinfo_state ilike '%kerela%') then 'Kerala'\n"+
            "                when (locationinfo_city ilike '%Lucknow%') then 'Lucknow'\n"+
            "                when (locationinfo_city ilike '%Agra%') then 'Agra'\n"+
            "                when (locationinfo_city ilike '%Varanasi%' or locationinfo_city ilike '%Banaras%') then 'Varanasi'\n"+
            "                when (locationinfo_city ilike '%Kanpur%') then 'Kanpur'\n"+
            "                when (locationinfo_city ilike '%meerut%') then 'Meerut'\n"+
            "                when (locationinfo_city ilike '%Allahabad%') then 'Allahabad'\n"+
            "                when (locationinfo_state ilike '%Uttar%' or locationinfo_state ilike '%UP%') then 'Uttar Pradesh'\n"+
            "                when (locationinfo_city ilike '%Chandigarh%') then 'Chandigarh'\n"+
            "                when (locationinfo_city ilike '%Ludhiana%') then 'Ludhiana'\n"+
            "                when (locationinfo_state ilike '%Punjab%') then 'Punjab'\n"+
            "                when (locationinfo_city ilike '%Guwahati%') then 'Guwahati'\n"+
            "                when (locationinfo_state ilike '%Assam%' or locationinfo_state ilike '%Mizoram%' or\n"+
            "                      locationinfo_state ilike '%Arunachal%' or locationinfo_state ilike '%Manipur%' or\n"+
            "                      locationinfo_state ilike '%Nagaland%' or locationinfo_state ilike '%Sikkim%' OR\n"+
            "                      locationinfo_state ilike '%Tripura%' OR locationinfo_state ilike '%Meghalaya%') then 'Assam'\n"+
            "                when (locationinfo_city ilike '%Hyderabad%') then 'Hyderabad'\n"+
            "                when (locationinfo_city ilike '%Vishakhapatnam%' or locationinfo_city ilike '%vizag%' or\n"+
            "                      locationinfo_city ilike '%vyzag%') then 'Vizag'\n"+
            "                when (locationinfo_state ilike '%Andhra%' or locationinfo_state ilike '%Telangana%') then 'Telangana'\n"+
            "                when (locationinfo_city ilike '%Mumbai%' or locationinfo_city ilike '%Bombay%') then 'Mumbai'\n"+
            "                when (locationinfo_city ilike '%Pune%') then 'Pune'\n"+
            "                when (locationinfo_city ilike '%Nagpur%') then 'Nagpur'\n"+
            "                when (locationinfo_state ilike '%Mahara%') then 'Maharashtra'\n"+
            "                when (locationinfo_city ilike '%Bangalore%' or locationinfo_city ilike '%Bengaluru%') then 'Bangalore'\n"+
            "                when (locationinfo_state ilike '%Karnataka%') then 'Karnataka'\n"+
            "                when (locationinfo_city ilike '%Jaipur%') then 'Jaipur'\n"+
            "                when (locationinfo_state ilike '%Rajasthan%') then 'Rajasthan'\n"+
            "                when (locationinfo_city ilike '%Ranchi%') then 'Ranchi'\n"+
            "                when (locationinfo_state ilike '%Jharkhand%') then 'Jharkhand'\n"+
            "                when (locationinfo_city ilike '%Delhi%' or locationinfo_city ilike '%Gurgaon%' or\n"+
            "                      locationinfo_city ilike '%Noida%' or locationinfo_city ilike '%greater Noida%' or\n"+
            "                      locationinfo_city ilike '%Ghaziabad%') then 'Delhi'\n"+
            "                when (locationinfo_state ilike '%Delhi%') then 'Delhi'\n"+
            "                when (locationinfo_city ilike '%Patna%') then 'Patna'\n"+
            "                when (locationinfo_state ilike '%Bihar%') then 'Bihar'\n"+
            "                when (locationinfo_city ilike '%Bhubaneshwar%') then 'Bhubaneshwar'\n"+
            "                when (locationinfo_state ilike '%Odisha%' or locationinfo_state ilike '%Orissa%') then 'Odisha'\n"+
            "                Else 'Other'\n"+
            "                End as \"Mapped_City_Or_State\",case\n"+
            "                 when lra.qualityflaghqlq = 'HighQuality' then 'HQ'\n"+
            "                 when (lra.assignment_type is null or lra.assignment_type = '') then 'Non Core'\n"+
            "                 else 'Core' end as mx_core_nonCore,\n"+
            "             o.subscriptionplanid\n"+
            " from        (select     *\n"+
            "             from        leadsquared.prospect_base \n"+
            "             where       ownerid = 'be57734e-c002-11e9-a003-0268638e5f02' \n"+
            "             and         len(right(phone,10)) = 10\n"+
            "             and         substring(right(phone,10),1,1) in ('6','7','8','9'))  pb\n"+
            " inner join  (select * from public.user\n"+
            "              where studentinfo_grade in (6, 7, 8, 9, 10, 11, 12, 13)\n"+
            "              and ((studentinfo_board  ilike '%state%' and studentinfo_grade in ('11', '12', '13')) or studentinfo_board not ilike '%state%')) u\n"+
            " on          pb.mx_user_id = split_part(u.id, '_', 2)\n"+
            " inner join  leadsquared.prospect_extensionbase pe\n"+
            " on          pb.prospectid = pe.prospectid\n"+
            " left join   (select userid, subscriptionplanid\n"+
            "             from public.orders \n"+
            "             where subscriptionplanid is not null\n"+
            "             and paymentstatus <> 'NOT_PAID'\n"+
            "             and amount <> 0) o\n"+
            " on          split_part(o.userid, '_', 2) = pb.mx_user_id\n"+
            " inner join  (select prospectid,grade,qualityflaghqlq,scoreid,firstname,lead_language,lead_score,archived_datetime,assignment_type, RANK() OVER (PARTITION BY prospectid ORDER BY archived_datetime DESC) as latest_status from analytics_dna.leadscore_result_archive_master where lead_score <> 0) lra\n"+
            " on          lra.prospectid = pb.prospectid\n"+
            " where       pb.prospectid not in ('{}') \n"+
            " and         latest_status = 1\n"+
            " and         ((u.studentinfo_board ilike '%icse%' and u.studentinfo_grade not in ('6', '7', '8')) or u.studentinfo_board not ilike '%icse%')\n"+
            " and         pb.prospectstage not in ('Duplicate Lead','Care-V-Student','FOS Demo Done','Invalid Lead','FOS Invalid Lead','6. TOS Done','6a. Sales Warm','7. Sales Won','Refund','Price Verification Done','Price Verification Not Done','Sales Form Filled','Sales Form Re-Filled','Onboarding','Enrollment Done','Live User','Installment Due','Trial Registration Done')\n"+
            " and         (pb.prospectstage in ('FOS Not Interested', 'Not Interested') and trunc(pe.mx_lastactivitydate) < dateadd(day, -30, current_date) or pb.prospectstage not in ('FOS Not Interested', 'Not Interested'))\n"+
            " and         (pe.mx_sub_stage not in ('No student','>13 Grade','Course Not Available','<6 grade','Not a Student','> 5 Attempts','Teacher Enquiry','5','No student','6','<4 Grade','>5 DNP','DNP 5','FUW > 5 Attempts','Attempt 5') or pe.mx_sub_stage is null)\n"+
            " order by    archived_datetime desc, lead_score desc)\n"+
            " where       subscriptionplanid is null";
}