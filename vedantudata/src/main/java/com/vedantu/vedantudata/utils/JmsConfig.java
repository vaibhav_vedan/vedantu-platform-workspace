/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.vedantudata.utils;

import com.amazon.sqs.javamessaging.SQSConnection;
import com.amazon.sqs.javamessaging.SQSConnectionFactory;
import com.amazon.sqs.javamessaging.SQSSession;
import com.amazonaws.metrics.AwsSdkMetrics;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.vedantu.aws.AwsCloudWatchManager;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.StringUtils;
import com.vedantu.util.enums.SQSQueue;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jms.annotation.EnableJms;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.jms.JMSException;
import javax.jms.MessageConsumer;
import javax.jms.Session;
import java.util.HashSet;
import java.util.Set;

/**
 * http://docs.aws.amazon.com/AWSSimpleQueueService/latest/SQSDeveloperGuide/getting-started.html
 */
@Configuration
@EnableJms
public class JmsConfig {

    @Autowired
    private SQSListener sqsListener;

    private SQSConnection connection;

    private static final Logger logger = LogManager.getRootLogger();

    private Set<SQSQueue> queueListToCreateAlarm = new HashSet<>();

    @Autowired
    private AwsCloudWatchManager awsCloudWatchManager;

    @Bean
    public SQSConnection sqsConnection() throws JMSException {        
        String env = ConfigUtils.INSTANCE.getStringValue("environment");
        if (!(StringUtils.isEmpty(env) || env.equals("LOCAL"))) {
            SQSConnectionFactory sqsConnectionFactory = SQSConnectionFactory.builder()
                    .withRegion(Region.getRegion(Regions.EU_WEST_1))
                    .withNumberOfMessagesToPrefetch(10).build();

            connection = sqsConnectionFactory.createConnection();
            Session session = connection.createSession(false, SQSSession.CLIENT_ACKNOWLEDGE);
            MessageConsumer dmlcLeadSquare = session.createConsumer(session.createQueue(SQSQueue.VEDANTU_DATA_LEADSQUARED_QUEUE.getQueueName(env)));
            MessageConsumer dmlcUpdatLeadSquare = session.createConsumer(session.createQueue(SQSQueue.UPDATE_LEADSQUARED_QUEUE.getQueueName(env)));
//            MessageConsumer leadSquaredEvents = session.createConsumer(session.createQueue(SQSQueue.LEADSQUARED_EVENTS.getQueueName(env)));
//            leadSquaredEvents.setMessageListener(sqsListener);
            MessageConsumer slashRTCEvents = session.createConsumer(session.createQueue(SQSQueue.SLASHRTC_ACTIVITY.getQueueName(env)));

            dmlcLeadSquare.setMessageListener(sqsListener);
            dmlcUpdatLeadSquare.setMessageListener(sqsListener);
            slashRTCEvents.setMessageListener(sqsListener);

            //Add queues to create alarm
            queueListToCreateAlarm.add(SQSQueue.VEDANTU_DATA_LEADSQUARED_QUEUE);
            queueListToCreateAlarm.add(SQSQueue.VEDANTU_DATA_LEADSQUARED_QUEUE_DL);
            queueListToCreateAlarm.add(SQSQueue.UPDATE_LEADSQUARED_QUEUE);
            queueListToCreateAlarm.add(SQSQueue.UPDATE_LEADSQUARED_QUEUE_DL);
            queueListToCreateAlarm.add(SQSQueue.LEADSQUARED_EVENTS);
            queueListToCreateAlarm.add(SQSQueue.LEADSQUARED_EVENTS_DL);

            connection.start();
            awsCloudWatchManager.createAlarms(queueListToCreateAlarm);
            logger.info("JmsConfig Bean created");

            return connection;
        } else {
            return null;
        }
    }


    @PreDestroy
    public void cleanUp() {
        try {
            if (connection != null) {
                connection.close();
            }
            com.amazonaws.http.IdleConnectionReaper.shutdown();
            AwsSdkMetrics.unregisterMetricAdminMBean();            
        } catch (Exception e) {
            logger.error("Error in closing sqs connection ", e);
        }
    }

}
