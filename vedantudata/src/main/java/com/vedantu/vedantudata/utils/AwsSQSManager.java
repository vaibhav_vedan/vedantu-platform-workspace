/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.vedantudata.utils;


import com.amazonaws.services.sqs.model.CreateQueueRequest;
import com.amazonaws.services.sqs.model.GetQueueAttributesRequest;
import com.amazonaws.services.sqs.model.GetQueueAttributesResult;
import com.amazonaws.services.sqs.model.QueueAttributeName;
import com.amazonaws.services.sqs.model.SetQueueAttributesRequest;
import com.vedantu.aws.AbstractAwsSQSManagerNew;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.enums.SQSQueue;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

/**
 * Created by somil on 31/08/17.
 * http://developer.lightbend.com/docs/alpakka/latest/sqs.html
 */
@Service
public class AwsSQSManager extends AbstractAwsSQSManagerNew {

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(AwsSQSManager.class);

    private String env;

    public AwsSQSManager() {
        super();
        logger.info("initializing AwsSQSManager");
        try {
            env = ConfigUtils.INSTANCE.getStringValue("environment");
            if (!(StringUtils.isEmpty(env) || env.equals("LOCAL"))) {
                
                CreateQueueRequest leadSquare_dl = new CreateQueueRequest(SQSQueue.VEDANTU_DATA_LEADSQUARED_QUEUE_DL
                        .getQueueName(env));
                leadSquare_dl.addAttributesEntry(QueueAttributeName.FifoQueue.name(), "true");
                sqsClient.createQueue(leadSquare_dl);
                logger.info("created queue for leadSquare deadletter");

                CreateQueueRequest leadSquaredQueue = new CreateQueueRequest(SQSQueue.VEDANTU_DATA_LEADSQUARED_QUEUE
                        .getQueueName(env));
                leadSquaredQueue.addAttributesEntry(QueueAttributeName.FifoQueue.name(), "true");
                leadSquaredQueue.addAttributesEntry(QueueAttributeName.VisibilityTimeout.name(), SQSQueue.VEDANTU_DATA_LEADSQUARED_QUEUE.getVisibilityTimeout());
                sqsClient.createQueue(leadSquaredQueue);

                assignDeadLetterQueue(SQSQueue.VEDANTU_DATA_LEADSQUARED_QUEUE
                        .getQueueName(env), SQSQueue.VEDANTU_DATA_LEADSQUARED_QUEUE_DL
                        .getQueueName(env), 6);

                CreateQueueRequest updateLeadSquaredQueue_dl = new CreateQueueRequest(SQSQueue.UPDATE_LEADSQUARED_QUEUE_DL
                        .getQueueName(env));
                updateLeadSquaredQueue_dl.addAttributesEntry(QueueAttributeName.FifoQueue.name(), "true");
                sqsClient.createQueue(updateLeadSquaredQueue_dl);
                logger.info("created queue for updateLeadSquaredQueue deadletter");

                CreateQueueRequest updateLeadSquaredQueue = new CreateQueueRequest(SQSQueue.UPDATE_LEADSQUARED_QUEUE.getQueueName(env));
                updateLeadSquaredQueue.addAttributesEntry(QueueAttributeName.FifoQueue.name(), "true");
                updateLeadSquaredQueue.addAttributesEntry(QueueAttributeName.VisibilityTimeout.name(), SQSQueue.UPDATE_LEADSQUARED_QUEUE.getVisibilityTimeout());
                sqsClient.createQueue(updateLeadSquaredQueue);

                assignDeadLetterQueue(SQSQueue.UPDATE_LEADSQUARED_QUEUE
                        .getQueueName(env), SQSQueue.UPDATE_LEADSQUARED_QUEUE_DL
                        .getQueueName(env), 6);


                CreateQueueRequest slashRTCQueue = new CreateQueueRequest(SQSQueue.SLASHRTC_ACTIVITY.getQueueName(env));
                slashRTCQueue.addAttributesEntry(QueueAttributeName.VisibilityTimeout.name(), SQSQueue.SLASHRTC_ACTIVITY.getVisibilityTimeout());
                sqsClient.createQueue(slashRTCQueue);
                logger.info("created queue for slashRTC");


//                CreateQueueRequest lsEventsQueue_dl = new CreateQueueRequest(SQSQueue.LEADSQUARED_EVENTS_DL
//                        .getQueueName(env));
//                sqsClient.createQueue(lsEventsQueue_dl);
//                logger.info("created queue for updateLeadSquaredQueue deadletter");
//
//                CreateQueueRequest lsEventsQueue = new CreateQueueRequest(SQSQueue.LEADSQUARED_EVENTS.getQueueName(env));
//                lsEventsQueue.addAttributesEntry(QueueAttributeName.VisibilityTimeout.name(), SQSQueue.LEADSQUARED_EVENTS.getVisibilityTimeout());
//                sqsClient.createQueue(lsEventsQueue);
//
//                assignDeadLetterQueue(SQSQueue.LEADSQUARED_EVENTS
//                        .getQueueName(env), SQSQueue.LEADSQUARED_EVENTS_DL
//                        .getQueueName(env), 6);

            }
        } catch (Exception e) {
            logger.error("Error in initializing AwsSQSManager " + e.getMessage());
        }

        try{
            CreateQueueRequest vglanceSessionQueue_dl = new CreateQueueRequest(SQSQueue.VGLANCE_SESSION_UPDATE_DL
                    .getQueueName(env));
            vglanceSessionQueue_dl.addAttributesEntry(QueueAttributeName.VisibilityTimeout.name(), SQSQueue.VGLANCE_SESSION_UPDATE_DL.getVisibilityTimeout());
            sqsClient.createQueue(vglanceSessionQueue_dl);
            logger.info("created queue for vglanceSessionUpdate deadletter");

            CreateQueueRequest vglanceSessionQueue = new CreateQueueRequest(SQSQueue.VGLANCE_SESSION_UPDATE.getQueueName(env));
            vglanceSessionQueue.addAttributesEntry(QueueAttributeName.VisibilityTimeout.name(), SQSQueue.VGLANCE_SESSION_UPDATE.getVisibilityTimeout());
            sqsClient.createQueue(vglanceSessionQueue);
            logger.info("created queue for vglanceSessionUpdate");

            assignDeadLetterQueue(SQSQueue.VGLANCE_SESSION_UPDATE
                    .getQueueName(env), SQSQueue.VGLANCE_SESSION_UPDATE_DL
                    .getQueueName(env), 6);
        } catch(Exception e){
            logger.error("Error in initializing VGLANCE AwsSQSManager " + e.getMessage());
        }


        try {
            CreateQueueRequest autoEnrollQueue_dl = new CreateQueueRequest(SQSQueue.AUTO_ENROLLMENT_QUEUE_DL
                    .getQueueName(env));
            autoEnrollQueue_dl.addAttributesEntry(QueueAttributeName.VisibilityTimeout.name(), SQSQueue.AUTO_ENROLLMENT_QUEUE_DL.getVisibilityTimeout());
            sqsClient.createQueue(autoEnrollQueue_dl);
            logger.info("created queue for autoEnrollment deadletter");

            CreateQueueRequest autoEnrollQueue = new CreateQueueRequest(SQSQueue.AUTO_ENROLLMENT_QUEUE.getQueueName(env));
            autoEnrollQueue.addAttributesEntry(QueueAttributeName.VisibilityTimeout.name(), SQSQueue.AUTO_ENROLLMENT_QUEUE.getVisibilityTimeout());
            sqsClient.createQueue(autoEnrollQueue);
            logger.info("created queue for autoEnrollment");

            assignDeadLetterQueue(SQSQueue.AUTO_ENROLLMENT_QUEUE
                    .getQueueName(env), SQSQueue.AUTO_ENROLLMENT_QUEUE_DL
                    .getQueueName(env), 6);
        } catch (Exception e) {
            logger.error("Error in initializing AutoEnrollment queue in AwsSQSManager: {}",e);
        }

    }
    private void assignDeadLetterQueue(String src_queue_name,String dl_queue_name,Integer maxReceive){
        if(maxReceive==null){
            maxReceive=4;
        }
        // Get dead-letter queue ARN
        String dl_queue_url = sqsClient.getQueueUrl(dl_queue_name)
                                 .getQueueUrl();

        GetQueueAttributesResult queue_attrs = sqsClient.getQueueAttributes(
                new GetQueueAttributesRequest(dl_queue_url)
                    .withAttributeNames("QueueArn"));

        String dl_queue_arn = queue_attrs.getAttributes().get("QueueArn");

        // Set dead letter queue with redrive policy on source queue.
        String src_queue_url = sqsClient.getQueueUrl(src_queue_name)
                                  .getQueueUrl();

        SetQueueAttributesRequest request = new SetQueueAttributesRequest()
                .withQueueUrl(src_queue_url)
                .addAttributesEntry("RedrivePolicy",
                        "{\"maxReceiveCount\":\""+maxReceive+"\", \"deadLetterTargetArn\":\""
                        + dl_queue_arn + "\"}");

        sqsClient.setQueueAttributes(request);
        logger.info("assigned dead letter queue: "+dl_queue_name+"  to:"+src_queue_name);
        
    }

    @PostConstruct
    public void postConstruct() {

    }

}
