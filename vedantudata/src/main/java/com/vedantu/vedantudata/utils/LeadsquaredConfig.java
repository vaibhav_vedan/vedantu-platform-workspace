package com.vedantu.vedantudata.utils;

public class LeadsquaredConfig {
	protected static String ADMIN_EMAIL_ID = "admin@vedantu.com";

	protected static String apiBaseUrl = "https://api.leadsquared.com/v2/";
	protected static String getUsersUrl = apiBaseUrl + "UserManagement.svc/Users.Get";
    protected static String getUsersByIdUrl = apiBaseUrl + "UserManagement.svc/User/Retrieve/ByUserId";
    protected static String getUsersByEmailUrl = apiBaseUrl + "UserManagement.svc/User/Retrieve/ByEmailAddress";
	protected static String getLeadByEmailIdUrl = apiBaseUrl + "LeadManagement.svc/Leads.GetByEmailaddress";
	protected static String getLeadByPhoneUrl = apiBaseUrl + "LeadManagement.svc/RetrieveLeadByPhoneNumber";
	protected static String getLeadByLeadIdUrl = apiBaseUrl + "LeadManagement.svc/Leads.GetById";
	protected static String getLeadByTimeIntervalUrl = apiBaseUrl + "LeadManagement.svc/Leads.RecentlyModified";
	protected static String getLeadActivityByLeadId = apiBaseUrl + "ProspectActivity.svc/Retrieve";
	protected static String getLeadActivityByTimeIntervalUrl = apiBaseUrl
			+ "ProspectActivity.svc/RetrieveRecentlyModified";
	protected static String getLeadMetadataUrl = apiBaseUrl + "LeadManagement.svc/LeadsMetaData.Get";
	protected static String getLeadUpsertUrl = apiBaseUrl + "LeadManagement.svc/Lead.CreateOrUpdate";
	protected static String updateLeadDetailsBulkUrl = apiBaseUrl + "LeadManagement.svc/Lead/Bulk/CreateOrUpdate";
	protected static String postLeadActivityBulkUrl = apiBaseUrl
			+ "ProspectActivity.svc/Bulk/CustomActivity/Add/ByLeadId";
	protected static String captureLeadUrl = apiBaseUrl + "LeadManagement.svc/Lead.Capture";
	protected static String getLeadActivityUrl = apiBaseUrl + "ProspectActivity.svc/Retrieve";
	protected static String updateBulkLeadFeildsUrl = apiBaseUrl + "LeadManagement.svc/Lead/Bulk/Update";
	protected static String getQuickSearchLeadsUrl = apiBaseUrl + "LeadManagement.svc/Leads.GetByQuickSearch";
	protected static String createNotesUrl = apiBaseUrl + "LeadManagement.svc/CreateNote";
	protected static String postLeadActivityUrl = apiBaseUrl + "ProspectActivity.svc/Create";
	protected static String postLeadActivityUrlByField = apiBaseUrl + "ProspectActivity.svc/Bulk/CustomActivity/Add/ByLeadField";
	protected static String postLeadActivityUpdateUrlBulk = apiBaseUrl + "ProspectActivity.svc/Bulk/CustomActivity/Update";
	protected static String postLeadActivityUpdateUrl = apiBaseUrl + "ProspectActivity.svc/CustomActivity/Update";
	protected static String getBulkLeads = apiBaseUrl +  "LeadManagement.svc/Leads.Get";

	public static String accessKey;
	public static String secretKey;

}
