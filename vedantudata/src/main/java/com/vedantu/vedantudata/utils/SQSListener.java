/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.vedantudata.utils;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.vedantu.User.User;
import com.vedantu.User.UserBasicInfo;
import com.vedantu.util.*;
import com.vedantu.util.enums.SQSMessageType;
import com.vedantu.vedantudata.dao.ActivityDao;
import com.vedantu.vedantudata.entities.ActivityDetails;
import com.vedantu.vedantudata.entities.leadsquared.LeadSquaredAgent;
import com.vedantu.vedantudata.entities.leadsquared.LeadsquaredUser;
import com.vedantu.vedantudata.managers.ActivityManager;
import com.vedantu.vedantudata.managers.ScEnrollmentManager;
import com.vedantu.vedantudata.managers.ScVglanceManager;
import com.vedantu.vedantudata.managers.dialers.SlashRTCManager;
import com.vedantu.vedantudata.managers.leadsquared.LeadSquaredUsers;
import com.vedantu.vedantudata.managers.leadsquared.LeadsquaredActivityManager;
import com.vedantu.vedantudata.managers.leadsquared.LeadsquaredManager;
import com.vedantu.vedantudata.managers.moengage.MoengageManager;
import com.vedantu.vedantudata.pojos.ActivityDetailsPojo;
import com.vedantu.vedantudata.pojos.ActivityResponseMessage;
import com.vedantu.vedantudata.pojos.ActivityresponsepayloadPojo;

import java.lang.reflect.Type;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

import com.vedantu.vedantudata.pojos.LeadsquaredActivityField;
import com.vedantu.vedantudata.pojos.request.OnlineSalesDemoSessionJoinedLSRequest;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author jeet
 */
@Component
public class SQSListener implements MessageListener {

    @Autowired
    private LogFactory logFactory;

    @Autowired
    private ActivityDao activityDao;

    @Autowired
    private LeadsquaredActivityManager leadsquaredActivityManager;

    @Autowired
    private ActivityManager activityManager;

    @Autowired
    private LeadsquaredManager leadsquaredManager;

    @Autowired
    private MoengageManager moengageManager;

    @Autowired
    private SlashRTCManager slashRTCManager;

    @Autowired
    private ScVglanceManager scVglanceManager;

    @Autowired
    private ScEnrollmentManager scEnrollmentManager;

    @Autowired
    private FosUtils fosUtils;


    private final Gson gson = new Gson();

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(SQSListener.class);

    @Override
    public void onMessage(Message message) {

        TextMessage textMessage = (TextMessage) message;

        String messageId = "";
        String messagetext = "";
        try {

            logger.info("MessageId: " + textMessage.getJMSMessageID());
            logger.info("Received message " + textMessage.getText());
            messageId = textMessage.getJMSMessageID();
            messagetext = textMessage.getText();
            SQSMessageType sqsMessageType = SQSMessageType.valueOf(textMessage.getStringProperty("messageType"));
            handleMessage(sqsMessageType, textMessage.getText());
            message.acknowledge();

        } catch (Exception e) {
            logger.error("Error processing message: {}, messageText: {}, MessageId: {} \n error: ",textMessage, messagetext, messageId, e);
        }
    }

    public void handleMessage(SQSMessageType sqsMessageType, String text) throws Exception {


        Type type = new TypeToken<List<ActivityDetails>>() {
        }.getType();
        switch (sqsMessageType) {
            case LS_PUSH_ACTIVITY_TRIGGER: {
                logger.info("======Recieved the request for trigger activities");

                List<ActivityDetails> activityDetailsStaticList = gson.fromJson(text, type);
                List<ActivityDetailsPojo> activityDetailsMobilePojos = new ArrayList<>();
                List<ActivityDetails> activityDetailsMobileStaticListSub  = new ArrayList<>();

                List<ActivityDetailsPojo> activityDetailsEmailPojos = new ArrayList<>();
                List<ActivityDetails> activityDetailsEmailStaticListSub  = new ArrayList<>();

                for (ActivityDetails details : activityDetailsStaticList) {
                    if ((ActivityEventConstants.EVENT_GRIEVANCE == details.getActivityEvent() || ActivityEventConstants.EVENT_OPEN_TICKETS == details.getActivityEvent())
                            && activityDao.checkForFreshdeskActivity(details.getLeadVedantuId(), details.getActivityEvent(), details.getFreshdeskTicketId())) {
                        continue;
                    }

                    ActivityDetailsPojo activityDetailsPojo = new Gson().fromJson(new Gson().toJson(details), ActivityDetailsPojo.class);

                    if(StringUtils.isNotEmpty(details.getPhone())){
                        activityDetailsPojo.setSearchByValue(details.getPhone());
                        activityDetailsMobilePojos.add(activityDetailsPojo);
                        activityDetailsMobileStaticListSub.add(details);
                    }else if(StringUtils.isNotEmpty(details.getEmailAddress())) {
                        activityDetailsPojo.setSearchByValue(details.getEmailAddress());
                        activityDetailsEmailPojos.add(activityDetailsPojo);
                        activityDetailsEmailStaticListSub.add(details);
                    }

                }

                List<ActivityDetails> activityDetailsList = new ArrayList<>();

                if(!activityDetailsMobilePojos.isEmpty()){
                    logger.info("======triggering activities search by mobile");
                    ActivityresponsepayloadPojo activityMobileresponsepayloadPojo = leadsquaredActivityManager.pushActivitiesByMobile(activityDetailsMobilePojos);
                    manageResponsePayload(activityMobileresponsepayloadPojo, activityDetailsList, activityDetailsMobileStaticListSub);
                }else{
                    logger.info("======Ignoring activities search by phone since the list is empty");
                }

                logger.info("======persisting phone activities");
                activityDao.creatActivities(activityDetailsList);

                activityDetailsList = new ArrayList<>();

                if(!activityDetailsEmailPojos.isEmpty()){
                    logger.info("======triggering activities search by email");
                    ActivityresponsepayloadPojo activityEmailresponsepayloadPojo = leadsquaredActivityManager.pushActivitiesByEmail(activityDetailsEmailPojos);
                    manageResponsePayload(activityEmailresponsepayloadPojo, activityDetailsList, activityDetailsEmailStaticListSub);
                }else{
                    logger.info("======Ignoring activities search by email since the list is empty");
                }

                logger.info("======persisting email activities");
                activityDao.creatActivities(activityDetailsList);
                break;
            }
            case LS_PUSH_UPDATE_ACTIVITY_TRIGGER: {
                Set<ActivityDetailsPojo> activityDetailsPojos = new HashSet<>();

                List<ActivityDetails> updateActivityList = gson.fromJson(text, type);
                for (ActivityDetails details : updateActivityList) {
                    details.setEmailAddress(null);
                    ActivityDetailsPojo activityDetailsPojo = new Gson().fromJson(new Gson().toJson(details), ActivityDetailsPojo.class);
                    activityDetailsPojos.add(activityDetailsPojo);
                }
                logger.info("======triggering activities");
                ActivityresponsepayloadPojo activityresponsepayloadPojo = leadsquaredActivityManager.updateActivities(activityDetailsPojos);

                if(activityresponsepayloadPojo != null) {
                    List<ActivityDetails> activityDetailsList = new ArrayList<>();
                    List<ActivityResponseMessage> responseMessages = activityresponsepayloadPojo.getResponse();
                    for (int i = 0; i < responseMessages.size(); i++) {
                        ActivityResponseMessage activityResponseMessage = responseMessages.get(i);
                        if (activityResponseMessage.isActivityUpdated()) {

                            String prospectActivityId = activityResponseMessage.getProspectActivityId();
                            ActivityDetails activity = updateActivityList.get(i);
                            activity.setProspectActivityId(prospectActivityId);
                            activityDetailsList.add(activity);
                        }
                    }

                    logger.info("======persisting activities");
                    activityDao.updateEntities(activityDetailsList);
                }
                break;
            }
            case FD_PERSIST_DATA_IN_RS: {
                Type dataType = new TypeToken<List<Map<String, Object>>>(){}.getType();
                List<Map<String, Object>> tickets = gson.fromJson(text, dataType);
                activityManager.persistTicketInRedshift(tickets);
                break;
            }
            case FRESHDESK_FETCH_LAST_UPDATED_RECORDS: {
                activityManager.triggerFreshdeskTicketActivity();
                break;
            }
            case UPDATE_LEAD_PAYMENT_LEADSQUARED:{
                leadsquaredManager.updatePaymentFromSQS(text);
                break;
            }
            case LEAD_STAGE_CHANGE:{
                moengageManager.updateLead(text);
                break;
            }
            case SLASHRTC_ACTIVITY:{
                slashRTCManager.addDialerActivityFromSQS(text);
                break;
            }
            case VGLANCE_SESSION_UPDATE:{
                scVglanceManager.addUserSessionDetails(text);
                break;
            }
            case AUTO_ENROLLMENT_UPDATE:{
                scEnrollmentManager.sendAutoEnrollmentUpdate(text);
                break;
            }
            default:
                throw new RuntimeException("Unknown SQS Consumer" + sqsMessageType);
        }

        logger.info("Message handled");
    }

    private void manageResponsePayload(ActivityresponsepayloadPojo activityResponsepayloadPojo, List<ActivityDetails> activityDetailsList, List<ActivityDetails> activityDetailsStaticListSub) {

        if(activityResponsepayloadPojo==null)
            return;

        logger.info("\n\nLS Response ::: {}",activityResponsepayloadPojo.toString());
        List<ActivityResponseMessage> responseMobileMessages = activityResponsepayloadPojo.getResponse();
        for (int i = 0; i < responseMobileMessages.size(); i++) {
            ActivityResponseMessage activityResponseMessage = responseMobileMessages.get(i);
            if (activityResponseMessage.isActivityCreated()) {
                String prospectActivityId = activityResponseMessage.getProspectActivityId();
                ActivityDetails activity = activityDetailsStaticListSub.get(i);
                activity.setProspectActivityId(prospectActivityId);
                activityDetailsList.add(activity);
            }
        }
    }
}
