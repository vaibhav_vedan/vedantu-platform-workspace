package com.vedantu.vedantudata.utils;

import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.VException;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.vedantudata.controllers.ActivityController;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.*;

@Service
public class VedantudataPostgressHandler {
    private static final String jdbcPrefix = ConfigUtils.INSTANCE.getStringValue("vedantudata.redshift.jdbc.prefix");
    private static final String host = ConfigUtils.INSTANCE.getStringValue("vedantudata.redshift.host");
    private static final String port = ConfigUtils.INSTANCE.getStringValue("vedantudata.redshift.port");
    private static final String database = ConfigUtils.INSTANCE.getStringValue("vedantudata.redshift.database");
    private static final String user = ConfigUtils.INSTANCE.getStringValue("vedantudata.redshift.user");
    private static final String password = ConfigUtils.INSTANCE.getStringValue("vedantudata.redshift.password");
    private Connection SINGLE_CONNECTION_INSTANCE;

    @Autowired
    private LogFactory logFactory;
    private Logger logger = logFactory.getLogger(VedantudataPostgressHandler.class);

    public VedantudataPostgressHandler() throws SQLException {

        loadConfig();
    }

    private void loadConfig() throws SQLException {

        if (SINGLE_CONNECTION_INSTANCE == null || SINGLE_CONNECTION_INSTANCE.isClosed()) {
            String url = jdbcPrefix + host + ":" + port + "/" + database;

            try {
                Class.forName("org.postgresql.Driver");
                SINGLE_CONNECTION_INSTANCE = DriverManager.getConnection(url, user, password);

            } catch (SQLException | ClassNotFoundException ex) {
                logger.error(ex.getMessage());
            }
        }
    }

    public ResultSet executeQuery(String query) throws SQLException, VException {

        loadConfig();


        try {
            Statement st = SINGLE_CONNECTION_INSTANCE.createStatement();
            ResultSet rs = st.executeQuery(query);
            return rs;
        } catch (Exception ex) {
            logger.error(ex);
            throw new VException(ErrorCode.SERVICE_ERROR, "Unable to connect redshift database..");
        }
    }
}
