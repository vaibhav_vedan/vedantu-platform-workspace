package com.vedantu.vedantudata.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class LeadSquaredUtils {

    /*
    custom function for validating Leadsquared emails
    for example:abc123.@gmail is an invalid mail in leadsquared
     */
    public static Boolean validateLSEmails(String validLSEmail) {
        try {
            String emailRegex = "^[a-zA-Z0-9_+&*-]+(?:\\." +
                    "[a-zA-Z0-9_+&*-]+)*@" +
                    "(?:[a-zA-Z0-9-]+\\.)+[a-z" +
                    "A-Z]{2,7}$";

            Pattern pattern = Pattern.compile(emailRegex);
            Matcher matcher = pattern.matcher(validLSEmail);
            return matcher.matches();
        } catch (Exception e) {
            return true;

        }

    }
}
