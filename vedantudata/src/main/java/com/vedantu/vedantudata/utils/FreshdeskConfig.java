package com.vedantu.vedantudata.utils;

import javax.validation.constraints.NotNull;
import java.net.URLEncoder;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.function.BiFunction;
import java.util.function.Function;

public final class FreshdeskConfig {
    public static final String API_BASE_URL = "https://vedantuinnovations.freshdesk.com";
    public static final String GET_LIST_TICKETS = "/api/v2/tickets";
    public static final String GET_SEARCH_TICKETS = "/api/v2/search/tickets";
    public static final String GET_VIEW_CONTACT = "/api/v2/contacts/";
    public static final String GET_VIEW_AGENTS = "/api/v2/agents/";

    public static final Function<Long, String> PARAM_UPDATED_SINCE_LAST_N_MIN = (n) -> "updated_since=" + OffsetDateTime.now(ZoneOffset.UTC)
            .minus(n, ChronoUnit.MINUTES).format(DateTimeFormatter.ISO_LOCAL_DATE_TIME) + "Z";

    public static final Function<Integer, String> PARAM_PAGE_N = (n) -> "page=" + n;

    public static final Function<Integer, String> PARAM_PER_PAGE_N = (n) -> "per_page=" + n;

    public static final BiFunction<String, Object, String> PARAM_SEARCH = (col, val) -> {
        String value;
        if (val instanceof Number) {
            value = val.toString();
        } else {
            value = "'" + val + "'";
        }
        return "query="  + URLEncoder.encode("\"" + col + ":" + value + "\"") ;
    };


    public static final Function<String[], String> PARAM_INCLUDE = (e) -> "include=" + String.join(",", e);

    public static final String PARAM_ORDER_BY = "order_by=";
}
