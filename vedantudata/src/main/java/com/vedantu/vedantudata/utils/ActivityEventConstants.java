package com.vedantu.vedantudata.utils;

import java.util.Arrays;
import java.util.List;

public class ActivityEventConstants {

    public static final Integer BATCH_SIZE = 100;

    public static final int TEST_NUMBER_OF_TESTS_FOR_PERFORMANCE = 3;

    //AM ACTIVITY
    public static final String ACTIVITY_ALL_AM_TRIGGERS = "ACTIVITY_ALL_AM_TRIGGERS";
    public static final String ACTIVITY_MISSED_CLASS = "ACTIVITY_MISSED_CLASS";
    public static final String ACTIVITY_ZERO_ATTENDANCE = "ACTIVITY_ZERO_ATTENDANCE";
    public static final String ACTIVITY_TEST = "ACTIVITY_TEST";
    public static final String ACTIVITY_ENGAGEMENT = "ACTIVITY_ENGAGEMENT";

    //CARE ACTIVITY
    public static final String ACTIVITY_SAM_ALLOCATION = "ACTIVITY_SAM_ALLOCATION";
    public static final String ACTIVITY_BANK_REFUND = "ACTIVITY_BANK_REFUND";

    //Event ids
    public static final int EVENT_MISSED_CLASS = 337;
    public static final int EVENT_ZERO_ATTENDANCE = 338;
    public static final int EVENT_TEST = 339;
    public static final int EVENT_STUDENT_ENGAGEMENT = 341;
    public static final int EVENT_PARENT_ENGAGEMENT = 342;
    public static final int EVENT_GRIEVANCE = 340;
    public static final int EVENT_OPEN_TICKETS = 353;
    public static final int EVENT_SAM_ALLOCATION = 350;
    public static final int EVENT_BANK_REFUND = 351;
    public static final int EVENT_INSTALLMENT_REMINDER = 352;
    public static final int EVENT_ORDERS = 270;

    public static final List<Integer> ACTIVITY_IDS_LIST = Arrays.asList(EVENT_MISSED_CLASS, EVENT_ZERO_ATTENDANCE, EVENT_TEST, EVENT_STUDENT_ENGAGEMENT, EVENT_PARENT_ENGAGEMENT);

    public static final int FORM_SALES_TEAM_CONFIRMATION = 331;
    public static final int FORM_VERIFICATION_STATUS = 332;
    public static final int FORM_INTRODUCTION_TEAM = 333;
    public static final int FORM_DEMO_TEAM = 334;
    public static final int FORM_FINTECH = 335;
    public static final int FORM_ENROLLMENT_TEAM = 336;

    //Activity Note
    public static final String ACTIVITY_EVENT_MISSED_CLASS_NOTE = "Student Missed the Class";
    public static final String ACTIVITY_EVENT_ZERO_ATTENDANCE_NOTE = "Student has not attended sessions";
    public static final String ACTIVITY_EVENT_TEST_NOTE_MISSED_TEST = "Student Missed the Test";
    public static final String ACTIVITY_EVENT_STUDENT_ENGAGEMENT_NOTE = "";
    public static final String ACTIVITY_EVENT_PARENT_ENGAGEMENT_NOTE = "";
    public static final String ACTIVITY_EVENT_EARLY_EDUCATION_PARENT_ENGAGEMENT_NOTE = "";
    public static final String ACTIVITY_EVENT_TEST_HT = "Students Falls under HT";
    public static final String ACTIVITY_EVENT_TEST_MT = "Students Falls under MT";
    public static final String ACTIVITY_EVENT_TEST_LT = "Students Falls under LT";
    public static final String ACTIVITY_EVENT_TEST_REGULAR = "Regular engagement for the higher grade students";
    public static final String ACTIVITY_EVENT_CARE_SAM = "SAM Allocation";

    //Attributes
    public static final String MX_CUSTOM = "mx_Custom";
    public static final String STATUS = "Status";
    public static final String OWNER = "Owner";
    public static final String MX_SAM_OWNER = "mx_SAM_Owner";
    public static final String EMAIL_ADDRESS = "EMAIL_ADDRESS";
    public static final String MX_CUSTOM_1 = MX_CUSTOM + "_1";
    public static final String MX_CUSTOM_2 = MX_CUSTOM + "_2";
    public static final String MX_CUSTOM_3 = MX_CUSTOM + "_3";
    public static final String MX_CUSTOM_4 = MX_CUSTOM + "_4";
    public static final String MX_CUSTOM_5 = MX_CUSTOM + "_5";
    public static final String MX_CUSTOM_6 = MX_CUSTOM + "_6";
    public static final String MX_CUSTOM_7 = MX_CUSTOM + "_7";
    public static final String MX_CUSTOM_8 = MX_CUSTOM + "_8";
    public static final String MX_CUSTOM_9 = MX_CUSTOM + "_9";
    public static final String MX_CUSTOM_10 = MX_CUSTOM + "_10";
    public static final String MX_CUSTOM_11 = MX_CUSTOM + "_11";
    public static final String MX_CUSTOM_12 = MX_CUSTOM + "_12";
    public static final String MX_CUSTOM_13 = MX_CUSTOM + "_13";
    public static final String MX_CUSTOM_14 = MX_CUSTOM + "_14";
    public static final String MX_CUSTOM_15 = MX_CUSTOM + "_15";

    //timestamp

    public static final Long MILLI_ORDER_NOT_PAID_TIMEOUT = 1800000L;
    public static final Long MILLI_MINUTE = 60000L;
    public static final Long MILLI_HOUR = 3600000L;
    public static final Long MILLI_DAY = 86400000L;
    public static final Long MILLI_WEEK = 1546281000000L;
    public static final Long MILLI_MONTH = 2635200000L;

    public static final String SIMPLE_DATE_FORMAT = "dd-MM-yyyy hh:mm:ss";
    public static final String LEAD_SQUARED_DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";
    public static final int ENGAGEMENT_ACTIVITY_DEFAULT_CHUNK_SIZE = 10;

    public static final int NO_OF_SUBJECTS = 3;

    //Attendance
    public static final String STUDENT_TYPE = "studentType";
    public static final String STUDENT_TYPE_NEW = "New Students";
    public static final String STUDENT_TYPE_REGULAR = "Regular Students";

    //Test
    public static final String MISSED_TEST = "missedTest";
    public static final String TEST_LEVEL = "testlevel";
    public static final String LAST_SCORE = "lastScore";

    //Missed class
    public static final String GRADE_VALUE = "gradeValue";
    public static final String NOTE = "note";

    //Engagement
    public static final String NO_OF_DAYS = "noOfDays";
    public static final int LS_ACTIVITY_CHUNK_SIZE = 25;
    public static final int ORDER_CHUNK_SIZE = 100;

    public static final String CARE_COURSE_TYPE_OTM = "OTM";
    public static final String CARE_COURSE_TYPE_OTO = "OTO";

    public static String getTestLevelBypercentage(Double average) {
        if (average < 60) {
            return "HT";
        }
        if (average < 80) {
            return "MT";
        }
        return "LT";
    }

    public static String getTestLevel(int grade, Double average) {
        return "Class - " + grade + " " + getTestLevelBypercentage(average);
    }

    //grade dropdown value
    public static String getGradeValue(String grade) {

        if (grade == null || grade.isEmpty()) {
            return "";
        }
        if (grade.contains("6")) {
            return "Class - 6 (Missed Last 2 Sessions)";
        }
        if (grade.contains("7")) {
            return "Class - 7 (Missed Last 2 Sessions)";
        }
        if (grade.contains("8")) {
            return "Class - 8 (Missed Last 2 Sessions)";
        }
        if (grade.contains("9")) {
            return "Class - 9 (Missed Last 3 Sessions)";
        }
        if (grade.contains("10")) {
            return "Class - 10 (Missed Last 3 Sessions)";
        }
        if (grade.contains("11")) {
            return "Class - 11 (Missed Last 4 Sessions)";
        }
        if (grade.contains("12")) {
            return "Class - 12 (Missed Last 4 Sessions)";
        }
        if (grade.contains("13")) {
            return "Class - 13 (Missed Last 4 Sessions)";
        }

        return "";
    }

}
