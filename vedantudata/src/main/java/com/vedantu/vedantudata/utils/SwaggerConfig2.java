package com.vedantu.vedantudata.utils;

import com.vedantu.util.ConfigUtils;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import org.springframework.retry.annotation.EnableRetry;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableRetry
@EnableSwagger2
public class SwaggerConfig2 {

    private boolean environmentSpeficicBooleanFlag = true;

    public SwaggerConfig2() {
        String env = ConfigUtils.INSTANCE.getStringValue("environment");
        if (env.equals("PROD")) {
//            environmentSpeficicBooleanFlag = false;
        }
    }

    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.any())
                .paths(PathSelectors.any())
                .build().enable(environmentSpeficicBooleanFlag);
    }
}
