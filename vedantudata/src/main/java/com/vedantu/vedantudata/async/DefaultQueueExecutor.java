/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.vedantudata.async;

import com.vedantu.async.AsyncTaskParams;
import com.vedantu.async.IAsyncQueueExecutor;

import com.vedantu.util.LogFactory;

import java.util.List;
import java.util.Map;

import com.vedantu.vedantudata.entities.leadsquared.AssignedLeads;
import com.vedantu.vedantudata.entities.leadsquared.LeadActualAssignment;
import com.vedantu.vedantudata.managers.*;
import com.vedantu.vedantudata.managers.leadsquared.LeadSquaredAgentDataManger;
import com.vedantu.vedantudata.utils.ActivityEventConstants;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Recover;
import org.springframework.retry.annotation.Retryable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

/**
 *
 * @author somil
 */
@Service
public class DefaultQueueExecutor implements IAsyncQueueExecutor {

    @Autowired
    public LogFactory logFactory;

    @Autowired
    private VedantuManager vedantuManager;

    @Autowired
    private ActivityManager activityManager;

    @Autowired
    private BankRefundTriggerManager bankRefundTriggerManager;

    @Autowired
    private InstallmentTriggerManager installmentTriggerManager;

    @Autowired
    private NotPaidOrdersTriggerManager notPaidOrdersTriggerManager;

    @Autowired
    private LeadSquaredAgentDataManger leadSquaredAgentDataManger;

    @Autowired
    private LeadsAutoAssignmentManager leadsAutoAssignmentManager;


    @SuppressWarnings("static-access")
    public Logger logger = logFactory.getLogger(DefaultQueueExecutor.class);

    @Async("taskExecutor")
    @Retryable(maxAttempts = 1, backoff = @Backoff(delay = 100, maxDelay = 500))
    @Override
    public void execute(AsyncTaskParams params) throws Exception {
        if (!(params.getAsyncTaskName() instanceof AsyncTaskName)) {
            logger.error(params.getAsyncTaskName() + " not supported");
            return;
        }
        AsyncTaskName taskName = (AsyncTaskName) params.getAsyncTaskName();
        Map<String, Object> payload = params.getPayload();
        switch (taskName) {
            case WEBINAR_REGISTER_VIA_MISSED_CALL:
                String mobileno = (String) payload.get("mobileno");
                String trainingId = (String) payload.get("trainingId");
                String smstext = null;
                if (payload.get("smstext") != null) {
                    smstext = (String) payload.get("smstext");
                }
                vedantuManager.registerviamissedcall(mobileno, trainingId, smstext);
                break;
            case WEBINAR_REMINDER_SMS_SCHEDULER:
                vedantuManager.webinarReminderScheduler();
                break;
            case WEBINAR_REMINDER_SMS:
                String smstrainingId = (String) payload.get("trainingId");
                String webinarTitle = (String) payload.get("webinarTitle");
                Integer start = (Integer) payload.get("start");
                Integer size = (Integer) payload.get("size");
                vedantuManager.webinarReminder(webinarTitle, smstrainingId, start, size);
                break;
            case TRIGGER_LS_AM_ACTIVITIES:
                activityManager.triggerAMActivities();
                break;
            case TRIGGER_LS_CARE_ACTIVITIES_SAM_ALLOCATION:
                    activityManager.triggerCareActivity(ActivityEventConstants.ACTIVITY_SAM_ALLOCATION);
                break;
            case TRIGGER_LS_GRIEVENCE_ACTIVITIES:
                activityManager.triggerFreshdeskTicketActivityPushToQueue();
                break;
            case TRIGGER_LS_CARE_BANK_REFUND_ACTIVITIES:
                bankRefundTriggerManager.process();
                break;
            case TRIGGER_LS_CARE_INSTALLMENT_ACTIVITIES:
                installmentTriggerManager.process();
                break;
            case TRIGGER_LS_NOT_PAID_ORDERS_ACTIVITIES:
                notPaidOrdersTriggerManager.process();
                break;
            case TRIGGER_LS_USERS_DATA_MIGRATION:
                leadSquaredAgentDataManger.executeDataMigrationUpdationOfLeadSquaredAgents();
                break;
            case TRIGGER_AUTO_LEAD_ASSIGNMENT:
                leadsAutoAssignmentManager.assignLeadstoCounsellors();
                break;
            case TRIGGER_AUTO_LEAD_ASSIGNMENT_DB:
                List<AssignedLeads> assignedLeads = (List<AssignedLeads>) payload.get("assignLead");
                leadsAutoAssignmentManager.pushAssignedleadsToDb(assignedLeads);
                break;
        }
    }

    @Recover
    @Override
    public void recover(Exception exception) {
        logger.error("exception thrown in async task" + Thread.currentThread().getName(), exception);
    }

}
