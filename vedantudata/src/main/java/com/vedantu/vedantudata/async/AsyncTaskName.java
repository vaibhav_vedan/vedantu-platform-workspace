/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.vedantudata.async;

import com.vedantu.async.IAsyncTaskName;

/**
 *
 * @author somil
 */
public enum AsyncTaskName implements IAsyncTaskName {

    WEBINAR_REGISTER_VIA_MISSED_CALL(AsyncQueueName.DEFAULT_QUEUE),
    WEBINAR_REMINDER_SMS_SCHEDULER(AsyncQueueName.DEFAULT_QUEUE),
    WEBINAR_REMINDER_SMS(AsyncQueueName.DEFAULT_QUEUE),
    TRIGGER_LS_AM_ACTIVITIES(AsyncQueueName.DEFAULT_QUEUE),
    TRIGGER_LS_CARE_BANK_REFUND_ACTIVITIES(AsyncQueueName.DEFAULT_QUEUE),
    TRIGGER_LS_CARE_INSTALLMENT_ACTIVITIES(AsyncQueueName.DEFAULT_QUEUE),
    TRIGGER_LS_NOT_PAID_ORDERS_ACTIVITIES(AsyncQueueName.DEFAULT_QUEUE),
    TRIGGER_LS_CARE_ACTIVITIES_SAM_ALLOCATION(AsyncQueueName.DEFAULT_QUEUE),
    TRIGGER_LS_GRIEVENCE_ACTIVITIES(AsyncQueueName.DEFAULT_QUEUE),
    TRIGGER_LS_USERS_DATA_MIGRATION(AsyncQueueName.DEFAULT_QUEUE),
    TRIGGER_AUTO_LEAD_ASSIGNMENT(AsyncQueueName.DEFAULT_QUEUE),
    TRIGGER_AUTO_LEAD_ASSIGNMENT_DB(AsyncQueueName.DEFAULT_QUEUE);

    private AsyncQueueName queue;

    private AsyncTaskName(AsyncQueueName queue) {
        this.queue = queue;
    }

    private AsyncTaskName() {
        this.queue = AsyncQueueName.DEFAULT_QUEUE;
    }

    @Override
    public AsyncQueueName getQueue() {
        return queue;
    }

}
