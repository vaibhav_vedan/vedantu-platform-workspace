package com.vedantu.vedantudata.pojos;

public enum UserEmailStatus {
	UNKNOWN, VALID, INVALID, BOUNCED
}
