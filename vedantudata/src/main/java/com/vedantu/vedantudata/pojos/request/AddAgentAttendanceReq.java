package com.vedantu.vedantudata.pojos.request;

import com.amazonaws.services.dynamodbv2.xspec.L;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.StringUtils;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import com.vedantu.vedantudata.entities.salesconversion.CentreInfo;
import com.vedantu.vedantudata.enums.salesconversion.AgentAttendanceStatus;
import lombok.Data;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

@Data
public class AddAgentAttendanceReq extends AbstractFrontEndReq {
    private List<AgentAttendancePojo> agentAttendanceList;

    protected List<String> validate() {
        List<String> errors = new ArrayList<>();
        if (ArrayUtils.isEmpty(agentAttendanceList)) {
            errors.add("agentAttendanceList should not be empty");
        } else {
            int cnt = 1;
            for (AgentAttendancePojo attendancePojo : agentAttendanceList) {
                if (StringUtils.isEmpty(attendancePojo.getAgentId()) || StringUtils.isEmpty(attendancePojo.getAgentId().trim())) {
                    errors.add("agentId is missing for record: " + cnt);
                }
                if (StringUtils.isEmpty(attendancePojo.getDate()) || StringUtils.isEmpty(attendancePojo.getDate().trim())) {
                    errors.add("Date is missing for record: " + cnt);
                } else {
                    try {
                        Date currentDate = new Date();
                        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        format.setLenient(false);
                        Date givenDate = format.parse(attendancePojo.getDate().trim());
                        if (!currentDate.after(givenDate)) {
                            errors.add("Date cannot be future date. check for record : " + cnt);
                        }
                    } catch (ParseException pe) {
                        errors.add("Date not in proper format for record: " + cnt);
                    }

                }
                if (attendancePojo.getAttendanceStatus() != null) {
                    if (!AgentAttendancePojo.checkValidAttendanceStatus(attendancePojo.getAttendanceStatus())){
                        errors.add("Attendance status is invalid for reord " + cnt);
                    }
                }
                cnt++;
            }
        }

        return errors;
    }
}

