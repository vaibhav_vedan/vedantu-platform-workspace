package com.vedantu.vedantudata.pojos;

import java.lang.reflect.Field;

import javax.persistence.Transient;

import com.vedantu.vedantudata.googlesheets.IGoogleSheetEntity;

public class WebinarReport implements IGoogleSheetEntity {
	private String userId;
	private String bundleId;
	private long sessionsScheduled;
	private long sessionsAttended;
	private long testsShared;
	private long testsAttempted;

	@Transient
	private static String[] GOOGLE_SHEET_FIELDS = { "userId", "bundleId", "sessionsScheduled", "sessionsAttended", "testsShared", "testsAttempted"};

	public WebinarReport() {
		super();
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getBundleId() {
		return bundleId;
	}

	public void setBundleId(String bundleId) {
		this.bundleId = bundleId;
	}

	public long getSessionsScheduled() {
		return sessionsScheduled;
	}

	public void setSessionsScheduled(long sessionsScheduled) {
		this.sessionsScheduled = sessionsScheduled;
	}

	public long getSessionsAttended() {
		return sessionsAttended;
	}

	public void setSessionsAttended(long sessionsAttended) {
		this.sessionsAttended = sessionsAttended;
	}

	public long getTestsShared() {
		return testsShared;
	}

	public void setTestsShared(long testsShared) {
		this.testsShared = testsShared;
	}

	public long getTestsAttempted() {
		return testsAttempted;
	}

	public void setTestsAttempted(long testsAttempted) {
		this.testsAttempted = testsAttempted;
	}

	@Override
	public String[] fetchGoogleSheetFields() {
		return GOOGLE_SHEET_FIELDS;
	}

	@Override
	public String[] fetchGoogleSheetValues() {
		String[] fields = fetchGoogleSheetFields();
		String[] values = new String[fields.length];
		for (int i = 0; i < fields.length; i++) {
			try {
				Field field = this.getClass().getDeclaredField(fields[i]);
				field.setAccessible(true);
				values[i] = String.valueOf(field.get(this));
			} catch (IllegalArgumentException | IllegalAccessException | NoSuchFieldException | SecurityException ex) {
				// Dont throw
			}
		}
		return values;
	}
}
