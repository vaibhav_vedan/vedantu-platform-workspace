/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.vedantudata.pojos;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author sanketj
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class LeadSquaredActivityFields {

    private String mx_Custom_1;
    private String mx_Custom_2;
    private String mx_Custom_3;
    private String mx_Custom_4;
    private String mx_Custom_5;
    private String mx_Custom_6;
    private String mx_Custom_7;
    private String mx_Custom_8;
    private String mx_Custom_9; //course mrp
    private String mx_Custom_10;//offered price
    private String mx_Custom_11;//Payment type
    private String mx_Custom_12;
    private String mx_Custom_13;//downpayment amount
    private String mx_Custom_22;//installment count
    private String mx_Custom_23;//installment amount
    private String mx_Custom_38;//agent employee id

}
