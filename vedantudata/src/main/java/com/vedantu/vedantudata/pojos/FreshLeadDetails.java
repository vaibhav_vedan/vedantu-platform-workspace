package com.vedantu.vedantudata.pojos;

import lombok.Data;

@Data
public class FreshLeadDetails {
    private String leadScore;
    private String grade;
    private String language;
    private String firstName;
    private String phone;
    private String prospectId;
    private String managerUserId;
    private Boolean isFreshLead;
    private Boolean isStateBoard = false;
    private String assignmentType;
    private String centreCode;
    private String ownerDesignation;
    private String mxCoreNonCore;
    private String scoreId;
    private String studentInfoBoard;
    private String location;
}
