package com.vedantu.vedantudata.pojos.freshdesk;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import lombok.Data;

@Data
public class Fields {

    @SerializedName("department")
    @Expose
    public String department;
    @SerializedName("fb_profile")
    @Expose
    public Object fbProfile;
    @SerializedName("permanent")
    @Expose
    public Boolean permanent;

}