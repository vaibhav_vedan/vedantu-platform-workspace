package com.vedantu.vedantudata.pojos;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AcadMentorStudents {

    private Long acadMentorId;
    private Long studentId;
}
