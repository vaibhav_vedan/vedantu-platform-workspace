package com.vedantu.vedantudata.pojos.response;


import com.vedantu.vedantudata.entities.salesconversion.CentreInfo;
import com.vedantu.vedantudata.entities.salesconversion.SalesAgentAttendanceInfo;
import com.vedantu.vedantudata.entities.salesconversion.SalesAgentInfo;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class SalesAgentAttendanceDataRes {

    private List<AgentAttendanceMetadataRes> agentAttendanceData = new ArrayList<>();
    private List<CentreInfo> centreInfos;
    private List<SalesAgentInfo> tlInfos;
    private List<SalesAgentInfo> rmInfos;
    private List<SalesAgentInfo> isFMInfos;
    private List<SalesAgentInfo> isRMInfos;
    private Integer salesAgentInfoCount = 0;
    private Map<String, List<SalesAgentAttendanceInfo>> agentToAttendanceListMap;
}
