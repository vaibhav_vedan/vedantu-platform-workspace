package com.vedantu.vedantudata.pojos;

import java.lang.reflect.Field;

import javax.persistence.Transient;

import com.vedantu.User.UserBasicInfo;
import com.vedantu.dinero.enums.PaymentStatus;
import com.vedantu.dinero.enums.PaymentType;
import com.vedantu.dinero.enums.PurchaseFlowType;
import com.vedantu.dinero.pojo.OrderedItem;
import com.vedantu.dinero.pojo.Orders;
import com.vedantu.util.CollectionUtils;
import com.vedantu.vedantudata.googlesheets.IGoogleSheetEntity;
import com.vedantu.vedantudata.managers.VedantuManager;

public class VedantuDataOrder implements IGoogleSheetEntity {
	private String orderId;
	private Long userId;
	private Integer amount;
	private Integer promotionalAmount;
	private Integer nonPromotionalAmount;
	private Integer amountPaid;
	private Integer promotionalAmountPaid;
	private Integer nonPromotionalAmountPaid;
	private PaymentStatus paymentStatus;
	private PaymentType paymentType = PaymentType.BULK;
	private String transactionId;
	private Long contextId;
	private Integer convenienceCharge;
	private PurchaseFlowType purchaseFlowType = PurchaseFlowType.DIRECT;
	private String purchaseFlowId;

	private String userEmailId;
	private String userName;
	private String userPhone;
	private String userGrade;
	private String entityType;
	private String entityId;
	private long creationTime;
	private String creationTimeStr;
	private String lastUpdated;
	private String lastUpdatedStr;

	// Additional fields
	private String agentName;
	private long totalAmountPaid;
	private long gmvPotential;
	private String firstTranasactionTime;
	private String deliverableEntityType;
	private String deliverableEntityId;

	@Transient
	private static String[] GOOGLE_SHEET_FIELDS = { "orderId", "userId", "userEmailId", "userPhone", "userGrade",
			"amount", "amountPaid", "entityType", "entityId", "paymentStatus", "paymentType", "creationTimeStr",
			"lastUpdated", "lastUpdatedStr", "agentName", "totalAmountPaid", "gmvPotential", "firstTranasactionTime",
			"deliverableEntityType", "deliverableEntityId" };

	public VedantuDataOrder() {
		super();
	}

	public VedantuDataOrder(Orders order, UserBasicInfo userBasicInfo, int orderItemIndex) {
		super();
		this.orderId = order.getId();
		this.userId = order.getUserId();
		this.amount = order.getAmount();
		this.promotionalAmount = order.getPromotionalAmount();
		this.nonPromotionalAmount = order.getNonPromotionalAmount();
		this.amountPaid = order.getAmountPaid();
		this.promotionalAmountPaid = order.getPromotionalAmountPaid();
		this.nonPromotionalAmountPaid = order.getNonPromotionalAmountPaid();
		this.paymentStatus = order.getPaymentStatus();
		this.paymentType = order.getPaymentType();
		this.transactionId = order.getTransactionId();
		this.contextId = order.getContextId();
		this.convenienceCharge = order.getConvenienceCharge();
		this.purchaseFlowId = order.getPurchaseFlowId();
		this.userEmailId = userBasicInfo.getEmail();
		this.userName = userBasicInfo.getFullName();
		this.userPhone = userBasicInfo.getContactNumber();
		this.userGrade = userBasicInfo.getGrade();
		if (!CollectionUtils.isEmpty(order.getItems()) && order.getItems().size() > orderItemIndex) {
			OrderedItem item = order.getItems().get(orderItemIndex);
			this.entityType = String.valueOf(item.getEntityType());
			this.entityId = String.valueOf(item.getEntityId());
			this.deliverableEntityId = String.valueOf(item.getDeliverableEntityId());
			this.deliverableEntityType = String.valueOf(item.getDeliverableEntityType());
		}
		this.creationTime = order.getCreationTime();
		this.creationTimeStr = VedantuManager.calculateDateIst(order.getCreationTime());
		this.lastUpdated = String.valueOf(order.getLastUpdated());
		this.lastUpdatedStr = VedantuManager.calculateDateIst(order.getLastUpdated());
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Integer getAmount() {
		return amount;
	}

	public void setAmount(Integer amount) {
		this.amount = amount;
	}

	public Integer getPromotionalAmount() {
		return promotionalAmount;
	}

	public void setPromotionalAmount(Integer promotionalAmount) {
		this.promotionalAmount = promotionalAmount;
	}

	public Integer getNonPromotionalAmount() {
		return nonPromotionalAmount;
	}

	public void setNonPromotionalAmount(Integer nonPromotionalAmount) {
		this.nonPromotionalAmount = nonPromotionalAmount;
	}

	public Integer getAmountPaid() {
		return amountPaid;
	}

	public void setAmountPaid(Integer amountPaid) {
		this.amountPaid = amountPaid;
	}

	public Integer getPromotionalAmountPaid() {
		return promotionalAmountPaid;
	}

	public void setPromotionalAmountPaid(Integer promotionalAmountPaid) {
		this.promotionalAmountPaid = promotionalAmountPaid;
	}

	public Integer getNonPromotionalAmountPaid() {
		return nonPromotionalAmountPaid;
	}

	public void setNonPromotionalAmountPaid(Integer nonPromotionalAmountPaid) {
		this.nonPromotionalAmountPaid = nonPromotionalAmountPaid;
	}

	public PaymentStatus getPaymentStatus() {
		return paymentStatus;
	}

	public void setPaymentStatus(PaymentStatus paymentStatus) {
		this.paymentStatus = paymentStatus;
	}

	public PaymentType getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(PaymentType paymentType) {
		this.paymentType = paymentType;
	}

	public String getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	public Long getContextId() {
		return contextId;
	}

	public void setContextId(Long contextId) {
		this.contextId = contextId;
	}

	public Integer getConvenienceCharge() {
		return convenienceCharge;
	}

	public void setConvenienceCharge(Integer convenienceCharge) {
		this.convenienceCharge = convenienceCharge;
	}

	public PurchaseFlowType getPurchaseFlowType() {
		return purchaseFlowType;
	}

	public void setPurchaseFlowType(PurchaseFlowType purchaseFlowType) {
		this.purchaseFlowType = purchaseFlowType;
	}

	public String getPurchaseFlowId() {
		return purchaseFlowId;
	}

	public void setPurchaseFlowId(String purchaseFlowId) {
		this.purchaseFlowId = purchaseFlowId;
	}

	public String getUserEmailId() {
		return userEmailId;
	}

	public void setUserEmailId(String userEmailId) {
		this.userEmailId = userEmailId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUserPhone() {
		return userPhone;
	}

	public void setUserPhone(String userPhone) {
		this.userPhone = userPhone;
	}

	public String getUserGrade() {
		return userGrade;
	}

	public void setUserGrade(String userGrade) {
		this.userGrade = userGrade;
	}

	public String getEntityType() {
		return entityType;
	}

	public void setEntityType(String entityType) {
		this.entityType = entityType;
	}

	public String getEntityId() {
		return entityId;
	}

	public void setEntityId(String entityId) {
		this.entityId = entityId;
	}

	public long getCreationTime() {
		return creationTime;
	}

	public void setCreationTime(long creationTime) {
		this.creationTime = creationTime;
	}

	public String getCreationTimeStr() {
		return creationTimeStr;
	}

	public void setCreationTimeStr(String creationTimeStr) {
		this.creationTimeStr = creationTimeStr;
	}

	public String getLastUpdated() {
		return lastUpdated;
	}

	public void setLastUpdated(String lastUpdated) {
		this.lastUpdated = lastUpdated;
	}

	public String getLastUpdatedStr() {
		return lastUpdatedStr;
	}

	public void setLastUpdatedStr(String lastUpdatedStr) {
		this.lastUpdatedStr = lastUpdatedStr;
	}

	public String getAgentName() {
		return agentName;
	}

	public void setAgentName(String agentName) {
		this.agentName = agentName;
	}

	public long getTotalAmountPaid() {
		return totalAmountPaid;
	}

	public void setTotalAmountPaid(long totalAmountPaid) {
		this.totalAmountPaid = totalAmountPaid;
	}

	public long getGmvPotential() {
		return gmvPotential;
	}

	public void setGmvPotential(long gmvPotential) {
		this.gmvPotential = gmvPotential;
	}

	public String getFirstTranasactionTime() {
		return firstTranasactionTime;
	}

	public void setFirstTranasactionTime(String firstTranasactionTime) {
		this.firstTranasactionTime = firstTranasactionTime;
	}

	public String getDeliverableEntityType() {
		return deliverableEntityType;
	}

	public void setDeliverableEntityType(String deliverableEntityType) {
		this.deliverableEntityType = deliverableEntityType;
	}

	public String getDeliverableEntityId() {
		return deliverableEntityId;
	}

	public void setDeliverableEntityId(String deliverableEntityId) {
		this.deliverableEntityId = deliverableEntityId;
	}

	@Override
	public String[] fetchGoogleSheetFields() {
		return GOOGLE_SHEET_FIELDS;
	}

	@Override
	public String[] fetchGoogleSheetValues() {
		String[] fields = fetchGoogleSheetFields();
		String[] values = new String[fields.length];
		for (int i = 0; i < fields.length; i++) {
			try {
				Field field = this.getClass().getDeclaredField(fields[i]);
				field.setAccessible(true);
				values[i] = String.valueOf(field.get(this));
			} catch (IllegalArgumentException | IllegalAccessException | NoSuchFieldException | SecurityException ex) {
				// Dont throw
			}
		}
		return values;
	}

	@Override
	public String toString() {
		return "VedantuDataOrder [orderId=" + orderId + ", userId=" + userId + ", amount=" + amount
				+ ", promotionalAmount=" + promotionalAmount + ", nonPromotionalAmount=" + nonPromotionalAmount
				+ ", amountPaid=" + amountPaid + ", promotionalAmountPaid=" + promotionalAmountPaid
				+ ", nonPromotionalAmountPaid=" + nonPromotionalAmountPaid + ", paymentStatus=" + paymentStatus
				+ ", paymentType=" + paymentType + ", transactionId=" + transactionId + ", contextId=" + contextId
				+ ", convenienceCharge=" + convenienceCharge + ", purchaseFlowType=" + purchaseFlowType
				+ ", purchaseFlowId=" + purchaseFlowId + ", userEmailId=" + userEmailId + ", userName=" + userName
				+ ", userPhone=" + userPhone + ", userGrade=" + userGrade + ", entityType=" + entityType + ", entityId="
				+ entityId + ", creationTime=" + creationTime + ", creationTimeStr=" + creationTimeStr
				+ ", lastUpdated=" + lastUpdated + ", lastUpdatedStr=" + lastUpdatedStr + ", agentName=" + agentName
				+ ", totalAmountPaid=" + totalAmountPaid + ", gmvPotential=" + gmvPotential + ", firstTranasactionTime="
				+ firstTranasactionTime + ", deliverableEntityType=" + deliverableEntityType + ", deliverableEntityId="
				+ deliverableEntityId + ", toString()=" + super.toString() + "]";
	}
}
