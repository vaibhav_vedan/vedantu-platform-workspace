package com.vedantu.vedantudata.pojos.request;

//import com.sun.org.apache.xpath.internal.operations.Bool;
import com.vedantu.util.CollectionUtils;
import com.vedantu.util.StringUtils;
import com.vedantu.vedantudata.utils.CommonUtils;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AddAgentsEmailReq {

    private Set<String> teacherEmailIds;
    private Boolean isScreenAllowed;

    public List<String> validate(AddAgentsEmailReq addAgentsEmailReq) {
        List<String> errors = new ArrayList<>();
        if (CollectionUtils.isEmpty(addAgentsEmailReq.getTeacherEmailIds())) {
            errors.add("Payload data is empty..");
        } else if (addAgentsEmailReq.getTeacherEmailIds().size() > 100) {
            errors.add("100 emailIds allowed at a time");
        }
        if (addAgentsEmailReq.getIsScreenAllowed() == null) {
            errors.add("isScreenAllowed data is empty..");
        }
        return errors;
    }
}
