package com.vedantu.vedantudata.pojos.freshdesk;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import lombok.Data;

@Data
public class Contact {

@SerializedName("active")
@Expose
private Boolean active;
@SerializedName("created_at")
@Expose
private String createdAt;
@SerializedName("email")
@Expose
private String email;
@SerializedName("job_title")
@Expose
private String jobTitle;
@SerializedName("language")
@Expose
private String language;
@SerializedName("last_login_at")
@Expose
private String lastLoginAt;
@SerializedName("mobile")
@Expose
private String mobile;
@SerializedName("name")
@Expose
private String name;
@SerializedName("phone")
@Expose
private String phone;
@SerializedName("time_zone")
@Expose
private String timeZone;
@SerializedName("updated_at")
@Expose
private String updatedAt;

}