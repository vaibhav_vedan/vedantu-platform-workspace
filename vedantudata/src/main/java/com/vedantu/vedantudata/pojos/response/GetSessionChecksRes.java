package com.vedantu.vedantudata.pojos.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class GetSessionChecksRes {

    private Boolean isLiveClass;
    private Boolean isMasterClass;
    private Boolean isMicroCourse;
    private Boolean isFreeTrial;

}
