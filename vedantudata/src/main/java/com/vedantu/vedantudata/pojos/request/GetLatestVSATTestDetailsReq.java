package com.vedantu.vedantudata.pojos.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class GetLatestVSATTestDetailsReq {

    private Long userId;
    private Long startTime;
    private Long endTime;
}
