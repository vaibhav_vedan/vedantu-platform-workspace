package com.vedantu.vedantudata.pojos.freshdesk;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import lombok.Data;

@Data
public class Company {

    @SerializedName("company_id")
    @Expose
    private Long companyId;
    @SerializedName("view_all_tickets")
    @Expose
    private Boolean viewAllTickets;

}