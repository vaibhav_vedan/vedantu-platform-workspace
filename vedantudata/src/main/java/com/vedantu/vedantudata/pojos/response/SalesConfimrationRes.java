package com.vedantu.vedantudata.pojos.response;

import com.vedantu.dinero.pojo.DashBoardInstalmentInfo;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Map;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SalesConfimrationRes {

    private Integer price;
    private String subscriptionFee;
    private String downPayment;
    private String remainingAmount;
    private String installmentAmount;
    private String noOfInstallments;
    private List<String> installmentDates;
    private Boolean isBaseInstallmentAdded;
    private Map<String, List<DashBoardInstalmentInfo>> existingInstallmentInfo;
    private String agentEmployeeCode;
    private String couponCode;
    private String paymentType;
    private String medium;
}
