package com.vedantu.vedantudata.pojos;

import lombok.Data;

@Data
public class CounsellorDetails {
    private String counsellor_id;
    private String counsellor_email_id;
    private String cohort;
    private String grade_preference_1;
    private String grade_preference_2;
    private String grade_preference_3;
    private String language_preference_1;
    private String language_preference_2;
    private String lower_limit_of_lead_score;
    private String upper_limit_of_lead_score;
    private String lead_type_preference_1;
    private String lead_type_preference_2;
    private String lead_type_preference_3;
    private String day_wise_requirement;
    private Integer assignLeads;
    private Integer buckentCount;
    private Integer stateBoardLeadsCount = 0;
    private Integer freshLeadsAssigned = 0;
    private Integer churnLeadsAssigned = 0;
    private Integer nonStateBoardLeadsCount = 0;
    private String centreCode;
    private String defaultLanguage;
    private String locationPreference;

}
