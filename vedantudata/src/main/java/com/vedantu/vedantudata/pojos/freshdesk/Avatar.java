package com.vedantu.vedantudata.pojos.freshdesk;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import lombok.Data;

@Data
public class Avatar {

    @SerializedName("avatar_url")
    @Expose
    private String avatarUrl;
    @SerializedName("content_type")
    @Expose
    private String contentType;
    @SerializedName("id")
    @Expose
    private Long id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("size")
    @Expose
    private Long size;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;

}