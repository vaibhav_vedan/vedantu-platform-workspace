package com.vedantu.vedantudata.pojos;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GetLeadActivityResp {
	
	@SerializedName("RecordCount")
	private String recordCount;
	
	@SerializedName("ProspectActivities")
	private List<LeadActivitySerialized> prospectActivities;

	public String getRecordCount() {
		return recordCount;
	}

	public void setRecordCount(String recordCount) {
		this.recordCount = recordCount;
	}

	public List<LeadActivitySerialized> getProspectActivities() {
		return prospectActivities;
	}

	public void setProspectActivities(List<LeadActivitySerialized> prospectActivities) {
		this.prospectActivities = prospectActivities;
	}

}
