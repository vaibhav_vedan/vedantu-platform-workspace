package com.vedantu.vedantudata.pojos.request;

import com.vedantu.util.StringUtils;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import com.vedantu.vedantudata.enums.salesconversion.AgentAttendanceStatus;
import lombok.Data;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Data
public class AgentAttendancePojo extends AbstractFrontEndReq {
    private String agentId;
    private String attendanceStatus;
    private String date;

    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();
        if (StringUtils.isEmpty(agentId.trim())) {
            errors.add("agentId should not be null");
        }
        if (attendanceStatus != null) {
            checkValidAttendanceStatus(attendanceStatus);

            if (checkValidAttendanceStatus(attendanceStatus)){
                errors.add("Attendance status is invalid");
            }

        }
        if (StringUtils.isEmpty(date.trim())) {
            errors.add("date should not be null");
        }else{
            try {
                Date currentDate = new Date();
                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                format.setLenient(false);
                Date givenDate = format.parse(date.trim());
                if (!currentDate.after(givenDate)) {
                    errors.add("Date cannot be future date");
                }
            } catch (ParseException pe) {
                errors.add("Date not in proper format");
            }
        }
        return errors;
    }

    static Boolean checkValidAttendanceStatus(String attendanceStatus) {
        List<String> agentAttendanceStatuses = new ArrayList<>();
        agentAttendanceStatuses.add(AgentAttendanceStatus.WO.value);
        agentAttendanceStatuses.add(AgentAttendanceStatus.P.value);
        agentAttendanceStatuses.add(AgentAttendanceStatus.SB.value);
        agentAttendanceStatuses.add(AgentAttendanceStatus.HDL.value);
        agentAttendanceStatuses.add(AgentAttendanceStatus.UL.value);
        agentAttendanceStatuses.add(AgentAttendanceStatus.AL.value);
        agentAttendanceStatuses.add(AgentAttendanceStatus.SL.value);
        agentAttendanceStatuses.add(AgentAttendanceStatus.AB.value);
        agentAttendanceStatuses.add(AgentAttendanceStatus.DP.value);
        agentAttendanceStatuses.add(AgentAttendanceStatus.LOP.value);
        agentAttendanceStatuses.add(AgentAttendanceStatus.ML.value);
        agentAttendanceStatuses.add(AgentAttendanceStatus.PL.value);
        agentAttendanceStatuses.add(AgentAttendanceStatus.R.value);
        if (!agentAttendanceStatuses.contains(attendanceStatus)){
            return false;
        }
        return true;

    }
}
