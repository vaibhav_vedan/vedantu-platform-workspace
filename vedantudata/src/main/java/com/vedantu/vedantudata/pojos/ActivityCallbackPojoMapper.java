package com.vedantu.vedantudata.pojos;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;
import lombok.ToString;
//import lombok.*;

import java.util.List;
//
//@Getter
//@Setter
//@Data
//@AllArgsConstructor
//@NoArgsConstructor
@ToString
public class ActivityCallbackPojoMapper {

    @JsonFormat(with = JsonFormat.Feature.ACCEPT_CASE_INSENSITIVE_PROPERTIES)
    private ActivityDetailsCallbackPojo Before;

    @JsonFormat(with = JsonFormat.Feature.ACCEPT_CASE_INSENSITIVE_PROPERTIES)
    private ActivityDetailsCallbackPojo After;

    public ActivityDetailsCallbackPojo getBefore() {
        return Before;
    }

    public void setBefore(ActivityDetailsCallbackPojo before) {
        Before = before;
    }

    public ActivityDetailsCallbackPojo getAfter() {
        return After;
    }

    public void setAfter(ActivityDetailsCallbackPojo after) {
        After = after;
    }
}

