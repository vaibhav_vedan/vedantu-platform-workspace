package com.vedantu.vedantudata.pojos.request;

import com.vedantu.util.ArrayUtils;
import com.vedantu.util.StringUtils;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import com.vedantu.vedantudata.enums.salesconversion.AgentDesignation;
import com.vedantu.vedantudata.enums.salesconversion.AgentDesignationType;
import com.vedantu.vedantudata.enums.salesconversion.AgentStatus;
import com.vedantu.vedantudata.enums.salesconversion.SalesTeam;
import lombok.Data;

import java.util.*;

@Data
public class AddAgentsAsBulkReq extends AbstractFrontEndReq {

    private List<SalesAgentDataReqPojo> agentDetailsList;

    Map<AgentDesignation,AgentDesignationType> fosDesignationMap = new HashMap<>();
    Map<AgentDesignation,AgentDesignationType> isDesignationMap = new HashMap<>();

    @Override
    protected List<String> collectVerificationErrors() {

        List<String> errors = super.collectVerificationErrors();
        if (ArrayUtils.isEmpty(agentDetailsList)) {
            errors.add("agentDetails list should not be null");
        }else{
            int row = 1;
            List<String> employeeIds = new ArrayList<>();

            List<AgentStatus> agentStatuses = Arrays.asList(AgentStatus.INACTIVE,AgentStatus.RESIGNED,AgentStatus.DROPOUT,AgentStatus.ABSCONDING,AgentStatus.TERMINATED);
            List<AgentDesignationType> designationTypes = Arrays.asList(AgentDesignationType.RM,AgentDesignationType.CM,AgentDesignationType.CH,AgentDesignationType.ZH,AgentDesignationType.FM);
            for (SalesAgentDataReqPojo salesAgentDataReq:agentDetailsList) {
                if (StringUtils.isEmpty(salesAgentDataReq.getName())) {
                    errors.add("Agent name missing for record .." + row);
                }
                if (StringUtils.isNotEmpty(salesAgentDataReq.getName()) && !salesAgentDataReq.getName().trim().matches("^[a-zA-Z\\s\\.]*$")) {
                    errors.add("name should not accept numbers and special characters. check for record :" + row);
                }
                if (StringUtils.isEmpty(salesAgentDataReq.getEmployeeId())) {
                    errors.add("Agent employee id missing for record .." + row);
                }else if (!(salesAgentDataReq.getEmployeeId().trim().startsWith("VD") || salesAgentDataReq.getEmployeeId().trim().startsWith("INT") || salesAgentDataReq.getEmployeeId().trim().startsWith("SContract"))){
                    errors.add("Agent employee id is invalid.." + row);
                }
                if (employeeIds.contains(salesAgentDataReq.getEmployeeId().trim())) {
                    errors.add("Employee should not be repeated. Repeated employeeid [" + salesAgentDataReq.getEmployeeId() + " ]. check record " + row);
                }else{
                    employeeIds.add(salesAgentDataReq.getEmployeeId().trim());
                }
                if (StringUtils.isNotEmpty(salesAgentDataReq.getEmployeeId()) && !salesAgentDataReq.getEmployeeId().trim().matches("^[a-zA-Z0-9]*$")) {
                    errors.add("EmployeeId should not accept special characters. check for record :" + row);
                }
                if (StringUtils.isEmpty(salesAgentDataReq.getEmail())) {
                    errors.add("Agent email missing for record .." + row);
                }else{
                    if (!salesAgentDataReq.getEmail().endsWith("@vedantu.com")) {
                        errors.add("Agent email not vedantu mail.. for record .." + row);
                    }
                }
                if (salesAgentDataReq.getDateOfJoining() == null) {
                    errors.add("Agent DOJ missing for record .." + row);
                }else {
                    if (salesAgentDataReq.getDateOfJoining() > new Date().getTime()) {
                        errors.add("Date of joining can not be future date for record " + row);
                    }
                }
                if (salesAgentDataReq.getDateOfLeaving() != null) {
                    if (salesAgentDataReq.getDateOfLeaving() > new Date().getTime()) {
                        errors.add("Date of leaving can not be future date. for record" + row);
                    }
                    if (salesAgentDataReq.getDateOfJoining() != null) {
                        if (salesAgentDataReq.getDateOfLeaving() < salesAgentDataReq.getDateOfJoining()) {
                            errors.add("Date of leaving should be grater than date of joining.. for record" + row);
                        }
                    }
                }
                if (salesAgentDataReq.getDesignation() == null) {
                    errors.add("Agent Designation missing for record .." + row);
                }
                if (salesAgentDataReq.getDesignationType() == null) {
                    errors.add("Agent designationType missing for record .." + row);
                }else{
                    if (designationTypes.contains(salesAgentDataReq.getDesignationType()) && StringUtils.isNotEmpty(salesAgentDataReq.getTlEmployeeId())) {
                        errors.add("TlEmployeeId should empty for designationTypes RM,CM,CH,ZH,FM. check record " + row);
                    }
                }
                if(salesAgentDataReq.getDesignation() != null && salesAgentDataReq.getDesignationType() != null) {
                    if (fosDesignationMap.get(salesAgentDataReq.getDesignation()) != salesAgentDataReq.getDesignationType() && isDesignationMap.get(salesAgentDataReq.getDesignation()) != salesAgentDataReq.getDesignationType()) {
                        errors.add("Designation and designation type should be matched.. for record" + row);
                    }
                }
                if (salesAgentDataReq.getTeam() == null) {
                    errors.add("Agent team missing for record .." + row);
                }
                if (salesAgentDataReq.getTeam() != null && SalesTeam.FOS.equals(salesAgentDataReq.getTeam())) {
                    if (!fosDesignationMap.containsKey(salesAgentDataReq.getDesignation())){
                        errors.add("Designation not belongs to FOS.check record : " + row);
                    }
                    if (salesAgentDataReq.getDesignation() != null && !AgentDesignation.ZONAL_HEAD.equals(salesAgentDataReq.getDesignation())
                            && !AgentDesignation.REGIONAL_MANAGER.equals(salesAgentDataReq.getDesignation()) &&
                            !AgentDesignation.CLUSTER_MANAGER.equals(salesAgentDataReq.getDesignation()) && !AgentDesignation.CENTER_HEAD.equals(salesAgentDataReq.getDesignation())) {
                        if (StringUtils.isEmpty(salesAgentDataReq.getCentreName())) {
                            errors.add("Agent centre missing for record .." + row);
                        }
                    }

                }
                if (salesAgentDataReq.getTeam() != null && SalesTeam.IS.equals(salesAgentDataReq.getTeam())) {
                    if (!isDesignationMap.containsKey(salesAgentDataReq.getDesignation())){
                        errors.add("Designation not belongs to IS. check record : " + row);
                    }
                    if (AgentDesignation.FLOOR_MANAGER.equals(salesAgentDataReq.getDesignation()) && (StringUtils.isNotEmpty(salesAgentDataReq.getIsFMEmployeeId()) || StringUtils.isNotEmpty(salesAgentDataReq.getIsRMEmployeeId()) || StringUtils.isNotEmpty(salesAgentDataReq.getTlEmployeeId()))){
                        errors.add("FM employeeId/Rm employeeId/Tl employeeId should be empty for record:" + row);
                    }
                    if (AgentDesignation.REVENUE_MANAGER.equals(salesAgentDataReq.getDesignation()) && (StringUtils.isNotEmpty(salesAgentDataReq.getIsRMEmployeeId()) || StringUtils.isNotEmpty(salesAgentDataReq.getTlEmployeeId()))){
                        errors.add("RM employeeId/TL employeeId should be empty for record:" + row);
                    }
                }
                if (StringUtils.isEmpty(salesAgentDataReq.getLeadsquaredId())) {
                    errors.add("Agent leadsquared id missing for record .." + row);
                }
                if (salesAgentDataReq.getStatus() == null) {
                    errors.add("Agent status missing for record .." + row);
                }else{
                    if (agentStatuses.contains(salesAgentDataReq.getStatus()) && salesAgentDataReq.getDateOfLeaving() == null) {
                        errors.add("Date of leaving should not be empty for record " + row);
                    }
                    if (!agentStatuses.contains(salesAgentDataReq.getStatus()) && salesAgentDataReq.getDateOfLeaving() != null) {
                        errors.add("Date of leaving should be empty for record " + row);
                    }
                }

                row++;
            }
        }
        return  errors;
    }

}
