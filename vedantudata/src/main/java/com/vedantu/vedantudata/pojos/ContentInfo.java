package com.vedantu.vedantudata.pojos;

import com.vedantu.lms.cmds.enums.ContentSubType;
import com.vedantu.lms.cmds.enums.ContentType;
import com.vedantu.lms.cmds.enums.ShareStatus;
import com.vedantu.lms.cmds.enums.ShareType;
import com.vedantu.util.ArrayUtils;
import lombok.Data;

import java.util.List;
import java.util.Set;

@Data
public class ContentInfo {

    private String title;
    private Long teacherId;
    private String description;
    private ContentType contentType;
    private String url;
    //private Boolean shared;
    //private String id;
    private String contentId;
    private List<String> contentInfoId;//once shared this will contain the value in ContenInfo
    private Set<String> sessionIds;
    private ShareType shareType;
    private ShareStatus shareStatus=ShareStatus.UNSHARED;

    // Log fields
    private String createdBy;
    private Long creationTime;
    private String sharedBy;
    private Long sharedTime;
    private Set<String> topicNames;
    private ContentSubType contentSubType;
    private String testId;

    public void setContentInfoId(List<String> contentInfoId) {
        if(ArrayUtils.isEmpty(this.contentInfoId)) {
            this.contentInfoId = contentInfoId;
        }else{
            this.contentInfoId.addAll(contentInfoId);
        }
    }

    public void setTopicNames(Set<String> topicNames) {
        if (ArrayUtils.isEmpty(this.topicNames)) {
            this.topicNames = topicNames;
        } else {
            this.topicNames.addAll(topicNames);
        }
    }
}
