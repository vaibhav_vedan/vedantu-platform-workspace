package com.vedantu.vedantudata.pojos.request;

import com.vedantu.util.fos.request.AbstractFrontEndListReq;
import com.vedantu.vedantudata.enums.salesconversion.AgentDesignationType;
import com.vedantu.vedantudata.enums.salesconversion.SalesTeam;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class GetAllSalesAgentReq extends AbstractFrontEndListReq {

    private SalesTeam team;
    private AgentDesignationType designationType;
    private String query;

    public List<String> validate(GetAllSalesAgentReq getAllSalesAgentReq) {
        List<String> errors = new ArrayList<>();
        if(getAllSalesAgentReq.getTeam() == null){
            errors.add("Team missing..");
        }
        return errors;
    }
}
