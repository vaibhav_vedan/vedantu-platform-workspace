package com.vedantu.vedantudata.pojos.freshdesk;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.vedantu.util.dbentities.mongo.AbstractMongoEntity;
import com.vedantu.util.dbentities.mongo.AbstractMongoLongIdEntity;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@EqualsAndHashCode(callSuper = false)
@Document(collection = "FreshdeskContact")
public class FreshdeskContact extends AbstractMongoStringIdEntity {

    public enum Type {LEAD, AGENT,};

    @SerializedName("active")
    @Expose
    private Boolean active;
    @SerializedName("address")
    @Expose
    private Object address;
    @SerializedName("company_id")
    @Expose
    private Long companyId;
    @SerializedName("view_all_tickets")
    @Expose
    private Boolean viewAllTickets;
    @SerializedName("description")
    @Expose
    private Object description;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("job_title")
    @Expose
    private Object jobTitle;
    @SerializedName("language")
    @Expose
    private String language;
    @SerializedName("mobile")
    @Expose
    private Object mobile;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("phone")
    @Expose
    private Object phone;
    @SerializedName("time_zone")
    @Expose
    private String timeZone;
    @SerializedName("twitter_id")
    @Expose
    private Object twitterId;
    @SerializedName("other_emails")
    @Expose
    private List<Object> otherEmails = null;
    @SerializedName("other_companies")
    @Expose
    private List<Company> companies = null;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("custom_fields")
    @Expose
    private Fields fields;
    @SerializedName("tags")
    @Expose
    private List<Object> tags = null;
    @SerializedName("avatar")
    @Expose
    private Avatar avatar;

    @SerializedName("available")
    @Expose
    private Boolean available;
    @SerializedName("available_since")
    @Expose
    private String availableSince;
    @SerializedName("contact")
    @Expose
    private Contact contact;
    @SerializedName("group_ids")
    @Expose
    private List<Long> groupIds = null;
    @SerializedName("occasional")
    @Expose
    private Boolean occasional;
    @SerializedName("role_ids")
    @Expose
    private List<Long> roleIds = null;
    @SerializedName("signature")
    @Expose
    private String signature;
    @SerializedName("ticket_scope")
    @Expose
    private Long ticketScope;
    @SerializedName("type")
    @Expose
    private String type;

    private Long vedantuUserId;
    private Long vedantuTeacherUserId;

    private Type contactType;

    @Indexed(background = true)
    private Long freshdeskContactId;

    public static class Constants extends AbstractMongoEntity.Constants {
        public static final String CONTACT_TYPE = "contactType";
        public static final String FRESHDESK_ID = "freshdeskContactId";
    }

}