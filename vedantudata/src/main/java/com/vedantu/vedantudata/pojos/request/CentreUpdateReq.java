package com.vedantu.vedantudata.pojos.request;

import com.vedantu.util.StringUtils;
import com.vedantu.vedantudata.enums.salesconversion.SalesTeam;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CentreUpdateReq {

    private String oldCentreName;
    private String newCentreName;
    private SalesTeam team;

    public List<String> validate(CentreUpdateReq centreUpdateReq) {
        List<String> errors = new ArrayList<>();
        if (StringUtils.isEmpty(centreUpdateReq.getOldCentreName())) {
            errors.add("Old centre name is missing..");
        }
        if (StringUtils.isEmpty(centreUpdateReq.getNewCentreName())) {
            errors.add("Old centre name is missing..");
        }
        if (centreUpdateReq.getTeam() == null) {
            errors.add("Team is missing...");
        }
        return errors;
    }

}
