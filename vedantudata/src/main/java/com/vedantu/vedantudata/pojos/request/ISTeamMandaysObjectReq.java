package com.vedantu.vedantudata.pojos.request;

import com.vedantu.util.CollectionUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.vedantudata.controllers.SalesConversionController;
import com.vedantu.vedantudata.utils.CommonUtils;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.lang.NumberUtils;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class    ISTeamMandaysObjectReq {

    private List<ISTeamMandaysReq> data;

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(ISTeamMandaysObjectReq.class);

    public List<String> validate(ISTeamMandaysObjectReq isTeamMandaysObjectReq) {
        List<String> errors = new ArrayList<>();
        if (isTeamMandaysObjectReq == null) {
            errors.add("Payload empty..");
            return errors;
        }
        if (CollectionUtils.isEmpty(isTeamMandaysObjectReq.getData())) {
            errors.add("Payload for data is empty..");
            return errors;
        }
        int cnt = 1;
        List<String> empCodeCheck = new ArrayList<>();
        Set<String> duplicateCheck = new HashSet<>();
        for (ISTeamMandaysReq isTeamMandaysReq : isTeamMandaysObjectReq.getData()) {
            if (empCodeCheck.contains(isTeamMandaysReq.getRmEmployeeCode())) {
                duplicateCheck.add(isTeamMandaysReq.getRmEmployeeCode());
            } else {
                empCodeCheck.add(isTeamMandaysReq.getRmEmployeeCode());
            }
            if (StringUtils.isEmpty(isTeamMandaysReq.getRmName()) || StringUtils.isEmpty(isTeamMandaysReq.getRmName().trim())) {
                errors.add("RM name is missing for record: " + cnt);
            }
            if (StringUtils.isNotEmpty(isTeamMandaysReq.getRmName()) && !isTeamMandaysReq.getRmName().trim().matches("^[a-zA-Z\\s\\.]*$")) {
                errors.add("RM name should be without special character for record: " + cnt);
            }
            if (StringUtils.isEmpty(isTeamMandaysReq.getRmEmployeeCode()) || StringUtils.isEmpty(isTeamMandaysReq.getRmEmployeeCode().trim())) {
                errors.add("RM employee code is missing for record: " + cnt);
            }

            if (StringUtils.isEmpty(isTeamMandaysReq.getDate()) || StringUtils.isEmpty(isTeamMandaysReq.getDate().trim())) {
                errors.add("Date is missing for record: " + cnt);
            } else {
                try {
                    Date givenDate = new SimpleDateFormat( "yyyy-MM-dd HH:mm:ss").parse(isTeamMandaysReq.getDate());
                    DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
                    Date todayWithZeroTime = formatter.parse(formatter.format(new Date()));
                    Date futuredate = new Date(todayWithZeroTime.getTime() + (1000 * 60 * 60 * 24));
                    if (!givenDate.before(futuredate)) {
                        errors.add("Future date not allowed for record: " + cnt);
                    }
                } catch (ParseException pe) {
                    errors.add("Date not in proper format for record: " + cnt);
                }
            }
            if (isTeamMandaysReq.getMandays() == null || isTeamMandaysReq.getMandays() < 0) {
                errors.add("Mandays is missing or has negative value for record: " + cnt);
            }
            cnt++;
        }
        if (CollectionUtils.isNotEmpty(duplicateCheck)) {
            errors.add("Multiple RM entries found for " + duplicateCheck + " these RM's");
        }
        return errors;
    }

}
