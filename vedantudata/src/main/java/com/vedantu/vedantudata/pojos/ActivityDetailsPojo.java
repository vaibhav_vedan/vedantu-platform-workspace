package com.vedantu.vedantudata.pojos;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Data
public class ActivityDetailsPojo {

    private String SearchByValue;
    private String ActivityOwnerEmail;
    private String EmailAddress;
    private String ProspectActivityId;
    private String RelatedProspectId;
    private Integer ActivityEvent;
    private String ActivityNote;
    private String ActivityDateTime;
    private List<LeadsquaredActivityField> Fields;

    public void addField(LeadsquaredActivityField leadsquaredActivityField) {

        if (Fields == null) {
            Fields = new ArrayList<>();
        }

        Fields.add(leadsquaredActivityField);
    }

    public class Constants {

        public static final String EMAIL_ADDRESS = "EmailAddress";
    }

    @Override
    public String toString() {
        return "ActivityDetailsPojo{" + "SearchByValue=" + SearchByValue + ", ActivityOwnerEmail=" + ActivityOwnerEmail + ", ProspectActivityId=" + ProspectActivityId + ", RelatedProspectId=" + RelatedProspectId + ", ActivityEvent=" + ActivityEvent + ", ActivityNote=" + ActivityNote + ", ActivityDateTime=" + ActivityDateTime + ", Fields=" + Fields + '}';
    }

}
