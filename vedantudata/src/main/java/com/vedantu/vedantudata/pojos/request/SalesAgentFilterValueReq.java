package com.vedantu.vedantudata.pojos.request;

import com.vedantu.util.fos.request.AbstractFrontEndListReq;
import com.vedantu.vedantudata.enums.salesconversion.AgentDesignation;
import com.vedantu.vedantudata.enums.salesconversion.AgentDesignationType;
import com.vedantu.vedantudata.enums.salesconversion.SalesTeam;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SalesAgentFilterValueReq extends AbstractFrontEndListReq {

    private AgentDesignationType designationType;
    private SalesTeam team;
}
