package com.vedantu.vedantudata.pojos;

import com.vedantu.vedantudata.enums.salesconversion.AgentDesignation;
import com.vedantu.vedantudata.enums.salesconversion.AgentDesignationType;
import com.vedantu.vedantudata.enums.salesconversion.AgentStatus;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.LinkedHashMap;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class AgentAttendanceDataExport {
    private String id;
    private String name;
    private String email;
    private String employeeId;
    private String dateOfJoining;
    private String cohortDOJ;
    private String designation;
    private String designationType;
    private String centre;
    private String zonalHead;
    private String regionalManager;
    private String clusterManager;
    private String centreHead;
    private String isFMId;
    private String isRMId;
    private String tlId;
    private String tlEmpId;
    private String status;
    private String dateOfLeaving;
    private String leadsquaredId;
    private LinkedHashMap<String,String> attendanceData;
}
