package com.vedantu.vedantudata.pojos;

import java.util.List;

public class GTWSessionAttendeeInfo {
	// {
	// "firstName": "Aditi",
	// "lastName": "Sengupta",
	// "sessionKey": 10770772,
	// "attendanceTimeInSeconds": 4591,
	// "registrantKey": 6234282991187424269,
	// "email": "aditisengupta71@gmail.com",
	// "attendance": [
	// {
	// "leaveTime": "2017-08-12T14:44:33Z",
	// "joinTime": "2017-08-12T13:28:02Z"
	// }
	// ]
	// },

	private String firstName;
	private String lastName;
	private List<String> sessionIds;
	private Integer attendanceTimeInSeconds;
	private String registrantKey;
	private String email;
	private List<GTWAttendenceInterval> attendance;

	public GTWSessionAttendeeInfo() {
		super();
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public List<String> getSessionIds() {
		return sessionIds;
	}

	public void setSessionIds(List<String> sessionIds) {
		this.sessionIds = sessionIds;
	}

	public Integer getAttendanceTimeInSeconds() {
		return attendanceTimeInSeconds;
	}

	public void setAttendanceTimeInSeconds(Integer attendanceTimeInSeconds) {
		this.attendanceTimeInSeconds = attendanceTimeInSeconds;
	}

	public String getRegistrantKey() {
		return registrantKey;
	}

	public void setRegistrantKey(String registrantKey) {
		this.registrantKey = registrantKey;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public List<GTWAttendenceInterval> getAttendance() {
		return attendance;
	}

	public void setAttendance(List<GTWAttendenceInterval> attendance) {
		this.attendance = attendance;
	}

	@Override
	public String toString() {
		return "GTWSessionAttendeeInfo [firstName=" + firstName + ", lastName=" + lastName + ", sessionIds="
				+ sessionIds + ", attendanceTimeInSeconds=" + attendanceTimeInSeconds + ", registrantKey="
				+ registrantKey + ", email=" + email + ", attendance=" + attendance + ", toString()=" + super.toString()
				+ "]";
	}
}
