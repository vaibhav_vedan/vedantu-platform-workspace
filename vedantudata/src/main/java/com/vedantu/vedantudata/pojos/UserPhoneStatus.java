package com.vedantu.vedantudata.pojos;

public enum UserPhoneStatus {
	UNKNOWN, DND, WHITELISTED
}
