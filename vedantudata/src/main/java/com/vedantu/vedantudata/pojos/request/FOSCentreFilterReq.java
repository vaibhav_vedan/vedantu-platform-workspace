package com.vedantu.vedantudata.pojos.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class FOSCentreFilterReq {

    private String zhId;
    private String rmId;
    private String cmId;
}
