package com.vedantu.vedantudata.pojos.request;

import com.google.gson.annotations.SerializedName;
import lombok.Data;

@Data
public class OnlineSalesDemoSessionJoinedLSRequest {

    @SerializedName("studentId")
    private String studentId;

    @SerializedName("sessionId")
    private String sessionId;

    @SerializedName("sessionJoinTimeStudent")
    private String sessionJoinTimeStudent;

    @SerializedName("sessionJoinTimeTeacher")
    private String sessionJoinTimeTeacher;

    @SerializedName("agentId")
    private String agentId;

    @SerializedName("sessionDuration")
    private String sessionDuration;

    @SerializedName("sessionStartTime")
    private String sessionStartTime;


}



