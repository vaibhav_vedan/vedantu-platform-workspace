package com.vedantu.vedantudata.pojos;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ActivityStatusChangeHistory {

    private String statusBefore;
    private String statusAfter;
    private String modifiedBy;
    private String modifiedByName;
    private Long modifiedOn;
}
