package com.vedantu.vedantudata.pojos.request;

import com.vedantu.vedantudata.enums.salesconversion.AgentDesignation;
import com.vedantu.vedantudata.enums.salesconversion.AgentDesignationType;
import com.vedantu.vedantudata.enums.salesconversion.AgentStatus;
import com.vedantu.vedantudata.enums.salesconversion.SalesTeam;
import lombok.Data;

@Data
public class SalesAgentDataReqPojo {

    private String agentId;
    private String name;
    private String employeeId;
    private String email;
    private Long dateOfJoining;
    private AgentDesignation designation;
    private AgentDesignationType designationType;
    private String centreName;
    private String isRMEmployeeId;
    private String isFMEmployeeId;
    private String tlEmployeeId;
    private AgentStatus status;
    private Long dateOfLeaving;
    private String leadsquaredId;
    private SalesTeam team;
}
