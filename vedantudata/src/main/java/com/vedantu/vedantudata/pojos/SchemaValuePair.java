/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.vedantudata.pojos;

import com.google.gson.annotations.SerializedName;

public class SchemaValuePair {

    @SerializedName("SchemaName")
    private String schemaName;

    @SerializedName("Value")
    private String value;

    public SchemaValuePair() {
        super();
    }

    public SchemaValuePair(String schemaName, String value) {
        this.schemaName = schemaName;
        this.value = value;
    }

    public String getSchemaName() {
        return schemaName;
    }

    public void setSchemaName(String schemaName) {
        this.schemaName = schemaName;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "SchemaValuePair{" + "schemaName=" + schemaName + ", value=" + value + '}';
    }

}
