package com.vedantu.vedantudata.pojos;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;

/**
 * NOTE : Response Manager
 *
 */

@Data
@ToString
public class SalesConversionApiResponseManager implements Serializable {

    private String responseStatus;
    private String responseMessage;
    private String errorMessage;
    private Object payload;

    public static class Builder {

        private SalesConversionApiResponseManager responseManager;

        public Builder() {

            this.responseManager = new SalesConversionApiResponseManager();
        }

        public Builder responseStatus(String responseStatus) {
            this.responseManager.responseStatus = responseStatus;
            return this;
        }

        public Builder responseMessage(String responseMessage) {
            this.responseManager.responseMessage = responseMessage;
            return this;
        }

        public Builder errorMessage(String errorMessage) {
            this.responseManager.errorMessage = errorMessage;
            return this;
        }

        public Builder payload(Object payload) {
            this.responseManager.payload = payload;
            return this;
        }

        public SalesConversionApiResponseManager build() {
            return this.responseManager;
        }
    }


}
