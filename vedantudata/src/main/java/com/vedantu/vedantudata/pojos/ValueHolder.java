package com.vedantu.vedantudata.pojos;

import com.vedantu.vedantudata.entities.leadsquared.LeadDetails;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * @author mano
 */
public class ValueHolder {
        public Map<String, List<LeadDetails>> leadDetails = new LinkedHashMap<>(); //key can be email/phoneno
        public Map<String, Map<String, String>> taskDetails = new HashMap<>();
}