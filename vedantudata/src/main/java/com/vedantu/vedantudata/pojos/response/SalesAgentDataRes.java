package com.vedantu.vedantudata.pojos.response;

import com.vedantu.vedantudata.entities.salesconversion.CentreInfo;
import com.vedantu.vedantudata.entities.salesconversion.SalesAgentInfo;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SalesAgentDataRes {

    private SalesAgentInfo agentInfo;
    private CentreInfo centreInfo;
    private SalesAgentInfo zhInfo;
    private SalesAgentInfo rmInfo;
    private SalesAgentInfo cmInfo;
    private SalesAgentInfo chInfo;
    private SalesAgentInfo tlInfo;
    private SalesAgentInfo fmInfo;

}
