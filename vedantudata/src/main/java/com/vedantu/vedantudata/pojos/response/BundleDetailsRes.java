package com.vedantu.vedantudata.pojos.response;


import com.vedantu.scheduling.pojo.session.BatchWiseSessionInfo;
import lombok.Data;

import java.util.List;

@Data
public class BundleDetailsRes {

    private String title;
    private List<Integer> grade;
    private Boolean unLimitedDoubtsIncluded = false;
    private Long validTill;
    private BatchWiseSessionInfo firstClass;
    private Boolean hasTest = false;
    private Boolean hasVideo = false;
    private Boolean hasLiveCourses = false;
    private Boolean amIncluded = false;

}
