package com.vedantu.vedantudata.pojos;

import com.vedantu.vedantudata.enums.salesconversion.AgentDesignation;
import com.vedantu.vedantudata.enums.salesconversion.AgentDesignationType;
import com.vedantu.vedantudata.enums.salesconversion.AgentStatus;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class SalesAgentDataExport {
    private String id;
    private String name;
    private String email;
    private String employeeId;
    private String dateOfJoining;
    private String cohortDOJ;
    private AgentDesignation designation;
    private AgentDesignationType designationType;
    private String centre;
    private String zonalHead;
    private String regionalManager;
    private String clusterManager;
    private String centreHead;
    private String isFMId;
    private String isRMId;
    private String tlId;
    private String tlEmpId;
    private AgentStatus status;
    private String dateOfLeaving;
    private String leadsquaredId;
    private String updatedBy;
    private String updatedAt;
}
