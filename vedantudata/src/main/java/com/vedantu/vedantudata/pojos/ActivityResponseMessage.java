package com.vedantu.vedantudata.pojos;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class ActivityResponseMessage {
    private boolean ActivityCreated;
    private boolean ActivityUpdated;
    private String ProspectActivityId;
    private Integer RowNumber;
}
