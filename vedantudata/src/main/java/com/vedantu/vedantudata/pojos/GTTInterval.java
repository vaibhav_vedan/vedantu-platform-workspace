package com.vedantu.vedantudata.pojos;

public class GTTInterval {
	private String startTime;
	private String endTime;

	public GTTInterval() {
		super();
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	@Override
	public String toString() {
		return "GTTInterval [startTime=" + startTime + ", endTime=" + endTime + ", toString()=" + super.toString()
				+ "]";
	}
}
