package com.vedantu.vedantudata.pojos;

public enum UserEntityOwner {
	STUDENT, MOTHER, FATHER, PARENT, SIBILING, UNKNOWN
}
