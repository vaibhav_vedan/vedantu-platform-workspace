package com.vedantu.vedantudata.pojos;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.google.gson.annotations.SerializedName;
import lombok.*;

@Getter
@Setter
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class ActivityDetailsCallbackPojo {

    @JsonFormat(with = JsonFormat.Feature.ACCEPT_CASE_INSENSITIVE_PROPERTIES)
    private String ProspectActivityId;

    @JsonFormat(with = JsonFormat.Feature.ACCEPT_CASE_INSENSITIVE_PROPERTIES)
    private String ProspectId;

    @JsonFormat(with = JsonFormat.Feature.ACCEPT_CASE_INSENSITIVE_PROPERTIES)
    private String RelatedProspectId;

    @JsonFormat(with = JsonFormat.Feature.ACCEPT_CASE_INSENSITIVE_PROPERTIES)
    private String EmailAddress;

    @JsonFormat(with = JsonFormat.Feature.ACCEPT_CASE_INSENSITIVE_PROPERTIES)
    private String FirstName;

    @JsonFormat(with = JsonFormat.Feature.ACCEPT_CASE_INSENSITIVE_PROPERTIES)
    private String Mobile;

    @JsonFormat(with = JsonFormat.Feature.ACCEPT_CASE_INSENSITIVE_PROPERTIES)
    private String ActivityEvent;

    @JsonFormat(with = JsonFormat.Feature.ACCEPT_CASE_INSENSITIVE_PROPERTIES)
    private String ActivityType;

    @JsonFormat(with = JsonFormat.Feature.ACCEPT_CASE_INSENSITIVE_PROPERTIES)
    private String ActivityEvent_Note;

    @JsonFormat(with = JsonFormat.Feature.ACCEPT_CASE_INSENSITIVE_PROPERTIES)
    private String ActivityDateTime;

    @JsonFormat(with = JsonFormat.Feature.ACCEPT_CASE_INSENSITIVE_PROPERTIES)
    private String CreatedOn;

    @JsonFormat(with = JsonFormat.Feature.ACCEPT_CASE_INSENSITIVE_PROPERTIES)
    private String CreatedBy;

    @JsonFormat(with = JsonFormat.Feature.ACCEPT_CASE_INSENSITIVE_PROPERTIES)
    private String CreatedByName;

    @JsonFormat(with = JsonFormat.Feature.ACCEPT_CASE_INSENSITIVE_PROPERTIES)
    private String ModifiedOn;

    @JsonFormat(with = JsonFormat.Feature.ACCEPT_CASE_INSENSITIVE_PROPERTIES)
    private String ModifiedBy;

    @JsonFormat(with = JsonFormat.Feature.ACCEPT_CASE_INSENSITIVE_PROPERTIES)
    private String ModifiedByName;

    @JsonFormat(with = JsonFormat.Feature.ACCEPT_CASE_INSENSITIVE_PROPERTIES)
    private String Status;

    @JsonFormat(with = JsonFormat.Feature.ACCEPT_CASE_INSENSITIVE_PROPERTIES)
    private String Owner;

    //Custom fields
    private String mx_Student_Name;
    private String mx_Parent_name;
    private String mx_Parent_emailId;
    private String mx_Parent_Contact_number;
    private String mx_PhoneNumberList;
    private String mx_ModeOfPaymentDP;
    private String mx_InstallmentAmount;
    private String mx_Overall_Score;
    private String mx_Student_Device;
    private String mx_Internet_Service_Provider;
    private String mx_User_Preferred_Language;
    private String mx_Mode_Of_Communication_for_Parents;
    private String mx_Student_Address;
    private String mx_Student_School_Name;
    private String mx_Student_Skype_ID;
    private String mx_Status_Of_The_Student_Attending_The_Session;
    private String mx_Custom_1;
    private String mx_Custom_2;
    private String mx_Custom_3;
    private String mx_Custom_4;
    private String mx_Custom_5;
    private String mx_Custom_6;
    private String mx_Custom_7;
    private String mx_Custom_8;
    private String mx_Custom_9;
    private String mx_Custom_10;
    private String mx_Custom_11;
    private String mx_Custom_12;
    private String mx_Custom_13;
    private String mx_Custom_14;
    private String mx_Custom_15;

}
