package com.vedantu.vedantudata.pojos.response;

import com.vedantu.vedantudata.entities.salesconversion.CentreInfo;
import com.vedantu.vedantudata.entities.salesconversion.SalesAgentInfo;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CentreInfoRes {

    private CentreInfo centreInfo;
    private List<SalesAgentInfo> salesAgentInfos;
}
