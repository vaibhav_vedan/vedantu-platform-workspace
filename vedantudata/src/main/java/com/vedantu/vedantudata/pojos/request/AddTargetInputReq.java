package com.vedantu.vedantudata.pojos.request;

import com.vedantu.util.ArrayUtils;
import com.vedantu.util.StringUtils;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import com.vedantu.vedantudata.utils.CommonUtils;
import lombok.Data;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Data
public class AddTargetInputReq extends AbstractFrontEndReq {
    private List<TargetInputItems> data;

    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();
        if (ArrayUtils.isEmpty(data)) {
            errors.add("data can't be null or empty");
        } else {
            int cnt = 1;
            for (TargetInputItems item : data) {
                if (StringUtils.isEmpty(item.getRm()) || StringUtils.isEmpty(item.getRm().trim())) {
                    errors.add("Rm is missing for record :" + cnt);
                }

                if (StringUtils.isEmpty(item.getRmEmployeeCode()) || StringUtils.isEmpty(item.getRmEmployeeCode().trim())) {
                    errors.add("RmEmployeeCode is missing for record : " + cnt);
                }

                if (StringUtils.isEmpty(item.getLocation()) || StringUtils.isEmpty(item.getLocation().trim())) {
                    errors.add("Location is missing for record : " + cnt);
                }
                if (StringUtils.isEmpty(item.getDate()) || StringUtils.isEmpty(item.getDate().trim())) {
                    errors.add("Date is missing for record: " + cnt);
                } else {
                    try {
                        Date currentDate = new Date();
                        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        format.setLenient(false);
                        Date givenDate = format.parse(item.getDate());
                        if (!currentDate.after(givenDate)) {
                            errors.add("Date cannot be future date. check for record : " + cnt);
                        }
                    } catch (ParseException pe) {
                        errors.add("Date not in proper format for record: " + cnt);
                    }

                }
                if (item.getSfCommited() == null) {
                    errors.add("SfCommitted is missing for record :" + cnt);
                } else {
                    if (item.getSfCommited() < 0) {
                        errors.add("Sfcommitted value should be 0 or positive value. check for record : " + cnt);
                    }
                }
                cnt++;
            }
        }
        return errors;
    }
}
