package com.vedantu.vedantudata.pojos.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class GetMasterLiveClassDetailsRes {

    private Long attendedOn;
    private String subject = "";
    private String teacher = "";
    private String title;
    private Boolean isHotspotAttempted = false;
    private Boolean isQuizAttempted = false;
}
