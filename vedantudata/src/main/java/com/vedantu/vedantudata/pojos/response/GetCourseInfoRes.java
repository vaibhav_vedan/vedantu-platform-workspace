package com.vedantu.vedantudata.pojos.response;

import com.vedantu.subscription.pojo.AutoEnrollCourseInfo;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class GetCourseInfoRes {

    private List<AutoEnrollCourseInfo> courses;
}
