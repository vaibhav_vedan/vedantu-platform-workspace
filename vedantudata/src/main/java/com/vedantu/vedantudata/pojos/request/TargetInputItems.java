package com.vedantu.vedantudata.pojos.request;

import lombok.Data;

import javax.persistence.Id;

@Data
public class TargetInputItems {
    private String rm;
    private String rmEmployeeCode;
    private String location;
    private String date;
    private Integer sfCommited;
}
