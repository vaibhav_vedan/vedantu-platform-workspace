package com.vedantu.vedantudata.pojos.response;

import com.vedantu.vedantudata.entities.salesconversion.SalesAgentAttendanceInfo;
import com.vedantu.vedantudata.enums.salesconversion.AgentDesignation;
import com.vedantu.vedantudata.enums.salesconversion.AgentDesignationType;
import com.vedantu.vedantudata.enums.salesconversion.AgentStatus;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class AgentAttendanceMetadataRes {

    private String name;
    private String employeeId;
    private String agentId;
    private String email;
    private Long dateOfJoining;
    private Long dateOfLeaving;
    private AgentDesignation designation;
    private AgentDesignationType designationType;
    private AgentStatus status;
    private String leadSquaredId;
    private String centreId;
    private String tlId;
    private String isFMId;
    private String isRMId;
    private List<SalesAgentAttendanceInfo> attendance;
}
