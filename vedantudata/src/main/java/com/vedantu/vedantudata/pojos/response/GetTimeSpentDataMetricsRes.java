package com.vedantu.vedantudata.pojos.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class GetTimeSpentDataMetricsRes {

    private Integer mostLiveClassAttended;
    private Double mostQuestionSolved;
    private Double mostAccuracy;
    private Double mostTimeSpent;
    private String mostTimeSpentSubject;
    private Integer leastLiveClassAttended;
    private Double leastQuestionSolved;
    private Double leastAccuracy;
    private Double leastTimeSpent;
    private String leastTimeSpentSubject;
}
