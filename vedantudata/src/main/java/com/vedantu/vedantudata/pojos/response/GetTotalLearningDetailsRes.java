package com.vedantu.vedantudata.pojos.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class GetTotalLearningDetailsRes {

    private Double learningTime;
    private Integer liveClassAttended;
    private Integer testGiven;
}
