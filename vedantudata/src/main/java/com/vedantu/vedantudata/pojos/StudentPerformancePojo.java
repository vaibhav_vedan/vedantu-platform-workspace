package com.vedantu.vedantudata.pojos;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
public class StudentPerformancePojo {

    private Integer id;
    private String email;
    private String subject;
    private Double average;
    private Date timestamp;
    private String interval;
    private Integer quantity;

    public static class Constants{
        public static String ID = "id";
        public static String EMAIL = "email";
        public static String SUBJECT = "subject";
        public static String AVERAGE = "average";
        public static String TIMESTAMP = "timestamp";
        public static String INTERVAl = "interval";
        public static String QUANTITY = "quantity";

    }
}
