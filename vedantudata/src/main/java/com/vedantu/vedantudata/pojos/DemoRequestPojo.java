package com.vedantu.vedantudata.pojos;

import com.vedantu.vedantudata.enums.DemoRequestType;
import lombok.Data;

@Data
public class DemoRequestPojo {

    private Long userId;
    private DemoRequestType demoRequestType;

}
