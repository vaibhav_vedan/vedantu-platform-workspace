package com.vedantu.vedantudata.pojos;

import com.google.gson.annotations.SerializedName;
import com.vedantu.lms.cmds.pojo.KeyValueObject;

import java.util.List;

public class LeadActivitySerialized {

    @SerializedName("Id")
    private String activityId;

    @SerializedName("EventCode")
    private String eventCode;

    @SerializedName("EventName")
    private String eventName;

    @SerializedName("ActivityScore")
    private String activityScore;

    @SerializedName("CreatedOn")
    private String createdOn;

    @SerializedName("ActivityType")
    private String activityType;

    @SerializedName("IsEmailType")
    private String checkEmailType;

    @SerializedName("Type")
    private String type;

    @SerializedName("EmailType")
    private String emailType;

    @SerializedName("RelatedProspectId")
    private String relatedProspectId;

    @SerializedName("Data")
    private List<KeyValueObject> data;

    @SerializedName("ActivityFields")
    private LeadSquaredActivityFields fields;

    public String getActivityId() {
        return activityId;
    }

    public void setActivityId(String activityId) {
        this.activityId = activityId;
    }

    public String getEventCode() {
        return eventCode;
    }

    public void setEventCode(String eventCode) {
        this.eventCode = eventCode;
    }

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public String getActivityScore() {
        return activityScore;
    }

    public void setActivityScore(String activityScore) {
        this.activityScore = activityScore;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public String getActivityType() {
        return activityType;
    }

    public void setActivityType(String activityType) {
        this.activityType = activityType;
    }

    public String getCheckEmailType() {
        return checkEmailType;
    }

    public void setCheckEmailType(String checkEmailType) {
        this.checkEmailType = checkEmailType;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getEmailType() {
        return emailType;
    }

    public void setEmailType(String emailType) {
        this.emailType = emailType;
    }

    public String getRelatedProspectId() {
        return relatedProspectId;
    }

    public void setRelatedProspectId(String relatedProspectId) {
        this.relatedProspectId = relatedProspectId;
    }

    public List<KeyValueObject> getData() {
        return data;
    }

    public void setData(List<KeyValueObject> data) {
        this.data = data;
    }

    public LeadSquaredActivityFields getFields() {
        return fields;
    }

    public void setFields(LeadSquaredActivityFields fields) {
        this.fields = fields;
    }

    public static class Constants {

        public static final String DATA_KEY = "NotableEventDescription";
    }
}
