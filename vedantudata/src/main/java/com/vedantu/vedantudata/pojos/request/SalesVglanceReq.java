package com.vedantu.vedantudata.pojos.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SalesVglanceReq {

    private Long userId;
    private Long startTime;

    public List<String> validate(SalesVglanceReq salesVglanceReq) {
        List<String> errors = new ArrayList<>();
        if (salesVglanceReq.getUserId() == null) {
            errors.add("UserId missing..");
        }
        if (salesVglanceReq.getStartTime() != null) {
            Long difference = System.currentTimeMillis() - salesVglanceReq.getStartTime();
            Long days = difference / (1000 * 60 * 60 * 24);
            if (days > 31) {
                errors.add("Vglance date range cannot exceed 31 days..");
            }
        } else {
            errors.add("StartTime should be present..");
        }
        return errors;
    }

}
