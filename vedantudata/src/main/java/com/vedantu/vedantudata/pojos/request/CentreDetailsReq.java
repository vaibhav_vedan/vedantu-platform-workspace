package com.vedantu.vedantudata.pojos.request;

import com.vedantu.util.CollectionUtils;
import com.vedantu.util.StringUtils;
import com.vedantu.vedantudata.enums.salesconversion.SalesTeam;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.index.Indexed;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CentreDetailsReq {

    private String centreId;
    private String centreName;

    private String centreHeadId;

    private String zhId;

    private String rmId;

    private String cmId;

    public List<String> validate(CentreDetailsReq centreDetailsReq) {
        List<String> errors = new ArrayList<>();
        if (StringUtils.isEmpty(centreDetailsReq.getCentreName())) {
            errors.add("Centre name is empty..");
            return errors;
        }
        return errors;
    }
}

