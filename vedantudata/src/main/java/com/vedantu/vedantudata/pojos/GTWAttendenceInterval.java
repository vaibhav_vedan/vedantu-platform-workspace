package com.vedantu.vedantudata.pojos;

public class GTWAttendenceInterval {
	// "leaveTime": "2017-08-12T14:44:33Z",
	// "joinTime": "2017-08-12T13:28:02Z"

	private String joinTime;
	private String leaveTime;

	public GTWAttendenceInterval() {
		super();
	}

	public String getJoinTime() {
		return joinTime;
	}

	public void setJoinTime(String joinTime) {
		this.joinTime = joinTime;
	}

	public String getLeaveTime() {
		return leaveTime;
	}

	public void setLeaveTime(String leaveTime) {
		this.leaveTime = leaveTime;
	}

	@Override
	public String toString() {
		return "GTWAttendenceInterval [joinTime=" + joinTime + ", leaveTime=" + leaveTime + ", toString()="
				+ super.toString() + "]";
	}
}
