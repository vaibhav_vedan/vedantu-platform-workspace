package com.vedantu.vedantudata.pojos.response;

import com.vedantu.vedantudata.entities.salesconversion.CentreInfo;
import com.vedantu.vedantudata.entities.salesconversion.SalesAgentInfo;
import com.vedantu.vedantudata.enums.salesconversion.AgentDesignation;
import com.vedantu.vedantudata.enums.salesconversion.AgentStatus;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class SalesAgentMetadataRes {
    private List<SalesAgentInfo> salesAgentInfoList = new ArrayList<>();
    private List<CentreInfo> centreInfos;
    private List<SalesAgentInfo> tlInfos;
    private Integer salesAgentInfoCount = 0;

}
