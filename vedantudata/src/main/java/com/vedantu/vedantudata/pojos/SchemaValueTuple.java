package com.vedantu.vedantudata.pojos;

import com.google.gson.annotations.SerializedName;

public class SchemaValueTuple {
	@SerializedName("SchemaName")
	private String SchemaName;

	@SerializedName("Value")
	private String Value;

	public SchemaValueTuple() {
		super();
	}

	public SchemaValueTuple(String schemaName, String value) {
		super();
		SchemaName = schemaName;
		Value = value;
	}

	public String getSchemaName() {
		return SchemaName;
	}

	public void setSchemaName(String schemaName) {
		SchemaName = schemaName;
	}

	public String getValue() {
		return Value;
	}

	public void setValue(String value) {
		Value = value;
	}

	@Override
	public String toString() {
		return "SchemaValueTuple [SchemaName=" + SchemaName + ", Value=" + Value + ", toString()=" + super.toString()
				+ "]";
	}
}
