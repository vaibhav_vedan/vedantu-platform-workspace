package com.vedantu.vedantudata.pojos.request;

import com.vedantu.util.StringUtils;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CheckSAUniqueValueReq {

    private String email;
    private String employeeId;


    public static List<String> validate(CheckSAUniqueValueReq checkSAUniqueValueReq) {
        List<String> errors = new ArrayList<>();
        if (StringUtils.isEmpty(checkSAUniqueValueReq.getEmail()) && StringUtils.isEmpty(checkSAUniqueValueReq.getEmployeeId())) {
            errors.add("fields are missing");
        }
        return errors;
    }
}
