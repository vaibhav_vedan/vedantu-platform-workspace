package com.vedantu.vedantudata.pojos;

import java.util.List;

public class WebinarInfoByIdRes {
	/*
	 * { "webinarKey": 5935334575730303234, "numberOfRegistrationLinkClicks": 0,
	 * "times": [ { "startTime": "2017-08-16T13:30:00Z", "endTime":
	 * "2017-08-16T14:30:00Z" } ], "numberOfRegistrants": 0, "webinarID":
	 * "5935334575730303234", "subject": "TurboMath - Be a Maths Genius",
	 * "description": "", "timeZone": "Asia/Calcutta", "inSession": false,
	 * "organizerKey": 7470416618302296069, "registrationUrl":
	 * "https://attendee.gotowebinar.com/register/5935334575730303234",
	 * "numberOfOpenedInvitations": 0 }
	 */

	private String webinarKey;
	private String webinarID;
	private String subject;
	private String description;
	private String timeZone;
	private String organizerKey;
	private String registrationUrl;
	private List<GTTInterval> times;

	public WebinarInfoByIdRes() {
		super();
	}

	public String getWebinarKey() {
		return webinarKey;
	}

	public void setWebinarKey(String webinarKey) {
		this.webinarKey = webinarKey;
	}

	public String getWebinarID() {
		return webinarID;
	}

	public void setWebinarID(String webinarID) {
		this.webinarID = webinarID;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getTimeZone() {
		return timeZone;
	}

	public void setTimeZone(String timeZone) {
		this.timeZone = timeZone;
	}

	public String getOrganizerKey() {
		return organizerKey;
	}

	public void setOrganizerKey(String organizerKey) {
		this.organizerKey = organizerKey;
	}

	public String getRegistrationUrl() {
		return registrationUrl;
	}

	public void setRegistrationUrl(String registrationUrl) {
		this.registrationUrl = registrationUrl;
	}

	public List<GTTInterval> getTimes() {
		return times;
	}

	public void setTimes(List<GTTInterval> times) {
		this.times = times;
	}

	@Override
	public String toString() {
		return "WebinarInfoByIdRes [webinarKey=" + webinarKey + ", webinarID=" + webinarID + ", subject=" + subject
				+ ", description=" + description + ", timeZone=" + timeZone + ", organizerKey=" + organizerKey
				+ ", registrationUrl=" + registrationUrl + ", times=" + times + ", toString()=" + super.toString()
				+ "]";
	}
}
