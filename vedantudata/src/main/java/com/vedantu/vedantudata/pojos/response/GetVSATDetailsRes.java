package com.vedantu.vedantudata.pojos.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class GetVSATDetailsRes {

    private Long attemptedOn;
    private Integer score = 0;
    private Integer scholarship = 0;
    private Integer topScore = 0;
    private Double speed = 0.0;
    private Double accuracy = 0.0;
    private Boolean isVSATAttended;
}
