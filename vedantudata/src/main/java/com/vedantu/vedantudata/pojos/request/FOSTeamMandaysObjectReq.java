package com.vedantu.vedantudata.pojos.request;

import com.vedantu.util.CollectionUtils;
import com.vedantu.util.StringUtils;
import com.vedantu.vedantudata.utils.CommonUtils;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class FOSTeamMandaysObjectReq {

    private List<FOSTeamMandaysReq> data;

    public List<String> validate(FOSTeamMandaysObjectReq fosTeamMandaysObjectReq) {
        List<String> errors = new ArrayList<>();
        if(fosTeamMandaysObjectReq == null){
            errors.add("Payload empty..");
            return errors;
        }
        if (CollectionUtils.isEmpty(fosTeamMandaysObjectReq.getData())) {
            errors.add("Payload data is empty..");
            return errors;
        }
        int cnt = 1;
        for (FOSTeamMandaysReq fosTeamMandaysReq : fosTeamMandaysObjectReq.getData()) {
            if (StringUtils.isEmpty(fosTeamMandaysReq.getCentreName()) || StringUtils.isEmpty(fosTeamMandaysReq.getCentreName().trim())) {
                errors.add("Centre name is missing for record: " + cnt);
            }
            if (StringUtils.isEmpty(fosTeamMandaysReq.getDate()) || StringUtils.isEmpty(fosTeamMandaysReq.getDate().trim())) {
                errors.add("Date is missing for record: " + cnt);
            } else{
                try{
                    CommonUtils.parseTime(fosTeamMandaysReq.getDate());
                } catch (ParseException pe){
                    errors.add("Date not in proper format for record: " + cnt);
                }
            }
            if (fosTeamMandaysReq.getMandays() == null) {
                errors.add("Mandays is missing for record: " + cnt);
            }
            cnt++;
        }
        return errors;
    }
}
