package com.vedantu.vedantudata.pojos;

import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class StudentAccountManagerStudentsRes extends AbstractMongoStringIdEntity {

    private Long studentAccountManagerId;
    private Long studentId;

    public static class Constants extends AbstractMongoStringIdEntity.Constants {

        public static final String STUDENTACCOUNTMANAGERID = "studentAccountManagerId";
        public static final String STUDENTID = "studentId";
    }
}