package com.vedantu.vedantudata.pojos;

import com.google.gson.annotations.SerializedName;

public class KeyValueObject {

	@SerializedName("Key")
	private String key;

	@SerializedName("Value")
	private String value;

	public KeyValueObject() {
		super();
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return "KeyValueObject [key=" + key + ", value=" + value + ", toString()=" + super.toString() + "]";
	}
}
