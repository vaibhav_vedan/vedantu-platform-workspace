package com.vedantu.vedantudata.pojos;

import com.vedantu.scheduling.pojo.HotspotNumbers;
import com.vedantu.scheduling.pojo.QuizNumbers;
import com.vedantu.util.dbentitybeans.mongo.AbstractMongoStringIdEntityBean;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AttendedSessionInfo extends AbstractMongoStringIdEntityBean {

    private String sessionId;
    private Long boardId;
    private String subject;
    private Long timeInSession = 0L;
    private QuizNumbers quizNumbers;
    private HotspotNumbers hotspotNumbers;

}
