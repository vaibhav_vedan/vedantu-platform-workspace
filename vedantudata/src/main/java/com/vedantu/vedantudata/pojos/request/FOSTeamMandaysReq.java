package com.vedantu.vedantudata.pojos.request;

import com.vedantu.util.CollectionUtils;
import com.vedantu.util.StringUtils;
import com.vedantu.vedantudata.utils.CommonUtils;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class FOSTeamMandaysReq {

    private String centreName;
    private String date;
    private Integer mandays;

}
