package com.vedantu.vedantudata.pojos;

import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class CustomLeadSquaredAgent {
    Integer isemailsender;
    Integer usertype;
    Integer isphonecallagent;

    @Override
    public String toString() {
        return "CustomLeadSquaredAgent{" +
                "isemailsender=" + isemailsender +
                ", usertype=" + usertype +
                ", isphonecallagent=" + isphonecallagent +
                ", modifyallleadsofgroup=" + modifyallleadsofgroup +
                ", viewallleadsofgroup=" + viewallleadsofgroup +
                ", isagencyuser=" + isagencyuser +
                ", isadministrator=" + isadministrator +
                ", isbillinguser=" + isbillinguser +
                ", ischeckedin=" + ischeckedin +
                ", ischeckinenabled=" + ischeckinenabled +
                ", autouserid=" + autouserid +
                ", role='" + role + '\'' +
                ", modifiedby='" + modifiedby + '\'' +
                ", createdby='" + createdby + '\'' +
                ", phoneothers='" + phoneothers + '\'' +
                ", phonemobile='" + phonemobile + '\'' +
                ", phonemain='" + phonemain + '\'' +
                ", associatedphonenumbers='" + associatedphonenumbers + '\'' +
                ", signature_text='" + signature_text + '\'' +
                ", signature_html='" + signature_html + '\'' +
                ", dateformat='" + dateformat + '\'' +
                ", timezone='" + timezone + '\'' +
                ", sessionid='" + sessionid + '\'' +
                ", tmp_forgotpassword='" + tmp_forgotpassword + '\'' +
                ", authtoken='" + authtoken + '\'' +
                ", password='" + password + '\'' +
                ", emailaddress='" + emailaddress + '\'' +
                ", lastname='" + lastname + '\'' +
                ", middlename='" + middlename + '\'' +
                ", firstname='" + firstname + '\'' +
                ", userid='" + userid + '\'' +
                ", lastcheckedon='" + lastcheckedon + '\'' +
                ", modifiedon='" + modifiedon + '\'' +
                ", createdon='" + createdon + '\'' +
                ", deletionstatuscode=" + deletionstatuscode +
                ", statusreason=" + statusreason +
                ", statuscode=" + statuscode +
                ", isdefaultowner=" + isdefaultowner +
                ", mx_custom_10='" + mx_custom_10 + '\'' +
                ", mx_custom_9='" + mx_custom_9 + '\'' +
                ", mx_custom_8='" + mx_custom_8 + '\'' +
                ", mx_custom_7='" + mx_custom_7 + '\'' +
                ", mx_custom_6='" + mx_custom_6 + '\'' +
                ", mx_custom_5='" + mx_custom_5 + '\'' +
                ", mx_custom_4='" + mx_custom_4 + '\'' +
                ", mx_custom_3='" + mx_custom_3 + '\'' +
                ", mx_custom_2='" + mx_custom_2 + '\'' +
                ", mx_custom_1='" + mx_custom_1 + '\'' +
                ", checkincheckouthistoryid='" + checkincheckouthistoryid + '\'' +
                ", lastcheckedipaddress='" + lastcheckedipaddress + '\'' +
                ", teamid='" + teamid + '\'' +
                ", holidaycalendarid='" + holidaycalendarid + '\'' +
                ", workdaytemplateid='" + workdaytemplateid + '\'' +
                ", telephonyagentid='" + telephonyagentid + '\'' +
                ", zipcode='" + zipcode + '\'' +
                ", country='" + country + '\'' +
                ", state='" + state + '\'' +
                ", city='" + city + '\'' +
                ", address='" + address + '\'' +
                ", officelocationname='" + officelocationname + '\'' +
                ", availabilitystatus='" + availabilitystatus + '\'' +
                ", skills='" + skills + '\'' +
                ", salesregions='" + salesregions + '\'' +
                ", department='" + department + '\'' +
                ", team='" + team + '\'' +
                ", designation='" + designation + '\'' +
                ", photourl='" + photourl + '\'' +
                ", groups='" + groups + '\'' +
                ", manageruserid='" + manageruserid + '\'' +
                ", l1userid='" + l1userid + '\'' +
                ", l1designation='" + l1designation + '\'' +
                ", l2userid='" + l2userid + '\'' +
                ", l2designation='" + l2designation + '\'' +
                ", l3userid='" + l3userid + '\'' +
                ", l3designation='" + l3designation + '\'' +
                ", l4userid='" + l4userid + '\'' +
                ", l4designation='" + l4designation + '\'' +
                '}';
    }

    Integer modifyallleadsofgroup;
    Integer viewallleadsofgroup;
    Integer isagencyuser;
    Integer isadministrator;
    Integer isbillinguser;
    Integer ischeckedin;
    Integer ischeckinenabled;
    Integer autouserid;
    String role;
    String modifiedby;
    String createdby;
    String phoneothers;
    String phonemobile;
    String phonemain;
    String associatedphonenumbers;
    String signature_text;
    String signature_html;
    String dateformat;
    String timezone;
    String sessionid;
    String tmp_forgotpassword;
    String authtoken;
    String password;
    String emailaddress;
    String lastname;
    String middlename;
    String firstname;
    String userid;
    String lastcheckedon;
    String modifiedon;
    String createdon;
    Integer deletionstatuscode;
    Integer statusreason;
    Integer statuscode;
    Integer isdefaultowner;
    String mx_custom_10;
    String mx_custom_9;
    String mx_custom_8;
    String mx_custom_7;
    String mx_custom_6;
    String mx_custom_5;
    String mx_custom_4;
    String mx_custom_3;
    String mx_custom_2;
    String mx_custom_1;
    String checkincheckouthistoryid;
    String lastcheckedipaddress;
    String teamid;
    String holidaycalendarid;
    String workdaytemplateid;
    String telephonyagentid;
    String zipcode;
    String country;
    String state;
    String city;
    String address;
    String officelocationname;
    String availabilitystatus;
    String skills;
    String salesregions;
    String department;
    String team;
    String designation;
    String photourl;
    String groups;
    String manageruserid;
    String l1userid;
    String l1designation;
    String l2userid;
    String l2designation;
    String l3userid;
    String l3designation;
    String l4userid;
    String l4designation;
    String l5userid;
    String l5designation;
}
