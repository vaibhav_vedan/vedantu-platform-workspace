package com.vedantu.vedantudata.pojos.request;

import com.vedantu.util.fos.request.AbstractFrontEndListReq;
import com.vedantu.vedantudata.enums.salesconversion.AgentDesignation;
import com.vedantu.vedantudata.enums.salesconversion.AgentStatus;
import com.vedantu.vedantudata.enums.salesconversion.SalesTeam;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SalesAgentMetadataReq extends AbstractFrontEndListReq {

    private String zhId;
    private String rmId;
    private String cmId;
    private String centreId;
    private String tlId;
    private String isFMId;
    private String isRMId;
    private AgentStatus status;
    private AgentDesignation designation;
    private Long dateOfJoiningFrom;
    private Long dateOfJoiningTo;
    private Long dateOfLeavingFrom;
    private Long dateOfLeavingTo;
    private SalesTeam team;

    public List<String> validate(SalesAgentMetadataReq salesAgentMetadataReq) {
        List<String> errors = new ArrayList<>();
        if(salesAgentMetadataReq.getTeam() == null){
            errors.add("Team missing..");
        }
        return errors;
    }


}
