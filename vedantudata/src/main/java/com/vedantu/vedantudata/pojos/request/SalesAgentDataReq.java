package com.vedantu.vedantudata.pojos.request;

import com.vedantu.dinero.enums.Centre;
import com.vedantu.util.StringUtils;
import com.vedantu.vedantudata.enums.salesconversion.AgentDesignation;
import com.vedantu.vedantudata.enums.salesconversion.AgentDesignationType;
import com.vedantu.vedantudata.enums.salesconversion.AgentStatus;
import com.vedantu.vedantudata.enums.salesconversion.SalesTeam;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.index.Indexed;

import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SalesAgentDataReq {

    private String agentId;
    private String name;
    private String employeeId;
    private String email;
    private Long dateOfJoining;
    private AgentDesignation designation;
    private AgentDesignationType designationType;
    private String centreId;
    private String isRMId;
    private String isFMId;
    private String tlId;
    private AgentStatus status;
    private Long dateOfLeaving;
    private String leadsquaredId;
    private SalesTeam team;

    public static List<String> validate(SalesAgentDataReq salesAgentDataReq) {
        List<String> errors = new ArrayList<>();
        if (StringUtils.isEmpty(salesAgentDataReq.getName())) {
            errors.add("Agent name missing..");
        }
        if (StringUtils.isEmpty(salesAgentDataReq.getEmployeeId())) {
            errors.add("Agent employee id missing..");
        }
        if (StringUtils.isEmpty(salesAgentDataReq.getEmail())) {
            errors.add("Agent email missing..");
        }
        if (salesAgentDataReq.getDateOfJoining() == null) {
            errors.add("Agent DOJ missing..");
        }
        if (salesAgentDataReq.getDesignation() == null) {
            errors.add("Agent Designation missing..");
        }
        if (salesAgentDataReq.getDesignationType() == null) {
            errors.add("Agent designationType missing..");
        }
        if (salesAgentDataReq.getTeam() != null && SalesTeam.FOS.equals(salesAgentDataReq.getTeam())) {
            if (salesAgentDataReq.getDesignation() != null && !AgentDesignation.ZONAL_HEAD.equals(salesAgentDataReq.getDesignation())
                    && !AgentDesignation.REGIONAL_MANAGER.equals(salesAgentDataReq.getDesignation()) &&
                    !AgentDesignation.CLUSTER_MANAGER.equals(salesAgentDataReq.getDesignation()) && !AgentDesignation.CENTER_HEAD.equals(salesAgentDataReq.getDesignation())) {
                if (StringUtils.isEmpty(salesAgentDataReq.getCentreId())) {
                    errors.add("Agent centre missing..");
                }
            }
        }
        if (StringUtils.isEmpty(salesAgentDataReq.getLeadsquaredId())) {
            errors.add("Agent leadsquared id missing..");
        }
        if (salesAgentDataReq.getStatus() == null) {
            errors.add("Agent status missing..");
        }
        if (salesAgentDataReq.getTeam() == null) {
            errors.add("Agent team missing..");
        }
        return errors;
    }
}
