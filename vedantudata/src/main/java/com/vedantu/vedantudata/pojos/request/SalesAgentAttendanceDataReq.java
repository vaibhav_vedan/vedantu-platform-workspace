package com.vedantu.vedantudata.pojos.request;

import com.vedantu.util.fos.request.AbstractFrontEndListReq;
import com.vedantu.vedantudata.enums.salesconversion.AgentDesignation;
import com.vedantu.vedantudata.enums.salesconversion.AgentStatus;
import com.vedantu.vedantudata.enums.salesconversion.SalesTeam;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SalesAgentAttendanceDataReq extends AbstractFrontEndListReq {

    private String zhId;
    private String rmId;
    private String cmId;
    private String centreId;
    private String tlId;
    private String isFMId;
    private String isRMId;
    private AgentStatus status;
    private AgentDesignation designation;
    private Long dateOfJoiningFrom;
    private Long dateOfJoiningTo;
    private Long attendanceFrom;
    private Long attendanceTo;
    private SalesTeam team;

    public List<String> validate(SalesAgentAttendanceDataReq salesAgentAttendanceDataReq) {
        List<String> errors = new ArrayList<>();
        if (salesAgentAttendanceDataReq.getTeam() == null) {
            errors.add("Team missing..");
        }
        if (salesAgentAttendanceDataReq.getAttendanceFrom() != null && salesAgentAttendanceDataReq.getAttendanceTo() != null) {
            Long difference = salesAgentAttendanceDataReq.getAttendanceTo() - salesAgentAttendanceDataReq.getAttendanceFrom();
            Long days = difference / (1000 * 60 * 60 * 24);
            if (days > 31) {
                errors.add("Attendance range cannot exceed 31 days..");
            }
        }
        return errors;
    }
}
