package com.vedantu.vedantudata.pojos;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Transient;

import org.springframework.util.StringUtils;

import com.vedantu.User.UserBasicInfo;
import com.vedantu.onetofew.pojo.OTFSessionAttendeeInfo;
import com.vedantu.session.pojo.OTFSessionPojoUtils;
import com.vedantu.util.CollectionUtils;
import com.vedantu.util.DateTimeUtils;
import com.vedantu.util.TimeInterval;
import com.vedantu.vedantudata.googlesheets.IGoogleSheetEntity;
import com.vedantu.vedantudata.utils.CommonUtils;

@Deprecated
public class GTTAttendeeInfo implements IGoogleSheetEntity {
	private String userName;
	private String userId;
	private String userEmailId;
	private String userPhone;

	private String sessionId;
	private String batchId;
	private String courseId;

	private String courseName;
	private String sessionTitle;
	private String startTime;
	private String endTime;
	private String duration;
	private String teacher;
	private String teacherName;
	private String teacherEmailId;

	private Long timeInSession;
	private Integer noOfDisconnections;
	private String activeIntervals;
	private String joinTimes;

	@Transient
	private static String[] GOOGLE_SHEET_FIELDS = { "userName", "userId", "userEmailId", "userPhone", "courseName",
			"startTime", "endTime", "duration", "sessionTitle", "sessionId", "courseId", "batchId", "teacher",
			"timeInSession", "noOfDisconnections", "activeIntervals", "joinTimes" };

	public GTTAttendeeInfo() {
		super();
	}

	public GTTAttendeeInfo(OTFSessionAttendeeInfo attendeeInfo, OTFSessionPojoUtils sessionPojo) {
		super();
		this.userName = StringUtils.isEmpty(attendeeInfo.getFullName()) ? attendeeInfo.getFirstName()
				: attendeeInfo.getFullName();
		this.userId = String.valueOf(attendeeInfo.getUserId());
		this.userEmailId = attendeeInfo.getEmail();
		this.userPhone = attendeeInfo.getContactNumber();
//		this.batchId = sessionPojo.getBatchId();
		this.sessionId = sessionPojo.getId();
//		if (sessionPojo.getCourseInfo() != null) {
//			this.courseId = sessionPojo.getCourseInfo().getId();
//			this.courseName = sessionPojo.getCourseInfo().getTitle();
//		}
		this.sessionTitle = sessionPojo.getTitle();
		this.startTime = CommonUtils.calculateTimeStampIst(sessionPojo.getStartTime());
		this.endTime = CommonUtils.calculateTimeStampIst(sessionPojo.getEndTime());
		this.duration = String.valueOf(((sessionPojo.getEndTime() - sessionPojo.getStartTime()) / 1000));
		this.teacher = sessionPojo.getPresenter();
		if (sessionPojo.getPresenterInfo() != null) {
			this.teacherName = sessionPojo.getPresenterInfo().getFullName();
			this.teacherEmailId = sessionPojo.getPresenterInfo().getEmail();
		}

		this.timeInSession = attendeeInfo.getTimeInSession();
		this.noOfDisconnections = calculateNoOfDisconnections(attendeeInfo);
		this.joinTimes = fetchJoinTimes(attendeeInfo, sessionPojo.getStartTime());
		this.activeIntervals = fetchJoinIntervals(attendeeInfo, sessionPojo.getStartTime());
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getUserEmailId() {
		return userEmailId;
	}

	public void setUserEmailId(String userEmailId) {
		this.userEmailId = userEmailId;
	}

	public String getCourseName() {
		return courseName;
	}

	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}

	public String getSessionTitle() {
		return sessionTitle;
	}

	public void setSessionTitle(String sessionTitle) {
		this.sessionTitle = sessionTitle;
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public String getCourseId() {
		return courseId;
	}

	public void setCourseId(String courseId) {
		this.courseId = courseId;
	}

	public String getBatchId() {
		return batchId;
	}

	public void setBatchId(String batchId) {
		this.batchId = batchId;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public String getTeacher() {
		return teacher;
	}

	public void setTeacher(String teacher) {
		this.teacher = teacher;
	}

	public String getTeacherName() {
		return teacherName;
	}

	public void setTeacherName(String teacherName) {
		this.teacherName = teacherName;
	}

	public String getTeacherEmailId() {
		return teacherEmailId;
	}

	public void setTeacherEmailId(String teacherEmailId) {
		this.teacherEmailId = teacherEmailId;
	}

	public Long getTimeInSession() {
		return timeInSession;
	}

	public void setTimeInSession(Long timeInSession) {
		this.timeInSession = timeInSession;
	}

	public Integer getNoOfDisconnections() {
		return noOfDisconnections;
	}

	public void setNoOfDisconnections(Integer noOfDisconnections) {
		this.noOfDisconnections = noOfDisconnections;
	}

	public String getUserPhone() {
		return userPhone;
	}

	public void setUserPhone(String userPhone) {
		this.userPhone = userPhone;
	}

	public String getDuration() {
		return duration;
	}

	public void setDuration(String duration) {
		this.duration = duration;
	}

	public String getActiveIntervals() {
		return activeIntervals;
	}

	public void setActiveIntervals(String activeIntervals) {
		this.activeIntervals = activeIntervals;
	}

	public String getJoinTimes() {
		return joinTimes;
	}

	public void setJoinTimes(String joinTimes) {
		this.joinTimes = joinTimes;
	}

	@Override
	public String[] fetchGoogleSheetFields() {
		return GOOGLE_SHEET_FIELDS;
	}

	public Integer calculateNoOfDisconnections(OTFSessionAttendeeInfo otfSessionAttendeeInfo) {
		return CollectionUtils.isEmpty(otfSessionAttendeeInfo.getActiveIntervals()) ? null
				: otfSessionAttendeeInfo.getActiveIntervals().size() - 1;
	}

	public String fetchJoinTimes(OTFSessionAttendeeInfo otfSessionAttendeeInfo, long startTime) {
		String joinTimeStr = "";
		if (!CollectionUtils.isEmpty(otfSessionAttendeeInfo.getJoinTimes())) {
			List<String> joinTimeSlug = new ArrayList<>();
			for (Long joinTime : otfSessionAttendeeInfo.getJoinTimes()) {
				if (joinTime < (startTime - 30 * DateTimeUtils.MILLIS_PER_MINUTE)) {
					continue;
				}
				joinTimeSlug.add(CommonUtils.getDurationString((joinTime - startTime) / 1000));
			}
			if (!CollectionUtils.isEmpty(joinTimeSlug)) {
				joinTimeStr = String.join(" | ", joinTimeSlug);
			}
		}
		return joinTimeStr;
	}

	public void updateUserBasicInfo(UserBasicInfo userBasicInfo) {
		if (StringUtils.isEmpty(this.userId) || this.userId.equals(String.valueOf(userBasicInfo.getUserId()))) {
			this.userId = String.valueOf(userBasicInfo.getUserId());
			this.userEmailId = userBasicInfo.getEmail();
			this.userName = userBasicInfo.getFullName();
			this.userPhone = userBasicInfo.getContactNumber();
		}
	}

	public String fetchJoinIntervals(OTFSessionAttendeeInfo otfSessionAttendeeInfo, long startTime) {
		String activeIntervalsStr = "";
		if (!CollectionUtils.isEmpty(otfSessionAttendeeInfo.getActiveIntervals())) {
			List<String> joinTimeSlug = new ArrayList<>();
			for (TimeInterval timeInterval : otfSessionAttendeeInfo.getActiveIntervals()) {
				joinTimeSlug.add(CommonUtils.getDurationString((timeInterval.getStartTime() - startTime) / 1000) + "-"
						+ CommonUtils.getDurationString((timeInterval.getEndTime() - startTime) / 1000));
			}
			if (!CollectionUtils.isEmpty(joinTimeSlug)) {
				activeIntervalsStr = String.join(" | ", joinTimeSlug);
			}
		}
		return activeIntervalsStr;
	}

	@Override
	public String[] fetchGoogleSheetValues() {
		String[] fields = fetchGoogleSheetFields();
		String[] values = new String[fields.length];
		for (int i = 0; i < fields.length; i++) {
			try {
				Field field = this.getClass().getDeclaredField(fields[i]);
				field.setAccessible(true);
				values[i] = String.valueOf(field.get(this));
			} catch (IllegalArgumentException | IllegalAccessException | NoSuchFieldException | SecurityException ex) {
				// Dont throw
			}
		}
		return values;
	}

	@Override
	public String toString() {
		return "GTTAttendeeInfo [userName=" + userName + ", userId=" + userId + ", userEmailId=" + userEmailId
				+ ", userPhone=" + userPhone + ", sessionId=" + sessionId + ", batchId=" + batchId + ", courseId="
				+ courseId + ", courseName=" + courseName + ", sessionTitle=" + sessionTitle + ", startTime="
				+ startTime + ", endTime=" + endTime + ", teacher=" + teacher + ", teacherName=" + teacherName
				+ ", teacherEmailId=" + teacherEmailId + ", timeInSession=" + timeInSession + ", noOfDisconnections="
				+ noOfDisconnections + ", joinIntervals=" + activeIntervals + ", joinTimes=" + joinTimes + "]";
	}
}
