package com.vedantu.vedantudata.pojos;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ActivityOwnerChangeHistory {

    private String ownerBefore;
    private String ownerAfter;
    private String modifiedBy;
    private String modifiedByName;
    private Long modifiedOn;
}
