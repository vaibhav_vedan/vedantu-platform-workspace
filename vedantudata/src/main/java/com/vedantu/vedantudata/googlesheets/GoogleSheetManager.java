package com.vedantu.vedantudata.googlesheets;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.services.drive.Drive;
import com.google.api.services.drive.model.File;
import com.google.api.services.drive.model.Permission;
import com.google.api.services.sheets.v4.Sheets;
import com.google.api.services.sheets.v4.model.BatchUpdateSpreadsheetRequest;
import com.google.api.services.sheets.v4.model.BatchUpdateSpreadsheetResponse;
import com.google.api.services.sheets.v4.model.DimensionRange;
import com.google.api.services.sheets.v4.model.GridCoordinate;
import com.google.api.services.sheets.v4.model.GridData;
import com.google.api.services.sheets.v4.model.InsertDimensionRequest;
import com.google.api.services.sheets.v4.model.Request;
import com.google.api.services.sheets.v4.model.RowData;
import com.google.api.services.sheets.v4.model.Spreadsheet;
import com.google.api.services.sheets.v4.model.SpreadsheetProperties;
import com.google.api.services.sheets.v4.model.UpdateCellsRequest;
import com.google.gson.Gson;
import com.vedantu.util.CollectionUtils;
import com.vedantu.util.DateTimeUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.PlatformTools;
import com.vedantu.util.StringUtils;
import com.vedantu.vedantudata.dao.serializers.GoogleSheetPropertiesDAO;
import com.vedantu.vedantudata.entities.GoogleSheetProperties;
import com.vedantu.vedantudata.enums.GoogleItemType;
import com.vedantu.vedantudata.enums.GoogleSheetType;
import com.vedantu.vedantudata.utils.CommonUtils;
import com.vedantu.vedantudata.utils.GoogleSheetUtils;

@Service
public class GoogleSheetManager extends GoogleSheetConfig {

	@Autowired
	protected GoogleSheetPropertiesDAO googleSheetPropertiesDAO;

	@Autowired
	protected PlatformTools platformTools;

	@Autowired
	private LogFactory logFactory;

	@SuppressWarnings("static-access")
	private Logger logger = logFactory.getLogger(GoogleSheetManager.class);

	private static GoogleCredential googleCredential = null;
	private static Sheets sheetService = null;
	private static Drive driveService = null;

	protected static Gson gson = new Gson();

	@PostConstruct
	private void init() {
		logger.info("Abstract sheet manager init");
		try {
			HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();
		} catch (Throwable t) {
			logger.error("Error init Sheet manageR:" + t.getMessage() + " string:" + t.toString());
		}

		try {
			googleCredential = getGoogleCredentials();
			sheetService = getSheetsService();
			driveService = getDriveService();
		} catch (Exception ex) {
			logger.error("Sheet service init error:" + ex.toString() + " message:" + ex.getMessage());
		}

		try {
			VEDANTU_DATA_FOLDER_ID = getVedantudataFolderId();
		} catch (Exception ex) {
			logger.error("Error occured creating vedantu data folder:" + VEDANTU_DATA_FOLDER_ID);
		}
	}

	public com.google.api.services.drive.model.File createFolder(String folderName, String parentFolderId)
			throws IOException {
		com.google.api.services.drive.model.File fileMetadata = new com.google.api.services.drive.model.File();
		fileMetadata.setName(folderName);
		fileMetadata.setMimeType("application/vnd.google-apps.folder");
		com.google.api.services.drive.model.File folder = driveService.files().create(fileMetadata).setFields("id")
				.execute();
		if (!StringUtils.isEmpty(parentFolderId)) {
			driveService.files().update(folder.getId(), null).setAddParents(parentFolderId).setFields("id, parents")
					.execute();
		}
		giveDefaultPermissions(folder.getId());
		logger.info("Folder id :" + folder.getId());
		return folder;
	}

	public String createSpreadsheet(String title, String folderId) throws IOException {
		// Create spread sheet
		logger.info("Title : " + title);
		Spreadsheet spreadSheet = constructSpreadSheet(title);
		spreadSheet = sheetService.spreadsheets().create(spreadSheet).execute();
		logger.info("Id:" + spreadSheet.toPrettyString());

		// Add permissions
		addParentFolder(spreadSheet.getSpreadsheetId(), folderId);
		giveDefaultPermissions(spreadSheet.getSpreadsheetId());
		return spreadSheet.getSpreadsheetId();
	}

	public void setValues(String spreadsheetId, int startRow, String[][] fieldsList) throws IOException {
		Spreadsheet spreadsheet = sheetService.spreadsheets().get(spreadsheetId).execute();
		List<GridData> gridDatas = spreadsheet.getSheets().get(0).getData();
		List<RowData> rowList = new ArrayList<>();
		for (String[] fields : fieldsList) {
			if (fields != null) {
				RowData rowData = GoogleSheetUtils.getRowData(fields);
				rowList.add(rowData);
			}
		}
		if (gridDatas == null) {
			gridDatas = new ArrayList<>();
		}
		gridDatas.add(new GridData().setRowData(rowList));
		spreadsheet.getSheets().get(0).setData(gridDatas);
		BatchUpdateSpreadsheetRequest batchUpdateReq = new BatchUpdateSpreadsheetRequest();
		UpdateCellsRequest updateCells = new UpdateCellsRequest();
		updateCells.setFields("*");
		updateCells.setRows(rowList);
		GridCoordinate gridCoordinate = new GridCoordinate();
		gridCoordinate.setRowIndex(startRow);
		gridCoordinate.setColumnIndex(0);
		updateCells.setStart(gridCoordinate);

		Request request = new Request();
		request.setUpdateCells(updateCells);

		List<Request> requests = new ArrayList<>();
		requests.add(request);

		batchUpdateReq.setRequests(requests);
		sheetService.spreadsheets().batchUpdate(spreadsheetId, batchUpdateReq).execute();
	}

	public void setValues(String spreadsheetId, int rowNumber, String[] fields) throws IOException {
		Spreadsheet spreadsheet = sheetService.spreadsheets().get(spreadsheetId).execute();
		List<GridData> gridDatas = spreadsheet.getSheets().get(0).getData();
		RowData rowData = GoogleSheetUtils.getRowData(fields);
		List<RowData> rowList = new ArrayList<>();
		rowList.add(rowData);
		if (gridDatas == null) {
			gridDatas = new ArrayList<>();
		}
		gridDatas.add(new GridData().setRowData(rowList));
		spreadsheet.getSheets().get(0).setData(gridDatas);
		BatchUpdateSpreadsheetRequest batchUpdateReq = new BatchUpdateSpreadsheetRequest();
		UpdateCellsRequest updateCells = new UpdateCellsRequest();
		updateCells.setFields("*");
		updateCells.setRows(rowList);
		GridCoordinate gridCoordinate = new GridCoordinate();
		gridCoordinate.setRowIndex(rowNumber);
		gridCoordinate.setColumnIndex(0);
		updateCells.setStart(gridCoordinate);

		Request request = new Request();
		request.setUpdateCells(updateCells);

		List<Request> requests = new ArrayList<>();
		requests.add(request);

		batchUpdateReq.setRequests(requests);
		sheetService.spreadsheets().batchUpdate(spreadsheetId, batchUpdateReq).execute();
	}

	protected void addParentFolder(String fileId, String folderId) throws IOException {
		Permission newPermission = getDefaultPermission();
		driveService.files().update(fileId, null).setAddParents(folderId).setFields("id, parents").execute();
		driveService.permissions().create(fileId, newPermission).setSendNotificationEmail(false).execute();
	}

	protected void giveDefaultPermissions(String fileId) throws IOException {
		Permission newPermission = new Permission();
		newPermission.setType("user");
		newPermission.setEmailAddress(DEFAULT_OWNER_EMAILID);
		newPermission.setRole("writer");
		driveService.permissions().create(fileId, newPermission).setSendNotificationEmail(false).execute();
	}

	protected static String getGoogleSheetFileName(GoogleSheetType googleSheetType, String fileName,
			String additionalName) {
		long currentTime = DateTimeUtils.getISTDayStartTime(System.currentTimeMillis());
		String sheetName = googleSheetType.name();
		if (StringUtils.isEmpty(fileName)) {
			fileName = CommonUtils.calculateTimeStampIst(currentTime);
		}
		sheetName += "_" + fileName;

		if (!StringUtils.isEmpty(additionalName)) {
			sheetName += "_" + additionalName;
		}
		return sheetName;
	}

	protected void updateRowLimit(String spreadSheetId) throws IOException {
		List<Request> requests = new ArrayList<>();
		InsertDimensionRequest insertDimensionRequest = new InsertDimensionRequest();
		DimensionRange dr = new DimensionRange();
		dr.setStartIndex(0);
		dr.setEndIndex(20000);
		dr.setDimension("ROWS");
		insertDimensionRequest.setRange(dr);
		requests.add(new Request().setInsertDimension(insertDimensionRequest));
		BatchUpdateSpreadsheetRequest body = new BatchUpdateSpreadsheetRequest().setRequests(requests);
		BatchUpdateSpreadsheetResponse response = sheetService.spreadsheets().batchUpdate(spreadSheetId, body)
				.execute();
		logger.info("Response : " + response.toPrettyString());
	}

	private synchronized String getVedantudataFolderId() throws IOException {
		// Fetch vedantu folder if already existing
		Query query = new Query();
		query.addCriteria(Criteria.where(GoogleSheetProperties.Constants.ITEM_NAME).is(VEDANTU_DATA_FOLDER_NAME));
		query.addCriteria(Criteria.where(GoogleSheetProperties.Constants.ITEM_TYPE).is(GoogleItemType.FOLDER));
		List<GoogleSheetProperties> results = googleSheetPropertiesDAO.runQuery(query, GoogleSheetProperties.class);
		if (!CollectionUtils.isEmpty(results)) {
			GoogleSheetProperties googleSheetProperties = results.get(0);
			return googleSheetProperties.getItemId();
		}

		// Create vedantu folder if it does not exist
		File folder = createFolder(VEDANTU_DATA_FOLDER_NAME, null);
		giveDefaultPermissions(folder.getId());
		GoogleSheetProperties googleSheetProperties = new GoogleSheetProperties(folder.getId(),
				VEDANTU_DATA_FOLDER_NAME, null, GoogleSheetType.VEDANTU_DATA, GoogleItemType.FOLDER, 0);
		googleSheetPropertiesDAO.create(googleSheetProperties);
		return folder.getId();
	}

	private Sheets getSheetsService() throws IOException, GeneralSecurityException {
		return new Sheets.Builder(HTTP_TRANSPORT, JSON_FACTORY, googleCredential).setApplicationName(APPLICATION_NAME)
				.build();
	}

	private Drive getDriveService() throws IOException, GeneralSecurityException {
		return new Drive.Builder(HTTP_TRANSPORT, JSON_FACTORY, googleCredential).build();
	}

	private GoogleCredential getGoogleCredentials() throws GeneralSecurityException, IOException {
		Resource resource = new ClassPathResource("/vedantu-data-b9f33b3cb747.p12");
		return new GoogleCredential.Builder().setTransport(HTTP_TRANSPORT).setJsonFactory(JSON_FACTORY)
				.setServiceAccountId("google-sheets@vedantu-data.iam.gserviceaccount.com")
				.setServiceAccountScopes(SCOPES).setServiceAccountPrivateKeyFromP12File(resource.getFile())
				.setServiceAccountUser("google-sheets@vedantu-data.iam.gserviceaccount.com").build();
	}

	private static Spreadsheet constructSpreadSheet(String title) {
		Spreadsheet spreadSheet = new Spreadsheet();
		SpreadsheetProperties spreadsheetProperties = new SpreadsheetProperties();
		spreadsheetProperties.setTitle(title);
		spreadSheet.setProperties(spreadsheetProperties);
		return spreadSheet;
	}

	private static Permission getDefaultPermission() {
		Permission newPermission = new Permission();
		newPermission.setType("user");
		newPermission.setEmailAddress(DEFAULT_OWNER_EMAILID);
		newPermission.setRole("writer");
		return newPermission;
	}
}
