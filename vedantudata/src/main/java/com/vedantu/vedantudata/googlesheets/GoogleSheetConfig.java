package com.vedantu.vedantudata.googlesheets;

import java.util.Arrays;
import java.util.List;

import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.sheets.v4.SheetsScopes;
import com.vedantu.vedantudata.enums.GoogleSheetType;

public class GoogleSheetConfig {
	protected static final String APPLICATION_NAME = "Vedantu sheets";
	protected static final JsonFactory JSON_FACTORY = JacksonFactory.getDefaultInstance();
	protected static HttpTransport HTTP_TRANSPORT;
	protected static final List<String> SCOPES = Arrays.asList(SheetsScopes.SPREADSHEETS, SheetsScopes.DRIVE);
	protected static final String DEFAULT_SHEET_NAME = "Sheet1";
	protected static String DEFAULT_OWNER_EMAILID = "domain.admin@vedantu.com";

	protected static String VEDANTU_DATA_FOLDER_NAME = GoogleSheetType.VEDANTU_DATA.name();
	protected String VEDANTU_DATA_FOLDER_ID = null;
}
