package com.vedantu.vedantudata.googlesheets;

public interface IGoogleSheetEntity {

	public String[] fetchGoogleSheetFields();

	public String[] fetchGoogleSheetValues();
}
