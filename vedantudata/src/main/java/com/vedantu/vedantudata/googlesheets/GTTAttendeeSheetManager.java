package com.vedantu.vedantudata.googlesheets;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import javax.annotation.PostConstruct;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import com.google.api.services.drive.model.File;
import com.vedantu.util.CollectionUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.vedantudata.entities.GoogleSheetProperties;
import com.vedantu.vedantudata.enums.GoogleItemType;
import com.vedantu.vedantudata.enums.GoogleSheetType;
import com.vedantu.vedantudata.pojos.GTTAttendeeInfo;
import com.vedantu.vedantudata.utils.CommonUtils;

@Service
public class GTTAttendeeSheetManager {

	@Autowired
	private GoogleSheetManager googleSheetManager;

	@Autowired
	private LogFactory logFactory;

	@SuppressWarnings("static-access")
	private Logger logger = logFactory.getLogger(GTTAttendeeSheetManager.class);

	private static String GTT_ATTENDEE_FOLDER_ID = null;
	private static String GTT_ATTENDEE_FOLDER_NAME = GoogleSheetType.GTT_ATTENDEE.name();

	@PostConstruct
	private void init() {
		try {
			GTT_ATTENDEE_FOLDER_ID = getGttAttendeeFolderId();
		} catch (Exception ex) {
			logger.error(
					"Error occured creating gtt attendee folder : " + ex.toString() + " message:" + ex.getMessage());
		}
	}

	public void updateGTTAttendees(List<GTTAttendeeInfo> gttAttendeeInfos, long startTime, long endTime)
			throws Exception {
		logger.info("Request:" + (gttAttendeeInfos == null ? 0 : gttAttendeeInfos.size()));
		GoogleSheetProperties currentAttendeeSheet = getGTTSpreadSheet(startTime, endTime);
		if (!CollectionUtils.isEmpty(gttAttendeeInfos)) {
			if (gttAttendeeInfos.size() > 500) {
				googleSheetManager.updateRowLimit(currentAttendeeSheet.getItemId());
			}

			int size = 500;
			int start = 0;
			for (int i = start; i < gttAttendeeInfos.size(); i += size) {
				String[][] fieldList = new String[gttAttendeeInfos.size()][];
				for (int index = i; index < (i + size) && index < gttAttendeeInfos.size(); index++) {
					GTTAttendeeInfo entry = gttAttendeeInfos.get(index);
					try {
						fieldList[index] = entry.fetchGoogleSheetValues();
					} catch (Exception ex) {
						logger.info("Updating gtt attendee : " + entry.toString() + " ex:" + ex.getMessage() + " ex:"
								+ ex.toString());
					}
				}
                                logger.info(i + " setting at row " + (currentAttendeeSheet.getLastUpdatedRow() + 1)
                                    + " fields " + Arrays.deepToString(fieldList));
				googleSheetManager.setValues(currentAttendeeSheet.getItemId(),
						currentAttendeeSheet.getLastUpdatedRow() + 1, fieldList);
				currentAttendeeSheet.setLastUpdatedRow(currentAttendeeSheet.getLastUpdatedRow() + size);
				googleSheetManager.googleSheetPropertiesDAO.save(currentAttendeeSheet);
			}
		}
	}

	private GoogleSheetProperties getGTTSpreadSheet(long startTime, long endTime) throws IOException {
		String fileName = CommonUtils.calculateTimeStampIst(startTime) + "-"
				+ CommonUtils.calculateTimeStampIst(endTime);
		fileName = GoogleSheetManager.getGoogleSheetFileName(GoogleSheetType.GTT_ATTENDEE, fileName, null);
		Query query = new Query();
		query.addCriteria(Criteria.where(GoogleSheetProperties.Constants.ITEM_NAME).is(fileName));
		query.addCriteria(Criteria.where(GoogleSheetProperties.Constants.SHEET_TYPE).is(GoogleSheetType.GTT_ATTENDEE));
		List<GoogleSheetProperties> results = googleSheetManager.googleSheetPropertiesDAO.runQuery(query,
				GoogleSheetProperties.class);

		if (!CollectionUtils.isEmpty(results)) {
			return results.get(0);
		}

		// Create spread sheet
		String spreadsheetId = googleSheetManager.createSpreadsheet(fileName, GTT_ATTENDEE_FOLDER_ID);

		// Set field values
		GTTAttendeeInfo gttAttendeeInfo = new GTTAttendeeInfo();
		String[] fields = gttAttendeeInfo.fetchGoogleSheetFields();
		googleSheetManager.setValues(spreadsheetId, 0, fields);

		// Create the sheet
		GoogleSheetProperties newSpreadsheet = new GoogleSheetProperties(spreadsheetId, fileName, Arrays.asList(fields),
				GoogleSheetType.GTT_ATTENDEE, GoogleItemType.SPREADSHEET, 0);
		return newSpreadsheet;
	}

	private String getGttAttendeeFolderId() throws IOException {
		// Fetch vedantu folder if already existing
		Query query = new Query();
		query.addCriteria(Criteria.where(GoogleSheetProperties.Constants.ITEM_NAME).is(GTT_ATTENDEE_FOLDER_NAME));
		query.addCriteria(Criteria.where(GoogleSheetProperties.Constants.ITEM_TYPE).is(GoogleItemType.FOLDER));
		List<GoogleSheetProperties> results = googleSheetManager.googleSheetPropertiesDAO.runQuery(query,
				GoogleSheetProperties.class);
		if (!CollectionUtils.isEmpty(results)) {
			GoogleSheetProperties googleSheetProperties = results.get(0);
			return googleSheetProperties.getItemId();
		}

		// Create vedantu folder if it does not exist
		File folder = googleSheetManager.createFolder(GTT_ATTENDEE_FOLDER_NAME,
				googleSheetManager.VEDANTU_DATA_FOLDER_ID);
		googleSheetManager.giveDefaultPermissions(folder.getId());
		GoogleSheetProperties googleSheetProperties = new GoogleSheetProperties(folder.getId(),
				GTT_ATTENDEE_FOLDER_NAME, null, GoogleSheetType.GTT_ATTENDEE, GoogleItemType.FOLDER, 0);
		googleSheetManager.googleSheetPropertiesDAO.create(googleSheetProperties);
		return folder.getId();
	}
}
