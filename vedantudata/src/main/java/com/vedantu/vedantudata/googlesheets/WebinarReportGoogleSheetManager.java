package com.vedantu.vedantudata.googlesheets;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import javax.annotation.PostConstruct;

import org.apache.commons.lang.math.RandomUtils;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import com.google.api.services.drive.model.File;
import com.vedantu.util.CollectionUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.vedantudata.entities.GoogleSheetProperties;
import com.vedantu.vedantudata.enums.GoogleItemType;
import com.vedantu.vedantudata.enums.GoogleSheetType;
import com.vedantu.vedantudata.pojos.WebinarReport;
import com.vedantu.vedantudata.utils.CommonUtils;

@Service
public class WebinarReportGoogleSheetManager {

	@Autowired
	private GoogleSheetManager googleSheetManager;

	@Autowired
	private LogFactory logFactory;

	@SuppressWarnings("static-access")
	private Logger logger = logFactory.getLogger(WebinarReportGoogleSheetManager.class);

	private static String WEBINAR_REPORT_ID = null;
	private static String WEBINAR_REPORT_NAME = GoogleSheetType.WEBINAR_REPORT.name();

	@PostConstruct
	private void init() {
		try {
			WEBINAR_REPORT_ID = getWebinarReportId();
		} catch (Exception ex) {
			logger.error("Error occured creating WebinarReport folder : " + ex.toString() + " message:" + ex.getMessage());
		}
	}

	public void updateWebinarEntries(List<WebinarReport> webinars) throws Exception {
		logger.info("Request:" + (webinars == null ? 0 : webinars.size()));
		GoogleSheetProperties currentWebinarReportSheet = getCurrentDayWebinarReportSheet();
		updateWebinarReportInSheet(webinars, currentWebinarReportSheet);
	}

	public void updateWebinarEntries(List<WebinarReport> webinarEntries, long startTime, long endTime) throws Exception {
		logger.info("Request:" + (webinarEntries == null ? 0 : webinarEntries.size()));
		GoogleSheetProperties webinarReportSheet = createWebinarReportSheet(startTime, endTime);
		if (webinarEntries.size() > 500) {
			googleSheetManager.updateRowLimit(webinarReportSheet.getItemId());
		}

		updateWebinarReportInSheet(webinarEntries, webinarReportSheet);
	}

	private void updateWebinarReportInSheet(List<WebinarReport> webinarInfos, GoogleSheetProperties currentWebinarReportSheet)
			throws Exception {
		if (!CollectionUtils.isEmpty(webinarInfos)) {
			String[][] fieldList = new String[webinarInfos.size()][];
			int index = 0;
			for (WebinarReport webinarEntry : webinarInfos) {
				try {
					fieldList[index] = webinarEntry.fetchGoogleSheetValues();
					index++;
				} catch (Exception ex) {
					logger.info(
							"Updating WebinarReport : " + webinarEntry.toString() + " ex:" + ex.getMessage() + " ex:" + ex.toString());
				}
			}
			googleSheetManager.setValues(currentWebinarReportSheet.getItemId(), currentWebinarReportSheet.getLastUpdatedRow() + 1,
					fieldList);
			currentWebinarReportSheet.setLastUpdatedRow(currentWebinarReportSheet.getLastUpdatedRow() + index);
			googleSheetManager.googleSheetPropertiesDAO.save(currentWebinarReportSheet);
		}
	}

	private GoogleSheetProperties createWebinarReportSheet(long startTime, long endTime) throws IOException {

		int randomInt = RandomUtils.nextInt();
		String fileName = GoogleSheetManager.getGoogleSheetFileName(GoogleSheetType.WEBINAR_REPORT,
				CommonUtils.calculateTimeStampIst(startTime) + "-" + CommonUtils.calculateTimeStampIst(endTime),
				String.valueOf(randomInt));
		// Create spread sheet
		String spreadsheetId = googleSheetManager.createSpreadsheet(fileName, WEBINAR_REPORT_ID);

		// Set field values
		WebinarReport webinarEntry = new WebinarReport();
		String[] fields = webinarEntry.fetchGoogleSheetFields();
		googleSheetManager.setValues(spreadsheetId, 0, fields);

		// Create the sheet
		GoogleSheetProperties newSpreadsheet = new GoogleSheetProperties(spreadsheetId, fileName, Arrays.asList(fields),
				GoogleSheetType.WEBINAR_REPORT, GoogleItemType.SPREADSHEET, 0);
		return newSpreadsheet;

	}

	private GoogleSheetProperties getCurrentDayWebinarReportSheet() throws IOException {
		String fileName = GoogleSheetManager.getGoogleSheetFileName(GoogleSheetType.WEBINAR_REPORT, null, null);
		Query query = new Query();
		query.addCriteria(Criteria.where(GoogleSheetProperties.Constants.ITEM_NAME).is(fileName));
		query.addCriteria(Criteria.where(GoogleSheetProperties.Constants.SHEET_TYPE).is(GoogleSheetType.WEBINAR_REPORT));
		List<GoogleSheetProperties> results = googleSheetManager.googleSheetPropertiesDAO.runQuery(query,
				GoogleSheetProperties.class);

		if (!CollectionUtils.isEmpty(results)) {
			return results.get(0);
		}

		// Create spread sheet
		String spreadsheetId = googleSheetManager.createSpreadsheet(fileName, WEBINAR_REPORT_ID);

		// Set field values
		WebinarReport webinarEntry = new WebinarReport();
		String[] fields = webinarEntry.fetchGoogleSheetFields();
		googleSheetManager.setValues(spreadsheetId, 0, fields);

		// Create the sheet
		GoogleSheetProperties newSpreadsheet = new GoogleSheetProperties(spreadsheetId, fileName, Arrays.asList(fields),
				GoogleSheetType.WEBINAR_REPORT, GoogleItemType.SPREADSHEET, 0);
		return newSpreadsheet;
	}

	private String getWebinarReportId() throws IOException {
		// Fetch vedantu folder if already existing
		Query query = new Query();
		query.addCriteria(Criteria.where(GoogleSheetProperties.Constants.ITEM_NAME).is(WEBINAR_REPORT_NAME));
		query.addCriteria(Criteria.where(GoogleSheetProperties.Constants.ITEM_TYPE).is(GoogleItemType.FOLDER));
		List<GoogleSheetProperties> results = googleSheetManager.googleSheetPropertiesDAO.runQuery(query,
				GoogleSheetProperties.class);
		if (!CollectionUtils.isEmpty(results)) {
			GoogleSheetProperties googleSheetProperties = results.get(0);
			return googleSheetProperties.getItemId();
		}

		// Create vedantu folder if it does not exist
		File folder = googleSheetManager.createFolder(WEBINAR_REPORT_NAME, googleSheetManager.VEDANTU_DATA_FOLDER_ID);
		googleSheetManager.giveDefaultPermissions(folder.getId());
		GoogleSheetProperties googleSheetProperties = new GoogleSheetProperties(folder.getId(), WEBINAR_REPORT_NAME, null,
				GoogleSheetType.WEBINAR_REPORT, GoogleItemType.FOLDER, 0);
		googleSheetManager.googleSheetPropertiesDAO.create(googleSheetProperties);
		return folder.getId();
	}

}
