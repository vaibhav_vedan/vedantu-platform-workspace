package com.vedantu.vedantudata.googlesheets;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import javax.annotation.PostConstruct;

import org.apache.commons.lang.math.RandomUtils;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import com.google.api.services.drive.model.File;
import com.vedantu.util.CollectionUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.vedantudata.entities.GoogleSheetProperties;
import com.vedantu.vedantudata.enums.GoogleItemType;
import com.vedantu.vedantudata.enums.GoogleSheetType;
import com.vedantu.vedantudata.pojos.VedantuDataOrder;
import com.vedantu.vedantudata.utils.CommonUtils;

@Service
public class OrdersGoogleSheetManager {

	@Autowired
	private GoogleSheetManager googleSheetManager;

	@Autowired
	private LogFactory logFactory;

	@SuppressWarnings("static-access")
	private Logger logger = logFactory.getLogger(OrdersGoogleSheetManager.class);

	private static String ORDER_FOLDER_ID = null;
	private static String ORDER_FOLDER_NAME = GoogleSheetType.ORDERS.name();

	@PostConstruct
	private void init() {
		try {
			ORDER_FOLDER_ID = getOrdersFolderId();
		} catch (Exception ex) {
			logger.error("Error occured creating order folder : " + ex.toString() + " message:" + ex.getMessage());
		}
	}

	public void updateOrders(List<VedantuDataOrder> orders) throws Exception {
		logger.info("Request:" + (orders == null ? 0 : orders.size()));
		GoogleSheetProperties currentOrdersSheet = getCurrentDayOrderSheet();
		updateOrdersInSheet(orders, currentOrdersSheet);
	}

	public void updateOrders(List<VedantuDataOrder> orders, long startTime, long endTime) throws Exception {
		logger.info("Request:" + (orders == null ? 0 : orders.size()));
		GoogleSheetProperties currentOrdersSheet = createOrderSheet(startTime, endTime);
		if (orders.size() > 500) {
			googleSheetManager.updateRowLimit(currentOrdersSheet.getItemId());
		}

		updateOrdersInSheet(orders, currentOrdersSheet);
	}

	private void updateOrdersInSheet(List<VedantuDataOrder> orders, GoogleSheetProperties currentOrdersSheet)
			throws Exception {
		if (!CollectionUtils.isEmpty(orders)) {
			String[][] fieldList = new String[orders.size()][];
			int index = 0;
			for (VedantuDataOrder order : orders) {
				try {
					fieldList[index] = order.fetchGoogleSheetValues();
					index++;
				} catch (Exception ex) {
					logger.info(
							"Updating order : " + order.toString() + " ex:" + ex.getMessage() + " ex:" + ex.toString());
				}
			}
			googleSheetManager.setValues(currentOrdersSheet.getItemId(), currentOrdersSheet.getLastUpdatedRow() + 1,
					fieldList);
			currentOrdersSheet.setLastUpdatedRow(currentOrdersSheet.getLastUpdatedRow() + index);
			googleSheetManager.googleSheetPropertiesDAO.save(currentOrdersSheet);
		}
	}

	private GoogleSheetProperties createOrderSheet(long startTime, long endTime) throws IOException {

		int randomInt = RandomUtils.nextInt();
		String fileName = GoogleSheetManager.getGoogleSheetFileName(GoogleSheetType.ORDERS,
				CommonUtils.calculateTimeStampIst(startTime) + "-" + CommonUtils.calculateTimeStampIst(endTime),
				String.valueOf(randomInt));
		// Create spread sheet
		String spreadsheetId = googleSheetManager.createSpreadsheet(fileName, ORDER_FOLDER_ID);

		// Set field values
		VedantuDataOrder vedantuDataOrder = new VedantuDataOrder();
		String[] fields = vedantuDataOrder.fetchGoogleSheetFields();
		googleSheetManager.setValues(spreadsheetId, 0, fields);

		// Create the sheet
		GoogleSheetProperties newSpreadsheet = new GoogleSheetProperties(spreadsheetId, fileName, Arrays.asList(fields),
				GoogleSheetType.ORDERS, GoogleItemType.SPREADSHEET, 0);
		return newSpreadsheet;

	}

	private GoogleSheetProperties getCurrentDayOrderSheet() throws IOException {
		String fileName = GoogleSheetManager.getGoogleSheetFileName(GoogleSheetType.ORDERS, null, null);
		Query query = new Query();
		query.addCriteria(Criteria.where(GoogleSheetProperties.Constants.ITEM_NAME).is(fileName));
		query.addCriteria(Criteria.where(GoogleSheetProperties.Constants.SHEET_TYPE).is(GoogleSheetType.ORDERS));
		List<GoogleSheetProperties> results = googleSheetManager.googleSheetPropertiesDAO.runQuery(query,
				GoogleSheetProperties.class);

		if (!CollectionUtils.isEmpty(results)) {
			return results.get(0);
		}

		// Create spread sheet
		String spreadsheetId = googleSheetManager.createSpreadsheet(fileName, ORDER_FOLDER_ID);

		// Set field values
		VedantuDataOrder vedantuDataOrder = new VedantuDataOrder();
		String[] fields = vedantuDataOrder.fetchGoogleSheetFields();
		googleSheetManager.setValues(spreadsheetId, 0, fields);

		// Create the sheet
		GoogleSheetProperties newSpreadsheet = new GoogleSheetProperties(spreadsheetId, fileName, Arrays.asList(fields),
				GoogleSheetType.ORDERS, GoogleItemType.SPREADSHEET, 0);
		return newSpreadsheet;
	}

	private String getOrdersFolderId() throws IOException {
		// Fetch vedantu folder if already existing
		Query query = new Query();
		query.addCriteria(Criteria.where(GoogleSheetProperties.Constants.ITEM_NAME).is(ORDER_FOLDER_NAME));
		query.addCriteria(Criteria.where(GoogleSheetProperties.Constants.ITEM_TYPE).is(GoogleItemType.FOLDER));
		List<GoogleSheetProperties> results = googleSheetManager.googleSheetPropertiesDAO.runQuery(query,
				GoogleSheetProperties.class);
		if (!CollectionUtils.isEmpty(results)) {
			GoogleSheetProperties googleSheetProperties = results.get(0);
			return googleSheetProperties.getItemId();
		}

		// Create vedantu folder if it does not exist
		File folder = googleSheetManager.createFolder(ORDER_FOLDER_NAME, googleSheetManager.VEDANTU_DATA_FOLDER_ID);
		googleSheetManager.giveDefaultPermissions(folder.getId());
		GoogleSheetProperties googleSheetProperties = new GoogleSheetProperties(folder.getId(), ORDER_FOLDER_NAME, null,
				GoogleSheetType.ORDERS, GoogleItemType.FOLDER, 0);
		googleSheetManager.googleSheetPropertiesDAO.create(googleSheetProperties);
		return folder.getId();
	}
}
