package com.vedantu.vedantudata.entities.salesconversion;

import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import com.vedantu.vedantudata.pojos.AttendedSessionInfo;
import lombok.Data;
import lombok.ToString;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;
import java.util.List;

@Data
@ToString
@Document(collection = "UserSessionDetails")
@CompoundIndexes({
        @CompoundIndex(name = "userId_1_date_1", def = "{'userId':1, 'date':1}", background = true, unique = true)
})
public class UserSessionDetails extends AbstractMongoStringIdEntity {

    private Long userId;
    private Long date;
    private Long totalTimeInSessions = 0L;
    private Integer totalClassAttended = 0;
    private List<AttendedSessionInfo> attendedSessionInfos;
    private Date lastUpdatedDate;

    public static class Constants {
        public static final String _ID = "_id";
        public static final String USER_ID = "userId";
        public static final String DATE = "date";
        public static final String TOTAL_TIME_IN_SESSION = "totalTimeInSessions";
        public static final String TOTAL_CLASS_ATTENDED = "totalClassAttended";
        public static final String ATTENDED_SESSION_INFO = "attendedSessionInfos";

    }

}
