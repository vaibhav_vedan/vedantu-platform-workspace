package com.vedantu.vedantudata.entities.salesconversion;

import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import com.vedantu.vedantudata.enums.salesconversion.AgentDesignation;
import com.vedantu.vedantudata.enums.salesconversion.AgentDesignationType;
import com.vedantu.vedantudata.enums.salesconversion.AgentStatus;
import com.vedantu.vedantudata.enums.salesconversion.SalesTeam;
import lombok.Data;
import lombok.ToString;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@ToString
@Document(collection = "SalesAgentInfo")
@CompoundIndex(name = "designationType_team", def = "{'designationType':1, 'team' : 1}", background = true)

public class SalesAgentInfo extends AbstractMongoStringIdEntity {

    private String name;

    @Indexed(background = true, unique = true)
    private String employeeId;

    @Indexed(background = true, unique = true)
    private String email;

    private Long dateOfJoining;

    @Indexed(background = true)
    private AgentDesignation designation;

    @Indexed(background = true)
    private AgentDesignationType designationType;

    @Indexed(background = true)
    private String centreId;

    @Indexed(background = true)
    private String isRMId;

    @Indexed(background = true)
    private String isFMId;

    @Indexed(background = true)
    private String tlId;

    @Indexed(background = true)
    private AgentStatus status;

    @Indexed(background = true)
    private SalesTeam team;

    private Long dateOfLeaving;

    private String leadsquaredId;

    public static class Constants {
        public static final String TEAM = "team";
        public static final String _ID = "_id";
        public static final String NAME = "name";
        public static final String STATUS = "status";
        public static final String EMAIL = "email";
        public static final String DESIGNATION = "designation";
        public static final String DESIGNATION_TYPE = "designationType";
        public static final String ENTITY_STATE = "entityState";
        public static final String CREATION_TIME = "creationTime";
        public static final String LAST_UPDATED = "lastUpdated";
        public static final String CENTRE_ID = "centreId";
        public static final String TL_ID = "tlId";

        public static final String DATE_OF_JOINING = "dateOfJoining";
        public static final String DATE_OF_LEAVING = "dateOfLeaving";

        public static final String IS_RM_ID = "isRMId";
        public static final String IS_FM_ID = "isFMId";
        public static final String EMPLOYEE_ID = "employeeId";
        public static final String LS_ID = "leadsquaredId";
    }

}
