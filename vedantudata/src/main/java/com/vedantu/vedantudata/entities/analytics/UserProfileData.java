package com.vedantu.vedantudata.entities.analytics;

import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;

public class UserProfileData extends AbstractMongoStringIdEntity {
	private String userId;
	private String email;
	private String role;
	private String active;
	private String isEmailVerified;
	private String phoneCode;
	private String contactNumber;
	private String firstName;
	private String lastName;
	private String fullName;
	private String city;
	private String state;
	private String country;
	private String parentRegistration;
	private String grade;
	private String school;
	private String parentContactNumbers;
	private String parentEmails;
	private String lastSessionTableUserId;
	private String lastSessionTeacherId;
	private String lastSessionSubscriptionId;
	private String lastSessionId;
	private Long lastSessionTime;
	private String lastSessionSubject;
	private String lastSessionTeacherName;
	private String lastSessionTeacherEmail;
	private String lastSessionTeacherContactNumber;
	private String firstSessionTableUserId;
	private String firstSessionTeacherId;
	private String firstSessionSubscriptionId;
	private String firstSessionId;
	private Long firstSessionTime;
	private String firstSessionSubject;
	private String firstSessionTeacherName;
	private String firstSessionTeacherEmail;
	private String firstSessionTeacherContactNumber;
	private Long lastPaymentTableAmount;
	private String lastPaymentTableUserId;
	private Long lastPaymentTime;
	private Long firstPaymentTableAmount;
	private String firstPaymentTableUserId;
	private Long firstPaymentTime;
	private Long totalPayment;
	private String totalPaymentTableUserId;
	private String totalOTOMillisUserId;
	private Long totalBillingPeriod;
	private String totalOTOMillisInAcademicyearUserId;
	private String totalBillingPeriodInAcademicYr;
	private String totalActiveSubscriptionHoursUserId;
	private Long totalConsumedHours;
	private String totalLockedHours;
	private String totalRemainingHours;
	private String teacherSubjectCombosUserId;
	private String teacherSubjects;

	public UserProfileData() {
		super();
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public String getActive() {
		return active;
	}

	public void setActive(String active) {
		this.active = active;
	}

	public String getIsEmailVerified() {
		return isEmailVerified;
	}

	public void setIsEmailVerified(String isEmailVerified) {
		this.isEmailVerified = isEmailVerified;
	}

	public String getPhoneCode() {
		return phoneCode;
	}

	public void setPhoneCode(String phoneCode) {
		this.phoneCode = phoneCode;
	}

	public String getContactNumber() {
		return contactNumber;
	}

	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getParentRegistration() {
		return parentRegistration;
	}

	public void setParentRegistration(String parentRegistration) {
		this.parentRegistration = parentRegistration;
	}

	public String getGrade() {
		return grade;
	}

	public void setGrade(String grade) {
		this.grade = grade;
	}

	public String getSchool() {
		return school;
	}

	public void setSchool(String school) {
		this.school = school;
	}

	public String getParentContactNumbers() {
		return parentContactNumbers;
	}

	public void setParentContactNumbers(String parentContactNumbers) {
		this.parentContactNumbers = parentContactNumbers;
	}

	public String getParentEmails() {
		return parentEmails;
	}

	public void setParentEmails(String parentEmails) {
		this.parentEmails = parentEmails;
	}

	public String getLastSessionTableUserId() {
		return lastSessionTableUserId;
	}

	public void setLastSessionTableUserId(String lastSessionTableUserId) {
		this.lastSessionTableUserId = lastSessionTableUserId;
	}

	public String getLastSessionTeacherId() {
		return lastSessionTeacherId;
	}

	public void setLastSessionTeacherId(String lastSessionTeacherId) {
		this.lastSessionTeacherId = lastSessionTeacherId;
	}

	public String getLastSessionSubscriptionId() {
		return lastSessionSubscriptionId;
	}

	public void setLastSessionSubscriptionId(String lastSessionSubscriptionId) {
		this.lastSessionSubscriptionId = lastSessionSubscriptionId;
	}

	public String getLastSessionId() {
		return lastSessionId;
	}

	public void setLastSessionId(String lastSessionId) {
		this.lastSessionId = lastSessionId;
	}

	public Long getLastSessionTime() {
		return lastSessionTime;
	}

	public void setLastSessionTime(Long lastSessionTime) {
		this.lastSessionTime = lastSessionTime;
	}

	public String getLastSessionSubject() {
		return lastSessionSubject;
	}

	public void setLastSessionSubject(String lastSessionSubject) {
		this.lastSessionSubject = lastSessionSubject;
	}

	public String getLastSessionTeacherName() {
		return lastSessionTeacherName;
	}

	public void setLastSessionTeacherName(String lastSessionTeacherName) {
		this.lastSessionTeacherName = lastSessionTeacherName;
	}

	public String getLastSessionTeacherEmail() {
		return lastSessionTeacherEmail;
	}

	public void setLastSessionTeacherEmail(String lastSessionTeacherEmail) {
		this.lastSessionTeacherEmail = lastSessionTeacherEmail;
	}

	public String getLastSessionTeacherContactNumber() {
		return lastSessionTeacherContactNumber;
	}

	public void setLastSessionTeacherContactNumber(String lastSessionTeacherContactNumber) {
		this.lastSessionTeacherContactNumber = lastSessionTeacherContactNumber;
	}

	public String getFirstSessionTableUserId() {
		return firstSessionTableUserId;
	}

	public void setFirstSessionTableUserId(String firstSessionTableUserId) {
		this.firstSessionTableUserId = firstSessionTableUserId;
	}

	public String getFirstSessionTeacherId() {
		return firstSessionTeacherId;
	}

	public void setFirstSessionTeacherId(String firstSessionTeacherId) {
		this.firstSessionTeacherId = firstSessionTeacherId;
	}

	public String getFirstSessionSubscriptionId() {
		return firstSessionSubscriptionId;
	}

	public void setFirstSessionSubscriptionId(String firstSessionSubscriptionId) {
		this.firstSessionSubscriptionId = firstSessionSubscriptionId;
	}

	public String getFirstSessionId() {
		return firstSessionId;
	}

	public void setFirstSessionId(String firstSessionId) {
		this.firstSessionId = firstSessionId;
	}

	public Long getFirstSessionTime() {
		return firstSessionTime;
	}

	public void setFirstSessionTime(Long firstSessionTime) {
		this.firstSessionTime = firstSessionTime;
	}

	public String getFirstSessionSubject() {
		return firstSessionSubject;
	}

	public void setFirstSessionSubject(String firstSessionSubject) {
		this.firstSessionSubject = firstSessionSubject;
	}

	public String getFirstSessionTeacherName() {
		return firstSessionTeacherName;
	}

	public void setFirstSessionTeacherName(String firstSessionTeacherName) {
		this.firstSessionTeacherName = firstSessionTeacherName;
	}

	public String getFirstSessionTeacherEmail() {
		return firstSessionTeacherEmail;
	}

	public void setFirstSessionTeacherEmail(String firstSessionTeacherEmail) {
		this.firstSessionTeacherEmail = firstSessionTeacherEmail;
	}

	public String getFirstSessionTeacherContactNumber() {
		return firstSessionTeacherContactNumber;
	}

	public void setFirstSessionTeacherContactNumber(String firstSessionTeacherContactNumber) {
		this.firstSessionTeacherContactNumber = firstSessionTeacherContactNumber;
	}

	public Long getLastPaymentTableAmount() {
		return lastPaymentTableAmount;
	}

	public void setLastPaymentTableAmount(Long lastPaymentTableAmount) {
		this.lastPaymentTableAmount = lastPaymentTableAmount;
	}

	public String getLastPaymentTableUserId() {
		return lastPaymentTableUserId;
	}

	public void setLastPaymentTableUserId(String lastPaymentTableUserId) {
		this.lastPaymentTableUserId = lastPaymentTableUserId;
	}

	public Long getLastPaymentTime() {
		return lastPaymentTime;
	}

	public void setLastPaymentTime(Long lastPaymentTime) {
		this.lastPaymentTime = lastPaymentTime;
	}

	public Long getFirstPaymentTableAmount() {
		return firstPaymentTableAmount;
	}

	public void setFirstPaymentTableAmount(Long firstPaymentTableAmount) {
		this.firstPaymentTableAmount = firstPaymentTableAmount;
	}

	public String getFirstPaymentTableUserId() {
		return firstPaymentTableUserId;
	}

	public void setFirstPaymentTableUserId(String firstPaymentTableUserId) {
		this.firstPaymentTableUserId = firstPaymentTableUserId;
	}

	public Long getFirstPaymentTime() {
		return firstPaymentTime;
	}

	public void setFirstPaymentTime(Long firstPaymentTime) {
		this.firstPaymentTime = firstPaymentTime;
	}

	public Long getTotalPayment() {
		return totalPayment;
	}

	public void setTotalPayment(Long totalPayment) {
		this.totalPayment = totalPayment;
	}

	public String getTotalPaymentTableUserId() {
		return totalPaymentTableUserId;
	}

	public void setTotalPaymentTableUserId(String totalPaymentTableUserId) {
		this.totalPaymentTableUserId = totalPaymentTableUserId;
	}

	public String getTotalOTOMillisUserId() {
		return totalOTOMillisUserId;
	}

	public void setTotalOTOMillisUserId(String totalOTOMillisUserId) {
		this.totalOTOMillisUserId = totalOTOMillisUserId;
	}

	public Long getTotalBillingPeriod() {
		return totalBillingPeriod;
	}

	public void setTotalBillingPeriod(Long totalBillingPeriod) {
		this.totalBillingPeriod = totalBillingPeriod;
	}

	public String getTotalOTOMillisInAcademicyearUserId() {
		return totalOTOMillisInAcademicyearUserId;
	}

	public void setTotalOTOMillisInAcademicyearUserId(String totalOTOMillisInAcademicyearUserId) {
		this.totalOTOMillisInAcademicyearUserId = totalOTOMillisInAcademicyearUserId;
	}

	public String getTotalBillingPeriodInAcademicYr() {
		return totalBillingPeriodInAcademicYr;
	}

	public void setTotalBillingPeriodInAcademicYr(String totalBillingPeriodInAcademicYr) {
		this.totalBillingPeriodInAcademicYr = totalBillingPeriodInAcademicYr;
	}

	public String getTotalActiveSubscriptionHoursUserId() {
		return totalActiveSubscriptionHoursUserId;
	}

	public void setTotalActiveSubscriptionHoursUserId(String totalActiveSubscriptionHoursUserId) {
		this.totalActiveSubscriptionHoursUserId = totalActiveSubscriptionHoursUserId;
	}

	public Long getTotalConsumedHours() {
		return totalConsumedHours;
	}

	public void setTotalConsumedHours(Long totalConsumedHours) {
		this.totalConsumedHours = totalConsumedHours;
	}

	public String getTotalLockedHours() {
		return totalLockedHours;
	}

	public void setTotalLockedHours(String totalLockedHours) {
		this.totalLockedHours = totalLockedHours;
	}

	public String getTotalRemainingHours() {
		return totalRemainingHours;
	}

	public void setTotalRemainingHours(String totalRemainingHours) {
		this.totalRemainingHours = totalRemainingHours;
	}

	public String getTeacherSubjectCombosUserId() {
		return teacherSubjectCombosUserId;
	}

	public void setTeacherSubjectCombosUserId(String teacherSubjectCombosUserId) {
		this.teacherSubjectCombosUserId = teacherSubjectCombosUserId;
	}

	public String getTeacherSubjects() {
		return teacherSubjects;
	}

	public void setTeacherSubjects(String teacherSubjects) {
		this.teacherSubjects = teacherSubjects;
	}

	@Override
	public String toString() {
		return "UserProfileData [userId=" + userId + ", email=" + email + ", role=" + role + ", active=" + active
				+ ", isEmailVerified=" + isEmailVerified + ", phoneCode=" + phoneCode + ", contactNumber="
				+ contactNumber + ", firstName=" + firstName + ", lastName=" + lastName + ", fullName=" + fullName
				+ ", city=" + city + ", state=" + state + ", country=" + country + ", parentRegistration="
				+ parentRegistration + ", grade=" + grade + ", school=" + school + ", parentContactNumbers="
				+ parentContactNumbers + ", parentEmails=" + parentEmails + ", lastSessionTableUserId="
				+ lastSessionTableUserId + ", lastSessionTeacherId=" + lastSessionTeacherId
				+ ", lastSessionSubscriptionId=" + lastSessionSubscriptionId + ", lastSessionId=" + lastSessionId
				+ ", lastSessionTime=" + lastSessionTime + ", lastSessionSubject=" + lastSessionSubject
				+ ", lastSessionTeacherName=" + lastSessionTeacherName + ", lastSessionTeacherEmail="
				+ lastSessionTeacherEmail + ", lastSessionTeacherContactNumber=" + lastSessionTeacherContactNumber
				+ ", firstSessionTableUserId=" + firstSessionTableUserId + ", firstSessionTeacherId="
				+ firstSessionTeacherId + ", firstSessionSubscriptionId=" + firstSessionSubscriptionId
				+ ", firstSessionId=" + firstSessionId + ", firstSessionTime=" + firstSessionTime
				+ ", firstSessionSubject=" + firstSessionSubject + ", firstSessionTeacherName="
				+ firstSessionTeacherName + ", firstSessionTeacherEmail=" + firstSessionTeacherEmail
				+ ", firstSessionTeacherContactNumber=" + firstSessionTeacherContactNumber + ", lastPaymentTableAmount="
				+ lastPaymentTableAmount + ", lastPaymentTableUserId=" + lastPaymentTableUserId + ", lastPaymentTime="
				+ lastPaymentTime + ", firstPaymentTableAmount=" + firstPaymentTableAmount
				+ ", firstPaymentTableUserId=" + firstPaymentTableUserId + ", firstPaymentTime=" + firstPaymentTime
				+ ", totalPayment=" + totalPayment + ", totalPaymentTableUserId=" + totalPaymentTableUserId
				+ ", totalOTOMillisUserId=" + totalOTOMillisUserId + ", totalBillingPeriod=" + totalBillingPeriod
				+ ", totalOTOMillisInAcademicyearUserId=" + totalOTOMillisInAcademicyearUserId
				+ ", totalBillingPeriodInAcademicYr=" + totalBillingPeriodInAcademicYr
				+ ", totalActiveSubscriptionHoursUserId=" + totalActiveSubscriptionHoursUserId + ", totalConsumedHours="
				+ totalConsumedHours + ", totalLockedHours=" + totalLockedHours + ", totalRemainingHours="
				+ totalRemainingHours + ", teacherSubjectCombosUserId=" + teacherSubjectCombosUserId
				+ ", teacherSubjects=" + teacherSubjects + ", toString()=" + super.toString() + "]";
	}
}
