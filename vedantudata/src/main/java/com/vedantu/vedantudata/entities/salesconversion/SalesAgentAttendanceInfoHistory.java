package com.vedantu.vedantudata.entities.salesconversion;

import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import com.vedantu.vedantudata.enums.salesconversion.AgentAttendanceStatus;
import lombok.Data;
import lombok.ToString;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@ToString
@Document(collection = "SalesAgentAttendanceInfoHistory")
public class SalesAgentAttendanceInfoHistory extends AbstractMongoStringIdEntity {

    @Indexed(background = true)
    private String agentId;

    @Indexed(background = true)
    private AgentAttendanceStatus attendanceStatus;

    @Indexed(background = true)
    private Long date;
}
