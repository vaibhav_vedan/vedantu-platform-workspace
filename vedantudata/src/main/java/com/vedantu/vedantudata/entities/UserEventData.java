package com.vedantu.vedantudata.entities;

import com.vedantu.util.dbentities.mongo.AbstractMongoEntity;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import com.vedantu.vedantudata.enums.UserEventName;
import com.vedantu.vedantudata.enums.UserEventType;

public class UserEventData extends AbstractMongoStringIdEntity {
	private UserEventName userEventName;
	private UserEventType userEventType;
	private String userEventStatus;
	private String referenceId;
	private String eventNote;
	private long referenceValue;
	private String referenceAgentId;
	private Long eventTime;

	public UserEventData() {
		super();
	}

	public UserEventName getUserEventName() {
		return userEventName;
	}

	public void setUserEventName(UserEventName userEventName) {
		this.userEventName = userEventName;
	}

	public UserEventType getUserEventType() {
		return userEventType;
	}

	public void setUserEventType(UserEventType userEventType) {
		this.userEventType = userEventType;
	}

	public String getReferenceId() {
		return referenceId;
	}

	public void setReferenceId(String referenceId) {
		this.referenceId = referenceId;
	}

	public String getEventNote() {
		return eventNote;
	}

	public void setEventNote(String eventNote) {
		this.eventNote = eventNote;
	}

	public long getReferenceValue() {
		return referenceValue;
	}

	public void setReferenceValue(long referenceValue) {
		this.referenceValue = referenceValue;
	}

	public String getReferenceAgentId() {
		return referenceAgentId;
	}

	public void setReferenceAgentId(String referenceAgentId) {
		this.referenceAgentId = referenceAgentId;
	}

	public String getUserEventStatus() {
		return userEventStatus;
	}

	public void setUserEventStatus(String userEventStatus) {
		this.userEventStatus = userEventStatus;
	}

	public Long getEventTime() {
		return eventTime;
	}

	public void setEventTime(Long eventTime) {
		this.eventTime = eventTime;
	}

	public static class Constants extends AbstractMongoEntity.Constants {
		public static final String USER_EVENT_TYPE = "userEventType";
		public static final String EVENT_TIME = "eventTime";
		public static final String REFERENCE_ID = "referenceId";
		public static final String REFERENCE_AGENT_ID = "referenceAgentId";
	}

	@Override
	public String toString() {
		return "UserEventData [userEventName=" + userEventName + ", userEventType=" + userEventType
				+ ", userEventStatus=" + userEventStatus + ", referenceId=" + referenceId + ", eventNote=" + eventNote
				+ ", referenceValue=" + referenceValue + ", referenceAgentId=" + referenceAgentId + ", eventTime="
				+ eventTime + ", toString()=" + super.toString() + "]";
	}
}
