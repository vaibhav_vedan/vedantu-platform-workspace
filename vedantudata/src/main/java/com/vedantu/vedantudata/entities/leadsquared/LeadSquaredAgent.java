package com.vedantu.vedantudata.entities.leadsquared;

import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.persistence.Entity;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
@Document(collection = "LeadSquaredAgent")
@CompoundIndex(name = "emailaddress_isInternittentScreenAllowed", def = "{'emailaddress':1, 'isInternittentScreenAllowed' : 1}", background = true)
public class LeadSquaredAgent extends AbstractMongoStringIdEntity {

    @Override
    public String toString() {
        return "LeadSquaredAgent{" +
                "isemailsender=" + isemailsender +
                ", usertype=" + usertype +
                ", isphonecallagent=" + isphonecallagent +
                ", modifyallleadsofgroup=" + modifyallleadsofgroup +
                ", viewallleadsofgroup=" + viewallleadsofgroup +
                ", isagencyuser=" + isagencyuser +
                ", isadministrator=" + isadministrator +
                ", isbillinguser=" + isbillinguser +
                ", ischeckedin=" + ischeckedin +
                ", ischeckinenabled=" + ischeckinenabled +
                ", autouserid=" + autouserid +
                ", role='" + role + '\'' +
                ", modifiedby='" + modifiedby + '\'' +
                ", createdby='" + createdby + '\'' +
                ", phoneothers='" + phoneothers + '\'' +
                ", phonemobile='" + phonemobile + '\'' +
                ", phonemain='" + phonemain + '\'' +
                ", associatedphonenumbers='" + associatedphonenumbers + '\'' +
                ", signature_text='" + signature_text + '\'' +
                ", signature_html='" + signature_html + '\'' +
                ", dateformat='" + dateformat + '\'' +
                ", timezone='" + timezone + '\'' +
                ", sessionid='" + sessionid + '\'' +
                ", tmp_forgotpassword='" + tmp_forgotpassword + '\'' +
                ", authtoken='" + authtoken + '\'' +
                ", password='" + password + '\'' +
                ", emailaddress='" + emailaddress + '\'' +
                ", lastname='" + lastname + '\'' +
                ", middlename='" + middlename + '\'' +
                ", firstname='" + firstname + '\'' +
                ", userid='" + userid + '\'' +
                ", lastcheckedon='" + lastcheckedon + '\'' +
                ", modifiedon='" + modifiedon + '\'' +
                ", createdon='" + createdon + '\'' +
                ", deletionstatuscode=" + deletionstatuscode +
                ", statusreason=" + statusreason +
                ", statuscode=" + statuscode +
                ", isdefaultowner=" + isdefaultowner +
                ", operator='" + operator + '\'' +
                ", did='" + did + '\'' +
                ", empID='" + empID + '\'' +
                ", phonename='" + phonename + '\'' +
                ", agentID='" + agentID + '\'' +
                ", dialerprocess='" + dialerprocess + '\'' +
                ", joiningdate='" + joiningdate + '\'' +
                ", dol='" + dol + '\'' +
                ", teacheriD='" + teacheriD + '\'' +
                ", mx_custom_10='" + mx_custom_10 + '\'' +
                ", checkincheckouthistoryid='" + checkincheckouthistoryid + '\'' +
                ", lastcheckedipaddress='" + lastcheckedipaddress + '\'' +
                ", teamid='" + teamid + '\'' +
                ", holidaycalendarid='" + holidaycalendarid + '\'' +
                ", workdaytemplateid='" + workdaytemplateid + '\'' +
                ", telephonyagentid='" + telephonyagentid + '\'' +
                ", zipcode='" + zipcode + '\'' +
                ", country='" + country + '\'' +
                ", state='" + state + '\'' +
                ", city='" + city + '\'' +
                ", address='" + address + '\'' +
                ", officelocationname='" + officelocationname + '\'' +
                ", availabilitystatus='" + availabilitystatus + '\'' +
                ", skills='" + skills + '\'' +
                ", salesregions='" + salesregions + '\'' +
                ", department='" + department + '\'' +
                ", team='" + team + '\'' +
                ", designation='" + designation + '\'' +
                ", photourl='" + photourl + '\'' +
                ", groups='" + groups + '\'' +
                ", manageruserid='" + manageruserid + '\'' +
                ", TL='" + TL + '\'' +
                ", CH='" + CH + '\'' +
                ", RM='" + RM + '\'' +
                ", ZM='" + ZM + '\'' +
                '}';
    }

    private Integer isemailsender;
    private Integer usertype;
    private Integer isphonecallagent;
    private Integer modifyallleadsofgroup;
    private Integer viewallleadsofgroup;
    private Integer isagencyuser;
    private Integer isadministrator;
    private Integer isbillinguser;
    private Integer ischeckedin;
    private Integer ischeckinenabled;
    private Integer autouserid;
    private String role;
    private String modifiedby;
    private String createdby;
    private String phoneothers;
    private String phonemobile;
    private String phonemain;
    private String associatedphonenumbers;
    private String signature_text;
    private String signature_html;
    private String dateformat;
    private String timezone;
    private String sessionid;
    private String tmp_forgotpassword;
    private String authtoken;
    private String password;
    @Indexed(background = true)
    private String emailaddress;
    private String lastname;
    private String middlename;
    private String firstname;
    @Indexed(background = true)
    private String userid;
    private String lastcheckedon;
    private String modifiedon;
    private String createdon;
    private Integer deletionstatuscode;
    private Integer statusreason;
    private Integer statuscode;
    private Integer isdefaultowner;
    private String operator;
    private String did;
    private String empID;
    private String phonename;
    private String agentID;
    private String dialerprocess;
    private String joiningdate;
    private String dol;
    private String teacheriD;
    private String mx_custom_10;
    private String checkincheckouthistoryid;
    private String lastcheckedipaddress;
    private String teamid;
    private String holidaycalendarid;
    private String workdaytemplateid;
    private String telephonyagentid;
    private String zipcode;
    private String country;
    private String state;
    private String city;
    private String address;
    private String officelocationname;
    private String availabilitystatus;
    private String skills;
    private String salesregions;
    private String department;
    private String team;
    private String designation;
    private String photourl;
    private String groups;
    private String manageruserid;
    private String TL;
    private String CH;
    private String RM;
    private String ZM;
    private Boolean isIntermittentScreenAllowed;
    private String cohort;
    private String STL;

    public static class Constants extends AbstractMongoStringIdEntity.Constants {
        public static final String TL = "TL";
        public static final String CH = "CH";
        public static final String RM = "RM";
        public static final String ZM = "ZM";
        public static final String EMAIL_ADDRESS = "emailaddress";
        public static final String USER_ID = "userid";
        public static final String IS_INTERMITTENT_SCREEN_ALLOWED = "isIntermittentScreenAllowed";
    }
}
