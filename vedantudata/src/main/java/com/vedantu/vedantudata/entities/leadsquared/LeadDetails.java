package com.vedantu.vedantudata.entities.leadsquared;

import java.util.HashSet;
import java.util.Set;

import com.google.gson.annotations.SerializedName;
import com.vedantu.util.StringUtils;
import com.vedantu.util.dbentities.mongo.AbstractMongoEntity;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import com.vedantu.vedantudata.entities.analytics.UserProfileData;
import com.vedantu.vedantudata.utils.CommonUtils;

public class LeadDetails extends AbstractMongoStringIdEntity {

    @SerializedName("Id")
    private String leadId;

    @SerializedName("ConversionReferrerURL")
    private String conversionReferrerURL;

    @SerializedName("SourceReferrerURL")
    private String sourceReferrerURL; // android-app://com.google.android.gm

    @SerializedName("SourceIPAddress")
    private String sourceIPAddress; // 49.207.141.246

    @SerializedName("ProspectAutoId")
    private String prospectAutoId; // 653564

    @SerializedName("ProspectID")
    private String prospectID; // 337bc48a-c549-40b5-8604-c6e70ebb44d0

    @SerializedName("ProspectActivityId_Max")
    private String prospectActivityId_Max; // 40d9e5f5-086e-11e7-b5f1-22000aa8e760

    @SerializedName("ProspectActivityId_Min")
    private String prospectActivityId_Min; // 428a4b84-411f-4ffe-8ef2-e1d0755a2747

    @SerializedName("StatusCode")
    private String statusCode; // 0

    @SerializedName("StatusReason")
    private String statusReason; // 0

    @SerializedName("DeletionStatusCode")
    private String deletionStatusCode; // 0

    @SerializedName("RelatedProspectId")
    private String relatedProspectId; // null

    @SerializedName("SourceReferrer")
    private String sourceReferrer; // null

    @SerializedName("NotableEvent")
    private String notableEvent; // Page Visited on Website

    @SerializedName("NotableEventdate")
    private String notableEventdate; // 2017-03-14 04:25:28.000

    @SerializedName("LastVisitDate")
    private String lastVisitDate; // ": "2017-03-14 04:25:28.000",

    @SerializedName("RelatedLandingPageId")
    private String relatedLandingPageId; // ": null,

    @SerializedName("FirstLandingPageSubmissionId")
    private String firstLandingPageSubmissionId; // ": null,

    @SerializedName("FirstLandingPageSubmissionDate")
    private String firstLandingPageSubmissionDate; // ": null,

    @SerializedName("ProspectActivityName_Max")
    private String prospectActivityName_Max; // ": "Page Visited on Website",

    @SerializedName("ProspectActivityDate_Max")
    private String prospectActivityDate_Max;// ": "2017-03-14 04:25:28.000",

    @SerializedName("FirstName")
    private String firstName; // ": "waseem",

    @SerializedName("LastName")
    private String lastName; // ": "khan",

    @SerializedName("EmailAddress")
    private String emailAddress; // ": "waseemgibroan777@gmail.com",

    @SerializedName("Phone")
    private String phone; // ": null,

    @SerializedName("Company")
    private String company; // ": null,

    @SerializedName("Website")
    private String website; // ": null,

    @SerializedName("DoNotEmail")
    private String doNotEmail; // ": "0",

    @SerializedName("DoNotCall")
    private String doNotCall; // ": "0",

    @SerializedName("Source")
    private String source; // ": "Zopim Chat",

    @SerializedName("Mobile")
    private String mobile; // ": null,

    @SerializedName("SourceCampaign")
    private String sourceCampaign; // ": null,

    @SerializedName("SourceMedium")
    private String sourceMedium; // ": null,

    @SerializedName("JobTitle")
    private String jobTitle; // ": null,

    @SerializedName("SourceContent")
    private String sourceContent; // ": null,

    @SerializedName("Notes")
    private String notes; // ": null,

    @SerializedName("Grade")
    private String grade; // ": null,

    @SerializedName("QualityScore01")
    private String qualityScore01; // ": null,

    @SerializedName("Score")
    private String score; // ": "18",

    @SerializedName("EngagementScore")
    private String engagementScore; // ": "18",

    @SerializedName("Revenue")
    private Long revenue; // ": null,

    @SerializedName("ProspectStage")
    private String prospectStage; // ": "PS-Fresh Sign up",

    @SerializedName("OwnerId")
    private String ownerId; // ": "4d2cf97c-31d0-11e5-981b-22000a9700b4",

    @SerializedName("OwnerIdName")
    private String ownerIdName; // ": "Pooja B",

    @SerializedName("CreatedBy")
    private String leadCreatedBy; // ": "79c86b16-1836-11e5-981b-22000a9700b4",

    @SerializedName("CreatedByName")
    private String leadCreatedByName; // ": "System",

    @SerializedName("CreatedOn")
    private String leadCreatedOn; // ": "2017-03-14 04:22:07.000",

    @SerializedName("LeadConversionDate")
    private String leadConversionDate; // ": null,

    @SerializedName("ModifiedBy")
    private String modifiedBy; // ": "79c86b16-1836-11e5-981b-22000a9700b4",

    @SerializedName("ModifiedByName")
    private String modifiedByName; // ": "System",

    @SerializedName("ModifiedOn")
    private String modifiedOn; // ": "2017-03-14 04:22:10.000",

    @SerializedName("TimeZone")
    private String timeZone; // ": null,

    @SerializedName("TotalVisits")
    private String totalVisits; // ": "0",

    @SerializedName("PageViewsPerVisit")
    private String pageViewsPerVisit; // ": "0",

    @SerializedName("AvgTimePerVisit")
    private String avgTimePerVisit; // ": "0",

    @SerializedName("Origin")
    private String origin; // ": "API",

    @SerializedName("FacebookId")
    private String facebookId; // ": null,

    @SerializedName("LinkedInId")
    private String linkedInId; // ": null,

    @SerializedName("MailingPreferences")
    private String mailingPreferences; // ": null,

    @SerializedName("GooglePlusId")
    private String googlePlusId; // ": null,

    @SerializedName("LeadAge")
    private String leadAge; // ": "0",

    @SerializedName("IsStarredLead")
    private String starredLead; // ": "false",

    @SerializedName("IsTaggedLead")
    private String taggedLead; // ": "0",

    @SerializedName("CanUpdate")
    private String canUpdate; // ": "true"

    private String mx_Street1; // ": null,
    private String mx_Street2; // ": null,
    private String mx_City; // ": null,
    private String mx_State; // ": null,
    private String mx_Country; // ": null,
    private String mx_Zip; // ": null,
    private String mx_FaceBookURL; // ": null,
    private String mx_LinkedInURL; // ": null,
    private String mx_TwitterURL; // ": null,
    private String mx_Student_Class; // ": null,
    private String mx_Source_Channel; // ": null,
    private String mx_Class; // ": null,
    private String mx_StudentGrade; // ": null,
    private String mx_Board; // ": null,
    private String mx_School; // ": null,
    private String mx_Mobile; // ": null,
    private String mx_PhoneNumberList; // ": null,
    private String mx_CreationTime; // ": null,
    private String mx_ModifiedTime; // ": null,
    private String mx_Gender; // ": null,
    private String mx_Role; // ": null,
    private String mx_LanguageList; // ": null,
    private String mx_IsEmailVerifed; // ": null,
    private String mx_IsContactNumberVerified; // ": null,
    private String mx_IsContactNumberWhiteListed; // ": null,
    private String mx_IsParentRegistration; // ": null,
    private String mx_ParentContactNumber; // ": null,
    private String mx_ParentContactNumberList; // ": null,
    private String mx_ParentEmailList; // ": null,
    private String mx_SourceCampaign; // ": null,
    private String mx_SourceLead; // ": null,
    private String mx_SourceMedium; // ": null,
    private String mx_IsContactNumberDND; // ": null,
    private String mx_User_Id; // ": null,
    private String mx_activeNumber; // ": null,
    private String mx_activeNumberVerified; // ": null,
    private String mx_activeNumberDND; // ": null,
    private String mx_Source_term; // ": null,
    private String mx_Remark; // ": null,
    private String mx_Query_Type; // ": null,
    private String mx_TOS_To_be_booked_on; // ": null,
    private String mx_Future_TOS_Booking_Date; // ": null,
    private String mx_TOS_Done_On; // ": null,
    private String mx_Source_Creation_Date; // ": null,
    private String mx_Reason_For_Not_Interested; // ": null,
    private String mx_Sign_up_URL; // ": null,
    private String mx_Sign_up_feature; // ": null,
    private String mx_featureHide; // ": null,
    private String mx_Session_Churn_NIT; // ": null,
    private String mx_Recharge_Churn_NIT; // ": null,
    private String mx_TOS_to_NS_Detail_Reason; // ": null,
    private String mx_preSales; // ": null,
    private String mx_Subjects; // ": null,
    private String mx_Course_Type; // ": null,
    private String mx_Date_of_Birth; // ": null,
    private String mx_Father_name; // ": null,
    private String mx_Father_emailId; // ": null,
    private String mx_Mother_Name; // ": null,
    private String mx_Mother_emailId; // ": null,
    private String mx_Student_School_Name; // ": null,
    private String mx_Sibling_Name; // ": null,
    private String mx_Sibling_Date_of_birth; // ": null,
    private String mx_Sibling_Grade; // ": null,
    private String mx_Sibling_Board; // ": null,
    private String mx_Postal_address; // ": null,
    private String mx_Student_Name; // ": null,
    private String mx_Father_contact_Number_KYC; // ": null,
    private String mx_Mother_contact_number_KYC; // ": null,
    private String mx_temp_first_name; // ": null,
    private String mx_Request_call_back; // ": null,
    private String mx_ghfh; // ": null,
    private String mx_SusbcriptionRequestLead; // ": null,
    private String mx_Future_Academic_Scope; // ": null,
    private String mx_Parent_name;
    private String mx_Parent_emailId;
    private String mx_Parent_Contact_Number;
    private String mx_Vedantu_data_source;

    private String mx_User_active;
    private String mx_Contact_number_list; // Create the contact number list
    private String mx_First_Session_id;
    private String mx_First_Session_Time;
    private String mx_First_Payment_Amount;
    private String mx_First_session_teacher_name;
    private String mx_First_session_subject;
    private String mx_First_Payment_Time;
    private String firstSessionTeacherId;
    private String firstSessionSubscriptionId;
    private String firstSessionTeacherEmail;
    private String firstSessionTeacherContactNumber;

    private Long mx_Last_Payment_Amount;
    private String mx_Last_payment_time;
    private String mx_Last_session_id;
    private String mx_Last_session_subject;
    private String mx_Last_session_teacher_name;
    private String mx_Last_session_time;
    private String lastSessionTeacherId;
    private String lastSessionSubscriptionId;
    private String lastSessionTeacherEmail;
    private String lastSessionTeacherContactNumber;

    private Long mx_Total_billing_period;
    private Long mx_Total_consumed_hours;

    private String totalLockedHours;
    private String totalRemainingHours;
    private String mx_Teacher_subjects;
    private String mx_SAM_Allocation;
    private String mx_Sub_Stage;
    private String mx_Last_Assigned_Date;
    private String mx_VCLead;
    private String mx_User_Target;
    private String mx_LastActivityDate;
    private String mx_Assigned_Count;
    private String mx_Core_Noncore;
    private String mx_GPS_City;
    private String mx_GPS_lat;
    private String mx_GPS_Long;
    private String mx_Location_type;
    private String mx_Nearby_City;
    private String mx_CC_LT;

    public transient boolean success = false;

    public LeadDetails() {
        super();
    }

    public String getConversionReferrerURL() {
        return conversionReferrerURL;
    }

    public void setConversionReferrerURL(String conversionReferrerURL) {
        this.conversionReferrerURL = conversionReferrerURL;
    }

    public String getSourceReferrerURL() {
        return sourceReferrerURL;
    }

    public void setSourceReferrerURL(String sourceReferrerURL) {
        this.sourceReferrerURL = sourceReferrerURL;
    }

    public String getSourceIPAddress() {
        return sourceIPAddress;
    }

    public void setSourceIPAddress(String sourceIPAddress) {
        this.sourceIPAddress = sourceIPAddress;
    }

    public String getProspectAutoId() {
        return prospectAutoId;
    }

    public void setProspectAutoId(String prospectAutoId) {
        this.prospectAutoId = prospectAutoId;
    }

    public String getProspectID() {
        return prospectID;
    }

    public void setProspectID(String prospectID) {
        this.prospectID = prospectID;
    }

    public String getProspectActivityId_Max() {
        return prospectActivityId_Max;
    }

    public void setProspectActivityId_Max(String prospectActivityId_Max) {
        this.prospectActivityId_Max = prospectActivityId_Max;
    }

    public String getProspectActivityId_Min() {
        return prospectActivityId_Min;
    }

    public void setProspectActivityId_Min(String prospectActivityId_Min) {
        this.prospectActivityId_Min = prospectActivityId_Min;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getStatusReason() {
        return statusReason;
    }

    public void setStatusReason(String statusReason) {
        this.statusReason = statusReason;
    }

    public String getDeletionStatusCode() {
        return deletionStatusCode;
    }

    public void setDeletionStatusCode(String deletionStatusCode) {
        this.deletionStatusCode = deletionStatusCode;
    }

    public String getRelatedProspectId() {
        return relatedProspectId;
    }

    public void setRelatedProspectId(String relatedProspectId) {
        this.relatedProspectId = relatedProspectId;
    }

    public String getSourceReferrer() {
        return sourceReferrer;
    }

    public void setSourceReferrer(String sourceReferrer) {
        this.sourceReferrer = sourceReferrer;
    }

    public String getNotableEvent() {
        return notableEvent;
    }

    public void setNotableEvent(String notableEvent) {
        this.notableEvent = notableEvent;
    }

    public String getNotableEventdate() {
        return notableEventdate;
    }

    public void setNotableEventdate(String notableEventdate) {
        this.notableEventdate = notableEventdate;
    }

    public String getLastVisitDate() {
        return lastVisitDate;
    }

    public void setLastVisitDate(String lastVisitDate) {
        this.lastVisitDate = lastVisitDate;
    }

    public String getRelatedLandingPageId() {
        return relatedLandingPageId;
    }

    public void setRelatedLandingPageId(String relatedLandingPageId) {
        this.relatedLandingPageId = relatedLandingPageId;
    }

    public String getFirstLandingPageSubmissionId() {
        return firstLandingPageSubmissionId;
    }

    public void setFirstLandingPageSubmissionId(String firstLandingPageSubmissionId) {
        this.firstLandingPageSubmissionId = firstLandingPageSubmissionId;
    }

    public String getFirstLandingPageSubmissionDate() {
        return firstLandingPageSubmissionDate;
    }

    public void setFirstLandingPageSubmissionDate(String firstLandingPageSubmissionDate) {
        this.firstLandingPageSubmissionDate = firstLandingPageSubmissionDate;
    }

    public String getProspectActivityName_Max() {
        return prospectActivityName_Max;
    }

    public void setProspectActivityName_Max(String prospectActivityName_Max) {
        this.prospectActivityName_Max = prospectActivityName_Max;
    }

    public String getProspectActivityDate_Max() {
        return prospectActivityDate_Max;
    }

    public void setProspectActivityDate_Max(String prospectActivityDate_Max) {
        this.prospectActivityDate_Max = prospectActivityDate_Max;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getDoNotEmail() {
        return doNotEmail;
    }

    public void setDoNotEmail(String doNotEmail) {
        this.doNotEmail = doNotEmail;
    }

    public String getDoNotCall() {
        return doNotCall;
    }

    public void setDoNotCall(String doNotCall) {
        this.doNotCall = doNotCall;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getSourceCampaign() {
        return sourceCampaign;
    }

    public void setSourceCampaign(String sourceCampaign) {
        this.sourceCampaign = sourceCampaign;
    }

    public String getSourceMedium() {
        return sourceMedium;
    }

    public void setSourceMedium(String sourceMedium) {
        this.sourceMedium = sourceMedium;
    }

    public String getJobTitle() {
        return jobTitle;
    }

    public void setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
    }

    public String getSourceContent() {
        return sourceContent;
    }

    public void setSourceContent(String sourceContent) {
        this.sourceContent = sourceContent;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

    public String getQualityScore01() {
        return qualityScore01;
    }

    public void setQualityScore01(String qualityScore01) {
        this.qualityScore01 = qualityScore01;
    }

    public String getScore() {
        return score;
    }

    public void setScore(String score) {
        this.score = score;
    }

    public String getEngagementScore() {
        return engagementScore;
    }

    public void setEngagementScore(String engagementScore) {
        this.engagementScore = engagementScore;
    }

    public Long getRevenue() {
        return revenue;
    }

    public void setRevenue(Long revenue) {
        this.revenue = revenue;
    }

    public String getProspectStage() {
        return prospectStage;
    }

    public void setProspectStage(String prospectStage) {
        this.prospectStage = prospectStage;
    }

    public String getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(String ownerId) {
        this.ownerId = ownerId;
    }

    public String getOwnerIdName() {
        return ownerIdName;
    }

    public void setOwnerIdName(String ownerIdName) {
        this.ownerIdName = ownerIdName;
    }

    public String getLeadCreatedBy() {
        return leadCreatedBy;
    }

    public void setLeadCreatedBy(String leadCreatedBy) {
        this.leadCreatedBy = leadCreatedBy;
    }

    public String getLeadCreatedByName() {
        return leadCreatedByName;
    }

    public void setLeadCreatedByName(String leadCreatedByName) {
        this.leadCreatedByName = leadCreatedByName;
    }

    public String getLeadCreatedOn() {
        return leadCreatedOn;
    }

    public void setLeadCreatedOn(String leadCreatedOn) {
        this.leadCreatedOn = leadCreatedOn;
    }

    public String getLeadConversionDate() {
        return leadConversionDate;
    }

    public void setLeadConversionDate(String leadConversionDate) {
        this.leadConversionDate = leadConversionDate;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public String getModifiedByName() {
        return modifiedByName;
    }

    public void setModifiedByName(String modifiedByName) {
        this.modifiedByName = modifiedByName;
    }

    public String getModifiedOn() {
        return modifiedOn;
    }

    public void setModifiedOn(String modifiedOn) {
        this.modifiedOn = modifiedOn;
    }

    public String getTimeZone() {
        return timeZone;
    }

    public void setTimeZone(String timeZone) {
        this.timeZone = timeZone;
    }

    public String getTotalVisits() {
        return totalVisits;
    }

    public void setTotalVisits(String totalVisits) {
        this.totalVisits = totalVisits;
    }

    public String getPageViewsPerVisit() {
        return pageViewsPerVisit;
    }

    public void setPageViewsPerVisit(String pageViewsPerVisit) {
        this.pageViewsPerVisit = pageViewsPerVisit;
    }

    public String getAvgTimePerVisit() {
        return avgTimePerVisit;
    }

    public void setAvgTimePerVisit(String avgTimePerVisit) {
        this.avgTimePerVisit = avgTimePerVisit;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getFacebookId() {
        return facebookId;
    }

    public void setFacebookId(String facebookId) {
        this.facebookId = facebookId;
    }

    public String getLinkedInId() {
        return linkedInId;
    }

    public void setLinkedInId(String linkedInId) {
        this.linkedInId = linkedInId;
    }

    public String getMailingPreferences() {
        return mailingPreferences;
    }

    public void setMailingPreferences(String mailingPreferences) {
        this.mailingPreferences = mailingPreferences;
    }

    public String getGooglePlusId() {
        return googlePlusId;
    }

    public void setGooglePlusId(String googlePlusId) {
        this.googlePlusId = googlePlusId;
    }

    public String getLeadAge() {
        return leadAge;
    }

    public void setLeadAge(String leadAge) {
        this.leadAge = leadAge;
    }

    public String getStarredLead() {
        return starredLead;
    }

    public void setStarredLead(String starredLead) {
        this.starredLead = starredLead;
    }

    public String getTaggedLead() {
        return taggedLead;
    }

    public void setTaggedLead(String taggedLead) {
        this.taggedLead = taggedLead;
    }

    public String getCanUpdate() {
        return canUpdate;
    }

    public void setCanUpdate(String canUpdate) {
        this.canUpdate = canUpdate;
    }

    public String getMx_Street1() {
        return mx_Street1;
    }

    public void setMx_Street1(String mx_Street1) {
        this.mx_Street1 = mx_Street1;
    }

    public String getMx_Street2() {
        return mx_Street2;
    }

    public void setMx_Street2(String mx_Street2) {
        this.mx_Street2 = mx_Street2;
    }

    public String getMx_City() {
        return mx_City;
    }

    public void setMx_City(String mx_City) {
        this.mx_City = mx_City;
    }

    public String getMx_State() {
        return mx_State;
    }

    public void setMx_State(String mx_State) {
        this.mx_State = mx_State;
    }

    public String getMx_Country() {
        return mx_Country;
    }

    public void setMx_Country(String mx_Country) {
        this.mx_Country = mx_Country;
    }

    public String getMx_Zip() {
        return mx_Zip;
    }

    public void setMx_Zip(String mx_Zip) {
        this.mx_Zip = mx_Zip;
    }

    public String getMx_FaceBookURL() {
        return mx_FaceBookURL;
    }

    public void setMx_FaceBookURL(String mx_FaceBookURL) {
        this.mx_FaceBookURL = mx_FaceBookURL;
    }

    public String getMx_LinkedInURL() {
        return mx_LinkedInURL;
    }

    public void setMx_LinkedInURL(String mx_LinkedInURL) {
        this.mx_LinkedInURL = mx_LinkedInURL;
    }

    public String getMx_TwitterURL() {
        return mx_TwitterURL;
    }

    public void setMx_TwitterURL(String mx_TwitterURL) {
        this.mx_TwitterURL = mx_TwitterURL;
    }

    public String getMx_Student_Class() {
        return mx_Student_Class;
    }

    public void setMx_Student_Class(String mx_Student_Class) {
        this.mx_Student_Class = mx_Student_Class;
    }

    public String getMx_Source_Channel() {
        return mx_Source_Channel;
    }

    public void setMx_Source_Channel(String mx_Source_Channel) {
        this.mx_Source_Channel = mx_Source_Channel;
    }

    public String getMx_Class() {
        return mx_Class;
    }

    public void setMx_Class(String mx_Class) {
        this.mx_Class = mx_Class;
    }

    public String getMx_StudentGrade() {
        return mx_StudentGrade;
    }

    public void setMx_StudentGrade(String mx_StudentGrade) {
        this.mx_StudentGrade = mx_StudentGrade;
    }

    public String getMx_Board() {
        return mx_Board;
    }

    public void setMx_Board(String mx_Board) {
        this.mx_Board = mx_Board;
    }

    public String getMx_School() {
        return mx_School;
    }

    public void setMx_School(String mx_School) {
        this.mx_School = mx_School;
    }

    public String getMx_Mobile() {
        return mx_Mobile;
    }

    public void setMx_Mobile(String mx_Mobile) {
        this.mx_Mobile = mx_Mobile;
    }

    public String getMx_PhoneNumberList() {
        return mx_PhoneNumberList;
    }

    public void setMx_PhoneNumberList(String mx_PhoneNumberList) {
        this.mx_PhoneNumberList = mx_PhoneNumberList;
    }

    public String getMx_CreationTime() {
        return mx_CreationTime;
    }

    public void setMx_CreationTime(String mx_CreationTime) {
        this.mx_CreationTime = mx_CreationTime;
    }

    public String getMx_ModifiedTime() {
        return mx_ModifiedTime;
    }

    public void setMx_ModifiedTime(String mx_ModifiedTime) {
        this.mx_ModifiedTime = mx_ModifiedTime;
    }

    public String getMx_Gender() {
        return mx_Gender;
    }

    public void setMx_Gender(String mx_Gender) {
        this.mx_Gender = mx_Gender;
    }

    public String getMx_Role() {
        return mx_Role;
    }

    public void setMx_Role(String mx_Role) {
        this.mx_Role = mx_Role;
    }

    public String getMx_LanguageList() {
        return mx_LanguageList;
    }

    public void setMx_LanguageList(String mx_LanguageList) {
        this.mx_LanguageList = mx_LanguageList;
    }

    public String getMx_IsEmailVerifed() {
        return mx_IsEmailVerifed;
    }

    public void setMx_IsEmailVerifed(String mx_IsEmailVerifed) {
        this.mx_IsEmailVerifed = mx_IsEmailVerifed;
    }

    public String getMx_IsContactNumberVerified() {
        return mx_IsContactNumberVerified;
    }

    public void setMx_IsContactNumberVerified(String mx_IsContactNumberVerified) {
        this.mx_IsContactNumberVerified = mx_IsContactNumberVerified;
    }

    public String getMx_IsContactNumberWhiteListed() {
        return mx_IsContactNumberWhiteListed;
    }

    public void setMx_IsContactNumberWhiteListed(String mx_IsContactNumberWhiteListed) {
        this.mx_IsContactNumberWhiteListed = mx_IsContactNumberWhiteListed;
    }

    public String getMx_IsParentRegistration() {
        return mx_IsParentRegistration;
    }

    public void setMx_IsParentRegistration(String mx_IsParentRegistration) {
        this.mx_IsParentRegistration = mx_IsParentRegistration;
    }

    public String getMx_ParentContactNumber() {
        return mx_ParentContactNumber;
    }

    public void setMx_ParentContactNumber(String mx_ParentContactNumber) {
        this.mx_ParentContactNumber = mx_ParentContactNumber;
    }

    public String getMx_ParentContactNumberList() {
        return mx_ParentContactNumberList;
    }

    public void setMx_ParentContactNumberList(String mx_ParentContactNumberList) {
        this.mx_ParentContactNumberList = mx_ParentContactNumberList;
    }

    public String getMx_ParentEmailList() {
        return mx_ParentEmailList;
    }

    public void setMx_ParentEmailList(String mx_ParentEmailList) {
        this.mx_ParentEmailList = mx_ParentEmailList;
    }

    public String getMx_SourceCampaign() {
        return mx_SourceCampaign;
    }

    public void setMx_SourceCampaign(String mx_SourceCampaign) {
        this.mx_SourceCampaign = mx_SourceCampaign;
    }

    public String getMx_SourceLead() {
        return mx_SourceLead;
    }

    public void setMx_SourceLead(String mx_SourceLead) {
        this.mx_SourceLead = mx_SourceLead;
    }

    public String getMx_SourceMedium() {
        return mx_SourceMedium;
    }

    public void setMx_SourceMedium(String mx_SourceMedium) {
        this.mx_SourceMedium = mx_SourceMedium;
    }

    public String getMx_IsContactNumberDND() {
        return mx_IsContactNumberDND;
    }

    public void setMx_IsContactNumberDND(String mx_IsContactNumberDND) {
        this.mx_IsContactNumberDND = mx_IsContactNumberDND;
    }

    public String getMx_User_Id() {
        return mx_User_Id;
    }

    public void setMx_User_Id(String mx_User_Id) {
        this.mx_User_Id = mx_User_Id;
    }

    public String getMx_activeNumber() {
        return mx_activeNumber;
    }

    public void setMx_activeNumber(String mx_activeNumber) {
        this.mx_activeNumber = mx_activeNumber;
    }

    public String getMx_activeNumberVerified() {
        return mx_activeNumberVerified;
    }

    public void setMx_activeNumberVerified(String mx_activeNumberVerified) {
        this.mx_activeNumberVerified = mx_activeNumberVerified;
    }

    public String getMx_activeNumberDND() {
        return mx_activeNumberDND;
    }

    public void setMx_activeNumberDND(String mx_activeNumberDND) {
        this.mx_activeNumberDND = mx_activeNumberDND;
    }

    public String getMx_Source_term() {
        return mx_Source_term;
    }

    public void setMx_Source_term(String mx_Source_term) {
        this.mx_Source_term = mx_Source_term;
    }

    public String getMx_Remark() {
        return mx_Remark;
    }

    public void setMx_Remark(String mx_Remark) {
        this.mx_Remark = mx_Remark;
    }

    public String getMx_Query_Type() {
        return mx_Query_Type;
    }

    public void setMx_Query_Type(String mx_Query_Type) {
        this.mx_Query_Type = mx_Query_Type;
    }

    public String getMx_TOS_To_be_booked_on() {
        return mx_TOS_To_be_booked_on;
    }

    public void setMx_TOS_To_be_booked_on(String mx_TOS_To_be_booked_on) {
        this.mx_TOS_To_be_booked_on = mx_TOS_To_be_booked_on;
    }

    public String getMx_Future_TOS_Booking_Date() {
        return mx_Future_TOS_Booking_Date;
    }

    public void setMx_Future_TOS_Booking_Date(String mx_Future_TOS_Booking_Date) {
        this.mx_Future_TOS_Booking_Date = mx_Future_TOS_Booking_Date;
    }

    public String getMx_TOS_Done_On() {
        return mx_TOS_Done_On;
    }

    public void setMx_TOS_Done_On(String mx_TOS_Done_On) {
        this.mx_TOS_Done_On = mx_TOS_Done_On;
    }

    public String getMx_Source_Creation_Date() {
        return mx_Source_Creation_Date;
    }

    public void setMx_Source_Creation_Date(String mx_Source_Creation_Date) {
        this.mx_Source_Creation_Date = mx_Source_Creation_Date;
    }

    public String getMx_Reason_For_Not_Interested() {
        return mx_Reason_For_Not_Interested;
    }

    public void setMx_Reason_For_Not_Interested(String mx_Reason_For_Not_Interested) {
        this.mx_Reason_For_Not_Interested = mx_Reason_For_Not_Interested;
    }

    public String getMx_Sign_up_URL() {
        return mx_Sign_up_URL;
    }

    public void setMx_Sign_up_URL(String mx_Sign_up_URL) {
        this.mx_Sign_up_URL = mx_Sign_up_URL;
    }

    public String getMx_Sign_up_feature() {
        return mx_Sign_up_feature;
    }

    public void setMx_Sign_up_feature(String mx_Sign_up_feature) {
        this.mx_Sign_up_feature = mx_Sign_up_feature;
    }

    public String getMx_featureHide() {
        return mx_featureHide;
    }

    public void setMx_featureHide(String mx_featureHide) {
        this.mx_featureHide = mx_featureHide;
    }

    public String getMx_Session_Churn_NIT() {
        return mx_Session_Churn_NIT;
    }

    public void setMx_Session_Churn_NIT(String mx_Session_Churn_NIT) {
        this.mx_Session_Churn_NIT = mx_Session_Churn_NIT;
    }

    public String getMx_Recharge_Churn_NIT() {
        return mx_Recharge_Churn_NIT;
    }

    public void setMx_Recharge_Churn_NIT(String mx_Recharge_Churn_NIT) {
        this.mx_Recharge_Churn_NIT = mx_Recharge_Churn_NIT;
    }

    public String getMx_TOS_to_NS_Detail_Reason() {
        return mx_TOS_to_NS_Detail_Reason;
    }

    public void setMx_TOS_to_NS_Detail_Reason(String mx_TOS_to_NS_Detail_Reason) {
        this.mx_TOS_to_NS_Detail_Reason = mx_TOS_to_NS_Detail_Reason;
    }

    public String getMx_preSales() {
        return mx_preSales;
    }

    public void setMx_preSales(String mx_preSales) {
        this.mx_preSales = mx_preSales;
    }

    public String getMx_Subjects() {
        return mx_Subjects;
    }

    public void setMx_Subjects(String mx_Subjects) {
        this.mx_Subjects = mx_Subjects;
    }

    public String getMx_Course_Type() {
        return mx_Course_Type;
    }

    public void setMx_Course_Type(String mx_Course_Type) {
        this.mx_Course_Type = mx_Course_Type;
    }

    public String getMx_Date_of_Birth() {
        return mx_Date_of_Birth;
    }

    public void setMx_Date_of_Birth(String mx_Date_of_Birth) {
        this.mx_Date_of_Birth = mx_Date_of_Birth;
    }

    public String getMx_Father_name() {
        return mx_Father_name;
    }

    public void setMx_Father_name(String mx_Father_name) {
        this.mx_Father_name = mx_Father_name;
    }

    public String getMx_Father_emailId() {
        return mx_Father_emailId;
    }

    public void setMx_Father_emailId(String mx_Father_emailId) {
        this.mx_Father_emailId = mx_Father_emailId;
    }

    public String getMx_Mother_Name() {
        return mx_Mother_Name;
    }

    public void setMx_Mother_Name(String mx_Mother_Name) {
        this.mx_Mother_Name = mx_Mother_Name;
    }

    public String getMx_Mother_emailId() {
        return mx_Mother_emailId;
    }

    public void setMx_Mother_emailId(String mx_Mother_emailId) {
        this.mx_Mother_emailId = mx_Mother_emailId;
    }

    public String getMx_Student_School_Name() {
        return mx_Student_School_Name;
    }

    public void setMx_Student_School_Name(String mx_Student_School_Name) {
        this.mx_Student_School_Name = mx_Student_School_Name;
    }

    public String getMx_Sibling_Name() {
        return mx_Sibling_Name;
    }

    public void setMx_Sibling_Name(String mx_Sibling_Name) {
        this.mx_Sibling_Name = mx_Sibling_Name;
    }

    public String getMx_Sibling_Date_of_birth() {
        return mx_Sibling_Date_of_birth;
    }

    public void setMx_Sibling_Date_of_birth(String mx_Sibling_Date_of_birth) {
        this.mx_Sibling_Date_of_birth = mx_Sibling_Date_of_birth;
    }

    public String getMx_Sibling_Grade() {
        return mx_Sibling_Grade;
    }

    public void setMx_Sibling_Grade(String mx_Sibling_Grade) {
        this.mx_Sibling_Grade = mx_Sibling_Grade;
    }

    public String getMx_Sibling_Board() {
        return mx_Sibling_Board;
    }

    public void setMx_Sibling_Board(String mx_Sibling_Board) {
        this.mx_Sibling_Board = mx_Sibling_Board;
    }

    public String getMx_Postal_address() {
        return mx_Postal_address;
    }

    public void setMx_Postal_address(String mx_Postal_address) {
        this.mx_Postal_address = mx_Postal_address;
    }

    public String getMx_Student_Name() {
        return mx_Student_Name;
    }

    public void setMx_Student_Name(String mx_Student_Name) {
        this.mx_Student_Name = mx_Student_Name;
    }

    public String getMx_Father_contact_Number_KYC() {
        return mx_Father_contact_Number_KYC;
    }

    public void setMx_Father_contact_Number_KYC(String mx_Father_contact_Number_KYC) {
        this.mx_Father_contact_Number_KYC = mx_Father_contact_Number_KYC;
    }

    public String getMx_Mother_contact_number_KYC() {
        return mx_Mother_contact_number_KYC;
    }

    public void setMx_Mother_contact_number_KYC(String mx_Mother_contact_number_KYC) {
        this.mx_Mother_contact_number_KYC = mx_Mother_contact_number_KYC;
    }

    public String getMx_temp_first_name() {
        return mx_temp_first_name;
    }

    public void setMx_temp_first_name(String mx_temp_first_name) {
        this.mx_temp_first_name = mx_temp_first_name;
    }

    public String getMx_Request_call_back() {
        return mx_Request_call_back;
    }

    public void setMx_Request_call_back(String mx_Request_call_back) {
        this.mx_Request_call_back = mx_Request_call_back;
    }

    public String getMx_ghfh() {
        return mx_ghfh;
    }

    public void setMx_ghfh(String mx_ghfh) {
        this.mx_ghfh = mx_ghfh;
    }

    public String getMx_SusbcriptionRequestLead() {
        return mx_SusbcriptionRequestLead;
    }

    public void setMx_SusbcriptionRequestLead(String mx_SusbcriptionRequestLead) {
        this.mx_SusbcriptionRequestLead = mx_SusbcriptionRequestLead;
    }

    public String getMx_Future_Academic_Scope() {
        return mx_Future_Academic_Scope;
    }

    public void setMx_Future_Academic_Scope(String mx_Future_Academic_Scope) {
        this.mx_Future_Academic_Scope = mx_Future_Academic_Scope;
    }

    public String getLeadId() {
        return leadId;
    }

    public void setLeadId(String leadId) {
        this.leadId = leadId;
    }

    public String getMx_User_active() {
        return mx_User_active;
    }

    public void setMx_User_active(String mx_User_active) {
        this.mx_User_active = mx_User_active;
    }

    public String getMx_Contact_number_list() {
        return mx_Contact_number_list;
    }

    public void setMx_Contact_number_list(String mx_Contact_number_list) {
        this.mx_Contact_number_list = mx_Contact_number_list;
    }

    public String getMx_First_Session_id() {
        return mx_First_Session_id;
    }

    public void setMx_First_Session_id(String mx_First_Session_id) {
        this.mx_First_Session_id = mx_First_Session_id;
    }

    public String getMx_First_session_teacher_name() {
        return mx_First_session_teacher_name;
    }

    public void setMx_First_session_teacher_name(String mx_First_session_teacher_name) {
        this.mx_First_session_teacher_name = mx_First_session_teacher_name;
    }

    public String getMx_First_session_subject() {
        return mx_First_session_subject;
    }

    public void setMx_First_session_subject(String mx_First_session_subject) {
        this.mx_First_session_subject = mx_First_session_subject;
    }

    public String getFirstSessionTeacherId() {
        return firstSessionTeacherId;
    }

    public void setFirstSessionTeacherId(String firstSessionTeacherId) {
        this.firstSessionTeacherId = firstSessionTeacherId;
    }

    public String getFirstSessionSubscriptionId() {
        return firstSessionSubscriptionId;
    }

    public void setFirstSessionSubscriptionId(String firstSessionSubscriptionId) {
        this.firstSessionSubscriptionId = firstSessionSubscriptionId;
    }

    public String getFirstSessionTeacherEmail() {
        return firstSessionTeacherEmail;
    }

    public void setFirstSessionTeacherEmail(String firstSessionTeacherEmail) {
        this.firstSessionTeacherEmail = firstSessionTeacherEmail;
    }

    public String getFirstSessionTeacherContactNumber() {
        return firstSessionTeacherContactNumber;
    }

    public void setFirstSessionTeacherContactNumber(String firstSessionTeacherContactNumber) {
        this.firstSessionTeacherContactNumber = firstSessionTeacherContactNumber;
    }

    public Long getMx_Last_Payment_Amount() {
        return mx_Last_Payment_Amount;
    }

    public void setMx_Last_Payment_Amount(Long mx_Last_Payment_Amount) {
        this.mx_Last_Payment_Amount = mx_Last_Payment_Amount;
    }

    public String getMx_Last_session_id() {
        return mx_Last_session_id;
    }

    public void setMx_Last_session_id(String mx_Last_session_id) {
        this.mx_Last_session_id = mx_Last_session_id;
    }

    public String getMx_Last_session_subject() {
        return mx_Last_session_subject;
    }

    public void setMx_Last_session_subject(String mx_Last_session_subject) {
        this.mx_Last_session_subject = mx_Last_session_subject;
    }

    public String getMx_Last_session_teacher_name() {
        return mx_Last_session_teacher_name;
    }

    public void setMx_Last_session_teacher_name(String mx_Last_session_teacher_name) {
        this.mx_Last_session_teacher_name = mx_Last_session_teacher_name;
    }

    public String getLastSessionTeacherId() {
        return lastSessionTeacherId;
    }

    public void setLastSessionTeacherId(String lastSessionTeacherId) {
        this.lastSessionTeacherId = lastSessionTeacherId;
    }

    public String getLastSessionSubscriptionId() {
        return lastSessionSubscriptionId;
    }

    public void setLastSessionSubscriptionId(String lastSessionSubscriptionId) {
        this.lastSessionSubscriptionId = lastSessionSubscriptionId;
    }

    public String getLastSessionTeacherEmail() {
        return lastSessionTeacherEmail;
    }

    public void setLastSessionTeacherEmail(String lastSessionTeacherEmail) {
        this.lastSessionTeacherEmail = lastSessionTeacherEmail;
    }

    public String getLastSessionTeacherContactNumber() {
        return lastSessionTeacherContactNumber;
    }

    public void setLastSessionTeacherContactNumber(String lastSessionTeacherContactNumber) {
        this.lastSessionTeacherContactNumber = lastSessionTeacherContactNumber;
    }

    public Long getMx_Total_billing_period() {
        return mx_Total_billing_period;
    }

    public void setMx_Total_billing_period(Long mx_Total_billing_period) {
        this.mx_Total_billing_period = mx_Total_billing_period;
    }

    public Long getMx_Total_consumed_hours() {
        return mx_Total_consumed_hours;
    }

    public void setMx_Total_consumed_hours(Long mx_Total_consumed_hours) {
        this.mx_Total_consumed_hours = mx_Total_consumed_hours;
    }

    public String getTotalLockedHours() {
        return totalLockedHours;
    }

    public void setTotalLockedHours(String totalLockedHours) {
        this.totalLockedHours = totalLockedHours;
    }

    public String getTotalRemainingHours() {
        return totalRemainingHours;
    }

    public void setTotalRemainingHours(String totalRemainingHours) {
        this.totalRemainingHours = totalRemainingHours;
    }

    public String getMx_Parent_name() {
        return mx_Parent_name;
    }

    public void setMx_Parent_name(String mx_Parent_name) {
        this.mx_Parent_name = mx_Parent_name;
    }

    public String getMx_Parent_emailId() {
        return mx_Parent_emailId;
    }

    public void setMx_Parent_emailId(String mx_Parent_emailId) {
        this.mx_Parent_emailId = mx_Parent_emailId;
    }

    public String getMx_Teacher_subjects() {
        return mx_Teacher_subjects;
    }

    public void setMx_Teacher_subjects(String mx_Teacher_subjects) {
        this.mx_Teacher_subjects = mx_Teacher_subjects;
    }

    public String getMx_Parent_Contact_Number() {
        return mx_Parent_Contact_Number;
    }

    public void setMx_Parent_Contact_Number(String mx_Parent_Contact_Number) {
        this.mx_Parent_Contact_Number = mx_Parent_Contact_Number;
    }

    public String getMx_Vedantu_data_source() {
        return mx_Vedantu_data_source;
    }

    public void setMx_Vedantu_data_source(String mx_Vedantu_data_source) {
        this.mx_Vedantu_data_source = mx_Vedantu_data_source;
    }

    public String getMx_First_Session_Time() {
        return mx_First_Session_Time;
    }

    public void setMx_First_Session_Time(String mx_First_Session_Time) {
        this.mx_First_Session_Time = mx_First_Session_Time;
    }

    public String getMx_First_Payment_Amount() {
        return mx_First_Payment_Amount;
    }

    public void setMx_First_Payment_Amount(String mx_First_Payment_Amount) {
        this.mx_First_Payment_Amount = mx_First_Payment_Amount;
    }

    public String getMx_First_Payment_Time() {
        return mx_First_Payment_Time;
    }

    public void setMx_First_Payment_Time(String mx_First_Payment_Time) {
        this.mx_First_Payment_Time = mx_First_Payment_Time;
    }

    public String getMx_Last_payment_time() {
        return mx_Last_payment_time;
    }

    public void setMx_Last_payment_time(String mx_Last_payment_time) {
        this.mx_Last_payment_time = mx_Last_payment_time;
    }

    public String getMx_Last_session_time() {
        return mx_Last_session_time;
    }

    public void setMx_Last_session_time(String mx_Last_session_time) {
        this.mx_Last_session_time = mx_Last_session_time;
    }

    public String getMx_Sub_Stage() {
        return mx_Sub_Stage;
    }

    public void setMx_Sub_Stage(String mx_Sub_Stage) {
        this.mx_Sub_Stage = mx_Sub_Stage;
    }

    public Set<String> fetchAllPhones() {
        Set<String> phones = new HashSet<>();
        if (!StringUtils.isEmpty(this.phone)) {
            phones.add(this.phone.trim());
        }

        if (!StringUtils.isEmpty(this.mobile)) {
            phones.add(this.mobile.trim());
        }

        if (!StringUtils.isEmpty(this.mx_PhoneNumberList)) {
            phones.addAll(CommonUtils.getAllElementSet(this.mx_PhoneNumberList, ","));
        }

        if (!StringUtils.isEmpty(this.mx_activeNumber)) {
            phones.add(this.mx_activeNumber);
        }

        if (!StringUtils.isEmpty(this.mx_Contact_number_list)) {
            phones.addAll(CommonUtils.getAllElementSet(this.mx_Contact_number_list, ","));
        }

        if (!StringUtils.isEmpty(this.mx_Father_contact_Number_KYC)) {
            phones.add(this.mx_Father_contact_Number_KYC);
        }

        if (!StringUtils.isEmpty(this.mx_Mother_contact_number_KYC)) {
            phones.add(this.mx_Mother_contact_number_KYC);
        }

        if (!StringUtils.isEmpty(this.mx_ParentContactNumber)) {
            phones.add(this.mx_ParentContactNumber);
        }

        if (!StringUtils.isEmpty(this.mx_ParentContactNumberList)) {
            phones.addAll(CommonUtils.getAllElementSet(this.mx_ParentContactNumberList, ","));
        }

        if (!StringUtils.isEmpty(this.mx_Parent_Contact_Number)) {
            phones.add(this.mx_Parent_Contact_Number);
        }

        return phones;
    }

    public Set<String> fetchAllEmails() {
        Set<String> allEmails = new HashSet<>();
        if (!StringUtils.isEmpty(this.emailAddress)) {
            allEmails.add(this.emailAddress);
        }

        if (!StringUtils.isEmpty(this.mx_Father_emailId)) {
            allEmails.add(this.mx_Father_emailId);
        }

        if (!StringUtils.isEmpty(this.mx_Mother_emailId)) {
            allEmails.add(this.mx_Mother_emailId);
        }

        if (!StringUtils.isEmpty(this.mx_Parent_emailId)) {
            allEmails.add(this.mx_Parent_emailId);
        }

        return allEmails;
    }

    public void updateUserProfileData(UserProfileData userProfileData) {
        this.lastSessionTeacherId = userProfileData.getLastSessionTeacherId();
        this.lastSessionSubscriptionId = userProfileData.getLastSessionSubscriptionId();
        this.mx_Last_session_id = userProfileData.getLastSessionId();
        this.mx_Last_session_time = CommonUtils.calculateTimeStampIst(userProfileData.getLastSessionTime());
        this.mx_Last_session_subject = userProfileData.getLastSessionSubject();
        this.mx_Last_session_teacher_name = userProfileData.getLastSessionTeacherName();
        this.lastSessionTeacherContactNumber = userProfileData.getLastSessionTeacherContactNumber();

        this.mx_First_session_teacher_name = userProfileData.getFirstSessionTeacherName();
        this.firstSessionTeacherId = userProfileData.getFirstSessionTeacherId();
        this.firstSessionSubscriptionId = userProfileData.getFirstSessionSubscriptionId();
        this.mx_First_Session_id = userProfileData.getFirstSessionId();
        this.mx_First_Session_Time = CommonUtils.calculateTimeStampIst(userProfileData.getFirstSessionTime());
        this.mx_First_session_subject = userProfileData.getFirstSessionSubject();
        this.mx_First_session_teacher_name = userProfileData.getFirstSessionTeacherName();
        this.firstSessionTeacherEmail = userProfileData.getFirstSessionTeacherEmail();
        this.firstSessionTeacherContactNumber = userProfileData.getFirstSessionTeacherContactNumber();

        this.mx_Last_Payment_Amount = userProfileData.getLastPaymentTableAmount();
        this.mx_Last_payment_time = CommonUtils.calculateTimeStampIst(userProfileData.getLastPaymentTime());
        this.mx_First_Payment_Amount = String.valueOf(userProfileData.getFirstPaymentTableAmount());
        this.mx_First_Payment_Time = CommonUtils.calculateTimeStampIst(userProfileData.getFirstPaymentTime());
        this.revenue = userProfileData.getTotalPayment();
        this.mx_Total_billing_period = userProfileData.getTotalBillingPeriod();
        this.mx_Total_consumed_hours = userProfileData.getTotalConsumedHours();
        this.totalLockedHours = userProfileData.getTotalLockedHours();
        this.totalRemainingHours = userProfileData.getTotalRemainingHours();
        this.mx_Teacher_subjects = userProfileData.getTeacherSubjects();
    }

    public static class Constants extends AbstractMongoEntity.Constants {

        public static final String LEAD_ID = "leadId";
        public static final String VEDANTU_USER_ID = "mx_User_Id";
    }

    public String getMx_SAM_Allocation() {
        return mx_SAM_Allocation;
    }

    public void setMx_SAM_Allocation(String mx_SAM_Allocation) {
        this.mx_SAM_Allocation = mx_SAM_Allocation;
    }

    public String getMx_Last_Assigned_Date() {
        return mx_Last_Assigned_Date;
    }

    public void setMx_Last_Assigned_Date(String mx_Last_Assigned_Date) {
        this.mx_Last_Assigned_Date = mx_Last_Assigned_Date;
    }

    public String getMx_VCLead() {
        return mx_VCLead;
    }

    public void setMx_VCLead(String mx_VCLead) {
        this.mx_VCLead = mx_VCLead;
    }

    public String getMx_User_Target() {
        return mx_User_Target;
    }

    public void setMx_User_Target(String mx_User_Target) {
        this.mx_User_Target = mx_User_Target;
    }

    public String getMx_LastActivityDate() {
        return mx_LastActivityDate;
    }

    public void setMx_LastActivityDate(String mx_LastActivityDate) {
        this.mx_LastActivityDate = mx_LastActivityDate;
    }

    private void setSuccess(boolean success) {
        this.success = success;
    }

    public boolean isSuccess() {
        return success;
    }

    public String getMx_Assigned_Count() {
        return mx_Assigned_Count;
    }

    public void setMx_Assigned_Count(String mx_Assigned_Count) {
        this.mx_Assigned_Count = mx_Assigned_Count;
    }

    public String getMx_Core_Noncore() {
        return mx_Core_Noncore;
    }

    public void setMx_Core_Noncore(String mx_Core_Noncore) {
        this.mx_Core_Noncore = mx_Core_Noncore;
    }

    public String getMx_GPS_City() {
        return mx_GPS_City;
    }

    public void setMx_GPS_City(String mx_GPS_City) {
        this.mx_GPS_City = mx_GPS_City;
    }

    public String getMx_GPS_lat() {
        return mx_GPS_lat;
    }

    public void setMx_GPS_lat(String mx_GPS_lat) {
        this.mx_GPS_lat = mx_GPS_lat;
    }

    public String getMx_GPS_Long() {
        return mx_GPS_Long;
    }

    public void setMx_GPS_Long(String mx_GPS_Long) {
        this.mx_GPS_Long = mx_GPS_Long;
    }

    public String getMx_Location_type() {
        return mx_Location_type;
    }

    public void setMx_Location_type(String mx_Location_type) {
        this.mx_Location_type = mx_Location_type;
    }

    public String getMx_Nearby_City() {
        return mx_Nearby_City;
    }

    public void setMx_Nearby_City(String mx_Nearby_City) {
        this.mx_Nearby_City = mx_Nearby_City;
    }

    public String getMx_CC_LT() {
        return mx_CC_LT;
    }

    public void setMx_CC_LT(String mx_CC_LT) {
        this.mx_CC_LT = mx_CC_LT;
    }

    @Override
    public String toString() {
        return "LeadDetails{" + "leadId=" + leadId + ", conversionReferrerURL=" + conversionReferrerURL + ", sourceReferrerURL=" + sourceReferrerURL + ", sourceIPAddress=" + sourceIPAddress + ", prospectAutoId=" + prospectAutoId + ", prospectID=" + prospectID + ", prospectActivityId_Max=" + prospectActivityId_Max + ", prospectActivityId_Min=" + prospectActivityId_Min + ", statusCode=" + statusCode + ", statusReason=" + statusReason + ", deletionStatusCode=" + deletionStatusCode + ", relatedProspectId=" + relatedProspectId + ", sourceReferrer=" + sourceReferrer + ", notableEvent=" + notableEvent + ", notableEventdate=" + notableEventdate + ", lastVisitDate=" + lastVisitDate + ", relatedLandingPageId=" + relatedLandingPageId + ", firstLandingPageSubmissionId=" + firstLandingPageSubmissionId + ", firstLandingPageSubmissionDate=" + firstLandingPageSubmissionDate + ", prospectActivityName_Max=" + prospectActivityName_Max + ", prospectActivityDate_Max=" + prospectActivityDate_Max + ", firstName=" + firstName + ", lastName=" + lastName + ", emailAddress=" + emailAddress + ", phone=" + phone + ", company=" + company + ", website=" + website + ", doNotEmail=" + doNotEmail + ", doNotCall=" + doNotCall + ", source=" + source + ", mobile=" + mobile + ", sourceCampaign=" + sourceCampaign + ", sourceMedium=" + sourceMedium + ", jobTitle=" + jobTitle + ", sourceContent=" + sourceContent + ", notes=" + notes + ", grade=" + grade + ", qualityScore01=" + qualityScore01 + ", score=" + score + ", engagementScore=" + engagementScore + ", revenue=" + revenue + ", prospectStage=" + prospectStage + ", ownerId=" + ownerId + ", ownerIdName=" + ownerIdName + ", leadCreatedBy=" + leadCreatedBy + ", leadCreatedByName=" + leadCreatedByName + ", leadCreatedOn=" + leadCreatedOn + ", leadConversionDate=" + leadConversionDate + ", modifiedBy=" + modifiedBy + ", modifiedByName=" + modifiedByName + ", modifiedOn=" + modifiedOn + ", timeZone=" + timeZone + ", totalVisits=" + totalVisits + ", pageViewsPerVisit=" + pageViewsPerVisit + ", avgTimePerVisit=" + avgTimePerVisit + ", origin=" + origin + ", facebookId=" + facebookId + ", linkedInId=" + linkedInId + ", mailingPreferences=" + mailingPreferences + ", googlePlusId=" + googlePlusId + ", leadAge=" + leadAge + ", starredLead=" + starredLead + ", taggedLead=" + taggedLead + ", canUpdate=" + canUpdate + ", mx_Street1=" + mx_Street1 + ", mx_Street2=" + mx_Street2 + ", mx_City=" + mx_City + ", mx_State=" + mx_State + ", mx_Country=" + mx_Country + ", mx_Zip=" + mx_Zip + ", mx_FaceBookURL=" + mx_FaceBookURL + ", mx_LinkedInURL=" + mx_LinkedInURL + ", mx_TwitterURL=" + mx_TwitterURL + ", mx_Student_Class=" + mx_Student_Class + ", mx_Source_Channel=" + mx_Source_Channel + ", mx_Class=" + mx_Class + ", mx_StudentGrade=" + mx_StudentGrade + ", mx_Board=" + mx_Board + ", mx_School=" + mx_School + ", mx_Mobile=" + mx_Mobile + ", mx_PhoneNumberList=" + mx_PhoneNumberList + ", mx_CreationTime=" + mx_CreationTime + ", mx_ModifiedTime=" + mx_ModifiedTime + ", mx_Gender=" + mx_Gender + ", mx_Role=" + mx_Role + ", mx_LanguageList=" + mx_LanguageList + ", mx_IsEmailVerifed=" + mx_IsEmailVerifed + ", mx_IsContactNumberVerified=" + mx_IsContactNumberVerified + ", mx_IsContactNumberWhiteListed=" + mx_IsContactNumberWhiteListed + ", mx_IsParentRegistration=" + mx_IsParentRegistration + ", mx_ParentContactNumber=" + mx_ParentContactNumber + ", mx_ParentContactNumberList=" + mx_ParentContactNumberList + ", mx_ParentEmailList=" + mx_ParentEmailList + ", mx_SourceCampaign=" + mx_SourceCampaign + ", mx_SourceLead=" + mx_SourceLead + ", mx_SourceMedium=" + mx_SourceMedium + ", mx_IsContactNumberDND=" + mx_IsContactNumberDND + ", mx_User_Id=" + mx_User_Id + ", mx_activeNumber=" + mx_activeNumber + ", mx_activeNumberVerified=" + mx_activeNumberVerified + ", mx_activeNumberDND=" + mx_activeNumberDND + ", mx_Source_term=" + mx_Source_term + ", mx_Remark=" + mx_Remark + ", mx_Query_Type=" + mx_Query_Type + ", mx_TOS_To_be_booked_on=" + mx_TOS_To_be_booked_on + ", mx_Future_TOS_Booking_Date=" + mx_Future_TOS_Booking_Date + ", mx_TOS_Done_On=" + mx_TOS_Done_On + ", mx_Source_Creation_Date=" + mx_Source_Creation_Date + ", mx_Reason_For_Not_Interested=" + mx_Reason_For_Not_Interested + ", mx_Sign_up_URL=" + mx_Sign_up_URL + ", mx_Sign_up_feature=" + mx_Sign_up_feature + ", mx_featureHide=" + mx_featureHide + ", mx_Session_Churn_NIT=" + mx_Session_Churn_NIT + ", mx_Recharge_Churn_NIT=" + mx_Recharge_Churn_NIT + ", mx_TOS_to_NS_Detail_Reason=" + mx_TOS_to_NS_Detail_Reason + ", mx_preSales=" + mx_preSales + ", mx_Subjects=" + mx_Subjects + ", mx_Course_Type=" + mx_Course_Type + ", mx_Date_of_Birth=" + mx_Date_of_Birth + ", mx_Father_name=" + mx_Father_name + ", mx_Father_emailId=" + mx_Father_emailId + ", mx_Mother_Name=" + mx_Mother_Name + ", mx_Mother_emailId=" + mx_Mother_emailId + ", mx_Student_School_Name=" + mx_Student_School_Name + ", mx_Sibling_Name=" + mx_Sibling_Name + ", mx_Sibling_Date_of_birth=" + mx_Sibling_Date_of_birth + ", mx_Sibling_Grade=" + mx_Sibling_Grade + ", mx_Sibling_Board=" + mx_Sibling_Board + ", mx_Postal_address=" + mx_Postal_address + ", mx_Student_Name=" + mx_Student_Name + ", mx_Father_contact_Number_KYC=" + mx_Father_contact_Number_KYC + ", mx_Mother_contact_number_KYC=" + mx_Mother_contact_number_KYC + ", mx_temp_first_name=" + mx_temp_first_name + ", mx_Request_call_back=" + mx_Request_call_back + ", mx_ghfh=" + mx_ghfh + ", mx_SusbcriptionRequestLead=" + mx_SusbcriptionRequestLead + ", mx_Future_Academic_Scope=" + mx_Future_Academic_Scope + ", mx_Parent_name=" + mx_Parent_name + ", mx_Parent_emailId=" + mx_Parent_emailId + ", mx_Parent_Contact_Number=" + mx_Parent_Contact_Number + ", mx_Vedantu_data_source=" + mx_Vedantu_data_source + ", mx_User_active=" + mx_User_active + ", mx_Contact_number_list=" + mx_Contact_number_list + ", mx_First_Session_id=" + mx_First_Session_id + ", mx_First_Session_Time=" + mx_First_Session_Time + ", mx_First_Payment_Amount=" + mx_First_Payment_Amount + ", mx_First_session_teacher_name=" + mx_First_session_teacher_name + ", mx_First_session_subject=" + mx_First_session_subject + ", mx_First_Payment_Time=" + mx_First_Payment_Time + ", firstSessionTeacherId=" + firstSessionTeacherId + ", firstSessionSubscriptionId=" + firstSessionSubscriptionId + ", firstSessionTeacherEmail=" + firstSessionTeacherEmail + ", firstSessionTeacherContactNumber=" + firstSessionTeacherContactNumber + ", mx_Last_Payment_Amount=" + mx_Last_Payment_Amount + ", mx_Last_payment_time=" + mx_Last_payment_time + ", mx_Last_session_id=" + mx_Last_session_id + ", mx_Last_session_subject=" + mx_Last_session_subject + ", mx_Last_session_teacher_name=" + mx_Last_session_teacher_name + ", mx_Last_session_time=" + mx_Last_session_time + ", lastSessionTeacherId=" + lastSessionTeacherId + ", lastSessionSubscriptionId=" + lastSessionSubscriptionId + ", lastSessionTeacherEmail=" + lastSessionTeacherEmail + ", lastSessionTeacherContactNumber=" + lastSessionTeacherContactNumber + ", mx_Total_billing_period=" + mx_Total_billing_period + ", mx_Total_consumed_hours=" + mx_Total_consumed_hours + ", totalLockedHours=" + totalLockedHours + ", totalRemainingHours=" + totalRemainingHours + ", mx_Teacher_subjects=" + mx_Teacher_subjects + ", mx_SAM_Allocation=" + mx_SAM_Allocation + ", mx_Sub_Stage=" + mx_Sub_Stage + ", mx_Last_Assigned_Date=" + mx_Last_Assigned_Date + ", mx_VCLead=" + mx_VCLead + ", mx_User_Target=" + mx_User_Target + ", success=" + success + '}' + mx_LastActivityDate;
    }

}
