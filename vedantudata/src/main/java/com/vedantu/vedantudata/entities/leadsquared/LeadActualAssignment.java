package com.vedantu.vedantudata.entities.leadsquared;

import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import lombok.Data;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

@Data
@Document(collection = "LeadActualAssignment")
@CompoundIndexes({
        @CompoundIndex(name = "counsellorId_1_date_1", def = "{'counsellorId':1, 'date':1}", background = true,unique = true)
})
public class LeadActualAssignment extends AbstractMongoStringIdEntity {
    private Integer bucketCount;
    private String counsellorId;
    private Integer requirement = 0;
    private Integer assignedLeadsCount = 0;
    private Integer freshAssignedCount = 0;
    private Integer churnAssignmentCount = 0;
    private Integer stateBoardCount = 0;
    private Integer nonStateBoardCount = 0;
    private Long date;
    private String languagePreference1;
    private String languagePreference2;
    private String gradePreference1;
    private String gradePreference2;
    private String gradePreference3;
    private String leadTypePreference1;
    private String leadTypePreference2;
    private String leadTypePreference3;
    private String defaultLanguage;
    private Date lastUpdatedDate;

}
