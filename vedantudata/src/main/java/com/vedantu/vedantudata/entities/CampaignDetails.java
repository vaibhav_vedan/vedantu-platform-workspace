package com.vedantu.vedantudata.entities;

import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import com.vedantu.vedantudata.enums.CampaignType;

public class CampaignDetails extends AbstractMongoStringIdEntity {
	private String campaignName;
	private String campaignId;
	private CampaignType campaignType;

	public CampaignDetails() {
		super();
	}

	public String getCampaignName() {
		return campaignName;
	}

	public void setCampaignName(String campaignName) {
		this.campaignName = campaignName;
	}

	public String getCampaignId() {
		return campaignId;
	}

	public void setCampaignId(String campaignId) {
		this.campaignId = campaignId;
	}

	public CampaignType getCampaignType() {
		return campaignType;
	}

	public void setCampaignType(CampaignType campaignType) {
		this.campaignType = campaignType;
	}

}
