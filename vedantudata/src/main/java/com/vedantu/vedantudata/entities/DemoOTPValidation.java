package com.vedantu.vedantudata.entities;

import com.google.gson.annotations.SerializedName;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import com.vedantu.vedantudata.pojos.ActivityOwnerChangeHistory;
import com.vedantu.vedantudata.pojos.ActivityStatusChangeHistory;
import com.vedantu.vedantudata.pojos.LeadsquaredActivityField;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

@Getter
@Setter
@Document(collection = "DemoOTPValidation")
public class DemoOTPValidation extends AbstractMongoStringIdEntity {

    String prospectActivityId;
    String phone;
    String emailAddress;
    String OTP;
    Long expiryTime;

    public static class Constants {

        public static final String EMAIL_ADDRESS = "emailAddress";
        public static final String EXPIRY_TIME = "expiryTime";

    }

}
