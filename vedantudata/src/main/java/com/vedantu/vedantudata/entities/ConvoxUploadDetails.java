package com.vedantu.vedantudata.entities;

import java.util.ArrayList;
import java.util.List;

import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import com.vedantu.vedantudata.enums.UploadFileStatus;

public class ConvoxUploadDetails extends AbstractMongoStringIdEntity {
	private String processName;
	private long count;
	private long date;
	private String fileName;
	private UploadFileStatus uploadFileStatus;
	private String fieldNames;
	private int updatedRows;
	private List<String> errorRowNumbers = new ArrayList<>();

	public ConvoxUploadDetails() {
		super();
	}

	public String getProcessName() {
		return processName;
	}

	public void setProcessName(String processName) {
		this.processName = processName;
	}

	public long getCount() {
		return count;
	}

	public void setCount(long count) {
		this.count = count;
	}

	public long getDate() {
		return date;
	}

	public void setDate(long date) {
		this.date = date;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public UploadFileStatus getUploadFileStatus() {
		return uploadFileStatus;
	}

	public void setUploadFileStatus(UploadFileStatus uploadFileStatus) {
		this.uploadFileStatus = uploadFileStatus;
	}

	public String getFieldNames() {
		return fieldNames;
	}

	public void setFieldNames(String fieldNames) {
		this.fieldNames = fieldNames;
	}

	public List<String> getErrorRowNumbers() {
		return errorRowNumbers;
	}

	public void setErrorRowNumbers(List<String> errorRowNumbers) {
		this.errorRowNumbers = errorRowNumbers;
	}

	public void addErrorRowNumbers(String errorString) {
		this.errorRowNumbers.add(errorString);
	}

	public int getUpdatedRows() {
		return updatedRows;
	}

	public void setUpdatedRows(int updatedRows) {
		this.updatedRows = updatedRows;
	}

	@Override
	public String toString() {
		return "ConvoxUploadDetails [processName=" + processName + ", count=" + count + ", date=" + date + ", fileName="
				+ fileName + ", uploadFileStatus=" + uploadFileStatus + ", fieldNames=" + fieldNames + ", updatedRows="
				+ updatedRows + ", errorRowNumbers=" + errorRowNumbers + ", toString()=" + super.toString() + "]";
	}
}
