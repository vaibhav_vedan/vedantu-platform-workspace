package com.vedantu.vedantudata.entities;

import java.util.HashSet;
import java.util.Set;

import com.vedantu.util.CollectionUtils;
import com.vedantu.util.dbentities.mongo.AbstractMongoEntity;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;

public class UserData extends AbstractMongoStringIdEntity {
	private String emailId;
	private String primaryContactNumber;
	private Set<String> allEmails;
	private Set<String> allPhoneNumber;
	private Long engagementScore;
	private Set<String> leadSquaredIds;
	private Set<String> userId;

	public UserData() {
		super();
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getPrimaryContactNumber() {
		return primaryContactNumber;
	}

	public void setPrimaryContactNumber(String primaryContactNumber) {
		this.primaryContactNumber = primaryContactNumber;
	}

	public Set<String> getAllEmails() {
		return allEmails;
	}

	public void setAllEmails(Set<String> allEmails) {
		this.allEmails = allEmails;
	}

	public void addAllEmails(Set<String> newEmails) {
		if (this.allEmails == null) {
			allEmails = new HashSet<>();
		}
		if (!CollectionUtils.isEmpty(newEmails)) {
			this.allEmails.addAll(newEmails);
		}
	}

	public Set<String> getAllPhoneNumber() {
		return allPhoneNumber;
	}

	public void setAllPhoneNumber(Set<String> allPhoneNumber) {
		this.allPhoneNumber = allPhoneNumber;
	}

	public void addAllPhoneNumbers(Set<String> newPhones) {
		if (this.allPhoneNumber == null) {
			allPhoneNumber = new HashSet<>();
		}
		if (!CollectionUtils.isEmpty(newPhones)) {
			this.allPhoneNumber.addAll(newPhones);
		}
	}

	public Long getEngagementScore() {
		return engagementScore;
	}

	public void setEngagementScore(Long engagementScore) {
		this.engagementScore = engagementScore;
	}

	public Set<String> getLeadSquaredIds() {
		return leadSquaredIds;
	}

	public void setLeadSquaredIds(Set<String> leadSquaredIds) {
		this.leadSquaredIds = leadSquaredIds;
	}

	public void addLeadSquaredId(String leadSquaredId) {
		if (this.leadSquaredIds == null) {
			this.leadSquaredIds = new HashSet<>();
		}
		this.leadSquaredIds.add(leadSquaredId);
	}

	public Set<String> getUserId() {
		return userId;
	}

	public void setUserId(Set<String> userId) {
		this.userId = userId;
	}

	public static class Constants extends AbstractMongoEntity.Constants {
		public static final String USER_EMAIL = "emailId";
		public static final String PRIMARY_CONTACT_NUMBER = "primaryContactNumber";
		public static final String ALL_EMAILS = "allEmails";
		public static final String ALL_PHONE_NUMBERS = "allPhoneNumber";
	}

	@Override
	public String toString() {
		return "UserData [emailId=" + emailId + ", primaryContactNumber=" + primaryContactNumber + ", allEmails="
				+ allEmails + ", allPhoneNumber=" + allPhoneNumber + ", engagementScore=" + engagementScore
				+ ", leadSquaredIds=" + leadSquaredIds + ", userId=" + userId + ", toString()=" + super.toString()
				+ "]";
	}
}
