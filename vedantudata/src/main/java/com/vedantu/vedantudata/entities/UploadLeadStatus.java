package com.vedantu.vedantudata.entities;

public class UploadLeadStatus {
	private String leadId;
	private Boolean leadCreated;
	private Boolean leadUpdated;

	public UploadLeadStatus() {
		super();
	}

	public UploadLeadStatus(String leadId, Boolean leadCreated, Boolean leadUpdated) {
		super();
		this.leadId = leadId;
		this.leadCreated = leadCreated;
		this.leadUpdated = leadUpdated;
	}

	public String getLeadId() {
		return leadId;
	}

	public void setLeadId(String leadId) {
		this.leadId = leadId;
	}

	public Boolean getLeadCreated() {
		return leadCreated;
	}

	public void setLeadCreated(Boolean leadCreated) {
		this.leadCreated = leadCreated;
	}

	public Boolean getLeadUpdated() {
		return leadUpdated;
	}

	public void setLeadUpdated(Boolean leadUpdated) {
		this.leadUpdated = leadUpdated;
	}

	@Override
	public String toString() {
		return "UploadLeadStatus [leadId=" + leadId + ", leadCreated=" + leadCreated + ", leadUpdated=" + leadUpdated
				+ ", toString()=" + super.toString() + "]";
	}
}
