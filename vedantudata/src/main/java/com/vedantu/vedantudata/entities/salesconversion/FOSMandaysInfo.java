package com.vedantu.vedantudata.entities.salesconversion;

import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@ToString
@Document(collection = "FOSMandaysInfo")
public class FOSMandaysInfo extends AbstractMongoStringIdEntity {

    @Indexed(background = true)
    private String centreId;

    @Indexed(background = true)
    private Long date;

    private Integer mandays;

    public static class Constants {
        public static String CENTRE_ID = "centreId";
        public static String DATE = "date";
        public static String MANDAYS = "mandays";
    }
}
