package com.vedantu.vedantudata.entities.salesconversion;

import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import com.vedantu.vedantudata.enums.salesconversion.AgentDesignation;
import com.vedantu.vedantudata.enums.salesconversion.AgentDesignationType;
import com.vedantu.vedantudata.enums.salesconversion.AgentStatus;
import com.vedantu.vedantudata.enums.salesconversion.SalesTeam;
import lombok.Data;
import lombok.ToString;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@ToString
@Document(collection = "SalesAgentInfoHistory")
public class SalesAgentInfoHistory extends AbstractMongoStringIdEntity {

    private String name;

    @Indexed(background = true)
    private String employeeId;

    @Indexed(background = true)
    private String email;

    private Long dateOfJoining;

    @Indexed(background = true)
    private AgentDesignation designation;

    @Indexed(background = true)
    private AgentDesignationType designationType;

    private String centreId;

    @Indexed(background = true)
    private String isRMId;

    @Indexed(background = true)
    private String isFMId;

    private String tlId;

    @Indexed(background = true)
    private AgentStatus status;

    @Indexed(background = true)
    private SalesTeam team;

    private Long dateOfLeaving;

    private String leadsquaredId;

    public static class Constants {
        public static final String TEAM = "team";
        public static final String CENTRE_ID = "centreId";

    }
}
