package com.vedantu.vedantudata.entities;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.vedantu.util.dbentities.mongo.AbstractMongoEntity;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import com.vedantu.vedantudata.enums.UploadFileStatus;
import com.vedantu.vedantudata.enums.UploadFileType;

@JsonPropertyOrder({ "fileName", "activityKey", "uploadFileStatus", "leadProperties" })
public class UploadIdentifier extends AbstractMongoStringIdEntity {
	private String fileName;
	private String activityKey;
	private UploadFileStatus uploadFileStatus;
	private UploadLeadProperties leadProperties = new UploadLeadProperties();
	private UploadFileType uploadFileType;
	private String fieldNames;
	private String source;
	private Boolean noActivity;
	private List<Long> uploadTimes = new ArrayList<>();

	public UploadIdentifier() {
		super();
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getActivityKey() {
		return activityKey;
	}

	public void setActivityKey(String activityKey) {
		this.activityKey = activityKey;
	}

	public List<Long> getUploadTimes() {
		return uploadTimes;
	}

	public void setUploadTimes(List<Long> uploadTimes) {
		this.uploadTimes = uploadTimes;
	}

	public void addUploadTime() {
		this.uploadTimes.add(System.currentTimeMillis());
	}

	public void addUploadTime(long time) {
		this.uploadTimes.add(time);
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public UploadLeadProperties getLeadProperties() {
		return leadProperties;
	}

	public void setLeadProperties(UploadLeadProperties leadProperties) {
		this.leadProperties = leadProperties;
	}

	public Boolean getNoActivity() {
		return noActivity;
	}

	public void setNoActivity(Boolean noActivity) {
		this.noActivity = noActivity;
	}

	public UploadFileStatus getUploadFileStatus() {
		return uploadFileStatus;
	}

	public void setUploadFileStatus(UploadFileStatus uploadFileStatus) {
		this.uploadFileStatus = uploadFileStatus;
	}

	public String getFieldNames() {
		return fieldNames;
	}

	public void setFieldNames(String fieldNames) {
		this.fieldNames = fieldNames;
	}

	public UploadFileType getUploadFileType() {
		return uploadFileType;
	}

	public void setUploadFileType(UploadFileType uploadFileType) {
		this.uploadFileType = uploadFileType;
	}

	public void addErrorRow(String errorRow) {
		this.leadProperties.addErrorRows(errorRow);
	}

	public void addUploadLeadStatus(String leadId, Boolean leadCreated, Boolean leadUpdated) {
		this.leadProperties.addUploadLeadStatus(leadId, leadCreated, leadUpdated);
	}

	public static class Constants extends AbstractMongoEntity.Constants {
		public static final String FILE_NAME = "fileName";
		public static final String ACTIVITY_KEY = "activityKey";
	}

	@Override
	public String toString() {
		return "UploadIdentifier [fileName=" + fileName + ", activityKey=" + activityKey + ", uploadTimes="
				+ uploadTimes + ", source=" + source + ", leadProperties=" + leadProperties + ", noActivity="
				+ noActivity + ", uploadFileStatus=" + uploadFileStatus + ", uploadFileType=" + uploadFileType
				+ ", fieldNames=" + fieldNames + ", toString()=" + super.toString() + "]";
	}
}
