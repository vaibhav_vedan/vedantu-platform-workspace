package com.vedantu.vedantudata.entities;

import java.util.List;

import com.vedantu.util.dbentities.mongo.AbstractMongoEntity;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import com.vedantu.vedantudata.enums.GoogleItemType;
import com.vedantu.vedantudata.enums.GoogleSheetType;

public class GoogleSheetProperties extends AbstractMongoStringIdEntity {
	private String itemName;
	private String itemId;
	private List<String> fields;
	private GoogleItemType itemType;
	private GoogleSheetType sheetType;
	private int lastUpdatedRow;

	public GoogleSheetProperties() {
		super();
	}

	public GoogleSheetProperties(String itemId, String itemName, List<String> fields, GoogleSheetType googleSheetType,
			GoogleItemType googleItemType, int lastUpdatedRow) {
		super();
		this.itemId = itemId;
		this.itemName = itemName;
		this.fields = fields;
		this.sheetType = googleSheetType;
		this.itemType = googleItemType;
		this.lastUpdatedRow = lastUpdatedRow;
	}

	public String getItemId() {
		return itemId;
	}

	public void setItemId(String itemId) {
		this.itemId = itemId;
	}

	public List<String> getFields() {
		return fields;
	}

	public void setFields(List<String> fields) {
		this.fields = fields;
	}

	public GoogleSheetType getSheetType() {
		return sheetType;
	}

	public void setSheetType(GoogleSheetType sheetType) {
		this.sheetType = sheetType;
	}

	public int getLastUpdatedRow() {
		return lastUpdatedRow;
	}

	public void setLastUpdatedRow(int lastUpdatedRow) {
		this.lastUpdatedRow = lastUpdatedRow;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public GoogleItemType getItemType() {
		return itemType;
	}

	public void setItemType(GoogleItemType itemType) {
		this.itemType = itemType;
	}

	public String getLastColumnExcelId() {
		return String.valueOf('A' + (fields.size() - 1));
	}

	public void incrementLastUpdatedRow() {
		this.lastUpdatedRow++;
	}

	public static class Constants extends AbstractMongoEntity.Constants {
		public static final String ITEM_NAME = "itemName";
		public static final String ITEM_TYPE = "itemType";
		public static final String ITEM_ID = "itemId";
		public static final String SHEET_TYPE = "sheetType";
	}

	@Override
	public String toString() {
		return "GoogleSheetProperties [itemName=" + itemName + ", itemId=" + itemId + ", fields=" + fields
				+ ", itemType=" + itemType + ", sheetType=" + sheetType + ", lastUpdatedRow=" + lastUpdatedRow
				+ ", toString()=" + super.toString() + "]";
	}
}
