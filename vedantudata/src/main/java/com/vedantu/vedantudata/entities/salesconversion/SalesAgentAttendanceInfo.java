package com.vedantu.vedantudata.entities.salesconversion;

import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import com.vedantu.vedantudata.enums.salesconversion.AgentAttendanceStatus;
import lombok.Data;
import lombok.ToString;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@ToString
@Document(collection = "SalesAgentAttendanceInfo")
@CompoundIndexes({
        @CompoundIndex(name = "agentId_1_date_1", def = "{'agentId':1, 'date':1}", background = true,unique = true)
})
public class SalesAgentAttendanceInfo extends AbstractMongoStringIdEntity {

    @Indexed(background = true)
    private String agentId;

    @Indexed(background = true)
    private AgentAttendanceStatus attendanceStatus;

    @Indexed(background = true)
    private Long date;

    public static class Constants extends AbstractMongoStringIdEntity.Constants {
        public static final String AGENT_ID = "agentId";
        public static final String ATTENDANCE_STATUS = "attendanceStatus";
        public static final String DATE = "date";
    }
}
