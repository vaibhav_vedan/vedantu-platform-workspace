package com.vedantu.vedantudata.entities;

import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "LeadVcCode")
public class LeadVcCode extends AbstractMongoStringIdEntity {
    private String email;
    @Indexed(background = true)
    private String vcCode;
    private String phone;
    private String mx_VCLead;

    public LeadVcCode() {
        super();
    }

    public LeadVcCode(String email, String vcCode, String phone, String mx_VCLead) {
        this.email = email;
        this.vcCode = vcCode;
        this.phone = phone;
        this.mx_VCLead = mx_VCLead;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getVcCode() {
        return vcCode;
    }

    public void setVcCode(String vcCode) {
        this.vcCode = vcCode;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getMx_VCLead() {
        return mx_VCLead;
    }

    public void setMx_VCLead(String mx_VCLead) {
        this.mx_VCLead = mx_VCLead;
    }
}
