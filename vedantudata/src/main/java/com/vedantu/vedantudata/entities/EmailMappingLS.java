package com.vedantu.vedantudata.entities;

import com.google.gson.annotations.SerializedName;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import com.vedantu.vedantudata.pojos.ActivityOwnerChangeHistory;
import com.vedantu.vedantudata.pojos.ActivityStatusChangeHistory;
import com.vedantu.vedantudata.pojos.LeadsquaredActivityField;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@Document(collection = "EmailMappingLS")
public class EmailMappingLS extends AbstractMongoStringIdEntity {

    private String vedantuEmail;
    private String lsEmail;

    public static class Constants {
        public static String VEDANTU_EMAIL = "vedantuEmail";
        public static String LS_EMAIL = "lsEmail";
    }

}
