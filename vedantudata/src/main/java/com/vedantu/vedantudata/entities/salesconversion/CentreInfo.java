package com.vedantu.vedantudata.entities.salesconversion;

import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import com.vedantu.vedantudata.enums.salesconversion.SalesTeam;
import lombok.Data;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Data
@ToString
@Document(collection = "CentreInfo")
@CompoundIndex(name = "team_centreName", def = "{'team':1, 'centreName' : 1}", background = true)
public class CentreInfo extends AbstractMongoStringIdEntity {

    @Indexed(background = true, unique = true)
    private String centreName;

    private String centreHeadId;

    @Indexed(background = true)
    private String zhId;

    @Indexed(background = true)
    private String rmId;

    @Indexed(background = true)
    private String cmId;

    @Indexed(background = true)
    private SalesTeam team;

    public static class Constants {
        public static final String TEAM = "team";
        public static final String ZH = "zhId";
        public static final String RM = "rmId";
        public static final String CM = "cmId";
        public static final String CH = "centreHeadId";
        public static final String CENTRE_NAME = "centreName";

        public static final String _ID = "_id";
        public static final String CENTRE_HEAD_ID = "centreHeadId";
        public static final String LAST_UPDATED = "lastUpdated";
    }
}
