package com.vedantu.vedantudata.entities.salesconversion;

import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.persistence.Id;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Document(collection = "TargetInput")
public class TargetInput extends AbstractMongoStringIdEntity {

    private String rm;

    @Indexed(background = true)
    private String rmEmployeeCode;

    @Indexed(background = true)
    private String centreId;

    @Indexed(background = true)
    private Long date;

    private Integer sfCommited;

    public static class Constants {
        public static String RM = "rm";
        public static String RM_EMPLOYEE_CODE = "rmEmployeeCode";
        public static String CENTRE_ID = "centreId";
        public static String DATE = "date";
        public static String SF_COMMITED = "sfCommited";
    }
}
