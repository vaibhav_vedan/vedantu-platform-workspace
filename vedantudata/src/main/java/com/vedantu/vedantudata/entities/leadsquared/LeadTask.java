package com.vedantu.vedantudata.entities.leadsquared;

import com.google.gson.annotations.SerializedName;
import com.vedantu.util.StringUtils;
import com.vedantu.util.dbentities.mongo.AbstractMongoEntity;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import com.vedantu.vedantudata.utils.CommonUtils;

public class LeadTask extends AbstractMongoStringIdEntity {

	@SerializedName("TaskID")
	private String taskId;

	@SerializedName("Name")
	private String name;

	@SerializedName("Description")
	private String description;

	@SerializedName("DueDate")
	private String dueDate;

	@SerializedName("CreatedOn")
	private String taskCreatedOn;

	@SerializedName("OwnerID")
	private String ownerID;

	@SerializedName("OwnerName")
	private String ownerName;

	@SerializedName("CreatedBy")
	private String createdBy;

	@SerializedName("CreatedByName")
	private String taskCreatedByName;

	@SerializedName("Status")
	private String status;

	@SerializedName("Reminder")
	private String reminder;

	@SerializedName("RelatedEntity")
	private String relatedEntity;

	@SerializedName("RelatedEntityId")
	private String relatedEntityId;

	@SerializedName("CompletedOn")
	private String completedOn;

	private Long dueDateLong;
	private Long createdOnLong;
	private Long completedOnLong;

	public LeadTask() {
		super();
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getDueDate() {
		return dueDate;
	}

	public void setDueDate(String dueDate) {
		this.dueDate = dueDate;
	}

	public String getCreatedOn() {
		return taskCreatedOn;
	}

	public void setCreatedOn(String createdOn) {
		this.taskCreatedOn = createdOn;
	}

	public String getOwnerID() {
		return ownerID;
	}

	public void setOwnerID(String ownerID) {
		this.ownerID = ownerID;
	}

	public String getOwnerName() {
		return ownerName;
	}

	public void setOwnerName(String ownerName) {
		this.ownerName = ownerName;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getCreatedByName() {
		return taskCreatedByName;
	}

	public void setCreatedByName(String createdByName) {
		this.taskCreatedByName = createdByName;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getReminder() {
		return reminder;
	}

	public void setReminder(String reminder) {
		this.reminder = reminder;
	}

	public Long getDueDateLong() {
		return dueDateLong;
	}

	public void setDueDateLong(Long dueDateLong) {
		this.dueDateLong = dueDateLong;
	}

	public Long getCreatedOnLong() {
		return createdOnLong;
	}

	public void setCreatedOnLong(Long createdOnLong) {
		this.createdOnLong = createdOnLong;
	}

	public String getRelatedEntity() {
		return relatedEntity;
	}

	public void setRelatedEntity(String relatedEntity) {
		this.relatedEntity = relatedEntity;
	}

	public String getRelatedEntityId() {
		return relatedEntityId;
	}

	public void setRelatedEntityId(String relatedEntityId) {
		this.relatedEntityId = relatedEntityId;
	}

	public String getCompletedOn() {
		return completedOn;
	}

	public void setCompletedOn(String completedOn) {
		this.completedOn = completedOn;
	}

	public Long getCompletedOnLong() {
		return completedOnLong;
	}

	public void setCompletedOnLong(Long completedOnLong) {
		this.completedOnLong = completedOnLong;
	}

	public void updateTimings() {
		if (!StringUtils.isEmpty(this.dueDate)) {
			try {
				this.dueDateLong = CommonUtils.parseTime(this.dueDate);
			} catch (Exception ex) {

			}
		}
		if (!StringUtils.isEmpty(this.taskCreatedOn)) {
			try {
				this.createdOnLong = CommonUtils.parseTime(this.taskCreatedOn);
			} catch (Exception ex) {

			}
		}
		if (!StringUtils.isEmpty(this.completedOn)) {
			try {
				this.completedOnLong = CommonUtils.parseTime(this.completedOn);
			} catch (Exception ex) {

			}
		}
	}

	public static class Constants extends AbstractMongoEntity.Constants {
		public static final String TASK_ID = "taskId";
		public static final String RELATED_ENTITY_ID = "relatedEntityId";
	}

	@Override
	public String toString() {
		return "LeadTask [taskId=" + taskId + ", name=" + name + ", description=" + description + ", dueDate=" + dueDate
				+ ", createdOn=" + taskCreatedOn + ", ownerID=" + ownerID + ", ownerName=" + ownerName + ", createdBy="
				+ createdBy + ", createdByName=" + taskCreatedByName + ", status=" + status + ", reminder=" + reminder
				+ ", relatedEntity=" + relatedEntity + ", relatedEntityId=" + relatedEntityId + ", completedOn="
				+ completedOn + ", dueDateLong=" + dueDateLong + ", createdOnLong=" + createdOnLong
				+ ", completedOnLong=" + completedOnLong + ", toString()=" + super.toString() + "]";
	}
}
