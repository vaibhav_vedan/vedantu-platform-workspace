package com.vedantu.vedantudata.entities.salesconversion;

import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@ToString
@Document(collection = "ISMandaysInfo")
public class ISMandaysInfo extends AbstractMongoStringIdEntity {

    @Indexed(background = true)
    private String rmEmployeeId;

    private String rmName;

    @Indexed(background = true)
    private Long date;

    private Integer mandays;

    public static class Constants {
        public static String RM_EMPLOYEE_ID = "rmEmployeeId";
        public static String DATE = "date";
        public static String MANDAYS = "mandays";
    }
}
