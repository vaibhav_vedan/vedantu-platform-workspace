package com.vedantu.vedantudata.entities;

import com.google.gson.annotations.SerializedName;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import lombok.Data;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;


@Data
@Document(collection = "SlashRTCActivityDetails")
public class SlashRTCActivityDetails extends AbstractMongoStringIdEntity {

//   Sample JSON from SlashRTC
//   {"dispose":"10076","dispose_name":"Not Answered","first_dispose_id":"10072","first_dispose_name":"FOS DNP (Did not pick)",
//   "second_dispose_id":"10076","second_dispose_name":"Not Answered","third_dispose_id":"0","third_dispose_name":"","comment":"",
//   "date":"2020-04-25 15:48:14","customer_name":"","customer_email":"","lead_json":"","process_json":"","leadset_id":"1",
//   "leadset_name":"default","phone_number":"7982984105","did_number":"false","process":"93",
//   "process_name":"DelhiSouthAnkurTyagi","campaign":"22","campaign_name":"DelhiSouth","callback":"0000-00-00 00:00:00",
//   "agent":"1622","agent_username":"GarishmaArora","agent_name":"Garishma Arora","accesslevel":"4",
//   "cdrid":"c361b16b-93d9-4d3d-bf5f-18a1bffc833f","agent_talktime_sec":"0","mute_time":"0","hold_time":"0","transfer_time":"0",
//   "conference_time":"0","dispose_time":"495","in_queue_time":"0","ringing_time_sec":"0","mode_of_calling":"manual",
//   "disconnected_by":"AGENT","start_stamp":"2020-04-25 15:39:12"}

    @SerializedName("dispose")
    private String dispose;

    @Indexed(background = true)
    @SerializedName("dispose_name")
    private String disposeName;

    @SerializedName("first_dispose_id")
    private String firstDisposeId;

    @Indexed(background = true)
    @SerializedName("first_dispose_name")
    private String firstDisposeName;

    @SerializedName("second_dispose_id")
    private String secondDisposeId;

    @Indexed(background = true)
    @SerializedName("second_dispose_name")
    private String secondDisposeName;

    @SerializedName("third_dispose_id")
    private String thirdDisposeId;

    @SerializedName("third_dispose_name")
    private String thirdDisposeName;

    @SerializedName("comment")
    private String comment;

    @SerializedName("date")
    private Long date;

    private String incomingDate;

    @SerializedName("customer_name")
    private String customeName;

    @SerializedName("customer_email")
    private String customerEmail;

    @SerializedName("lead_json")
    private String leadJson;

    @SerializedName("process_json")
    private String processJson;

    @SerializedName("leadset_id")
    private String leadsetId;

    @SerializedName("leadset_name")
    private String leadsetName;

    @SerializedName("phone_number")
    private String phoneNumber;

    @SerializedName("did_number")
    private String didNumber;

    @SerializedName("process")
    private String process;

    @Indexed(background = true)
    @SerializedName("process_name")
    private String processName;

    @SerializedName("campaign")
    private String campaign;

    @Indexed(background = true)
    @SerializedName("campaign_name")
    private String campaignName;

    @SerializedName("callback")
    private String callback;

    @SerializedName("agent")
    private String agent;

    @Indexed(background = true)
    @SerializedName("agent_username")
    private String agentUsername;

    @SerializedName("agent_name")
    private String agentName;

    @SerializedName("accesslevel")
    private String accessLevel;

    @Indexed(background = true, unique = true)
    @SerializedName("cdrid")
    private String cdrid;

    @SerializedName("ResourceURL")
    private String resourceURL;

    @SerializedName("agent_talktime_sec")
    private String agentTalktimeSec;

    @SerializedName("mute_time")
    private String muteTime;

    @SerializedName("hold_time")
    private String holdTime;

    @SerializedName("transfer_time")
    private String transferTime;

    @SerializedName("conference_time")
    private String conferenceTime;

    @SerializedName("dispose_time")
    private String disposeTime;

    @SerializedName("in_queue_time")
    private String inQueueTime;

    @SerializedName("ringing_time_sec")
    private String ringingTimeSec;

    @SerializedName("mode_of_calling")
    private String modeOfCalling;

    @SerializedName("disconnected_by")
    private String disconnectedBy;

    @Indexed(background = true)
    @SerializedName("start_stamp")
    private Long startStamp;

    @SerializedName("Status")
    private String status;

    @Indexed(background = true)
    private String employeeId;

    @Indexed(background = true)
    private String employeeEmail;

    @Indexed(background = true)
    @SerializedName("HangupCause")
    private String hangupCause;

    public static class Constants {
        public static String CDRID = "cdrid";
        public static String START_TIME = "startStamp";

    }

}
