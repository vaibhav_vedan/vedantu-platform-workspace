package com.vedantu.vedantudata.entities;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONObject;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import com.vedantu.util.dbentities.mongo.AbstractMongoEntity;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import com.vedantu.vedantudata.enums.WebinarUserTag;
import com.vedantu.vedantudata.pojos.GTWAttendenceInterval;
import com.vedantu.vedantudata.pojos.GTWSessionAttendeeInfo;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "WebinarUserRegistrationInfo")
public class WebinarUserRegistrationInfo extends AbstractMongoStringIdEntity {
	/*
	 * {"responses":[{"questionKey":"45815183","responseText":""},{"questionKey":
	 * "45815184","responseText":""}, {"questionKey":"45815185","responseText":""}],
	 * "firstName":"Shyam","lastName":"krishna","email":"shyam.krishna@vedantu.com",
	 * "timeZone":"Asia/Kolkata"}
	 */

	/*
	 * firstName, lastName, emailId, phone, utm_source, utm_campaign, utm_medium
	 * 
	 */
	private String firstName;
	private String lastName;
	private String emailId;
	private String phone;
	private String utm_source;
	private String utm_campaign;
	private String utm_medium;
        
        @Indexed(background = true)
	private String trainingId;
        
        private String webinarId;
	private String timeZone = "Asia/Kolkata";
	private String registerJoinUrl;
	private String registerRegistrantKey;
	private String registerStatus;
	private List<String> responses;

	private Integer timeInSession;
	private List<GTWAttendenceInterval> attendanceIntervals;

	private List<WebinarUserTag> tags = new ArrayList<>();

	public WebinarUserRegistrationInfo() {
		super();
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getTimeZone() {
		return timeZone;
	}

	public void setTimeZone(String timeZone) {
		this.timeZone = timeZone;
	}

	public List<String> getResponses() {
		return responses;
	}

	public void setResponses(List<String> responses) {
		this.responses = responses;
	}

	public String getTrainingId() {
		return trainingId;
	}

	public void setTrainingId(String trainingId) {
		this.trainingId = trainingId;
	}

	public String getRegisterJoinUrl() {
		return registerJoinUrl;
	}

	public void setRegisterJoinUrl(String registerJoinUrl) {
		this.registerJoinUrl = registerJoinUrl;
	}

	public String getRegisterRegistrantKey() {
		return registerRegistrantKey;
	}

	public void setRegisterRegistrantKey(String registerRegistrantKey) {
		this.registerRegistrantKey = registerRegistrantKey;
	}

	public String getRegisterStatus() {
		return registerStatus;
	}

	public void setRegisterStatus(String registerStatus) {
		this.registerStatus = registerStatus;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getUtm_source() {
		return utm_source;
	}

	public void setUtm_source(String utm_source) {
		this.utm_source = utm_source;
	}

	public String getUtm_campaign() {
		return utm_campaign;
	}

	public void setUtm_campaign(String utm_campaign) {
		this.utm_campaign = utm_campaign;
	}

	public String getUtm_medium() {
		return utm_medium;
	}

	public void setUtm_medium(String utm_medium) {
		this.utm_medium = utm_medium;
	}

	public List<WebinarUserTag> getTags() {
		return tags;
	}

	public void setTags(List<WebinarUserTag> tags) {
		this.tags = tags;
	}

	public Integer getTimeInSession() {
		return timeInSession;
	}

	public void setTimeInSession(Integer timeInSession) {
		this.timeInSession = timeInSession;
	}

	public List<GTWAttendenceInterval> getAttendanceIntervals() {
		return attendanceIntervals;
	}

	public void setAttendanceIntervals(List<GTWAttendenceInterval> attendanceIntervals) {
		this.attendanceIntervals = attendanceIntervals;
	}

	public void addTag(WebinarUserTag webinarUserTag) {
		if (webinarUserTag != null) {
			if (this.tags == null) {
				this.tags = new ArrayList<>();
			}
			this.tags.add(webinarUserTag);
		}
	}

	public boolean containsTag(WebinarUserTag webinarUserTag) {
		return !CollectionUtils.isEmpty(this.tags) && this.tags.contains(webinarUserTag);
	}

	public void updateRegisterResponse(JSONObject jsonObject) {
		/*
		 * { "registrantKey": "7637606173704895757", "joinUrl":
		 * "https://global.gotowebinar.com/join/150586194716221441/785351865", "status":
		 * "APPROVED" }
		 */
		if (jsonObject != null) {
			if (jsonObject.has("registrantKey")) {
				this.registerRegistrantKey = jsonObject.getString("registrantKey");
			}
			if (jsonObject.has("joinUrl")) {
				this.registerJoinUrl = jsonObject.getString("joinUrl");
			}
			if (jsonObject.has("status")) {
				this.registerStatus = jsonObject.getString("status");
			}
		}
	}

	public void updateRegisterResponse(WebinarUserRegistrationInfo webinarUserRegistrationInfo) {
		/*
		 * { "registrantKey": "7637606173704895757", "joinUrl":
		 * "https://global.gotowebinar.com/join/150586194716221441/785351865", "status":
		 * "APPROVED" }
		 */
		if (webinarUserRegistrationInfo != null) {
			if (!StringUtils.isEmpty(webinarUserRegistrationInfo.getRegisterJoinUrl())) {
				this.registerJoinUrl = webinarUserRegistrationInfo.getRegisterJoinUrl();
			}
			if (!StringUtils.isEmpty(webinarUserRegistrationInfo.getRegisterRegistrantKey())) {
				this.registerRegistrantKey = webinarUserRegistrationInfo.getRegisterRegistrantKey();
			}
			if (!StringUtils.isEmpty(webinarUserRegistrationInfo.getRegisterStatus())) {
				this.registerStatus = webinarUserRegistrationInfo.getRegisterStatus();
			}
		}
	}

	public void updateAttendance(List<GTWSessionAttendeeInfo> attendanceInfos) {
		int timeInsession = 0;
		List<GTWAttendenceInterval> attendanceIntervals = new ArrayList<>();
		if (!CollectionUtils.isEmpty(attendanceInfos)) {
			for (GTWSessionAttendeeInfo entry : attendanceInfos) {
				if (entry.getAttendanceTimeInSeconds() != null) {
					timeInsession += entry.getAttendanceTimeInSeconds();
				}
				if (!CollectionUtils.isEmpty(entry.getAttendance())) {
					attendanceIntervals.addAll(entry.getAttendance());
				}
			}

			this.timeInSession = timeInsession;
			this.attendanceIntervals = attendanceIntervals;
		}
	}

        public String getWebinarId() {
            return webinarId;
        }

        public void setWebinarId(String webinarId) {
            this.webinarId = webinarId;
        }

	public static class Constants extends AbstractMongoEntity.Constants {
		public static final String TRAINING_ID = "trainingId";
		public static final String EMAIL_ID = "emailId";
	}

	@Override
	public String toString() {
		return "WebinarUserRegistrationInfo [firstName=" + firstName + ", lastName=" + lastName + ", emailId=" + emailId
				+ ", phone=" + phone + ", utm_source=" + utm_source + ", utm_campaign=" + utm_campaign + ", utm_medium="
				+ utm_medium + ", trainingId=" + trainingId + ", timeZone=" + timeZone + ", registerJoinUrl="
				+ registerJoinUrl + ", registerRegistrantKey=" + registerRegistrantKey + ", registerStatus="
				+ registerStatus + ", responses=" + responses + ", tags=" + tags + ", toString()=" + super.toString()
				+ "]";
	}
}
