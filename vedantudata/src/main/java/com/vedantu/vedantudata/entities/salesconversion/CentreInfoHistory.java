package com.vedantu.vedantudata.entities.salesconversion;

import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import com.vedantu.vedantudata.enums.salesconversion.SalesTeam;
import lombok.Data;
import lombok.ToString;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@ToString
@Document(collection = "CentreInfoHistory")
public class CentreInfoHistory extends AbstractMongoStringIdEntity {

    @Indexed(background = true)
    private String centreName;
    private String centreHeadId;
    @Indexed(background = true)
    private String zhId;

    @Indexed(background = true)
    private String rmId;

    @Indexed(background = true)
    private String cmId;
    @Indexed(background = true)
    private SalesTeam team;

    public static class Constants {
        public static final String TEAM = "team";
        public static String CENTRE_NAME = "centreName";

    }
}
