package com.vedantu.vedantudata.entities.leadsquared;

import java.util.List;

import com.google.gson.annotations.SerializedName;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import com.vedantu.vedantudata.enums.LeadsquaredAgentBucket;

public class LeadsquaredUser extends AbstractMongoStringIdEntity {
	/*
	 * { "ID": "9336bf715a0711e696be22000aa8e760", "FirstName": "Akshay",
	 * "LastName": "Kumar", "EmailAddress": "akshay.kumar@vedantu.com", "Role":
	 * "Sales_User", "StatusCode": 0, "Tag": null },
	 */

	@SerializedName("ID")
	private String leadsquaredId;

	@SerializedName("FirstName")
	private String firstName;

	@SerializedName("LastName")
	private String lastName;

	@SerializedName("EmailAddress")
	private String email;

	@SerializedName("Role")
	private String role;

	@SerializedName("StatusCode")
	private String active;

	@SerializedName("UserId")
	private String userId;

	@SerializedName("AssociatedPhoneNumbers")
	private String associatedPhoneNumbers;

	private String convoxAgentId;

	private List<LeadsquaredAgentBucket> bucketName;

	public LeadsquaredUser() {
		super();
	}

	public String getLeadsquaredId() {
		return leadsquaredId;
	}

	public void setLeadsquaredId(String leadsquaredId) {
		this.leadsquaredId = leadsquaredId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public String getActive() {
		return active;
	}

	public void setActive(String active) {
		this.active = active;
	}

	public List<LeadsquaredAgentBucket> getBucketName() {
		return bucketName;
	}

	public void setBucketName(List<LeadsquaredAgentBucket> bucketName) {
		this.bucketName = bucketName;
	}

	public String getConvoxAgentId() {
		return convoxAgentId;
	}

	public void setConvoxAgentId(String convoxAgentId) {
		this.convoxAgentId = convoxAgentId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getAssociatedPhoneNumbers() {
		return associatedPhoneNumbers;
	}

	public void setAssociatedPhoneNumbers(String associatedPhoneNumbers) {
		this.associatedPhoneNumbers = associatedPhoneNumbers;
	}

	public static class Constants extends AbstractMongoStringIdEntity.Constants {
		public static final String LEAD_SQUARED_USER_ID = "leadsquaredId";
		public static final String EMAIL = "email";
		public static final String AGENT_ID = "convoxAgentId";
	}

	@Override
	public String toString() {
		return "LeadsquaredUser [leadsquaredId=" + leadsquaredId + ", firstName=" + firstName + ", lastName=" + lastName
				+ ", email=" + email + ", role=" + role + ", active=" + active + ", convoxAgentId=" + convoxAgentId
				+ ", bucketName=" + bucketName + ", toString()=" + super.toString() + "]";
	}
}
