package com.vedantu.vedantudata.entities;

import java.util.List;

import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;

public class UserIdData extends AbstractMongoStringIdEntity {
	private String userDataId;
	private List<String> leadsquaredIds;

	public UserIdData() {
		super();
	}

	public String getUserDataId() {
		return userDataId;
	}

	public void setUserDataId(String userDataId) {
		this.userDataId = userDataId;
	}

	public List<String> getLeadsquaredIds() {
		return leadsquaredIds;
	}

	public void setLeadsquaredIds(List<String> leadsquaredIds) {
		this.leadsquaredIds = leadsquaredIds;
	}
}
