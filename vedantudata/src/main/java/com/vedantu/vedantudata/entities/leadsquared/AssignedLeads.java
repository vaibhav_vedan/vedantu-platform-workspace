package com.vedantu.vedantudata.entities.leadsquared;

import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import lombok.Data;
import lombok.ToString;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

@Data
@ToString
@Document(collection = "AssignedLeads")
public class AssignedLeads extends AbstractMongoStringIdEntity {

    private String phone;
    private String grade;
    private String firstName;
    private String mx_VC_code;
    private String ownerId;
    private String mx_vc_lead;
    private String mx_Sub_Stage;
    private String prospectStage;
    private String mx_Last_Assigned_Date;
    private String mx_Assigned_Count;
    private String userId;
    private String proseprctid;
    private String mx_Location_type;
    private String mx_city;
    private String mx_User_Target;
    private String mx_Core_Noncore;
    private String mx_Week;
    private String mx_CC_LT;
    private String mx_Board;
    private String isFreshLead;
    private String leadScore;
    private String assignmentType;
    private String mxScore;
    private String scoreId;
    private String langugage;
    private String category;
    private Date lastUpdatedDate;
    private String location;
}
