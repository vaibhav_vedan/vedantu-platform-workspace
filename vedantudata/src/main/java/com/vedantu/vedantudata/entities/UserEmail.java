package com.vedantu.vedantudata.entities;

import java.util.List;

import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import com.vedantu.vedantudata.pojos.UserEmailStatus;
import com.vedantu.vedantudata.pojos.UserEntityOwner;

public class UserEmail extends AbstractMongoStringIdEntity {
	private String emailId;
	private UserEntityOwner owner;
	private List<String> leadSquaredIds;
	private List<String> ids;
	private UserEmailStatus emailStatus;

	public UserEmail() {
		super();
		// TODO Auto-generated constructor stub
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public UserEntityOwner getOwner() {
		return owner;
	}

	public void setOwner(UserEntityOwner owner) {
		this.owner = owner;
	}

	public List<String> getLeadSquaredIds() {
		return leadSquaredIds;
	}

	public void setLeadSquaredIds(List<String> leadSquaredIds) {
		this.leadSquaredIds = leadSquaredIds;
	}

	public List<String> getIds() {
		return ids;
	}

	public void setIds(List<String> ids) {
		this.ids = ids;
	}

	public UserEmailStatus getEmailStatus() {
		return emailStatus;
	}

	public void setEmailStatus(UserEmailStatus emailStatus) {
		this.emailStatus = emailStatus;
	}

	@Override
	public String toString() {
		return "UserEmail [emailId=" + emailId + ", owner=" + owner + ", leadSquaredIds=" + leadSquaredIds + ", ids="
				+ ids + ", toString()=" + super.toString() + "]";
	}
}
