package com.vedantu.vedantudata.entities;

import com.google.gson.annotations.SerializedName;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import com.vedantu.vedantudata.pojos.ActivityOwnerChangeHistory;
import com.vedantu.vedantudata.pojos.ActivityStatusChangeHistory;
import com.vedantu.vedantudata.pojos.LeadsquaredActivityField;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

@Getter
@Setter
@Document(collection = "ActivityDetails")
public class ActivityDetails extends AbstractMongoStringIdEntity {

    @SerializedName("ProspectActivityId")
    private String prospectActivityId;

    @Indexed(background = true)
    @SerializedName("SearchByValue")
    private String searchByValue;

    @Indexed(background = true)
    @SerializedName("Phone")
    private String phone;

    @Indexed(background = true)
    @SerializedName("EmailAddress")
    private String emailAddress;

    @SerializedName("ActivityOwnerEmail")
    private String activityOwnerEmail;

    @SerializedName("RelatedProspectId")
    private String relatedProspectId;

    @SerializedName("ActivityEvent")
    private Integer activityEvent;

    @SerializedName("ActivityNote")
    private String activityNote;

    @SerializedName("ActivityDateTime")
    private String activityDateTime;

    @SerializedName("ModifiedBy")
    private String modifiedBy;

    @SerializedName("ModifiedByName")
    private String modifiedByName;

    @SerializedName("CreatedBy")
    private String CreatedBy;

    @SerializedName("CreatedByName")
    private String createdByName;

    @Indexed(background = true)
    private Long leadVedantuId;
    @Indexed(background = true)
    private Long ownerVedantuId;

    @SerializedName("Fields")
    private List<LeadsquaredActivityField> fields;

    private List<ActivityStatusChangeHistory> statusChangeHistory;
    private List<ActivityOwnerChangeHistory> ownerChangeHisory;

    @Indexed(background = true)
    private Long freshdeskTicketId;
    private Long freshdeskStatus;

    public void addField(LeadsquaredActivityField leadsquaredActivityField) {

        if (fields == null) {
            fields = new ArrayList<>();
        }

        fields.add(leadsquaredActivityField);
    }

    public void addFields(List<LeadsquaredActivityField> leadsquaredActivityFields) {

        if (fields == null) {
            fields = new ArrayList<>();
        }

        fields.addAll(leadsquaredActivityFields);
    }

    public boolean sameAs(ActivityDetails that) {
        if (this == that) {
            return true;
        }
        if (that == null || getClass() != that.getClass()) {
            return false;
        }

        /*if (!Objects.equals(prospectActivityId, that.prospectActivityId)) {
            return false;
        }*/
        if (!Objects.equals(emailAddress, that.emailAddress)) {
            return false;
        }
        if (!Objects.equals(activityOwnerEmail, that.activityOwnerEmail)) {
            return false;
        }
        /*if (!Objects.equals(relatedProspectId, that.relatedProspectId)) {
            return false;
        }*/
        if (!Objects.equals(activityEvent, that.activityEvent)) {
            return false;
        }
        if (!Objects.equals(activityNote, that.activityNote)) {
            return false;
        }
        /*if (!Objects.equals(activityDateTime, that.activityDateTime)) {
            return false;
        }
        if (!Objects.equals(modifiedBy, that.modifiedBy)) {
            return false;
        }
        if (!Objects.equals(modifiedByName, that.modifiedByName)) {
            return false;
        }
        if (!Objects.equals(CreatedBy, that.CreatedBy)) {
            return false;
        }
        if (!Objects.equals(createdByName, that.createdByName)) {
            return false;
        }*/
        if (!Objects.equals(leadVedantuId, that.leadVedantuId)) {
            return false;
        }
        if (!Objects.equals(ownerVedantuId, that.ownerVedantuId)) {
            return false;
        }
        /*if (!Objects.equals(statusChangeHistory, that.statusChangeHistory)) {
            return false;
        }
        if (!Objects.equals(ownerChangeHisory, that.ownerChangeHisory)) {
            return false;
        }*/
        if (!Objects.equals(freshdeskTicketId, that.freshdeskTicketId)) {
            return false;
        }
        if ( !Objects.equals(freshdeskStatus, that.freshdeskStatus)) {
            return false;
        }
        if (fields != null && that.fields != null) {
            Map<String, String> map = fields.stream().collect(Collectors.toMap(LeadsquaredActivityField::getSchemaName, LeadsquaredActivityField::getValue));
            for (LeadsquaredActivityField field : that.fields) {
                String thisValue = map.get(field.getSchemaName());
                String thatValue = field.getValue();
                if (!Objects.equals(thisValue, thatValue)) {
                    return false;
                }
            }
        } else {
            return that.fields == null;
        }
        return true;
    }


    public static class Constants {
        public static String PROSPET_ACTIVITY_ID = "prospectActivityId";
        public static String LEAD_VEDANTU_ID = "leadVedantuId";
        public static String LEAD_EMAIL_ADDRESS = "emailAddress";
        public static String ACTIVITY_EVENT = "activityEvent";
        public static String CREATION_TIME = "creationTime";
        public static String FRESHDESK_TICKET_ID = "freshdeskTicketId";
        public static String ACTIVITY_OWNER_EMAIL = "activityOwnerEmail";
        public static String ACTIVITY_FIELDS_STATUS = "fields.0.Value";
        public static String ACTIVITY_ACTIVITY_NOTE = "activityNote";

    }

}
