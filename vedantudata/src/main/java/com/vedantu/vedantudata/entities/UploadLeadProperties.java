package com.vedantu.vedantudata.entities;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class UploadLeadProperties {
	private long totalLeads;
	private long leadsPushed;
	private long leadsUpdated;
	private Set<String> updatedLeadIds = new HashSet<String>();
	private List<String> duplicateIds = new ArrayList<>();
	private List<UploadLeadStatus> leadStatus = new ArrayList<>();
	private List<String> errorRows = new ArrayList<>();

	public UploadLeadProperties() {
		super();
	}

	public long getTotalLeads() {
		return totalLeads;
	}

	public void setTotalLeads(long totalLeads) {
		this.totalLeads = totalLeads;
	}

	public long getLeadsPushed() {
		return leadsPushed;
	}

	public void setLeadsPushed(long leadsPushed) {
		this.leadsPushed = leadsPushed;
	}

	public long getLeadsUpdated() {
		return leadsUpdated;
	}

	public void setLeadsUpdated(long leadsUpdated) {
		this.leadsUpdated = leadsUpdated;
	}

	public List<String> getDuplicateIds() {
		return duplicateIds;
	}

	public List<String> getErrorRows() {
		return errorRows;
	}

	public void setErrorRows(List<String> errorRows) {
		this.errorRows = errorRows;
	}

	public void addErrorRows(String errorRow) {
		this.errorRows.add(errorRow);
	}

	public Set<String> getUpdatedLeadIds() {
		return updatedLeadIds;
	}

	public void addUploadLeadStatus(String leadId, Boolean leadCreated, Boolean leadUpdated) {
		this.leadStatus.add(new UploadLeadStatus(leadId, leadCreated, leadUpdated));
		if (Boolean.TRUE.equals(leadCreated)) {
			this.leadsPushed++;
		} else if (Boolean.TRUE.equals(leadUpdated)) {
			this.leadsUpdated++;
		}
		this.addUpdatedLeadId(leadId);
	}

	private void addUpdatedLeadId(String leadId) {
		if (updatedLeadIds.contains(leadId)) {
			this.duplicateIds.add(leadId);
		} else {
			updatedLeadIds.add(leadId);
		}
	}

	public List<UploadLeadStatus> getLeadStatus() {
		return leadStatus;
	}

	@Override
	public String toString() {
		return "UploadLeadProperties [totalLeads=" + totalLeads + ", leadsPushed=" + leadsPushed + ", leadsUpdated="
				+ leadsUpdated + ", duplicateIds=" + duplicateIds + ", errorRows=" + errorRows + ", toString()="
				+ super.toString() + "]";
	}
}
