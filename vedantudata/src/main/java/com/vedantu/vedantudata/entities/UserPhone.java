package com.vedantu.vedantudata.entities;

import java.util.List;

import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import com.vedantu.vedantudata.pojos.UserEntityOwner;
import com.vedantu.vedantudata.pojos.UserPhoneStatus;

public class UserPhone extends AbstractMongoStringIdEntity {
	private String phoneNumber;
	private UserEntityOwner userPhoneOwner;
	private List<String> leadSquaredIds;
	private List<String> ids;
	private UserPhoneStatus userPhoneStatus;

	// Need to store phone status

	public UserPhone() {
		super();
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public List<String> getLeadSquaredIds() {
		return leadSquaredIds;
	}

	public void setLeadSquaredIds(List<String> leadSquaredIds) {
		this.leadSquaredIds = leadSquaredIds;
	}

	public List<String> getIds() {
		return ids;
	}

	public void setIds(List<String> ids) {
		this.ids = ids;
	}

	public UserPhoneStatus getUserPhoneStatus() {
		return userPhoneStatus;
	}

	public void setUserPhoneStatus(UserPhoneStatus userPhoneStatus) {
		this.userPhoneStatus = userPhoneStatus;
	}

	public UserEntityOwner getUserPhoneOwner() {
		return userPhoneOwner;
	}

	public void setUserPhoneOwner(UserEntityOwner userPhoneOwner) {
		this.userPhoneOwner = userPhoneOwner;
	}

	@Override
	public String toString() {
		return "UserPhone [phoneNumber=" + phoneNumber + ", userPhoneOwner=" + userPhoneOwner + ", leadSquaredIds="
				+ leadSquaredIds + ", ids=" + ids + ", userPhoneStatus=" + userPhoneStatus + ", toString()="
				+ super.toString() + "]";
	}
}
