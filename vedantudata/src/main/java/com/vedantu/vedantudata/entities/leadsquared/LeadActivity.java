package com.vedantu.vedantudata.entities.leadsquared;

import java.util.List;

import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import com.google.gson.annotations.SerializedName;
import com.vedantu.util.dbentities.mongo.AbstractMongoEntity;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import com.vedantu.vedantudata.pojos.KeyValueObject;

@Document(collection = "LeadActivity")
@CompoundIndexes({
    @CompoundIndex(name = "lead_activity_event_code", def = "{'relatedProspectId' : 1, 'eventCode' : 1}"),
})
public class LeadActivity extends AbstractMongoStringIdEntity {
	// {
	// "Id": "36c6337a-086e-11e7-b5f1-22000aa8e760",
	// "EventCode": 0,
	// "EventName": "Email Opened",
	// "ActivityScore": 3,
	// "CreatedOn": "2017-03-14 04:25:11",
	// "ActivityType": 0,
	// "IsEmailType": true,
	// "Type": "Inbound",
	// "EmailType": "Quick Email",
	// "SessionId": "36c608b8-086e-11e7-b5f1-22000aa8e760",
	// "TrackLocation": 0,
	// "Latitude": 0,
	// "Longitude": 0,
	// "RelatedProspectId": "337bc48a-c549-40b5-8604-c6e70ebb44d0",
	// "Data": [
	// {
	// "Key": "Subject",
	// "Value": "Welcome! Need help getting started?"
	// },
	// {
	// "Key": "CampaignActivityRecordId",
	// "Value": "aec9cde1-b9f1-4da8-b60b-996905185afa"
	// }
	// ]
	// }

	@SerializedName("Id")
	private String activityId;

	@SerializedName("EventCode")
	@Indexed(background = true)
	private String eventCode;

	@SerializedName("EventName")
	private String eventName;

	@SerializedName("ActivityScore")
	private String activityScore;

	@SerializedName("CreatedOn")
	private String createdOn;

	@SerializedName("ActivityType")
	private String activityType;

	@SerializedName("IsEmailType")
	private String checkEmailType;

	@SerializedName("Type")
	private String type;

	@SerializedName("EmailType")
	private String emailType;

	@SerializedName("RelatedProspectId")
	@Indexed(background = true)
	private String relatedProspectId;

	@SerializedName("Data")
	private List<KeyValueObject> data;

	public LeadActivity() {
		super();
	}

	public String getActivityId() {
		return activityId;
	}

	public void setActivityId(String activityId) {
		this.activityId = activityId;
	}

	public String getEventName() {
		return eventName;
	}

	public void setEventName(String eventName) {
		this.eventName = eventName;
	}

	public String getCheckEmailType() {
		return checkEmailType;
	}

	public void setCheckEmailType(String checkEmailType) {
		this.checkEmailType = checkEmailType;
	}

	public String getEventCode() {
		return eventCode;
	}

	public void setEventCode(String eventCode) {
		this.eventCode = eventCode;
	}

	public String getActivityScore() {
		return activityScore;
	}

	public void setActivityScore(String activityScore) {
		this.activityScore = activityScore;
	}

	public String getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(String createdOn) {
		this.createdOn = createdOn;
	}

	public String getActivityType() {
		return activityType;
	}

	public void setActivityType(String activityType) {
		this.activityType = activityType;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getEmailType() {
		return emailType;
	}

	public void setEmailType(String emailType) {
		this.emailType = emailType;
	}

	public String getRelatedProspectId() {
		return relatedProspectId;
	}

	public void setRelatedProspectId(String relatedProspectId) {
		this.relatedProspectId = relatedProspectId;
	}

	public List<KeyValueObject> getData() {
		return data;
	}

	public void setData(List<KeyValueObject> data) {
		this.data = data;
	}

	public static class Constants extends AbstractMongoEntity.Constants {
		public static final String ACTIVITY_ID = "activityId";
		public static final String LEAD_ID = "relatedProspectId";
		public static final String EVENT_CODE = "eventCode";
	}

	@Override
	public String toString() {
		return "LeadActivity [activityId=" + activityId + ", eventCode=" + eventCode + ", eventName=" + eventName
				+ ", activityScore=" + activityScore + ", createdOn=" + createdOn + ", activityType=" + activityType
				+ ", checkEmailType=" + checkEmailType + ", type=" + type + ", emailType=" + emailType
				+ ", relatedProspectId=" + relatedProspectId + ", data=" + data + ", toString()=" + super.toString()
				+ "]";
	}
}
