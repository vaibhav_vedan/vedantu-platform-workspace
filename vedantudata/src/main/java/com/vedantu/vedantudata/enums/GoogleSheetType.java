package com.vedantu.vedantudata.enums;

public enum GoogleSheetType {
	VEDANTU_DATA, ORDERS, AGENT_REPORT, UPLOAD_IDENTIFIER, GTT_ATTENDEE, WEBINAR_REPORT
}
