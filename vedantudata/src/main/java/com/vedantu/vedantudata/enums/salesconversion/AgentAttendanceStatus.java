package com.vedantu.vedantudata.enums.salesconversion;


public enum AgentAttendanceStatus {
    WO("Week Off"),
    P("Present"),
    SB("Study Breaks"),
    HDL("Half-day Leave"),
    UL("Uninformed Leave"),
    AL("Approved Leave"),
    SL("Sick Leave"),
    R("Resigned"),
    AB("Absconding"),
    DP("Dropout"),
    LOP("Loss of pay"),
    ML("Maternity Leave"),
    PL("Paternity Leave");

    public String value;

    AgentAttendanceStatus(String value) {
        this.value = value;
    }
}
