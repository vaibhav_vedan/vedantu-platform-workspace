package com.vedantu.vedantudata.enums.salesconversion;

public enum AgentDesignation {

    INTERN,
    ACADEMIC_COUNSELLOR,
    SENIOR_ACADEMIC_COUNSELLOR,
    TELE_CALLER,
    TEAM_LEADER,
    SENIOR_TEAM_LEADER,
    OPES,
    CENTER_HEAD,
    ZONAL_HEAD,
    CLUSTER_MANAGER,
    FLOOR_MANAGER,
    TRAINING_QUALITY,
    REVENUE_MANAGER,
    REGIONAL_MANAGER;
}
