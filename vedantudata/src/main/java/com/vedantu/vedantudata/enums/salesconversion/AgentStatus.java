package com.vedantu.vedantudata.enums.salesconversion;

public enum AgentStatus {

    ACTIVE,
    INACTIVE,
    RESIGNED,
    TRAINING,
    DROPOUT,
    ABSCONDING,
    TERMINATED,
    LONG_LEAVE,
    ON_JOB_TRAINING,
    PIP_WARNING_GIVEN;
}
