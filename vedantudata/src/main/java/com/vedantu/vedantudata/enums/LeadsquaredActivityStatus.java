package com.vedantu.vedantudata.enums;

import javax.validation.constraints.NotNull;

public enum LeadsquaredActivityStatus {
    Active("Active"),
    Paid("Paid"),
    Open("Open"),
    Pending("Pending"),
    Resolved("Resolved"),
    Closed("Closed"),
    WaitingOnCustomer("Waiting on Customer"),
    WaitingOnInternalTeam("Waiting on Internal Team"),
    Inactive("Inactive"),

    ;
    private String value;

    LeadsquaredActivityStatus(@NotNull String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public static LeadsquaredActivityStatus getFreshDeskActivityStatus(Long status) {
        if (status == 2L) {
            return Open;
        } else if (status == 3L) {
            return Pending;
        } else if (status == 4L) {
            return Resolved;
        } else if (status == 5L) {
            return Closed;
        } else if (status == 6L) {
            return WaitingOnCustomer;
        } else if (status == 7L) {
            return WaitingOnInternalTeam;
        } else {
            return Pending;
        }
    }

}
