package com.vedantu.vedantudata.enums;

public enum UserEventType {
	LEAD_SQUARED, USER_PROFILE, GUPSHUP, MANDRILL, CONVOX, VEDANTU
}
