package com.vedantu.vedantudata.enums.salesconversion;

import java.util.Arrays;

public enum AgentCohort {

    M1(0, 30),
    M2(30, 60),
    M3(60, 90),
    M4(90, 120),
    M5(120, 150),
    M5Plus(150);//Default case if grater than 150


    private int minDays;
    private int maxDays;
    private int upperBoundDays;

    private AgentCohort(int min, int max) {
        this.minDays = min;
        this.maxDays = max;
    }

    private AgentCohort(int max) {
        this.upperBoundDays = max;
    }

    public static AgentCohort from(int noOfdays) {
        return Arrays.stream(AgentCohort.values())
                .filter(range -> noOfdays >= range.minDays && noOfdays < range.maxDays)
                .findAny()
                .orElse(M5Plus);
    }
}
