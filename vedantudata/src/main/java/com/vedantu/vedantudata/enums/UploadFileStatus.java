package com.vedantu.vedantudata.enums;

public enum UploadFileStatus {
	PROCESSING, PROCESSED, SUCCESSFUL, FAILED
}
