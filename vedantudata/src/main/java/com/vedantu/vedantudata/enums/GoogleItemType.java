package com.vedantu.vedantudata.enums;

public enum GoogleItemType {
	SPREADSHEET, FOLDER
}
