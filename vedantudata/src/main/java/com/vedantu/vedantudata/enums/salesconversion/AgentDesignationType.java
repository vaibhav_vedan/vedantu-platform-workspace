package com.vedantu.vedantudata.enums.salesconversion;

public enum AgentDesignationType {

    AC,
    TL,
    CH,
    OPS,
    RM,
    TC,
    CM,
    ZH,
    FM,
    TQ;

}
