package com.vedantu.vedantudata.controllers;

import com.vedantu.util.LogFactory;
import com.vedantu.util.PlatformBasicResponse;
import com.vedantu.vedantudata.managers.MongoIndexingManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/mongoIndexing")
public class MongoIndexingController {
    @Autowired
    private LogFactory logFactory;

    @Autowired
    MongoIndexingManager mongoIndexingManager;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(MongoIndexingController.class);

    @RequestMapping(value = "/createIndexWithTTL", method = RequestMethod.GET)
    @ResponseBody
    public PlatformBasicResponse createIndexWithTTL(@RequestParam(name = "entityName") String entityName, @RequestParam(name = "fieldName") String fieldName, @RequestParam(name = "expireTimeInSeconds") Long expireTimeInSeconds) {
        return mongoIndexingManager.createIndexWithTTL(entityName, fieldName, expireTimeInSeconds);
    }

    @RequestMapping(value = "/droppingIndex", method = RequestMethod.GET)
    @ResponseBody
    public PlatformBasicResponse droppingIndex(@RequestParam(name = "entityName") String entityName, @RequestParam(name = "fieldName") String fieldName) {
        return mongoIndexingManager.droppingIndex(entityName,fieldName);
    }

    @RequestMapping(value = "/createIndexWithTTLFromCron", method = RequestMethod.POST, consumes = MediaType.TEXT_PLAIN_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void createIndexWithTTLFromCron(@RequestHeader(value = "x-amz-sns-message-type") String messageType, @RequestBody String request, @RequestParam(name = "entityName") String entityName, @RequestParam(name = "fieldName") String fieldName, @RequestParam(name = "expireTimeInSeconds") Long expireTimeInSeconds) throws Throwable {
        logger.info(request);
        mongoIndexingManager.createIndexWithTTLFromCron(request,messageType,entityName, fieldName, expireTimeInSeconds);
    }

    @RequestMapping(value = "/droppingIndexFromCron", method = RequestMethod.POST, consumes = MediaType.TEXT_PLAIN_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void droppingIndexFromCron(@RequestHeader(value = "x-amz-sns-message-type") String messageType, @RequestBody String request, @RequestParam(name = "entityName") String entityName, @RequestParam(name = "fieldName") String fieldName) throws Throwable {
        logger.info(request);
        mongoIndexingManager.droppingIndexFromCron(request,messageType,entityName, fieldName);
    }

}
