package com.vedantu.vedantudata.controllers;

import com.vedantu.User.Role;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.VException;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.CollectionUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.PlatformBasicResponse;
import com.vedantu.util.security.HttpSessionUtils;
import com.vedantu.vedantudata.entities.salesconversion.SalesAgentInfo;
import com.vedantu.vedantudata.enums.salesconversion.*;
import com.vedantu.vedantudata.managers.SalesConversionManager;
import com.vedantu.vedantudata.pojos.SalesConversionApiResponseManager;
import com.vedantu.vedantudata.pojos.request.*;
import com.vedantu.vedantudata.pojos.response.*;
import io.swagger.annotations.ApiOperation;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.text.ParseException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/salesc")
public class SalesConversionController {

    @Autowired
    private HttpSessionUtils sessionUtils;

    @Autowired
    private SalesConversionManager salesConversionManager;

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(SalesConversionController.class);

    @RequestMapping(value = "/addFOSTeamMandays", method = RequestMethod.POST)
    @ApiOperation(value = "addFOSTeamMandays", notes = "Adds mandays for sales FOS team..")
    public @ResponseBody
    SalesConversionApiResponseManager addFOSTeamMandays(@RequestBody FOSTeamMandaysObjectReq fosTeamMandaysObjectReq) throws VException, IOException, ParseException {

        logger.info("fosTeamMandaysReq req: {}", fosTeamMandaysObjectReq);
        sessionUtils.checkIfAllowedList(null, Arrays.asList(Role.ADMIN), true);

        SalesConversionApiResponseManager response;
        List<String> errors = new FOSTeamMandaysObjectReq().validate(fosTeamMandaysObjectReq);
        //if request is not proper then return with appropriate msg
        if (!CollectionUtils.isEmpty(errors)) {
            response = new SalesConversionApiResponseManager.Builder()
                    .responseStatus("ERROR")
                    .responseMessage("Request fields are missing..")
                    .payload(errors)
                    .build();
            return response;
        }

        long loggedInUserId = sessionUtils.getCurrentSessionData().getUserId();

        String serviceResponse = salesConversionManager.addSalesFOSMandays(fosTeamMandaysObjectReq.getData(), loggedInUserId, SalesTeam.FOS);
        if ("SUCCESS".equals(serviceResponse)) {
            response = new SalesConversionApiResponseManager.Builder()
                    .responseStatus("SUCCESS")
                    .responseMessage("FOS team Mandays data added successfully..")
                    .build();
        } else {
            response = new SalesConversionApiResponseManager.Builder()
                    .responseStatus("ERROR")
                    .errorMessage("Error while adding FOS team Mandays data")
                    .payload(serviceResponse)
                    .build();
        }
        return response;
    }

    @RequestMapping(value = "/addCentreDetails", method = RequestMethod.POST)
    @ApiOperation(value = "addCentreDetails", notes = "Adds centre details..")
    public @ResponseBody
    SalesConversionApiResponseManager addCentreDetails(@RequestBody CentreDetailsReq centreDetailsReq) throws VException, IOException, ParseException {

        logger.info("addCentreDetails req payload: {}", centreDetailsReq);
        sessionUtils.checkIfAllowedList(null, Arrays.asList(Role.ADMIN), true);

        SalesConversionApiResponseManager response;
        //if request is not proper then return with appropriate msg
        List<String> errors = new CentreDetailsReq().validate(centreDetailsReq);
        if (!CollectionUtils.isEmpty(errors)) {
            response = new SalesConversionApiResponseManager.Builder()
                    .responseStatus("ERROR")
                    .responseMessage("Request fields are missing..")
                    .payload(errors)
                    .build();
            return response;
        }

        long loggedInUserId = sessionUtils.getCurrentSessionData().getUserId();

        Boolean serviceResponse = salesConversionManager.addCentreDetails(centreDetailsReq, loggedInUserId);
        if (serviceResponse) {
            response = new SalesConversionApiResponseManager.Builder()
                    .responseStatus("SUCCESS")
                    .responseMessage("Centre data added successfully..")
                    .build();
        } else {
            response = new SalesConversionApiResponseManager.Builder()
                    .responseStatus("ERROR")
                    .errorMessage("Error while adding Centre data")
                    .build();
        }
        return response;
    }

    @RequestMapping(value = "/updateCentreName", method = RequestMethod.POST)
    @ApiOperation(value = "updateCentreName", notes = "Update centre name..")
    public @ResponseBody
    SalesConversionApiResponseManager updateCentreName(@RequestBody CentreUpdateReq centreUpdateReq) throws VException, IOException, ParseException {

        logger.info("updateCentreName req payload: {}", centreUpdateReq);
        sessionUtils.checkIfAllowedList(null, Arrays.asList(Role.ADMIN), true);

        SalesConversionApiResponseManager response;
        //if request is not proper then return with appropriate msg
        List<String> errors = new CentreUpdateReq().validate(centreUpdateReq);
        if (!CollectionUtils.isEmpty(errors)) {
            response = new SalesConversionApiResponseManager.Builder()
                    .responseStatus("ERROR")
                    .responseMessage("Request fields are missing..")
                    .payload(errors)
                    .build();
            return response;
        }

        long loggedInUserId = sessionUtils.getCurrentSessionData().getUserId();

        String serviceResponse = salesConversionManager.updateCentreName(centreUpdateReq, loggedInUserId);
        if ("SUCCESS".equals(serviceResponse)) {
            response = new SalesConversionApiResponseManager.Builder()
                    .responseStatus("SUCCESS")
                    .responseMessage("Centre data updated successfully..")
                    .build();
        } else {
            response = new SalesConversionApiResponseManager.Builder()
                    .responseStatus("ERROR")
                    .errorMessage("Error while adding Centre data")
                    .payload(serviceResponse)
                    .build();
        }
        return response;
    }

    @RequestMapping(value = "/addTargetInput", method = RequestMethod.POST)
    @ApiOperation(value = "addTargetInput", notes = "Adds target input details..")
    @ResponseBody
    public PlatformBasicResponse addTargetInput(@RequestBody AddTargetInputReq addTargetInputReq) throws VException, ParseException {
        addTargetInputReq.verify();
        sessionUtils.checkIfAllowed(null, Role.ADMIN, true);
        return salesConversionManager.addTargetInput(addTargetInputReq);
    }

    @RequestMapping(value = "/addISTeamMandays", method = RequestMethod.POST)
    @ApiOperation(value = "addISTeamMandays", notes = "Adds mandays for sales IS team..")
    public @ResponseBody
    SalesConversionApiResponseManager addISTeamMandays(@RequestBody ISTeamMandaysObjectReq isTeamMandaysObjectReq) throws VException, IOException, ParseException {

        logger.info("isTeamMandaysObjectReq req: {}", isTeamMandaysObjectReq);
        sessionUtils.checkIfAllowedList(null, Arrays.asList(Role.ADMIN), true);

        SalesConversionApiResponseManager response;
        List<String> errors = new ISTeamMandaysObjectReq().validate(isTeamMandaysObjectReq);
        //if request is not proper then return with appropriate msg
        if (!CollectionUtils.isEmpty(errors)) {
            response = new SalesConversionApiResponseManager.Builder()
                    .responseStatus("ERROR")
                    .responseMessage("Request fields are missing..")
                    .payload(errors)
                    .build();
            return response;
        }

        long loggedInUserId = sessionUtils.getCurrentSessionData().getUserId();

        String serviceResponse = salesConversionManager.addSalesISMandays(isTeamMandaysObjectReq.getData(), loggedInUserId, SalesTeam.IS);
        if ("SUCCESS".equals(serviceResponse)) {
            response = new SalesConversionApiResponseManager.Builder()
                    .responseStatus("SUCCESS")
                    .responseMessage("IS team Mandays data added successfully..")
                    .build();
        } else {
            response = new SalesConversionApiResponseManager.Builder()
                    .responseStatus("ERROR")
                    .errorMessage("Error while adding IS team Mandays data")
                    .payload(serviceResponse)
                    .build();
        }
        return response;
    }


    @RequestMapping(value = "/getCentreInfos", method = RequestMethod.GET)
    @ApiOperation(value = "getCentreInfos", notes = "Get all centre infos")
    public @ResponseBody
    SalesConversionApiResponseManager getCentreInfos(@RequestParam("start") Integer start, @RequestParam("size") Integer size) throws VException {

        sessionUtils.checkIfAllowedList(null, Arrays.asList(Role.ADMIN), true);
        FOSCentreFilterRes fosCentreFilterRes = salesConversionManager.getCentreInfos(start, size);
        SalesConversionApiResponseManager response = new SalesConversionApiResponseManager.Builder()
                .responseStatus("SUCCESS")
                .payload(fosCentreFilterRes)
                .build();

        return response;
    }

    @RequestMapping(value = "/getSAStatus", method = RequestMethod.GET)
    @ApiOperation(value = "getSAStatus", notes = "Get all Sales Agent status.")
    public @ResponseBody
    SalesConversionApiResponseManager getSAStatus(@RequestParam(name = "team", required = true) SalesTeam team) throws VException {

        logger.info("getSAStatus req: {}", team);
        sessionUtils.checkIfAllowedList(null, Arrays.asList(Role.ADMIN), true);
        List<AgentStatus> agentStatuses = salesConversionManager.getSAStatus(team);
        SalesConversionApiResponseManager response = new SalesConversionApiResponseManager.Builder()
                .responseStatus("SUCCESS")
                .payload(agentStatuses)
                .build();

        return response;
    }

    @RequestMapping(value = "/getSADesignation", method = RequestMethod.GET)
    @ApiOperation(value = "getSADesignation", notes = "Get all Sales Agent designation.")
    public @ResponseBody
    SalesConversionApiResponseManager getSADesignation(@RequestParam(name = "team", required = true) SalesTeam team) throws VException {

        logger.info("getSADesignation req: {}", team);
        sessionUtils.checkIfAllowedList(null, Arrays.asList(Role.ADMIN), true);
        Map<AgentDesignation, AgentDesignationType> agentDesgToTypeMap = salesConversionManager.getSADesignation(team);
        SalesConversionApiResponseManager response = new SalesConversionApiResponseManager.Builder()
                .responseStatus("SUCCESS")
                .payload(agentDesgToTypeMap)
                .build();

        return response;
    }


    @RequestMapping(value = "/getSalesAgentMetadata", method = RequestMethod.POST)
    @ApiOperation(value = "getSalesAgentMetadata", notes = "Get all Sales Agent metadata")
    public @ResponseBody
    SalesConversionApiResponseManager getSalesAgentMetadata(@RequestBody SalesAgentMetadataReq salesAgentMetadataReq) throws VException {

        logger.info("getSalesAgentMetadata req: {}", salesAgentMetadataReq);
        sessionUtils.checkIfAllowedList(null, Arrays.asList(Role.ADMIN), true);

        SalesConversionApiResponseManager response;
        List<String> errors = new SalesAgentMetadataReq().validate(salesAgentMetadataReq);
        //if request is not proper then return with appropriate msg
        if (!CollectionUtils.isEmpty(errors)) {
            response = new SalesConversionApiResponseManager.Builder()
                    .responseStatus("ERROR")
                    .responseMessage("Request fields are missing..")
                    .payload(errors)
                    .build();
            return response;
        }

        SalesAgentMetadataRes salesAgentMetadataRes = salesConversionManager.getSalesAgentMetadata(salesAgentMetadataReq);
        response = new SalesConversionApiResponseManager.Builder()
                .responseStatus("SUCCESS")
                .payload(salesAgentMetadataRes)
                .build();

        return response;
    }

    @RequestMapping(value = "/getSalesAgentDataById", method = RequestMethod.GET)
    @ApiOperation(value = "getSalesAgentDataById", notes = "Get Sales agent data by id")
    public @ResponseBody
    SalesConversionApiResponseManager getSalesAgentDataById(@RequestParam(name = "id", required = true) String fosAgentId,
                                                            @RequestParam(name = "team", required = true) SalesTeam team) throws VException {

        logger.info("getSalesAgentDataById req");
//        sessionUtils.checkIfAllowedList(null, Arrays.asList(Role.ADMIN), true);
        SalesAgentDataRes salesAgentDataRes = salesConversionManager.getSalesAgentDataById(fosAgentId, team);

        logger.info(" getSalesAgentDataById res: {}", salesAgentDataRes);
        SalesConversionApiResponseManager response = new SalesConversionApiResponseManager.Builder()
                .responseStatus("SUCCESS")
                .payload(salesAgentDataRes)
                .build();

        return response;
    }

    @RequestMapping(value = "/getCentreInfoById", method = RequestMethod.GET)
    @ApiOperation(value = "getCentreInfoById", notes = "Get Centre info by id")
    public @ResponseBody
    SalesConversionApiResponseManager getCentreInfoById(@RequestParam(name = "id", required = true) String centreId) throws VException {

        logger.info("getCentreInfoById");
        sessionUtils.checkIfAllowedList(null, Arrays.asList(Role.ADMIN), true);
        CentreInfoRes centreInfoRes = salesConversionManager.getCentreInfoById(centreId);

        logger.info(" getSalesAgentDataById req: {}", centreId);
        SalesConversionApiResponseManager response = new SalesConversionApiResponseManager.Builder()
                .responseStatus("SUCCESS")
                .payload(centreInfoRes)
                .build();

        return response;
    }

    @RequestMapping(value = "/checkSAUniqueValue", method = RequestMethod.POST)
    @ApiOperation(value = "checkSAUniqueValue", notes = "Checks whether Agent data present")
    public @ResponseBody
    SalesConversionApiResponseManager checkSAUniqueValue(@RequestBody CheckSAUniqueValueReq checkSAUniqueValueReq) throws VException {

        logger.info("checkSAUniqueValue req: {}", checkSAUniqueValueReq);
        sessionUtils.checkIfAllowedList(null, Arrays.asList(Role.ADMIN), true);

        SalesConversionApiResponseManager responseMessage = null;
        List<String> errors = CheckSAUniqueValueReq.validate(checkSAUniqueValueReq);
        //if request is not proper then return with appropriate msg
        if (!CollectionUtils.isEmpty(errors)) {
            responseMessage = new SalesConversionApiResponseManager.Builder()
                    .responseStatus("ERROR")
                    .responseMessage("Request fields are missing..")
                    .payload(errors)
                    .build();
            return responseMessage;
        }

        boolean flag = salesConversionManager.checkSAUniqueValue(checkSAUniqueValueReq);
        if (flag) {
            responseMessage = new SalesConversionApiResponseManager.Builder()
                    .responseStatus("ERROR")
                    .responseMessage("Sales agent data already present")
                    .build();
        } else {
            responseMessage = new SalesConversionApiResponseManager.Builder()
                    .responseStatus("SUCCESS")
                    .errorMessage("Can add new data..")
                    .build();
        }
        return responseMessage;

    }

    @RequestMapping(value = "/checkCentreUniqueValue", method = RequestMethod.GET)
    @ApiOperation(value = "checkCentreUniqueValue", notes = "Checks whether Centre data present")
    public @ResponseBody
    SalesConversionApiResponseManager checkCentreUniqueValue(@RequestParam(name = "name", required = true) String centreName) throws VException {

        logger.info("checkCentreUniqueValue req: {}", centreName);
        sessionUtils.checkIfAllowedList(null, Arrays.asList(Role.ADMIN), true);

        SalesConversionApiResponseManager responseMessage = null;

        boolean flag = salesConversionManager.checkCentreUniqueValue(centreName);
        if (flag) {
            responseMessage = new SalesConversionApiResponseManager.Builder()
                    .responseStatus("ERROR")
                    .responseMessage("Centre with name " + centreName + " already present")
                    .build();
        } else {
            responseMessage = new SalesConversionApiResponseManager.Builder()
                    .responseStatus("SUCCESS")
                    .errorMessage("Can add new data..")
                    .build();
        }
        return responseMessage;

    }

    @RequestMapping(value = "/addSalesAgentData", method = RequestMethod.POST)
    @ApiOperation(value = "addSalesAgentData", notes = "Adds Sales agent details")
    public @ResponseBody
    SalesConversionApiResponseManager addSalesAgentData(@RequestBody SalesAgentDataReq salesAgentDataReq) throws VException, IOException {

        logger.info("addSalesAgentData req: {}", salesAgentDataReq);
        sessionUtils.checkIfAllowedList(null, Arrays.asList(Role.ADMIN), true);

        SalesConversionApiResponseManager responseMessage = null;
        List<String> errors = SalesAgentDataReq.validate(salesAgentDataReq);
        //if request is not proper then return with appropriate msg
        if (!CollectionUtils.isEmpty(errors)) {
            responseMessage = new SalesConversionApiResponseManager.Builder()
                    .responseStatus("ERROR")
                    .responseMessage("Request fields are missing..")
                    .payload(errors)
                    .build();
            return responseMessage;
        }

        long callingUserId = sessionUtils.getCallingUserId();
        boolean flag = salesConversionManager.addSalesAgentData(salesAgentDataReq, callingUserId, false);
        if (flag) {
            responseMessage = new SalesConversionApiResponseManager.Builder()
                    .responseStatus("SUCCESS")
                    .responseMessage("Sales agent data added successfully")
                    .build();
        } else {
            responseMessage = new SalesConversionApiResponseManager.Builder()
                    .responseStatus("ERROR")
                    .errorMessage("Error while adding Sales agent data")
                    .build();
        }
        return responseMessage;
    }

    @RequestMapping(value = "/getSalesAgentFilterValue", method = RequestMethod.POST)
    @ApiOperation(value = "getSalesAgentFilterValue", notes = "Get filter value metadata")
    public @ResponseBody
    SalesConversionApiResponseManager getSalesAgentFilterValue(@RequestBody SalesAgentFilterValueReq salesAgentFilterValueReq) throws VException {

        logger.info("getSalesAgentFilterValue req: {}", salesAgentFilterValueReq);
        sessionUtils.checkIfAllowedList(null, Arrays.asList(Role.ADMIN), true);

        List<SalesAgentInfo> salesAgentInfos = salesConversionManager.getSalesAgentFilterValue(salesAgentFilterValueReq);
        SalesConversionApiResponseManager response = new SalesConversionApiResponseManager.Builder()
                .responseStatus("SUCCESS")
                .payload(salesAgentInfos)
                .build();

        return response;
    }

    @RequestMapping(value = "/getISFilterByDesignation", method = RequestMethod.GET)
    @ApiOperation(value = "getISFilterByDesignation", notes = "Get IS filter value by designation")
    public @ResponseBody
    SalesConversionApiResponseManager getSAFilterByDesignation(@RequestParam(name = "id", required = true) String isFMId) throws VException {

        logger.info("getISFilterByDesignation req: {}", isFMId);
        sessionUtils.checkIfAllowedList(null, Arrays.asList(Role.ADMIN), true);

        List<SalesAgentInfo> salesAgentInfos = salesConversionManager.getISFilterByDesignation(isFMId);
        SalesConversionApiResponseManager response = new SalesConversionApiResponseManager.Builder()
                .responseStatus("SUCCESS")
                .payload(salesAgentInfos)
                .build();

        return response;
    }

    @RequestMapping(value = "/getFOSCentreFilter", method = RequestMethod.POST)
    @ApiOperation(value = "getFOSCentreFilter", notes = "Get FOS filter value by designation")
    public @ResponseBody
    SalesConversionApiResponseManager getFOSCentreFilter(@RequestBody FOSCentreFilterReq fosCentreFilterReq) throws VException {

        logger.info("getFOSCentreFilter req: {}", fosCentreFilterReq);
        sessionUtils.checkIfAllowedList(null, Arrays.asList(Role.ADMIN), true);

        FOSCentreFilterRes fosCentreFilterRes = salesConversionManager.getFOSCentreFilter(fosCentreFilterReq);
        SalesConversionApiResponseManager response = new SalesConversionApiResponseManager.Builder()
                .responseStatus("SUCCESS")
                .payload(fosCentreFilterRes)
                .build();

        return response;
    }

    @RequestMapping(value = "/getFOSTeamLeadFilter", method = RequestMethod.GET)
    @ApiOperation(value = "getFOSTeamLeadFilter", notes = "Get FOS Team Lead filter value by designation")
    public @ResponseBody
    SalesConversionApiResponseManager getFOSTeamLeadFilter(@RequestParam(name = "id", required = true) String centreId) throws VException {

        logger.info("getFOSTeamLeadFilter req: {}", centreId);
        sessionUtils.checkIfAllowedList(null, Arrays.asList(Role.ADMIN), true);

        List<SalesAgentInfo> salesAgentInfos = salesConversionManager.getFOSTeamLeadFilter(centreId);
        SalesConversionApiResponseManager response = new SalesConversionApiResponseManager.Builder()
                .responseStatus("SUCCESS")
                .payload(salesAgentInfos)
                .build();

        return response;
    }

    @RequestMapping(value = "/generateSaleAgentCsv", method = RequestMethod.POST)
    @ApiOperation(value = "generateSaleAgentCsv", notes = "Generate Sales agent Csv")
    public @ResponseBody
    SalesConversionApiResponseManager generateSaleAgentCsv(@RequestBody SalesAgentMetadataReq salesAgentMetadataReq) throws VException, IOException {

        logger.info("salesAgentMetadataReq req: {}", salesAgentMetadataReq);
        sessionUtils.checkIfAllowedList(null, Arrays.asList(Role.ADMIN), true);

        SalesConversionApiResponseManager response;
        List<String> errors = new SalesAgentMetadataReq().validate(salesAgentMetadataReq);
        //if request is not proper then return with appropriate msg
        if (!CollectionUtils.isEmpty(errors)) {
            response = new SalesConversionApiResponseManager.Builder()
                    .responseStatus("ERROR")
                    .responseMessage("Request fields are missing..")
                    .payload(errors)
                    .build();
            return response;
        }

        String link = salesConversionManager.generateSaleAgentCsv(salesAgentMetadataReq);
        if (!"ERROR".equals(link)) {
            response = new SalesConversionApiResponseManager.Builder()
                    .responseStatus("SUCCESS")
                    .payload(link)
                    .build();
        } else {
            response = new SalesConversionApiResponseManager.Builder()
                    .responseStatus("ERROR")
                    .payload("Error while generating csv")
                    .build();
        }

        return response;
    }

    @RequestMapping(value = "/getSARegex", method = RequestMethod.POST)
    @ApiOperation(value = "getSARegex", notes = "Generate Sales agent Csv")
    public @ResponseBody
    SalesConversionApiResponseManager getSARegex(@RequestBody GetAllSalesAgentReq getAllSalesAgentReq) throws VException, IOException {

        logger.info("getSARegex req: {}", getAllSalesAgentReq);
        sessionUtils.checkIfAllowedList(null, Arrays.asList(Role.ADMIN), true);

        SalesConversionApiResponseManager response;
        List<String> errors = new GetAllSalesAgentReq().validate(getAllSalesAgentReq);
        //if request is not proper then return with appropriate msg
        if (!CollectionUtils.isEmpty(errors)) {
            response = new SalesConversionApiResponseManager.Builder()
                    .responseStatus("ERROR")
                    .responseMessage("Request fields are missing..")
                    .payload(errors)
                    .build();
            return response;
        }

        SalesAgentMetadataRes salesAgentMetadataRes = salesConversionManager.getSARegex(getAllSalesAgentReq);
        response = new SalesConversionApiResponseManager.Builder()
                .responseStatus("SUCCESS")
                .payload(salesAgentMetadataRes)
                .build();

        return response;
    }

    @RequestMapping(value = "/addAgentAttendance", method = RequestMethod.POST)
    @ApiOperation(value = "addAgentAttendance", notes = "Adds agent attendance as bulk")
    public @ResponseBody
    SalesConversionApiResponseManager addAgentAttendance(@RequestBody AddAgentAttendanceReq addAgentAttendanceReq) throws VException, ParseException {
        sessionUtils.checkIfAllowedList(null, Arrays.asList(Role.ADMIN), true);
        addAgentAttendanceReq.verify();
        return salesConversionManager.addAgentAttendance(addAgentAttendanceReq);
    }

    @RequestMapping(value = "/getAgentAttendanceData", method = RequestMethod.POST)
    @ApiOperation(value = "getAgentAttendanceData", notes = "Get all Sales Agent attendance data")
    public @ResponseBody
    SalesConversionApiResponseManager getAgentAttendanceData(@RequestBody SalesAgentAttendanceDataReq salesAgentAttendanceDataReq) throws VException {

        logger.info("getAgentAttendanceData req: {}", salesAgentAttendanceDataReq);
        sessionUtils.checkIfAllowedList(null, Arrays.asList(Role.ADMIN), true);

        SalesConversionApiResponseManager response;
        List<String> errors = new SalesAgentAttendanceDataReq().validate(salesAgentAttendanceDataReq);
        //if request is not proper then return with appropriate msg
        if (!CollectionUtils.isEmpty(errors)) {
            response = new SalesConversionApiResponseManager.Builder()
                    .responseStatus("ERROR")
                    .responseMessage("Request fields are missing..")
                    .payload(errors)
                    .build();
            return response;
        }

        SalesAgentAttendanceDataRes salesAgentAttendanceDataRes = salesConversionManager.getAgentAttendanceData(salesAgentAttendanceDataReq,false);
        response = new SalesConversionApiResponseManager.Builder()
                .responseStatus("SUCCESS")
                .payload(salesAgentAttendanceDataRes)
                .build();

        return response;
    }

    @RequestMapping(value = "/editAgentAttendance", method = RequestMethod.POST)
    @ApiOperation(value = "editAgentAttendance", notes = "Edit Agent Attendance")
    public @ResponseBody
    PlatformBasicResponse editAgentAttendance(@RequestBody AgentAttendancePojo agentAttendancePojo) throws VException, ParseException {
        sessionUtils.checkIfAllowedList(null, Arrays.asList(Role.ADMIN), true);
        agentAttendancePojo.verify();
        return salesConversionManager.editAgentAttendance(agentAttendancePojo);
    }

    @RequestMapping(value = "/getAgentAttendanceById", method = RequestMethod.GET)
    @ApiOperation(value = "getAgentAttendanceById", notes = "Get agent attendance data")
    public @ResponseBody
    SalesConversionApiResponseManager getAgentAttendanceById(@RequestParam(name = "id", required = true) String id,
                                                             @RequestParam(name = "team", required = true) SalesTeam team) throws VException {

        logger.info("getAgentAttendanceById req: {}", id);
        sessionUtils.checkIfAllowedList(null, Arrays.asList(Role.ADMIN), true);

        SalesAgentAttendanceDataRes salesAgentAttendanceDataRes = salesConversionManager.getAgentAttendanceById(id, team);
        SalesConversionApiResponseManager response = new SalesConversionApiResponseManager.Builder()
                .responseStatus("SUCCESS")
                .payload(salesAgentAttendanceDataRes)
                .build();

        return response;
    }

    @RequestMapping(value = "/getAttendanceAcronyms", method = RequestMethod.GET)
    @ApiOperation(value = "getAttendanceAcronyms", notes = "Get attendance acryonyms")
    public @ResponseBody
    SalesConversionApiResponseManager getAttendanceAcronyms() throws VException {

        sessionUtils.checkIfAllowedList(null, Arrays.asList(Role.ADMIN), true);

        Map<String,AgentAttendanceStatus> agentAttendanceStatus = salesConversionManager.getAttendanceAcronyms();
        SalesConversionApiResponseManager response = new SalesConversionApiResponseManager.Builder()
                .responseStatus("SUCCESS")
                .payload(agentAttendanceStatus)
                .build();

        return response;
    }

    @RequestMapping(value = "/generateAgentAttendanceCsv", method = RequestMethod.POST)
    @ApiOperation(value = "generateAgentAttendanceCsv", notes = "Generate Sales agent attendance Csv")
    public @ResponseBody
    SalesConversionApiResponseManager generateAgentAttendanceCsv(@RequestBody SalesAgentAttendanceDataReq salesAgentAttendanceDataReq) throws VException, IOException {

        logger.info("generateAgentAttendanceCsv req: {}", salesAgentAttendanceDataReq);
        sessionUtils.checkIfAllowedList(null, Arrays.asList(Role.ADMIN), true);

        SalesConversionApiResponseManager response;
        List<String> errors = new SalesAgentAttendanceDataReq().validate(salesAgentAttendanceDataReq);
        //if request is not proper then return with appropriate msg
        if (!CollectionUtils.isEmpty(errors)) {
            response = new SalesConversionApiResponseManager.Builder()
                    .responseStatus("ERROR")
                    .responseMessage("Request fields are missing..")
                    .payload(errors)
                    .build();
            return response;
        }

        String link = salesConversionManager.generateAgentAttendanceCsv(salesAgentAttendanceDataReq);
        if (!"ERROR".equals(link)) {
            response = new SalesConversionApiResponseManager.Builder()
                    .responseStatus("SUCCESS")
                    .payload(link)
                    .build();
        } else {
            response = new SalesConversionApiResponseManager.Builder()
                    .responseStatus("ERROR")
                    .payload("Error while generating csv")
                    .build();
        }

        return response;
    }
    @RequestMapping(value = "/addAgentsAsBulk", method = RequestMethod.POST)
    public @ResponseBody
    PlatformBasicResponse addAgentsAsBulk(@RequestBody AddAgentsAsBulkReq addAgentsAsBulkReq) throws VException {

        sessionUtils.checkIfAllowedList(null, Arrays.asList(Role.ADMIN), true);
        long callingUserId = sessionUtils.getCallingUserId();
        return salesConversionManager.addAgentsAsBulk(addAgentsAsBulkReq,callingUserId);
    }

    @RequestMapping(value = "/getAgentInfoByEmployeeId", method = RequestMethod.GET)
    public @ResponseBody
    SalesAgentInfo getAgentInfoByEmployeeId(@RequestParam(name = "employeeId",required = true) String employeeId) throws VException {
        sessionUtils.checkIfAllowedList(null, Arrays.asList(Role.ADMIN), true);
        return salesConversionManager.getAgentInfoByEmployeeId(employeeId);
    }
}
