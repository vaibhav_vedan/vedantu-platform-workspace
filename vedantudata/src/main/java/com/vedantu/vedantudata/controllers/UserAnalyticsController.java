package com.vedantu.vedantudata.controllers;

import com.vedantu.User.Role;
import com.vedantu.util.security.HttpSessionUtils;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.vedantu.exception.VException;
import com.vedantu.util.LogFactory;
import com.vedantu.vedantudata.entities.analytics.UserProfileData;
import com.vedantu.vedantudata.managers.UserAnalyticsManager;

import io.swagger.annotations.ApiOperation;

import java.util.Arrays;

@RestController
@RequestMapping("/analytics/user")
public class UserAnalyticsController {

	@Autowired
	private UserAnalyticsManager userAnalyticsManager;

	@Autowired
	private LogFactory logFactory;

	@SuppressWarnings("static-access")
	private Logger logger = logFactory.getLogger(UserAnalyticsController.class);

	@Autowired
	private HttpSessionUtils httpSessionUtils;

	@RequestMapping(value = "/getUserByVedantuId", method = RequestMethod.GET)
	@ApiOperation(value = "Get user profile by user id", notes = "User profile")
	public UserProfileData getLeadsquaredUsers(@RequestParam(name = "userId", required = true) String userId)
			throws VException {
		logger.info("Request");
		httpSessionUtils.checkIfAllowedList(null, Arrays.asList(Role.ADMIN, Role.STUDENT_CARE), true);
		return userAnalyticsManager.getUserProfileDetailsById(userId);
	}

	@RequestMapping(value = "/updateUserProfileData", method = RequestMethod.GET)
	@ApiOperation(value = "Get user profile by user id", notes = "User profile")
	public String updateUserProfileData(@RequestParam(name = "startTime", required = true) long startTime,
			@RequestParam(name = "endTime", required = true) long endTime) throws VException {
		logger.info("Request");
		httpSessionUtils.checkIfAllowedList(null, Arrays.asList(Role.ADMIN, Role.STUDENT_CARE), true);
		return userAnalyticsManager.updateUserProfileData(startTime, endTime);
	}

}
