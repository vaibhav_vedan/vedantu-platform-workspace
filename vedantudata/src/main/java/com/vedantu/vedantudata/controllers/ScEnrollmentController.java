package com.vedantu.vedantudata.controllers;

import com.vedantu.User.Role;
import com.vedantu.User.TeacherBasicInfo;
import com.vedantu.exception.VException;
import com.vedantu.scheduling.pojo.session.BatchWiseSessionInfo;
import com.vedantu.util.CollectionUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.security.HttpSessionUtils;
import com.vedantu.vedantudata.managers.ScEnrollmentManager;
import com.vedantu.vedantudata.pojos.SalesConversionApiResponseManager;
import com.vedantu.vedantudata.pojos.response.BundleDetailsRes;
import com.vedantu.vedantudata.pojos.response.GetCourseInfoRes;
import io.swagger.annotations.ApiOperation;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/salesc/enrollment")
public class ScEnrollmentController {

    @Autowired
    private ScEnrollmentManager sCEnrollmentManager;

    @Autowired
    private HttpSessionUtils sessionUtils;

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(ScEnrollmentController.class);


    @RequestMapping(value = "/getBundleDetails", method = RequestMethod.GET)
    @ResponseBody
    public BundleDetailsRes getBundleDetails(@RequestParam(name = "entityId") String entityId) throws VException {
        sessionUtils.checkIfAllowedList(null, Arrays.asList(Role.STUDENT, Role.PARENT), true);
        return sCEnrollmentManager.getBundleDetails(entityId);
    }

    @RequestMapping(value = "/getCourseInfo", method = RequestMethod.GET)
    @ApiOperation(value = "getCourseInfo", notes = "Gets course info..from aio")
    public @ResponseBody
    SalesConversionApiResponseManager getCourseInfo(@RequestParam(name = "entityId", required = true) String entityId) throws VException, InterruptedException {

        logger.info("getCourseInfo req entityId: {}", entityId);
        sessionUtils.checkIfAllowedList(null, Arrays.asList(Role.STUDENT, Role.PARENT), true);
        Long userId = sessionUtils.getCallingUserId();
        GetCourseInfoRes getCourseInfoRes = sCEnrollmentManager.getCourseInfo(entityId);
        SalesConversionApiResponseManager response = new SalesConversionApiResponseManager.Builder()
                .responseStatus("SUCCESS")
                .payload(getCourseInfoRes)
                .build();

        return response;
    }

    @RequestMapping(value = "/getCourseWiseSessionInfo", method = RequestMethod.GET)
    @ApiOperation(value = "getCourseWiseSessionInfo", notes = "Gets course wise session info..")
    public @ResponseBody
    SalesConversionApiResponseManager getCourseWiseSessionInfo(@RequestParam(name = "batchId", required = true) String batchId,
                                                               @RequestParam(name = "displayDate", required = true) Long displayDate,
                                                               @RequestParam(name = "validFrom", required = true) Long validFrom,
                                                               @RequestParam(name = "validTill", required = true) Long validTill) throws VException, InterruptedException {

        logger.info("getCourseWiseSessionInfo req batchId: {}, displayDate: {}, validTill: {}, validFrom: {}", batchId, displayDate, validTill, validFrom);
        sessionUtils.checkIfAllowedList(null, Arrays.asList(Role.STUDENT, Role.PARENT), true);
        Long userId = sessionUtils.getCallingUserId();
        Map<String, List<BatchWiseSessionInfo>> getCourseInfoRes = sCEnrollmentManager.getCourseWiseSessionInfo(batchId, displayDate, validTill, validFrom);
        SalesConversionApiResponseManager response = new SalesConversionApiResponseManager.Builder()
                .responseStatus("SUCCESS")
                .payload(getCourseInfoRes)
                .build();

        return response;
    }

    @RequestMapping(value = "/getClassTimings", method = RequestMethod.GET)
    @ApiOperation(value = "getClassTimings", notes = "Gets course wise session info..")
    public @ResponseBody
    SalesConversionApiResponseManager getClassTimings(@RequestParam(name = "entityId", required = true) String entityId) throws VException, InterruptedException {

        logger.info("getClassTimings req entityId: {}", entityId);
        sessionUtils.checkIfAllowedList(null, Arrays.asList(Role.STUDENT, Role.PARENT), true);
        Long userId = sessionUtils.getCallingUserId();
        Object getCourseInfoRes = sCEnrollmentManager.getClassTimings(entityId);
        SalesConversionApiResponseManager response = new SalesConversionApiResponseManager.Builder()
                .responseStatus("SUCCESS")
                .payload(getCourseInfoRes)
                .build();

        return response;
    }

    @RequestMapping(value = "/getSalesConfirmationDetails", method = RequestMethod.GET)
    @ApiOperation(value = "getSalesConfirmationDetails", notes = "Gets Sales Confirmation details from LS")
    public @ResponseBody
    SalesConversionApiResponseManager getSalesConfirmationDetails(@RequestParam(name = "leadId", required = true) String leadId,
                                                                  @RequestParam(name = "entityId", required = true) String entityId) throws VException, InterruptedException {

        logger.info("getSalesConfirmationDetails req leadId: {}, entityId: {}", leadId, entityId);
        sessionUtils.checkIfAllowedList(null, Arrays.asList(Role.STUDENT, Role.PARENT), true);
        Long userId = sessionUtils.getCallingUserId();
        Object obj = sCEnrollmentManager.getSalesConfirmationDetails(leadId, entityId, userId);
        SalesConversionApiResponseManager response = new SalesConversionApiResponseManager.Builder()
                .responseStatus("SUCCESS")
                .payload(obj)
                .build();

        return response;
    }

    @RequestMapping(value = "/getTeacherInfo", method = RequestMethod.POST)
    @ApiOperation(value = "getTeacherInfo", notes = "Gets teacher info")
    public @ResponseBody
    SalesConversionApiResponseManager getTeacherInfo(@RequestBody List<String> teacherIds) throws VException, InterruptedException {

        logger.info("getTeacherInfo req teacherIds: {}", teacherIds);
        SalesConversionApiResponseManager response = null;
        if (CollectionUtils.isEmpty(teacherIds)) {
            response = new SalesConversionApiResponseManager.Builder()
                    .responseStatus("ERROR")
                    .build();
            return response;
        }
        sessionUtils.checkIfAllowedList(null, Arrays.asList(Role.STUDENT, Role.PARENT), true);
        Long userId = sessionUtils.getCallingUserId();
        List<TeacherBasicInfo> obj = sCEnrollmentManager.getTeacherInfo(teacherIds);
        response = new SalesConversionApiResponseManager.Builder()
                .responseStatus("SUCCESS")
                .payload(obj)
                .build();

        return response;
    }

}
