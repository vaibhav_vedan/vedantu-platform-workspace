package com.vedantu.vedantudata.controllers;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Arrays;

import com.vedantu.User.Role;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.ForbiddenException;
import com.vedantu.exception.VException;
import com.vedantu.util.security.HttpSessionUtils;
import com.vedantu.vedantudata.managers.leadsquared.LeadsquaredManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.vedantu.util.CommonUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.vedantudata.entities.UploadIdentifier;
import com.vedantu.vedantudata.request.leadsquared.PushLeadsquaredReq;

import io.swagger.annotations.ApiOperation;

import javax.servlet.http.HttpServletResponse;

@RestController
@RequestMapping("/leadsquared/upload")
public class LeadsquaredUploadController {

    @Autowired
    private LeadsquaredManager leadsquaredManager;

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(LeadsquaredUploadController.class);

    @Autowired
    private HttpSessionUtils httpSessionUtils;

    private static final String DRYRUN_SECRET_KEY = "Vedantu!@#DryRun123";

    @RequestMapping(value = "/leadsDryRun", method = RequestMethod.POST, headers = "content-type=multipart/form-data", produces = MediaType.TEXT_PLAIN_VALUE)
    public String leadsDryRun(@RequestParam(name = "file", required = true) MultipartFile multipart,
                              @RequestParam(name = "secretKey", required = true) String secretKey,
                              HttpServletResponse response) throws Exception {

        logger.info("Request:" + System.currentTimeMillis());

        checkIfDryRunAllowed(secretKey);

        File file = CommonUtils.multipartToFile(multipart);
        Path copy = Files.copy(file.toPath(), Paths.get(file.getAbsolutePath() + "COPY.csv"), StandardCopyOption.REPLACE_EXISTING);
        File copyFile = copy.toFile();
        if (file.exists()) {
            //noinspection ResultOfMethodCallIgnored
            file.delete();
        }
        String jsonResponse = leadsquaredManager.leadsDryRun(copyFile);

        //noinspection ResultOfMethodCallIgnored
        copyFile.delete();

        return jsonResponse;
    }

    @RequestMapping(value = "/pushLeadsToLeadsquared", method = RequestMethod.POST, headers = "content-type=multipart/form-data", produces = MediaType.APPLICATION_JSON_VALUE)
    public UploadIdentifier pushLeadsToLeadsquared(
            @RequestParam(name = "file", required = true) MultipartFile multipart,
            @RequestParam(name = "secretKey", required = true) String secretKey,
            @ModelAttribute PushLeadsquaredReq pushLeadsquaredReq) throws Exception {

        logger.info("Request:" + System.currentTimeMillis());

        checkIfDryRunAllowed(secretKey);

        File file = CommonUtils.multipartToFile(multipart);
        UploadIdentifier updatedEntries = leadsquaredManager.pushLeadsToLeadsquared(file, pushLeadsquaredReq);
        file.delete();
        return updatedEntries;
    }

    @RequestMapping(value = "/uploadLeadNotes", method = RequestMethod.POST, headers = "content-type=multipart/form-data", produces = MediaType.APPLICATION_JSON_VALUE)
    public long uploadLeadNotes(@RequestParam(name = "file", required = true) MultipartFile multipart)
            throws Exception {
        logger.info("Request:" + System.currentTimeMillis());


        httpSessionUtils.checkIfAllowedList(null, Arrays.asList(Role.ADMIN, Role.STUDENT_CARE), true);

        File file = CommonUtils.multipartToFile(multipart);
        long leadsUpdated = leadsquaredManager.uploadLeadNotes(file);
        file.delete();
        return leadsUpdated;
    }

    @RequestMapping(value = "/uploadIdentiferSheet", method = RequestMethod.GET)
    @ApiOperation(value = "uploadIdentiferSheet", notes = "PhoneNumber field")
    public void uploadIdentiferSheet() throws Exception {
        logger.info("Request");
    }

    @RequestMapping(value = "/status", method = RequestMethod.POST)
    @ApiOperation(value = "uploadStatus", notes = "PhoneNumber field")
    public String uploadStatus(@RequestParam(name = "file", required = true) MultipartFile multipart,
                               @RequestParam(name = "secretKey", required = true) String secretKey) throws Exception {

        checkIfDryRunAllowed(secretKey);

        File file = CommonUtils.multipartToFile(multipart);
        String s = leadsquaredManager.uploadStatus(file);
        if (file.exists()) {
            //noinspection ResultOfMethodCallIgnored
            file.delete();
        }
        return s;
    }

    private void checkIfDryRunAllowed(@RequestParam(name = "secretKey", required = true) String secretKey) throws VException {
        httpSessionUtils.checkIfAllowedList(null, Arrays.asList(Role.ADMIN, Role.STUDENT_CARE), true);

        if (!DRYRUN_SECRET_KEY.equals(secretKey))
            throw new ForbiddenException(ErrorCode.FORBIDDEN_ERROR, "You are not authorised to use this feature; Invalid secret key");
    }


}
