package com.vedantu.vedantudata.controllers;

import com.google.gson.Gson;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.User.Role;
import com.vedantu.aws.pojo.AWSSNSSubscriptionRequest;
import com.vedantu.exception.ForbiddenException;
import com.vedantu.exception.VException;
import com.vedantu.util.CollectionUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.PlatformBasicResponse;
import com.vedantu.util.WebUtils;
import com.vedantu.util.security.HttpSessionUtils;
import com.vedantu.vedantudata.managers.leadsquared.LeadSquaredAgentDataManger;
import com.vedantu.vedantudata.pojos.SalesConversionApiResponseManager;
import com.vedantu.vedantudata.pojos.request.AddAgentsEmailReq;
import com.vedantu.vedantudata.pojos.request.FOSTeamMandaysObjectReq;
import com.vedantu.vedantudata.pojos.response.CentreInfoRes;
import com.vedantu.vedantudata.request.LeadVcCodeReq;
import com.vedantu.vedantudata.utils.ActivityEventConstants;
import io.swagger.annotations.ApiOperation;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping("/leadsquared/data")
public class LeadSquaredDataMangementController {


    @Autowired
    HttpSessionUtils sessionUtils;
    @Autowired
    LogFactory logFactory;

    @Autowired
    LeadSquaredAgentDataManger leadSquaredAgentDataManger;

    private Logger logger = logFactory.getLogger(LeadSquaredDataMangementController.class);

    @RequestMapping(value = "/agent/migration", method = RequestMethod.POST)
    public PlatformBasicResponse executeDataMigrationUpdationOfLeadSquaredAgents() throws  Exception {

        return leadSquaredAgentDataManger.executeDataMigrationUpdationOfLeadSquaredAgentsAsync();
    }

    @RequestMapping(value = "/trigger-migration-lsusers-cron", method = RequestMethod.POST,
            consumes = MediaType.TEXT_PLAIN_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void triggerAllAmActivities(@RequestHeader(value = "x-amz-sns-message-type") String messageType, @RequestBody String request) throws ForbiddenException
    {

        logger.info(request);
        Gson gson = new Gson();
        AWSSNSSubscriptionRequest subscriptionRequest = gson.fromJson(request, AWSSNSSubscriptionRequest.class);

        if (messageType.equals("SubscriptionConfirmation"))
        {
            String json = null;
            logger.info("SubscribeURL = " + subscriptionRequest.getSubscribeURL());
            ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionRequest.getSubscribeURL(), HttpMethod.GET, json, false);
            logger.info(resp.getEntity(String.class));
        }
        else if (messageType.equals("Notification"))
        {
            logger.info("Notification received - SNS");
            logger.info("Cron job started for ls users migaration"+request);
            leadSquaredAgentDataManger.executeDataMigrationUpdationOfLeadSquaredAgentsAsync();
        }
    }

    @RequestMapping(value = "/addAgentsEmailForIntermittentScreen", method = RequestMethod.POST)
    @ResponseBody
    public SalesConversionApiResponseManager addAgentsEmailForIntermittentScreen(@RequestBody AddAgentsEmailReq addAgentsEmailReq) throws InterruptedException, VException, VException {

        logger.info("addAgentsEmail: {}", addAgentsEmailReq);

        sessionUtils.checkIfAllowedList(null, Arrays.asList(Role.ADMIN), true);
        SalesConversionApiResponseManager response;
        List<String> errors = new AddAgentsEmailReq().validate(addAgentsEmailReq);
        //if request is not proper then return with appropriate msg
        if (!CollectionUtils.isEmpty(errors)) {
            response = new SalesConversionApiResponseManager.Builder()
                    .responseStatus("ERROR")
                    .responseMessage("Request fields are missing..")
                    .payload(errors)
                    .build();
            return response;
        }

        String counsellorEmail = sessionUtils.getCurrentSessionData().getEmail();
        Boolean isAgentsEmailAdded = leadSquaredAgentDataManger.addAgentsEmailForIntermittentScreen(addAgentsEmailReq);
        response = new SalesConversionApiResponseManager.Builder()
                .responseStatus("SUCCESS")
                .payload(isAgentsEmailAdded)
                .build();

        return response;
    }

    @RequestMapping(value = "/isIntermittentScreenAllowed", method = RequestMethod.GET)
    @ApiOperation(value = "isIntermittentScreenAllowed", notes = "Checks if Intermittent screen allowed")
    public @ResponseBody
    SalesConversionApiResponseManager isIntermittentScreenAllowed(@RequestParam(name = "emailId", required = true) String emailId) throws VException {

        logger.info("isIntermittentScreenAllowed called");

//        sessionUtils.checkIfAllowedList(null, Arrays.asList(Role.ADMIN), true);
        Boolean isIntermittentScreenAllowed = leadSquaredAgentDataManger.isIntermittentScreenAllowed(emailId);
        SalesConversionApiResponseManager response = new SalesConversionApiResponseManager.Builder()
                .responseStatus("SUCCESS")
                .payload(isIntermittentScreenAllowed)
                .build();
        return response;
    }

}
