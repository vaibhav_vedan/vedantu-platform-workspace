package com.vedantu.vedantudata.controllers;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.vedantu.onetofew.pojo.EnrollmentPojo;
import com.vedantu.vedantudata.managers.leadsquared.LeadSquaredWebhooksManager;
import com.vedantu.vedantudata.pojos.ActivityCallbackPojoMapper;
import com.vedantu.vedantudata.pojos.ActivityDetailsCallbackPojo;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.vedantu.util.LogFactory;
import org.apache.logging.log4j.Logger;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/callback")
public class LeadSquaredAMWebhooksController {

    @Autowired
    private LeadSquaredWebhooksManager leadSquaredWebhooksManager;

    @Autowired
    private LogFactory logFactory;

    private Logger logger = logFactory.getLogger(ActivityController.class);

    @RequestMapping(value = "/update-activity", method = RequestMethod.POST)
    @ApiOperation(value = "Callback", notes = "Update Activity Callback")
    public @ResponseBody
    Map<String, String> updateActivityCallback(@RequestBody String activityCallbackPojoMapper) {

        try {

            logger.info("\n\n update-activity callback :: {}", activityCallbackPojoMapper);

            Type listType = new TypeToken<ArrayList<ActivityCallbackPojoMapper>>() {
            }.getType();

            List<ActivityCallbackPojoMapper> activityCallbackPojoMapperObject = new Gson().fromJson(activityCallbackPojoMapper, listType);
            leadSquaredWebhooksManager.updateActivity(activityCallbackPojoMapperObject.get(0));

            Map<String, String> response = new HashMap<>();
            response.put("Status", "Success");
            response.put("StatusReason", "");
            return response;
        } catch (Exception e) {
            logger.error(e.getMessage(),e);
            Map<String, String> response = new HashMap<>();
            response.put("Status", "Error");
            response.put("StatusReason", e.getMessage());

            return response;

        }
    }


    @RequestMapping(value = "/update-activity", method = RequestMethod.GET)
    @ApiOperation(value = "Callback", notes = "Update Activity Callback")
    public @ResponseBody
    Map<String, String> updateActivityCallbackGet() {

            Map<String, String> response = new HashMap<>();
            response.put("Status", "Success");
            response.put("StatusReason", "");
            return response;
    }

    @RequestMapping(value = "/update-activity", method = RequestMethod.HEAD)
    @ApiOperation(value = "Callback", notes = "Update Activity Callback")
    public @ResponseBody
    Map<String, String> updateActivityCallback() {

        Map<String, String> response = new HashMap<>();
        response.put("Status", "Success");
        response.put("StatusReason", "0");

        return response;
    }
}
