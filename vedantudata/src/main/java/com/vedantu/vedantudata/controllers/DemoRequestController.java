package com.vedantu.vedantudata.controllers;

import com.vedantu.User.Role;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.ForbiddenException;
import com.vedantu.exception.VException;
import com.vedantu.util.BasicResponse;
import com.vedantu.util.LogFactory;
import com.vedantu.util.PlatformBasicResponse;
import com.vedantu.util.security.HttpSessionUtils;
import com.vedantu.vedantudata.managers.DemoRequestManager;
import com.vedantu.vedantudata.pojos.DemoRequestPojo;
import com.vedantu.vedantudata.request.DemoRequest;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping(value = "/demo-request")
public class DemoRequestController {

    @Autowired
    private LogFactory logFactory;

    private final Logger logger = logFactory.getLogger(DemoRequestController.class);

    @Autowired
    private DemoRequestManager demoRequestManager;

    @Autowired
    private HttpSessionUtils httpSessionUtils;

    @RequestMapping(value = "/register", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public PlatformBasicResponse registerDemo(@RequestBody DemoRequestPojo demoRequestPojo) throws VException {

        httpSessionUtils.checkIfAllowedList(null, Arrays.asList(Role.STUDENT), true);
        demoRequestManager.registerDemo(demoRequestPojo);

        return new PlatformBasicResponse();
    }

    @RequestMapping(value = "/is-demo-registered", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public PlatformBasicResponse getRegisteredDemo(@RequestBody DemoRequestPojo demoRequestPojo) throws VException {

        httpSessionUtils.checkIfAllowedList(null, Arrays.asList(Role.STUDENT, Role.ADMIN, Role.STUDENT_CARE), true);

        if(!httpSessionUtils.getCurrentSessionData().hasAdminRole()){
            if(!httpSessionUtils.getCallingUserId().equals(demoRequestPojo.getUserId()))
                throw new ForbiddenException(ErrorCode.FORBIDDEN_ERROR,"Not authorised to access the data");
        }

        DemoRequest demoRequest= demoRequestManager.getRegisteredDemo(demoRequestPojo.getUserId(), demoRequestPojo.getDemoRequestType());

        return new PlatformBasicResponse(true, demoRequest != null,"");

    }

}
