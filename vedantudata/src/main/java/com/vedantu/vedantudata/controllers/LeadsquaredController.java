package com.vedantu.vedantudata.controllers;

import com.google.gson.Gson;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.User.Role;
import com.vedantu.aws.pojo.AWSSNSSubscriptionRequest;
import com.vedantu.exception.NotFoundException;
import com.vedantu.exception.VException;
import com.vedantu.util.*;
import com.vedantu.util.security.HttpSessionUtils;
import com.vedantu.vedantudata.entities.leadsquared.LeadActivity;
import com.vedantu.vedantudata.entities.leadsquared.LeadDetails;
import com.vedantu.vedantudata.entities.leadsquared.LeadTask;
import com.vedantu.vedantudata.managers.UserManager;
import com.vedantu.vedantudata.managers.leadsquared.LeadsquaredActivityManager;
import com.vedantu.vedantudata.managers.leadsquared.LeadsquaredAsyncTaskManager;
import com.vedantu.vedantudata.managers.leadsquared.LeadsquaredManager;
import com.vedantu.vedantudata.managers.leadsquared.LeadsquaredTaskManager;
import com.vedantu.vedantudata.request.LeadVcCodeReq;
import io.swagger.annotations.ApiOperation;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.util.*;


@RestController
@RequestMapping("/leadsquared/lead")
public class LeadsquaredController {

	@Autowired
	private LeadsquaredManager leadsquaredManager;

	@Autowired
	private LeadsquaredActivityManager leadsquaredActivityManager;

	@Autowired
	private LeadsquaredTaskManager leadsquaredTaskManager;

	@Autowired
	private LeadsquaredAsyncTaskManager leadsquaredAsyncTaskManager;

	@Autowired
	private UserManager userManager;

	@Autowired
	private LogFactory logFactory;

	@SuppressWarnings("static-access")
	private Logger logger = logFactory.getLogger(LeadsquaredController.class);

	@Autowired
	private HttpSessionUtils httpSessionUtils;

	@RequestMapping(value = "/getLeadByEmailId", method = RequestMethod.GET)
	@ApiOperation(value = "Get lead by email id", notes = "LeadDetails")
	public List<LeadDetails> getLeadsquaredUsers(@RequestParam(name = "emailId", required = true) String emailId)
			throws VException, InterruptedException {
		logger.info("Request");

		httpSessionUtils.checkIfAllowedList(null, Arrays.asList(Role.ADMIN, Role.STUDENT_CARE), true);

		return leadsquaredManager.fetchLeadsByEmailId(emailId);
	}

	@RequestMapping(value = "/getLeadEmailsByPhone", method = RequestMethod.GET)
	@ApiOperation(value = "Get lead by phone", notes = "LeadDetails")
	public List<Map<String,String>> getLeadsquaredUserEmailByPhone(@RequestParam(name = "phoneNo", required = true) String phone,
																   HttpServletRequest request)
			throws VException, InterruptedException, UnsupportedEncodingException {
		logger.info("Request");

		httpSessionUtils.isAllowedApp(request);

		List<LeadDetails> leadDetails = leadsquaredManager.fetchLeadsByPhone(phone);
		List<Map<String,String>> leadDetailsList = new ArrayList<>();
		if (leadDetails != null) {
			for(LeadDetails leadDetail : leadDetails){
				Map<String , String> map =  new HashMap<>();
				map.put("emailAddress",leadDetail.getEmailAddress());
				map.put("prospectId",leadDetail.getProspectID());
				leadDetailsList.add(map);
			}
		}
		return leadDetailsList;
	}

	@RequestMapping(value = "/getLeadActivityByLeadId", method = RequestMethod.GET)
	@ApiOperation(value = "Get lead activity by lead id", notes = "LeadActivity")
	public List<LeadActivity> getLeadActivityByLeadId(@RequestParam(name = "leadId", required = true) String leadId)
			throws VException {
		logger.info("Request");

		httpSessionUtils.checkIfAllowedList(null, Arrays.asList(Role.ADMIN, Role.STUDENT_CARE), true);

		return leadsquaredActivityManager.fetchLeadActivityByLeadId(leadId);
	}

	@RequestMapping(value = "/getAllLeads", method = RequestMethod.GET)
	@ApiOperation(value = "Get lead by day", notes = "LeadDetails")
	public BasicResponse getAllLeads(@RequestParam(name = "startTime", required = true) long startTime,
			@RequestParam(name = "endTime", required = true) long endTime) throws Exception {
		logger.info("Request");

		httpSessionUtils.checkIfAllowedList(null, Arrays.asList(Role.ADMIN, Role.STUDENT_CARE), true);

		long currentTime = System.currentTimeMillis();
		leadsquaredManager.fetchAllLeads(startTime, endTime);
		logger.info("Time taken:" + (System.currentTimeMillis() - currentTime));
		return new BasicResponse();
	}

	@RequestMapping(value = "/syncAllLeadsPreviousHour", method = RequestMethod.GET)
	@ApiOperation(value = "Get lead by day", notes = "LeadDetails")
	public BasicResponse syncAllLeadsPreviousHour() throws Exception {
		logger.info("Request");

		httpSessionUtils.checkIfAllowedList(null, Arrays.asList(Role.ADMIN, Role.STUDENT_CARE), true);

		long currentTime = System.currentTimeMillis();
		currentTime -= currentTime % DateTimeUtils.MILLIS_PER_MINUTE;
		leadsquaredManager.fetchLeadsByTimeRange(currentTime - DateTimeUtils.MILLIS_PER_HOUR, currentTime);
		logger.info("Time taken:" + (System.currentTimeMillis() - currentTime));
		return new BasicResponse();
	}

	@RequestMapping(value = "/getLeadsByDay", method = RequestMethod.GET)
	@ApiOperation(value = "Get lead by day", notes = "LeadDetails")
	public List<String> getLeadsByTimeRange(@RequestParam(name = "startTime", required = true) long startTime)
			throws Exception {
		logger.info("Request");

		httpSessionUtils.checkIfAllowedList(null, Arrays.asList(Role.ADMIN, Role.STUDENT_CARE), true);

		long currentTime = System.currentTimeMillis();
		List<String> results = leadsquaredManager.fetchLeadsByDay(startTime);
		logger.info("Results:" + Arrays.toString(results.toArray()));
		logger.info("Time taken:" + (System.currentTimeMillis() - currentTime));
		return results;
	}

	@RequestMapping(value = "/getLeadsByTimeRange", method = RequestMethod.GET)
	@ApiOperation(value = "Get lead by time interval", notes = "LeadDetails")
	public List<LeadDetails> getLeadsByTimeRange(@RequestParam(name = "startTime", required = true) long startTime,
			@RequestParam(name = "endTime", required = true) long endTime) throws Exception {
		logger.info("Request");

		httpSessionUtils.checkIfAllowedList(null, Arrays.asList(Role.ADMIN, Role.STUDENT_CARE), true);

		return leadsquaredManager.fetchLeadsByTimeRange(startTime, endTime);
	}

	@RequestMapping(value = "/getAllLeadActivites", method = RequestMethod.GET)
	@ApiOperation(value = "Get lead activites by day", notes = "LeadDetails")
	public BasicResponse getAllLeadActivites(@RequestParam(name = "startTime", required = true) long startTime,
			@RequestParam(name = "endTime", required = true) long endTime) throws Exception {
		logger.info("Request");

		httpSessionUtils.checkIfAllowedList(null, Arrays.asList(Role.ADMIN, Role.STUDENT_CARE), true);

		long currentTime = System.currentTimeMillis();
		leadsquaredActivityManager.fetchAllLeadActivities(startTime, endTime);
		logger.info("Time taken:" + (System.currentTimeMillis() - currentTime));
		return new BasicResponse();
	}

	@RequestMapping(value = "/syncAllLeadActivitesPreviousHour", method = RequestMethod.GET)
	@ApiOperation(value = "Get lead activites by day", notes = "LeadDetails")
	public BasicResponse syncAllLeadActivitesPreviousHour() throws Exception {
		logger.info("Request");

		httpSessionUtils.checkIfAllowedList(null, Arrays.asList(Role.ADMIN, Role.STUDENT_CARE), true);

		leadsquaredActivityManager.syncAllLeadActivitesPreviousHour();
		return new BasicResponse();
	}

	@RequestMapping(value = "/getLeadsActivitesByDay", method = RequestMethod.GET)
	@ApiOperation(value = "Get lead by time interval", notes = "LeadDetails")
	public BasicResponse getLeadsActivitesByDay(@RequestParam(name = "startTime", required = true) long startTime)
			throws VException, FileNotFoundException, IOException {
		logger.info("Request");

		httpSessionUtils.checkIfAllowedList(null, Arrays.asList(Role.ADMIN, Role.STUDENT_CARE), true);

		long currentTime = System.currentTimeMillis();
		leadsquaredActivityManager.fetchLeadActivityByDay(startTime);
		logger.info("Timetaken:" + (System.currentTimeMillis() - currentTime));
		return new BasicResponse();
	}

	@RequestMapping(value = "/getLeadsActivitesByTimeRange", method = RequestMethod.GET)
	@ApiOperation(value = "Get lead by time interval", notes = "LeadDetails")
	public int getLeadActivityByTimeRange(@RequestParam(name = "startTime", required = true) long startTime,
			@RequestParam(name = "endTime", required = true) long endTime) throws Exception {
		logger.info("Request");

		httpSessionUtils.checkIfAllowedList(null, Arrays.asList(Role.ADMIN, Role.STUDENT_CARE), true);

		return leadsquaredActivityManager.fetchLeadActivityByTimeRange(startTime, endTime);
	}

	@RequestMapping(value = "/updateUserData", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public int updateUserData(@RequestParam(name = "startTime", required = true) long startTime,
			@RequestParam(name = "endTime", required = true) long endTime) throws Exception {

		httpSessionUtils.checkIfAllowedList(null, Arrays.asList(Role.ADMIN, Role.STUDENT_CARE), true);

		return userManager.updateUserData(startTime, endTime);
	}
	//
	// @RequestMapping(value = "/getLeadCallActivityCount", method =
	// RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces
	// = MediaType.APPLICATION_JSON_VALUE)
	// public long updateUserData(@RequestParam(name = "email", required =
	// false) String email,
	// @RequestParam(name = "phone", required = false) String phone) throws
	// Exception {
	// return leadsquaredManager.getLeadCallActivityCount(email, phone);
	// }
	//
	// @RequestMapping(value = "/mergeLeadFields", method = RequestMethod.POST,
	// consumes = MediaType.APPLICATION_JSON_VALUE, produces =
	// MediaType.APPLICATION_JSON_VALUE)
	// public long mergeLeadFields(@RequestParam(name = "startTime", required =
	// true) long startTime,
	// @RequestParam(name = "endTime", required = true) long endTime) throws
	// Exception {
	// return leadsquaredManager.mergeLeadFields(startTime, endTime);
	// }

	@RequestMapping(value = "/fixPhoneNumberField", method = RequestMethod.POST)
	@ApiOperation(value = "fix phone number field", notes = "PhoneNumber field")
	public long fixPhoneNumberField(@RequestBody String leadIds) throws VException, InterruptedException {
		logger.info("Request");


		httpSessionUtils.checkIfAllowedList(null, Arrays.asList(Role.ADMIN, Role.STUDENT_CARE), true);

		return leadsquaredManager.fixPhoneNumberField(leadIds);
	}

	@RequestMapping(value = "/getLeadContextField", method = RequestMethod.POST)
	@ApiOperation(value = "getLeadContextField", notes = "PhoneNumber field")
	public String getLeadContextField(@RequestBody String leadIds) throws VException, InterruptedException {
		logger.info("Request");

		httpSessionUtils.checkIfAllowedList(null, Arrays.asList(Role.ADMIN, Role.STUDENT_CARE), true);

		return leadsquaredManager.getLeadContextField(leadIds);
	}

	@RequestMapping(value = "/updateLeadContextSource", method = RequestMethod.POST)
	@ApiOperation(value = "updateLeadContextSource", notes = "PhoneNumber field")
	public String updateLeadContextSource(@RequestBody String leadIds,
			@RequestParam(name = "phone", required = false) String source)
			throws VException, InterruptedException {

		logger.info("Request");

		httpSessionUtils.checkIfAllowedList(null, Arrays.asList(Role.ADMIN, Role.STUDENT_CARE), true);

		return leadsquaredManager.uploadVedantuDataSource(leadIds, source);
	}

	@RequestMapping(value = "/getLeadsForMerging", method = RequestMethod.GET)
	@ApiOperation(value = "getLeadsForMerging", notes = "PhoneNumber field")
	public Set<String> getLeadsForMerging(@RequestParam(name = "email", required = false) String email,
			@RequestParam(name = "phone", required = false) String phoneNumber)
			throws NotFoundException, InterruptedException, ParseException {
		logger.info("Request");
		return leadsquaredManager.getLeadsForMerging(email, phoneNumber);
	}

	@RequestMapping(value = "/mergeLeads", method = RequestMethod.GET)
	@ApiOperation(value = "getLeadsForMerging", notes = "PhoneNumber field")
	public void mergeLeads(@RequestParam(name = "parentId", required = false) String parentId,
			@RequestParam(name = "childId", required = false) String childId) throws Exception {
		logger.info("Request");

		httpSessionUtils.checkIfAllowedList(null, Arrays.asList(Role.ADMIN, Role.STUDENT_CARE), true);

		leadsquaredManager.mergeLeadFields(parentId, childId);
	}

	@RequestMapping(value = "/processLeadsquaredData", method = RequestMethod.GET)
	@ApiOperation(value = "processLeadsquaredData", notes = "lead data and activities")
	public void processLeadsquaredData(@RequestParam(name = "parentId", required = false) String parentId,
			@RequestParam(name = "childId", required = false) String childId) throws Exception {
		logger.info("Request");

		httpSessionUtils.checkIfAllowedList(null, Arrays.asList(Role.ADMIN, Role.STUDENT_CARE), true);

		long currentTime = System.currentTimeMillis();
		currentTime -= (currentTime % DateTimeUtils.MILLIS_PER_MINUTE);
		leadsquaredAsyncTaskManager.processLeadsquaredDate(currentTime,
				currentTime + 10 * DateTimeUtils.MILLIS_PER_MINUTE);
	}

	@RequestMapping(value = "/updateLeadTask", method = RequestMethod.POST)
	@ApiOperation(value = "updateLeadTask", notes = "updateLeadTask")
	public LeadTask updateLeadTask(@RequestBody String request) throws Exception {
		logger.info("Request : " + request);

		httpSessionUtils.checkIfAllowedList(null, Arrays.asList(Role.ADMIN, Role.STUDENT_CARE), true);

		LeadTask leadTask = new Gson().fromJson(request, LeadTask.class);
		leadsquaredTaskManager.updateLeadTask(leadTask);
		return leadTask;
	}

	@CrossOrigin
	@RequestMapping(value = "/getLeadByVcCode", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public boolean getLeadsquaredLeadsByVcCode(@RequestParam(name = "vcCode") String vcCode)
		throws VException, InterruptedException {
			logger.info("Request vcCode : "+vcCode);

		httpSessionUtils.checkIfAllowedList(null, Arrays.asList(Role.ADMIN, Role.STUDENT_CARE), true);

		return leadsquaredManager.getLeadsquaredLeadsByVcCode(vcCode);
	}
	@RequestMapping(value = "/postLeadByVcCode", method = RequestMethod.POST)
	@ApiOperation(value = "LeadVcCode inserting", notes = "LeadVcCode")
	public PlatformBasicResponse setVcCode(@RequestParam(name = "email", required = false) String email,
										   @RequestParam(name = "vcCode", required = false) String vcCode,
										   @RequestParam(name = "phone", required = false) String phone,
										   @RequestParam(name = "mx_VCLead", required = false) String mx_VCLead) throws Exception {
		logger.info("Request vcCode : "+vcCode+" email: "+ email +" phone: "+ phone +" mx_VCLead : "+mx_VCLead);

		httpSessionUtils.checkIfAllowedList(null, Arrays.asList(Role.ADMIN, Role.STUDENT_CARE), true);

		return leadsquaredManager.setVcCode(vcCode,email,phone,mx_VCLead);
	}

	@RequestMapping(value = "/postLeadByVcCodeBatch", method = RequestMethod.POST)
	public PlatformBasicResponse setVcCodes(@RequestBody List<LeadVcCodeReq> leadVcCodeReqs) throws  Exception {

		httpSessionUtils.checkIfAllowedList(null, Arrays.asList(Role.ADMIN, Role.STUDENT_CARE), true);

		return leadsquaredManager.setVcCodes(leadVcCodeReqs);
	}

	@RequestMapping(value = "/updatePaymentThroughSNS", method = RequestMethod.POST, consumes = MediaType.TEXT_PLAIN_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public void updatePaymentThroughSNS(@RequestHeader(value = "x-amz-sns-message-type") String messageType,
									@RequestBody String request) throws Exception {
		logger.info("sns controller for updatePaymentThroughSNS with messegeType : "+messageType);
		Gson gson = new Gson();
		AWSSNSSubscriptionRequest subscriptionRequest = gson.fromJson(request, AWSSNSSubscriptionRequest.class);
		if (messageType.equals("SubscriptionConfirmation")) {
			String json = null;
			logger.info("SubscribeURL = " + subscriptionRequest.getSubscribeURL());
			ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionRequest.getSubscribeURL(), HttpMethod.GET, json, false);
			logger.info(resp.getEntity(String.class));
		} else if (messageType.equals("Notification")) {
			logger.info("Notification received - SNS");
			logger.info(subscriptionRequest.toString());
			logger.info("sns payload message : "+subscriptionRequest.getMessage());
			leadsquaredManager.updatePaymentThroughSNS(subscriptionRequest.getMessage());
		}
	}

	@RequestMapping(value = "/updateSalesUsersSessionData", method = RequestMethod.POST, consumes = MediaType.TEXT_PLAIN_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public void updateSalesUsersSessionData(@RequestHeader(value = "x-amz-sns-message-type") String messageType,
										@RequestBody String request) throws Exception {
		logger.info("sns controller for updatePaymentThroughSNS with messegeType : "+messageType);
		Gson gson = new Gson();
		AWSSNSSubscriptionRequest subscriptionRequest = gson.fromJson(request, AWSSNSSubscriptionRequest.class);
		if (messageType.equals("SubscriptionConfirmation")) {
			String json = null;
			logger.info("SubscribeURL = " + subscriptionRequest.getSubscribeURL());
			ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionRequest.getSubscribeURL(), HttpMethod.GET, json, false);
			logger.info(resp.getEntity(String.class));
		} else if (messageType.equals("Notification")) {
			logger.info("Notification received - SNS");
			logger.info(subscriptionRequest.toString());
			logger.info("sns payload message : "+subscriptionRequest.getMessage());
			leadsquaredManager.updateSalesUsersSessionData(subscriptionRequest.getMessage());
		}
	}
}
