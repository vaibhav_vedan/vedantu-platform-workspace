package com.vedantu.vedantudata.controllers;

import java.io.File;
import java.util.Arrays;

import com.vedantu.User.Role;
import com.vedantu.util.security.HttpSessionUtils;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.vedantu.util.CommonUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.vedantudata.entities.ConvoxUploadDetails;
import com.vedantu.vedantudata.managers.upload.ConvoxManager;
import com.vedantu.vedantudata.request.ConvoxUploadReq;

@RestController
@RequestMapping("/convox")
public class ConvoxUploadController {
	@Autowired
	private ConvoxManager convoxManager;

	@Autowired
	private LogFactory logFactory;

	@Autowired
	private HttpSessionUtils httpSessionUtils;

	@SuppressWarnings("static-access")
	private Logger logger = logFactory.getLogger(ConvoxUploadController.class);

	@RequestMapping(value = "/upload", method = RequestMethod.POST, headers = "content-type=multipart/form-data", produces = MediaType.APPLICATION_JSON_VALUE)
	public ConvoxUploadDetails convoxUpload(@RequestParam(name = "file", required = true) MultipartFile multipart,
			@ModelAttribute ConvoxUploadReq convoxUploadReq) throws Exception {
		logger.info("Request:" + System.currentTimeMillis());

		httpSessionUtils.checkIfAllowedList(null, Arrays.asList(Role.ADMIN, Role.STUDENT_CARE), true);

		File file = CommonUtils.multipartToFile(multipart);
		ConvoxUploadDetails convoxUploadDetails = convoxManager.uploadCallData(file, convoxUploadReq);
		file.delete();
		return convoxUploadDetails;
	}
}
