package com.vedantu.vedantudata.controllers;

import com.vedantu.User.Role;
import com.vedantu.exception.VException;
import com.vedantu.util.CollectionUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.security.HttpSessionUtils;
import com.vedantu.vedantudata.managers.ScVglanceManager;
import com.vedantu.vedantudata.pojos.SalesConversionApiResponseManager;
import com.vedantu.vedantudata.pojos.request.SalesVglanceReq;
import com.vedantu.vedantudata.pojos.response.*;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping("/salesc/vglance")
public class ScVglanceController {

    @Autowired
    private ScVglanceManager scVglanceManager;

    @Autowired
    private HttpSessionUtils sessionUtils;

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(ScVglanceController.class);

    @RequestMapping(value = "/isStudentBelongstoCounsellor", method = RequestMethod.GET)
    @ResponseBody
    public SalesConversionApiResponseManager isStudentBelongstoCounsellor(@RequestParam(name = "studentEmail", required = true) String studentEmail, @RequestParam(name = "studentContactNumber", required = true) String studentContactNumber) throws InterruptedException, VException {

        logger.info("isStudentBelongstoCounsellor studentEmail: {}", studentEmail);

        sessionUtils.checkIfAllowedList(null, Arrays.asList(Role.ADMIN, Role.TEACHER), true);
        String counsellorEmail = sessionUtils.getCurrentSessionData().getEmail();
        Boolean isStudentBelongsToCounsellor = scVglanceManager.isStudentBelongstoCounsellor(studentEmail, studentContactNumber, counsellorEmail);
        SalesConversionApiResponseManager response = new SalesConversionApiResponseManager.Builder()
                .responseStatus("SUCCESS")
                .payload(isStudentBelongsToCounsellor)
                .build();

        return response;
    }

    @RequestMapping(value = "/getSessionChecks", method = RequestMethod.POST)
    @ResponseBody
    public SalesConversionApiResponseManager getSessionChecks(@RequestBody SalesVglanceReq salesVglanceReq) throws InterruptedException, VException {

        logger.info("getSessionChecks userId: {}", salesVglanceReq);

        sessionUtils.checkIfAllowedList(null, Arrays.asList(Role.ADMIN, Role.TEACHER), true);
        SalesConversionApiResponseManager response;
        List<String> errors = new SalesVglanceReq().validate(salesVglanceReq);
        if (!CollectionUtils.isEmpty(errors)) {
            response = new SalesConversionApiResponseManager.Builder()
                    .responseStatus("ERROR")
                    .responseMessage("Request fields are missing..")
                    .payload(errors)
                    .build();
            return response;
        }
        GetSessionChecksRes getSessionChecksRes = scVglanceManager.getSessionChecks(salesVglanceReq);
        response = new SalesConversionApiResponseManager.Builder()
                .responseStatus("SUCCESS")
                .payload(getSessionChecksRes)
                .build();

        return response;
    }

    @RequestMapping(value = "/getMasterLiveClassDetails", method = RequestMethod.POST)
    @ResponseBody
    public SalesConversionApiResponseManager getMasterLiveClassDetails(@RequestBody SalesVglanceReq salesVglanceReq) throws InterruptedException, VException {

        logger.info("getMasterLiveClassDetails req: {}", salesVglanceReq);

        sessionUtils.checkIfAllowedList(null, Arrays.asList(Role.ADMIN, Role.TEACHER), true);
        SalesConversionApiResponseManager response;
        List<String> errors = new SalesVglanceReq().validate(salesVglanceReq);
        if (!CollectionUtils.isEmpty(errors)) {
            response = new SalesConversionApiResponseManager.Builder()
                    .responseStatus("ERROR")
                    .responseMessage("Request fields are missing..")
                    .payload(errors)
                    .build();
            return response;
        }

        GetMasterLiveClassDetailsRes getMasterLiveClassDetailsRes = scVglanceManager.getMasterLiveClassDetails(salesVglanceReq);
        response = new SalesConversionApiResponseManager.Builder()
                .responseStatus("SUCCESS")
                .payload(getMasterLiveClassDetailsRes)
                .build();

        return response;
    }

    @RequestMapping(value = "/getVSATDetails", method = RequestMethod.POST)
    @ResponseBody
    public SalesConversionApiResponseManager getVSATDetails(@RequestBody SalesVglanceReq salesVglanceReq) throws InterruptedException, VException {

        logger.info("getVSATDetails req: {}", salesVglanceReq);

        sessionUtils.checkIfAllowedList(null, Arrays.asList(Role.ADMIN, Role.TEACHER), true);
        SalesConversionApiResponseManager response;
        List<String> errors = new SalesVglanceReq().validate(salesVglanceReq);
        if (!CollectionUtils.isEmpty(errors)) {
            response = new SalesConversionApiResponseManager.Builder()
                    .responseStatus("ERROR")
                    .responseMessage("Request fields are missing..")
                    .payload(errors)
                    .build();
            return response;
        }
        GetVSATDetailsRes getVSATDetailsRes = scVglanceManager.getVSATDetails(salesVglanceReq);
        response = new SalesConversionApiResponseManager.Builder()
                .responseStatus("SUCCESS")
                .payload(getVSATDetailsRes)
                .build();

        return response;
    }

    @RequestMapping(value = "/getTotalLearningDetails", method = RequestMethod.POST)
    @ResponseBody
    public SalesConversionApiResponseManager getTotalLearningDetails(@RequestBody SalesVglanceReq salesVglanceReq) throws InterruptedException, VException {

        logger.info("getTotalLearningDetails req: {}", salesVglanceReq);

        sessionUtils.checkIfAllowedList(null, Arrays.asList(Role.ADMIN, Role.TEACHER), true);
        SalesConversionApiResponseManager response;
        List<String> errors = new SalesVglanceReq().validate(salesVglanceReq);
        if (!CollectionUtils.isEmpty(errors)) {
            response = new SalesConversionApiResponseManager.Builder()
                    .responseStatus("ERROR")
                    .responseMessage("Request fields are missing..")
                    .payload(errors)
                    .build();
            return response;
        }
        GetTotalLearningDetailsRes getTotalLearningDetailsRes = scVglanceManager.getTotalLearningDetails(salesVglanceReq);
        response = new SalesConversionApiResponseManager.Builder()
                .responseStatus("SUCCESS")
                .payload(getTotalLearningDetailsRes)
                .build();

        return response;
    }

    @RequestMapping(value = "/getTimeSpentDataMetrics", method = RequestMethod.POST)
    @ResponseBody
    public SalesConversionApiResponseManager getTimeSpentDataMetrics(@RequestBody SalesVglanceReq salesVglanceReq) throws InterruptedException, VException {

        logger.info("getTimeSpentDataMetrics req: {}", salesVglanceReq);

        sessionUtils.checkIfAllowedList(null, Arrays.asList(Role.ADMIN, Role.TEACHER), true);
        SalesConversionApiResponseManager response;
        List<String> errors = new SalesVglanceReq().validate(salesVglanceReq);
        if (!CollectionUtils.isEmpty(errors)) {
            response = new SalesConversionApiResponseManager.Builder()
                    .responseStatus("ERROR")
                    .responseMessage("Request fields are missing..")
                    .payload(errors)
                    .build();
            return response;
        }
        GetTimeSpentDataMetricsRes getTimeSpentDataMetricsRes = scVglanceManager.getTimeSpentDataMetrics(salesVglanceReq);
        response = new SalesConversionApiResponseManager.Builder()
                .responseStatus("SUCCESS")
                .payload(getTimeSpentDataMetricsRes)
                .build();

        return response;
    }

}
