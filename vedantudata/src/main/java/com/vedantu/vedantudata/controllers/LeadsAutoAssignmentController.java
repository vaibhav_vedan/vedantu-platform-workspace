package com.vedantu.vedantudata.controllers;


import com.vedantu.User.Role;
import com.vedantu.exception.VException;
import com.vedantu.util.LogFactory;
import com.vedantu.util.PlatformBasicResponse;
import com.vedantu.util.StringUtils;
import com.vedantu.util.security.HttpSessionUtils;
import com.vedantu.vedantudata.managers.LeadsAutoAssignmentManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.mail.internet.AddressException;
import java.sql.SQLException;
import java.text.ParseException;

@RestController
public class LeadsAutoAssignmentController {

    @Autowired
    private LeadsAutoAssignmentManager leadsAutoAssignmentManager;

    @Autowired
    private HttpSessionUtils httpSessionUtils;

    @Autowired
    private LogFactory logFactory;
    private Logger logger = logFactory.getLogger(LeadsAutoAssignmentController.class);


    @RequestMapping(value = "/assignLeadstoCounsellorsFromCron", method = RequestMethod.POST, consumes = MediaType.TEXT_PLAIN_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void assignLeadstoCounsellorsFromCron(@RequestHeader(value = "x-amz-sns-message-type") String messageType, @RequestBody String request) throws Throwable {
        logger.info("assignLeadstoCounsellorsFromCron req: {}", request);
        leadsAutoAssignmentManager.assignLeadstoCounsellorsFromCron(request, messageType);
    }

    @RequestMapping(value = "/assignLeadstoCounsellors", method = RequestMethod.GET)
    @ResponseBody
    public void assignLeadstoCounsellors() throws SQLException, VException, AddressException, ParseException, InterruptedException {
        httpSessionUtils.checkIfAllowed(null, Role.ADMIN, true);
        leadsAutoAssignmentManager.processAsync();
    }

}
