package com.vedantu.vedantudata.controllers;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.vedantu.util.LogFactory;
import com.vedantu.vedantudata.managers.leadsquared.LeadSquaredWebhooksManager;
import com.vedantu.vedantudata.pojos.ActivityCallbackPojoMapper;
import com.vedantu.vedantudata.pojos.ActivityCreatedCallbackPojo;
import com.vedantu.vedantudata.pojos.ActivityDetailsCallbackPojo;
import io.swagger.annotations.ApiOperation;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/callback")
public class LeadSquaredDemoWebhooksController {

    @Autowired
    private LeadSquaredWebhooksManager leadSquaredWebhooksManager;

    @Autowired
    private LogFactory logFactory;

    private Logger logger = logFactory.getLogger(LeadSquaredDemoWebhooksController.class);

    @RequestMapping(value = "/demo-created", method = RequestMethod.POST)
    @ApiOperation(value = "Callback", notes = "Update Activity Callback")
    public @ResponseBody
    Map<String, String> captureDemoCallback(@RequestBody String activityCallbackPojoMapper) {

        try {

            logger.info("\n\n demo-created callback :: {}", activityCallbackPojoMapper);

            Type listType = new TypeToken<ArrayList<ActivityCreatedCallbackPojo>>() {
            }.getType();

            List<ActivityCreatedCallbackPojo> activityCallbackPojoMapperObject = new Gson().fromJson(activityCallbackPojoMapper, listType);
            leadSquaredWebhooksManager.captureDemoCallback(activityCallbackPojoMapperObject.get(0));

            Map<String, String> response = new HashMap<>();
            response.put("Status", "Success");
            response.put("StatusReason", "");
            return response;
        } catch (Exception e) {
            logger.error(e.getMessage(),e);
            Map<String, String> response = new HashMap<>();
            response.put("Status", "Error");
            response.put("StatusReason", e.getMessage());

            return response;

        }
    }


    @RequestMapping(value = "/demo-created", method = RequestMethod.GET)
    @ApiOperation(value = "Callback", notes = "Update Activity Callback")
    public @ResponseBody
    Map<String, String> captureDemoCallbackGet() {

        Map<String, String> response = new HashMap<>();
        response.put("Status", "Success");
        response.put("StatusReason", "");
        return response;
    }

    @RequestMapping(value = "/demo-created", method = RequestMethod.HEAD)
    @ApiOperation(value = "Callback", notes = "Update Activity Callback")
    public @ResponseBody
    Map<String, String> captureDemoCallbackHead() {

        Map<String, String> response = new HashMap<>();
        response.put("Status", "Success");
        response.put("StatusReason", "0");

        return response;
    }

}
