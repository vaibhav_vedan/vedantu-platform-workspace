package com.vedantu.vedantudata.controllers;

import java.util.Arrays;
import java.util.List;
import java.util.Set;

import com.vedantu.User.Role;
import com.vedantu.util.PlatformBasicResponse;
import com.vedantu.util.security.HttpSessionUtils;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.vedantu.exception.NotFoundException;
import com.vedantu.exception.VException;
import com.vedantu.util.LogFactory;
import com.vedantu.vedantudata.entities.leadsquared.LeadsquaredUser;
import com.vedantu.vedantudata.managers.leadsquared.LeadsquaredUserManager;
import com.vedantu.vedantudata.request.UpdateConvoxAgentIdReq;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/leadsquared/user")
public class LeadsquaredUserController {
	@Autowired
	private LeadsquaredUserManager leadsquaredUserManager;

	@Autowired
	private LogFactory logFactory;

	@SuppressWarnings("static-access")
	private Logger logger = logFactory.getLogger(LeadsquaredUserController.class);

	@Autowired
	private HttpSessionUtils httpSessionUtils;

	@RequestMapping(value = "/update", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	@ApiOperation(value = "Update users", notes = "LeadsquaredUser")
	public List<LeadsquaredUser> createSession() throws Exception {
		logger.info("Request");

		httpSessionUtils.checkIfAllowedList(null, Arrays.asList(Role.ADMIN, Role.STUDENT_CARE), true);

		return leadsquaredUserManager.updateLeadsquaredUsers();
	}

	@RequestMapping(value = "/updateConvoxAgentId", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	@ApiOperation(value = "Update agent", notes = "LeadsquaredUser")
	public LeadsquaredUser updateConvoxAgentId(@RequestParam(name = "emailId", required = true) String emailId,
			@RequestParam(name = "agentId", required = true) String agentId) throws Exception {
		logger.info("Request");

		httpSessionUtils.checkIfAllowedList(null, Arrays.asList(Role.ADMIN, Role.STUDENT_CARE), true);

		return leadsquaredUserManager.updateLeadsquaredAgentId(emailId, agentId);
	}

	@RequestMapping(value = "/updateConvoxAgentIds", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	@ApiOperation(value = "updateConvoxAgentIds", notes = "LeadsquaredUser")
	public int updateConvoxAgentIds(@RequestBody List<UpdateConvoxAgentIdReq> req) throws Exception {
		logger.info("Request");
		httpSessionUtils.checkIfAllowedList(null, Arrays.asList(Role.ADMIN, Role.STUDENT_CARE), true);
		return leadsquaredUserManager.updateLeadsquaredAgentIds(req);
	}

	@RequestMapping(value = "/getUsers", method = RequestMethod.GET)
	@ApiOperation(value = "Get users", notes = "LeadsquaredUsers")
	public List<LeadsquaredUser> getLeadsquaredUsers() throws VException {
		logger.info("Request");
		httpSessionUtils.checkIfAllowedList(null, Arrays.asList(Role.ADMIN, Role.STUDENT_CARE), true);
		return leadsquaredUserManager.getLeadsquaredUsers();
	}

	@RequestMapping(value = "/getUserRoles", method = RequestMethod.GET)
	@ApiOperation(value = "Get user roles", notes = "LeadsquaredUsers roles")
	public Set<String> getUserRoles() throws VException {
		logger.info("Request");
		httpSessionUtils.checkIfAllowedList(null, Arrays.asList(Role.ADMIN, Role.STUDENT_CARE), true);
		return leadsquaredUserManager.getLeadsquaredRoles();
	}

	@RequestMapping(value = "/getUserByEmailId", method = RequestMethod.GET)
	@ApiOperation(value = "Get user roles", notes = "LeadsquaredUsers roles")
	public LeadsquaredUser getUserByEmailId(@RequestParam(name = "emailId", required = true) String emailId)
			throws VException {
		logger.info("Request");
		httpSessionUtils.checkIfAllowedList(null, Arrays.asList(Role.ADMIN, Role.STUDENT_CARE), true);
		return leadsquaredUserManager.getLeadsquaredUserByEmailId(emailId);
	}

	@RequestMapping(value = "/getDataFromCSVAndInsert", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public PlatformBasicResponse getDataFromCSVAndInsert() throws Exception {
		logger.info("getDataFromCSVAndInsert");
		httpSessionUtils.checkIfAllowedList(null, Arrays.asList(Role.ADMIN, Role.STUDENT_CARE), true);
		long noInserted = leadsquaredUserManager.getDataFromCSVAndInsert();
		PlatformBasicResponse platformBasicResponse = new PlatformBasicResponse();
		platformBasicResponse.setResponse("inserted count " + noInserted);
		return platformBasicResponse;
	}

}
