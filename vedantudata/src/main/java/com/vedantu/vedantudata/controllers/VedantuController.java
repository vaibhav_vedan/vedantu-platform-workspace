package com.vedantu.vedantudata.controllers;

import com.google.gson.Gson;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.User.Role;
import com.vedantu.async.AsyncTaskFactory;
import com.vedantu.async.AsyncTaskParams;
import com.vedantu.exception.VException;
import com.vedantu.session.pojo.OTFSessionPojoUtils;
import com.vedantu.util.CollectionUtils;
import com.vedantu.util.PlatformBasicResponse;
import com.vedantu.util.StringUtils;
import com.vedantu.util.WebUtils;
import com.vedantu.util.security.HttpSessionUtils;
import com.vedantu.vedantudata.async.AsyncTaskName;
import com.vedantu.vedantudata.entities.WebinarInfo;
import com.vedantu.vedantudata.entities.WebinarUserRegistrationInfo;
import com.vedantu.vedantudata.managers.GTWManager;
import com.vedantu.vedantudata.managers.VedantuManager;
import com.vedantu.vedantudata.pojos.AwsSNSRequest;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.*;


@RestController
@RequestMapping("/vedantu/")
public class VedantuController {

    @Autowired
    private VedantuManager vedantuManager;

    @Autowired
    private GTWManager gtwManager;

    @Autowired
    private AsyncTaskFactory asyncTaskFactory;

    @Autowired
    private HttpSessionUtils httpSessionUtils;

//    @RequestMapping(value = "/order/get", method = RequestMethod.GET)
//    @ApiOperation(value = "GetOrders", notes = "LeadsquaredUsers")
//    public List<VedantuDataOrder> generateOrderSheets(@RequestParam(name = "startTime", required = true) long startTime,
//            @RequestParam(name = "endTime", required = true) long endTime,
//            @RequestParam(name = "fetchAgent", required = false) Boolean fetchAgent) throws VException {
//        return vedantuManager.generateOrderSheets(startTime, endTime, fetchAgent);
//    }
//
//    @SuppressWarnings("unused")
//    @RequestMapping(value = "/order/updateOrderSheet/hour", method = RequestMethod.POST, consumes = MediaType.TEXT_PLAIN_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
//    @ResponseBody
//    public void updateOrderSheetHourly(@RequestHeader(value = "x-amz-sns-message-type") String messageType,
//            @RequestBody String request) throws Exception {
//        AwsSNSRequest subscriptionRequest = new Gson().fromJson(request, AwsSNSRequest.class);
//        if (messageType.equals("SubscriptionConfirmation")) {
//            String json = null;
//            ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionRequest.getSubscribeURL(), HttpMethod.GET, json, false);
//        } else if (messageType.equals("Notification")) {
//            vedantuManager.updateOrderSheetHourly();
//        }
//    }

    @RequestMapping(value = "/onetofew/attendees/report", method = RequestMethod.GET)
    @ApiOperation(value = "GetOrders", notes = "LeadsquaredUsers")
    public List<OTFSessionPojoUtils> otfAttendeeReport(@RequestParam(name = "startTime", required = true) long startTime,
            @RequestParam(name = "endTime", required = true) long endTime) throws Exception {
        return new ArrayList<>();
//        return vedantuManager.otfAttendeeReport(startTime, endTime);
    }

//    @SuppressWarnings("unused")
//    @RequestMapping(value = "/onetofew/attendees/report/daily", method = RequestMethod.POST, consumes = MediaType.TEXT_PLAIN_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
//    @ResponseBody
//    public void otfAttendeeReportDaily(@RequestHeader(value = "x-amz-sns-message-type") String messageType,
//            @RequestBody String request) throws Exception {
//        AwsSNSRequest subscriptionRequest = new Gson().fromJson(request, AwsSNSRequest.class);
//        if (messageType.equals("SubscriptionConfirmation")) {
//            String json = null;
//            ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionRequest.getSubscribeURL(), HttpMethod.GET, json, false);
//        } else if (messageType.equals("Notification")) {
//            vedantuManager.otfAttendeeReportDaily();
//        }
//    }

    @RequestMapping(value = "/leadActivity/searchLeadsByActivity", method = RequestMethod.GET)
    @ApiOperation(value = "searchLeadsByActivity", notes = "LeadsquaredUsers")
    public String searchLeadsByActivity(@RequestParam(name = "start", required = true) Long start,
            @RequestParam(name = "end", required = true) Long end,
            @RequestParam(name = "searchString", required = true) String searchString) throws Exception {
        httpSessionUtils.checkIfAllowedList(null, Arrays.asList(Role.ADMIN, Role.STUDENT_CARE), true);
        return vedantuManager.searchLeadsByActivity(searchString, start, end);
    }

    @RequestMapping(value = "/webinar/track/export", method = RequestMethod.GET)
    @ApiOperation(value = "searchLeadsByActivity", notes = "LeadsquaredUsers")
    public String webinarTrackExport(@RequestParam(name = "start", required = true) Long startTime,
            @RequestParam(name = "end", required = true) Long endTime) throws Exception {
        httpSessionUtils.checkIfAllowedList(null, Arrays.asList(Role.ADMIN, Role.STUDENT_CARE), true);
        return vedantuManager.webinarTrackExport(startTime, endTime);
    }

    @RequestMapping(value = "/webinar/register", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "searchLeadsByActivity", notes = "LeadsquaredUsers")
    public PlatformBasicResponse registerWebinarUser(@RequestBody WebinarUserRegistrationInfo req, HttpServletRequest servletRequest) throws Exception {

        httpSessionUtils.isAllowedApp(servletRequest);

        vedantuManager.registerWebinar(req);
        return new PlatformBasicResponse();
    }

    @RequestMapping(value = "/webinar/register/formdata", method = RequestMethod.POST, consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    @ApiOperation(value = "searchLeadsByActivity", notes = "LeadsquaredUsers")
    public PlatformBasicResponse registerWebinarUserFormData(HttpServletRequest req) throws Exception {
        httpSessionUtils.checkIfAllowedList(null, Arrays.asList(Role.ADMIN, Role.STUDENT_CARE), true);
        String jsonStr = req.getParameter("data.json");
        vedantuManager.registerWebinarFormData(jsonStr);
        return new PlatformBasicResponse();
    }

    @RequestMapping(value = "/webinar/register/bulk", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "searchLeadsByActivity", notes = "LeadsquaredUsers")
    public String registerWebinarUser(@RequestBody List<WebinarUserRegistrationInfo> req,
            @RequestParam(name = "webinarId", required = true) String webinarId) throws Exception {

        httpSessionUtils.checkIfAllowedList(null, Arrays.asList(Role.ADMIN, Role.STUDENT_CARE), true);

        int total_count = 0;
        int processed = 0;
        int error = 0;
        if (!CollectionUtils.isEmpty(req)) {
            for (WebinarUserRegistrationInfo entry : req) {
                total_count++;
                try {
                    entry.setWebinarId(webinarId);
                    vedantuManager.registerWebinar(entry);
                    processed++;
                } catch (Exception ex) {
                    error++;
                }
            }
        }

        return "total_" + total_count + ":processed_" + processed + ":error_" + error;
    }

    @RequestMapping(value = "/webinar/registerviamissedcall", method = RequestMethod.GET)
    public PlatformBasicResponse registerviamissedcall(@RequestParam("From") String mobileno,
            @RequestParam("trainingId") String trainingId,
            @RequestParam(name = "smstext", required = false) String smstext) throws Exception {
        if (StringUtils.isNotEmpty(mobileno) && mobileno.length() == 11
                && mobileno.charAt(0) == '0') {
            mobileno = mobileno.substring(1);
        }
        Map<String, Object> payload = new HashMap<>();
        payload.put("mobileno", mobileno);
        payload.put("trainingId", trainingId);
        payload.put("smstext", smstext);
        AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.WEBINAR_REGISTER_VIA_MISSED_CALL,
                payload);
        asyncTaskFactory.executeTask(params);
        return new PlatformBasicResponse();
    }

    @RequestMapping(value = "/webinar/syncinfo/async", method = RequestMethod.POST, consumes = MediaType.TEXT_PLAIN_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void syncWebinarInfos(@RequestHeader(value = "x-amz-sns-message-type") String messageType,
            @RequestBody String request) throws Exception {
        AwsSNSRequest subscriptionRequest = new Gson().fromJson(request, AwsSNSRequest.class);
        if (messageType.equals("SubscriptionConfirmation")) {
            String json = null;
            @SuppressWarnings("unused")
            ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionRequest.getSubscribeURL(), HttpMethod.GET, json, false);
        } else if (messageType.equals("Notification")) {
            vedantuManager.syncWebinarInfosAsync();
        }
    }

    @RequestMapping(value = "/webinar/attendence/process/async", method = RequestMethod.POST, consumes = MediaType.TEXT_PLAIN_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void processWebinarAttendenceAsync(@RequestHeader(value = "x-amz-sns-message-type") String messageType,
            @RequestBody String request) throws Exception {
        AwsSNSRequest subscriptionRequest = new Gson().fromJson(request, AwsSNSRequest.class);
        if (messageType.equals("SubscriptionConfirmation")) {
            String json = null;
            @SuppressWarnings("unused")
            ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionRequest.getSubscribeURL(), HttpMethod.GET, json, false);
        } else if (messageType.equals("Notification")) {
            vedantuManager.processWebinarAttendenceAsync();
        }
    }

    @RequestMapping(value = "/webinar/attendence/process", method = RequestMethod.POST, consumes = MediaType.TEXT_PLAIN_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse processWebinarAttendence(
            @RequestParam(name = "startTime", required = true) long startTime,
            @RequestParam(name = "endTime", required = true) long endTime) throws Exception {
        httpSessionUtils.checkIfAllowedList(null, Arrays.asList(Role.ADMIN, Role.STUDENT_CARE), true);
        vedantuManager.processWebinarAttendence(startTime, endTime);
        return new PlatformBasicResponse();
    }

    @RequestMapping(value = "/webinar/syncinfo", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "searchLeadsByActivity", notes = "LeadsquaredUsers")
    public PlatformBasicResponse syncWebinarInfos(@RequestParam(name = "startTime", required = true) long startTime,
            @RequestParam(name = "endTime", required = true) long endTime) throws Exception {
        httpSessionUtils.checkIfAllowedList(null, Arrays.asList(Role.ADMIN, Role.STUDENT_CARE), true);
        vedantuManager.syncWebinarInfos(startTime, endTime);
        return new PlatformBasicResponse();
    }

    @RequestMapping(value = "/webinar/getinfo", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "searchLeadsByActivity", notes = "LeadsquaredUsers")
    public WebinarInfo getWebinarInfo(@RequestParam(name = "webinarId", required = true) String webinarId)
            throws Exception {
        httpSessionUtils.checkIfAllowedList(null, Arrays.asList(Role.ADMIN, Role.STUDENT_CARE), true);
        return vedantuManager.getWebinarInfo(webinarId);
    }

    @RequestMapping(value = "/webinar/register/cleanup", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "searchLeadsByActivity", notes = "LeadsquaredUsers")
    public String cleanUpWebinarUsers(@RequestParam(name = "startTime", required = true) long startTime,
            @RequestParam(name = "endTime", required = true) long endTime)
            throws Exception {
        httpSessionUtils.checkIfAllowedList(null, Arrays.asList(Role.ADMIN, Role.STUDENT_CARE), true);
        return vedantuManager.cleanUpWebinarUsers(startTime, endTime);
    }

    @RequestMapping(value = "/webinar/reminder", method = RequestMethod.POST, consumes = MediaType.TEXT_PLAIN_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse webinarReminder(@RequestHeader(value = "x-amz-sns-message-type") String messageType,
            @RequestBody String request) throws Exception {
        AwsSNSRequest subscriptionRequest = new Gson().fromJson(request, AwsSNSRequest.class);
        if (messageType.equals("SubscriptionConfirmation")) {
            String json = null;
            ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionRequest.getSubscribeURL(), HttpMethod.GET, json, false);
        } else if (messageType.equals("Notification")) {
            AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.WEBINAR_REMINDER_SMS_SCHEDULER,
                    null);
            asyncTaskFactory.executeTask(params);
        }
        return new PlatformBasicResponse();
    }

    // TODO rename api correctly
    @PostMapping(value = "/subscription/bundle/update/aio-details", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    @ResponseBody
    @ApiOperation(value = "Update AIO Bundle Details", notes = "Update AIO Bundle Details In Subscription Subsystem")
    public void createBundleBulk(@RequestParam(name = "file", required = true) MultipartFile multipart, HttpServletResponse response) throws IOException, VException {

        httpSessionUtils.checkIfAllowedList(null, Arrays.asList(Role.ADMIN, Role.STUDENT_CARE), true);
        File csv = vedantuManager.createBundleBulk(multipart);

        try {
            response.setHeader("Content-Disposition", "attachment; filename=\"" + System.currentTimeMillis() + "\"");
            response.setContentType("text/csv");
            // copy it to response's OutputStream
            org.apache.commons.io.IOUtils.copy(new FileInputStream(csv), response.getOutputStream());
            response.flushBuffer();
        } catch (Exception ex) {
            throw new RuntimeException("IOError writing file to output stream");
        }
    }

    @RequestMapping(value = "/subscription/bundle/update/aio/bundle-details", method = RequestMethod.POST, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    @ResponseBody
    @ApiOperation(value = "Update AIO Bundle Details", notes = "Update AIO Bundle Details In Subscription Subsystem")
    public String updateAIOBundleTagDetails(@RequestParam(name = "file", required = true) MultipartFile multipart, HttpServletResponse response) throws IOException, VException {

        httpSessionUtils.checkIfAllowedList(null, Arrays.asList(Role.ADMIN, Role.STUDENT_CARE), true);
        return vedantuManager.updateAIOBundleTagDetails(multipart);
    }



}
