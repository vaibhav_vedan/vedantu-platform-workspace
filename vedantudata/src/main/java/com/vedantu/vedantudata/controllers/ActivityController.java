package com.vedantu.vedantudata.controllers;

import com.google.gson.Gson;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.User.Role;
import com.vedantu.aws.pojo.AWSSNSSubscriptionRequest;
import com.vedantu.exception.ForbiddenException;
import com.vedantu.exception.VException;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.WebUtils;
import com.vedantu.util.security.HttpSessionUtils;
import com.vedantu.vedantudata.managers.ActivityManager;
import com.vedantu.vedantudata.managers.BankRefundTriggerManager;
import com.vedantu.vedantudata.managers.InstallmentTriggerManager;
import com.vedantu.vedantudata.managers.NotPaidOrdersTriggerManager;
import com.vedantu.vedantudata.utils.ActivityEventConstants;
import io.swagger.annotations.ApiOperation;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/trigger-activity")
public class ActivityController {

    @Autowired
    private ActivityManager activityManager;

    @Autowired
    private BankRefundTriggerManager bankRefundTriggerManager;

    @Autowired
    private InstallmentTriggerManager installmentTriggerManager;

    @Autowired
    private NotPaidOrdersTriggerManager notPaidOrdersTriggerManager;

    @Autowired
    private LogFactory logFactory;

    @Autowired
    private HttpSessionUtils httpSessionUtils;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(ActivityController.class);

    private static final String ENVIRONMENT = ConfigUtils.INSTANCE.getStringValue("environment");


/*    @RequestMapping(value = "/am-triggers-one-am", method = RequestMethod.POST)
    @ApiOperation(value = "Trigger Activities", notes = "All triggers")
    public void triggerAllAMActivities(@RequestBody Map<String, String> requestPayload)
            throws VException, IOException, GeneralSecurityException, SQLException, ParseException {

        if(!isProdEnvironment())
            return;

        activityManager.triggerActivities(ActivityEventConstants.ACTIVITY_ALL_AM_TRIGGERS, requestPayload.get("email"));

    }*/

    @RequestMapping(value = "/am-trigger-activities-cron", method = RequestMethod.POST,
            consumes = MediaType.TEXT_PLAIN_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void triggerAllAmActivities(@RequestHeader(value = "x-amz-sns-message-type") String messageType, @RequestBody String request) throws ForbiddenException
    {

        if(!isProdEnvironment())
            return;

        logger.info(request);
        Gson gson = new Gson();
        AWSSNSSubscriptionRequest subscriptionRequest = gson.fromJson(request, AWSSNSSubscriptionRequest.class);

        if (messageType.equals("SubscriptionConfirmation"))
        {
            String json = null;
            logger.info("SubscribeURL = " + subscriptionRequest.getSubscribeURL());
            ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionRequest.getSubscribeURL(), HttpMethod.GET, json, false);
            logger.info(resp.getEntity(String.class));
        }
        else if (messageType.equals("Notification"))
        {
            logger.info("Notification received - SNS");
            logger.info(subscriptionRequest.toString());
            activityManager.triggerActivitiesAsync(ActivityEventConstants.ACTIVITY_ALL_AM_TRIGGERS);
        }

    }

    @RequestMapping(value = "/am-trigger-activities", method = RequestMethod.GET)
    @ApiOperation(value = "Trigger Activities", notes = "")
    public void triggerAllAmActivities()
            throws VException, IOException, GeneralSecurityException, SQLException, ParseException {

        if(!isProdEnvironment())
            return;

        httpSessionUtils.checkIfAllowedList(null, Arrays.asList(Role.ADMIN, Role.STUDENT_CARE), true);

        activityManager.triggerAMActivities();

    }

    /*@Deprecated
    @RequestMapping(value = "/missed-class", method = RequestMethod.GET)
    @ApiOperation(value = "Trigger Activities", notes = "Missed Class")
    public void triggerMissedClassActivities()
            throws VException, IOException, GeneralSecurityException, SQLException, ParseException {

        if(!isProdEnvironment())
            return;

        activityManager.triggerActivities(ActivityEventConstants.ACTIVITY_MISSED_CLASS);

    }

    @Deprecated
    @RequestMapping(value = "/missed-class-one-am", method = RequestMethod.POST)
    @ApiOperation(value = "Trigger Activities", notes = "Missed Class")
    public void triggerMissedClassActivitiesTemp(@RequestBody Map<String, String> requestPayload)
            throws VException, IOException, GeneralSecurityException, SQLException, ParseException {

        if(!isProdEnvironment())
            return;

        activityManager.triggerActivities(ActivityEventConstants.ACTIVITY_MISSED_CLASS, requestPayload.get("email"));

    }

    @Deprecated
    @RequestMapping(value = "/zero-attendance", method = RequestMethod.GET)
    @ApiOperation(value = "Trigger Activities", notes = "Zero Attendance")
    public void triggerZeroAttendanceActivities()
            throws VException, IOException, GeneralSecurityException, SQLException, ParseException {

        if(!isProdEnvironment())
            return;

        activityManager.triggerActivities(ActivityEventConstants.ACTIVITY_ZERO_ATTENDANCE);

    }

    @Deprecated
    @RequestMapping(value = "/zero-attendance-one-am", method = RequestMethod.POST)
    @ApiOperation(value = "Trigger Activities", notes = "Zero Attendance")
    public void triggerZeroAttendanceActivities(@RequestBody Map<String, String> requestPayload)
            throws VException, IOException, GeneralSecurityException, SQLException, ParseException {

        if(!isProdEnvironment())
            return;

        activityManager.triggerActivities(ActivityEventConstants.ACTIVITY_ZERO_ATTENDANCE, requestPayload.get("email"));

    }

    @RequestMapping(value = "/test", method = RequestMethod.GET)
    @ApiOperation(value = "Trigger Activities", notes = "Test")
    public void triggerTestActivities()
            throws VException, IOException, GeneralSecurityException, SQLException, ParseException {

        if(!isProdEnvironment())
            return;

        activityManager.triggerActivities(ActivityEventConstants.ACTIVITY_TEST);

    }

    @Deprecated
    @RequestMapping(value = "/test-one-am", method = RequestMethod.POST)
    @ApiOperation(value = "Trigger Activities", notes = "Test")
    public void triggerTestActivities(@RequestBody Map<String, String> requestPayload)
            throws VException, IOException, GeneralSecurityException, SQLException, ParseException {

        if(!isProdEnvironment())
            return;

        activityManager.triggerActivities(ActivityEventConstants.ACTIVITY_TEST, requestPayload.get("email"));

    }

    @Deprecated
    @RequestMapping(value = "/engagement", method = RequestMethod.GET)
    @ApiOperation(value = "Trigger Activities", notes = "Engagement")
    public void triggerEngagementActivities()
            throws VException, IOException, GeneralSecurityException, SQLException, ParseException {

        if(!isProdEnvironment())
            return;

        activityManager.triggerActivities(ActivityEventConstants.ACTIVITY_ENGAGEMENT);

    }

    @RequestMapping(value = "/engagement-one-am", method = RequestMethod.POST)
    @ApiOperation(value = "Trigger Activities", notes = "Engagement")
    public void triggerEngagementActivities(@RequestBody Map<String, String> requestPayload)
            throws VException, IOException, GeneralSecurityException, SQLException, ParseException {

        if(!isProdEnvironment())
            return;

        activityManager.triggerActivities(ActivityEventConstants.ACTIVITY_ENGAGEMENT, requestPayload.get("email"));

    }*/

    @RequestMapping(value = "/sam-allocation-cron", method = RequestMethod.POST,
            consumes = MediaType.TEXT_PLAIN_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void triggerSamAllocationAsync(@RequestHeader(value = "x-amz-sns-message-type") String messageType, @RequestBody String request) throws ForbiddenException
    {

        if(!isProdEnvironment())
            return;

        logger.info(request);
        Gson gson = new Gson();
        AWSSNSSubscriptionRequest subscriptionRequest = gson.fromJson(request, AWSSNSSubscriptionRequest.class);

        if (messageType.equals("SubscriptionConfirmation"))
        {
            String json = null;
            logger.info("SubscribeURL = " + subscriptionRequest.getSubscribeURL());
            ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionRequest.getSubscribeURL(), HttpMethod.GET, json, false);
            logger.info(resp.getEntity(String.class));
        }
        else if (messageType.equals("Notification"))
        {
            logger.info("Notification received - SNS");
            logger.info(subscriptionRequest.toString());
            activityManager.triggerSamAllocationActivitiesAsync();
        }

    }

    @RequestMapping(value = "/sam-allocation", method = RequestMethod.GET)
    @ApiOperation(value = "Trigger Activities", notes = "Sam allocation")
    public void triggerSamAllocation()
            throws VException, IOException, GeneralSecurityException, SQLException, ParseException {

        if(!isProdEnvironment())
            return;

        httpSessionUtils.checkIfAllowedList(null, Arrays.asList(Role.ADMIN, Role.STUDENT_CARE), true);

        activityManager.triggerCareActivity(ActivityEventConstants.ACTIVITY_SAM_ALLOCATION);

    }

    @RequestMapping(value = "/freshdesk/ticket/activity-cron", method = RequestMethod.POST,
            consumes = MediaType.TEXT_PLAIN_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void triggerGrievanceActivitiesAsync(@RequestHeader(value = "x-amz-sns-message-type") String messageType, @RequestBody String request) throws ForbiddenException
    {

        if(!isProdEnvironment())
            return;

        logger.info(request);
        Gson gson = new Gson();
        AWSSNSSubscriptionRequest subscriptionRequest = gson.fromJson(request, AWSSNSSubscriptionRequest.class);

        if (messageType.equals("SubscriptionConfirmation"))
        {
            String json = null;
            logger.info("SubscribeURL = " + subscriptionRequest.getSubscribeURL());
            ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionRequest.getSubscribeURL(), HttpMethod.GET, json, false);
            logger.info(resp.getEntity(String.class));
        }
        else if (messageType.equals("Notification"))
        {
            logger.info("Notification received - SNS");
            logger.info(subscriptionRequest.toString());
            activityManager.triggerGrievanceActivityAsync();
        }

    }

    @RequestMapping(value = "/freshdesk/ticket/activity", method = RequestMethod.GET)
    @ApiOperation(value = "Trigger Activities", notes = "Grievances")
    public void triggerFreshdeskTicketActivities()
            throws VException, IOException, GeneralSecurityException, SQLException, ParseException {

        if(!isProdEnvironment())
            return;

        httpSessionUtils.checkIfAllowedList(null, Arrays.asList(Role.ADMIN, Role.STUDENT_CARE), true);

        activityManager.triggerFreshdeskTicketActivity();

    }

    @RequestMapping(value = "/freshdesk/refund/ticket/activity", method = RequestMethod.GET)
    @ApiOperation(value = "Trigger Activities", notes = "Grievances")
    public void triggerFreshdeskRefundTicketActivities(@RequestParam(value = "searchCol") String searchCol,
                                                       @RequestParam(value = "searchVal") String searchVal)
            throws VException {

        httpSessionUtils.checkIfAllowedList(null, Arrays.asList(Role.ADMIN, Role.STUDENT_CARE), true);

        activityManager.triggerFreshdeskRefundTicketActivity(searchCol, searchVal);
    }

    @RequestMapping(value = "/bank-refund-cron", method = RequestMethod.POST,
            consumes = MediaType.TEXT_PLAIN_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void triggerBankRefundAsync(@RequestHeader(value = "x-amz-sns-message-type") String messageType, @RequestBody String request) throws ForbiddenException
    {

        if(!isProdEnvironment())
            return;

        logger.info(request);
        Gson gson = new Gson();
        AWSSNSSubscriptionRequest subscriptionRequest = gson.fromJson(request, AWSSNSSubscriptionRequest.class);

        if (messageType.equals("SubscriptionConfirmation")){
            String json = null;
            logger.info("SubscribeURL = " + subscriptionRequest.getSubscribeURL());
            ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionRequest.getSubscribeURL(), HttpMethod.GET, json, false);
            logger.info(resp.getEntity(String.class));
        }
        else if (messageType.equals("Notification")){
            logger.info("Notification received - SNS");
            logger.info(subscriptionRequest.toString());
            bankRefundTriggerManager.processAsync();
        }

    }

    @RequestMapping(value = "/bank-refund", method = RequestMethod.GET)
    @ApiOperation(value = "Trigger Activities", notes = "Sam allocation")
    public void triggerBankRefund()
            throws VException, IOException, GeneralSecurityException, SQLException, ParseException {


        if(!isProdEnvironment())
            return;

        httpSessionUtils.checkIfAllowedList(null, Arrays.asList(Role.ADMIN, Role.STUDENT_CARE), true);

        bankRefundTriggerManager.process();

    }

    @RequestMapping(value = "/installments-cron", method = RequestMethod.POST,
            consumes = MediaType.TEXT_PLAIN_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void triggerInstallmentsAsync(@RequestHeader(value = "x-amz-sns-message-type") String messageType, @RequestBody String request) throws ForbiddenException
    {

        if(!isProdEnvironment())
            return;

        logger.info(request);
        Gson gson = new Gson();
        AWSSNSSubscriptionRequest subscriptionRequest = gson.fromJson(request, AWSSNSSubscriptionRequest.class);

        if (messageType.equals("SubscriptionConfirmation"))
        {
            String json = null;
            logger.info("SubscribeURL = " + subscriptionRequest.getSubscribeURL());
            ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionRequest.getSubscribeURL(), HttpMethod.GET, json, false);
            logger.info(resp.getEntity(String.class));
        }
        else if (messageType.equals("Notification"))
        {
            logger.info("Notification received - SNS");
            logger.info(subscriptionRequest.toString());
            installmentTriggerManager.processAsync();
        }

    }

    @RequestMapping(value = "/installments", method = RequestMethod.GET)
    @ApiOperation(value = "Trigger Activities", notes = "Sam allocation")
    public void triggerInstallments()
            throws VException, IOException, GeneralSecurityException, SQLException, ParseException {

        if(!isProdEnvironment())
            return;

        httpSessionUtils.checkIfAllowedList(null, Arrays.asList(Role.ADMIN, Role.STUDENT_CARE), true);

        installmentTriggerManager.process();

    }

    private boolean isProdEnvironment() {

        if("PROD".equalsIgnoreCase(ENVIRONMENT)){
            return true;
        }

        if("LOCAL".equalsIgnoreCase(ENVIRONMENT)){
            return true;
        }

        logger.info("Ignoring since environment is non PROD");
        return false;
    }

/*    @RequestMapping(value = "/testBundle", method = RequestMethod.POST)
    @ApiOperation(value = "Trigger Activities", notes = "Sam allocation")
    public @ResponseBody List<Long> testBundle(@RequestBody List<String> userIds)
            throws VException, IOException, GeneralSecurityException, SQLException, ParseException {

        if(!isProdEnvironment())
            return null;

        List<Long> activeEarlyEducationStudentIds = activityManager.getActiveEarlyEducationStudentsIds(userIds);
        return activeEarlyEducationStudentIds;

    }*/

    @RequestMapping(value = "/not_paid_orders_cron", method = RequestMethod.POST,
            consumes = MediaType.TEXT_PLAIN_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void triggerNotPaidOrdersCron(@RequestHeader(value = "x-amz-sns-message-type") String messageType, @RequestBody String request) throws ForbiddenException
    {

        if(!isProdEnvironment())
            return;

        logger.info(request);
        Gson gson = new Gson();
        AWSSNSSubscriptionRequest subscriptionRequest = gson.fromJson(request, AWSSNSSubscriptionRequest.class);

        if (messageType.equals("SubscriptionConfirmation"))
        {
            String json = null;
            logger.info("SubscribeURL = " + subscriptionRequest.getSubscribeURL());
            ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionRequest.getSubscribeURL(), HttpMethod.GET, json, false);
            logger.info(resp.getEntity(String.class));
        }
        else if (messageType.equals("Notification"))
        {
            logger.info("Notification received - SNS");
            logger.info(subscriptionRequest.toString());
            notPaidOrdersTriggerManager.processAsync();
        }

    }

    @RequestMapping(value = "/not-paid-orders", method = RequestMethod.GET)
    @ApiOperation(value = "Trigger Activities", notes = "Not Paid Orders")
    public void triggerNotPaidOrders()
            throws VException, IOException, GeneralSecurityException, SQLException, ParseException {

        if(!isProdEnvironment())
            return;

        httpSessionUtils.checkIfAllowedList(null, Arrays.asList(Role.ADMIN, Role.STUDENT_CARE), true);

        notPaidOrdersTriggerManager.process();

    }

}