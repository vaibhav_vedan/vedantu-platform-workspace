package com.vedantu.vedantudata.config;

import com.vedantu.util.ConfigUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.vedantudata.utils.PostgressHandler;
import org.apache.logging.log4j.Logger;
import org.hibernate.SessionFactory;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.cfg.Environment;
import org.hibernate.service.ServiceRegistry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Properties;
@Service
public class HibernateConfig {

	private static final String jdbcPrefix = ConfigUtils.INSTANCE.getStringValue("redshift.jdbc.prefix");
	private static final String host = ConfigUtils.INSTANCE.getStringValue("redshift.host");
	private static final String port = ConfigUtils.INSTANCE.getStringValue("redshift.port");
	private static final String database = ConfigUtils.INSTANCE.getStringValue("redshift.database");
	private static final String user = ConfigUtils.INSTANCE.getStringValue("redshift.user");
	private static final String password = ConfigUtils.INSTANCE.getStringValue("redshift.password");

	private static final String PROPERTY_NAME_HIBERNATE_DIALECT = "hibernate.dialect";
	private static final String PROPERTY_NAME_HIBERNATE_FORMAT_SQL = "hibernate.format_sql";
	private static final String PROPERTY_NAME_HIBERNATE_HBM2DDL_AUTO = "hibernate.hbm2ddl.auto";
	private static final String PROPERTY_NAME_HIBERNATE_NAMING_STRATEGY = "hibernate.ejb.naming_strategy";
	private static final String PROPERTY_NAME_HIBERNATE_SHOW_SQL = "hibernate.show_sql";
	private static final String PROPERTY_NAME_HIBERNATE_SESSION_CONTEXT = "hibernate.current_session_context_class";
	

	private static SessionFactory sessionFactory;


	public static SessionFactory getSessionFactoryUtils() {
		try {

			if (sessionFactory == null) {

				String url = jdbcPrefix + host+ ":" + port + "/" + database;
				Class.forName("org.postgresql.Driver");
				Configuration configuration = new Configuration();
				Properties hibernateProperties = new Properties();

				hibernateProperties.put(Environment.USER,
						user);
				hibernateProperties.put(Environment.PASS,
						password);
				hibernateProperties.put(Environment.URL,
						url);

				hibernateProperties.put(Environment.DRIVER,
						"org.postgresql.Driver");
				hibernateProperties.put("hibernate.temp.use_jdbc_metadata_defaults",
						false);
				hibernateProperties.put(Environment.USE_GET_GENERATED_KEYS,
						true);
				hibernateProperties.put(Environment.AUTOCOMMIT,
						true);
				hibernateProperties.put(Environment.AUTO_CLOSE_SESSION,
						true);
				hibernateProperties.put(Environment.DIALECT,
						ConfigUtils.INSTANCE.getStringValue(PROPERTY_NAME_HIBERNATE_DIALECT));

				hibernateProperties.put(Environment.HBM2DDL_AUTO,
						ConfigUtils.INSTANCE.getStringValue(PROPERTY_NAME_HIBERNATE_HBM2DDL_AUTO));

				hibernateProperties.put(Environment.SHOW_SQL,
						ConfigUtils.INSTANCE.getStringValue(PROPERTY_NAME_HIBERNATE_SHOW_SQL));

				hibernateProperties.put(Environment.FORMAT_SQL,
						ConfigUtils.INSTANCE.getStringValue(PROPERTY_NAME_HIBERNATE_FORMAT_SQL));

				hibernateProperties.put(Environment.CURRENT_SESSION_CONTEXT_CLASS,
						"thread");
				hibernateProperties.put(Environment.C3P0_IDLE_TEST_PERIOD,
						100);

				hibernateProperties.put(Environment.C3P0_MAX_SIZE,
						10);

				hibernateProperties.put(Environment.C3P0_MAX_STATEMENTS,
						50);

				hibernateProperties.put(Environment.C3P0_MIN_SIZE,
						5);
				hibernateProperties.put(Environment.C3P0_TIMEOUT,
						60);


				configuration.setProperties(hibernateProperties);
				ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder()
						.applySettings(configuration.getProperties()).build();
				Metadata metaData = new MetadataSources(serviceRegistry)
						.getMetadataBuilder()
						.build();
				sessionFactory = metaData.getSessionFactoryBuilder().build();

			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return sessionFactory;
	}

}