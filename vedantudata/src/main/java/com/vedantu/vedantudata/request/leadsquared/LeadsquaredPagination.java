package com.vedantu.vedantudata.request.leadsquared;

import com.google.gson.annotations.SerializedName;

public class LeadsquaredPagination {
	// "Paging": {
	// "PageIndex": 1,
	// "PageSize": 100
	// }

	@SerializedName("PageIndex")
	private Integer PageIndex = 1;

	@SerializedName("PageSize")
	private Integer PageSize = 1000;

	public LeadsquaredPagination(Integer pageIndex, Integer pageSize) {
		super();
		this.PageIndex = pageIndex;
		this.PageSize = pageSize;
	}

	public Integer getPageIndex() {
		return PageIndex;
	}

	public void setPageIndex(Integer pageIndex) {
		PageIndex = pageIndex;
	}

	public Integer getPageSize() {
		return PageSize;
	}

	public void setPageSize(Integer pageSize) {
		PageSize = pageSize;
	}

	@Override
	public String toString() {
		return "LeadsquaredPagination [PageIndex=" + PageIndex + ", PageSize=" + PageSize + ", toString()="
				+ super.toString() + "]";
	}
}
