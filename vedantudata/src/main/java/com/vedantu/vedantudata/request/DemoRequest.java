package com.vedantu.vedantudata.request;

import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import com.vedantu.vedantudata.enums.DemoRequestType;
import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "DemoRequest")
@Data
public class DemoRequest extends AbstractMongoStringIdEntity {

    private Long userId;
    private DemoRequestType demoRequestType;

    public static class Constants {

        public static final String USER_ID = "userId";
        public static final String DEMO_REQUEST_TYPE = "demoRequestType";

    }
}
