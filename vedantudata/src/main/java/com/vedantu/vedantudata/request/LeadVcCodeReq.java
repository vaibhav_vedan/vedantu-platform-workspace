package com.vedantu.vedantudata.request;

public class LeadVcCodeReq {
    private String email;
    private String vcCode;
    private String phone;
    private String mx_VCLead;


    public LeadVcCodeReq() {
        super();
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getVcCode() {
        return vcCode;
    }

    public void setVcCode(String vcCode) {
        this.vcCode = vcCode;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getMx_VCLead() {
        return mx_VCLead;
    }

    public void setMx_VCLead(String mx_VCLead) {
        this.mx_VCLead = mx_VCLead;
    }

    @Override
    public String toString() {
        return "LeadVcCodeReq [email=" + email + ", vcCode=" + vcCode +  ", phone=" + phone + ", mx_VCLead=" + mx_VCLead + ", toString()=" + super.toString()
                + "]";
    }

}
