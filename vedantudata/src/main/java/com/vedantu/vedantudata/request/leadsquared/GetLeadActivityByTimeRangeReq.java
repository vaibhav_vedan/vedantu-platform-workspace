package com.vedantu.vedantudata.request.leadsquared;

import com.google.gson.annotations.SerializedName;

public class GetLeadActivityByTimeRangeReq {
	@SerializedName("Parameter")
	private IntervalParameter Parameter;

	@SerializedName("Paging")
	private LeadsquaredPagination Paging;

	public GetLeadActivityByTimeRangeReq(String fromDate, String toDate, Integer pageIndex, Integer pageSize) {
		super();
		this.Parameter = new IntervalParameter(fromDate, toDate);
		this.Paging = new LeadsquaredPagination(pageIndex, pageSize);
	}

	public IntervalParameter getParameter() {
		return Parameter;
	}

	public void setParameter(IntervalParameter parameter) {
		Parameter = parameter;
	}

	public LeadsquaredPagination getPaging() {
		return Paging;
	}

	public void setPaging(LeadsquaredPagination paging) {
		Paging = paging;
	}

	@Override
	public String toString() {
		return "GetLeadActivityByTimeRangeReq [Parameter=" + Parameter + ", Paging=" + Paging + ", toString()="
				+ super.toString() + "]";
	}
}
