package com.vedantu.vedantudata.request;

import java.util.ArrayList;
import java.util.List;

import com.vedantu.vedantudata.utils.GoogleSheetUtils;

public class UpdateSpreadsheetReq {
	private String range;
	private String majorDimension = "ROWS";
	private ArrayList<List<String>> values;

	public UpdateSpreadsheetReq(String range, ArrayList<List<String>> values) {
		super();
		this.range = range;
		this.values = values;
	}

	public UpdateSpreadsheetReq(int rowNumber, List<String> values) {
		super();
		String rowIdentifier = GoogleSheetUtils.getRangeIdentifier(rowNumber);
		this.range = rowIdentifier + ":" + rowIdentifier + "" + values.size();
		this.values = new ArrayList<List<String>>();
		this.values.add(values);
	}

	public String getRange() {
		return range;
	}

	public void setRange(String range) {
		this.range = range;
	}

	public String getMajorDimension() {
		return majorDimension;
	}

	public void setMajorDimension(String majorDimension) {
		this.majorDimension = majorDimension;
	}

	public ArrayList<List<String>> getValues() {
		return values;
	}

	public void setValues(ArrayList<List<String>> values) {
		this.values = values;
	}

	@Override
	public String toString() {
		return "UpdateSpreadsheetReq [range=" + range + ", majorDimension=" + majorDimension + ", values=" + values
				+ ", toString()=" + super.toString() + "]";
	}
}
