package com.vedantu.vedantudata.request.leadsquared;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import org.springframework.util.StringUtils;

import com.google.gson.annotations.SerializedName;
import com.vedantu.util.DateTimeUtils;
import com.vedantu.vedantudata.entities.WebinarInfo;
import com.vedantu.vedantudata.entities.WebinarUserRegistrationInfo;
import com.vedantu.vedantudata.pojos.SchemaValueTuple;

public class PostLeadActivitiyReq {
	/*
	 * { "EmailAddress": "demo@ignore.com", "ActivityEvent": 249, "ActivityNote":
	 * "activity test 123", "ActivityDateTime": "2017-08-14 12:13:44" }
	 * 
	 */
	@SerializedName("EmailAddress")
	private String EmailAddress;

	@SerializedName("Phone")
	private String Phone;

	@SerializedName("ActivityEvent")
	private int ActivityEvent;

	@SerializedName("ActivityNote")
	private String ActivityNote;

	@SerializedName("Fields")
	private List<SchemaValueTuple> Fields = new ArrayList<>();

	public PostLeadActivitiyReq() {
		super();
	}

	public PostLeadActivitiyReq(String emailAddress, int activityEvent, String activityNote) {
		super();
		EmailAddress = emailAddress;
		ActivityEvent = activityEvent;
		ActivityNote = activityNote;
	}

	public String getEmailAddress() {
		return EmailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		EmailAddress = emailAddress;
	}

	public int getActivityEvent() {
		return ActivityEvent;
	}

	public void setActivityEvent(int activityEvent) {
		ActivityEvent = activityEvent;
	}

	public String getActivityNote() {
		return ActivityNote;
	}

	public void setActivityNote(String activityNote) {
		ActivityNote = activityNote;
	}

	public List<SchemaValueTuple> getFields() {
		return Fields;
	}

	public void setFields(List<SchemaValueTuple> fields) {
		Fields = fields;
	}

	public void addFieldEntry(String fieldName, String value) {
		addFieldEntry(new SchemaValueTuple(fieldName, value));
	}

	public void addFieldEntry(SchemaValueTuple entry) {
		if (this.Fields == null) {
			this.Fields = new ArrayList<>();
		}
		this.Fields.add(entry);
	}

	public String getPhone() {
		return Phone;
	}

	public void setPhone(String phone) {
		Phone = phone;
	}

	public static PostLeadActivitiyReq createWebinarRegisteredActivitiyReq(
			WebinarUserRegistrationInfo webinarUserRegistrationInfo, WebinarInfo webinarInfo) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		sdf.setTimeZone(TimeZone.getTimeZone(DateTimeUtils.TIME_ZONE_IN));

		// Construct note
		String note = "WebinarRegistration - userFirstName:" + webinarUserRegistrationInfo.getFirstName()
				+ ", userLastName:" + webinarUserRegistrationInfo.getLastName() + " userEmail:"
				+ webinarUserRegistrationInfo.getEmailId() + " userPhone:" + webinarUserRegistrationInfo.getPhone()
				+ ".";
		note += "WebinarInfo - webinarName:" + webinarInfo.getName() + " webinarSubject:" + webinarInfo.getSubject()
				+ " startTime:" + sdf.format(new Date(webinarInfo.getStartTime())) + " endTime:"
				+ sdf.format(new Date(webinarInfo.getEndTime()));

		// mx_Custom_1 - GotoWebinar Attendee url
		// mx_Custom_2 - GotoWebinar title
		// mx_Custom_3 - GotoWebinar date
		// mx_Custom_4 - GotoWebinar time
		PostLeadActivitiyReq req = new PostLeadActivitiyReq(webinarUserRegistrationInfo.getEmailId(), 255, note);
		req.setPhone(webinarUserRegistrationInfo.getPhone());
		if (!StringUtils.isEmpty(webinarUserRegistrationInfo.getRegisterJoinUrl())) {
			req.addFieldEntry("mx_Custom_1", webinarUserRegistrationInfo.getRegisterJoinUrl());
		}
		if (!StringUtils.isEmpty(webinarInfo.getSubject())) {
			req.addFieldEntry("mx_Custom_2", webinarInfo.getSubject());
		}

		String startTimeStr = sdf.format(new Date(webinarInfo.getStartTime()));
		String[] timeArr = startTimeStr.split(" ");
		req.addFieldEntry("mx_Custom_3", timeArr[0]);
		req.addFieldEntry("mx_Custom_4", timeArr[1]);
		return req;
	}

	public static PostLeadActivitiyReq createWebinarAttendedActivitiyReq(
			WebinarUserRegistrationInfo webinarUserRegistrationInfo, WebinarInfo webinarInfo) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		sdf.setTimeZone(TimeZone.getTimeZone(DateTimeUtils.TIME_ZONE_IN));

		// Construct note
		String note = "WebinarRegistration - userFirstName:" + webinarUserRegistrationInfo.getFirstName()
				+ ", userLastName:" + webinarUserRegistrationInfo.getLastName() + " userEmail:"
				+ webinarUserRegistrationInfo.getEmailId() + " userPhone:" + webinarUserRegistrationInfo.getPhone()
				+ " timeInSession:" + webinarUserRegistrationInfo.getTimeInSession() + " intervals:"
				+ (webinarUserRegistrationInfo.getAttendanceIntervals() == null ? 0
						: webinarUserRegistrationInfo.getAttendanceIntervals().size())
				+ ".";
		note += "WebinarInfo - webinarName:" + webinarInfo.getName() + " webinarSubject:" + webinarInfo.getSubject()
				+ " startTime:" + sdf.format(new Date(webinarInfo.getStartTime())) + " endTime:"
				+ sdf.format(new Date(webinarInfo.getEndTime()));

		// mx_Custom_1 - Attendee url
		// mx_Custom_2 - GotoWebinar title
		// mx_Custom_3 - GotoWebinar date
		// mx_Custom_4 - GotoWebinar time
		PostLeadActivitiyReq req = new PostLeadActivitiyReq(webinarUserRegistrationInfo.getEmailId(), 256, note);
		req.setPhone(webinarUserRegistrationInfo.getPhone());
		if (!StringUtils.isEmpty(webinarUserRegistrationInfo.getRegisterJoinUrl())) {
			req.addFieldEntry("mx_Custom_1", webinarUserRegistrationInfo.getRegisterJoinUrl());
		}
		if (!StringUtils.isEmpty(webinarInfo.getSubject())) {
			req.addFieldEntry("mx_Custom_2", webinarInfo.getSubject());
		}

		String startTimeStr = sdf.format(new Date(webinarInfo.getStartTime()));
		String[] timeArr = startTimeStr.split(" ");
		req.addFieldEntry("mx_Custom_3", timeArr[0]);
		req.addFieldEntry("mx_Custom_4", timeArr[1]);
		return req;
	}

	public static PostLeadActivitiyReq createWebinarNotAttendedActivitiyReq(
			WebinarUserRegistrationInfo webinarUserRegistrationInfo, WebinarInfo webinarInfo) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		sdf.setTimeZone(TimeZone.getTimeZone(DateTimeUtils.TIME_ZONE_IN));

		// Construct note
		String note = "WebinarRegistration - userFirstName:" + webinarUserRegistrationInfo.getFirstName()
				+ ", userLastName:" + webinarUserRegistrationInfo.getLastName() + " userEmail:"
				+ webinarUserRegistrationInfo.getEmailId() + " userPhone:" + webinarUserRegistrationInfo.getPhone()
				+ ".";
		note += "WebinarInfo - webinarName:" + webinarInfo.getName() + " webinarSubject:" + webinarInfo.getSubject()
				+ " startTime:" + sdf.format(new Date(webinarInfo.getStartTime())) + " endTime:"
				+ sdf.format(new Date(webinarInfo.getEndTime()));

		// mx_Custom_1 - Attendee url
		// mx_Custom_2 - GotoWebinar title
		// mx_Custom_3 - GotoWebinar date
		// mx_Custom_4 - GotoWebinar time
		PostLeadActivitiyReq req = new PostLeadActivitiyReq(webinarUserRegistrationInfo.getEmailId(), 257, note);
		req.setPhone(webinarUserRegistrationInfo.getPhone());
		if (!StringUtils.isEmpty(webinarUserRegistrationInfo.getRegisterJoinUrl())) {
			req.addFieldEntry("mx_Custom_1", webinarUserRegistrationInfo.getRegisterJoinUrl());
		}
		if (!StringUtils.isEmpty(webinarInfo.getSubject())) {
			req.addFieldEntry("mx_Custom_2", webinarInfo.getSubject());
		}

		String startTimeStr = sdf.format(new Date(webinarInfo.getStartTime()));
		String[] timeArr = startTimeStr.split(" ");
		req.addFieldEntry("mx_Custom_3", timeArr[0]);
		req.addFieldEntry("mx_Custom_4", timeArr[1]);
		return req;
	}

	@Override
	public String toString() {
		return "PostLeadActivitiyReq [EmailAddress=" + EmailAddress + ", ActivityEvent=" + ActivityEvent
				+ ", ActivityNote=" + ActivityNote + ", Fields=" + Fields + ", toString()=" + super.toString() + "]";
	}
}
