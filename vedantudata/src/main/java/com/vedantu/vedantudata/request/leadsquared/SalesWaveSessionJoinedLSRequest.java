package com.vedantu.vedantudata.request.leadsquared;

import com.google.gson.annotations.SerializedName;
import com.vedantu.util.dbentities.mongo.AbstractMongoEntity;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import com.vedantu.vedantudata.enums.SessionConnectionState;
import com.vedantu.vedantudata.enums.SessionRole;
import lombok.*;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Document("SalesWaveSessionJoinedLSRequest")
public class SalesWaveSessionJoinedLSRequest extends AbstractMongoStringIdEntity {

    @SerializedName("userId")
    private String userId;

    @Indexed
    @SerializedName("sessionId")
    private String sessionId;

    @SerializedName("sessionRole")
    private SessionRole sessionRole;

    @SerializedName("sessionJoinTime")
    private String sessionJoinTime;

    @SerializedName("sessionDuration")
    private String sessionDuration;

    @SerializedName("sessionStartTime")
    private String sessionStartTime;

    @SerializedName("connectionState")
    private SessionConnectionState connectionState;

    @SerializedName("isTeacherActivityPushed")
    private Boolean isTeacherActivityPushed;

    @SerializedName("isTAActivityPushed")
    private Boolean isTAActivityPushed;

}
