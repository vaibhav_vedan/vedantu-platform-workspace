package com.vedantu.vedantudata.request.leadsquared;

public class PushLeadsquaredReq {
	private String source;
	private Boolean noActivity;

	public PushLeadsquaredReq() {
		super();
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public Boolean getNoActivity() {
		return noActivity;
	}

	public void setNoActivity(Boolean noActivity) {
		this.noActivity = noActivity;
	}

	@Override
	public String toString() {
		return "PushLeadsquaredReq [source=" + source + ", noActivity=" + noActivity + ", toString()="
				+ super.toString() + "]";
	}
}
