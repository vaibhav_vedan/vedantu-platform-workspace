package com.vedantu.vedantudata.request;

public class ConvoxUploadReq {
	private String processName;
	private String date;

	public ConvoxUploadReq() {
		super();
	}

	public String getProcessName() {
		return processName;
	}

	public void setProcessName(String processName) {
		this.processName = processName;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	@Override
	public String toString() {
		return "ConvoxUploadReq [processName=" + processName + ", date=" + date + ", toString()=" + super.toString()
				+ "]";
	}
}
