package com.vedantu.vedantudata.request.leadsquared;

import com.google.gson.annotations.SerializedName;

public class IntervalParameter {

	@SerializedName("FromDate")
	private String FromDate;

	@SerializedName("ToDate")
	private String ToDate;

	public IntervalParameter(String fromDate, String toDate) {
		super();
		this.FromDate = fromDate;
		this.ToDate = toDate;
	}

	public String getFromDate() {
		return FromDate;
	}

	public void setFromDate(String fromDate) {
		FromDate = fromDate;
	}

	public String getToDate() {
		return ToDate;
	}

	public void setToDate(String toDate) {
		ToDate = toDate;
	}

	@Override
	public String toString() {
		return "IntervalParameter [FromDate=" + FromDate + ", ToDate=" + ToDate + ", toString()=" + super.toString()
				+ "]";
	}
}
