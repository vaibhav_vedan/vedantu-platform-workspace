package com.vedantu.vedantudata.request.leadsquared;

import com.google.gson.annotations.SerializedName;

public class GetLeadsByTimeRangeReq {
	// {
	// "Parameter": {
	// "FromDate": "2016-09-05 02:00:00",
	// "ToDate": "2016-09-08 02:20:00"
	// },
	// "Columns": {
	// "Include_CSV": "ProspectID,FirstName,LastName,EmailAddress"
	// },
	// "Paging": {
	// "PageIndex": 1,
	// "PageSize": 100
	// }
	// }

	@SerializedName("Parameter")
	private IntervalParameter Parameter;

	@SerializedName("Paging")
	private LeadsquaredPagination Paging;

	public GetLeadsByTimeRangeReq(String fromDate, String toDate, Integer pageIndex, Integer pageSize) {
		super();
		this.Parameter = new IntervalParameter(fromDate, toDate);
		this.Paging = new LeadsquaredPagination(pageIndex, pageSize);
	}

	public IntervalParameter getParameter() {
		return Parameter;
	}

	public void setParameter(IntervalParameter parameter) {
		Parameter = parameter;
	}

	public LeadsquaredPagination getPaging() {
		return Paging;
	}

	public void setPaging(LeadsquaredPagination paging) {
		Paging = paging;
	}

	@Override
	public String toString() {
		return "GetLeadsByTimeRangeReq [Parameter=" + Parameter + ", Paging=" + Paging + ", toString()="
				+ super.toString() + "]";
	}
}
