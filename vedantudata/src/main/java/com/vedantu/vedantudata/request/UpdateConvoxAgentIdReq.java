package com.vedantu.vedantudata.request;

public class UpdateConvoxAgentIdReq {
	private String agentName;
	private String relatedEmailId;

	public UpdateConvoxAgentIdReq() {
		super();
	}

	public String getAgentName() {
		return agentName;
	}

	public void setAgentName(String agentName) {
		this.agentName = agentName;
	}

	public String getRelatedEmailId() {
		return relatedEmailId;
	}

	public void setRelatedEmailId(String relatedEmailId) {
		this.relatedEmailId = relatedEmailId;
	}

	@Override
	public String toString() {
		return "UpdateConvoxAgentIdReq [agentName=" + agentName + ", relatedEmailId=" + relatedEmailId + ", toString()="
				+ super.toString() + "]";
	}
}
