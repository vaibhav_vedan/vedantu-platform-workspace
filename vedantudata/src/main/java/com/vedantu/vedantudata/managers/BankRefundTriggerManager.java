package com.vedantu.vedantudata.managers;

import com.vedantu.async.AsyncTaskFactory;
import com.vedantu.async.AsyncTaskParams;
import com.vedantu.util.ConfigUtils;
import com.vedantu.vedantudata.async.AsyncTaskName;
import com.vedantu.vedantudata.utils.ActivityEventConstants;
import com.vedantu.vedantudata.utils.LeadsquaredActivityHelper;
import com.vedantu.vedantudata.utils.PostgressHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

@Service
public class BankRefundTriggerManager {

    @Autowired
    private PostgressHandler postgressHandler;

    @Autowired
    private ActivityManager activityManager;

    @Autowired
    private LeadsquaredActivityHelper leadsquaredActivityHelper;

    @Autowired
    private AsyncTaskFactory asyncTaskFactory;

    private static final String CARE_TEAM_EMAIL = ConfigUtils.INSTANCE.getStringValue("care_team_email");

    public void process() {

        String query = "select  amount, reasonreftype, credittouseremail from " +
                "transaction where reasontype = 'REFUND' AND DATEDIFF(DAY, creationtime_istdate, GETDATE()) < 1;";

        //int count = 0;
        try {
            ResultSet rs = postgressHandler.executeQuery(query);
            if (rs != null) {
                while (rs.next()) {
                    Float amount = rs.getFloat("amount");
                    String reasonreftype = rs.getString("reasonreftype");
                    String email = rs.getString("credittouseremail");
                    //String email = "tes11t@test.com";

                    Map<String, String> param = new HashMap<>();
                    param.put(ActivityEventConstants.EMAIL_ADDRESS, email);
                    param.put(ActivityEventConstants.OWNER, CARE_TEAM_EMAIL);
                    param.put(ActivityEventConstants.MX_CUSTOM_1, amount.toString());
                    param.put(ActivityEventConstants.MX_CUSTOM_2, "Pending");
                    param.put(ActivityEventConstants.EMAIL_ADDRESS, email);
                    param.put(ActivityEventConstants.NOTE, reasonreftype);

                    activityManager.pushCareActivity(ActivityEventConstants.EVENT_BANK_REFUND, param);

                    //if(count >=2)
                      //  break;
                    //count ++;
                }
            }
        } catch (SQLException e) {

        }

        leadsquaredActivityHelper.triggerAllActivities();
    }

    public void processAsync() {
        Map<String, Object> payload = new HashMap<>();
        AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.TRIGGER_LS_CARE_BANK_REFUND_ACTIVITIES, payload);
        asyncTaskFactory.executeTask(params);
    }
}
