package com.vedantu.vedantudata.managers;

import java.util.List;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import com.vedantu.util.CollectionUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.vedantudata.dao.serializers.UserEventDAO;
import com.vedantu.vedantudata.entities.UserEventData;
import com.vedantu.vedantudata.enums.UserEventName;
import com.vedantu.vedantudata.enums.UserEventType;
import com.vedantu.vedantudata.pojos.VedantuDataOrder;

@Service
public class UserEventManager {

	@Autowired
	private UserEventDAO userEventDAO;

	@Autowired
	private LogFactory logFactory;

	@SuppressWarnings("static-access")
	private Logger logger = logFactory.getLogger(UserEventManager.class);

	public Boolean updateConvoxEvent(UserEventData userEventData) {
		logger.info("Request:" + userEventData.toString());
		Query query = new Query();
		query.addCriteria(Criteria.where(UserEventData.Constants.EVENT_TIME).is(userEventData.getEventTime()));
		query.addCriteria(Criteria.where(UserEventData.Constants.USER_EVENT_TYPE).is(UserEventType.CONVOX));
		query.addCriteria(Criteria.where(UserEventData.Constants.REFERENCE_ID).is(userEventData.getReferenceId()));

		List<UserEventData> existingEntries = userEventDAO.runQuery(query, UserEventData.class);
		if (CollectionUtils.isEmpty(existingEntries)) {
			userEventDAO.save(userEventData);
			return true;
		} else {
			logger.info("Entry already existing");
		}

		return false;
	}

	public Boolean updateOrderData(List<VedantuDataOrder> orders) {
		if (!CollectionUtils.isEmpty(orders)) {
			for (VedantuDataOrder order : orders) {
				updateOrderData(order);
			}
		}
		return true;
	}

	public Boolean updateOrderData(VedantuDataOrder order) {

		// Check if it already exists
		Query query = new Query();
		query.addCriteria(Criteria.where(UserEventData.Constants.REFERENCE_ID).is(order.getOrderId()));
		List<UserEventData> orders = userEventDAO.runQuery(query, UserEventData.class);
		if (CollectionUtils.isEmpty(orders)) {

			UserEventData userEventData = new UserEventData();
			userEventData.setUserEventName(UserEventName.VEDANTU_ORDER);
			userEventData.setUserEventType(UserEventType.VEDANTU);
			userEventData.setUserEventStatus(String.valueOf(order.getPaymentStatus()));
			userEventData.setReferenceId(order.getOrderId());
			userEventData.setReferenceValue(order.getAmount());
			userEventData.setEventNote(getOrderEventNote(order));
			userEventData.setEventTime(order.getCreationTime());
			userEventDAO.create(userEventData);
			logger.info("User event : " + userEventData.toString());
		} else {
			logger.info("Entry found");
			orders.get(0).setUserEventStatus(String.valueOf(order.getPaymentStatus()));
			userEventDAO.save(orders.get(0));
			logger.info("User event : " + orders.get(0));
		}

		return true;
	}

	private static String getOrderEventNote(VedantuDataOrder vedantuDataOrder) {
		return "TransactionId:" + vedantuDataOrder.getTransactionId() + " ContextId:" + vedantuDataOrder.getContextId()
				+ " PurchaseFlowId:" + vedantuDataOrder.getPurchaseFlowId() + " PurchaseFlowType:"
				+ vedantuDataOrder.getPurchaseFlowType() + " EntityType:" + vedantuDataOrder.getEntityType()
				+ " PaymentType:" + vedantuDataOrder.getPaymentType() + " UserGrade:" + vedantuDataOrder.getUserGrade();
	}
}
