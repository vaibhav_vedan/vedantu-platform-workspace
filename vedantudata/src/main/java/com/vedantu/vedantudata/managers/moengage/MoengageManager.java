package com.vedantu.vedantudata.managers.moengage;

import com.google.gson.JsonObject;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.User.User;
import com.vedantu.exception.VException;
import com.vedantu.exception.VExceptionFactory;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.WebUtils;
import com.vedantu.vedantudata.managers.UserManager;
import com.vedantu.vedantudata.utils.AwsSQSManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Date;
import java.util.List;

@Service
public class MoengageManager {


    @Autowired
    private UserManager userManager;

    @Autowired
    private LogFactory logFactory;

    @Autowired
    public AwsSQSManager awsSQSManager;


    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(MoengageManager.class);

    private static String appId = ConfigUtils.INSTANCE.getStringValue("moengage.app.id");
    private static String appKey = ConfigUtils.INSTANCE.getStringValue("moengage.app.key");
    private static String customerUrl = "https://api.moengage.com/v1/customer/";

    public void updateLead(String text) throws VException, ParseException {
        JSONObject lsData = new JSONObject(text);
        User user = null;
        long ProspectActivityDate_Max_TS = 0;
        String FirstName = lsData.getJSONObject("After").optString("FirstName");
        String LastName = lsData.getJSONObject("After").optString("LastName");
        String Mobile = lsData.getJSONObject("After").optString("Mobile");

        String EmailAddress = lsData.getJSONObject("After").optString("EmailAddress").trim().toLowerCase();
        String ProspectStage = lsData.getJSONObject("After").optString("ProspectStage");
        String ProspectActivityName_Max = lsData.getJSONObject("After").optString("ProspectActivityName_Max");
        String ProspectActivityDate_Max = lsData.getJSONObject("After").optString("ProspectActivityDate_Max");
        String mx_Sub_Stage = lsData.getJSONObject("After").optString("mx_Sub_Stage");
        String SourceMedium = lsData.getJSONObject("After").optString("SourceMedium");
        String SourceCampaign = lsData.getJSONObject("After").optString("SourceCampaign");
        String Source = lsData.getJSONObject("After").optString("Source");

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        if (!ProspectActivityDate_Max.isEmpty()) {
            Date parsedDate = dateFormat.parse(ProspectActivityDate_Max);
            ProspectActivityDate_Max_TS = parsedDate.getTime() / 1000;
        }

        if (Mobile.isEmpty()) {
            Mobile = lsData.getJSONObject("After").optString("Phone");
        }
        if (!Mobile.isEmpty()) {
            user = userManager.getUserByPhone(Mobile);
        }
        if (user == null && !EmailAddress.isEmpty()) {
            user = userManager.getUserByEmail(EmailAddress);
        }
        if (user != null && user.getStudentInfo() != null) {
            String Target = user.getStudentInfo().getTarget();
            String Grade = user.getStudentInfo().getGrade();
            String Board = user.getStudentInfo().getBoard();
            List<String> targets = user.getStudentInfo().getExamTargets();
            String delim = ",";
            String ExamTargets = String.join(delim, targets);

            JsonObject moengagePayload = new JsonObject();
            moengagePayload.addProperty("type", "customer");
            moengagePayload.addProperty("customer_id", user.getId());
            JsonObject attributes = new JsonObject();
            attributes.addProperty("name", FirstName + LastName);
            attributes.addProperty("first_name", FirstName);
            attributes.addProperty("last_name", LastName);
            attributes.addProperty("email", EmailAddress);
            attributes.addProperty("mobile", Mobile);
            attributes.addProperty("Grade", Grade);
            attributes.addProperty("Board", Board);
            attributes.addProperty("Target", Target);
            attributes.addProperty("ProspectStage", ProspectStage);
            attributes.addProperty("ProspectActivityName_Max", ProspectActivityName_Max);
            if (ProspectActivityDate_Max_TS != 0) {
                attributes.addProperty("ProspectActivityDate_Max", ProspectActivityDate_Max_TS);
            }
            attributes.addProperty("mx_Sub_Stage", mx_Sub_Stage);
            attributes.addProperty("SourceMedium", SourceMedium);
            attributes.addProperty("SourceCampaign", SourceCampaign);
            attributes.addProperty("Source", Source);
            attributes.addProperty("ExamTargets", ExamTargets);
            moengagePayload.add("attributes", attributes);

            String serverUrl = customerUrl + appId;
            String AuthenticationString = "Basic " + Base64.getEncoder().encodeToString((appId + ":" + appKey).getBytes());
            logger.info("Moengage Payload" + moengagePayload.toString());
            ClientResponse resp = WebUtils.INSTANCE.doCall(serverUrl, HttpMethod.POST, moengagePayload.toString(), AuthenticationString);
            logger.info("User updated In MoEngage" + resp);

        }

    }

}

