package com.vedantu.vedantudata.managers.leadsquared;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.PostConstruct;

import com.vedantu.util.*;
import com.vedantu.util.logger.LoggingMarkers;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.NotFoundException;
import com.vedantu.vedantudata.dao.serializers.LeadsquaredUserDAO;
import com.vedantu.vedantudata.entities.leadsquared.LeadsquaredUser;
import com.vedantu.vedantudata.enums.LeadsquaredAgentBucket;
import com.vedantu.vedantudata.request.UpdateConvoxAgentIdReq;
import com.vedantu.vedantudata.utils.LeadsquaredConfig;

import java.io.FileReader;

import org.supercsv.io.CsvListReader;
import org.supercsv.io.ICsvListReader;
import org.supercsv.prefs.CsvPreference;

@Service
public class LeadsquaredUserManager extends LeadsquaredConfig {

    @Autowired
    private LeadsquaredUserDAO leadsquaredUserDAO;

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(LeadsquaredUserManager.class);

    private static Gson gson = new Gson();

    private static final Type leadsquaredUserListType = new TypeToken<List<LeadsquaredUser>>() {
    }.getType();

    private static Map<String, String> AGENT_ID_LEAD_ID_MAP = new HashMap<>();

    private static String DEFAULT_AGENT_ID = "79c86af4-1836-11e5-981b-22000a9700b4";

    private static final String LS_AGENT_RECORDS_CSV_FILENAME = "/home/vipl099/Downloads/LeadSquareUsers - Sheet1.csv";

    // https://api.leadsquared.com/v2/ProspectActivity.svc/Retrieve?accessKey=AccessKey&secretKey=SecretKey&leadId=2b8a68af-b4ee-4eca-bba8-39be6f678991
    private static String accessKey;
    private static String secretKey;

    @PostConstruct
    public void init() {
        accessKey = ConfigUtils.INSTANCE.getStringValue("leadsquared.access.id");
        secretKey = ConfigUtils.INSTANCE.getStringValue("leadsquared.secret.key");

        // Populate agent id lead id map
        List<LeadsquaredUser> users = getLeadsquaredUsers();
        if (!CollectionUtils.isEmpty(users)) {
            for (LeadsquaredUser user : users) {
                if (!StringUtils.isEmpty(user.getConvoxAgentId())) {
                    AGENT_ID_LEAD_ID_MAP.put(user.getConvoxAgentId(), user.getLeadsquaredId());
                }
            }
        }
    }

    public List<LeadsquaredUser> updateLeadsquaredUsers() throws Exception {
        logger.info("Request");
        List<LeadsquaredUser> users = new ArrayList<>();
        String serverUrl = appendAccessKeys(getUsersUrl);
        ClientResponse cRes = WebUtils.INSTANCE.doCall(serverUrl, HttpMethod.GET, null);
        logger.info(LoggingMarkers.JSON_MASK,"cRes:" + cRes.toString());
        String response = cRes.getEntity(String.class);
        if (cRes.getStatus() == 429) {
            logger.error(LoggingMarkers.JSON_MASK, "Too many requests for updateLeadsquaredUsers  , response: " + response);
        } else if (cRes.getStatus() != 200) {
            logger.error(LoggingMarkers.JSON_MASK, "InvalidStatusCodeGetUsers - get lead users not 200 - " + cRes.getStatus() + " cRes:"
                    + cRes.toString());
            return users;
        }

        users = gson.fromJson(response, leadsquaredUserListType);
        if (CollectionUtils.isEmpty(users)) {
            logger.error("No users found in leaddsquared");
        }

        List<String> leadsquaredUserIds = getAllUserIds(users);
        List<LeadsquaredUser> existingEntries = leadsquaredUserDAO.getByUserIds(leadsquaredUserIds);
        Map<String, LeadsquaredUser> existingUserMap = new HashMap<>();
        if (!CollectionUtils.isEmpty(existingEntries)) {
            for (LeadsquaredUser user : existingEntries) {
                existingUserMap.put(user.getLeadsquaredId(), user);
            }
        }

        for (LeadsquaredUser user : users) {
            if (existingUserMap.containsKey(user.getLeadsquaredId())) {
                // Handle all the custom field here
                LeadsquaredUser existingEntry = existingUserMap.get(user.getLeadsquaredId());
                user.setId(existingEntry.getId());
                user.setCreationTime(existingEntry.getCreationTime());
                user.setBucketName(existingEntry.getBucketName());
                user.setConvoxAgentId(existingEntry.getConvoxAgentId());
            }
            logger.info("User:" + user.toString());
            leadsquaredUserDAO.save(user);
        }

        return users;
    }

    public LeadsquaredUser updateLeadsquaredAgentId(String email, String agentId) throws Exception {
        LeadsquaredUser user = getLeadsquaredUserByEmailId(email);
        if (user != null) {
            user.setConvoxAgentId(agentId);
        }
        leadsquaredUserDAO.save(user);
        return user;
    }

    public int updateLeadsquaredAgentIds(List<UpdateConvoxAgentIdReq> updateReqList) throws Exception {
        int updatedEntries = 0;
        if (!CollectionUtils.isEmpty(updateReqList)) {
            for (UpdateConvoxAgentIdReq req : updateReqList) {
                updateLeadsquaredAgentId(req.getRelatedEmailId(), req.getAgentName());
                updatedEntries++;
            }
        }

        return updatedEntries;
    }

    public List<LeadsquaredUser> getLeadsquaredUsers() {
        Query query = new Query();
        return leadsquaredUserDAO.runQuery(query, LeadsquaredUser.class);
    }

    public Set<String> getLeadsquaredRoles() {
        Query query = new Query();
        List<LeadsquaredUser> leadsquaredUsers = leadsquaredUserDAO.runQuery(query, LeadsquaredUser.class);
        Set<String> roles = new HashSet<>();
        if (!CollectionUtils.isEmpty(leadsquaredUsers)) {
            for (LeadsquaredUser user : leadsquaredUsers) {
                roles.add(user.getRole());
            }
        }
        return roles;
    }

    public LeadsquaredUser getLeadsquaredUserByEmailId(String emailId) throws NotFoundException {
        Query query = new Query();
        query.addCriteria(Criteria.where(LeadsquaredUser.Constants.EMAIL).is(emailId));
        List<LeadsquaredUser> leadsquaredUsers = leadsquaredUserDAO.runQuery(query, LeadsquaredUser.class);
        if (!CollectionUtils.isEmpty(leadsquaredUsers)) {
            return leadsquaredUsers.get(0);
        } else {
            throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR,
                    "Lead squared user not found for emailid:" + emailId);
        }
    }

    public List<LeadsquaredUser> updateLeadsquaredBucket(List<LeadsquaredAgentBucket> leadsquaredAgentBucket,
            List<String> emailIds) throws Exception {
        Query query = new Query();
        query.addCriteria(Criteria.where(LeadsquaredUser.Constants.EMAIL).in(emailIds));

        List<LeadsquaredUser> users = leadsquaredUserDAO.runQuery(query, LeadsquaredUser.class);
        if (!CollectionUtils.isEmpty(users)) {
            for (LeadsquaredUser user : users) {
                user.setBucketName(leadsquaredAgentBucket);
                leadsquaredUserDAO.save(user);
            }
        }

        return users;
    }

    public String getLeadsquareduserIdByConvoxAgentId(String convoxAgentID) {
        logger.info("Agent id:" + convoxAgentID);
        if (!StringUtils.isEmpty(convoxAgentID)) {
            if (AGENT_ID_LEAD_ID_MAP.containsKey(convoxAgentID)) {
                return AGENT_ID_LEAD_ID_MAP.get(convoxAgentID);
            } else {
                Query query = new Query();
                query.addCriteria(Criteria.where(LeadsquaredUser.Constants.AGENT_ID).is(convoxAgentID));
                List<LeadsquaredUser> users = leadsquaredUserDAO.runQuery(query, LeadsquaredUser.class);
                if (!CollectionUtils.isEmpty(users)) {
                    AGENT_ID_LEAD_ID_MAP.put(convoxAgentID, users.get(0).getLeadsquaredId());
                    return users.get(0).getLeadsquaredId();
                }
            }
        }

        return DEFAULT_AGENT_ID;
    }

    private String appendAccessKeys(String url) {
        if (url.contains("?")) {
            return url + "&accessKey=" + accessKey + "&secretKey=" + secretKey;
        } else {
            return url + "?accessKey=" + accessKey + "&secretKey=" + secretKey;
        }
    }

    private List<String> getAllUserIds(List<LeadsquaredUser> users) {
        List<String> leadsquaredUserIds = new ArrayList<>();
        for (LeadsquaredUser user : users) {
            leadsquaredUserIds.add(user.getLeadsquaredId());
        }

        return leadsquaredUserIds;
    }

    public long getDataFromCSVAndInsert() throws IOException {
        logger.info("CSV_FILENAME : "+ LS_AGENT_RECORDS_CSV_FILENAME);
        List<LeadsquaredUser> leadsquaredUserList = new ArrayList<>();
        try(ICsvListReader listReader = new CsvListReader(new FileReader(LS_AGENT_RECORDS_CSV_FILENAME), CsvPreference.STANDARD_PREFERENCE))
        {
            List<String> headerList = listReader.read();
            logger.info("headerList : "+headerList);
            List<String> fieldsInCurrentRow;
            List<String> emailList = new ArrayList<>();
            while ((fieldsInCurrentRow = listReader.read()) != null) {
                logger.info("fieldsInCurrentRow : "+listReader.getRowNumber()+"  "+fieldsInCurrentRow);
                LeadsquaredUser leadsquaredUser = new LeadsquaredUser();
                leadsquaredUser.setLeadsquaredId(fieldsInCurrentRow.get(0));
                leadsquaredUser.setFirstName(fieldsInCurrentRow.get(1));
                leadsquaredUser.setLastName(fieldsInCurrentRow.get(2));
                leadsquaredUser.setEmail(fieldsInCurrentRow.get(3));
                leadsquaredUser.setRole(fieldsInCurrentRow.get(4));
//                leadsquaredUser.setActive(fieldsInCurrentRow.get(5));
//                leadsquaredUser.setUserId(fieldsInCurrentRow.get(6));
//                leadsquaredUser.setConvoxAgentId(fieldsInCurrentRow.get(7));
//                leadsquaredUser.setBucketName(fieldsInCurrentRow.get(8));
                leadsquaredUserList.add(leadsquaredUser);

                emailList.add(fieldsInCurrentRow.get(3));
            }

            logger.info("leadsquaredUserList : "+leadsquaredUserList);
            logger.info("leadsquaredEmails : "+emailList);
            if(ArrayUtils.isNotEmpty(leadsquaredUserList)){
                leadsquaredUserDAO.deleteByEmais(emailList);
                leadsquaredUserDAO.insertLeadSquaredUserList(leadsquaredUserList);
            }
        }

        return leadsquaredUserList.size();
    }
}
