package com.vedantu.vedantudata.managers.leadsquared;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.vedantu.util.LogFactory;

@Service
public class LeadsquaredAsyncTaskManager {

	@Autowired
	private LeadsquaredManager leadsquaredManager;

	@Autowired
	private LeadsquaredActivityManager leadsquaredActivityManager;

	@Autowired
	private LogFactory logFactory;

	@SuppressWarnings({ "static-access" })
	private Logger logger = logFactory.getLogger(LeadsquaredAsyncTaskManager.class);

	// Triggered every 5 minutes
	@Async
	public void processLeadsquaredDate(long startTime, long endTime) {
		logger.info("Start time:" + startTime + " endTime:" + endTime);

		// Process leads
		try {
			leadsquaredManager.fetchLeadsByTimeRange(startTime, endTime);
		} catch (Exception ex) {
			logger.info("Error processing leads:" + startTime + " endTime:" + endTime);
		}

		// Process leads activities
		try {
			leadsquaredActivityManager.fetchLeadActivityByTimeRange(startTime, endTime);
		} catch (Exception ex) {
			logger.info("Error processing leads:" + startTime + " endTime:" + endTime);
		}

		// Process tasks
		try {
			// TODO : write logic
		} catch (Exception ex) {
			logger.info("Error processing tasks:" + startTime + " endTime:" + endTime);
		}
	}
}
