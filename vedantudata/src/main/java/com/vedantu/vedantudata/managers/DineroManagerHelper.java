package com.vedantu.vedantudata.managers;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.dinero.enums.InstalmentPurchaseEntity;
import com.vedantu.dinero.pojo.BaseInstalment;
import com.vedantu.exception.VException;
import com.vedantu.exception.VExceptionFactory;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.WebUtils;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

import java.lang.reflect.Type;
import java.util.List;

@Service
public class DineroManagerHelper {

    @Autowired
    private LogFactory logFactory;
    private Logger logger = logFactory.getLogger(DineroManagerHelper.class);

    private final static Gson GSON = new Gson();

    public List<BaseInstalment> getBaseInstalments(InstalmentPurchaseEntity instalmentPurchaseEntity,
                                                   List<String> purchaseEntityIds) throws VException {
        String dineroEndpoint = ConfigUtils.INSTANCE.getStringValue("DINERO_ENDPOINT");
        String url = "/payment/getBaseInstalments?instalmentPurchaseEntity=" + instalmentPurchaseEntity
                + "&purchaseEntityIds="
                + org.springframework.util.StringUtils.collectionToDelimitedString(purchaseEntityIds, ",");
        ;
        ClientResponse resp = WebUtils.INSTANCE.doCall(dineroEndpoint + url, HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        Type listType = new TypeToken<List<BaseInstalment>>() {
        }.getType();
        List<BaseInstalment> response = GSON.fromJson(jsonString, listType);
        logger.info("EXIT " + response);
        return response;
    }
}
