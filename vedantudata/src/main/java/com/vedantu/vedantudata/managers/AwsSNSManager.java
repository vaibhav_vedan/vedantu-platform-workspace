package com.vedantu.vedantudata.managers;

import com.vedantu.async.AsyncTaskFactory;
import com.vedantu.aws.AbstractAwsSNSManager;
import com.vedantu.aws.pojo.CronTopic;
import com.vedantu.util.LogFactory;
import com.vedantu.util.enums.SNSTopic;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;



@Service
public class AwsSNSManager extends AbstractAwsSNSManager {
    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(AwsSNSManager.class);

    @Autowired
    private AsyncTaskFactory asyncTaskFactory;

    public AwsSNSManager() {
        super();
        logger.info("initializing AwsSNSManager");
    }

    @Override
    public void createSubscriptions() {
        createCronSubscription(CronTopic.CRON_CHIME_15_Minutes, "trigger-activity/freshdesk/ticket/activity-cron");
        createCronSubscription(CronTopic.CRON_CHIME_30_Minutes, "vedantu/webinar/reminder");
        createCronSubscription(CronTopic.CRON_CHIME_DAILY_1AM_IST, "vedantu/onetofew/attendees/report/daily");
        createCronSubscription(CronTopic.CRON_CHIME_DAILY_6_30AM_IST, "trigger-activity/bank-refund-cron");
        createCronSubscription(CronTopic.CRON_CHIME_DAILY_6_30AM_IST, "trigger-activity/installments-cron");
        createCronSubscription(CronTopic.CRON_CHIME_DAILY_6_30AM_IST, "trigger-activity/sam-allocation-cron");
        createCronSubscription(CronTopic.CRON_CHIME_DAILY_6_30AM_IST, "trigger-activity/am-trigger-activities-cron");
        createCronSubscription(SNSTopic.UPDATE_LEAD_PAYMENT_LS, "leadsquared/lead/updatePaymentThroughSNS");
    }

    @Override
    public void createTopics() {
        createSNSTopic(CronTopic.CRON_CHIME_DAILY_6_30AM_IST);
        createSNSTopic(SNSTopic.UPDATE_LEAD_PAYMENT_LS);
    }


}
