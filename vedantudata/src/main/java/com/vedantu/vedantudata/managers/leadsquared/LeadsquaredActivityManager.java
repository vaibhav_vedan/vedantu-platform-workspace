package com.vedantu.vedantudata.managers.leadsquared;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.*;

import javax.annotation.PostConstruct;

import com.vedantu.scheduling.pojo.session.Session;
import com.vedantu.vedantudata.dao.serializers.LeadSquaredAgentDao;
import com.vedantu.vedantudata.entities.leadsquared.LeadSquaredAgent;
import com.vedantu.vedantudata.enums.SessionRole;
import com.vedantu.vedantudata.request.leadsquared.SalesWaveSessionJoinedLSRequest;
import com.vedantu.vedantudata.utils.CommonUtils;
import com.vedantu.vedantudata.utils.LeadSquaredUtils;
import org.apache.commons.io.IOUtils;
import org.apache.logging.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.User.UserBasicInfo;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.InternalServerErrorException;
import com.vedantu.exception.VException;
import com.vedantu.util.CollectionUtils;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.CustomValidator;
import com.vedantu.util.DateTimeUtils;
import com.vedantu.util.FosUtils;
import com.vedantu.util.LSWebUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.WebUtils;
import com.vedantu.util.logger.LoggingMarkers;
import com.vedantu.vedantudata.dao.serializers.LeadsquaredDAO;
import com.vedantu.vedantudata.entities.leadsquared.LeadActivity;
import com.vedantu.vedantudata.entities.leadsquared.LeadDetails;
import com.vedantu.vedantudata.entities.leadsquared.LeadsquaredUser;
import com.vedantu.vedantudata.pojos.ActivityDetailsPojo;
import com.vedantu.vedantudata.pojos.ActivityResponseMessage;
import com.vedantu.vedantudata.pojos.ActivityresponsepayloadPojo;
import com.vedantu.vedantudata.pojos.LeadsquaredActivityField;
import com.vedantu.vedantudata.pojos.request.OnlineSalesDemoSessionJoinedLSRequest;
import com.vedantu.vedantudata.request.leadsquared.GetLeadActivityByTimeRangeReq;
import com.vedantu.vedantudata.request.leadsquared.PostLeadActivitiyReq;
import com.vedantu.vedantudata.utils.LeadsquaredConfig;

@Service
public class LeadsquaredActivityManager extends LeadsquaredConfig {

    private static final Type leadActivityListType = new TypeToken<List<LeadActivity>>() {
    }.getType();
    private static Gson gson = new Gson();
    @Autowired
    private LogFactory logFactory;
    @Autowired
    private LeadsquaredDAO leadsquaredDAO;

    @Autowired
    private LeadsquaredManager leadsquaredManager;

    @Autowired
    private LSWebUtils lsWebUtils;

    @Autowired
    private FosUtils fosUtils;

    @Autowired
    private LeadSquaredAgentDao leadSquaredAgentDao;



    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(LeadsquaredActivityManager.class);

    @PostConstruct
    public void init() {

        accessKey = ConfigUtils.INSTANCE.getStringValue("leadsquared.access.id");
        secretKey = ConfigUtils.INSTANCE.getStringValue("leadsquared.secret.key");

        try {
            new File("lead").mkdir();
        } catch (Exception ex) {

        }
        try {
            new File("leadActivity").mkdir();
        } catch (Exception ex) {

        }
    }

    public void syncAllLeadActivitesPreviousHour() {
        long currentTime = System.currentTimeMillis();
        currentTime -= currentTime % DateTimeUtils.MILLIS_PER_MINUTE;
        long startTime = currentTime - DateTimeUtils.MILLIS_PER_HOUR;
        long duration = DateTimeUtils.MILLIS_PER_MINUTE;
        for (long time = startTime; time < currentTime; time += duration) {
            try {
                fetchLeadActivityByTimeRange(time, time + duration);
            } catch (Exception ex) {
                logger.error("Error update");
            }
        }
    }

    public void PostActivityOnLead(PostLeadActivitiyReq req) {

        logger.info("Request: " + req.toString());

        leadsquaredManager.createLeadIfDeleted(req.getEmailAddress(), req.getPhone());

        String serverUrl = appendAccessKeys(postLeadActivityUrl);
        ClientResponse cRes = WebUtils.INSTANCE.doCall(serverUrl, HttpMethod.POST, gson.toJson(req));
        logger.info(LoggingMarkers.JSON_MASK,"cRes:" + cRes.toString());
        if (cRes.getStatus() == 429) {
            try {
                Thread.sleep(1000);
            } catch (Exception ex) {

            }
            PostActivityOnLead(req);
        }
    }

    public ActivityresponsepayloadPojo pushActivitiesByEmail(Collection<ActivityDetailsPojo> req) throws VException, InterruptedException {
        String serverUrl = appendAccessKeys(postLeadActivityUrlByField) + "&searchBy=EmailAddress";
        return pushActivities(req, serverUrl);

    }

    private ActivityresponsepayloadPojo pushActivities(Collection<ActivityDetailsPojo> req,  String serverUrl ) throws VException, InterruptedException {

        for(ActivityDetailsPojo detailsPojo : req){
            String searchByValue = detailsPojo.getSearchByValue();

            //Checking if the search value is email or phone
            if(CustomValidator.validEmail(searchByValue)) {
                if(searchByValue.matches(".*[a-zA-Z]+.*"))
                    leadsquaredManager.createLeadIfDeleted(searchByValue, null);
                else
                    leadsquaredManager.createLeadIfDeleted(null, searchByValue);
            }
        }

        if(req.size() <= 5){

            List<ActivityResponseMessage> activityResponseMessages = new ArrayList<>();

            int idx = 0;
            for(ActivityDetailsPojo detailsPojo : req){
                idx++;

                ActivityResponseMessage activityResponseMessage = new ActivityResponseMessage();
                activityResponseMessage.setRowNumber(idx);

                String searchByValue = detailsPojo.getSearchByValue();
                if(searchByValue.matches(".*[a-zA-Z]+.*")){
                    detailsPojo.setEmailAddress(searchByValue);
                }else {
                    List<LeadDetails> leadDetails = leadsquaredManager.fetchLeadsByPhoneOrEmail(searchByValue);

                    if (CollectionUtils.isNotEmpty(leadDetails))
                        detailsPojo.setRelatedProspectId(leadDetails.get(0).getProspectID());
                }

                Boolean isSuccess = pushActivitiy(detailsPojo);

                activityResponseMessage.setActivityUpdated(isSuccess);
                activityResponseMessages.add(activityResponseMessage);
            }

            ActivityresponsepayloadPojo pojo = new ActivityresponsepayloadPojo();
            pojo.setResponse(activityResponseMessages);
            return pojo;

        }else {
            for (int i = 0; (i < 7); i++) {
                logger.info("Request: " + req.toString());
                //changed to routing url as per threshold
                ClientResponse cRes = lsWebUtils.leadsquaredDoBulkCallWithRetries(serverUrl, HttpMethod.POST, gson.toJson(req),true);
                logger.info(LoggingMarkers.JSON_MASK,"cRes:" + cRes.toString());

                String jsonString = cRes.getEntity(String.class);
                if (cRes.getStatus() != 200 && cRes.getStatus() != 429) {
                    logger.warn("Error pushing data to leadsquared- Code:" + cRes.getStatus() + " response:" + jsonString
                            + " req: " + req.toString() + ", url:" + serverUrl);
                } else if (cRes.getStatus() == 429) {
                    try {
                        Thread.sleep(1000);
                    } catch (Exception ex) {

                    }
                    continue;
                } else {
                    logger.info("Data successfully posted");
                    ActivityresponsepayloadPojo activityresponsepayloadPojo = new Gson().fromJson(jsonString, ActivityresponsepayloadPojo.class);
                    return activityresponsepayloadPojo;
                }
            }
        }

        return null;
    }

    public ActivityresponsepayloadPojo pushActivitiesByMobile(Collection<ActivityDetailsPojo> req) throws VException, InterruptedException {
        String serverUrl = appendAccessKeys(postLeadActivityUrlByField) + "&searchBy=Phone";
        return pushActivities(req, serverUrl);
    }

    public ActivityresponsepayloadPojo updateActivities(Collection<ActivityDetailsPojo> reqs) throws VException, InterruptedException {
        logger.info("Request: " + reqs.toString());
        if(reqs.size() <= 5){

            List<ActivityResponseMessage> activityResponseMessages = new ArrayList<>();
            String serverUrl = appendAccessKeys(postLeadActivityUpdateUrl);

            int idx = 0;
            for(ActivityDetailsPojo req : reqs){
                idx++;

                ActivityResponseMessage activityResponseMessage = new ActivityResponseMessage();
                activityResponseMessage.setRowNumber(idx);
                activityResponseMessage.setActivityUpdated(false);

                for (int i = 0; (i < 5); i++) {

                    //changed to routing url as per threshold
                    ClientResponse cRes = WebUtils.INSTANCE.doCall(serverUrl, HttpMethod.POST, gson.toJson(req), true);
                    logger.info(LoggingMarkers.JSON_MASK, "cRes:" + cRes.toString());

                    String jsonString = cRes.getEntity(String.class);

                    if (cRes.getStatus() != 200 && cRes.getStatus() != 429) {
                        logger.error("Error pushing activity to LS : {}", cRes);
                        break;
                    } else if (cRes.getStatus() == 429) {
                        Thread.sleep(1000);
                        continue;
                    } else {
                        activityResponseMessage.setActivityUpdated(true);
                        break;
                    }
                }

                activityResponseMessages.add(activityResponseMessage);
            }
            ActivityresponsepayloadPojo pojo = new ActivityresponsepayloadPojo();
            pojo.setResponse(activityResponseMessages);
            return pojo;
        }else {

            for (int i = 0; (i < 5); i++) {

                String serverUrl = appendAccessKeys(postLeadActivityUpdateUrlBulk);
                //changed to routing url as per threshold
                ClientResponse cRes = lsWebUtils.leadsquaredDoBulkCallWithRetries(serverUrl, HttpMethod.POST, gson.toJson(reqs), true);
                logger.info(LoggingMarkers.JSON_MASK, "cRes:" + cRes.toString());

                String jsonString = cRes.getEntity(String.class);

                if (cRes.getStatus() != 200 && cRes.getStatus() != 429) {
                    logger.error("Error pushing activity to LS : {}", cRes);
                    throw new InternalServerErrorException(ErrorCode.SERVICE_ERROR, "Error pushing activity to LS");
                } else if (cRes.getStatus() == 429) {
                    Thread.sleep(1000);
                    continue;
                } else {
                    return new Gson().fromJson(jsonString, ActivityresponsepayloadPojo.class);
                }

            }
        }
        return null;
    }

    public Boolean pushActivitiy(ActivityDetailsPojo req) throws VException, InterruptedException {

        String serverUrl = appendAccessKeys(postLeadActivityUrl);

        for (int i = 0; (i < 5); i++) {

            //changed to routing url as per threshold
            ClientResponse cRes = WebUtils.INSTANCE.doCall(serverUrl, HttpMethod.POST, gson.toJson(req), true);
            logger.info(LoggingMarkers.JSON_MASK, "cRes:" + cRes.toString());

            String jsonString = cRes.getEntity(String.class);

            if (cRes.getStatus() != 200 && cRes.getStatus() != 429) {
                logger.warn("Error pushing activity to LS : {}", cRes);
                return false;
            } else if (cRes.getStatus() == 429) {
                Thread.sleep(1000);
                continue;
            } else {
                return true;
            }
        }

        return false;
    }

    public void fetchAllLeadActivities(long startTime, long endTime) {
        long currentTime = startTime;
        long currentDayStartTime = currentTime % DateTimeUtils.MILLIS_PER_DAY == 0 ? currentTime
                : (currentTime - DateTimeUtils.MILLIS_PER_DAY);
        logger.info("currentDayTime :" + currentDayStartTime + " currentTime:" + currentTime);
        if (endTime <= 0) {
            endTime = currentDayStartTime - (Long.valueOf(DateTimeUtils.MILLIS_PER_DAY) * 365 * 2);
        }

        for (long time = currentDayStartTime; time > endTime; time -= DateTimeUtils.MILLIS_PER_DAY) {
            try {
                fetchLeadActivityByDay(time);
            } catch (Exception ex) {
                logger.error("Error occured:" + ex.toString());
            }
        }
    }

    public List<String> fetchLeadActivityByDay(long startTime) throws IOException {
        long startDate = startTime - (startTime % DateTimeUtils.MILLIS_PER_DAY);
        long duration = DateTimeUtils.MILLIS_PER_HOUR;
        logger.info("Request:" + startDate + " duration:" + duration);
        List<String> activitiesFetched = new ArrayList<>();
        for (long time = startDate; time < (startDate + DateTimeUtils.MILLIS_PER_DAY); time += duration) {
            try {
                int fetchedCount = fetchLeadActivityByTimeRange(time, time + duration);
                activitiesFetched.add(String.valueOf(fetchedCount));
            } catch (Exception ex) {
                logger.error("Error:" + time + " ex:" + ex.toString());
                activitiesFetched.add("time:" + time);
            }
        }

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        sdf.setTimeZone(TimeZone.getTimeZone(DateTimeUtils.TIME_ZONE_GMT));
        String startDateStr = sdf.format(new Date(startDate));
        File file = new File("leadActivity", "LeadActivity_" + startDateStr + "_" + System.currentTimeMillis());
        IOUtils.write(Arrays.toString(activitiesFetched.toArray()), new FileOutputStream(file));
        return activitiesFetched;
    }

    public List<LeadActivity> fetchLeadActivityByLeadId(String leadId) {
        List<LeadActivity> activities = new ArrayList<LeadActivity>();
        int offset = 0;
        int size = 1000;
        while (true) {
            String serverUrl = appendAccessKeys(getLeadActivityByLeadId) + "&leadId=" + leadId;

            JSONObject pageLimits = new JSONObject();
            pageLimits.put("Offset", offset);
            pageLimits.put("RowCount", size);
            JSONObject fetchReq = new JSONObject();
            fetchReq.put("Paging", pageLimits);
            JSONObject activityEvent = new JSONObject();
            activityEvent.put("ActivityEvent", "244");
            fetchReq.put("Parameter", activityEvent);

            ClientResponse cRes = WebUtils.INSTANCE.doCall(serverUrl, HttpMethod.POST, fetchReq.toString());
            logger.info("cRes:" + cRes.getStatus());
            String response = cRes.getEntity(String.class);
            if (cRes.getStatus() != 200 && cRes.getStatus() != 429) {
                logger.error(LoggingMarkers.JSON_MASK, "InvalidStatusCodeGetUsers - get lead users not 200 - " + cRes.getStatus() + " cRes:"
                        + cRes.toString());
                return activities;
            } else if (cRes.getStatus() == 429) {
                logger.error(LoggingMarkers.JSON_MASK,"Too many requests for fetchLeadActivityByLeadId " + leadId + ", response: " + response);
                return null;
            }

            if (StringUtils.isEmpty(response)) {
                break;
            }

            JSONObject jsonObject = new JSONObject(response);
            JSONArray res = jsonObject.getJSONArray("ProspectActivities");
            List<LeadActivity> results = gson.fromJson(res.toString(), leadActivityListType);
            if (CollectionUtils.isEmpty(results)) {
                break;
            }

            activities.addAll(results);
            // Handle loop params
            offset += size;
        }
        return activities;
    }

    public int fetchLeadActivityByTimeRange(long startTime, long endTime) throws Exception {
        logger.info("Request:" + startTime + " endTime:" + endTime);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        sdf.setTimeZone(TimeZone.getTimeZone(DateTimeUtils.TIME_ZONE_GMT));
        String startDate = sdf.format(new Date(startTime));
        String endDate = sdf.format(new Date(endTime));

        int totalFetched = 0;
        int page = 1;
        while (true) {
            GetLeadActivityByTimeRangeReq req = new GetLeadActivityByTimeRangeReq(startDate, endDate, page, 1000);
            String serverUrl = appendAccessKeys(getLeadActivityByTimeIntervalUrl);
            ClientResponse cRes = WebUtils.INSTANCE.doCall(serverUrl, HttpMethod.POST, gson.toJson(req));
            logger.info("cRes:" + cRes.getStatus());
            String response = cRes.getEntity(String.class);
            if (cRes.getStatus() != 200 && cRes.getStatus() != 429) {
                logger.info("InvalidStatusCode getLeadActivityByTimeIntervalUrl - not 200 - "
                        + cRes.getStatus() + " cRes:" + cRes.toString());
                throw new InternalServerErrorException(ErrorCode.SERVICE_ERROR, "InvalidStatusCode getLeadActivityByTimeIntervalUrl - not 200 - "
                        + cRes.getStatus() + " cRes:" + cRes.toString());
            } else if (cRes.getStatus() == 429) {
                logger.error("Too many requests for fetchLeadActivityByTimeRange , response: " + response);
                Thread.sleep(5000);
                continue;
            }

            if (StringUtils.isEmpty(response)) {
                break;
            }

            JSONObject jsonObject = new JSONObject(response);
            // TODO : Validate if record counts is zero, if it contains
            // ProspectActivities
            JSONArray jsString = jsonObject.getJSONArray("ProspectActivities");
            if (jsString == null || jsString.length() <= 0) {
                break;
            }

            List<LeadActivity> activities = gson.fromJson(jsString.toString(), leadActivityListType);
            int recordCount = CollectionUtils.isEmpty(activities) ? 0 : activities.size();
            if (recordCount == 0) {
                break;
            }
            logger.info("page:" + page + " count:" + recordCount);
            saveAllActivities(activities);
            totalFetched += recordCount;
            page++;
        }

        return totalFetched;
    }

    private void saveAllActivities(List<LeadActivity> leadActivities) throws Exception {
        if (CollectionUtils.isEmpty(leadActivities)) {
            return;
        }

        Query query = new Query();
        List<String> activityIdList = getAllActivityEventList(leadActivities);
        query.addCriteria(Criteria.where(LeadActivity.Constants.ACTIVITY_ID).in(activityIdList));
        List<LeadActivity> existingActivities = leadsquaredDAO.runQuery(query, LeadActivity.class);
        Map<String, LeadActivity> activityMap = getAllActivityEventMap(existingActivities);
        for (LeadActivity leadActivity : leadActivities) {
            if (!activityMap.containsKey(leadActivity.getActivityId())) {
                leadsquaredDAO.createLeadActivity(leadActivity);
            }
        }
    }

    private List<String> getAllActivityEventList(List<LeadActivity> leadActivities) {
        List<String> activityIdList = new ArrayList<>();
        if (!CollectionUtils.isEmpty(leadActivities)) {
            for (LeadActivity leadActivity : leadActivities) {
                activityIdList.add(leadActivity.getActivityId());
            }
        }

        return activityIdList;
    }

    private Map<String, LeadActivity> getAllActivityEventMap(List<LeadActivity> leadActivities) {
        Map<String, LeadActivity> activityMap = new HashMap<>();
        if (!CollectionUtils.isEmpty(leadActivities)) {
            for (LeadActivity leadActivity : leadActivities) {
                activityMap.put(leadActivity.getActivityId(), leadActivity);
            }
        }

        return activityMap;
    }

    private String appendAccessKeys(String url) {

        if (url.contains("?")) {
            return url + "&accessKey=" + accessKey + "&secretKey=" + secretKey;
        } else {
            return url + "?accessKey=" + accessKey + "&secretKey=" + secretKey;
        }
    }

    public LeadsquaredActivityField provideLeadsquaredActivityFieldFromCustomeKeyPair(String mxCustom,String value)
    {
        LeadsquaredActivityField leadsquaredActivityField=new LeadsquaredActivityField();
        leadsquaredActivityField.setSchemaName(mxCustom);
        leadsquaredActivityField.setValue(value);
        return leadsquaredActivityField;

    }

    public void convertAndPushLeadActivitiesMessagesToLeadSquared(String message) throws VException, InterruptedException {
        OnlineSalesDemoSessionJoinedLSRequest onlineSalesDemoSessionJoinedLSRequest = gson.fromJson(message,
                OnlineSalesDemoSessionJoinedLSRequest.class);
        String studentEmailAddress = "";
        String teacherEmailAddress = "";
        String activityNote = "Online counselling session joined-" + onlineSalesDemoSessionJoinedLSRequest.getStudentId();
        UserBasicInfo student = fosUtils.getUserBasicInfo(onlineSalesDemoSessionJoinedLSRequest.getStudentId(),
                true);
        UserBasicInfo agent = fosUtils.getUserBasicInfo(onlineSalesDemoSessionJoinedLSRequest.getAgentId(),
                true);
        teacherEmailAddress = agent.getEmail();
        studentEmailAddress = student.getEmail();
        List<ActivityDetailsPojo> activityDetailsEmailPojos = new ArrayList<>();
        List<ActivityDetailsPojo> activityDetailsMobilePojos = new ArrayList<>();
        List<LeadsquaredActivityField> customFields = new ArrayList<>();
        ActivityDetailsPojo activityDetailsPojo = new ActivityDetailsPojo();
        activityDetailsPojo.setActivityNote(activityNote);
        activityDetailsPojo.setActivityEvent(
                ConfigUtils.INSTANCE.getIntValue("leadsquared.activity.counselling.session.joined"));

        customFields.add(
                provideLeadsquaredActivityFieldFromCustomeKeyPair("mx_Custom_8", teacherEmailAddress));
        teacherEmailAddress=teacherEmailAddress.replaceAll("\\+.*@", "@");
        customFields.add(
                provideLeadsquaredActivityFieldFromCustomeKeyPair("mx_Custom_4", teacherEmailAddress));
        if (StringUtils.isNotEmpty(onlineSalesDemoSessionJoinedLSRequest.getSessionJoinTimeTeacher()))
            customFields.add(
                    provideLeadsquaredActivityFieldFromCustomeKeyPair("mx_Custom_3", CommonUtils.calculateTimeStampIst(Long
                            .parseLong(onlineSalesDemoSessionJoinedLSRequest.getSessionJoinTimeTeacher()))));

        if (StringUtils.isNotEmpty(onlineSalesDemoSessionJoinedLSRequest.getSessionJoinTimeStudent()))
            customFields.add(
                    provideLeadsquaredActivityFieldFromCustomeKeyPair("mx_Custom_2", CommonUtils.calculateTimeStampIst(Long
                            .parseLong(onlineSalesDemoSessionJoinedLSRequest.getSessionJoinTimeStudent()))));
        if (StringUtils.isNotEmpty(onlineSalesDemoSessionJoinedLSRequest.getSessionStartTime()))
            customFields.add(
                    provideLeadsquaredActivityFieldFromCustomeKeyPair("mx_Custom_7", CommonUtils.calculateTimeStampIst(Long
                            .parseLong(onlineSalesDemoSessionJoinedLSRequest.getSessionStartTime()))));
        if (StringUtils.isNotEmpty(onlineSalesDemoSessionJoinedLSRequest.getSessionDuration()))
            customFields.add(
                    provideLeadsquaredActivityFieldFromCustomeKeyPair("mx_Custom_6", Objects.toString(Long.parseLong(onlineSalesDemoSessionJoinedLSRequest.getSessionDuration()) / DateTimeUtils.MILLIS_PER_MINUTE)));
        if (StringUtils.isNotEmpty(onlineSalesDemoSessionJoinedLSRequest.getSessionId()))
            customFields.add(
                    provideLeadsquaredActivityFieldFromCustomeKeyPair("mx_Custom_1", onlineSalesDemoSessionJoinedLSRequest.getSessionId()));
        List<LeadsquaredUser> agents = leadsquaredManager.fetchAgentsByEmailId(teacherEmailAddress);
        if (CollectionUtils.isNotEmpty(agents)) {
            customFields.add(
                    provideLeadsquaredActivityFieldFromCustomeKeyPair("mx_Custom_5", agents.get(0).getUserId()));
            customFields.add(
                    provideLeadsquaredActivityFieldFromCustomeKeyPair("Owner", agents.get(0).getUserId()));
        }

        String studentPhone=student.getContactNumber();

        if(StringUtils.isNotEmpty(studentPhone)){
            activityDetailsPojo.setSearchByValue(studentPhone);
            activityDetailsMobilePojos.add(activityDetailsPojo);
        } else if (StringUtils.isNotEmpty(studentEmailAddress) && LeadSquaredUtils.validateLSEmails(studentEmailAddress)) {
            activityDetailsPojo.setEmailAddress(studentEmailAddress);
            activityDetailsPojo.setSearchByValue(studentEmailAddress);
            activityDetailsEmailPojos.add(activityDetailsPojo);
        }
        activityDetailsPojo.setFields(customFields);
        if (CollectionUtils.isNotEmpty(activityDetailsMobilePojos)) {
            logger.info("======triggering activities search by mobile");
            ActivityresponsepayloadPojo activityMobileresponsepayloadPojo = pushActivitiesByMobile(activityDetailsMobilePojos);
            logger.info("Activity created for online counselling joined {}", activityMobileresponsepayloadPojo);
        } else {
            logger.info("======Ignoring activities search by phone since the list is empty");
        }
        if (CollectionUtils.isNotEmpty(activityDetailsEmailPojos)) {
            logger.info("======triggering activities search by email");
            ActivityresponsepayloadPojo activityEmailresponsepayloadPojo = pushActivitiesByEmail(activityDetailsEmailPojos);
            logger.info("Activity created for online counselling joined {}", activityEmailresponsepayloadPojo);
        } else {
            logger.info("======Ignoring activities search by email and search by phonesince the list is empty");
        }
    }

    public void convertAndPushLeadActivitiesMessagesToLeadSquaredTeacher(Map<SessionRole, List<SalesWaveSessionJoinedLSRequest>> teacherStudentMap) throws VException, InterruptedException {
        SalesWaveSessionJoinedLSRequest studentSessionData=teacherStudentMap.get(SessionRole.STUDENT).get(0);
        SalesWaveSessionJoinedLSRequest teacherSessionData=teacherStudentMap.get(SessionRole.TEACHER).get(0);
        String studentEmailAddress = "";
        String teacherEmailAddress = "";
        String activityNote = "Online counselling session joined-" + studentSessionData.getUserId();
        UserBasicInfo student = fosUtils.getUserBasicInfo(studentSessionData.getUserId(),
                true);
        UserBasicInfo agent = fosUtils.getUserBasicInfo(teacherSessionData.getUserId(),
                true);
        teacherEmailAddress = agent.getEmail();
        studentEmailAddress = student.getEmail();
        List<ActivityDetailsPojo> activityDetailsEmailPojos = new ArrayList<>();
        List<ActivityDetailsPojo> activityDetailsMobilePojos = new ArrayList<>();
        List<LeadsquaredActivityField> customFields = new ArrayList<>();
        ActivityDetailsPojo activityDetailsPojo = new ActivityDetailsPojo();
        activityDetailsPojo.setActivityNote(activityNote);
        activityDetailsPojo.setActivityEvent(
                ConfigUtils.INSTANCE.getIntValue("leadsquared.activity.counselling.session.joined"));

        customFields.add(
                provideLeadsquaredActivityFieldFromCustomeKeyPair("mx_Custom_8", teacherEmailAddress));
        teacherEmailAddress=teacherEmailAddress.replaceAll("\\+.*@", "@");
        customFields.add(
                provideLeadsquaredActivityFieldFromCustomeKeyPair("mx_Custom_4", teacherEmailAddress));
        if (StringUtils.isNotEmpty(teacherSessionData.getSessionJoinTime()))
            customFields.add(
                    provideLeadsquaredActivityFieldFromCustomeKeyPair("mx_Custom_3", CommonUtils.calculateTimeStampIst(Long
                            .parseLong(teacherSessionData.getSessionJoinTime()))));

        if (StringUtils.isNotEmpty(studentSessionData.getSessionJoinTime()))
            customFields.add(
                    provideLeadsquaredActivityFieldFromCustomeKeyPair("mx_Custom_2", CommonUtils.calculateTimeStampIst(Long
                            .parseLong(studentSessionData.getSessionJoinTime()))));
        if (StringUtils.isNotEmpty(studentSessionData.getSessionStartTime()))
            customFields.add(
                    provideLeadsquaredActivityFieldFromCustomeKeyPair("mx_Custom_7", CommonUtils.calculateTimeStampIst(Long
                            .parseLong(studentSessionData.getSessionStartTime()))));
        if (StringUtils.isNotEmpty(studentSessionData.getSessionDuration()))
            customFields.add(
                    provideLeadsquaredActivityFieldFromCustomeKeyPair("mx_Custom_6", Objects.toString(Long.parseLong(studentSessionData.getSessionDuration()) / DateTimeUtils.MILLIS_PER_MINUTE)));
        if (StringUtils.isNotEmpty(studentSessionData.getSessionId()))
            customFields.add(
                    provideLeadsquaredActivityFieldFromCustomeKeyPair("mx_Custom_1", studentSessionData.getSessionId()));
        List<LeadsquaredUser> agents = leadsquaredManager.fetchAgentsByEmailId(teacherEmailAddress);
        if (CollectionUtils.isNotEmpty(agents)) {
            customFields.add(
                    provideLeadsquaredActivityFieldFromCustomeKeyPair("mx_Custom_5", agents.get(0).getUserId()));
            customFields.add(
                    provideLeadsquaredActivityFieldFromCustomeKeyPair("Owner", agents.get(0).getUserId()));
        }

        String studentPhone=student.getContactNumber();

        if(StringUtils.isNotEmpty(studentPhone)){
            activityDetailsPojo.setSearchByValue(studentPhone);
            activityDetailsMobilePojos.add(activityDetailsPojo);
        } else if (StringUtils.isNotEmpty(studentEmailAddress) && LeadSquaredUtils.validateLSEmails(studentEmailAddress)) {
            activityDetailsPojo.setEmailAddress(studentEmailAddress);
            activityDetailsPojo.setSearchByValue(studentEmailAddress);
            activityDetailsEmailPojos.add(activityDetailsPojo);
        }
        activityDetailsPojo.setFields(customFields);
        if (CollectionUtils.isNotEmpty(activityDetailsMobilePojos)) {
            logger.info("======triggering activities search by mobile");
            ActivityresponsepayloadPojo activityMobileresponsepayloadPojo = pushActivitiesByMobile(activityDetailsMobilePojos);
            logger.info("Activity created for online counselling joined {}", activityMobileresponsepayloadPojo);
        } else {
            logger.info("======Ignoring activities search by phone since the list is empty");
        }
        if (CollectionUtils.isNotEmpty(activityDetailsEmailPojos)) {
            logger.info("======triggering activities search by email");
            ActivityresponsepayloadPojo activityEmailresponsepayloadPojo = pushActivitiesByEmail(activityDetailsEmailPojos);
            logger.info("Activity created for online counselling joined {}", activityEmailresponsepayloadPojo);
        } else {
            logger.info("======Ignoring activities search by email and search by phonesince the list is empty");
        }
    }

    public void convertAndPushLeadActivitiesMessagesToLeadSquaredTA(Map<SessionRole, List<SalesWaveSessionJoinedLSRequest>>teacherTAMap) throws VException, InterruptedException {
        SalesWaveSessionJoinedLSRequest studentSessionData=teacherTAMap.get(SessionRole.STUDENT).get(0);
        SalesWaveSessionJoinedLSRequest teacherSessionData=teacherTAMap.get(SessionRole.TEACHER).get(0);
        SalesWaveSessionJoinedLSRequest taSessionData=teacherTAMap.get(SessionRole.TA).get(0);

        String studentEmailAddress = StringUtils.EMPTY;
        String teacherEmailAddress = StringUtils.EMPTY;
        String taEmailAddress=StringUtils.EMPTY;
        String activityNote = "Online counselling session joined TA-" + taSessionData.getUserId();
        UserBasicInfo student = fosUtils.getUserBasicInfo(studentSessionData.getUserId(),
                true);
        UserBasicInfo agent = fosUtils.getUserBasicInfo(teacherSessionData.getUserId(),
                true);
        UserBasicInfo ta = fosUtils.getUserBasicInfo(taSessionData.getUserId(),
                true);
        teacherEmailAddress = agent.getEmail();
        studentEmailAddress = student.getEmail();
        taEmailAddress=ta.getEmail();
        List<ActivityDetailsPojo> activityDetailsEmailPojos = new ArrayList<>();
        List<ActivityDetailsPojo> activityDetailsMobilePojos = new ArrayList<>();
        List<LeadsquaredActivityField> customFields = new ArrayList<>();
        ActivityDetailsPojo activityDetailsPojo = new ActivityDetailsPojo();
        activityDetailsPojo.setActivityNote(activityNote);
        activityDetailsPojo.setActivityEvent(
                ConfigUtils.INSTANCE.getIntValue("leadsquared.activity.counselling.session.joined.ta"));

        if (StringUtils.isNotEmpty(taSessionData.getSessionId()))
            customFields.add(
                    provideLeadsquaredActivityFieldFromCustomeKeyPair("mx_Custom_2", taSessionData.getSessionId()));
        customFields.add(
					provideLeadsquaredActivityFieldFromCustomeKeyPair("mx_Custom_3", taEmailAddress));
        if (StringUtils.isNotEmpty(taSessionData.getSessionJoinTime()))
            customFields.add(
                    provideLeadsquaredActivityFieldFromCustomeKeyPair("mx_Custom_4", CommonUtils.calculateTimeStampIst(Long
                            .parseLong(taSessionData.getSessionJoinTime()))));
        if (StringUtils.isNotEmpty(taSessionData.getSessionStartTime()))
            customFields.add(
                    provideLeadsquaredActivityFieldFromCustomeKeyPair("mx_Custom_5", CommonUtils.calculateTimeStampIst(Long
                            .parseLong(taSessionData.getSessionStartTime()))));
        taEmailAddress=taEmailAddress.replaceAll("\\+.*@", "@");
        customFields.add(
                provideLeadsquaredActivityFieldFromCustomeKeyPair("mx_Custom_6", taEmailAddress));
       List<LeadSquaredAgent> taAgents=leadSquaredAgentDao.getAgentByEmailIds(Collections.singletonList(taEmailAddress));
       if(CollectionUtils.isNotEmpty(taAgents))
       {
           String designation=taAgents.get(0).getDesignation();
           customFields.add(
                   provideLeadsquaredActivityFieldFromCustomeKeyPair("mx_Custom_7", designation));
       }
        teacherEmailAddress=teacherEmailAddress.replaceAll("\\+.*@", "@");
        List<LeadsquaredUser> agents = leadsquaredManager.fetchAgentsByEmailId(teacherEmailAddress);
        if (CollectionUtils.isNotEmpty(agents)) {
            customFields.add(
                    provideLeadsquaredActivityFieldFromCustomeKeyPair("mx_Custom_1", agents.get(0).getUserId()));
            customFields.add(
                    provideLeadsquaredActivityFieldFromCustomeKeyPair("Owner", agents.get(0).getUserId()));
        }

        String studentPhone=student.getContactNumber();

        if(StringUtils.isNotEmpty(studentPhone)){
            activityDetailsPojo.setSearchByValue(studentPhone);
            activityDetailsMobilePojos.add(activityDetailsPojo);
        } else if (StringUtils.isNotEmpty(studentEmailAddress) && LeadSquaredUtils.validateLSEmails(studentEmailAddress)) {
            activityDetailsPojo.setEmailAddress(studentEmailAddress);
            activityDetailsPojo.setSearchByValue(studentEmailAddress);
            activityDetailsEmailPojos.add(activityDetailsPojo);
        }
        activityDetailsPojo.setFields(customFields);
        if (CollectionUtils.isNotEmpty(activityDetailsMobilePojos)) {
            logger.info("======triggering activities search by mobile");
            ActivityresponsepayloadPojo activityMobileresponsepayloadPojo = pushActivitiesByMobile(activityDetailsMobilePojos);
            logger.info("Activity created for online counselling joined ta :{}", activityMobileresponsepayloadPojo);
        } else {
            logger.info("======Ignoring activities search by phone since the list is empty");
        }
        if (CollectionUtils.isNotEmpty(activityDetailsEmailPojos)) {
            logger.info("======triggering activities search by email");
            ActivityresponsepayloadPojo activityEmailresponsepayloadPojo = pushActivitiesByEmail(activityDetailsEmailPojos);
            logger.info("Activity created for online counselling joined ta :{}", activityEmailresponsepayloadPojo);
        } else {
            logger.info("======Ignoring activities search by email and search by phone since the list is empty");
        }
    }

}
