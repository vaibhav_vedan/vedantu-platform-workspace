package com.vedantu.vedantudata.managers;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.User.User;
import com.vedantu.User.UserInfo;
import com.vedantu.dinero.enums.InstalmentPurchaseEntity;
import com.vedantu.dinero.pojo.BaseInstalment;
import com.vedantu.exception.VException;
import com.vedantu.exception.VExceptionFactory;
import com.vedantu.onetofew.pojo.BatchBasicInfo;
import com.vedantu.onetofew.pojo.CourseInfo;
import com.vedantu.onetofew.pojo.OTFBundleInfo;
import com.vedantu.onetofew.request.GetOTFBundlesReq;
import com.vedantu.subscription.pojo.BundleInfo;
import com.vedantu.subscription.pojo.BundlePackageDetailsInfo;
import com.vedantu.subscription.request.GetBundleEnrolmentsReq;
import com.vedantu.subscription.request.GetBundlesReq;
import com.vedantu.subscription.response.BundleEnrolmentInfo;
import com.vedantu.subscription.response.CoursePlanBasicInfo;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.FosUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.WebUtils;
import org.apache.logging.log4j.Logger;
import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class SubscriptionManagerHelper {

    @Autowired
    private LogFactory logFactory;
    private Logger logger = logFactory.getLogger(SubscriptionManagerHelper.class);

    @Autowired
    private FosUtils fosUtils;

    @Autowired
    private DineroManagerHelper dineroManagerHelper;

    private final static Gson GSON = new Gson();

    private static final String SUBSCRIPTION_ENDPOINT = ConfigUtils.INSTANCE.getStringValue("SUBSCRIPTION_ENDPOINT");
    private static final String ONETOFEW_ENDPOINT = ConfigUtils.INSTANCE.getStringValue("SUBSCRIPTION_ENDPOINT") + "/";

    public List<BatchBasicInfo> getBatchBasicInfosByIds(List<String> ids) throws VException {
        String batchUrl = SUBSCRIPTION_ENDPOINT + "/batch/getBatchBasicInfosByIds";
        ClientResponse batchResp = WebUtils.INSTANCE.doCall(batchUrl, HttpMethod.POST, new Gson().toJson(ids));
        VExceptionFactory.INSTANCE.parseAndThrowException(batchResp);
        String jsonString = batchResp.getEntity(String.class);
        Type listType = new TypeToken<List<BatchBasicInfo>>() {
        }.getType();
        List<BatchBasicInfo> batchBasicInfo = GSON.fromJson(jsonString, listType);

        return batchBasicInfo;
    }

    public List<CourseInfo> getCoursesBasicInfos(List<String> courseIds)
            throws VException, JSONException, IllegalArgumentException, IllegalAccessException {
        String ids = "";
        for (String id : courseIds) {
            ids += "ids=" + id + "&";
        }
        String getCoursesUrl = SUBSCRIPTION_ENDPOINT+"/course/getCoursesBasicInfos" + "?" + ids;
        ClientResponse resp = WebUtils.INSTANCE.doCall(getCoursesUrl, HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        Type listType = new TypeToken<ArrayList<CourseInfo>>() {
        }.getType();
        List<CourseInfo> courses = new Gson().fromJson(jsonString, listType);
        return courses;
    }

    public CoursePlanBasicInfo getCoursePlanBasicInfo(String id, boolean fillDetails, boolean exposeEmail) throws VException {
        // TODO add access control
        ClientResponse resp = WebUtils.INSTANCE.doCall(SUBSCRIPTION_ENDPOINT + "/courseplan/basicInfo/" + id,
                HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        CoursePlanBasicInfo coursePlanBasicInfo = GSON.fromJson(jsonString, CoursePlanBasicInfo.class);
        if (fillDetails) {
            List<Long> userIds = new ArrayList<>();
            userIds.add(coursePlanBasicInfo.getStudentId());
            userIds.add(coursePlanBasicInfo.getTeacherId());
            Map<Long, User> userInfos = fosUtils.getUserInfosMap(userIds, exposeEmail);
            UserInfo teacherUserInfo = new UserInfo(userInfos.get(coursePlanBasicInfo.getTeacherId()), null, exposeEmail);
            UserInfo studentUserInfo = new UserInfo(userInfos.get(coursePlanBasicInfo.getStudentId()), null, exposeEmail);
            coursePlanBasicInfo.setStudent(studentUserInfo);
            coursePlanBasicInfo.setTeacher(teacherUserInfo);
        }
        return coursePlanBasicInfo;
    }

    public List<OTFBundleInfo> getOTFBundleInfos(GetOTFBundlesReq req) throws VException {
        String queryString = WebUtils.INSTANCE.createQueryStringOfObject(req);
        ClientResponse resp = WebUtils.INSTANCE.doCall(ONETOFEW_ENDPOINT
                + "otfBundle/get?" + queryString, HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        Type listType1 = new TypeToken<ArrayList<OTFBundleInfo>>() {
        }.getType();
        List<OTFBundleInfo> bundleInfos = new Gson().fromJson(jsonString, listType1);
        if (bundleInfos != null && !bundleInfos.isEmpty()) {
            List<String> otfBundleIds = new ArrayList<>();
            for (OTFBundleInfo otfBundleInfo : bundleInfos) {
                otfBundleIds.add(otfBundleInfo.getId());
            }
            List<BaseInstalment> baseInstalments = dineroManagerHelper.getBaseInstalments(InstalmentPurchaseEntity.OTF_BUNDLE, otfBundleIds);
            if (baseInstalments != null && !baseInstalments.isEmpty()) {
                Map<String, BaseInstalment> baseInstalmentMap = new HashMap<>();
                for (BaseInstalment baseInstalment : baseInstalments) {
                    baseInstalmentMap.put(baseInstalment.getPurchaseEntityId(), baseInstalment);
                }
                for (OTFBundleInfo otfBundleInfo : bundleInfos) {
                    BaseInstalment baseInstalment = baseInstalmentMap.get(otfBundleInfo.getId());
                    if (baseInstalment != null) {
                        otfBundleInfo.setInstalmentDetails(baseInstalment.getInfo());
                    }
                }

            }
        }
        return bundleInfos;
    }

    public List<BundlePackageDetailsInfo> getBundlePackageDetailsList(GetBundlesReq req) throws VException {
        String queryString = WebUtils.INSTANCE.createQueryStringOfObject(req);
        ClientResponse resp = WebUtils.INSTANCE.doCall(SUBSCRIPTION_ENDPOINT
                + "/bundlePackage/getBundlePackageDetailsList?" + queryString, HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        Type listType1 = new TypeToken<ArrayList<BundlePackageDetailsInfo>>() {
        }.getType();
        List<BundlePackageDetailsInfo> bundleInfos = new Gson().fromJson(jsonString, listType1);
        return bundleInfos;
    }

    public List<BundleInfo> getBundleInfos(GetBundlesReq req) throws VException {
        String queryString = WebUtils.INSTANCE.createQueryStringOfObject(req);
        ClientResponse resp = WebUtils.INSTANCE.doCall(SUBSCRIPTION_ENDPOINT + "/bundle/get?" + queryString,
                HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        Type listType1 = new TypeToken<ArrayList<BundleInfo>>() {
        }.getType();
        List<BundleInfo> bundleInfos = new Gson().fromJson(jsonString, listType1);
        return bundleInfos;
    }
}
