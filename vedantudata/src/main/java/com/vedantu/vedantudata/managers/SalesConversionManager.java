package com.vedantu.vedantudata.managers;

import com.vedantu.User.User;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.NotFoundException;
import com.vedantu.exception.VException;
import com.vedantu.util.*;
import com.vedantu.util.dbentities.mongo.AbstractMongoEntity;
import com.vedantu.vedantudata.dao.SalesConversionDao;
import com.vedantu.vedantudata.entities.salesconversion.*;
import com.vedantu.vedantudata.enums.salesconversion.*;
import com.vedantu.vedantudata.pojos.AgentAttendanceDataExport;
import com.vedantu.vedantudata.pojos.SalesAgentDataExport;
import com.vedantu.vedantudata.pojos.SalesConversionApiResponseManager;
import com.vedantu.vedantudata.pojos.request.*;
import com.vedantu.vedantudata.pojos.response.*;
import com.vedantu.vedantudata.utils.CommonUtils;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class SalesConversionManager {

    @Autowired
    private SalesConversionDao salesConversionDao;

    @Autowired
    private AwsS3Manager awsS3Manager;

    @Autowired
    private FosUtils fosUtils;

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(SalesConversionManager.class);

    private static final String ENV = ConfigUtils.INSTANCE.getStringValue("environment").toLowerCase();
    private static final String S3_BUCKET = ConfigUtils.INSTANCE.getStringValue("aws.s3.bucket");

    /**
     * Adding sales mandays in system
     *
     * @param fosTeamMandaysReqList
     * @param loggedInUserId
     * @param team
     * @return
     * @throws ParseException
     * @throws VException
     */
    public String addSalesFOSMandays(List<FOSTeamMandaysReq> fosTeamMandaysReqList, long loggedInUserId, SalesTeam team) throws ParseException, VException {

        //Check whether req centre names are present in system
        Set<String> centreListReq = fosTeamMandaysReqList.stream().map(FOSTeamMandaysReq::getCentreName).collect(Collectors.toSet());
        List<CentreInfo> centreInfos = salesConversionDao.getCentreDetailsByName(centreListReq, team);
        Set<String> centreListDB = centreInfos.stream().map(CentreInfo::getCentreName).collect(Collectors.toSet());
        centreListReq.removeAll(centreListDB);

        logger.info("centreListReq: {}", centreListReq);
        if (CollectionUtils.isNotEmpty(centreListReq)) {
            return "Centre name's " + centreListReq.toString() + " is/are not present in system";
        }

        Map<String, String> centreNameToIdMap = centreInfos.stream().collect(Collectors.toMap(CentreInfo::getCentreName, CentreInfo::getId));
        logger.info("centreNameToIdMap: {}", centreNameToIdMap);

        List<FOSMandaysInfo> fosMandaysInfoList = new ArrayList<>();
        for (FOSTeamMandaysReq fosTeamMandaysReq : fosTeamMandaysReqList) {
            FOSMandaysInfo fosMandaysInfo = new FOSMandaysInfo();
            fosMandaysInfo.setCentreId(centreNameToIdMap.get(fosTeamMandaysReq.getCentreName()));
            fosMandaysInfo.setDate(CommonUtils.parseTime(fosTeamMandaysReq.getDate()));
            fosMandaysInfo.setMandays(fosTeamMandaysReq.getMandays());
            fosMandaysInfoList.add(fosMandaysInfo);
        }
        salesConversionDao.addFOSTeamMandays(fosMandaysInfoList, loggedInUserId);
        return "SUCCESS";
    }

    /**
     * Adding centre name in system
     *
     * @param centreDetailsReq
     * @param loggedInUserId
     * @return
     */
    public Boolean addCentreDetails(CentreDetailsReq centreDetailsReq, long loggedInUserId) {
        CentreInfo centreInfo = null;
        if (StringUtils.isNotEmpty(centreDetailsReq.getCentreId())) {
            centreInfo = salesConversionDao.getCentreInfoById(centreDetailsReq.getCentreId());
            addCentreInfoToHistory(centreInfo, loggedInUserId);
        } else {
            centreInfo = new CentreInfo();
        }
        centreInfo.setCentreName(centreDetailsReq.getCentreName());
        centreInfo.setZhId(centreDetailsReq.getZhId());
        centreInfo.setRmId(centreDetailsReq.getRmId());
        centreInfo.setCmId(centreDetailsReq.getCmId());
        centreInfo.setCentreHeadId(centreDetailsReq.getCentreHeadId());
        centreInfo.setTeam(SalesTeam.FOS);
        return salesConversionDao.addCentreDetails(centreInfo, loggedInUserId);
    }

    /**
     * adds data to history on every update
     *
     * @param centreInfo
     * @param loggedInUserId
     * @return
     */
    private Boolean addCentreInfoToHistory(CentreInfo centreInfo, long loggedInUserId) {
        CentreInfoHistory centreInfoHistory = new CentreInfoHistory();
        centreInfoHistory.setCentreName(centreInfo.getCentreName());
        centreInfoHistory.setZhId(centreInfo.getZhId());
        centreInfoHistory.setRmId(centreInfo.getRmId());
        centreInfoHistory.setCmId(centreInfo.getCmId());
        centreInfoHistory.setCentreHeadId(centreInfo.getCentreHeadId());
        centreInfoHistory.setTeam(SalesTeam.FOS);
        return salesConversionDao.addCentreInfoToHistory(centreInfoHistory, loggedInUserId);
    }

    /**
     * Adding target input details in system
     *
     * @param addTargetInputReq
     * @return
     */
    public PlatformBasicResponse addTargetInput(AddTargetInputReq addTargetInputReq) throws NotFoundException, BadRequestException, ParseException {

        Set<String> centreListReq = addTargetInputReq.getData().stream().map(TargetInputItems::getLocation).collect(Collectors.toSet());
        List<CentreInfo> centreInfos = salesConversionDao.getCentreDetailsByName(centreListReq, null);
        Map<String, String> centreNameToIdMap = centreInfos.stream().collect(Collectors.toMap(CentreInfo::getCentreName, CentreInfo::getId));
        logger.info("centreNameToIdMap: {}", centreNameToIdMap);
        Set<String> centreListDB = centreNameToIdMap.keySet();
        centreListReq.removeAll(centreListDB);

        logger.info("centreListReq: {}", centreListReq);
        if (CollectionUtils.isNotEmpty(centreListReq)) {
            throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, "Centre name's " + centreListReq.toString() + " is/are not present in system");
        }


        List<TargetInput> targetInputs = new ArrayList<>();
        for (TargetInputItems targetInputReq : addTargetInputReq.getData()) {
            TargetInput targetInput = new TargetInput();
            targetInput.setRm(targetInputReq.getRm());
            targetInput.setRmEmployeeCode(targetInputReq.getRmEmployeeCode());
            targetInput.setDate(CommonUtils.parseTime(targetInputReq.getDate()));
            targetInput.setCentreId(centreNameToIdMap.get(targetInputReq.getLocation()));
            targetInput.setSfCommited(targetInputReq.getSfCommited());
            targetInputs.add(targetInput);
        }
        salesConversionDao.addTargetInput(targetInputs);
        return new PlatformBasicResponse();
    }

    /**
     * update centre name from old to new
     *
     * @param centreDetailsReq
     * @param loggedInUserId
     * @return
     */
    public String updateCentreName(CentreUpdateReq centreDetailsReq, long loggedInUserId) {
        List<CentreInfo> centreInfoList = salesConversionDao.getCentreDetailsByName(new HashSet<>(Arrays.asList(centreDetailsReq.getOldCentreName())), centreDetailsReq.getTeam());
        if (CollectionUtils.isEmpty(centreInfoList)) {
            return "Centre name " + centreDetailsReq.getOldCentreName() + " is not present in system";
        }
        CentreInfo centreInfo = centreInfoList.get(0);
        centreInfo.setCentreName(centreDetailsReq.getNewCentreName());
        salesConversionDao.updateCentreName(centreInfo, loggedInUserId);
        return "SUCCESS";
    }

    /**
     * Adds mandays for sales IS team
     *
     * @param isTeamMandaysReqList
     * @param loggedInUserId
     * @param team
     * @return
     * @throws ParseException
     * @throws VException
     */
    public String addSalesISMandays(List<ISTeamMandaysReq> isTeamMandaysReqList, long loggedInUserId, SalesTeam team) throws ParseException, VException {

        List<ISMandaysInfo> isMandaysInfos = new ArrayList<>();
        for (ISTeamMandaysReq isTeamMandaysReq : isTeamMandaysReqList) {
            ISMandaysInfo isMandaysInfo = new ISMandaysInfo();
            isMandaysInfo.setRmEmployeeId(isTeamMandaysReq.getRmEmployeeCode());
            isMandaysInfo.setRmName(isTeamMandaysReq.getRmName());
            isMandaysInfo.setDate(CommonUtils.parseTime(isTeamMandaysReq.getDate()));
            isMandaysInfo.setMandays(isTeamMandaysReq.getMandays());
            isMandaysInfos.add(isMandaysInfo);
        }
        salesConversionDao.addISTeamMandays(isMandaysInfos, loggedInUserId);
        return "SUCCESS";
    }

    /**
     * Gets agent metadata list on the basis of filter
     *
     * @param salesAgentMetadataReq
     * @return
     */
    public SalesAgentMetadataRes getSalesAgentMetadata(SalesAgentMetadataReq salesAgentMetadataReq) {

        //Projection on fields to fetch
        Set<String> requiredFields = new HashSet<>();
        requiredFields.add(SalesAgentInfo.Constants.NAME);
        requiredFields.add(SalesAgentInfo.Constants.EMAIL);
        requiredFields.add(SalesAgentInfo.Constants.EMPLOYEE_ID);
        requiredFields.add(SalesAgentInfo.Constants.DESIGNATION);
        requiredFields.add(SalesAgentInfo.Constants.DESIGNATION_TYPE);
        requiredFields.add(SalesAgentInfo.Constants.CENTRE_ID);
        requiredFields.add(SalesAgentInfo.Constants.TL_ID);
        requiredFields.add(SalesAgentInfo.Constants.DATE_OF_JOINING);
        requiredFields.add(SalesAgentInfo.Constants.STATUS);
        requiredFields.add(SalesAgentInfo.Constants.IS_FM_ID);
        requiredFields.add(SalesAgentInfo.Constants.IS_RM_ID);
        requiredFields.add(SalesAgentInfo.Constants.LS_ID);
        requiredFields.add(SalesAgentInfo.Constants.DATE_OF_LEAVING);
        addAbstractMongoEntityFields(requiredFields);
        SalesAgentMetadataRes salesAgentMetadataRes = salesConversionDao.getSalesAgentMetadata(salesAgentMetadataReq, requiredFields, true);
        List<SalesAgentInfo> salesAgentInfos = salesAgentMetadataRes.getSalesAgentInfoList();
        if (CollectionUtils.isNotEmpty(salesAgentInfos)) {
            salesAgentMetadataRes.setTlInfos(getTlInfoFromAgent(salesAgentInfos, requiredFields));
            salesAgentMetadataRes.setCentreInfos(getCentreInfoFromAgent(salesAgentInfos));
        }
        logger.info("getSalesAgentMetadata response: {}", salesAgentMetadataRes);
        return salesAgentMetadataRes;

    }

    /**
     * Add/Edit agent data
     *
     * @param salesAgentDataReq
     * @param callingUserId
     * @return
     * @throws VException
     */
    public boolean addSalesAgentData(SalesAgentDataReq salesAgentDataReq, long callingUserId, boolean isBulk) throws VException {

        SalesAgentInfo salesAgentInfo = null;
        if (StringUtils.isNotEmpty(salesAgentDataReq.getAgentId())) {
            salesAgentInfo = salesConversionDao.getSalesAgentInfoById(salesAgentDataReq.getAgentId());
            if (salesAgentInfo == null) {
                throw new VException(ErrorCode.BAD_REQUEST_ERROR, "Agent id not present in system");
            }
            addSalesAgentDataToHistory(salesAgentInfo, callingUserId);
        } else {
            salesAgentInfo = new SalesAgentInfo();
            salesAgentInfo.setTeam(salesAgentDataReq.getTeam());
        }
        salesAgentInfo.setName(salesAgentDataReq.getName());
        salesAgentInfo.setEmail(salesAgentDataReq.getEmail());
        salesAgentInfo.setEmployeeId(salesAgentDataReq.getEmployeeId());
        salesAgentInfo.setDateOfJoining(salesAgentDataReq.getDateOfJoining());
        if (salesAgentDataReq.getDateOfLeaving() != null) {
            salesAgentInfo.setDateOfLeaving(salesAgentDataReq.getDateOfLeaving());
        }
        salesAgentInfo.setDesignation(salesAgentDataReq.getDesignation());
        salesAgentInfo.setDesignationType(salesAgentDataReq.getDesignationType());
        salesAgentInfo.setStatus(salesAgentDataReq.getStatus());
        salesAgentInfo.setLeadsquaredId(salesAgentDataReq.getLeadsquaredId());

        if (SalesTeam.FOS.equals(salesAgentDataReq.getTeam())) {
            if (!AgentDesignation.ZONAL_HEAD.equals(salesAgentDataReq.getDesignation())
                    && !AgentDesignation.REGIONAL_MANAGER.equals(salesAgentDataReq.getDesignation())
                    && !AgentDesignation.CLUSTER_MANAGER.equals(salesAgentDataReq.getDesignation())
                    && !AgentDesignation.CENTER_HEAD.equals(salesAgentDataReq.getDesignation())) {
                salesAgentInfo.setCentreId(salesAgentDataReq.getCentreId());
            }
            if (isBulk) {
                salesAgentInfo.setCentreId(salesAgentDataReq.getCentreId());
            }
        }
        if (SalesTeam.IS.equals(salesAgentDataReq.getTeam())) {
            salesAgentInfo.setIsFMId(salesAgentDataReq.getIsFMId());
            salesAgentInfo.setIsRMId(salesAgentDataReq.getIsRMId());
        }
        salesAgentInfo.setTlId(salesAgentDataReq.getTlId());
        logger.info("salesAgentInfo: {}", salesAgentInfo);
        return salesConversionDao.addSalesAgentData(salesAgentInfo, callingUserId);
    }

    /**
     * Adds agent data to history on every edit
     *
     * @param salesAgentInfo
     * @param callingUserId
     * @return
     */
    private Boolean addSalesAgentDataToHistory(SalesAgentInfo salesAgentInfo, long callingUserId) {
        SalesAgentInfoHistory salesAgentInfoHistory = new SalesAgentInfoHistory();
        salesAgentInfoHistory.setName(salesAgentInfo.getName());
        salesAgentInfoHistory.setEmail(salesAgentInfo.getEmail());
        salesAgentInfoHistory.setEmployeeId(salesAgentInfo.getEmployeeId());
        if (SalesTeam.FOS.equals(salesAgentInfo.getTeam())) {
            if (!AgentDesignation.ZONAL_HEAD.equals(salesAgentInfo.getDesignation()) && !AgentDesignation.REGIONAL_MANAGER.equals(salesAgentInfo.getDesignation())
                    && !AgentDesignation.CLUSTER_MANAGER.equals(salesAgentInfo.getDesignation())) {
                salesAgentInfoHistory.setCentreId(salesAgentInfo.getCentreId());
                salesAgentInfoHistory.setTlId(salesAgentInfo.getTlId());
            }
        }
        if (SalesTeam.IS.equals(salesAgentInfo.getTeam())) {
            salesAgentInfoHistory.setIsFMId(salesAgentInfo.getIsFMId());
            salesAgentInfoHistory.setIsRMId(salesAgentInfo.getIsRMId());
            salesAgentInfoHistory.setTlId(salesAgentInfo.getTlId());
        }
        salesAgentInfoHistory.setDateOfJoining(salesAgentInfo.getDateOfJoining());
        salesAgentInfoHistory.setDateOfLeaving(salesAgentInfo.getDateOfLeaving());
        salesAgentInfoHistory.setDesignation(salesAgentInfo.getDesignation());
        salesAgentInfoHistory.setDesignationType(salesAgentInfo.getDesignationType());
        salesAgentInfoHistory.setStatus(salesAgentInfo.getStatus());
        salesAgentInfoHistory.setLeadsquaredId(salesAgentInfo.getLeadsquaredId());
        salesAgentInfoHistory.setTeam(salesAgentInfo.getTeam());
        return salesConversionDao.addSalesDataToHistory(salesAgentInfoHistory, callingUserId);
    }

    /**
     * Get sales data by id
     *
     * @param salesAgentId
     * @param team
     * @return
     * @throws VException
     */
    public SalesAgentDataRes getSalesAgentDataById(String salesAgentId, SalesTeam team) throws VException {

        SalesAgentDataRes salesAgentDataRes = new SalesAgentDataRes();
        Set<String> agentList = new HashSet<>();
        SalesAgentInfo salesAgentInfo = salesConversionDao.getSalesAgentInfoById(salesAgentId);
        if (salesAgentInfo == null) {
            throw new VException(ErrorCode.BAD_REQUEST_ERROR, "Agent not present: " + salesAgentId);
        }
        if (SalesTeam.FOS.equals(team)) {
            if (!AgentDesignation.ZONAL_HEAD.equals(salesAgentInfo.getDesignation()) && !AgentDesignation.CLUSTER_MANAGER.equals(salesAgentInfo.getDesignation())
                    && !AgentDesignation.REGIONAL_MANAGER.equals(salesAgentInfo.getDesignation()) && !AgentDesignation.CENTER_HEAD.equals(salesAgentInfo.getDesignation())) {
                CentreInfo centreInfo = salesConversionDao.getCentreInfoById(salesAgentInfo.getCentreId());
                if (centreInfo == null) {
                    throw new VException(ErrorCode.BAD_REQUEST_ERROR, "Centre not present for : " + salesAgentInfo.getId());
                }
                salesAgentDataRes.setCentreInfo(centreInfo);
                if (StringUtils.isNotEmpty(centreInfo.getZhId())) {
                    agentList.add(centreInfo.getZhId());
                }
                if (StringUtils.isNotEmpty(centreInfo.getRmId())) {
                    agentList.add(centreInfo.getRmId());
                }
                if (StringUtils.isNotEmpty(centreInfo.getCmId())) {
                    agentList.add(centreInfo.getCmId());
                }
                if (StringUtils.isNotEmpty(centreInfo.getCentreHeadId())) {
                    agentList.add(centreInfo.getCentreHeadId());
                }
                if (StringUtils.isNotEmpty(salesAgentInfo.getTlId())) {
                    agentList.add(salesAgentInfo.getTlId());
                }

                List<SalesAgentInfo> agentDetails = getAgentDataByIds(agentList);
                for (SalesAgentInfo salesAgent : agentDetails) {
                    if (AgentDesignation.TEAM_LEADER.equals(salesAgent.getDesignation())) {
                        salesAgentDataRes.setTlInfo(salesAgent);
                    }
                    if (AgentDesignation.SENIOR_TEAM_LEADER.equals(salesAgent.getDesignation())) {
                        salesAgentDataRes.setTlInfo(salesAgent);
                    }
                    if (AgentDesignation.CENTER_HEAD.equals(salesAgent.getDesignation())) {
                        salesAgentDataRes.setChInfo(salesAgent);
                    }
                    if (AgentDesignation.CLUSTER_MANAGER.equals(salesAgent.getDesignation())) {
                        salesAgentDataRes.setCmInfo(salesAgent);
                    }
                    if (AgentDesignation.REGIONAL_MANAGER.equals(salesAgent.getDesignation())) {
                        salesAgentDataRes.setRmInfo(salesAgent);
                    }
                    if (AgentDesignation.ZONAL_HEAD.equals(salesAgent.getDesignation())) {
                        salesAgentDataRes.setZhInfo(salesAgent);
                    }
                }
            }
        } else {
            if (StringUtils.isNotEmpty(salesAgentInfo.getIsFMId())) {
                agentList.add(salesAgentInfo.getIsFMId());
            }
            if (StringUtils.isNotEmpty(salesAgentInfo.getIsRMId())) {
                agentList.add(salesAgentInfo.getIsRMId());
            }
            if (StringUtils.isNotEmpty(salesAgentInfo.getTlId())) {
                agentList.add(salesAgentInfo.getTlId());
            }
            List<SalesAgentInfo> agentDetails = getAgentDataByIds(agentList);
            for (SalesAgentInfo salesAgent : agentDetails) {
                if (AgentDesignation.TEAM_LEADER.equals(salesAgent.getDesignation())) {
                    salesAgentDataRes.setTlInfo(salesAgent);
                }
                if (AgentDesignation.SENIOR_TEAM_LEADER.equals(salesAgent.getDesignation())) {
                    salesAgentDataRes.setTlInfo(salesAgent);
                }
                if (AgentDesignation.REVENUE_MANAGER.equals(salesAgent.getDesignation())) {
                    salesAgentDataRes.setRmInfo(salesAgent);
                }
                if (AgentDesignation.FLOOR_MANAGER.equals(salesAgent.getDesignation())) {
                    salesAgentDataRes.setFmInfo(salesAgent);
                }
            }
            salesAgentInfo.setIsFMId(null);
            salesAgentInfo.setIsRMId(null);
            salesAgentInfo.setTlId(null);
        }
        salesAgentDataRes.setAgentInfo(salesAgentInfo);
        return salesAgentDataRes;
    }

    /**
     * Get agent data by ids
     *
     * @param agentList
     * @return
     */
    private List<SalesAgentInfo> getAgentDataByIds(Set<String> agentList) {
        Set<String> requiredFields = new HashSet<>();
        addSalesAgentRequiredFields(requiredFields);
        addAbstractMongoEntityFields(requiredFields);
        return salesConversionDao.getSalesAgentInfoByIds(agentList, requiredFields, true);
    }

    /**
     * Agent filter on the basis of designation and team
     *
     * @param salesAgentFilterValueReq
     * @return
     */
    public List<SalesAgentInfo> getSalesAgentFilterValue(SalesAgentFilterValueReq salesAgentFilterValueReq) {
        Set<String> requiredFields = new HashSet<>();
        addSalesAgentRequiredFields(requiredFields);
        addAbstractMongoEntityFields(requiredFields);
        return salesConversionDao.getSalesAgentFilterValue(salesAgentFilterValueReq, requiredFields, true);
    }

    /**
     * Centre filter on the basis of FH, RM, CM
     *
     * @param fosCentreFilterReq
     * @return
     */
    public FOSCentreFilterRes getFOSCentreFilter(FOSCentreFilterReq fosCentreFilterReq) {

        FOSCentreFilterRes fosCentreFilterRes = new FOSCentreFilterRes();
        Set<String> requiredFields = new HashSet<>();
        requiredFields.add(CentreInfo.Constants.CENTRE_NAME);
        requiredFields.add(CentreInfo.Constants.CENTRE_HEAD_ID);
        addAbstractMongoEntityFields(requiredFields);
        List<CentreInfo> centreInfos = salesConversionDao.getFOSCentreFilter(fosCentreFilterReq, requiredFields, true);
        if (CollectionUtils.isNotEmpty(centreInfos)) {
            fosCentreFilterRes.setCentreInfos(centreInfos);
            Set<String> centreHeadIds = centreInfos.stream().map(p -> p.getCentreHeadId()).filter(Objects::nonNull).collect(Collectors.toSet());
            Set<String> requiredAgentFields = new HashSet<>();
            addSalesAgentRequiredFields(requiredAgentFields);
            addAbstractMongoEntityFields(requiredAgentFields);
            List<SalesAgentInfo> salesAgentInfos = salesConversionDao.getSalesAgentInfoByIds(centreHeadIds, requiredAgentFields, true);
            if (CollectionUtils.isNotEmpty(salesAgentInfos)) {
                fosCentreFilterRes.setSalesAgentInfos(salesAgentInfos);
            }
        }
        return fosCentreFilterRes;
    }

    /**
     * Filter for Revenue Manager on the basis of Floor Manager
     *
     * @param isFMId
     * @return
     */
    public List<SalesAgentInfo> getISFilterByDesignation(String isFMId) {
        Set<String> requiredFields = new HashSet<>();
        addSalesAgentRequiredFields(requiredFields);
        addAbstractMongoEntityFields(requiredFields);
        return salesConversionDao.getISFilterByDesignation(isFMId, requiredFields, true);
    }

    /**
     * Gets all Centreinfos
     *
     * @return
     */
    public FOSCentreFilterRes getCentreInfos(Integer start, Integer size) {
        FOSCentreFilterRes fosCentreFilterRes = salesConversionDao.getCentreInfos(start, size);
        List<CentreInfo> centreInfos = fosCentreFilterRes.getCentreInfos();
        if (CollectionUtils.isNotEmpty(centreInfos)) {
            fosCentreFilterRes.setCentreInfos(centreInfos);
            Set<String> agentIds = new HashSet<>();
            agentIds.addAll(centreInfos.stream().map(p -> p.getZhId()).filter(Objects::nonNull).collect(Collectors.toSet()));
            agentIds.addAll(centreInfos.stream().map(p -> p.getRmId()).filter(Objects::nonNull).collect(Collectors.toSet()));
            agentIds.addAll(centreInfos.stream().map(p -> p.getCmId()).filter(Objects::nonNull).collect(Collectors.toSet()));
            agentIds.addAll(centreInfos.stream().map(p -> p.getCentreHeadId()).filter(Objects::nonNull).collect(Collectors.toSet()));
            Set<String> requiredAgentFields = new HashSet<>();
            addSalesAgentRequiredFields(requiredAgentFields);
            addAbstractMongoEntityFields(requiredAgentFields);
            List<SalesAgentInfo> salesAgentInfos = salesConversionDao.getSalesAgentInfoByIds(agentIds, requiredAgentFields, true);
            if (CollectionUtils.isNotEmpty(salesAgentInfos)) {
                fosCentreFilterRes.setSalesAgentInfos(salesAgentInfos);
            }
        }
        return fosCentreFilterRes;

    }

    /**
     * Filter for Team lead
     *
     * @param centreId
     * @return
     */
    public List<SalesAgentInfo> getFOSTeamLeadFilter(String centreId) {
        Set<String> requiredFields = new HashSet<>();
        addSalesAgentRequiredFields(requiredFields);
        addAbstractMongoEntityFields(requiredFields);
        return salesConversionDao.getTeamLeadByCentre(centreId, requiredFields, true);
    }

    /**
     * Generate csv and upload to s3
     *
     * @param salesAgentMetadataReq
     * @return
     * @throws IOException
     */
    public String generateSaleAgentCsv(SalesAgentMetadataReq salesAgentMetadataReq) throws IOException {

        try {
            Path page = Files.createTempFile("page", ".csv");
            Map<String, String> headerMap = getSalesAgentCsvHeaderMap(salesAgentMetadataReq.getTeam());
            List<String> header = new ArrayList<>(headerMap.values());
            Files.write(page, String.join(",", header).concat(System.lineSeparator()).getBytes(),
                    StandardOpenOption.CREATE, StandardOpenOption.APPEND);

            Integer size = 500;
            Integer start = 0;
            salesAgentMetadataReq.setSize(size);
            List<SalesAgentDataExport> salesAgentDataExportList;
            SalesAgentMetadataRes salesAgentMetadataRes;
            do {
                salesAgentMetadataReq.setStart(start);
                //Getting data on the basis of filter
                salesAgentMetadataRes = salesConversionDao.getSalesAgentMetadata(salesAgentMetadataReq, null, true);
                if (CollectionUtils.isEmpty(salesAgentMetadataRes.getSalesAgentInfoList())) {
                    logger.info("Data not present for requested filters");
                    return "Data not present for requested filters";
                }
                salesAgentDataExportList = getGenerateCsvData(salesAgentMetadataRes.getSalesAgentInfoList(), salesAgentMetadataReq.getTeam());
                start += size;
                Files.write(page, getSalesAgentCsvBytes(salesAgentDataExportList, headerMap, salesAgentMetadataReq.getTeam()), StandardOpenOption.CREATE, StandardOpenOption.APPEND);
            } while (salesAgentMetadataRes.getSalesAgentInfoList().size() == 500);

            String key = ENV + "/temp/sales/agent/csv/" + LocalDate.now().format(DateTimeFormatter.BASIC_ISO_DATE) + "/" +
                    UUID.randomUUID().toString() + ".csv";
            awsS3Manager.uploadFile(S3_BUCKET, key, page.toFile());
            logger.info(page.toAbsolutePath());
            page.toFile().delete();
            return awsS3Manager.getPublicBucketDownloadUrl(S3_BUCKET, key);
        } catch (Exception e) {
            logger.error("Error ocurred while generating sales agent csv: {}", e);
            return "ERROR";
        }
    }

    /**
     * Generate csv on the basis of filter selected
     *
     * @param salesAgentInfos
     * @param team
     * @return
     */
    private List<SalesAgentDataExport> getGenerateCsvData(List<SalesAgentInfo> salesAgentInfos, SalesTeam team) throws NotFoundException {
        List<SalesAgentDataExport> salesAgentDataExportList = new ArrayList<>();
        Set<String> agentIds = new HashSet<>();
        List<User> updatedByIds = new ArrayList<>();
        Map<String, String> agentIdToNameMap = new HashMap<>();
        Map<String, String> agentIdToEmployeeIdMap = new HashMap<>();
        Map<String, CentreInfo> centreIdToObject = new HashMap<>();
        Map<Long, String> userIdToNameMap = new HashMap<>();
        SimpleDateFormat sdf = new SimpleDateFormat("MM-dd-yyyy");
        sdf.setTimeZone(TimeZone.getTimeZone(DateTimeUtils.TIME_ZONE_IN));
        logger.info("************sales Agent Infos" + salesAgentInfos.size());
        if (CollectionUtils.isNotEmpty(salesAgentInfos)) {
            //getting all the data for updatedBy from user
            updatedByIds = fosUtils.getUserInfos(getUpdatedByIds(salesAgentInfos), false);
            if (CollectionUtils.isNotEmpty(updatedByIds)) {
                userIdToNameMap = updatedByIds.parallelStream().filter(Objects::nonNull).filter(p -> p.getFullName() != null).collect(Collectors.toMap(User::getId, User::getFullName));
            }
            agentIds.addAll(salesAgentInfos.parallelStream().filter(Objects::nonNull).map(p -> p.getTlId()).filter(Objects::nonNull).collect(Collectors.toSet()));
            if (SalesTeam.FOS.equals(team)) {
                //getting centre data..
                Set<String> centreIds = salesAgentInfos.stream().map(p -> p.getCentreId()).filter(Objects::nonNull).collect(Collectors.toSet());
                List<CentreInfo> centreInfos = salesConversionDao.getCentreInfoByIds(centreIds, null, true);
                if (CollectionUtils.isNotEmpty(centreInfos)) {
                    //getting all the agents related to centre
                    centreIdToObject = getCentreIdToObjectMap(centreInfos);
                    agentIds.addAll(centreInfos.parallelStream().map(p -> p.getZhId()).filter(Objects::nonNull).collect(Collectors.toSet()));
                    agentIds.addAll(centreInfos.parallelStream().map(p -> p.getRmId()).filter(Objects::nonNull).collect(Collectors.toSet()));
                    agentIds.addAll(centreInfos.parallelStream().map(p -> p.getCmId()).filter(Objects::nonNull).collect(Collectors.toSet()));
                    agentIds.addAll(centreInfos.parallelStream().map(p -> p.getCentreHeadId()).filter(Objects::nonNull).collect(Collectors.toSet()));
                }
            } else if (SalesTeam.IS.equals(team)) {
                agentIds.addAll(salesAgentInfos.parallelStream().map(p -> p.getIsFMId()).filter(Objects::nonNull).collect(Collectors.toSet()));
                agentIds.addAll(salesAgentInfos.parallelStream().map(p -> p.getIsRMId()).filter(Objects::nonNull).collect(Collectors.toSet()));
            }
            //Maintaing map of all the agents related to a particluar agent
            if (CollectionUtils.isNotEmpty(agentIds)) {
                Set<String> requiredAgentFields = new HashSet<>();
                addSalesAgentRequiredFields(requiredAgentFields);
                addAbstractMongoEntityFields(requiredAgentFields);
                List<SalesAgentInfo> agentInfo = salesConversionDao.getSalesAgentInfoByIds(agentIds, requiredAgentFields, true);

                if (CollectionUtils.isNotEmpty(agentInfo)) {
                    agentIdToNameMap = agentInfo.stream().filter(Objects::nonNull).filter(p -> p.getName() != null).collect(Collectors.toMap(SalesAgentInfo::getId, SalesAgentInfo::getName));
                    agentIdToEmployeeIdMap = agentInfo.stream().filter(Objects::nonNull).filter(p -> p.getEmployeeId() != null).collect(Collectors.toMap(SalesAgentInfo::getId, SalesAgentInfo::getEmployeeId));
                }
            }
            //building data for csv
            for (SalesAgentInfo sales : salesAgentInfos) {
                SalesAgentDataExport salesAgentDataExport = new SalesAgentDataExport();
                salesAgentDataExport.setId(sales.getId());
                salesAgentDataExport.setName(sales.getName());
                salesAgentDataExport.setEmail(sales.getEmail());
                salesAgentDataExport.setEmployeeId(sales.getEmployeeId());
                if (sales.getDateOfJoining() != null) {
                    sdf.setTimeZone(TimeZone.getTimeZone(DateTimeUtils.TIME_ZONE_IN));
                    String DOJ = sdf.format(new Date(sales.getDateOfJoining()));
                    salesAgentDataExport.setDateOfJoining(org.apache.commons.lang.StringUtils.defaultString(DOJ, " "));
                    salesAgentDataExport.setCohortDOJ(org.apache.commons.lang.StringUtils.defaultString(calculateCohortDate(sales.getDateOfJoining()), " "));
                } else {
                    salesAgentDataExport.setDateOfJoining(" ");
                    salesAgentDataExport.setCohortDOJ(" ");
                }
                salesAgentDataExport.setDesignation(sales.getDesignation());
                salesAgentDataExport.setDesignationType(sales.getDesignationType());

                if (SalesTeam.FOS.equals(team)) {
                    if (centreIdToObject.containsKey(sales.getCentreId())) {
                        CentreInfo centreInfo = centreIdToObject.get(sales.getCentreId());
                        salesAgentDataExport.setCentre(org.apache.commons.lang.StringUtils.defaultString(centreInfo.getCentreName(), " "));

                        if (null != centreInfo.getZhId()) {
                            salesAgentDataExport.setZonalHead(org.apache.commons.lang.StringUtils.defaultString(agentIdToNameMap.get(centreInfo.getZhId()), " "));
                        } else {
                            salesAgentDataExport.setZonalHead(" ");
                        }

                        if (null != centreInfo.getRmId()) {
                            salesAgentDataExport.setRegionalManager(org.apache.commons.lang.StringUtils.defaultString(agentIdToNameMap.get(centreInfo.getRmId()), " "));
                        } else {
                            salesAgentDataExport.setRegionalManager(" ");
                        }

                        if (null != centreInfo.getCmId()) {
                            salesAgentDataExport.setClusterManager(org.apache.commons.lang.StringUtils.defaultString(agentIdToNameMap.get(centreInfo.getCmId()), " "));
                        } else {
                            salesAgentDataExport.setClusterManager(" ");
                        }

                        if (null != centreInfo.getCentreHeadId()) {
                            salesAgentDataExport.setCentreHead(org.apache.commons.lang.StringUtils.defaultString(agentIdToNameMap.get(centreInfo.getCentreHeadId()), " "));
                        } else {
                            salesAgentDataExport.setCentreHead(" ");
                        }
                    } else {
                        salesAgentDataExport.setCentre(" ");
                        salesAgentDataExport.setZonalHead(" ");
                        salesAgentDataExport.setRegionalManager(" ");
                        salesAgentDataExport.setClusterManager(" ");
                        salesAgentDataExport.setCentreHead(" ");
                    }
                } else if (SalesTeam.IS.equals(team)) {
                    salesAgentDataExport.setIsFMId(org.apache.commons.lang.StringUtils.defaultString(agentIdToNameMap.get(sales.getIsFMId()), " "));
                    salesAgentDataExport.setIsRMId(org.apache.commons.lang.StringUtils.defaultString(agentIdToNameMap.get(sales.getIsRMId()), " "));
                }
                salesAgentDataExport.setTlId(org.apache.commons.lang.StringUtils.defaultString(agentIdToNameMap.get(sales.getTlId()), " "));
                salesAgentDataExport.setTlEmpId(org.apache.commons.lang.StringUtils.defaultString(agentIdToEmployeeIdMap.get(sales.getTlId()), " "));
                salesAgentDataExport.setStatus(sales.getStatus());
                if (sales.getDateOfLeaving() != null) {
                    String DOL = sdf.format(new Date(sales.getDateOfLeaving()));
                    salesAgentDataExport.setDateOfLeaving(org.apache.commons.lang.StringUtils.defaultString(DOL, " "));
                } else {
                    salesAgentDataExport.setDateOfLeaving(" ");
                }
                salesAgentDataExport.setLeadsquaredId(sales.getLeadsquaredId());
                if (StringUtils.isNotEmpty(sales.getLastUpdatedBy())) {
                    try {
                        Long updatedBy = Long.parseLong(sales.getLastUpdatedBy());
                        if (userIdToNameMap.containsKey(updatedBy)) {
                            salesAgentDataExport.setUpdatedBy(userIdToNameMap.get(updatedBy));
                        } else {
                            salesAgentDataExport.setUpdatedBy(" ");
                        }
                    } catch (Exception e) {
                        if ("SYSTEM".equals(sales.getLastUpdatedBy())) {
                            salesAgentDataExport.setUpdatedBy("SYSTEM");
                        } else {
                            salesAgentDataExport.setUpdatedBy(" ");
                        }
                    }
                } else {
                    salesAgentDataExport.setUpdatedBy(" ");
                }
                if (sales.getLastUpdated() != null) {
                    String lastUpdated = sdf.format(new Date(sales.getLastUpdated()));
                    salesAgentDataExport.setUpdatedAt(lastUpdated);
                } else {
                    salesAgentDataExport.setUpdatedAt(" ");
                }
                salesAgentDataExportList.add(salesAgentDataExport);
            }
        }
        return salesAgentDataExportList;
    }

    private Set<Long> getUpdatedByIds(List<SalesAgentInfo> salesAgentInfos) {
        Set<Long> updatedByIds = new HashSet<>();
        Long updatedBy = null;
        for (SalesAgentInfo salesAgentInfo : salesAgentInfos) {
            try {
                updatedBy = Long.parseLong(salesAgentInfo.getLastUpdatedBy());
                updatedByIds.add(updatedBy);
            } catch (Exception e) {
            }
        }
        logger.info("updated ids {}", updatedByIds);
        return updatedByIds;
    }

    /**
     * calculates cohort date for agents
     *
     * @param dateOfJoining
     * @return
     */
    private String calculateCohortDate(Long dateOfJoining) {
        try {
            Calendar doj = new GregorianCalendar();
            doj.setTimeInMillis(dateOfJoining + DateTimeUtils.IST_TIME_DIFFERENCE_NEGATIVE);
            Calendar today = new GregorianCalendar();

            today.setTimeInMillis(new Date().getTime() + DateTimeUtils.IST_TIME_DIFFERENCE_NEGATIVE);
            int yearsInBetween = today.get(Calendar.YEAR) - doj.get(Calendar.YEAR);
            int monthsDiff = today.get(Calendar.MONTH) - doj.get(Calendar.MONTH);
            int days = today.get(Calendar.DAY_OF_YEAR) - today.get(Calendar.DAY_OF_MONTH) - (doj.get(Calendar.DAY_OF_YEAR)) + 1;
            if (days < 0){
                return "M0";
            }
            if (days < 30) {
                return  "M1";
            }
            if (days >=30 && days < 60){
                return  "M2";
            }
            if (days >=60 && days < 90){
                return  "M3";
            }
            if (days >=90 && days < 120){
                return  "M4";
            }
            if (days >=120 && days < 150){
                return  "M5";
            }
            else{
                return "M5+";
            }
        } catch (Exception e) {
            return null;
        }
    }

    private Map<String, CentreInfo> getCentreIdToObjectMap(List<CentreInfo> centreInfos) {
        Map<String, CentreInfo> map = new HashMap<>();
        for (CentreInfo centreInfo : centreInfos) {
            map.put(centreInfo.getId(), centreInfo);
        }
        return map;
    }

    /**
     * generating data in bytes..on the basis of header
     *
     * @param salesAgentDataExportList
     * @param headerMap
     * @param team
     * @return
     */
    private byte[] getSalesAgentCsvBytes(List<SalesAgentDataExport> salesAgentDataExportList, Map<String, String> headerMap, SalesTeam team) {
        List<Map<String, String>> list = new ArrayList<>();

        for (SalesAgentDataExport salesAgentDataExport : salesAgentDataExportList) {
            Map<String, String> map = new LinkedHashMap<>();
            for (Map.Entry<String, String> entry : headerMap.entrySet()) {
                String key = entry.getKey();
                if ("id".equals(key)) {
                    map.put(key, salesAgentDataExport.getId());
                }
                if ("name".equals(key)) {
                    map.put(key, salesAgentDataExport.getName());
                }
                if ("email".equals(key)) {
                    map.put(key, salesAgentDataExport.getEmail());
                }
                if ("employeeId".equals(key)) {
                    map.put(key, salesAgentDataExport.getEmployeeId());
                }
                if ("dateOfJoining".equals(key)) {
                    map.put(key, salesAgentDataExport.getDateOfJoining());
                }
                if ("cohort".equals(key)) {
                    map.put(key, salesAgentDataExport.getCohortDOJ());
                }
                if ("designation".equals(key)) {
                    map.put(key, String.valueOf(salesAgentDataExport.getDesignation()));
                }
                if ("designationType".equals(key)) {
                    map.put(key, String.valueOf(salesAgentDataExport.getDesignationType()));
                }
                if (SalesTeam.FOS.equals(team)) {
                    if ("centreId".equals(key)) {
                        map.put("centreId", salesAgentDataExport.getCentre());
                    }
                    if ("zhId".equals(key)) {
                        map.put("zhId", salesAgentDataExport.getZonalHead());
                    }
                    if ("rmId".equals(key)) {
                        map.put("rmId", salesAgentDataExport.getRegionalManager());
                    }
                    if ("cmId".equals(key)) {
                        map.put("cmId", salesAgentDataExport.getClusterManager());
                    }
                    if ("centreHeadId".equals(key)) {
                        map.put("centreHeadId", salesAgentDataExport.getCentreHead());
                    }
                }
                if (SalesTeam.IS.equals(team)) {
                    if ("isFMId".equals(key)) {
                        map.put("isFMId", salesAgentDataExport.getIsFMId());
                    }
                    if ("isRMId".equals(key)) {
                        map.put("isRMId", salesAgentDataExport.getIsRMId());
                    }
                }
                if ("tlId".equals(key)) {
                    map.put("tlId", salesAgentDataExport.getTlId());
                }
                if ("tlEmpId".equals(key)) {
                    map.put("tlEmpId", salesAgentDataExport.getTlEmpId());
                }
                if ("status".equals(key)) {
                    map.put("status", String.valueOf(salesAgentDataExport.getStatus()));
                }
                if ("dateOfLeaving".equals(key)) {
                    map.put("dateOfLeaving", salesAgentDataExport.getDateOfLeaving());
                }
                if ("leadsquaredId".equals(key)) {
                    map.put("leadsquaredId", salesAgentDataExport.getLeadsquaredId());
                }
                if ("updatedBy".equals(key)) {
                    map.put("updatedBy", salesAgentDataExport.getUpdatedBy());
                }
                if ("updatedAt".equals(key)) {
                    map.put("updatedAt", salesAgentDataExport.getUpdatedAt());
                }
            }
            list.add(map);
        }
        StringBuilder builder = new StringBuilder();
        for (Map<String, String> map : list) {
            builder.append(map.values()
                    .stream()
                    .map(e -> "\"" + e + "\"")
                    .collect(Collectors.joining(",")))
                    .append(System.lineSeparator());
        }
        return builder.toString().getBytes();
    }

    /**
     * header for csv
     *
     * @param team
     * @return
     */
    private Map<String, String> getSalesAgentCsvHeaderMap(SalesTeam team) {
        Map<String, String> map = new LinkedHashMap<>();
        map.put("id", "id");
        map.put("name", "name");
        map.put("email", "email");
        map.put("employeeId", "employee id");
        map.put("dateOfJoining", "DOJ");
        map.put("cohort", "cohort joining");
        map.put("designation", "designation");
        map.put("designationType", "designation type");
        if (SalesTeam.FOS.equals(team)) {
            map.put("centreId", "centre");
            map.put("zhId", "zonal head");
            map.put("rmId", "regional manager");
            map.put("cmId", "cluster manager");
            map.put("centreHeadId", "centre head");
        }
        if (SalesTeam.IS.equals(team)) {
            map.put("isFMId", "floor manager");
            map.put("isRMId", "revenue manager");
        }
        map.put("tlId", "team leader");
        map.put("tlEmpId", "tl employee id");
        map.put("status", "status");
        map.put("dateOfLeaving", "DOL");
        map.put("leadsquaredId", "leadsquared id");
        map.put("updatedBy", "updated by");
        map.put("updatedAt", "updated at");
        return map;
    }


    /**
     * Get centreInfo by Id
     *
     * @param centreId
     * @return
     */
    public CentreInfoRes getCentreInfoById(String centreId) {
        CentreInfoRes centreInfoRes = new CentreInfoRes();
        CentreInfo centreInfo = salesConversionDao.getCentreInfoById(centreId);
        if (centreInfo != null) {
            centreInfoRes.setCentreInfo(centreInfo);
            Set<String> agentIds = new HashSet<>();
            if (StringUtils.isNotEmpty(centreInfo.getZhId())) {
                agentIds.add(centreInfo.getZhId());
            }
            if (StringUtils.isNotEmpty(centreInfo.getRmId())) {
                agentIds.add(centreInfo.getRmId());
            }
            if (StringUtils.isNotEmpty(centreInfo.getCmId())) {
                agentIds.add(centreInfo.getCmId());
            }
            if (StringUtils.isNotEmpty(centreInfo.getCentreHeadId())) {
                agentIds.add(centreInfo.getCentreHeadId());
            }
            Set<String> requiredAgentFields = new HashSet<>();
            addSalesAgentRequiredFields(requiredAgentFields);
            addAbstractMongoEntityFields(requiredAgentFields);
            List<SalesAgentInfo> salesAgentInfos = salesConversionDao.getSalesAgentInfoByIds(agentIds, requiredAgentFields, true);
            if (CollectionUtils.isNotEmpty(salesAgentInfos)) {
                centreInfoRes.setSalesAgentInfos(salesAgentInfos);
            }
        }
        return centreInfoRes;
    }

    /**
     * Check whether user already present or not..
     *
     * @param checkSAUniqueValueReq
     * @return
     */
    public boolean checkSAUniqueValue(CheckSAUniqueValueReq checkSAUniqueValueReq) {
        List<SalesAgentInfo> salesAgentInfos = new ArrayList<>();
        if (StringUtils.isNotEmpty(checkSAUniqueValueReq.getEmail()) || StringUtils.isNotEmpty(checkSAUniqueValueReq.getEmployeeId())) {
            Set<String> requiredAgentFields = new HashSet<>();
            requiredAgentFields.add(SalesAgentInfo.Constants.EMAIL);
            requiredAgentFields.add(SalesAgentInfo.Constants.EMPLOYEE_ID);
            addAbstractMongoEntityFields(requiredAgentFields);
            salesAgentInfos = salesConversionDao.checkSAUniqueValue(checkSAUniqueValueReq, requiredAgentFields, true);
        }
        if (CollectionUtils.isNotEmpty(salesAgentInfos)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Gets all Status
     *
     * @param team
     * @return
     */
    public List<AgentStatus> getSAStatus(SalesTeam team) {
        List<AgentStatus> agentStatusList = new ArrayList<>();
        agentStatusList.add(AgentStatus.ACTIVE);
        agentStatusList.add(AgentStatus.INACTIVE);
        agentStatusList.add(AgentStatus.RESIGNED);
        agentStatusList.add(AgentStatus.TRAINING);
        agentStatusList.add(AgentStatus.DROPOUT);
        agentStatusList.add(AgentStatus.ABSCONDING);
        agentStatusList.add(AgentStatus.TERMINATED);
        agentStatusList.add(AgentStatus.LONG_LEAVE);
        agentStatusList.add(AgentStatus.ON_JOB_TRAINING);
        if (SalesTeam.IS.equals(team)) {
            agentStatusList.add(AgentStatus.PIP_WARNING_GIVEN);
        }
        return agentStatusList;
    }

    /**
     * Gets all the designations on the basis of team
     *
     * @param team
     * @return
     */
    public Map<AgentDesignation, AgentDesignationType> getSADesignation(SalesTeam team) {
        Map<AgentDesignation, AgentDesignationType> agentDesgToTypeMap = new HashMap<>();
        agentDesgToTypeMap.put(AgentDesignation.ACADEMIC_COUNSELLOR, AgentDesignationType.AC);
        agentDesgToTypeMap.put(AgentDesignation.SENIOR_ACADEMIC_COUNSELLOR, AgentDesignationType.AC);
        agentDesgToTypeMap.put(AgentDesignation.TEAM_LEADER, AgentDesignationType.TL);
        agentDesgToTypeMap.put(AgentDesignation.SENIOR_TEAM_LEADER, AgentDesignationType.TL);
        agentDesgToTypeMap.put(AgentDesignation.OPES, AgentDesignationType.OPS);
        if (SalesTeam.FOS.equals(team)) {
            agentDesgToTypeMap.put(AgentDesignation.INTERN, AgentDesignationType.AC);
            agentDesgToTypeMap.put(AgentDesignation.TELE_CALLER, AgentDesignationType.TC);
            agentDesgToTypeMap.put(AgentDesignation.ZONAL_HEAD, AgentDesignationType.ZH);
            agentDesgToTypeMap.put(AgentDesignation.REGIONAL_MANAGER, AgentDesignationType.RM);
            agentDesgToTypeMap.put(AgentDesignation.CLUSTER_MANAGER, AgentDesignationType.CM);
            agentDesgToTypeMap.put(AgentDesignation.CENTER_HEAD, AgentDesignationType.CH);
        }
        if (SalesTeam.IS.equals(team)) {
            agentDesgToTypeMap.put(AgentDesignation.FLOOR_MANAGER, AgentDesignationType.FM);
            agentDesgToTypeMap.put(AgentDesignation.TRAINING_QUALITY, AgentDesignationType.OPS);
            agentDesgToTypeMap.put(AgentDesignation.REVENUE_MANAGER, AgentDesignationType.RM);
        }
        return agentDesgToTypeMap;
    }

    public boolean checkCentreUniqueValue(String centreName) {
        List<CentreInfo> centreInfos = salesConversionDao.getCentreDetailsByName(centreName);
        if (CollectionUtils.isNotEmpty(centreInfos)) {
            return true;
        } else {
            return false;
        }
    }

    private void addAbstractMongoEntityFields(Set<String> requiredFields) {
        requiredFields.add(AbstractMongoEntity.Constants._ID);
        requiredFields.add(AbstractMongoEntity.Constants.CREATION_TIME);
        requiredFields.add(AbstractMongoEntity.Constants.CREATED_BY);
        requiredFields.add(AbstractMongoEntity.Constants.LAST_UPDATED);
        requiredFields.add(AbstractMongoEntity.Constants.ID);
        requiredFields.add(AbstractMongoEntity.Constants.LAST_UPDATED_BY);
        requiredFields.add(AbstractMongoEntity.Constants.ENTITY_STATE);
    }

    private void addSalesAgentRequiredFields(Set<String> requiredFields) {
        requiredFields.add(SalesAgentInfo.Constants.NAME);
        requiredFields.add(SalesAgentInfo.Constants.EMAIL);
        requiredFields.add(SalesAgentInfo.Constants.EMPLOYEE_ID);
        requiredFields.add(SalesAgentInfo.Constants.DESIGNATION);
    }

    /**
     * Agent search for email and employeeid
     *
     * @param getAllSalesAgentReq
     * @return
     */
    public SalesAgentMetadataRes getSARegex(GetAllSalesAgentReq getAllSalesAgentReq) {
        SalesAgentMetadataRes salesAgentMetadataRes = new SalesAgentMetadataRes();
        Set<String> requiredFields = new HashSet<>();
        requiredFields.add(SalesAgentInfo.Constants.CENTRE_ID);
        requiredFields.add(SalesAgentInfo.Constants.TL_ID);
        requiredFields.add(SalesAgentInfo.Constants.STATUS);
        requiredFields.add(SalesAgentInfo.Constants.TEAM);
        addSalesAgentRequiredFields(requiredFields);
        addAbstractMongoEntityFields(requiredFields);
        List<SalesAgentInfo> salesAgentInfos = salesConversionDao.getSARegex(getAllSalesAgentReq, requiredFields, true);
        if (CollectionUtils.isNotEmpty(salesAgentInfos)) {
            salesAgentMetadataRes.setSalesAgentInfoList(salesAgentInfos);
            salesAgentMetadataRes.setTlInfos(getTlInfoFromAgent(salesAgentInfos, requiredFields));
            salesAgentMetadataRes.setCentreInfos(getCentreInfoFromAgent(salesAgentInfos));
        }
        return salesAgentMetadataRes;
    }

    private List<CentreInfo> getCentreInfoFromAgent(List<SalesAgentInfo> salesAgentInfos) {
        List<CentreInfo> centreInfos = new ArrayList<>();
        if (CollectionUtils.isNotEmpty(salesAgentInfos)) {
            Set<String> centreIds = salesAgentInfos.stream().map(SalesAgentInfo::getCentreId).filter(Objects::nonNull).collect(Collectors.toSet());
            if (CollectionUtils.isNotEmpty(centreIds)) {
                Set<String> requiredCentreFields = new HashSet<>();
                requiredCentreFields.add(CentreInfo.Constants.CENTRE_NAME);
                requiredCentreFields.add(CentreInfo.Constants.RM);
                centreInfos = salesConversionDao.getCentreInfoByIds(centreIds, requiredCentreFields, true);
            }
        }
        return centreInfos;
    }

    private List<SalesAgentInfo> getTlInfoFromAgent(List<SalesAgentInfo> salesAgentInfos, Set<String> requiredFields) {
        List<SalesAgentInfo> tlInfos = new ArrayList<>();
        if (CollectionUtils.isNotEmpty(salesAgentInfos)) {
            Set<String> tlIds = salesAgentInfos.stream().map(SalesAgentInfo::getTlId).filter(Objects::nonNull).collect(Collectors.toSet());
            if (CollectionUtils.isNotEmpty(tlIds)) {
                tlInfos = salesConversionDao.getSalesAgentInfoByIds(tlIds, requiredFields, true);
            }
        }
        return tlInfos;
    }

    private List<SalesAgentInfo> getISFloorManagerInfoFromAgent(List<SalesAgentInfo> salesAgentInfos, Set<String> requiredFields) {
        List<SalesAgentInfo> fmInfos = new ArrayList<>();
        if (CollectionUtils.isNotEmpty(salesAgentInfos)) {
            Set<String> fmIds = salesAgentInfos.stream().map(SalesAgentInfo::getIsFMId).filter(Objects::nonNull).collect(Collectors.toSet());
            if (CollectionUtils.isNotEmpty(fmIds)) {
                fmInfos = salesConversionDao.getSalesAgentInfoByIds(fmIds, requiredFields, true);
            }
        }
        return fmInfos;
    }

    private List<SalesAgentInfo> getISRevenueManagerInfoFromAgent(List<SalesAgentInfo> salesAgentInfos, Set<String> requiredFields) {
        List<SalesAgentInfo> rmInfos = new ArrayList<>();
        if (CollectionUtils.isNotEmpty(salesAgentInfos)) {
            Set<String> rmIds = salesAgentInfos.stream().map(SalesAgentInfo::getIsRMId).filter(Objects::nonNull).collect(Collectors.toSet());
            if (CollectionUtils.isNotEmpty(rmIds)) {
                rmInfos = salesConversionDao.getSalesAgentInfoByIds(rmIds, requiredFields, true);
            }
        }
        return rmInfos;
    }

    private List<SalesAgentInfo> getRMInfoFromAgent(List<SalesAgentInfo> salesAgentInfos, Set<String> requiredFields) {
        List<SalesAgentInfo> rmInfos = new ArrayList<>();

        if (CollectionUtils.isNotEmpty(salesAgentInfos)) {
            Set<String> centreIds = salesAgentInfos.stream().map(SalesAgentInfo::getCentreId).filter(Objects::nonNull).collect(Collectors.toSet());
            List<CentreInfo> centreInfos = new ArrayList<>();
            if (CollectionUtils.isNotEmpty(centreIds)) {
                Set<String> requiredCentreFields = new HashSet<>();
                requiredCentreFields.add(CentreInfo.Constants.CENTRE_NAME);
                requiredCentreFields.add(CentreInfo.Constants.RM);
                centreInfos = salesConversionDao.getCentreInfoByIds(centreIds, requiredCentreFields, true);
            }
            Set<String> rmIds = centreInfos.stream().map(CentreInfo::getRmId).filter(Objects::nonNull).collect(Collectors.toSet());
            if (CollectionUtils.isNotEmpty(rmIds)) {
                rmInfos = salesConversionDao.getSalesAgentInfoByIds(rmIds, requiredFields, true);
            }
        }
        return rmInfos;
    }

    private void getCentreAndTlInfosOfAgents(SalesAgentMetadataRes salesAgentMetadataRes, Set<String> requiredFields) {
        if (CollectionUtils.isNotEmpty(salesAgentMetadataRes.getSalesAgentInfoList())) {
            Set<String> centreIds = salesAgentMetadataRes.getSalesAgentInfoList().stream().map(SalesAgentInfo::getCentreId).filter(Objects::nonNull).collect(Collectors.toSet());
            if (CollectionUtils.isNotEmpty(centreIds)) {
                Set<String> requiredCentreFields = new HashSet<>();
                requiredCentreFields.add(CentreInfo.Constants.CENTRE_NAME);
                List<CentreInfo> centreInfos = salesConversionDao.getCentreInfoByIds(centreIds, requiredCentreFields, true);
                if (CollectionUtils.isNotEmpty(centreInfos)) {
                    salesAgentMetadataRes.setCentreInfos(centreInfos);
                }
            }

            Set<String> tlIds = salesAgentMetadataRes.getSalesAgentInfoList().stream().map(SalesAgentInfo::getTlId).filter(Objects::nonNull).collect(Collectors.toSet());
            if (CollectionUtils.isNotEmpty(tlIds)) {
                List<SalesAgentInfo> tlInfos = salesConversionDao.getSalesAgentInfoByIds(tlIds, requiredFields, true);
                if (CollectionUtils.isNotEmpty(tlInfos)) {
                    salesAgentMetadataRes.setTlInfos(tlInfos);
                }
            }
        }
    }

    public PlatformBasicResponse addAgentsAsBulk(AddAgentsAsBulkReq addAgentsAsBulkReq, long callingUserId) throws VException {
        if (ArrayUtils.isNotEmpty(addAgentsAsBulkReq.getAgentDetailsList())) {
            addAgentsAsBulkReq.setFosDesignationMap(getSADesignation(SalesTeam.FOS));
            addAgentsAsBulkReq.setIsDesignationMap(getSADesignation(SalesTeam.IS));
        }
        addAgentsAsBulkReq.verify();
        int cnt = 1;
        Set<String> employeeIds = new HashSet<>();
        Set<String> centreNames = new HashSet<>();
        for (SalesAgentDataReqPojo salesAgentDataReqPojo : addAgentsAsBulkReq.getAgentDetailsList()) {
            employeeIds.add(salesAgentDataReqPojo.getEmployeeId());
            if (StringUtils.isNotEmpty(salesAgentDataReqPojo.getTlEmployeeId())) {
                employeeIds.add(salesAgentDataReqPojo.getTlEmployeeId());
            }
            if (StringUtils.isNotEmpty(salesAgentDataReqPojo.getIsRMEmployeeId())) {
                employeeIds.add(salesAgentDataReqPojo.getIsRMEmployeeId());
            }
            if (StringUtils.isNotEmpty(salesAgentDataReqPojo.getIsFMEmployeeId())) {
                employeeIds.add(salesAgentDataReqPojo.getIsFMEmployeeId());
            }
            centreNames.add(salesAgentDataReqPojo.getCentreName());
        }
        List<SalesAgentInfo> salesAgentInfosList = salesConversionDao.getSalesAgentInfosByEmployeeIds(employeeIds);
        List<CentreInfo> centreInfos = salesConversionDao.getCentreIdsByNames(centreNames);
        Map<String, String> centreNameToIdMap = centreInfos.stream().collect(Collectors.toMap(CentreInfo::getCentreName, CentreInfo::getId));
        Map<String, SalesAgentInfo> employeeIdToAgent = getEmployeeIdtoAgentMap(salesAgentInfosList);

        for (SalesAgentDataReqPojo salesAgentDataReqPojo : addAgentsAsBulkReq.getAgentDetailsList()) {
            SalesAgentInfo salesAgentInfo = employeeIdToAgent.get(salesAgentDataReqPojo.getEmployeeId());
            SalesAgentDataReq salesAgentDataReq = new SalesAgentDataReq();
            CheckSAUniqueValueReq checkSAUniqueValueReq = new CheckSAUniqueValueReq();
            checkSAUniqueValueReq.setEmail(salesAgentDataReqPojo.getEmail());
            boolean flag = checkSAUniqueValue(checkSAUniqueValueReq);

            if (salesAgentInfo != null) {
                salesAgentDataReq.setAgentId(salesAgentInfo.getId());
                if (salesAgentInfo.getEmail().equals(salesAgentDataReqPojo.getEmail()) || !flag) {
                    salesAgentDataReq.setEmail(salesAgentDataReqPojo.getEmail());
                } else {
                    throw new BadRequestException(ErrorCode.ALREADY_EXISTS, "Email already exists for another agent. check for record:" + cnt);
                }
                if (!salesAgentInfo.getTeam().equals(salesAgentDataReqPojo.getTeam())) {
                    throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "You are trying to update " + salesAgentInfo.getTeam() + "agent. record " + cnt);
                }
            } else {
                if (flag) {
                    throw new BadRequestException(ErrorCode.ALREADY_EXISTS, "Email already exists for another agent. check for record:" + cnt);
                } else {
                    salesAgentDataReq.setEmail(salesAgentDataReqPojo.getEmail());
                }
            }


            if (StringUtils.isNotEmpty(salesAgentDataReqPojo.getCentreName())) {
                String centreId = centreNameToIdMap.get(salesAgentDataReqPojo.getCentreName());
                if (StringUtils.isEmpty(centreId)) {
                    throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Entered centre not found for record " + cnt);
                }
                salesAgentDataReq.setCentreId(centreId);
            }

            salesAgentDataReq.setName(salesAgentDataReqPojo.getName());
            salesAgentDataReq.setEmployeeId(salesAgentDataReqPojo.getEmployeeId());
            salesAgentDataReq.setDateOfLeaving(salesAgentDataReqPojo.getDateOfLeaving());
            salesAgentDataReq.setDesignation(salesAgentDataReqPojo.getDesignation());
            salesAgentDataReq.setDesignationType(salesAgentDataReqPojo.getDesignationType());
            if (SalesTeam.IS.equals(salesAgentDataReqPojo.getTeam())) {
                if (StringUtils.isNotEmpty(salesAgentDataReqPojo.getIsFMEmployeeId())) {
                    SalesAgentInfo fmAgent = employeeIdToAgent.get(salesAgentDataReqPojo.getIsFMEmployeeId());
                    if (fmAgent != null) {
                        salesAgentDataReq.setIsFMId(fmAgent.getId());
                    } else {
                        throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, "Entered FM not found.. record " + cnt);
                    }
                }
                if (StringUtils.isNotEmpty(salesAgentDataReqPojo.getIsRMEmployeeId())) {
                    SalesAgentInfo rmAgent = employeeIdToAgent.get(salesAgentDataReqPojo.getIsRMEmployeeId());
                    if (rmAgent != null) {
                        salesAgentDataReq.setIsRMId(rmAgent.getId());
                    } else {
                        throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, "Entered RM not found.. record " + cnt);
                    }
                }

            }
            if (StringUtils.isNotEmpty(salesAgentDataReqPojo.getTlEmployeeId())) {
                SalesAgentInfo tlAgent = employeeIdToAgent.get(salesAgentDataReqPojo.getTlEmployeeId());
                if (tlAgent != null) {
                    salesAgentDataReq.setTlId(tlAgent.getId());
                } else {
                    throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, "Entered TL not found... record " + cnt);
                }
            }
            salesAgentDataReq.setStatus(salesAgentDataReqPojo.getStatus());
            salesAgentDataReq.setDateOfJoining(salesAgentDataReqPojo.getDateOfJoining());
            salesAgentDataReq.setLeadsquaredId(salesAgentDataReqPojo.getLeadsquaredId());
            salesAgentDataReq.setTeam(salesAgentDataReqPojo.getTeam());
            addSalesAgentData(salesAgentDataReq, callingUserId, true);
            cnt++;
        }
        return new PlatformBasicResponse();
    }

    private Map<String, SalesAgentInfo> getEmployeeIdtoAgentMap(List<SalesAgentInfo> salesAgentInfosList) {
        Map<String, SalesAgentInfo> employeeIdToAgent = new HashMap<>();
        if (ArrayUtils.isNotEmpty(salesAgentInfosList)) {
            for (SalesAgentInfo salesAgentInfo : salesAgentInfosList) {
                employeeIdToAgent.put(salesAgentInfo.getEmployeeId(), salesAgentInfo);
            }
        }
        return employeeIdToAgent;
    }

    public SalesAgentInfo getAgentInfoByEmployeeId(String employeeId) throws NotFoundException {
        return salesConversionDao.getSalesAgentInfoByEmployeeId(employeeId);
    }


    public SalesConversionApiResponseManager addAgentAttendance(AddAgentAttendanceReq addAgentAttendanceReq) throws ParseException, BadRequestException {

        SalesConversionApiResponseManager response;
        List<String> agentNotFoundIdsList = new ArrayList<>();
        Set<String> agentEmployeeIds = new HashSet<>();
        for (AgentAttendancePojo attendancePojo : addAgentAttendanceReq.getAgentAttendanceList()) {
            agentEmployeeIds.add(attendancePojo.getAgentId());
        }
        List<SalesAgentInfo> salesAgentInfos = new ArrayList<>();
        Map<String, SalesAgentInfo> salesAgentInfoMap = new HashMap<>();
        if (ArrayUtils.isNotEmpty(agentEmployeeIds)) {
            salesAgentInfos = salesConversionDao.getSalesAgentInfoByEmployeeIds(agentEmployeeIds);
            logger.info("****salesAgentInfos***" + salesAgentInfos);
        }
        salesAgentInfos.forEach(sa -> salesAgentInfoMap.put(sa.getEmployeeId(), sa));
        logger.info("****salesAgentInfosmap***" + salesAgentInfoMap);
        for (String agentId : agentEmployeeIds) {
            if (!salesAgentInfoMap.containsKey(agentId)) {
                agentNotFoundIdsList.add(agentId);
            }
        }
        logger.info("****agentNotFoundIdsList***" + agentNotFoundIdsList);


        if (ArrayUtils.isNotEmpty(agentNotFoundIdsList)) {
            response = new SalesConversionApiResponseManager.Builder()
                    .responseStatus("AGENT_NOT_FOUND")
                    .responseMessage("These Agents are not found in system..")
                    .payload(agentNotFoundIdsList)
                    .build();
            return response;
        }

        List<AgentAttendancePojo> updateList = new ArrayList<>();

        List<SalesAgentAttendanceInfo> salesAgentAttendanceInfos = new ArrayList<>();
        for (AgentAttendancePojo attendancePojo : addAgentAttendanceReq.getAgentAttendanceList()) {
            String agentId = salesConversionDao.getAgentIdByEmployeeId(attendancePojo.getAgentId());
            SalesAgentAttendanceInfo existedAgentInfo = salesConversionDao.getSAAttendanceInfo(agentId, attendancePojo.getDate());
            if (existedAgentInfo != null) {
                attendancePojo.setAgentId(agentId);
                if (StringUtils.isEmpty(attendancePojo.getAttendanceStatus())){
                    addAgentAttendanceToHistory(existedAgentInfo);
                    updateList.add(attendancePojo);
                }else if(existedAgentInfo.getAttendanceStatus() != AgentAttendanceStatus.valueOf(attendancePojo.getAttendanceStatus())) {
                    addAgentAttendanceToHistory(existedAgentInfo);
                    updateList.add(attendancePojo);
                }
            } else {
                if (StringUtils.isNotEmpty(attendancePojo.getAttendanceStatus())){
                    SalesAgentAttendanceInfo salesAgentAttendanceInfo = new SalesAgentAttendanceInfo();
                    salesAgentAttendanceInfo.setAgentId(agentId);
                    salesAgentAttendanceInfo.setAttendanceStatus(AgentAttendanceStatus.valueOf(attendancePojo.getAttendanceStatus()));
                    salesAgentAttendanceInfo.setDate(CommonUtils.parseTime(attendancePojo.getDate()));
                    salesAgentAttendanceInfos.add(salesAgentAttendanceInfo);
                }

            }

        }
        salesConversionDao.addAgentAttendance(salesAgentAttendanceInfos,addAgentAttendanceReq.getCallingUserId());
        logger.info("updated List " + updateList);
        if (ArrayUtils.isNotEmpty(updateList)) {
            for (AgentAttendancePojo attendancePojo : updateList) {
                salesConversionDao.updateSAAttendanceInfo(attendancePojo);
            }
        }
        response = new SalesConversionApiResponseManager.Builder()
                .responseStatus("SUCCESS")
                .build();
        return response;
    }

    public void addAgentAttendanceToHistory(SalesAgentAttendanceInfo agentAttendancePojo) throws ParseException {
        SalesAgentAttendanceInfoHistory salesAgentAttendanceInfoHistory = new SalesAgentAttendanceInfoHistory();
        salesAgentAttendanceInfoHistory.setAgentId(agentAttendancePojo.getAgentId());
        salesAgentAttendanceInfoHistory.setDate(agentAttendancePojo.getDate());
        salesAgentAttendanceInfoHistory.setAttendanceStatus(agentAttendancePojo.getAttendanceStatus());
        salesConversionDao.addAgentAttendanceToHistory(salesAgentAttendanceInfoHistory);
    }

    /**
     * get attendance data for listing
     *
     * @param salesAgentAttendanceDataReq
     * @param isGenerateCsv
     * @return
     * @throws VException
     */
    public SalesAgentAttendanceDataRes getAgentAttendanceData(SalesAgentAttendanceDataReq salesAgentAttendanceDataReq, Boolean isGenerateCsv) throws VException {

        SalesAgentAttendanceDataRes salesAgentAttendanceDataRes = new SalesAgentAttendanceDataRes();

        SalesAgentMetadataReq salesAgentMetadataReq = new SalesAgentMetadataReq();
        salesAgentMetadataReq.setZhId(salesAgentAttendanceDataReq.getZhId());
        salesAgentMetadataReq.setRmId(salesAgentAttendanceDataReq.getRmId());
        salesAgentMetadataReq.setCmId(salesAgentAttendanceDataReq.getCmId());
        salesAgentMetadataReq.setCentreId(salesAgentAttendanceDataReq.getCentreId());
        salesAgentMetadataReq.setTlId(salesAgentAttendanceDataReq.getTlId());
        salesAgentMetadataReq.setIsFMId(salesAgentAttendanceDataReq.getIsFMId());
        salesAgentMetadataReq.setIsRMId(salesAgentAttendanceDataReq.getIsRMId());
        salesAgentMetadataReq.setStatus(salesAgentAttendanceDataReq.getStatus());
        salesAgentMetadataReq.setDesignation(salesAgentAttendanceDataReq.getDesignation());
        salesAgentMetadataReq.setDateOfJoiningFrom(salesAgentAttendanceDataReq.getDateOfJoiningFrom());
        salesAgentMetadataReq.setDateOfJoiningTo(salesAgentAttendanceDataReq.getDateOfJoiningTo());
        salesAgentMetadataReq.setTeam(salesAgentAttendanceDataReq.getTeam());
        salesAgentMetadataReq.setStart(salesAgentAttendanceDataReq.getStart());
        salesAgentMetadataReq.setSize(salesAgentAttendanceDataReq.getSize());
        SalesAgentMetadataRes salesAgentMetadataRes = getSalesAgentMetadata(salesAgentMetadataReq);

        if (CollectionUtils.isNotEmpty(salesAgentMetadataRes.getSalesAgentInfoList())) {
            salesAgentAttendanceDataRes = getAttendanceData(salesAgentMetadataRes.getSalesAgentInfoList(), salesAgentMetadataRes.getSalesAgentInfoCount(), salesAgentAttendanceDataReq, isGenerateCsv);
        }
        return salesAgentAttendanceDataRes;
    }

    private SalesAgentAttendanceDataRes getAttendanceData(List<SalesAgentInfo> salesAgentInfoList, Integer salesAgentInfoCount, SalesAgentAttendanceDataReq salesAgentAttendanceDataReq, Boolean isGenerateCsv) throws VException {

        SalesAgentAttendanceDataRes salesAgentAttendanceDataRes = new SalesAgentAttendanceDataRes();
        List<AgentAttendanceMetadataRes> agentAttendanceData = new ArrayList<>();
        Map<String, List<SalesAgentAttendanceInfo>> agentToAttendanceListMap = new LinkedHashMap<>();

        List<String> agentIds = salesAgentInfoList.parallelStream().map(p -> p.getId()).filter(Objects::nonNull).collect(Collectors.toList());

        List<SalesAgentAttendanceInfo> salesAgentAttendanceInfos = salesConversionDao.getAgentAttendanceData(agentIds, salesAgentAttendanceDataReq);
        salesAgentAttendanceDataRes.setSalesAgentInfoCount(salesAgentInfoCount);

        if (CollectionUtils.isNotEmpty(salesAgentAttendanceInfos)) {
            for (SalesAgentAttendanceInfo salesAgentAttendanceInfo : salesAgentAttendanceInfos) {
                if (agentToAttendanceListMap.containsKey(salesAgentAttendanceInfo.getAgentId())) {
                    agentToAttendanceListMap.get(salesAgentAttendanceInfo.getAgentId()).add(salesAgentAttendanceInfo);
                } else {
                    List<SalesAgentAttendanceInfo> addIntoMap = new ArrayList<>();
                    addIntoMap.add(salesAgentAttendanceInfo);
                    agentToAttendanceListMap.put(salesAgentAttendanceInfo.getAgentId(), addIntoMap);
                }
            }
            Map<String, SalesAgentInfo> agentIdToObject = getAgentIdToObjectMap(salesAgentInfoList, agentToAttendanceListMap.keySet());
            for (Map.Entry<String, List<SalesAgentAttendanceInfo>> entry : agentToAttendanceListMap.entrySet()) {
                SalesAgentInfo salesAgentInfo = agentIdToObject.get(entry.getKey());
                AgentAttendanceMetadataRes attendanceMetadataRes = getAgentDataForAttendance(salesAgentInfo, salesAgentAttendanceDataReq.getTeam(), entry.getValue());
                agentAttendanceData.add(attendanceMetadataRes);
            }
        }
        for (SalesAgentInfo agentInfo : salesAgentInfoList) {
            if (!agentToAttendanceListMap.containsKey(agentInfo.getId())) {
                AgentAttendanceMetadataRes agentAttendanceMetadataRes = getAgentDataForAttendance(agentInfo, salesAgentAttendanceDataReq.getTeam(), new ArrayList<>());
                agentAttendanceData.add(agentAttendanceMetadataRes);
            }
        }
        salesAgentAttendanceDataRes.setAgentAttendanceData(agentAttendanceData);

        if (!isGenerateCsv) {
            Set<String> requiredFields = new HashSet<>();
            addSalesAgentRequiredFields(requiredFields);
            salesAgentAttendanceDataRes.setTlInfos(getTlInfoFromAgent(salesAgentInfoList, requiredFields));

            if (SalesTeam.FOS.equals(salesAgentAttendanceDataReq.getTeam())) {
                salesAgentAttendanceDataRes.setCentreInfos(getCentreInfoFromAgent(salesAgentInfoList));
                salesAgentAttendanceDataRes.setRmInfos(getRMInfoFromAgent(salesAgentInfoList, requiredFields));
            } else if (SalesTeam.IS.equals(salesAgentAttendanceDataReq.getTeam())) {
                salesAgentAttendanceDataRes.setIsFMInfos(getISFloorManagerInfoFromAgent(salesAgentInfoList, requiredFields));
                salesAgentAttendanceDataRes.setIsRMInfos(getISRevenueManagerInfoFromAgent(salesAgentInfoList, requiredFields));
            }
        }
        return salesAgentAttendanceDataRes;
    }

    private AgentAttendanceMetadataRes getAgentDataForAttendance(SalesAgentInfo salesAgentInfo, SalesTeam team, List<SalesAgentAttendanceInfo> attendanceInfo) throws VException {
        AgentAttendanceMetadataRes attendanceMetadataRes = new AgentAttendanceMetadataRes();
        if (salesAgentInfo == null) {
            throw new VException(ErrorCode.BAD_REQUEST_ERROR, "Agent id not present in system");
        }
        attendanceMetadataRes.setName(salesAgentInfo.getName());
        attendanceMetadataRes.setEmployeeId(salesAgentInfo.getEmployeeId());
        attendanceMetadataRes.setAgentId(salesAgentInfo.getId());
        attendanceMetadataRes.setEmail(salesAgentInfo.getEmail());
        attendanceMetadataRes.setDateOfJoining(salesAgentInfo.getDateOfJoining());
        attendanceMetadataRes.setDesignation(salesAgentInfo.getDesignation());
        attendanceMetadataRes.setDesignationType(salesAgentInfo.getDesignationType());
        attendanceMetadataRes.setStatus(salesAgentInfo.getStatus());
        attendanceMetadataRes.setLeadSquaredId(salesAgentInfo.getLeadsquaredId());
        attendanceMetadataRes.setDateOfLeaving(salesAgentInfo.getDateOfLeaving());
        if (SalesTeam.FOS.equals(team)) {
            attendanceMetadataRes.setCentreId(salesAgentInfo.getCentreId());
        }
        if (SalesTeam.IS.equals(team)) {
            attendanceMetadataRes.setIsFMId(salesAgentInfo.getIsFMId());
            attendanceMetadataRes.setIsRMId(salesAgentInfo.getIsRMId());
        }
        attendanceMetadataRes.setTlId(salesAgentInfo.getTlId());
        attendanceMetadataRes.setAttendance(attendanceInfo);
        return attendanceMetadataRes;
    }

    private Map<String, SalesAgentInfo> getAgentIdToObjectMap(List<SalesAgentInfo> salesAgentInfos, Set<String> matchedIds) {
        Map<String, SalesAgentInfo> map = new HashMap<>();
        for (SalesAgentInfo salesAgentInfo : salesAgentInfos) {
            if (matchedIds.contains(salesAgentInfo.getId())) {
                map.put(salesAgentInfo.getId(), salesAgentInfo);
            }
        }
        return map;
    }

    public PlatformBasicResponse editAgentAttendance(AgentAttendancePojo agentAttendancePojo) throws ParseException, NotFoundException {
        String agentId = salesConversionDao.getAgentIdByEmployeeId(agentAttendancePojo.getAgentId());
        SalesAgentAttendanceInfo existedAgentInfo = salesConversionDao.getSAAttendanceInfo(agentId, agentAttendancePojo.getDate());
        if (existedAgentInfo != null) {
            addAgentAttendanceToHistory(existedAgentInfo);
        }
        if (existedAgentInfo == null && StringUtils.isNotEmpty(agentAttendancePojo.getAttendanceStatus())) {
            SalesAgentAttendanceInfo salesAgentAttendanceInfo =  new SalesAgentAttendanceInfo();
            salesAgentAttendanceInfo.setAgentId(agentId);
            salesAgentAttendanceInfo.setAttendanceStatus(AgentAttendanceStatus.valueOf(agentAttendancePojo.getAttendanceStatus()));
            salesAgentAttendanceInfo.setDate(CommonUtils.parseTime(agentAttendancePojo.getDate()));
            salesConversionDao.editAgentAttendance(salesAgentAttendanceInfo, agentAttendancePojo.getCallingUserId());
            return new PlatformBasicResponse();
        }
        agentAttendancePojo.setAgentId(agentId);
        salesConversionDao.updateSAAttendanceInfo(agentAttendancePojo);
        return new PlatformBasicResponse();
    }

    public SalesAgentAttendanceDataRes getAgentAttendanceById(String id, SalesTeam team) throws VException {
        SalesAgentAttendanceDataReq salesAgentAttendanceDataReq = new SalesAgentAttendanceDataReq();
        salesAgentAttendanceDataReq.setTeam(team);
        SalesAgentInfo salesAgentInfo = salesConversionDao.getSalesAgentInfoById(id);
        if (salesAgentInfo != null) {
            return getAttendanceData(Arrays.asList(salesAgentInfo), 1, salesAgentAttendanceDataReq, false);
        } else {
            return new SalesAgentAttendanceDataRes();
        }
    }

    public Map<String, AgentAttendanceStatus> getAttendanceAcronyms() {
        Map<String, AgentAttendanceStatus> agentAttendanceStatusMap = new HashMap<>();
        agentAttendanceStatusMap.put(AgentAttendanceStatus.WO.value, AgentAttendanceStatus.WO);
        agentAttendanceStatusMap.put(AgentAttendanceStatus.P.value, AgentAttendanceStatus.P);
        agentAttendanceStatusMap.put(AgentAttendanceStatus.SB.value, AgentAttendanceStatus.SB);
        agentAttendanceStatusMap.put(AgentAttendanceStatus.HDL.value, AgentAttendanceStatus.HDL);
        agentAttendanceStatusMap.put(AgentAttendanceStatus.UL.value, AgentAttendanceStatus.UL);
        agentAttendanceStatusMap.put(AgentAttendanceStatus.AL.value, AgentAttendanceStatus.AL);
        agentAttendanceStatusMap.put(AgentAttendanceStatus.SL.value, AgentAttendanceStatus.SL);
        agentAttendanceStatusMap.put(AgentAttendanceStatus.R.value, AgentAttendanceStatus.R);
        agentAttendanceStatusMap.put(AgentAttendanceStatus.AB.value, AgentAttendanceStatus.AB);
        agentAttendanceStatusMap.put(AgentAttendanceStatus.DP.value, AgentAttendanceStatus.DP);
        agentAttendanceStatusMap.put(AgentAttendanceStatus.LOP.value, AgentAttendanceStatus.LOP);
        agentAttendanceStatusMap.put(AgentAttendanceStatus.ML.value, AgentAttendanceStatus.ML);
        agentAttendanceStatusMap.put(AgentAttendanceStatus.PL.value, AgentAttendanceStatus.PL);
        logger.info("***agentAttendanceStatusMap***", agentAttendanceStatusMap);
        return agentAttendanceStatusMap;
    }

    public String generateAgentAttendanceCsv(SalesAgentAttendanceDataReq salesAgentAttendanceDataReq) {
        try {
            Path page = Files.createTempFile("page", ".csv");
            Map<String, String> headerMap = getAgentAttendanceCsvHeaderMap(salesAgentAttendanceDataReq);
            List<String> header = new ArrayList<>(headerMap.values());
            Files.write(page, String.join(",", header).concat(System.lineSeparator()).getBytes(),
                    StandardOpenOption.CREATE, StandardOpenOption.APPEND);

            Integer size = 500;
            Integer start = 0;
            salesAgentAttendanceDataReq.setSize(size);
            List<AgentAttendanceDataExport> agentAttendanceDataExportList;
            SalesAgentAttendanceDataRes salesAgentAttendanceDataRes;
            do {
                salesAgentAttendanceDataReq.setStart(start);
                //Getting data on the basis of filter
                salesAgentAttendanceDataRes = getAgentAttendanceData(salesAgentAttendanceDataReq, true);
                if (CollectionUtils.isEmpty(salesAgentAttendanceDataRes.getAgentAttendanceData())) {
                    logger.info("Data not present for requested filters");
                    return "Data not present for requested filters";
                }
                agentAttendanceDataExportList = getAgentAttendanceCsvData(salesAgentAttendanceDataRes.getAgentAttendanceData(), salesAgentAttendanceDataReq.getTeam(), salesAgentAttendanceDataReq.getAttendanceFrom(), salesAgentAttendanceDataReq.getAttendanceTo());
                start += size;
                Files.write(page, getAgentAttendanceCsvBytes(agentAttendanceDataExportList, headerMap, salesAgentAttendanceDataReq.getTeam()), StandardOpenOption.CREATE, StandardOpenOption.APPEND);
            } while (salesAgentAttendanceDataRes.getAgentAttendanceData().size() == 500);

            String key = ENV + "/temp/sales/agent/csv/" + LocalDate.now().format(DateTimeFormatter.BASIC_ISO_DATE) + "/" +
                    UUID.randomUUID().toString() + ".csv";
            awsS3Manager.uploadFile(S3_BUCKET, key, page.toFile());
            logger.info(page.toAbsolutePath());
            page.toFile().delete();
            return awsS3Manager.getPublicBucketDownloadUrl(S3_BUCKET, key);
        } catch (Exception e) {
            logger.error("Error ocurred while generating sales agent csv: {}", e);
            return "ERROR";
        }
    }

    private byte[] getAgentAttendanceCsvBytes(List<AgentAttendanceDataExport> agentAttendanceDataExportList, Map<String, String> headerMap, SalesTeam team) {

        List<Map<String, String>> list = new ArrayList<>();

        for (AgentAttendanceDataExport agentAttendanceDataExport : agentAttendanceDataExportList) {
            Map<String, String> map = new LinkedHashMap<>();
            for (Map.Entry<String, String> entry : headerMap.entrySet()) {
                String key = entry.getKey();
                if ("id".equals(key)){
                    map.put(key, agentAttendanceDataExport.getId());
                }
                if ("name".equals(key)) {
                    map.put(key, agentAttendanceDataExport.getName());
                }
                if ("email".equals(key)) {
                    map.put(key, agentAttendanceDataExport.getEmail());
                }
                if ("employeeId".equals(key)) {
                    map.put(key, agentAttendanceDataExport.getEmployeeId());
                }
                if ("dateOfJoining".equals(key)) {
                    map.put(key, agentAttendanceDataExport.getDateOfJoining());
                }
                if ("cohort".equals(key)) {
                    map.put(key, agentAttendanceDataExport.getCohortDOJ());
                }
                if ("designation".equals(key)) {
                    map.put(key, agentAttendanceDataExport.getDesignation());
                }
                if ("designationType".equals(key)) {
                    map.put(key, agentAttendanceDataExport.getDesignationType());
                }
                if (SalesTeam.FOS.equals(team)) {
                    if ("centreId".equals(key)) {
                        map.put(key, agentAttendanceDataExport.getCentre());
                    }
                    if ("zhId".equals(key)) {
                        map.put(key, agentAttendanceDataExport.getZonalHead());
                    }
                    if ("rmId".equals(key)) {
                        map.put(key, agentAttendanceDataExport.getRegionalManager());
                    }
                    if ("cmId".equals(key)) {
                        map.put(key, agentAttendanceDataExport.getClusterManager());
                    }
                    if ("centreHeadId".equals(key)) {
                        map.put(key, agentAttendanceDataExport.getCentreHead());
                    }
                }
                if (SalesTeam.IS.equals(team)) {
                    if ("isFMId".equals(key)) {
                        map.put(key, agentAttendanceDataExport.getIsFMId());
                    }
                    if ("isRMId".equals(key)) {
                        map.put(key, agentAttendanceDataExport.getIsRMId());
                    }
                }
                if ("tlId".equals(key)) {
                    map.put(key, agentAttendanceDataExport.getTlId());
                }
                if ("tlEmpId".equals(key)) {
                    map.put(key, agentAttendanceDataExport.getTlEmpId());
                }
                if ("status".equals(key)) {
                    map.put(key, agentAttendanceDataExport.getStatus());
                }
                if ("dateOfLeaving".equals(key)) {
                    map.put(key,agentAttendanceDataExport.getDateOfLeaving());
                }
                if ("leadsquaredId".equals(key)) {
                    map.put(key,agentAttendanceDataExport.getLeadsquaredId());
                }
            }
            for (Map.Entry<String, String> entryAttendance : agentAttendanceDataExport.getAttendanceData().entrySet()) {
                map.put(entryAttendance.getKey(), entryAttendance.getValue());
            }
            list.add(map);
        }
        StringBuilder builder = new StringBuilder();
        for (Map<String, String> map : list) {
            builder.append(map.values()
                    .stream()
                    .map(e -> "\"" + e + "\"")
                    .collect(Collectors.joining(",")))
                    .append(System.lineSeparator());
        }
        return builder.toString().getBytes();

    }

    private List<AgentAttendanceDataExport> getAgentAttendanceCsvData(List<AgentAttendanceMetadataRes> agentAttendanceData, SalesTeam team, Long attendanceFrom, Long attendanceTo) {
        Set<String> agentIds = new HashSet<>();
        Map<String, String> agentIdToNameMap = new HashMap<>();
        Map<String, String> agentIdToEmployeeIdMap = new HashMap<>();
        Map<String, CentreInfo> centreIdToObject = new HashMap<>();
        List<AgentAttendanceDataExport> agentAttendanceDataExportList = new ArrayList<>();
        Map<String, String> dateMap = new LinkedHashMap<>();
        addDateColumns(dateMap, attendanceFrom, attendanceTo);
        logger.info("data header map, {}", dateMap);
        if (SalesTeam.FOS.equals(team)) {
            //getting centre data..
            Set<String> centreIds = agentAttendanceData.stream().map(p -> p.getCentreId()).filter(Objects::nonNull).collect(Collectors.toSet());
            List<CentreInfo> centreInfos = salesConversionDao.getCentreInfoByIds(centreIds, null, true);
            if (CollectionUtils.isNotEmpty(centreInfos)) {
                //getting all the agents related to centre
                centreIdToObject = getCentreIdToObjectMap(centreInfos);
                agentIds.addAll(centreInfos.parallelStream().map(p -> p.getZhId()).filter(Objects::nonNull).collect(Collectors.toSet()));
                agentIds.addAll(centreInfos.parallelStream().map(p -> p.getRmId()).filter(Objects::nonNull).collect(Collectors.toSet()));
                agentIds.addAll(centreInfos.parallelStream().map(p -> p.getCmId()).filter(Objects::nonNull).collect(Collectors.toSet()));
                agentIds.addAll(centreInfos.parallelStream().map(p -> p.getCentreHeadId()).filter(Objects::nonNull).collect(Collectors.toSet()));
            }
        } else if (SalesTeam.IS.equals(team)) {
            agentIds.addAll(agentAttendanceData.parallelStream().map(p -> p.getIsFMId()).filter(Objects::nonNull).collect(Collectors.toSet()));
            agentIds.addAll(agentAttendanceData.parallelStream().map(p -> p.getIsRMId()).filter(Objects::nonNull).collect(Collectors.toSet()));
        }
        agentIds.addAll(agentAttendanceData.parallelStream().filter(Objects::nonNull).map(p -> p.getTlId()).filter(Objects::nonNull).collect(Collectors.toSet()));
        //Maintaing map of all the agents related to a particluar agent
        if (CollectionUtils.isNotEmpty(agentIds)) {
            Set<String> requiredAgentFields = new HashSet<>();
            addSalesAgentRequiredFields(requiredAgentFields);
            addAbstractMongoEntityFields(requiredAgentFields);
            List<SalesAgentInfo> agentInfo = salesConversionDao.getSalesAgentInfoByIds(agentIds, requiredAgentFields, true);

            if (CollectionUtils.isNotEmpty(agentInfo)) {
                agentIdToNameMap = agentInfo.stream().filter(Objects::nonNull).filter(p -> p.getName() != null).collect(Collectors.toMap(SalesAgentInfo::getId, SalesAgentInfo::getName));
                agentIdToEmployeeIdMap = agentInfo.stream().filter(Objects::nonNull).filter(p -> p.getEmployeeId() != null).collect(Collectors.toMap(SalesAgentInfo::getId, SalesAgentInfo::getEmployeeId));
            }
        }
        SimpleDateFormat sdf = new SimpleDateFormat("MM-dd-yyyy");

        for (AgentAttendanceMetadataRes data : agentAttendanceData) {
            AgentAttendanceDataExport agentAttendanceDataExport = new AgentAttendanceDataExport();
            agentAttendanceDataExport.setId(data.getAgentId());
            agentAttendanceDataExport.setName(data.getName());
            agentAttendanceDataExport.setEmail(data.getEmail());
            agentAttendanceDataExport.setEmployeeId(data.getEmployeeId());
            agentAttendanceDataExport.setDesignation(data.getDesignation().name());
            agentAttendanceDataExport.setDesignationType(data.getDesignationType().name());
            if (StringUtils.isNotEmpty(data.getLeadSquaredId())){
                agentAttendanceDataExport.setLeadsquaredId(data.getLeadSquaredId());
            }else {
                agentAttendanceDataExport.setLeadsquaredId("");
            }
            if (data.getDateOfJoining() != null) {
                sdf.setTimeZone(TimeZone.getTimeZone(DateTimeUtils.TIME_ZONE_IN));
                String DOJ = sdf.format(new Date(data.getDateOfJoining()));
                agentAttendanceDataExport.setDateOfJoining(org.apache.commons.lang.StringUtils.defaultString(DOJ, " "));
                agentAttendanceDataExport.setCohortDOJ(org.apache.commons.lang.StringUtils.defaultString(calculateCohortDate(data.getDateOfJoining()), " "));
            } else {
                agentAttendanceDataExport.setDateOfJoining(" ");
                agentAttendanceDataExport.setCohortDOJ(" ");
            }
            if (data.getDateOfLeaving() != null) {
                logger.info("******dol" + data.getDateOfLeaving());
                String DOL = sdf.format(new Date(data.getDateOfLeaving()));
                agentAttendanceDataExport.setDateOfLeaving(org.apache.commons.lang.StringUtils.defaultString(DOL, " "));
            } else {
                agentAttendanceDataExport.setDateOfLeaving(" ");
            }
            if (StringUtils.isNotEmpty(data.getLeadSquaredId())) {
                agentAttendanceDataExport.setLeadsquaredId(data.getLeadSquaredId());
            }else {
                agentAttendanceDataExport.setLeadsquaredId(data.getLeadSquaredId());
            }


            if (data.getStatus() != null) {
                agentAttendanceDataExport.setStatus(data.getStatus().name());
            }
            if (StringUtils.isNotEmpty(data.getLeadSquaredId())){
                agentAttendanceDataExport.setLeadsquaredId(data.getLeadSquaredId());
            }

            if (SalesTeam.FOS.equals(team)) {
                if (centreIdToObject.containsKey(data.getCentreId())) {
                    CentreInfo centreInfo = centreIdToObject.get(data.getCentreId());
                    agentAttendanceDataExport.setCentre(org.apache.commons.lang.StringUtils.defaultString(centreInfo.getCentreName(), " "));

                    if (null != centreInfo.getZhId()) {
                        agentAttendanceDataExport.setZonalHead(org.apache.commons.lang.StringUtils.defaultString(agentIdToNameMap.get(centreInfo.getZhId()), " "));
                    } else {
                        agentAttendanceDataExport.setZonalHead(" ");
                    }

                    if (null != centreInfo.getRmId()) {
                        agentAttendanceDataExport.setRegionalManager(org.apache.commons.lang.StringUtils.defaultString(agentIdToNameMap.get(centreInfo.getRmId()), " "));
                    } else {
                        agentAttendanceDataExport.setRegionalManager(" ");
                    }

                    if (null != centreInfo.getCmId()) {
                        agentAttendanceDataExport.setClusterManager(org.apache.commons.lang.StringUtils.defaultString(agentIdToNameMap.get(centreInfo.getCmId()), " "));
                    } else {
                        agentAttendanceDataExport.setClusterManager(" ");
                    }

                    if (null != centreInfo.getCentreHeadId()) {
                        agentAttendanceDataExport.setCentreHead(org.apache.commons.lang.StringUtils.defaultString(agentIdToNameMap.get(centreInfo.getCentreHeadId()), " "));
                    } else {
                        agentAttendanceDataExport.setCentreHead(" ");
                    }
                } else {
                    agentAttendanceDataExport.setCentre(" ");
                    agentAttendanceDataExport.setZonalHead(" ");
                    agentAttendanceDataExport.setRegionalManager(" ");
                    agentAttendanceDataExport.setClusterManager(" ");
                    agentAttendanceDataExport.setCentreHead(" ");
                }
            } else if (SalesTeam.IS.equals(team)) {
                agentAttendanceDataExport.setIsFMId(org.apache.commons.lang.StringUtils.defaultString(agentIdToNameMap.get(data.getIsFMId()), " "));
                agentAttendanceDataExport.setIsRMId(org.apache.commons.lang.StringUtils.defaultString(agentIdToNameMap.get(data.getIsRMId()), " "));
            }
            agentAttendanceDataExport.setTlId(org.apache.commons.lang.StringUtils.defaultString(agentIdToNameMap.get(data.getTlId()), " "));
            agentAttendanceDataExport.setTlEmpId(org.apache.commons.lang.StringUtils.defaultString(agentIdToEmployeeIdMap.get(data.getTlId()), " "));

            LinkedHashMap<String, String> attendanceData = new LinkedHashMap<>();
            Map<String, AgentAttendanceStatus> attendanceMap = getDateToAttendanceStatusMap(data.getAttendance());
            for (Map.Entry<String, String> entry : dateMap.entrySet()) {
                if (attendanceMap.containsKey(entry.getKey())) {
                    String value = String.valueOf(attendanceMap.get(entry.getKey()));
                    attendanceData.put(entry.getKey(), org.apache.commons.lang.StringUtils.defaultString(value, " "));
                } else {
                    attendanceData.put(entry.getKey(), " ");
                }
            }
            agentAttendanceDataExport.setAttendanceData(attendanceData);
            agentAttendanceDataExportList.add(agentAttendanceDataExport);
        }
        return agentAttendanceDataExportList;
    }

    private Map<String, AgentAttendanceStatus> getDateToAttendanceStatusMap(List<SalesAgentAttendanceInfo> attendance) {
        SimpleDateFormat sdf = new SimpleDateFormat("MM-dd-yyyy");
        Map<String, AgentAttendanceStatus> dateToAttendanceMap = new HashMap<>();
        for (SalesAgentAttendanceInfo info : attendance) {
            sdf.setTimeZone(TimeZone.getTimeZone(DateTimeUtils.TIME_ZONE_IN));
            String date = sdf.format(new Date(info.getDate()));
            dateToAttendanceMap.put(date, info.getAttendanceStatus());
        }
        return dateToAttendanceMap;
    }

    private Map<String, String> getAgentAttendanceCsvHeaderMap(SalesAgentAttendanceDataReq salesAgentAttendanceDataReq) {
        Map<String, String> map = new LinkedHashMap<>();
        map.put("id","id");
        map.put("name", "name");
        map.put("email", "email");
        map.put("employeeId", "employee id");
        map.put("dateOfJoining", "DOJ");
        map.put("cohort", "cohort joining");
        map.put("designation", "designation");
        map.put("designationType", "designation type");
        if (SalesTeam.FOS.equals(salesAgentAttendanceDataReq.getTeam())) {
            map.put("centreId", "centre");
            map.put("zhId", "zonal head");
            map.put("rmId", "regional manager");
            map.put("cmId", "cluster manager");
            map.put("centreHeadId", "centre head");
        }
        if (SalesTeam.IS.equals(salesAgentAttendanceDataReq.getTeam())) {
            map.put("isFMId", "floor manager");
            map.put("isRMId", "revenue manager");
        }
        map.put("tlId", "team leader");
        map.put("tlEmpId", "tl employee id");
        map.put("status", "status");
        map.put("dateOfLeaving", "DOL");
        map.put("leadsquaredId", "leadsquared id");
        addDateColumns(map, salesAgentAttendanceDataReq.getAttendanceFrom(), salesAgentAttendanceDataReq.getAttendanceTo());
        return map;
    }

    private void addDateColumns(Map<String, String> map, Long attendanceFrom, Long attendanceTo) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM-dd-YYYY");
        LocalDate localDateTo = null;
        LocalDate localDateFrom = null;
        if ((attendanceFrom != null) || (attendanceTo != null)) {
            if (attendanceFrom != null && attendanceTo != null) {
                localDateTo = Instant.ofEpochMilli(attendanceTo)
                        .atZone(ZoneId.systemDefault())
                        .toLocalDate();
                localDateFrom = Instant.ofEpochMilli(attendanceFrom)
                        .atZone(ZoneId.systemDefault())
                        .toLocalDate().plusDays(1);
            } else if (attendanceFrom != null && attendanceTo == null) {
                //considering default 30 days
                localDateFrom = Instant.ofEpochMilli(attendanceFrom)
                        .atZone(ZoneId.systemDefault())
                        .toLocalDate().plusDays(1);
                Long difference = System.currentTimeMillis() - attendanceFrom;
                Long days = difference / (1000 * 60 * 60 * 24);
                localDateTo = localDateFrom.plusDays(days);
            } else {
                localDateTo = Instant.ofEpochMilli(attendanceTo)
                        .atZone(ZoneId.systemDefault())
                        .toLocalDate();
                localDateFrom = localDateTo.minusDays(31);
            }
        } else {
            localDateTo = Instant.ofEpochMilli(System.currentTimeMillis())
                    .atZone(ZoneId.systemDefault())
                    .toLocalDate();
            localDateFrom = localDateTo.minusDays(31);
        }
        for (LocalDate date = localDateFrom; date.isBefore(localDateTo.plusDays(1)); date = date.plusDays(1)) {
            map.put(formatter.format(date), formatter.format(date));
        }
    }

}