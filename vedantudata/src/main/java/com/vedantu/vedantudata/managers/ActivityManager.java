package com.vedantu.vedantudata.managers;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.User.*;
import com.vedantu.async.AsyncTaskFactory;
import com.vedantu.async.AsyncTaskParams;
import com.vedantu.dinero.enums.InstalmentPurchaseEntity;
import com.vedantu.dinero.pojo.BaseInstalment;
import com.vedantu.dinero.pojo.OrderedItem;
import com.vedantu.dinero.pojo.Orders;
import com.vedantu.exception.VException;
import com.vedantu.exception.VExceptionFactory;
import com.vedantu.lms.cmds.enums.ContentState;
import com.vedantu.lms.cmds.pojo.ContentInfoResp;
import com.vedantu.lms.request.CMDSTestInfo;
import com.vedantu.lms.request.GetContentsReq;
import com.vedantu.onetofew.enums.CourseTerm;
import com.vedantu.onetofew.enums.EntityStatus;
import com.vedantu.onetofew.pojo.*;
import com.vedantu.onetofew.request.GetOTFBundlesReq;
import com.vedantu.session.pojo.EntityType;
import com.vedantu.subscription.pojo.BundleInfo;
import com.vedantu.subscription.pojo.BundlePackageDetailsInfo;
import com.vedantu.subscription.request.GTTAttendeeRequest;
import com.vedantu.subscription.request.GetBundleEnrolmentsReq;
import com.vedantu.subscription.request.GetBundlesReq;
import com.vedantu.subscription.request.GetCoursePlansReq;
import com.vedantu.subscription.response.BundleEnrolmentInfo;
import com.vedantu.subscription.response.CoursePlanBasicInfo;
import com.vedantu.util.*;
import com.vedantu.util.request.GetEnrollmentsReq;
import com.vedantu.vedantudata.async.AsyncTaskName;
import com.vedantu.vedantudata.dao.ActivityDao;
import com.vedantu.vedantudata.dao.EmailMappingLSDao;
import com.vedantu.vedantudata.dao.FreshdeskContactDao;
import com.vedantu.vedantudata.entities.ActivityDetails;
import com.vedantu.vedantudata.entities.EmailMappingLS;
import com.vedantu.vedantudata.entities.leadsquared.LeadsquaredUser;
import com.vedantu.vedantudata.enums.FreshdeskStatus;
import com.vedantu.vedantudata.enums.LeadsquaredActivityStatus;
import com.vedantu.vedantudata.managers.leadsquared.LeadsquaredManager;
import com.vedantu.vedantudata.pojos.AcadMentorStudents;
import com.vedantu.vedantudata.pojos.LeadsquaredActivityField;
import com.vedantu.vedantudata.pojos.StudentAccountManagerStudentsRes;
import com.vedantu.vedantudata.pojos.freshdesk.FreshdeskContact;
import com.vedantu.vedantudata.utils.ActivityEventConstants;
import com.vedantu.vedantudata.utils.FreshdeskConfig;
import com.vedantu.vedantudata.utils.LeadsquaredActivityHelper;
import com.vedantu.vedantudata.utils.PostgressHandler;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.Logger;
import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.lang.reflect.Type;
import java.security.GeneralSecurityException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.time.Instant;
import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
public class ActivityManager extends LeadsquaredActivityHelper {

    private static final String SCHEDULING_ENDPOINT = ConfigUtils.INSTANCE.getStringValue("SCHEDULING_ENDPOINT");
    private static final String USER_ENDPOINT = ConfigUtils.INSTANCE.getStringValue("USER_ENDPOINT");
    private static final String LMS_ENDPOINT = ConfigUtils.INSTANCE.getStringValue("LMS_ENDPOINT");
    private static final String SUBSCRIPTION_ENDPOINT = ConfigUtils.INSTANCE.getStringValue("SUBSCRIPTION_ENDPOINT");
    public static final Integer AM_DAILY_TRIGGER_LIMIT = ConfigUtils.INSTANCE.getIntValue("AM_DAILY_TRIGGER_LIMIT", 10);
    private final static Gson GSON = new Gson();
    private static final Function<Object, Long> PARSE_LONG = o -> {
        if (o != null) {
            if (o instanceof Integer || o instanceof Long) {
                return (long) o;
            }
            if (o instanceof Double) {
                return ((Double) o).longValue();
            }
            if (o instanceof Float) {
                return ((Float) o).longValue();
            }
            return Long.parseLong((o.toString()));
        } else {
            return null;
        }
    };

    @Autowired
    private LeadsquaredManager leadsquaredManager;

    @Autowired
    private MissedClassTriggerManager missedClassTriggerManager;

    @Autowired
    private ZeroAttendanceTriggerManager zeroAttendanceTriggerManager;

    @Autowired
    private TestTriggerManager testTriggerManager;

    @Autowired
    private EngagementTriggerManager engagementTriggerManager;

    @Autowired
    private ActivityDao activityDao;

    @Autowired
    private FreshdeskContactDao freshdeskContactDao;

    @Autowired
    private SamAllocationTriggerManager samAllocationTriggerManager;

    @Autowired
    private AsyncTaskFactory asyncTaskFactory;

    @Autowired
    private EmailMappingLSDao emailMappingLSDao;

    @Autowired
    private PostgressHandler postgressHandler;

    @Autowired
    private SubscriptionManagerHelper subscriptionManagerHelper;

    @Autowired
    private LogFactory logFactory;

    private Logger logger = LogFactory.getLogger(ActivityManager.class);

    private Map<String, Integer> AM_TRIGGER_COUNT = new HashMap<>();

    public void triggerActivitiesAsync(String activity) {
        Map<String, Object> payload = new HashMap<>();
        AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.TRIGGER_LS_AM_ACTIVITIES, payload);
        asyncTaskFactory.executeTask(params);
    }

    public void triggerSamAllocationActivitiesAsync() {
        Map<String, Object> payload = new HashMap<>();
        AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.TRIGGER_LS_CARE_ACTIVITIES_SAM_ALLOCATION, payload);
        asyncTaskFactory.executeTask(params);
    }

    public void triggerGrievanceActivityAsync() {
        Map<String, Object> payload = new HashMap<>();
        AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.TRIGGER_LS_GRIEVENCE_ACTIVITIES, payload);
        asyncTaskFactory.executeTask(params);
    }

    /*public void triggerActivities(String activity) throws VException, IOException, GeneralSecurityException, SQLException, ParseException {

        int startWith = 0;

        AM_TRIGGER_COUNT = new HashMap<>();

        logger.info("Starting the trigger execution for event : {}", activity);
        List<EnrollmentPojo> enrolmentInfos;
        do {
            enrolmentInfos = getEnrolmentInfos(startWith, ActivityEventConstants.BATCH_SIZE, System.currentTimeMillis() - (ActivityEventConstants.MILLI_MONTH * 12), System.currentTimeMillis());
            processBundleEnrollmentInfos(enrolmentInfos, activity);
            startWith += ActivityEventConstants.BATCH_SIZE;
        } while (enrolmentInfos.size() == ActivityEventConstants.BATCH_SIZE);

        triggerAllActivities();
    }

     public void triggerActivities(String activity, String email) throws VException, IOException, GeneralSecurityException, SQLException, ParseException {

        AM_TRIGGER_COUNT = new HashMap<>();

        Map<String, Integer> amPendingActivitiesCount = activityDao.getAcadMentorPendingActivityCount(Arrays.asList(email));


        int currentDayActivityCount = amPendingActivitiesCount.get(email) != null ? amPendingActivitiesCount.get(email) : 0;

        AM_TRIGGER_COUNT.put(email, currentDayActivityCount);

        AM_LIMIT = AM_DAILY_TRIGGER_LIMIT;

        processBundleEnrollmentInfosForAM(email, activity);

        logger.info("======Push to queue all leftout activities");
        triggerAllActivities();
    }*/

    public void triggerAMActivities() throws VException, IOException, GeneralSecurityException, SQLException, ParseException {

        AM_TRIGGER_COUNT = new HashMap<>();
        int startWith = 0;
        List<User> basicInfos;
        do {
            basicInfos = getAllAcadMentors(startWith, ActivityEventConstants.BATCH_SIZE);
            logger.info("======acadmentor records in batch is {}:", basicInfos.size());

            List<String> acadMentorEmailList = basicInfos.parallelStream()
                    .filter(user -> StringUtils.isNotEmpty(user.getEmail()))
                    .distinct()
                    .map(user -> user.getEmail()).collect(Collectors.toList());

            Map<String, Integer> amPendingActivitiesCount = activityDao.getAcadMentorPendingActivityCount(acadMentorEmailList);

            for (User userBasicInfo : basicInfos) {
                int currentDayActivityCount = amPendingActivitiesCount.get(userBasicInfo.getEmail()) != null ? amPendingActivitiesCount.get(userBasicInfo.getEmail()) : 0;

                AM_TRIGGER_COUNT.put(userBasicInfo.getEmail(), currentDayActivityCount);

                AM_LIMIT = AM_DAILY_TRIGGER_LIMIT;
                processAMTriggers(userBasicInfo);
            }

            startWith += ActivityEventConstants.BATCH_SIZE;
        } while (basicInfos.size() == ActivityEventConstants.BATCH_SIZE);

        logger.info("======Push to queue all leftout activities");
        triggerAllActivities();
    }

    private void processAMTriggers(User userBasicInfo) throws VException, IOException, GeneralSecurityException, SQLException, ParseException {

        if (StringUtils.isEmpty(userBasicInfo.getEmail())) {
            return;
        }

        logger.info("======Triggering activities for the AM : {} ", userBasicInfo.getEmail());

        List<User> allEnrolledUsers = getStudentsByacadmentorEmail(userBasicInfo.getEmail());

        logger.info("======fetched total {} enrolled users records", allEnrolledUsers.size());

        if (allEnrolledUsers.isEmpty())
            return;

        List<Long> studentIds = allEnrolledUsers.parallelStream()
                .map(User::getId)
                .distinct()
                .collect(Collectors.toList());

        Set<Long> activeLongTermStudentIds = getActiveStudentsIds(studentIds);

        List<String> nonLongTermStudentIds = studentIds.parallelStream().filter(studentId -> !activeLongTermStudentIds.contains(studentId)).map(studentId->studentId.toString()).collect(Collectors.toList());
        List<Long> activeEarlyEducationStudentIds = getActiveEarlyEducationStudentsIds(nonLongTermStudentIds);

        logger.info("\n\nactiveUserIds : {}", activeLongTermStudentIds);
        List<User> activeEnrolledUsers = allEnrolledUsers.parallelStream().filter(user -> activeLongTermStudentIds.contains(user.getId())).collect(Collectors.toList());
        logger.info("======total active users records  :: {}", activeLongTermStudentIds);

        logger.info("\n\nactiveEarlyEducationStudentIds : {}", activeEarlyEducationStudentIds);
        List<User> activeEarlyEducationStudents = allEnrolledUsers.parallelStream().filter(user -> activeEarlyEducationStudentIds.contains(user.getId())).collect(Collectors.toList());
        logger.info("======total active early education users records  :: {}", activeEarlyEducationStudentIds);

        if (activeEnrolledUsers.isEmpty() && activeEarlyEducationStudents.isEmpty())
            return;

        //TODO:remove mapping in future, pass only AM object directly
        Map<Long, User> studentAmMapping = new HashMap<>();
        for (Long studentId : activeLongTermStudentIds)
            studentAmMapping.put(studentId, userBasicInfo);

        List<User> firstChunk = activeEnrolledUsers;
        List<User> secondChunk = new ArrayList<>();

        if (activeEnrolledUsers.size() > 50) {
            firstChunk = activeEnrolledUsers.subList(0, activeEnrolledUsers.size() / 2 - 1);
            secondChunk = activeEnrolledUsers.subList(activeEnrolledUsers.size() / 2, activeEnrolledUsers.size() - 1);
        }


        {//Zero attendance
            //first chunk
            // TODO PARTITION
            try {
                logger.info("======Checking zero attendance activities for the AM : {} ", userBasicInfo.getEmail());
                zeroAttendanceTriggerManager.process(firstChunk, studentAmMapping);
            } catch (Exception e) {
                logger.error("Error processing first chunk of ZERO-ATTENDANCE TRIGGER FOR AM::{}", userBasicInfo.getEmail(), e);
            }

            //second chunk
            try {
                logger.info("======Checking zero attendance activities for the AM : {} ", userBasicInfo.getEmail());
                zeroAttendanceTriggerManager.process(secondChunk, studentAmMapping);
            } catch (Exception e) {
                logger.error("Error processing second chunk of ZERO-ATTENDANCE TRIGGER FOR AM::{}", userBasicInfo.getEmail(), e);
            }
        }

        //Check if am daily limit exceeded
        int count = AM_TRIGGER_COUNT.get(userBasicInfo.getEmail()) == null ? 0 : AM_TRIGGER_COUNT.get(userBasicInfo.getEmail());
        if (count >= AM_LIMIT) {
            return;
        }

        {//Missed class
            //first chunk
            try {
                logger.info("======Checking missed class activities for the AM : {} ", userBasicInfo.getEmail());
                missedClassTriggerManager.process(firstChunk, studentAmMapping);
            } catch (Exception e) {
                logger.error("Error processing first chunk of MISSED-CLASS TRIGGER FOR AM::{}", userBasicInfo.getEmail(), e);
            }
            //second chunk
            try {
                logger.info("======Checking missed class activities for the AM : {} ", userBasicInfo.getEmail());
                missedClassTriggerManager.process(secondChunk, studentAmMapping);
            } catch (Exception e) {
                logger.error("Error processing second chunk of MISSED-CLASS TRIGGER FOR AM::{}", userBasicInfo.getEmail(), e);
            }

            //early education
            try {
                logger.info("======Checking missed class activities for the AM : {} ", userBasicInfo.getEmail());
                missedClassTriggerManager.processEarlyEducation(activeEarlyEducationStudents, studentAmMapping);
            } catch (Exception e) {
                logger.error("Error processing activeEarlyEducationStudents chunk of MISSED-CLASS TRIGGER FOR AM::{}", userBasicInfo.getEmail(), e);
            }

        }

        //Check if am daily limit exceeded
        count = AM_TRIGGER_COUNT.get(userBasicInfo.getEmail()) == null ? 0 : AM_TRIGGER_COUNT.get(userBasicInfo.getEmail());
        if (count >= AM_LIMIT) {
            return;
        }

        {
            //first chunk
            try {
                logger.info("======Checking test activities for the AM : {} ", userBasicInfo.getEmail());
                testTriggerManager.process(firstChunk, studentAmMapping);
            } catch (Exception e) {
                logger.error("Error processing first chunk of TEST TRIGGER FOR AM::{}", userBasicInfo.getEmail(), e);
            }
            //second chunk
            try {
                logger.info("======Checking test activities for the AM : {} ", userBasicInfo.getEmail());
                testTriggerManager.process(secondChunk, studentAmMapping);
            } catch (Exception e) {
                logger.error("Error processing second chunk of TEST TRIGGER FOR AM::{}", userBasicInfo.getEmail(), e);
            }
        }

        //Check if am daily limit exceeded
        count = AM_TRIGGER_COUNT.get(userBasicInfo.getEmail()) == null ? 0 : AM_TRIGGER_COUNT.get(userBasicInfo.getEmail());
        if (count >= AM_LIMIT) {
            return;
        }

        {//engagement
            //first chunk
            try {
                logger.info("======Checking engagement activities for the AM : {} ", userBasicInfo.getEmail());
                engagementTriggerManager.process(firstChunk, studentAmMapping);
            } catch (Exception e) {
                logger.error("Error processing first chunk of ENGAGEMENT TRIGGER FOR AM::{}", userBasicInfo.getEmail(), e);
            }
            //second chunk
            try {
                logger.info("======Checking engagement activities for the AM : {} ", userBasicInfo.getEmail());
                engagementTriggerManager.process(secondChunk, studentAmMapping);
            } catch (Exception e) {
                logger.error("Error processing second chunk of ENGAGEMENT TRIGGER FOR AM::{}", userBasicInfo.getEmail(), e);
            }

            //early education
            try {
                logger.info("======Checking YODA engagement activities for the AM : {} ", userBasicInfo.getEmail());
                engagementTriggerManager.processEarlyEducation(activeEarlyEducationStudents, studentAmMapping);
            } catch (Exception e) {
                logger.error("Error processing second chunk of YODA ENGAGEMENT TRIGGER FOR AM::{}", userBasicInfo.getEmail(), e);
            }
        }
    }

    private Set<Long> getActiveStudentsIds(List<Long> studentIds) throws SQLException {

        String studentIdsString = studentIds.toString().replace("[", "(").replace("]", ")");

        String query = "select en.userid " +
                "from enrollment en " +
                "join longtermenrolments le ON en.id=le.enrolmentid " +
                "where userid IN " + studentIdsString +
                "AND en.role = 'STUDENT' " +
                "AND en.status = 'ACTIVE' " +
                "AND en.batchendtime > getdate();";

        ResultSet rs = postgressHandler.executeQuery(query);

        Set<Long> activeUserIdList = new HashSet<>();

        if (rs != null) {
            while (rs.next()) {
                String userid = rs.getString("userid");
                CharSequence V = "V";
                CharSequence v = "v";
                if(userid.contains(V) || userid.contains(v)){
                    userid = userid.substring(2);
                }

                if (StringUtils.isNotEmpty(userid)) {
                    try {
                        Long userIdLong = Long.parseLong(userid);
                        activeUserIdList.add(userIdLong);
                    } catch (Exception e) {
                        //swallow
                    }
                }
            }
        }

        return activeUserIdList;
    }

    private List<Long> getActiveEarlyEducationStudentsIds(List<String> studentIds) throws SQLException, VException {

        GetBundleEnrolmentsReq getBundleEnrolmentsReq = new GetBundleEnrolmentsReq();
        getBundleEnrolmentsReq.setUserIdList(studentIds);

        List<EntityStatus> entityStatuses = new ArrayList<>();
        entityStatuses.add(EntityStatus.ACTIVE);
        //entityStatuses.add(EntityStatus.INACTIVE);
        getBundleEnrolmentsReq.setStatusList(entityStatuses);

        ClientResponse bundleEnollmentResponse = WebUtils.INSTANCE.doCall(SUBSCRIPTION_ENDPOINT + "/bundle/getActiveBundleEnrolmentsForUsers", HttpMethod.POST,
                new Gson().toJson(getBundleEnrolmentsReq), true);
        VExceptionFactory.INSTANCE.parseAndThrowException(bundleEnollmentResponse);
        String bundleEnrollmentResponseString = bundleEnollmentResponse.getEntity(String.class);

        Type bundleEnrollmentListType = new TypeToken<ArrayList<BundleEnrolmentInfo>>() {
        }.getType();

        List<BundleEnrolmentInfo> bundleEnrolmentInfos = new Gson().fromJson(bundleEnrollmentResponseString, bundleEnrollmentListType);

        logger.info("======== fetched bundle enrollments for the students : {}", bundleEnrolmentInfos);

        List<String> bundleIdList = bundleEnrolmentInfos.parallelStream()
                .filter(bundleEnrolmentInfo -> bundleEnrolmentInfo.getBundleId() != null)
                .map(bundleEnrolmentInfo -> bundleEnrolmentInfo.getBundleId())
                .distinct()
                .collect(Collectors.toList());
        GetBundlesReq getBundlesReq = new GetBundlesReq();
        getBundlesReq.setBundleIds(bundleIdList);

        ClientResponse bundleResponse = WebUtils.INSTANCE.doCall(SUBSCRIPTION_ENDPOINT + "/bundle/getBundles", HttpMethod.POST,
                new Gson().toJson(getBundlesReq), true);
        VExceptionFactory.INSTANCE.parseAndThrowException(bundleResponse);
        String bundleResponseString = bundleResponse.getEntity(String.class);

        Type bundleResponseListType = new TypeToken<ArrayList<BundleInfo>>() {
        }.getType();
        List<BundleInfo> bundleInfos = new Gson().fromJson(bundleResponseString, bundleResponseListType);

        List<String> earlyEducationBundle = bundleInfos.parallelStream()
                .filter(bundleInfo -> bundleInfo.getSearchTerms()!=null)
                .filter(bundleInfo -> bundleInfo.getSearchTerms().contains(CourseTerm.YODA))
                .map(bundleInfo -> bundleInfo.getId())
                .collect(Collectors.toList());

        logger.info("======== fetched bundle early education : {}", earlyEducationBundle);

        List<Long> activeUserIdList = bundleEnrolmentInfos.parallelStream()
                .filter(bundleEnrolmentInfo -> bundleEnrolmentInfo.getBundleId()!=null)
                .filter(bundleEnrolmentInfo -> earlyEducationBundle.contains(bundleEnrolmentInfo.getBundleId()))
                .map(bundleEnrolmentInfo -> Long.parseLong(bundleEnrolmentInfo.getUserId()))
                .distinct()
                .collect(Collectors.toList());

        logger.info("======== filtered yoda students : {}", activeUserIdList);
        return activeUserIdList;
    }


    /*private void processBundleEnrollmentInfosForAM(String email, String activity) throws VException, IOException, GeneralSecurityException, SQLException, ParseException {

        //Active and inactive users
        List<User> enrolledUsers = getStudentsByacadmentorEmail(email);

        logger.info("======fetched total {} enrolled users records", enrolledUsers.size());

        List<Long> studentIds = enrolledUsers.parallelStream()
                .map(User::getId)
                .distinct()
                .collect(Collectors.toList());

        logger.info("\n\nactiveUserIds : {}", studentIds);

        //keeping this call for verifying the call
        List<AcadMentorStudents> acadMentorsList = getAcadMentors(studentIds);

        logger.info("\n\nacadMentorsList : {}", acadMentorsList.size());

        List<Long> AMIds = acadMentorsList.parallelStream().map(AcadMentorStudents::getAcadMentorId).collect(Collectors.toList());
        logger.info("\n\nAMIds : {}", AMIds);

        List<User> amList = getUsers(AMIds);

        //Map curresponding student to AM
        Map<Long, User> studentAmMapping = new HashMap<>();
        for (User student : enrolledUsers) {

            Optional<AcadMentorStudents> acadMentorStudentsOptional = acadMentorsList.parallelStream().filter(val -> val.getStudentId().equals(student.getId())).findFirst();
            if (acadMentorStudentsOptional.isPresent()) {
                AcadMentorStudents acadMentorStudents = acadMentorStudentsOptional.get();

                Optional<User> amOptional = amList.parallelStream().filter(val -> val.getId().equals(acadMentorStudents.getAcadMentorId())).findFirst();

                if (amOptional.isPresent()) {
                    studentAmMapping.put(student.getId(), amOptional.get());
                }
            }
        }

        logger.info("======executing activity : {}", activity);

        if (ActivityEventConstants.ACTIVITY_ALL_AM_TRIGGERS.equalsIgnoreCase(activity)) {
            zeroAttendanceTriggerManager.process(enrolledUsers, studentAmMapping);
            missedClassTriggerManager.process(enrolledUsers, studentAmMapping);
            testTriggerManager.process(enrolledUsers, studentAmMapping);
            engagementTriggerManager.process(enrolledUsers, studentAmMapping);
        } else {
            if (ActivityEventConstants.ACTIVITY_MISSED_CLASS.equalsIgnoreCase(activity)) {
                missedClassTriggerManager.process(enrolledUsers, studentAmMapping);
            }

            if (ActivityEventConstants.ACTIVITY_ZERO_ATTENDANCE.equalsIgnoreCase(activity)) {
                zeroAttendanceTriggerManager.process(enrolledUsers, studentAmMapping);
            }

            if (ActivityEventConstants.ACTIVITY_TEST.equalsIgnoreCase(activity)) {
                testTriggerManager.process(enrolledUsers, studentAmMapping);
            }

            if (ActivityEventConstants.ACTIVITY_ENGAGEMENT.equalsIgnoreCase(activity)) {
                engagementTriggerManager.process(enrolledUsers, studentAmMapping);
            }
        }

    }*/

    private List<EnrollmentPojo> getEnrolmentInfos(int startWith, int batchSize, Long fromTime, Long thruTime) throws VException {
        GetEnrollmentsReq getEnrollmentsReq = new GetEnrollmentsReq();

        getEnrollmentsReq.setStart(startWith);

        if (batchSize > 0) {
            getEnrollmentsReq.setSize(batchSize);
        }

        getEnrollmentsReq.setFromTime(fromTime);
        getEnrollmentsReq.setTillTime(thruTime);
        getEnrollmentsReq.setRole(Role.STUDENT);

        List<EntityStatus> entityStatuses = new ArrayList<>();
        entityStatuses.add(EntityStatus.ACTIVE);
        entityStatuses.add(EntityStatus.INACTIVE);
        getEnrollmentsReq.setStatuses(entityStatuses);

        ClientResponse resp = WebUtils.INSTANCE.doCall(SUBSCRIPTION_ENDPOINT + "/enroll/getActiveAndInactiveEnrollments", HttpMethod.POST,
                new Gson().toJson(getEnrollmentsReq), true);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);

        Type listType = new TypeToken<ArrayList<EnrollmentPojo>>() {
        }.getType();

        return new Gson().fromJson(jsonString, listType);
    }

    public List<CoursePlanBasicInfo> getAllOtoRecords(Long fromTime, Long thruTime) throws VException {

        GetCoursePlansReq getCoursePlansReq = new GetCoursePlansReq();

        getCoursePlansReq.setFromStartDate(fromTime);
        getCoursePlansReq.setTillStartDate(thruTime);

        ClientResponse resp = WebUtils.INSTANCE.doCall(SUBSCRIPTION_ENDPOINT + "/dashboard/getStudentsCoursePlanInfos", HttpMethod.POST,
                new Gson().toJson(getCoursePlansReq), true);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);

        Type listType = new TypeToken<ArrayList<CoursePlanBasicInfo>>() {
        }.getType();

        return new Gson().fromJson(jsonString, listType);
    }

    @Deprecated
    private void processBundleEnrollmentInfos(List<EnrollmentPojo> enrolmentInfos, String activity) throws VException, IOException, GeneralSecurityException, SQLException, ParseException {

        if (enrolmentInfos == null || enrolmentInfos.isEmpty())
            return;

        List<Long> studentIds = enrolmentInfos.parallelStream()
                .filter(enrolmentInfo -> enrolmentInfo.getUserId() != null)
                .filter(enrolmentInfo -> {
                    try {
                        Long.parseLong(enrolmentInfo.getUserId());
                        return true;
                    } catch (Exception e) {
                        return false;
                    }
                })
                .map(enrolmentInfo -> Long.parseLong(enrolmentInfo.getUserId()))
                .distinct()
                .collect(Collectors.toList());

        //Active and inactive users
        List<User> enrolledUsers = getUsers(studentIds);

        logger.info("\n\nactiveUserIds : {}", studentIds);

        List<AcadMentorStudents> acadMentorsList = getAcadMentors(studentIds);

        logger.info("\n\nacadMentorsList : {}", acadMentorsList.size());

        List<Long> AMIds = acadMentorsList.parallelStream().map(AcadMentorStudents::getAcadMentorId).collect(Collectors.toList());
        logger.info("\n\nAMIds : {}", AMIds);

        List<User> amList = getUsers(AMIds);

        //Map curresponding student to AM
        Map<Long, User> studentAmMapping = new HashMap<>();
        for (User student : enrolledUsers) {

            Optional<AcadMentorStudents> acadMentorStudentsOptional = acadMentorsList.parallelStream().filter(val -> val.getStudentId().equals(student.getId())).findFirst();
            if (acadMentorStudentsOptional.isPresent()) {
                AcadMentorStudents acadMentorStudents = acadMentorStudentsOptional.get();

                Optional<User> amOptional = amList.parallelStream().filter(val -> val.getId().equals(acadMentorStudents.getAcadMentorId())).findFirst();

                if (amOptional.isPresent()) {
                    studentAmMapping.put(student.getId(), amOptional.get());
                }
            }
        }

        if (ActivityEventConstants.ACTIVITY_ALL_AM_TRIGGERS.equalsIgnoreCase(activity)) {
            zeroAttendanceTriggerManager.process(enrolledUsers, studentAmMapping);
            missedClassTriggerManager.process(enrolledUsers, studentAmMapping);
            testTriggerManager.process(enrolledUsers, studentAmMapping);
            engagementTriggerManager.process(enrolledUsers, studentAmMapping);
        } else {
            if (ActivityEventConstants.ACTIVITY_MISSED_CLASS.equalsIgnoreCase(activity)) {
                missedClassTriggerManager.process(enrolledUsers, studentAmMapping);
            }

            if (ActivityEventConstants.ACTIVITY_ZERO_ATTENDANCE.equalsIgnoreCase(activity)) {
                zeroAttendanceTriggerManager.process(enrolledUsers, studentAmMapping);
            }

            if (ActivityEventConstants.ACTIVITY_TEST.equalsIgnoreCase(activity)) {
                testTriggerManager.process(enrolledUsers, studentAmMapping);
            }

            if (ActivityEventConstants.ACTIVITY_ENGAGEMENT.equalsIgnoreCase(activity)) {
                engagementTriggerManager.process(enrolledUsers, studentAmMapping);
            }
        }

        if (ActivityEventConstants.ACTIVITY_SAM_ALLOCATION.equalsIgnoreCase(activity)) {

            List<StudentAccountManagerStudentsRes> samStudentList = getSamByStudents(studentIds);

            List<Long> samids = samStudentList.parallelStream()
                    .map(StudentAccountManagerStudentsRes::getStudentAccountManagerId)
                    .distinct()
                    .collect(Collectors.toList());
            logger.info("\n\nAMIds : {}", samids);

            List<User> samList = getUsers(samids);

            //Map curresponding student to AM
            Map<Long, User> studentSamMapping = new HashMap<>();
            for (User student : enrolledUsers) {

                Optional<StudentAccountManagerStudentsRes> samStudentsOptional = samStudentList.parallelStream().filter(val -> val.getStudentId().equals(student.getId())).findFirst();
                if (samStudentsOptional.isPresent()) {
                    StudentAccountManagerStudentsRes studentAccountManagerStudentsRes = samStudentsOptional.get();

                    Optional<User> samOptional = samList.parallelStream().filter(val -> val.getId().equals(studentAccountManagerStudentsRes.getStudentAccountManagerId())).findFirst();

                    if (samOptional.isPresent()) {
                        studentSamMapping.put(student.getId(), samOptional.get());
                    }
                }
            }

            samAllocationTriggerManager.process(enrolledUsers, studentSamMapping, enrolmentInfos);
        }

    }

    private List<StudentAccountManagerStudentsRes> getSamByStudents(List<Long> studentIds) throws VException {

        ClientResponse resp = WebUtils.INSTANCE.doCall(SUBSCRIPTION_ENDPOINT + "/studentaccountmanagerstudents/getSAMByStudentsId", HttpMethod.POST,
                new Gson().toJson(studentIds), true);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);

        Type listType = new TypeToken<ArrayList<StudentAccountManagerStudentsRes>>() {
        }.getType();
        List<StudentAccountManagerStudentsRes> samList = new Gson().fromJson(jsonString, listType);
        return samList;
    }

    public List<User> getUsers(List<Long> userIds) throws VException {

        ClientResponse resp = WebUtils.INSTANCE.doCall(USER_ENDPOINT + "/getUsersById?exposeEmail=true", HttpMethod.POST,
                new Gson().toJson(userIds), true);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);

        Type listType = new TypeToken<ArrayList<User>>() {
        }.getType();
        List<User> userList = new Gson().fromJson(jsonString, listType);

        return userList;
    }

/*    public User getUserByEmail(String email) throws VException {

        ClientResponse resp = WebUtils.INSTANCE.doCall(USER_ENDPOINT + "/getUserByEmail?email=" + email, HttpMethod.GET,
                null, true);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);

        User user = new Gson().fromJson(jsonString, User.class);

        return user;
    }*/

    public List<User> getAllAcadMentors(int start, int size) throws VException {

        ClientResponse resp = WebUtils.INSTANCE.doCall(USER_ENDPOINT +
                        "/getAllAcadMentorsUserObject?start=" + start + "&size=" + size, HttpMethod.GET,
                null, true);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        Type listType = new TypeToken<ArrayList<User>>() {
        }.getType();
        List<User> userList = new Gson().fromJson(jsonString, listType);

        return userList;
    }

    public List<AcadMentorStudents> getAcadMentors(List<Long> userIdList) throws VException {

        ClientResponse resp = WebUtils.INSTANCE.doCall(SUBSCRIPTION_ENDPOINT + "/acadmentorstudents/getAcaMentorsForStudents", HttpMethod.POST,
                new Gson().toJson(userIdList), true);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);

        Type listType = new TypeToken<ArrayList<AcadMentorStudents>>() {
        }.getType();
        List<AcadMentorStudents> acadMentorStudentsList = new Gson().fromJson(jsonString, listType);
        return acadMentorStudentsList;

    }

    public List<User> getStudentsByacadmentorEmail(String email) throws VException {

        ClientResponse resp = WebUtils.INSTANCE.doCall(SUBSCRIPTION_ENDPOINT + "/acadmentorstudents/" + email + "/getStudents", HttpMethod.GET,
                null, true);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);

        Type listType = new TypeToken<ArrayList<User>>() {
        }.getType();
        List<User> studentsList = new Gson().fromJson(jsonString, listType);
        return studentsList;

    }

    public void pushActivity(User user, Map<Long, User> studentAMMapping, int eventCode, Map<String, String> params) {

        if (user == null)
            return;

        User AM = studentAMMapping != null && user != null ? studentAMMapping.get(user.getId()) : null;

        if (AM == null) {
            return;
        }

        String ownerEmail;
        if (params.containsKey(ActivityEventConstants.OWNER)) {
            ownerEmail = params.get(ActivityEventConstants.OWNER);
        } else {
            ownerEmail = AM.getEmail();
        }

        if (AM_TRIGGER_COUNT.get(ownerEmail) == null) {
            AM_TRIGGER_COUNT.put(ownerEmail, 0);
        } else {
            int count = AM_TRIGGER_COUNT.get(ownerEmail) == null ? 0 : AM_TRIGGER_COUNT.get(ownerEmail);
            if (count >= AM_LIMIT) {
                logger.info("======ignoring the trigger since the AM reached the daily quota,,,", ownerEmail);
                return;
            }
        }

        ActivityDetails previousActivityDetails = activityDao.getLastActivity(user.getId(), eventCode);

        if (previousActivityDetails != null && !isClosedActivity(previousActivityDetails)) {
            logger.info("======checking if any previous activity open in state:: OPEN");
            return;
        }

        logger.info("======checking if any previous activity open in state:: CLOSED");

        switch (eventCode) {
            case ActivityEventConstants.EVENT_MISSED_CLASS:
            case ActivityEventConstants.EVENT_ZERO_ATTENDANCE: {
                if (previousActivityDetails != null && System.currentTimeMillis() - previousActivityDetails.getCreationTime() < ActivityEventConstants.MILLI_WEEK) {
                    return;
                }
                break;
            }
            case ActivityEventConstants.EVENT_PARENT_ENGAGEMENT:{
                if (previousActivityDetails != null && System.currentTimeMillis() - previousActivityDetails.getCreationTime() < ActivityEventConstants.MILLI_MONTH) {
                    return;
                }
                break;
            }
            case ActivityEventConstants.EVENT_STUDENT_ENGAGEMENT:{
                if (previousActivityDetails != null && System.currentTimeMillis() - previousActivityDetails.getCreationTime() < ActivityEventConstants.MILLI_DAY * 15) {
                    return;
                }
                break;
            }

        }


        String lsOwnerEmail = getLsOwnerEmail(ownerEmail);

        if (lsOwnerEmail == null) {
            logger.warn("did not find the LSowneremail mapping for : " + ownerEmail);
            return;
        }

        List<LeadsquaredUser> owners = null;
        try {
            //TODO get the owner details from redshift
            owners = leadsquaredManager.fetchAgentsByEmailId(lsOwnerEmail);
        } catch (Exception e) {

        }

        LeadsquaredUser owner;
        if (owners != null && !owners.isEmpty()) {
            owner = owners.get(0);
        } else {
            logger.warn("did not find the owner LS account for owneremail " + lsOwnerEmail);
            return;
        }

        ActivityDetails activityDetails = new ActivityDetails();
        activityDetails.setActivityEvent(eventCode);
        activityDetails.setLeadVedantuId(user != null ? user.getId() : null);
        activityDetails.setOwnerVedantuId(AM != null ? AM.getId() : null);
        activityDetails.setActivityNote(params.get(ActivityEventConstants.NOTE));

        if (params.containsKey(ActivityEventConstants.EMAIL_ADDRESS)) {
            activityDetails.setEmailAddress(params.get(ActivityEventConstants.EMAIL_ADDRESS));
        } else {
            activityDetails.setEmailAddress(user.getEmail());
        }

        if(StringUtils.isNotEmpty(user.getContactNumber())){
            String phone = cleanPhoneNumber(user.getContactNumber());
            activityDetails.setPhone(phone);
        }

        activityDetails.setActivityOwnerEmail(lsOwnerEmail);

        LeadsquaredActivityField leadsquaredActivityField;
        if (ActivityEventConstants.EVENT_BANK_REFUND != eventCode
                && ActivityEventConstants.EVENT_SAM_ALLOCATION != eventCode
                && ActivityEventConstants.EVENT_INSTALLMENT_REMINDER != eventCode) {
            leadsquaredActivityField = new LeadsquaredActivityField();
            leadsquaredActivityField.setSchemaName(ActivityEventConstants.STATUS);
            leadsquaredActivityField.setValue(LeadsquaredActivityStatus.Pending.name());
            activityDetails.addField(leadsquaredActivityField);
        } else {
            leadsquaredActivityField = new LeadsquaredActivityField();
            leadsquaredActivityField.setSchemaName(ActivityEventConstants.STATUS);
            leadsquaredActivityField.setValue(LeadsquaredActivityStatus.Active.name());
            activityDetails.addField(leadsquaredActivityField);
        }

        leadsquaredActivityField = new LeadsquaredActivityField();
        leadsquaredActivityField.setSchemaName(ActivityEventConstants.OWNER);
        leadsquaredActivityField.setValue(owner != null ? owner.getUserId() : "");
        activityDetails.addField(leadsquaredActivityField);

        switch (eventCode) {
            case ActivityEventConstants.EVENT_MISSED_CLASS: {

                leadsquaredActivityField = new LeadsquaredActivityField();
                leadsquaredActivityField.setSchemaName(ActivityEventConstants.MX_CUSTOM_1);
                leadsquaredActivityField.setValue(params.get(ActivityEventConstants.GRADE_VALUE));
                activityDetails.addField(leadsquaredActivityField);
                break;
            }
            case ActivityEventConstants.EVENT_ZERO_ATTENDANCE: {

                leadsquaredActivityField = new LeadsquaredActivityField();
                leadsquaredActivityField.setSchemaName(ActivityEventConstants.MX_CUSTOM_1);
                leadsquaredActivityField.setValue(params.get(ActivityEventConstants.STUDENT_TYPE));
                activityDetails.addField(leadsquaredActivityField);
                break;
            }
            case ActivityEventConstants.EVENT_TEST: {

                if (params.containsKey(ActivityEventConstants.MISSED_TEST)) {
                    leadsquaredActivityField = new LeadsquaredActivityField();
                    leadsquaredActivityField.setSchemaName(ActivityEventConstants.MX_CUSTOM_1);
                    leadsquaredActivityField.setValue(params.get(ActivityEventConstants.MISSED_TEST));
                    activityDetails.addField(leadsquaredActivityField);
                }

                if (params.containsKey(ActivityEventConstants.TEST_LEVEL)) {
                    leadsquaredActivityField = new LeadsquaredActivityField();
                    leadsquaredActivityField.setSchemaName(ActivityEventConstants.MX_CUSTOM_2);
                    leadsquaredActivityField.setValue(params.get(ActivityEventConstants.TEST_LEVEL));
                    activityDetails.addField(leadsquaredActivityField);
                }

                if (params.containsKey(ActivityEventConstants.LAST_SCORE)) {
                    leadsquaredActivityField = new LeadsquaredActivityField();
                    leadsquaredActivityField.setSchemaName(ActivityEventConstants.MX_CUSTOM_9);
                    leadsquaredActivityField.setValue(params.get(ActivityEventConstants.LAST_SCORE));
                    activityDetails.addField(leadsquaredActivityField);
                }
                break;
            }
            case ActivityEventConstants.EVENT_STUDENT_ENGAGEMENT:
            case ActivityEventConstants.EVENT_PARENT_ENGAGEMENT: {

                leadsquaredActivityField = new LeadsquaredActivityField();
                leadsquaredActivityField.setSchemaName(ActivityEventConstants.MX_CUSTOM_7);
                leadsquaredActivityField.setValue(params.get(ActivityEventConstants.NO_OF_DAYS));
                activityDetails.addField(leadsquaredActivityField);
                break;
            }

            case ActivityEventConstants.EVENT_SAM_ALLOCATION: {

                leadsquaredActivityField = new LeadsquaredActivityField();
                leadsquaredActivityField.setSchemaName(ActivityEventConstants.MX_CUSTOM_1);
                leadsquaredActivityField.setValue(params.get(ActivityEventConstants.MX_CUSTOM_1));
                activityDetails.addField(leadsquaredActivityField);

                leadsquaredActivityField = new LeadsquaredActivityField();
                leadsquaredActivityField.setSchemaName(ActivityEventConstants.MX_CUSTOM_2);
                leadsquaredActivityField.setValue(params.get(ActivityEventConstants.MX_CUSTOM_2));
                activityDetails.addField(leadsquaredActivityField);

                leadsquaredActivityField = new LeadsquaredActivityField();
                leadsquaredActivityField.setSchemaName(ActivityEventConstants.MX_CUSTOM_3);
                leadsquaredActivityField.setValue(params.get(ActivityEventConstants.MX_CUSTOM_3));
                activityDetails.addField(leadsquaredActivityField);

                leadsquaredActivityField = new LeadsquaredActivityField();
                leadsquaredActivityField.setSchemaName(ActivityEventConstants.MX_CUSTOM_4);
                leadsquaredActivityField.setValue(params.get(ActivityEventConstants.MX_CUSTOM_4));
                activityDetails.addField(leadsquaredActivityField);

                leadsquaredActivityField = new LeadsquaredActivityField();
                leadsquaredActivityField.setSchemaName(ActivityEventConstants.MX_CUSTOM_5);
                leadsquaredActivityField.setValue(params.get(ActivityEventConstants.MX_CUSTOM_5));
                activityDetails.addField(leadsquaredActivityField);

                leadsquaredActivityField = new LeadsquaredActivityField();
                leadsquaredActivityField.setSchemaName(ActivityEventConstants.MX_SAM_OWNER);
                leadsquaredActivityField.setValue(params.get(ActivityEventConstants.MX_SAM_OWNER));
                activityDetails.addField(leadsquaredActivityField);
                break;
            }
            case ActivityEventConstants.EVENT_BANK_REFUND: {

                leadsquaredActivityField = new LeadsquaredActivityField();
                leadsquaredActivityField.setSchemaName(ActivityEventConstants.MX_CUSTOM_1);
                leadsquaredActivityField.setValue(params.get(ActivityEventConstants.MX_CUSTOM_1));
                activityDetails.addField(leadsquaredActivityField);

                activityDetails.setEmailAddress(params.get(ActivityEventConstants.EMAIL_ADDRESS));
                break;
            }
        }

        logger.info("======Pushing the activity to the list for event : {}", eventCode);

        int count = AM_TRIGGER_COUNT.get(ownerEmail) == null ? 0 : AM_TRIGGER_COUNT.get(ownerEmail);
        AM_TRIGGER_COUNT.put(ownerEmail, count + 1);

        queueActivityToPush(activityDetails);

    }

    public void pushCareActivity(int eventCode, Map<String, String> params) {

        String lsOwnerEmail = params.get(ActivityEventConstants.OWNER);

        String userEmail = params.get(ActivityEventConstants.EMAIL_ADDRESS);

        ActivityDetails previousActivityDetails = activityDao.getLastActivity(userEmail, eventCode);

        if (previousActivityDetails != null && !isClosedActivity(previousActivityDetails)) {
            logger.info("======checking if any previous activity open in state:: OPEN");
            return;
        }

        logger.info("======checking if any previous activity open in state:: CLOSED");

        if (previousActivityDetails != null && System.currentTimeMillis() - previousActivityDetails.getCreationTime() < ActivityEventConstants.MILLI_MONTH) {
            return;
        }

        List<LeadsquaredUser> owners = null;
        try {
            owners = leadsquaredManager.fetchAgentsByEmailId(lsOwnerEmail);
        } catch (Exception e) {

        }

        LeadsquaredUser owner;
        if (owners != null && !owners.isEmpty()) {
            owner = owners.get(0);
        } else {
            logger.warn("did not find the owner LS account for owneremail " + lsOwnerEmail);
            return;
        }

        ActivityDetails activityDetails = new ActivityDetails();
        activityDetails.setActivityEvent(eventCode);
        activityDetails.setActivityNote(params.get(ActivityEventConstants.NOTE));

        activityDetails.setActivityOwnerEmail(lsOwnerEmail);

        LeadsquaredActivityField leadsquaredActivityField = new LeadsquaredActivityField();
        leadsquaredActivityField.setSchemaName(ActivityEventConstants.STATUS);
        if (ActivityEventConstants.EVENT_INSTALLMENT_REMINDER == eventCode) {
            leadsquaredActivityField.setValue(LeadsquaredActivityStatus.Open.name());
        } else {
            leadsquaredActivityField.setValue(LeadsquaredActivityStatus.Active.name());
        }
        activityDetails.addField(leadsquaredActivityField);


        leadsquaredActivityField = new LeadsquaredActivityField();
        leadsquaredActivityField.setSchemaName(ActivityEventConstants.OWNER);
        leadsquaredActivityField.setValue(owner != null ? owner.getUserId() : "");
        activityDetails.addField(leadsquaredActivityField);

        switch (eventCode) {
            case ActivityEventConstants.EVENT_BANK_REFUND: {

                leadsquaredActivityField = new LeadsquaredActivityField();
                leadsquaredActivityField.setSchemaName(ActivityEventConstants.MX_CUSTOM_1);
                leadsquaredActivityField.setValue(params.get(ActivityEventConstants.MX_CUSTOM_1));
                activityDetails.addField(leadsquaredActivityField);

                activityDetails.setEmailAddress(params.get(ActivityEventConstants.EMAIL_ADDRESS));
                break;
            }
            case ActivityEventConstants.EVENT_INSTALLMENT_REMINDER: {

                leadsquaredActivityField = new LeadsquaredActivityField();
                leadsquaredActivityField.setSchemaName(ActivityEventConstants.MX_CUSTOM_1);
                leadsquaredActivityField.setValue(params.get(ActivityEventConstants.MX_CUSTOM_1));
                activityDetails.addField(leadsquaredActivityField);

                leadsquaredActivityField = new LeadsquaredActivityField();
                leadsquaredActivityField.setSchemaName(ActivityEventConstants.MX_CUSTOM_2);
                leadsquaredActivityField.setValue(params.get(ActivityEventConstants.MX_CUSTOM_2));
                activityDetails.addField(leadsquaredActivityField);

                leadsquaredActivityField = new LeadsquaredActivityField();
                leadsquaredActivityField.setSchemaName(ActivityEventConstants.MX_CUSTOM_3);
                leadsquaredActivityField.setValue(params.get(ActivityEventConstants.MX_CUSTOM_3));
                activityDetails.addField(leadsquaredActivityField);


                leadsquaredActivityField = new LeadsquaredActivityField();
                leadsquaredActivityField.setSchemaName(ActivityEventConstants.MX_CUSTOM_5);
                leadsquaredActivityField.setValue(params.get(ActivityEventConstants.MX_CUSTOM_5));
                activityDetails.addField(leadsquaredActivityField);

                leadsquaredActivityField = new LeadsquaredActivityField();
                leadsquaredActivityField.setSchemaName(ActivityEventConstants.MX_CUSTOM_6);
                leadsquaredActivityField.setValue(params.get(ActivityEventConstants.MX_CUSTOM_6));
                activityDetails.addField(leadsquaredActivityField);

                leadsquaredActivityField = new LeadsquaredActivityField();
                leadsquaredActivityField.setSchemaName(ActivityEventConstants.MX_CUSTOM_7);
                leadsquaredActivityField.setValue(params.get(ActivityEventConstants.MX_CUSTOM_7));
                activityDetails.addField(leadsquaredActivityField);

                leadsquaredActivityField = new LeadsquaredActivityField();
                leadsquaredActivityField.setSchemaName(ActivityEventConstants.MX_CUSTOM_8);
                leadsquaredActivityField.setValue(params.get(ActivityEventConstants.MX_CUSTOM_8));
                activityDetails.addField(leadsquaredActivityField);

                activityDetails.setEmailAddress(params.get(ActivityEventConstants.EMAIL_ADDRESS));
                break;
            }
        }

        logger.info("======Pushing the activity to the list for event : {}", eventCode);

        queueActivityToPush(activityDetails);

    }

    public void pushOrderActivity(int eventCode, List<Orders> orderList, Map<Long, List<UserBasicInfo>> usersmap) throws VException {
        List<Orders> firstChunkOrders = orderList;
        List<Orders> secondChunkOrders = new LinkedList<>();

        if (orderList.size() >= ActivityEventConstants.ORDER_CHUNK_SIZE / 2) {
            firstChunkOrders = orderList.subList(0, orderList.size() / 2 - 1);
            secondChunkOrders = orderList.subList(orderList.size() / 2, orderList.size() - 1);
        }

        processOrderActivities(eventCode, firstChunkOrders, usersmap);
        processOrderActivities(eventCode, secondChunkOrders, usersmap);

    }

    private void processOrderActivities(int eventCode, List<Orders> orders, Map<Long, List<UserBasicInfo>> usersmap) throws VException {

        if(CollectionUtils.isEmpty(orders))
            return;

        logger.info("======Processing orders by chunk : {}", orders.size());

        Map<String, List<LeadsquaredActivityField>> fieldsfieldsMap = getActivityFields(orders);

        for(Orders order : orders) {
            ActivityDetails activityDetails = new ActivityDetails();
            activityDetails.setActivityEvent(eventCode);
            activityDetails.setActivityNote(order.getId());
            activityDetails.setLeadVedantuId(order.getUserId());

            if(!usersmap.containsKey(order.getUserId()))
                continue;

            UserBasicInfo user = usersmap.get(order.getUserId()).get(0);

            activityDetails.setEmailAddress(user.getEmail());

            if(StringUtils.isNotEmpty(user.getContactNumber())){
                String phone = cleanPhoneNumber(user.getContactNumber());
                activityDetails.setPhone(phone);
            }
            if(fieldsfieldsMap.containsKey(order.getId())) {
                List<LeadsquaredActivityField> fields = fieldsfieldsMap.get(order.getId());
                activityDetails.addFields(fields);

                logger.info("++++++++++++++++++++++++++\n\n\n\n {}", activityDetails);

                queueActivityToPush(activityDetails);
            }
        }
    }

    private Map<String, List<LeadsquaredActivityField>> getActivityFields(List<Orders> orders) throws VException {

        Map<String, List<LeadsquaredActivityField>> leadsquaredActivityFieldsList = getOrderCustomValues(orders);

        return leadsquaredActivityFieldsList;
    }


    public String getStringValueInRupee(Integer integer) {
        if (integer == null) {
            return "";
        } else {
            Double doubleNumber = integer*1.0/100.0;
            return doubleNumber.toString();
        }
    }

    public String getLsOwnerEmail(String ownerEmail) {

        EmailMappingLS emailMappingLS = emailMappingLSDao.getLsEmail(ownerEmail);

        if (emailMappingLS != null) {
            return emailMappingLS.getLsEmail();
        }

        return null;
    }

    public String getTeacherEmail(String ownerEmail) {

        EmailMappingLS emailMappingLS = emailMappingLSDao.getLsEmail(ownerEmail);

        if (emailMappingLS != null) {
            return emailMappingLS.getVedantuEmail();
        }

        return null;
    }

    public boolean isClosedActivity(ActivityDetails previousActivityDetails) {

        if(previousActivityDetails==null)
            return true;

        Long creationTime = previousActivityDetails.getCreationTime();

        //NOTE: if the last activity is older than 2 weeks consider it as closed irrespective of status
        if(creationTime < (System.currentTimeMillis() - ActivityEventConstants.MILLI_WEEK * 2)){
            logger.info("Previous Activity is older than 2 weeks, considering it as closed : {}", previousActivityDetails.getId());
            return true;
        }

        String status =  previousActivityDetails.getFields().get(0).getValue();

        return status.toLowerCase().startsWith(LeadsquaredActivityStatus.Closed.name().toLowerCase()) ||
                status.toLowerCase().startsWith(LeadsquaredActivityStatus.Paid.name().toLowerCase());
    }

    public List<GTTAttendeeDetailsInfo> getGttAttendeeDetails(List<String> userIds, Long fromTime, Long thruTime) throws VException {

        GTTAttendeeRequest gttAttendeeRequest = new GTTAttendeeRequest();
        gttAttendeeRequest.setUserIds(userIds);
        gttAttendeeRequest.setFromTime(fromTime);
        gttAttendeeRequest.setThruTime(thruTime);
        ClientResponse resp = WebUtils.INSTANCE.doCall(SCHEDULING_ENDPOINT + "/gttattendee/getGttAttendeeForUsers", HttpMethod.POST,
                new Gson().toJson(gttAttendeeRequest), true);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);

        logger.info("\n\n\njsonString : {}", jsonString);

        Type listType = new TypeToken<ArrayList<GTTAttendeeDetailsInfo>>() {
        }.getType();
        List<GTTAttendeeDetailsInfo> gttAttendeeInfos = new Gson().fromJson(jsonString, listType);

        return gttAttendeeInfos;
    }

    public List<ContentInfoResp> getContentInfo(List<String> activeUserIds, Long fromTime, Long thruTime) throws VException {

        GetContentsReq getContentsReq = new GetContentsReq();
        getContentsReq.setStudentIds(activeUserIds);
        getContentsReq.setFromTime(fromTime);
        getContentsReq.setThruTime(thruTime);
        getContentsReq.setContentState(Arrays.asList(ContentState.EVALUATED.name()));

        ClientResponse resp = WebUtils.INSTANCE.doCall(LMS_ENDPOINT + "/getContentsForStudentsByFilter", HttpMethod.POST,
                new Gson().toJson(getContentsReq), true);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);

        Type listType = new TypeToken<ArrayList<ContentInfoResp>>() {
        }.getType();
        List<ContentInfoResp> contentInfos = new Gson().fromJson(jsonString, listType);

        return contentInfos;
    }

    public List<CMDSTestInfo> getTestsDetails(List<String> testIds) throws VException {

        ClientResponse resp = WebUtils.INSTANCE.doCall(LMS_ENDPOINT + "/cmds/test/getTestsBasicDetails", HttpMethod.POST,
                new Gson().toJson(testIds), true);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);

        Type listType = new TypeToken<ArrayList<CMDSTestInfo>>() {
        }.getType();
        List<CMDSTestInfo> cmdsTestInfos = new Gson().fromJson(jsonString, listType);

        return cmdsTestInfos;
    }

    public ActivityDetails getActivityById(String prospectActivityId) {
        ActivityDetails activityDetails = activityDao.getActivityById(prospectActivityId);
        return activityDetails;
    }

    public void updateActivity(ActivityDetails activityDetails) {
        activityDao.updateActivity(activityDetails);
    }

    private UserBasicInfo getUserBasicInfoByEmail(String email) throws VException {
        if (email != null) {
            String url = USER_ENDPOINT + "/getUserBasicInfoByEmail?exposeEmail=true&email=" + email;
            ClientResponse response = WebUtils.INSTANCE.doCall(url, HttpMethod.GET, null);
            VExceptionFactory.INSTANCE.parseAndThrowException(response);
            String entity = response.getEntity(String.class);
            return GSON.fromJson(entity, UserBasicInfo.class);
        }
        return null;
    }

    public void triggerFreshdeskTicketActivityPushToQueue(){
        freshDeskFetchRecordsTaskPushToQueue();

    }

    public void triggerFreshdeskTicketActivity() throws VException {
        List<Map<String, Object>> tickets = new ArrayList<>();
        int page = 0;
        int BATCH_SIZE = 100;
        int eventGrievance = ActivityEventConstants.EVENT_GRIEVANCE;
        int eventOpenTickets = ActivityEventConstants.EVENT_OPEN_TICKETS;

        do {
            String serverUrl = FreshdeskConfig.API_BASE_URL + FreshdeskConfig.GET_LIST_TICKETS
                    + "?" + FreshdeskConfig.PARAM_UPDATED_SINCE_LAST_N_MIN.apply(16L)
                    + "&" + FreshdeskConfig.PARAM_INCLUDE.apply(new String[]{"description"})
                    + "&" + FreshdeskConfig.PARAM_PER_PAGE_N.apply(BATCH_SIZE)
                    + "&" + FreshdeskConfig.PARAM_PAGE_N.apply(++page);
            ClientResponse response = WebUtils.INSTANCE.doCall(serverUrl, HttpMethod.GET, null, getFreshDeskApiHeader());
            VExceptionFactory.INSTANCE.parseAndThrowException(response);
            String entity = response.getEntity(String.class);
            tickets = GSON.fromJson(entity, new TypeToken<List<Map<String, Object>>>() {
            }.getType());


            Map<FreshdeskContact.Type, Map<Long, FreshdeskContact>> contactEmailMap = getFreshdeskContactMap(tickets);
            Map<Long, FreshdeskContact> reqVedUserIds = new HashMap<>(BATCH_SIZE * 2);
            Map<Long, FreshdeskContact> resVedUserIds = new HashMap<>(BATCH_SIZE * 2);
            mutateVedUserIdForTicketsList(tickets, contactEmailMap, reqVedUserIds, resVedUserIds);

            List<AcadMentorStudents> mentors = getAcadMentors(new ArrayList<>(reqVedUserIds.keySet()));
            Map<Long, Long> studentAcadMentorMap = mentors.stream().collect(Collectors.toMap(AcadMentorStudents::getStudentId, AcadMentorStudents::getAcadMentorId));


            pushGrievanceActivity(tickets, eventGrievance, resVedUserIds, contactEmailMap, studentAcadMentorMap);
            pushOpenTicketActivity(tickets, eventOpenTickets, contactEmailMap);
            persistTicketInRedshift(tickets);
        } while (tickets.size() == BATCH_SIZE);

        triggerAllActivities();
    }


    public void triggerFreshdeskRefundTicketActivity(String searchCol, String searchVal) throws VException {
        List<Map<String, Object>> tickets;
        int page = 0;
        int eventOpenTickets = ActivityEventConstants.EVENT_OPEN_TICKETS;

        do {

            String serverUrl = FreshdeskConfig.API_BASE_URL + FreshdeskConfig.GET_SEARCH_TICKETS
                    + "?" + FreshdeskConfig.PARAM_SEARCH.apply(searchCol, searchVal)
                    + "&" + FreshdeskConfig.PARAM_PAGE_N.apply(++page);
            ClientResponse response = WebUtils.INSTANCE.doCall(serverUrl, HttpMethod.GET, null, getFreshDeskApiHeader());
            String entity = response.getEntity(String.class);
            VExceptionFactory.INSTANCE.parseAndThrowException(response);
            Map<String, Object> map = GSON.fromJson(entity, new TypeToken<Map<String, Object>>() {
            }.getType());
            tickets = GSON.fromJson(GSON.toJson(map.get("results")), new TypeToken<List<Map<String, Object>>>() {
            }.getType());

            Map<FreshdeskContact.Type, Map<Long, FreshdeskContact>> contactEmailMap = getFreshdeskContactMap(tickets);

            pushRefundTicketActivity(tickets, eventOpenTickets, contactEmailMap);
        } while (tickets.size() == 0);

        triggerAllActivities();
    }

    private void pushGrievanceActivity(List<Map<String, Object>> tickets, int eventCode,
                                       Map<Long, FreshdeskContact> resVedUserIds, Map<FreshdeskContact.Type, Map<Long, FreshdeskContact>> contactEmailMap,
                                       Map<Long, Long> studentAcadMentorMap) {
        logger.info("GRIEVANCE ACTIVITY");
        for (Map<String, Object> ticket : tickets) {
            Long studentIdFreshdesk = PARSE_LONG.apply(ticket.get("requester_id"));
            FreshdeskContact studentContact = contactEmailMap.get(FreshdeskContact.Type.LEAD).get(studentIdFreshdesk);
            Long studentId = studentContact == null ? null : studentContact.getVedantuUserId();

            // this is to check assignee is the acadmentor
            Long acadMentorId = studentAcadMentorMap.get(studentId);
            FreshdeskContact acadMentorContact = resVedUserIds.get(acadMentorId);
            Long id = PARSE_LONG.apply(ticket.get("id"));

            logger.info("FRESH DESK CONTACT " + Objects.isNull(studentContact));
            logger.info("ACADMENTOR ID " + acadMentorId);
            logger.info("ACADMENTOR CONTACT " + Objects.isNull(acadMentorContact));

            if (acadMentorId == null || acadMentorContact == null || studentContact == null ||
                    "noreply@exotel.com".equalsIgnoreCase(studentContact.getEmail())) {
                continue;
            }

            List<ActivityDetails> freshdeskActivities = activityDao.getFreshDeskActivityDetails(studentId, eventCode, id);
            boolean updateMode = false;
            if (!freshdeskActivities.isEmpty()) {
                freshdeskActivities.sort(Comparator.comparing(ActivityDetails::getCreationTime));
                updateMode = true;
            }
            List<LeadsquaredUser> owners = null;
            try {
                String email = acadMentorContact.getEmail();
                owners = leadsquaredManager.fetchAgentsByEmailId(email);
                if (ArrayUtils.isEmpty(owners)) {
                    String lsOwnerEmail = getLsOwnerEmail(acadMentorContact.getEmail());
                    owners = leadsquaredManager.fetchAgentsByEmailId(lsOwnerEmail);
                }
            } catch (Exception e) {
                logger.error(e.getMessage(), e);
            }

            logger.info("GRIVANCE OWNER " + ArrayUtils.isEmpty(owners));
            LeadsquaredUser owner = null;
            if (owners != null && !owners.isEmpty()) {
                owner = owners.get(0);
            } else {
                continue;
            }

            Long status = PARSE_LONG.apply(ticket.get("status"));
            String description = StringUtils.defaultIfEmpty((String) ticket.get("description_text"), "");

            ActivityDetails details = new ActivityDetails();
            details.setActivityEvent(eventCode);
            details.setLeadVedantuId(studentId);
            details.setOwnerVedantuId(acadMentorContact.getVedantuUserId());
            details.setEmailAddress(studentContact.getEmail());
            String phone = cleanPhoneNumber(studentContact.getPhone());
            details.setPhone(phone);
            details.setActivityOwnerEmail(acadMentorContact.getEmail());
            details.setFreshdeskTicketId(id);
            details.setFreshdeskStatus(status);

            details.setActivityNote("Ticket ID : " + id + "\n" + "Description : " + description);

            LeadsquaredActivityField leadsquaredActivityField = new LeadsquaredActivityField();
            leadsquaredActivityField.setSchemaName(ActivityEventConstants.STATUS);
            leadsquaredActivityField.setValue(LeadsquaredActivityStatus.getFreshDeskActivityStatus(status).getValue());
            details.addField(leadsquaredActivityField);

            leadsquaredActivityField = new LeadsquaredActivityField();
            leadsquaredActivityField.setSchemaName(ActivityEventConstants.OWNER);
            leadsquaredActivityField.setValue(StringUtils.defaultIfEmpty(owner.getUserId(), ""));
            details.addField(leadsquaredActivityField);

            leadsquaredActivityField = new LeadsquaredActivityField();
            leadsquaredActivityField.setSchemaName(ActivityEventConstants.MX_CUSTOM_1);
            leadsquaredActivityField.setValue(StringUtils.defaultIfEmpty(String.valueOf(description), ""));
            details.addField(leadsquaredActivityField);

            Object customFields = ticket.get("custom_fields");
            if (customFields instanceof Map) {
                Map fields = (Map) customFields;
                Object type = fields.get("type");

                leadsquaredActivityField = new LeadsquaredActivityField();
                leadsquaredActivityField.setSchemaName(ActivityEventConstants.MX_CUSTOM_7);
                leadsquaredActivityField.setValue(StringUtils.defaultIfEmpty((String) type, ""));
                details.addField(leadsquaredActivityField);

                Object subType = fields.get("sub_type");
                leadsquaredActivityField = new LeadsquaredActivityField();
                leadsquaredActivityField.setSchemaName(ActivityEventConstants.MX_CUSTOM_8);
                leadsquaredActivityField.setValue(StringUtils.defaultIfEmpty((String) subType, ""));
                details.addField(leadsquaredActivityField);
            }

            if (!updateMode) {
                triggerActivity(details);
            } else if (!details.sameAs(freshdeskActivities.get(freshdeskActivities.size() - 1))) {
                String prospectActivityId = freshdeskActivities.stream().
                        filter(e -> StringUtils.isNotEmpty(e.getProspectActivityId()))
                        .findAny().orElseGet(ActivityDetails::new)
                        .getProspectActivityId();
                if (StringUtils.isNotEmpty(prospectActivityId)) {
                    details.setProspectActivityId(prospectActivityId);
                    addUpdateActivityToQueue(details);
                } else {
                    logger.error("PROSPECT ACTIVITY ID NULL FOR FD TICKET " + id);
                }
            }
        }
    }

    private void pushOpenTicketActivity(List<Map<String, Object>> tickets, int eventCode, Map<FreshdeskContact.Type, Map<Long, FreshdeskContact>> contactEmailMap) {

        logger.info("OPEN TICKET ACTIVITY");
        for (Map<String, Object> ticket : tickets) {
            Long studentIdFreshdesk = PARSE_LONG.apply(ticket.get("requester_id"));
            Long agentIdFreshdesk = PARSE_LONG.apply(ticket.get("responder_id"));
            FreshdeskContact studentContact = contactEmailMap.get(FreshdeskContact.Type.LEAD).get(studentIdFreshdesk);
            FreshdeskContact agentContact = contactEmailMap.get(FreshdeskContact.Type.AGENT).get(agentIdFreshdesk);
            Long studentId = studentContact == null ? null : studentContact.getVedantuUserId();
            Long id = PARSE_LONG.apply(ticket.get("id"));

            logger.info("FRESH DESK CONTACT " + studentContact);

            if (studentContact == null || "noreply@exotel.com".equalsIgnoreCase(studentContact.getEmail())) {
                continue;
            }
            List<ActivityDetails> freshdeskActivities = activityDao.getFreshDeskActivityDetails(studentId, eventCode, id);
            boolean updateMode = false;
            if (!freshdeskActivities.isEmpty()) {
                freshdeskActivities.sort(Comparator.comparing(ActivityDetails::getCreationTime));
                updateMode = true;
            }

            Long status = PARSE_LONG.apply(ticket.get("status"));
            if (status != 2 && !updateMode) {
                continue;
            }

            LeadsquaredUser owner = null;
            String email = "";
            Long ownerVedantuId = null;
            if (agentContact != null) {
                String lsOwnerEmail = getLsOwnerEmail(agentContact.getEmail());
                ownerVedantuId = lsOwnerEmail == null ? agentContact.getVedantuUserId() : agentContact.getVedantuTeacherUserId();
                email = lsOwnerEmail == null ? agentContact.getEmail() : email;
            }
            try {
                String agentContactEmail = agentContact == null ? null : agentContact.getEmail();
                List<LeadsquaredUser> owners = leadsquaredManager.fetchAgentsByEmailId(agentContactEmail);
                if (ArrayUtils.isEmpty(owners)) {
                    String lsOwnerEmail = getLsOwnerEmail(agentContactEmail);
                    owners = leadsquaredManager.fetchAgentsByEmailId(lsOwnerEmail);
                    owner = owners == null || owners.isEmpty() ? null : owners.get(0);
                } else {
                    owner = owners.get(0);
                }
            } catch (Exception ignored) {
            }
            String description = StringUtils.defaultIfEmpty((String) ticket.get("description_text"), "");

            ActivityDetails details = new ActivityDetails();
            details.setActivityEvent(eventCode);
            details.setLeadVedantuId(studentId);
            details.setOwnerVedantuId(ownerVedantuId);
            details.setEmailAddress(studentContact.getEmail());
            String phone = cleanPhoneNumber(studentContact.getPhone());
            details.setPhone(phone);
            details.setActivityOwnerEmail(email);
            details.setFreshdeskTicketId(id);
            details.setFreshdeskStatus(status);

            details.setActivityNote("Ticket ID : " + id + "\n" + "Description : " + description);

            LeadsquaredActivityField leadsquaredActivityField = new LeadsquaredActivityField();
            leadsquaredActivityField.setSchemaName(ActivityEventConstants.STATUS);
            leadsquaredActivityField.setValue(LeadsquaredActivityStatus.getFreshDeskActivityStatus(status).getValue());
            details.addField(leadsquaredActivityField);

            if (owner != null) {
                leadsquaredActivityField = new LeadsquaredActivityField();
                leadsquaredActivityField.setSchemaName(ActivityEventConstants.OWNER);
                leadsquaredActivityField.setValue(StringUtils.defaultIfEmpty(owner.getUserId(), ""));
                details.addField(leadsquaredActivityField);
            }

            leadsquaredActivityField = new LeadsquaredActivityField();
            leadsquaredActivityField.setSchemaName(ActivityEventConstants.MX_CUSTOM_1);
            leadsquaredActivityField.setValue(String.valueOf(id));
            details.addField(leadsquaredActivityField);

            leadsquaredActivityField = new LeadsquaredActivityField();
            leadsquaredActivityField.setSchemaName(ActivityEventConstants.MX_CUSTOM_2);
            leadsquaredActivityField.setValue(String.valueOf(email));
            details.addField(leadsquaredActivityField);

            leadsquaredActivityField = new LeadsquaredActivityField();
            leadsquaredActivityField.setSchemaName(ActivityEventConstants.MX_CUSTOM_3);
            Object createdAt = ticket.get("created_at");
            OffsetDateTime dateTime = createdAt == null ? null
                    : OffsetDateTime.parse(String.valueOf(createdAt)).atZoneSameInstant(ZoneId.of("Asia/Kolkata")).toOffsetDateTime();
            leadsquaredActivityField.setValue(dateTime + " [Asia/Kolkata]");
            details.addField(leadsquaredActivityField);

            Object customFields = ticket.get("custom_fields");
            if (customFields instanceof Map) {
                Map fields = (Map) customFields;
                Object type = fields.get("type");

                leadsquaredActivityField = new LeadsquaredActivityField();
                leadsquaredActivityField.setSchemaName(ActivityEventConstants.MX_CUSTOM_4);
                leadsquaredActivityField.setValue(StringUtils.defaultIfEmpty((String) type, ""));
                details.addField(leadsquaredActivityField);

                Object subType = fields.get("sub_type");
                leadsquaredActivityField = new LeadsquaredActivityField();
                leadsquaredActivityField.setSchemaName(ActivityEventConstants.MX_CUSTOM_6);
                leadsquaredActivityField.setValue(StringUtils.defaultIfEmpty((String) subType, ""));
                details.addField(leadsquaredActivityField);

                Object refund = fields.get("cf_refund");
                leadsquaredActivityField = new LeadsquaredActivityField();
                leadsquaredActivityField.setSchemaName(ActivityEventConstants.MX_CUSTOM_7);
                leadsquaredActivityField.setValue(StringUtils.defaultIfEmpty((String) refund, ""));
                details.addField(leadsquaredActivityField);
            }

            if (!updateMode) {
                triggerActivity(details);
            } else if (!details.sameAs(freshdeskActivities.get(freshdeskActivities.size() - 1))) {
                String prospectActivityId = freshdeskActivities.stream().
                        filter(e -> StringUtils.isNotEmpty(e.getProspectActivityId()))
                        .findAny().orElseGet(ActivityDetails::new)
                        .getProspectActivityId();
                if (StringUtils.isNotEmpty(prospectActivityId)) {
                    details.setProspectActivityId(prospectActivityId);
                    addUpdateActivityToQueue(details);
                } else {
                    logger.error("OPEN TICKENT ACTIVITY : PROSPECT ACTIVITY ID NULL FOR FD TICKET " + id);
                }
            }
        }
    }



    public void persistTicketInRedshift(List<Map<String, Object>> tickets) {

        logger.info("Persisting tickets : {}",tickets);

        String insertQuery = "INSERT INTO booking_reversal(ticket_id,subject,status,priority,ticket_source,student_type," +
                "ticket_group,created_time,due_by_time,resolved_time,closed_time,last_update_time," +
                "resolution_status,user_email_id,mobile_number,user_name,course_type,sub_type,category," +
                "concern_status,teacher_name_in_case_of_teacher_related_issues,teacher_change,refund,refund_request_date," +
                "retention_status,refund_amount,account_Holder_name,account_number,ifsc_code,students_location_city," +
                "students_location_state,students_location_country,students_location_pincode,rdx,refund_done,full_name) " +
                "values";

        boolean isInsertRecordFound = false;

        try {
            for (Map<String, Object> ticket : tickets) {

                try {
                    Map<String, Object> customFields = (Map<String, Object>) ticket.get("custom_fields");

                    Long id = PARSE_LONG.apply(ticket.get("id"));
                    String subject = removeSpeciaCharacters(ticket.get("subject") != null ? ticket.get("subject").toString() : "");
                    String status = ticket.get("status") != null ? ticket.get("status").toString() : "";
                    String priority = ticket.get("priority") != null ? ticket.get("priority").toString() : "";
                    String student_type = ticket.get("type") != null ? ticket.get("type").toString() : "";
                    String ticket_group = ticket.get("group_id") != null ? ticket.get("group_id").toString() : "";
                    String created_time = ticket.get("created_at") != null ? ticket.get("created_at").toString() : "";
                    String due_by_time = ticket.get("due_by") != null ? ticket.get("due_by").toString() : "";
                    String last_update_time = ticket.get("updated_at") != null ? ticket.get("updated_at").toString() : "";
                    String resolved_time = status == "4" ? last_update_time : null;
                    String closed_time = status == "5" ? last_update_time : null;
                    String resolution_status = ticket.get("cf_concern_status") != null ? ticket.get("cf_concern_status").toString() : "";
                    String user_email_id = customFields.get("users_email_id") != null ? customFields.get("users_email_id").toString() : "";
                    String mobile_number = customFields.get("mobile_number") != null ? customFields.get("mobile_number").toString() : "";
                    String student_name = customFields.get("student_name") != null ? customFields.get("student_name").toString() : "";
                    String course_type = customFields.get("course_type") != null ? customFields.get("course_type").toString() : "";
                    String sub_type = customFields.get("sub_type") != null ? customFields.get("sub_type").toString() : "";
                    String category = customFields.get("cf_category") != null ? customFields.get("cf_category").toString() : "";
                    String concern_status = customFields.get("cf_concern_status") != null ? customFields.get("cf_concern_status").toString() : "";
                    String teacher_name_in_case_of_teacher_related_issues = customFields.get("cf_teacher_name_in_case_of_teacher_related_issues") != null ? customFields.get("cf_teacher_name_in_case_of_teacher_related_issues").toString() : "";
                    String teacher_change = customFields.get("cf_teacher_change") != null ? customFields.get("cf_teacher_change").toString() : "";
                    String refund = customFields.get("cf_refund") != null ? customFields.get("cf_refund").toString() : "";
                    String refund_request_date = customFields.get("cf_refund_request_date") != null ? customFields.get("cf_refund_request_date").toString() : null;
                    String retention_status = customFields.get("cf_retention_status") != null ? customFields.get("cf_retention_status").toString() : "";
                    Double refund_amount = customFields.get("cf_refund_amount") != null ? Double.parseDouble(customFields.get("cf_refund_amount").toString().replace(",","")) : 0;
                    String account_Holder_name = customFields.get("cf_account_holder_name") != null ? customFields.get("cf_account_holder_name").toString() : "";
                    String account_number = customFields.get("cf_account_number") != null ? customFields.get("cf_account_number").toString() : "";
                    String ifsc_code = customFields.get("cf_ifsc_code") != null ? customFields.get("cf_ifsc_code").toString() : "";
                    String students_location_city = customFields.get("cf_students_location_city") != null ? customFields.get("cf_students_location_city").toString() : "";
                    String students_location_state = customFields.get("cf_students_location_state") != null ? customFields.get("cf_students_location_state").toString() : "";
                    String students_location_country = customFields.get("cf_students_location_country") != null ? customFields.get("cf_students_location_country").toString() : "";
                    String students_location_pincode = customFields.get("cf_students_location_pincode") != null ? customFields.get("cf_students_location_pincode").toString() : "";
                    String rdx = customFields.get("rdx") != null ? customFields.get("rdx").toString() : "";
                    String refund_done = customFields.get("refund_done") != null ? customFields.get("refund_done").toString() : "";
                    String full_name = student_name;
                    String source = customFields.get("source") != null ? customFields.get("source").toString() : "";

                    if ("2".equalsIgnoreCase(status)) {
                        status = FreshdeskStatus.Open.name();
                    }
                    if ("3".equalsIgnoreCase(status)) {
                        status = FreshdeskStatus.Pending.name();
                    }
                    if ("4".equalsIgnoreCase(status)) {
                        status = FreshdeskStatus.Resolved.name();
                    }
                    if ("5".equalsIgnoreCase(status)) {
                        status = FreshdeskStatus.Closed.name();
                    }

                    String query = "SELECT ticket_id,subject,status,priority,ticket_source,student_type,ticket_group," +
                            "created_time,due_by_time,resolved_time,closed_time,last_update_time,resolution_status," +
                            "user_email_id,mobile_number,user_name,course_type,sub_type,category,concern_status," +
                            "teacher_name_in_case_of_teacher_related_issues,teacher_change,refund,refund_request_date," +
                            "retention_status,refund_amount,account_Holder_name,account_number,ifsc_code," +
                            "students_location_city,students_location_state,students_location_country," +
                            "students_location_pincode,rdx,refund_done,full_name from booking_reversal " +
                            "where ticket_id = '" + id + "';";

                    ResultSet rs = postgressHandler.executeQuery(query);

                    if (rs!=null && rs.next()) {

                        String prevStatus = rs.getString("status");

                        subject = StringUtils.isEmpty(subject) ? rs.getString("subject") : subject;
                        status = StringUtils.isEmpty(status) ? rs.getString("status") : status;
                        priority = StringUtils.isEmpty(priority) ? rs.getString("priority") : priority;
                        student_type = StringUtils.isEmpty(student_type) ? rs.getString("student_type") : student_type;
                        ticket_group = StringUtils.isEmpty(ticket_group) ? rs.getString("ticket_group") : ticket_group;
                        due_by_time = StringUtils.isEmpty(due_by_time) ? rs.getString("due_by_time") : due_by_time;
                        last_update_time = StringUtils.isEmpty(last_update_time) ? new Date().toString() : last_update_time;
                        resolved_time = !prevStatus.equalsIgnoreCase(status) && status.equalsIgnoreCase(FreshdeskStatus.Resolved.name()) ? last_update_time : rs.getString("resolved_time");
                        closed_time = !prevStatus.equalsIgnoreCase(status) && status.equalsIgnoreCase(FreshdeskStatus.Closed.name()) ? last_update_time : rs.getString("closed_time");
                        resolution_status = StringUtils.isEmpty(resolution_status) ? rs.getString("resolution_status") : resolution_status;
                        user_email_id = StringUtils.isEmpty(user_email_id) ? rs.getString("user_email_id") : user_email_id;
                        mobile_number = StringUtils.isEmpty(mobile_number) ? rs.getString("mobile_number") : mobile_number;
                        student_name = StringUtils.isEmpty(student_name) ? rs.getString("user_name") : student_name;
                        course_type = StringUtils.isEmpty(course_type) ? rs.getString("course_type") : course_type;
                        sub_type = StringUtils.isEmpty(sub_type) ? rs.getString("sub_type") : sub_type;
                        category = StringUtils.isEmpty(category) ? rs.getString("category") : category;
                        concern_status = StringUtils.isEmpty(concern_status) ? rs.getString("concern_status") : concern_status;
                        teacher_name_in_case_of_teacher_related_issues = StringUtils.isEmpty(teacher_name_in_case_of_teacher_related_issues) ? rs.getString("teacher_name_in_case_of_teacher_related_issues") : teacher_name_in_case_of_teacher_related_issues;
                        teacher_change = StringUtils.isEmpty(teacher_change) ? rs.getString("teacher_change") : teacher_change;
                        refund = StringUtils.isEmpty(refund) ? rs.getString("refund") : refund;
                        refund_request_date = StringUtils.isEmpty(refund_request_date) ? rs.getString("refund_request_date") : refund_request_date;
                        retention_status = StringUtils.isEmpty(retention_status) ? rs.getString("retention_status") : retention_status;
                        refund_amount = StringUtils.isEmpty(refund_amount.toString()) ? rs.getDouble("refund_amount") : refund_amount;
                        account_Holder_name = StringUtils.isEmpty(account_Holder_name) ? rs.getString("account_Holder_name") : account_Holder_name;
                        account_number = StringUtils.isEmpty(account_number) ? rs.getString("account_number") : account_number;
                        ifsc_code = StringUtils.isEmpty(ifsc_code) ? rs.getString("ifsc_code") : ifsc_code;
                        students_location_city = StringUtils.isEmpty(students_location_city) ? rs.getString("students_location_city") : students_location_city;
                        students_location_state = StringUtils.isEmpty(students_location_state) ? rs.getString("students_location_state") : students_location_state;
                        students_location_country = StringUtils.isEmpty(students_location_country) ? rs.getString("students_location_country") : students_location_country;
                        students_location_pincode = StringUtils.isEmpty(students_location_pincode) ? rs.getString("students_location_pincode") : students_location_pincode;
                        rdx = StringUtils.isEmpty(rdx) ? rs.getString("rdx") : rdx;
                        refund_done = StringUtils.isEmpty(refund_done) ? rs.getString("refund_done") : refund_done;
                        full_name = StringUtils.isEmpty(full_name) ? rs.getString("full_name") : full_name;
                        source = StringUtils.isEmpty(source) ? rs.getString("ticket_source") : source;
                        String updateQuery = "UPDATE booking_reversal " +
                                "set " +
                                "subject = '" + subject + "'," +
                                "status = '" + status + "'," +
                                "priority = '" + priority + "'," +
                                "student_type = '" + student_type + "'," +
                                "ticket_group = '" + ticket_group + "',";

                        if (due_by_time != null) {
                            updateQuery += "due_by_time = '" + due_by_time + "',";
                        }

                        if (last_update_time != null) {
                            updateQuery += "last_update_time = '" + last_update_time + "',";
                        }

                        if (resolved_time != null) {
                            updateQuery += "resolved_time = '" + resolved_time + "',";
                        }

                        if (closed_time != null) {
                            updateQuery += "closed_time = '" + closed_time + "',";
                        }

                        if(student_name.contains("'")){
                            student_name = student_name.replace("'", " ");
                        }

                        updateQuery += "resolution_status = '" + resolution_status + "'," +
                                "user_email_id = '" + user_email_id + "'," +
                                "mobile_number = '" + mobile_number + "'," +
                                "full_name = '" + student_name + "'," +
                                "course_type = '" + course_type + "'," +
                                "sub_type = '" + sub_type + "'," +
                                "category = '" + category + "'," +
                                "concern_status = '" + concern_status + "'," +
                                "teacher_name_in_case_of_teacher_related_issues = '" + teacher_name_in_case_of_teacher_related_issues + "'," +
                                "teacher_change = '" + teacher_change + "'," +
                                "refund = '" + refund + "',";
                        if (refund_request_date != null) {
                            updateQuery += "refund_request_date = '" + refund_request_date + "',";
                        }
                        updateQuery += "retention_status = '" + retention_status + "'," +
                                "refund_amount = " + refund_amount + "," +
                                "account_Holder_name = '" + account_Holder_name + "'," +
                                "account_number = '" + account_number + "'," +
                                "ifsc_code = '" + ifsc_code + "'," +
                                "students_location_city = '" + students_location_city + "'," +
                                "students_location_state = '" + students_location_state + "'," +
                                "students_location_country = '" + students_location_country + "'," +
                                "students_location_pincode = '" + students_location_pincode + "'," +
                                "rdx = '" + rdx + "'," +
                                "refund_done = '" + refund_done + "'," +
                                "ticket_source = '" + source + "'" +
                                " where ticket_id = '" + id + "';";

                        logger.info("Updating existing record with id : {}",id);
                        logger.info("Update Query : {}", updateQuery);

                        postgressHandler.execute(updateQuery);
                        logger.info("Successfully updated record with id : {}",id);

                    } else {

                        isInsertRecordFound = true;

                        insertQuery += "('" +
                                id + "','" +
                                subject + "','" +
                                status + "','" +
                                priority + "','" +
                                source + "','" +
                                student_type + "','" +
                                ticket_group + "',";

                        if (created_time != null) {
                            insertQuery += "'" + created_time + "',";
                        } else {
                            insertQuery += null + ",";
                        }

                        if (due_by_time != null) {
                            insertQuery += "'" + due_by_time + "',";
                        } else {
                            insertQuery += null + ",";
                        }

                        if (resolved_time != null) {
                            insertQuery += "'" + resolved_time + "',";
                        } else {
                            insertQuery += null + ",";
                        }

                        if (closed_time != null) {
                            insertQuery += "'" + closed_time + "',";
                        } else {
                            insertQuery += null + ",";
                        }

                        if (last_update_time != null) {
                            insertQuery += "'" + last_update_time + "',";
                        } else {
                            insertQuery += null + ",";
                        }


                        insertQuery += "'" + resolution_status + "','" +
                                user_email_id + "','" +
                                mobile_number + "','" +
                                student_name + "','" +
                                course_type + "','" +
                                sub_type + "','" +
                                category + "','" +
                                concern_status + "','" +
                                teacher_name_in_case_of_teacher_related_issues + "','" +
                                teacher_change + "','" +
                                refund + "',";

                        if (refund_request_date != null) {
                            insertQuery += "'" + refund_request_date + "',";
                        } else {
                            insertQuery += null + ",";
                        }

                        insertQuery += "'" + retention_status + "'," +
                                refund_amount + ",'" +
                                account_Holder_name + "','" +
                                account_number + "','" +
                                ifsc_code + "','" +
                                students_location_city + "','" +
                                students_location_state + "','" +
                                students_location_country + "','" +
                                students_location_pincode + "','" +
                                rdx + "','" +
                                refund_done + "','" +
                                full_name + "'" +
                                "),";

                    }

                } catch (Exception e) {
                    logger.error("Error persisting freshdesk tickets in redshift", e);
                }
            }

            if (isInsertRecordFound) {

                if (insertQuery.endsWith(",")) {
                    insertQuery = insertQuery.substring(0,insertQuery.length() - 1);
                }

                insertQuery += ";";

                logger.info("Inserting freshdesk activities");
                
                logger.info("Insert Query : {}", insertQuery);

                postgressHandler.execute(insertQuery);
                logger.info("Successfully inserted freshdesk activities");
            }

        } catch (Exception e) {
            logger.error("Error persisting freshdesk tickets in redshift", e);
        }

    }

    private String removeSpeciaCharacters(String string) {

        if(StringUtils.isEmpty(string))
            return "";

        return string.replaceAll("[^a-zA-Z0-9 ]", "");
    }

    private void pushRefundTicketActivity(List<Map<String, Object>> tickets, int eventCode, Map<FreshdeskContact.Type, Map<Long, FreshdeskContact>> contactEmailMap) {

        for (Map<String, Object> ticket : tickets) {
            Long studentIdFreshdesk = PARSE_LONG.apply(ticket.get("requester_id"));
            Long agentIdFreshdesk = PARSE_LONG.apply(ticket.get("responder_id"));
            FreshdeskContact studentContact = contactEmailMap.get(FreshdeskContact.Type.LEAD).get(studentIdFreshdesk);
            FreshdeskContact agentContact = contactEmailMap.get(FreshdeskContact.Type.AGENT).get(agentIdFreshdesk);
            Long studentId = studentContact == null ? null : studentContact.getVedantuUserId();
            Long id = PARSE_LONG.apply(ticket.get("id"));

            logger.info("FRESH DESK CONTACT" + studentContact);

            if (studentContact == null || activityDao.checkForFreshdeskActivity(studentId, eventCode, id)) {
                continue;
            }

            LeadsquaredUser owner = null;
            try {
                List<LeadsquaredUser> owners = leadsquaredManager.fetchAgentsByEmailId(agentContact.getEmail());
                owner = owners == null || owners.isEmpty() ? null : owners.get(0);
            } catch (Exception ignored) {
            }
            String description = StringUtils.defaultIfEmpty((String) ticket.get("description_text"), "");
            Long status = PARSE_LONG.apply(ticket.get("status"));

            ActivityDetails details = new ActivityDetails();
            details.setActivityEvent(eventCode);
            details.setLeadVedantuId(studentId);
            details.setOwnerVedantuId(agentContact == null ? null : agentContact.getVedantuUserId());
            details.setEmailAddress(studentContact.getEmail());
            String phone = cleanPhoneNumber(studentContact.getPhone());
            details.setPhone(phone);
            details.setActivityOwnerEmail(agentContact == null ? null : agentContact.getEmail());
            details.setFreshdeskTicketId(id);
            details.setFreshdeskStatus(status);

            details.setActivityNote("Ticket ID : " + id + "\n" + "Description : " + description);

            LeadsquaredActivityField leadsquaredActivityField = new LeadsquaredActivityField();
            leadsquaredActivityField.setSchemaName(ActivityEventConstants.STATUS);
            leadsquaredActivityField.setValue(LeadsquaredActivityStatus.getFreshDeskActivityStatus(status).getValue());
            details.addField(leadsquaredActivityField);

            if (owner != null) {
                leadsquaredActivityField = new LeadsquaredActivityField();
                leadsquaredActivityField.setSchemaName(ActivityEventConstants.OWNER);
                leadsquaredActivityField.setValue(StringUtils.defaultIfEmpty(owner.getUserId(), ""));
                details.addField(leadsquaredActivityField);
            }

            leadsquaredActivityField = new LeadsquaredActivityField();
            leadsquaredActivityField.setSchemaName(ActivityEventConstants.MX_CUSTOM_1);
            leadsquaredActivityField.setValue(String.valueOf(id));
            details.addField(leadsquaredActivityField);

            leadsquaredActivityField = new LeadsquaredActivityField();
            leadsquaredActivityField.setSchemaName(ActivityEventConstants.MX_CUSTOM_2);
            leadsquaredActivityField.setValue(String.valueOf(agentContact == null ? "" : agentContact.getEmail()));
            details.addField(leadsquaredActivityField);

            leadsquaredActivityField = new LeadsquaredActivityField();
            leadsquaredActivityField.setSchemaName(ActivityEventConstants.MX_CUSTOM_3);
            Object createdAt = ticket.get("created_at");
            OffsetDateTime dateTime = createdAt == null ? null
                    : OffsetDateTime.parse(String.valueOf(createdAt)).atZoneSameInstant(ZoneId.of("Asia/Kolkata")).toOffsetDateTime();
            leadsquaredActivityField.setValue(dateTime + " [Asia/Kolkata]");
            details.addField(leadsquaredActivityField);

            Object customFields = ticket.get("custom_fields");
            if (customFields instanceof Map) {
                Map fields = (Map) customFields;
                Object type = fields.get("type");

                leadsquaredActivityField = new LeadsquaredActivityField();
                leadsquaredActivityField.setSchemaName(ActivityEventConstants.MX_CUSTOM_4);
                leadsquaredActivityField.setValue(StringUtils.defaultIfEmpty((String) type, ""));
                details.addField(leadsquaredActivityField);

                Object subType = fields.get("sub_type");
                leadsquaredActivityField = new LeadsquaredActivityField();
                leadsquaredActivityField.setSchemaName(ActivityEventConstants.MX_CUSTOM_6);
                leadsquaredActivityField.setValue(StringUtils.defaultIfEmpty((String) subType, ""));
                details.addField(leadsquaredActivityField);

                Object refund = fields.get("cf_refund");
                leadsquaredActivityField = new LeadsquaredActivityField();
                leadsquaredActivityField.setSchemaName(ActivityEventConstants.MX_CUSTOM_7);
                leadsquaredActivityField.setValue(StringUtils.defaultIfEmpty((String) refund, ""));
                details.addField(leadsquaredActivityField);
            }

            queueActivityToPush(details);
        }
    }

    private void mutateVedUserIdForTicketsList(List<Map<String, Object>> tickets, Map<FreshdeskContact.Type, Map<Long, FreshdeskContact>> contactEmailMap,
                                               Map<Long, FreshdeskContact> reqVedUserIds, Map<Long, FreshdeskContact> resVedUserIds) {
        for (Map<String, Object> ticket : tickets) {
            Long requesterId = PARSE_LONG.apply(ticket.get("requester_id"));
            Long responderId = PARSE_LONG.apply(ticket.get("responder_id"));

            FreshdeskContact requester = contactEmailMap.get(FreshdeskContact.Type.LEAD).get(requesterId);
            FreshdeskContact responder = contactEmailMap.get(FreshdeskContact.Type.AGENT).get(responderId);
            if (requester != null && (requester.getEmail() != null || requester.getPhone() !=null ) && responder != null && responder.getEmail() != null) {
                if (requester.getVedantuUserId() != null) {
                    reqVedUserIds.putIfAbsent(requester.getVedantuUserId(), requester);
                }
                if (responder.getVedantuUserId() != null) {
                    resVedUserIds.putIfAbsent(responder.getVedantuUserId(), responder);
                }
                if (responder.getVedantuTeacherUserId() != null) {
                    resVedUserIds.putIfAbsent(responder.getVedantuTeacherUserId(), responder);
                }
            }
        }
    }

    private Map<FreshdeskContact.Type, Map<Long, FreshdeskContact>> getFreshdeskContactMap(List<Map<String, Object>> tickets) throws VException {
        List<Long> requesterIds = new ArrayList<>();
        List<Long> responderIds = new ArrayList<>();
        for (Map<String, Object> ticket : tickets) {
            requesterIds.add(PARSE_LONG.apply(ticket.get("requester_id")));
            responderIds.add(PARSE_LONG.apply(ticket.get("responder_id")));
        }
        Map<Long, FreshdeskContact> lead = getContactEmail(requesterIds, FreshdeskContact.Type.LEAD);
        Map<Long, FreshdeskContact> agent = getContactEmail(responderIds, FreshdeskContact.Type.AGENT);
        Map<FreshdeskContact.Type, Map<Long, FreshdeskContact>> contacts = new HashMap<>();
        contacts.put(FreshdeskContact.Type.LEAD, lead);
        contacts.put(FreshdeskContact.Type.AGENT, agent);
        return contacts;
    }

    private Map<Long, FreshdeskContact> getContactEmail(List<Long> ids, FreshdeskContact.Type type) throws VException {
        List<FreshdeskContact> entities = freshdeskContactDao.getFreshDeskContacts(ids, type);
        Map<Long, FreshdeskContact> contactMap = new HashMap<>();
        if (entities != null) {
            for (FreshdeskContact entity : entities) {
                contactMap.putIfAbsent(entity.getFreshdeskContactId(), entity);
                if (setVedUserId(entity, type)) {
                    freshdeskContactDao.save(entity);
                }
            }
        }

        for (Long id : ids) {
            if (!contactMap.containsKey(id) && id != null) {
                String url = FreshdeskContact.Type.LEAD.equals(type) ? FreshdeskConfig.API_BASE_URL + FreshdeskConfig.GET_VIEW_CONTACT + id
                        : FreshdeskConfig.API_BASE_URL + FreshdeskConfig.GET_VIEW_AGENTS + id;

                ClientResponse response = WebUtils.INSTANCE.doCall(url, HttpMethod.GET, null, getFreshDeskApiHeader());
                if (response.getResponseStatus().getStatusCode() != 200) {
                    continue;
                }
                VExceptionFactory.INSTANCE.parseAndThrowException(response);
                String responseEntity = response.getEntity(String.class);
                FreshdeskContact contact = GSON.fromJson(responseEntity, FreshdeskContact.class);
                contact.setContactType(type);
                contact.setFreshdeskContactId(PARSE_LONG.apply(contact.getId()));
                contact.setId(null);
                if (contact.getContact() != null && FreshdeskContact.Type.AGENT.equals(type)) {
                    contact.setEmail(contact.getContact().getEmail());
                    contact.setPhone(contact.getContact().getPhone());
                }
                setVedUserId(contact, type);
                freshdeskContactDao.save(contact);
                contactMap.putIfAbsent(contact.getFreshdeskContactId(), contact);
            }
        }
        return contactMap;
    }

    private Map<String, String> getFreshDeskApiHeader() {
        Map<String, String> headers = new HashMap<>();
        headers.put("Authorization", Base64.getEncoder().encodeToString(ConfigUtils.INSTANCE.getStringValue("freshdesk.api.key").getBytes()));
        return headers;
    }

    private boolean setVedUserId(FreshdeskContact contact, FreshdeskContact.Type type) throws VException {
        if (contact != null &&
                (contact.getVedantuUserId() == null ||
                        (FreshdeskContact.Type.AGENT.equals(type) && contact.getVedantuTeacherUserId() == null))) {
            String teacherEmail = null;
            if (FreshdeskContact.Type.AGENT.equals(type)) {
                teacherEmail = getTeacherEmail(contact.getEmail());
            }
            UserBasicInfo teacherInfo = getUserBasicInfoByEmail(teacherEmail);
            if (teacherInfo != null && teacherInfo.getUserId() != null) {
                contact.setVedantuTeacherUserId(teacherInfo.getUserId());
            }
            UserBasicInfo info = getUserBasicInfoByEmail(contact.getEmail());
            if (info != null && info.getUserId() != null) {
                contact.setVedantuUserId(info.getUserId());
            }
            return (teacherInfo != null && teacherInfo.getUserId() != null) || (info != null && info.getUserId() != null);
        }
        return false;
    }

    public void triggerCareActivity(String activity) throws VException, GeneralSecurityException, SQLException, ParseException, IOException {

        List<EnrollmentPojo> enrolmentInfos;

        Long thruTime = Instant.now().toEpochMilli();

        Long fromTime = thruTime - ActivityEventConstants.MILLI_DAY * 1;

        enrolmentInfos = getEnrolmentInfos(0, 0, fromTime, thruTime);
        processBundleEnrollmentInfos(enrolmentInfos, activity);
        triggerAllActivities();
    }

    public int getGrade(String grade) {

        if (grade.contains("6")) {
            return 6;
        }
        if (grade.contains("7")) {
            return 7;
        }
        if (grade.contains("8")) {
            return 8;
        }
        if (grade.contains("9")) {
            return 9;
        }
        if (grade.contains("10")) {
            return 10;
        }
        if (grade.contains("11")) {
            return 11;
        }
        if (grade.contains("12")) {
            return 12;
        }

        return 0;
    }

    public List<User> getFilteredUserByGradeRange(List<User> activeEnrolledUsers, int fromGrade, int toGrade) {
        return activeEnrolledUsers.parallelStream().filter(user -> {
            StudentInfo studentInfo = user.getStudentInfo();
            String gradeString = studentInfo != null ? studentInfo.getGrade() : "";

            try {
                logger.info("\n\n\ngradeString : {}", gradeString);
                int grade = getGrade(gradeString);
                return (grade >= fromGrade && grade <= toGrade);
            } catch (NumberFormatException | NullPointerException nfe) {
                return false;
            }
        }).collect(Collectors.toList());
    }

    private String cleanPhoneNumber(Object phoneObject) {

        String phone = phoneObject != null? phoneObject.toString() : "";
        if(phone.length()>10){
            phone = phone.replaceAll(" ","");
            phone = phone.replaceAll("-","");
            if(phone.startsWith("0"))
                phone=phone.replaceFirst("0","");
        }
        return phone;
    }

    public Map<String,List<LeadsquaredActivityField>> getOrderCustomValues(List<Orders> orders) throws VException {

        logger.info("======Processing orders getOrderCustomValues : {}", orders.size());

        Map<String,List<LeadsquaredActivityField>> stringListMap =  new HashMap<>();

        List<OrderedItem> orderedItemList = new ArrayList<>();

        for(Orders order : orders){

            if(order.getItems()!=null && CollectionUtils.isNotEmpty(order.getItems())) {
                OrderedItem item = order.getItems().get(0);
                orderedItemList.add(item);
            }
        }

        try{
            Map<String, List<String>> listMap = getEntityGroupInfo(orderedItemList);

            List<String> enrollmentList = listMap.get(EntityType.OTF_BATCH_REGISTRATION.name());
            Map<String, List<BatchBasicInfo>> batchInfoMap = new HashMap<>();
            if(CollectionUtils.isNotEmpty(enrollmentList)){
                try {
                    List<BatchBasicInfo> batchInfo = subscriptionManagerHelper.getBatchBasicInfosByIds(enrollmentList);
                    batchInfoMap = batchInfo.parallelStream()
                            .collect(Collectors.groupingBy(BatchBasicInfo::getId));
                }catch(Exception e){
                    logger.error("Error processing getBatchBasicInfosByIds with the following ids : {}, error message : {}", enrollmentList, e.getMessage(), e);
                }
            }

            List<String> otfList = listMap.get(EntityType.OTF_COURSE_REGISTRATION.name());
            Map<String, List<CourseInfo>> courseInfoMap = new HashMap<>();
            if(CollectionUtils.isNotEmpty(otfList)){
                try{
                    List<CourseInfo> courseInfo = subscriptionManagerHelper.getCoursesBasicInfos(otfList);
                    courseInfoMap = courseInfo.parallelStream()
                            .collect(Collectors.groupingBy(CourseInfo::getId));
                }catch(Exception e){
                    logger.error("Error processing getCoursesBasicInfos with the following ids : {}, error message : {}", otfList, e.getMessage(), e);
                }
            }

            List<String> coursePlanList = listMap.get(EntityType.COURSE_PLAN.name());
            Map<String, CoursePlanBasicInfo> coursePlanInfoMap = new HashMap<>();
            if(CollectionUtils.isNotEmpty(coursePlanList)){
                try{
                    //TODO Change it to bulk call
                    for(String  coursePlanId: coursePlanList){
                        CoursePlanBasicInfo coursePlanInfo = subscriptionManagerHelper.getCoursePlanBasicInfo(coursePlanId, false, true);
                        coursePlanInfoMap.put(coursePlanId, coursePlanInfo);
                    }
                }catch(Exception e){
                    logger.error("Error processing getCoursesBasicInfos with the following ids : {}, error message : {}", otfList, e.getMessage(), e);
                }
            }

            List<String> planList = listMap.get(EntityType.PLAN.name());

            List<String> otfBundleList = listMap.get(EntityType.OTM_BUNDLE.name());
            Map<String, List<OTFBundleInfo>> otfBundleInfosMap = new HashMap<>();
            if(CollectionUtils.isNotEmpty(otfBundleList)){
                try {
                    GetOTFBundlesReq otfReq = new GetOTFBundlesReq();
                    otfReq.setEntityId(otfBundleList);

                    List<OTFBundleInfo> otfBundleInfos = subscriptionManagerHelper.getOTFBundleInfos(otfReq);
                    otfBundleInfosMap = otfBundleInfos.parallelStream()
                            .collect(Collectors.groupingBy(OTFBundleInfo::getId));
                }catch(Exception e){
                    logger.error("Error processing getOTFBundleInfos with the following ids : {}, error message : {}", otfBundleList, e.getMessage(), e);
                }
            }

            List<String> bundlePackageList = listMap.get(EntityType.BUNDLE_PACKAGE.name());
            Map<String, List<BundlePackageDetailsInfo>> bpInfoMap = new HashMap<>();
            if(CollectionUtils.isNotEmpty(bundlePackageList)){
                try {
                    GetBundlesReq getBundlesReq = new GetBundlesReq();
                    getBundlesReq.setBundleIds(bundlePackageList);

                    List<BundlePackageDetailsInfo> bpInfo = subscriptionManagerHelper.getBundlePackageDetailsList(getBundlesReq);
                    bpInfoMap = bpInfo.parallelStream()
                            .collect(Collectors.groupingBy(BundlePackageDetailsInfo::getId));
                }catch(Exception e){
                    logger.error("Error processing getBundlePackageDetailsList with the following ids : {}, error message : {}", bundlePackageList, e.getMessage(), e);
                }
            }

            List<String> bundleList = listMap.get(EntityType.BUNDLE.name());
            Map<String, List<BundleInfo>> bundleInfosMap = new HashMap<>();
            if(CollectionUtils.isNotEmpty(bundleList)){
                try {
                    GetBundlesReq getBundlesReq = new GetBundlesReq();
                    getBundlesReq.setBundleIds(bundleList);
                    List<BundleInfo> bundleInfos = subscriptionManagerHelper.getBundleInfos(getBundlesReq);
                    bundleInfosMap = bundleInfos.parallelStream()
                            .collect(Collectors.groupingBy(BundleInfo::getId));
                }catch(Exception e){
                    logger.error("Error processing getBundleInfos with the following ids : {}, error message : {}", bundleList, e.getMessage(), e);
                }
            }

            for(Orders order : orders){
                try {
                    List<String> req = new ArrayList<>();
                    req.add(order.getId());

                    OrderedItem item = order.getItems().get(0);
                    String title = "";

                    if (CollectionUtils.isNotEmpty(enrollmentList) && item != null && enrollmentList.contains(item.getEntityId())) {
                        if (batchInfoMap.containsKey(item.getEntityId()) && CollectionUtils.isNotEmpty(batchInfoMap.get(item.getEntityId()))) {
                            BatchBasicInfo batchBasicInfos = batchInfoMap.get(item.getEntityId()).get(0);
                            title = batchBasicInfos.getCourseInfo().getTitle();
                        }
                    } else if (CollectionUtils.isNotEmpty(otfList) && item != null && otfList.contains(item.getEntityId())) {
                        if (courseInfoMap.containsKey(item.getEntityId()) && CollectionUtils.isNotEmpty(courseInfoMap.get(item.getEntityId()))) {
                            title = courseInfoMap.get(item.getEntityId()).get(0).getTitle();
                        }
                    } else if (CollectionUtils.isNotEmpty(coursePlanList) && item != null && coursePlanList.contains(item.getEntityId())) {
                        if(coursePlanInfoMap.containsKey(item.getEntityId())){
                            title = coursePlanInfoMap.get(item.getEntityId()).getTitle();
                        }
                    } else if (CollectionUtils.isNotEmpty(planList) && item != null && planList.contains(item.getEntityId())) {
                        title = "PLAN";
                    } else if (CollectionUtils.isNotEmpty(otfBundleList) && item != null && otfBundleList.contains(item.getEntityId())) {
                        if (otfBundleInfosMap.containsKey(item.getEntityId()) && CollectionUtils.isNotEmpty(otfBundleInfosMap.get(item.getEntityId()))) {
                            title = otfBundleInfosMap.get(item.getEntityId()).get(0).getTitle();
                        }
                    } else if (CollectionUtils.isNotEmpty(bundlePackageList) && item != null && bundlePackageList.contains(item.getEntityId())) {
                        if (bpInfoMap.containsKey(item.getEntityId()) && CollectionUtils.isNotEmpty(bpInfoMap.get(item.getEntityId()))) {
                            title = bpInfoMap.get(item.getEntityId()).get(0).getTitle();
                        }
                    } else if (CollectionUtils.isNotEmpty(bundleList) && item != null && bundleList.contains(item.getEntityId())) {
                        if (bundleInfosMap.containsKey(item.getEntityId()) && CollectionUtils.isNotEmpty(bundleInfosMap.get(item.getEntityId()))) {
                            title = bundleInfosMap.get(item.getEntityId()).get(0).getTitle();
                        }
                    }

                    req.add(title);
                    req.add(item.getEntityId());
                    req.add(item.getEntityType().name());
                    req.add(getStringValueInRupee(order.getNonPromotionalAmountPaid()));
                    req.add(getStringValueInRupee(order.getPromotionalAmountPaid()));
                    req.add(getStringValueInRupee(item.getVedantuDiscountAmount()));
                    req.add(LeadsquaredActivityHelper.getLeadSquaredDate(order.getCreationTime()));
                    req.add(getStringValueInRupee(item.getCost()));
                    req.add(getStringValueInRupee(order.getNonPromotionalAmount()));
                    req.add(order.getPaymentStatus().name());
                    req.add(getStringValueInRupee(order.getAmount()));
                    req.add(order.getPaymentSource());

                    stringListMap.put(order.getId(), LeadsquaredActivityHelper.getSchemaValuePair(req));
                }catch (Exception e){
                    logger.error("Error while forming activity fields for not paid orders orderId : , {}",order.getId(), e.getMessage(), e);
                    //Ignore if failed
                }
            }

        }catch (Exception e){
            logger.error("Error while forming activity fields for not paid orders : {}", e.getMessage(), e);
        }
        return stringListMap;
    }

    private Map<String, List<String>> getEntityGroupInfo(List<OrderedItem> items) throws VException {

        Map<String, List<String>> entityInfoMap = new HashMap<>();

        for(OrderedItem item : items) {
            switch (item.getEntityType()) {
                case OTF:
                case OTF_BATCH_REGISTRATION:
                    List<String> enrollmentList = CollectionUtils.putIfAbsentAndGet(entityInfoMap, EntityType.OTF_BATCH_REGISTRATION.name(), ArrayList::new);
                    enrollmentList.add(item.getEntityId());
                    break;
                case OTF_COURSE_REGISTRATION:
                    List<String> otfList = CollectionUtils.putIfAbsentAndGet(entityInfoMap, EntityType.OTF_COURSE_REGISTRATION.name(), ArrayList::new);
                    otfList.add(item.getEntityId());
                    break;
                case COURSE_PLAN:
                case COURSE_PLAN_REGISTRATION:
                case OTO_COURSE_REGISTRATION:
                    List<String> coursePlanList = CollectionUtils.putIfAbsentAndGet(entityInfoMap, EntityType.COURSE_PLAN.name(), ArrayList::new);
                    coursePlanList.add(item.getEntityId());
                    break;
                case PLAN:
                    List<String> planList = CollectionUtils.putIfAbsentAndGet(entityInfoMap,  EntityType.PLAN.name(), ArrayList::new);
                    planList.add(item.getEntityId());
                    break;
                case OTM_BUNDLE:
                case OTM_BUNDLE_ADVANCE_PAYMENT:
                case OTM_BUNDLE_REGISTRATION:
                    List<String> otfBundle = CollectionUtils.putIfAbsentAndGet(entityInfoMap, EntityType.OTM_BUNDLE.name(), ArrayList::new);
                    otfBundle.add(item.getEntityId());
                    break;
                case BUNDLE_PACKAGE:
                    List<String> bundlePackage = CollectionUtils.putIfAbsentAndGet(entityInfoMap, EntityType.BUNDLE_PACKAGE.name(), ArrayList::new);
                    bundlePackage.add(item.getEntityId());
                    break;
                case BUNDLE:
                    List<String> bundleList = CollectionUtils.putIfAbsentAndGet(entityInfoMap, EntityType.BUNDLE.name(), ArrayList::new);
                    bundleList.add(item.getEntityId());
                    break;
                default:

                    break;

            }
        }
        return entityInfoMap;
    }

}
