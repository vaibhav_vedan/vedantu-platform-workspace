package com.vedantu.vedantudata.managers.leadsquared;

import com.fasterxml.jackson.databind.util.BeanUtil;
import com.google.gson.Gson;
import com.vedantu.async.AsyncTaskFactory;
import com.vedantu.async.AsyncTaskParams;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.VException;
import com.vedantu.util.CollectionUtils;
import com.vedantu.util.DateTimeUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.PlatformBasicResponse;
import com.vedantu.vedantudata.async.AsyncTaskName;
import com.vedantu.vedantudata.dao.postgresRO.CustomLeadSquaredAgentDao;
import com.vedantu.vedantudata.dao.serializers.LeadSquaredAgentDao;
import com.vedantu.vedantudata.entities.leadsquared.LeadSquaredAgent;
import com.vedantu.vedantudata.entities.salesconversion.SalesAgentInfo;
import com.vedantu.vedantudata.enums.salesconversion.AgentCohort;
import com.vedantu.vedantudata.enums.salesconversion.AgentDesignation;
import com.vedantu.vedantudata.enums.salesconversion.SalesTeam;
import com.vedantu.vedantudata.pojos.CustomLeadSquaredAgent;
import com.vedantu.vedantudata.pojos.request.AddAgentsEmailReq;
import com.vedantu.vedantudata.utils.CommonUtils;
import com.vedantu.vedantudata.utils.LeadSquaredQueryUtils;
import com.vedantu.vedantudata.utils.PostgressHandler;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.lang.reflect.InvocationTargetException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.temporal.TemporalAdjuster;
import java.time.temporal.TemporalAdjusters;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author Puranjay gade
 */
@Service
public class LeadSquaredAgentDataManger {


    @Autowired
    private LogFactory logFactory;

    @Autowired
    private LeadSquaredAgentDao leadSquaredAgentDao;

    @Autowired
    LeadSquaredQueryUtils leadSquaredQueryUtils;

    @Autowired
    private CustomLeadSquaredAgentDao customLeadSquaredAgentDao;

    @Autowired
    private AsyncTaskFactory asyncTaskFactory;

    @Autowired
    private PostgressHandler postgressHandler;

    private static final Set<String> fosDesinagtions = new HashSet<>(Arrays.asList("CENTER_HEAD", "TEAM_LEADER", "SENIOR_TEAM_LEADER", "REGIONAL_MANAGER", "ZONAL_HEAD"));


    @SuppressWarnings({"static-access"})
    private Logger logger = logFactory.getLogger(LeadSquaredAgentDataManger.class);


    public PlatformBasicResponse executeDataMigrationUpdationOfLeadSquaredAgents() throws SQLException, InvocationTargetException, IllegalAccessException {
        logger.info("staring migration script for agents data");
        logger.info("Fetching the query for FOS agents--->");
        StringBuilder sb = leadSquaredQueryUtils.getLeadSquaredAgentHierarichalDataQuery(SalesTeam.FOS);
        List<CustomLeadSquaredAgent> customAgentsDB = customLeadSquaredAgentDao.getLeadSquaredAgentsFromDB(sb, SalesTeam.FOS);
        logger.info(sb.toString());
        LeadSquaredAgent agent = null;
        for (CustomLeadSquaredAgent customLeadSquaredAgent : customAgentsDB) {
            List<LeadSquaredAgent> oldAgent = leadSquaredAgentDao.findLeadSquaredAgentByUserId(customLeadSquaredAgent.getUserid());
            if (CollectionUtils.isNotEmpty(oldAgent)) {
                agent = oldAgent.get(0);
            } else {
                agent = new LeadSquaredAgent();
            }
            try {
                getLeadSquaredAgentBasicEntityFromCustomAgent(agent, customLeadSquaredAgent);
                assignCohortToLeadSquaredAgent(agent);
            } catch (Exception e) {
                logger.error("Unable to map the agent from redshit db for" + "->" + customLeadSquaredAgent.getUserid() + e);
            }
            getAgentDesignationHierarchy(agent, AgentDesignation.TEAM_LEADER, SalesTeam.FOS, customLeadSquaredAgent.getL1userid(), customLeadSquaredAgent.getL1designation());
            getAgentDesignationHierarchy(agent, AgentDesignation.SENIOR_TEAM_LEADER, SalesTeam.FOS, customLeadSquaredAgent.getL2userid(), customLeadSquaredAgent.getL2designation());
            getAgentDesignationHierarchy(agent, AgentDesignation.CENTER_HEAD, SalesTeam.FOS, customLeadSquaredAgent.getL3userid(), customLeadSquaredAgent.getL3designation());
            getAgentDesignationHierarchy(agent, AgentDesignation.REGIONAL_MANAGER, SalesTeam.FOS, customLeadSquaredAgent.getL4userid(), customLeadSquaredAgent.getL4designation());
            getAgentDesignationHierarchy(agent, AgentDesignation.ZONAL_HEAD, SalesTeam.FOS, customLeadSquaredAgent.getL5userid(), customLeadSquaredAgent.getL5designation());

            leadSquaredAgentDao.saveLeadSquaredAgent(agent);
            JSONObject jsonAgent = new JSONObject(agent);
            logger.info(jsonAgent.toString());

        }

        logger.info("Fetching the query for IS agents--->");
        sb = leadSquaredQueryUtils.getLeadSquaredAgentHierarichalDataQuery(SalesTeam.IS);
        customAgentsDB = customLeadSquaredAgentDao.getLeadSquaredAgentsFromDB(sb, SalesTeam.IS);
        for (CustomLeadSquaredAgent customLeadSquaredAgent : customAgentsDB) {

            List<LeadSquaredAgent> oldAgent = leadSquaredAgentDao.findLeadSquaredAgentByUserId(customLeadSquaredAgent.getUserid());
            if (CollectionUtils.isNotEmpty(oldAgent)) {
                agent = oldAgent.get(0);
            } else {
                agent = new LeadSquaredAgent();
            }
            try {
                getLeadSquaredAgentBasicEntityFromCustomAgent(agent, customLeadSquaredAgent);
                assignCohortToLeadSquaredAgent(agent);
            } catch (Exception e) {
                logger.error("Unable to map the agent from redshit db for" + "->" + customLeadSquaredAgent.getUserid() + e);
            }

            getAgentDesignationHierarchy(agent, AgentDesignation.TEAM_LEADER, SalesTeam.IS, customLeadSquaredAgent.getL1userid(), customLeadSquaredAgent.getL1designation());
            getAgentDesignationHierarchy(agent, AgentDesignation.SENIOR_TEAM_LEADER, SalesTeam.IS, customLeadSquaredAgent.getL2userid(), customLeadSquaredAgent.getL2designation());
            getAgentDesignationHierarchy(agent, AgentDesignation.REVENUE_MANAGER, SalesTeam.IS, customLeadSquaredAgent.getL3userid(), customLeadSquaredAgent.getL3designation());
            getAgentDesignationHierarchy(agent, AgentDesignation.CENTER_HEAD, SalesTeam.IS, customLeadSquaredAgent.getL4userid(), customLeadSquaredAgent.getL4designation());
            getAgentDesignationHierarchy(agent, AgentDesignation.ZONAL_HEAD, SalesTeam.IS, "ce5454d2-846c-11ea-a18c-02a595923f34", "ZONAL_HEAD");

            JSONObject jsonAgent = new JSONObject(agent);
            logger.info(jsonAgent.toString());
            leadSquaredAgentDao.saveLeadSquaredAgent(agent);

        }
        return new PlatformBasicResponse(true, "Success", "");
    }


    public void getLeadSquaredAgentBasicEntityFromCustomAgent(LeadSquaredAgent leadSquaredAgent, CustomLeadSquaredAgent customLeadSquaredAgent) throws SQLException {

        leadSquaredAgent.setAddress(customLeadSquaredAgent.getAddress());
        leadSquaredAgent.setAssociatedphonenumbers(customLeadSquaredAgent.getAssociatedphonenumbers());
        leadSquaredAgent.setAuthtoken(customLeadSquaredAgent.getAuthtoken());
        leadSquaredAgent.setAutouserid(customLeadSquaredAgent.getAutouserid());
        leadSquaredAgent.setAvailabilitystatus(customLeadSquaredAgent.getAvailabilitystatus());
        leadSquaredAgent.setCheckincheckouthistoryid(customLeadSquaredAgent.getCheckincheckouthistoryid());
        leadSquaredAgent.setCity(customLeadSquaredAgent.getCity());
        leadSquaredAgent.setCreatedby(customLeadSquaredAgent.getCreatedby());
        leadSquaredAgent.setDateformat(customLeadSquaredAgent.getDateformat());
        leadSquaredAgent.setCountry(customLeadSquaredAgent.getCountry());
        leadSquaredAgent.setCreatedon(customLeadSquaredAgent.getCreatedon());
        leadSquaredAgent.setDeletionstatuscode(customLeadSquaredAgent.getDeletionstatuscode());
        leadSquaredAgent.setDepartment(customLeadSquaredAgent.getDepartment());
        leadSquaredAgent.setDesignation(customLeadSquaredAgent.getDesignation());
        leadSquaredAgent.setEmailaddress(customLeadSquaredAgent.getEmailaddress());
        leadSquaredAgent.setFirstname(customLeadSquaredAgent.getFirstname());
        leadSquaredAgent.setGroups(customLeadSquaredAgent.getGroups());
        leadSquaredAgent.setHolidaycalendarid(customLeadSquaredAgent.getHolidaycalendarid());
        leadSquaredAgent.setTeam(customLeadSquaredAgent.getTeam());
        leadSquaredAgent.setTeamid(customLeadSquaredAgent.getTeamid());
        leadSquaredAgent.setTelephonyagentid(customLeadSquaredAgent.getTelephonyagentid());
        leadSquaredAgent.setTmp_forgotpassword(customLeadSquaredAgent.getTmp_forgotpassword());
        leadSquaredAgent.setTimezone(customLeadSquaredAgent.getTimezone());
        leadSquaredAgent.setSalesregions(customLeadSquaredAgent.getSalesregions());
        leadSquaredAgent.setSessionid(customLeadSquaredAgent.getSessionid());
        leadSquaredAgent.setSignature_html(customLeadSquaredAgent.getSignature_html());
        leadSquaredAgent.setSignature_text(customLeadSquaredAgent.getSignature_text());
        leadSquaredAgent.setSkills(customLeadSquaredAgent.getSkills());
        leadSquaredAgent.setStatuscode(customLeadSquaredAgent.getStatuscode());
        leadSquaredAgent.setStatusreason(customLeadSquaredAgent.getStatusreason());
        leadSquaredAgent.setState(customLeadSquaredAgent.getState());
        leadSquaredAgent.setManageruserid(customLeadSquaredAgent.getManageruserid());
        leadSquaredAgent.setMiddlename(customLeadSquaredAgent.getMiddlename());
        leadSquaredAgent.setModifiedby(customLeadSquaredAgent.getModifiedby());
        leadSquaredAgent.setModifiedon(customLeadSquaredAgent.getModifiedon());
        leadSquaredAgent.setWorkdaytemplateid(customLeadSquaredAgent.getWorkdaytemplateid());
        leadSquaredAgent.setPassword(customLeadSquaredAgent.getPassword());
        leadSquaredAgent.setPhonemain(customLeadSquaredAgent.getPhonemain());
        leadSquaredAgent.setPhonemobile(customLeadSquaredAgent.getPhonemobile());
        leadSquaredAgent.setPhoneothers(customLeadSquaredAgent.getPhoneothers());
        leadSquaredAgent.setPhotourl(customLeadSquaredAgent.getPhotourl());
        leadSquaredAgent.setIsadministrator(customLeadSquaredAgent.getIsadministrator());
        leadSquaredAgent.setIsagencyuser(customLeadSquaredAgent.getIsagencyuser());
        leadSquaredAgent.setIsbillinguser(customLeadSquaredAgent.getIsbillinguser());
        leadSquaredAgent.setIscheckedin(customLeadSquaredAgent.getIscheckedin());
        leadSquaredAgent.setIscheckinenabled(customLeadSquaredAgent.getIscheckinenabled());
        leadSquaredAgent.setIsdefaultowner(customLeadSquaredAgent.getIsdefaultowner());
        leadSquaredAgent.setIsemailsender(customLeadSquaredAgent.getIsemailsender());
        leadSquaredAgent.setIsphonecallagent(customLeadSquaredAgent.getIsphonecallagent());
        leadSquaredAgent.setRole(customLeadSquaredAgent.getRole());
        leadSquaredAgent.setZipcode(customLeadSquaredAgent.getZipcode());
        leadSquaredAgent.setOfficelocationname(customLeadSquaredAgent.getOfficelocationname());
        leadSquaredAgent.setLastcheckedipaddress(customLeadSquaredAgent.getLastcheckedipaddress());
        leadSquaredAgent.setLastcheckedon(customLeadSquaredAgent.getLastcheckedon());
        leadSquaredAgent.setLastname(customLeadSquaredAgent.getLastname());
        leadSquaredAgent.setViewallleadsofgroup(customLeadSquaredAgent.getViewallleadsofgroup());
        leadSquaredAgent.setUserid(customLeadSquaredAgent.getUserid());
        leadSquaredAgent.setUsertype(customLeadSquaredAgent.getUsertype());
        leadSquaredAgent.setModifyallleadsofgroup(customLeadSquaredAgent.getModifyallleadsofgroup());
        leadSquaredAgent.setOperator(customLeadSquaredAgent.getMx_custom_1());
        leadSquaredAgent.setDid(customLeadSquaredAgent.getMx_custom_2());
        leadSquaredAgent.setEmpID(customLeadSquaredAgent.getMx_custom_3());
        leadSquaredAgent.setPhonename(customLeadSquaredAgent.getMx_custom_4());
        leadSquaredAgent.setAgentID(customLeadSquaredAgent.getMx_custom_5());
        leadSquaredAgent.setDialerprocess(customLeadSquaredAgent.getMx_custom_6());
        leadSquaredAgent.setJoiningdate(customLeadSquaredAgent.getMx_custom_7());
        leadSquaredAgent.setDol(customLeadSquaredAgent.getMx_custom_8());
        leadSquaredAgent.setTeacheriD(customLeadSquaredAgent.getMx_custom_9());
        leadSquaredAgent.setMx_custom_10(customLeadSquaredAgent.getMx_custom_10());
    }

    public LeadSquaredAgent getAgentDesignationHierarchy(LeadSquaredAgent leadSquaredAgent, AgentDesignation agentDesignation, SalesTeam salesTeam, String managerUserId, String genericDesignation) {
        String designation = StringUtils.isNotEmpty(genericDesignation) ? genericDesignation : "";
        String tl = StringUtils.EMPTY;
        String stl = StringUtils.EMPTY;
        String ch = StringUtils.EMPTY;
        String rm = StringUtils.EMPTY;
        String zm = StringUtils.EMPTY;
        switch (salesTeam) {
            case FOS:
                switch (agentDesignation) {
                    case TEAM_LEADER:
                        if (designation.equalsIgnoreCase(AgentDesignation.TEAM_LEADER.toString()) || designation.equalsIgnoreCase(AgentDesignation.SENIOR_TEAM_LEADER.toString())) {
                            leadSquaredAgent.setTL(managerUserId);
                        } else if (designation.equalsIgnoreCase(AgentDesignation.CENTER_HEAD.toString())) {
                            leadSquaredAgent.setTL(StringUtils.EMPTY);
                            leadSquaredAgent.setCH(managerUserId);
                        } else if (designation.equalsIgnoreCase(AgentDesignation.REGIONAL_MANAGER.toString())) {
                            leadSquaredAgent.setTL(StringUtils.EMPTY);
                            leadSquaredAgent.setCH(StringUtils.EMPTY);
                            leadSquaredAgent.setRM(managerUserId);

                        } else if ((designation.equalsIgnoreCase(AgentDesignation.ZONAL_HEAD.toString()))) {
                            leadSquaredAgent.setTL(StringUtils.EMPTY);
                            leadSquaredAgent.setCH(StringUtils.EMPTY);
                            leadSquaredAgent.setRM(StringUtils.EMPTY);
                            leadSquaredAgent.setZM(managerUserId);
                        }
                        break;
                    case SENIOR_TEAM_LEADER:
                        stl = leadSquaredAgent.getSTL();
                        ch = leadSquaredAgent.getCH();
                        rm = leadSquaredAgent.getRM();
                        zm = leadSquaredAgent.getZM();
                        if (designation.equalsIgnoreCase(AgentDesignation.TEAM_LEADER.toString())
                                || designation.equalsIgnoreCase(AgentDesignation.SENIOR_TEAM_LEADER.toString())) {
                            leadSquaredAgent.setSTL(managerUserId);
                        } else if (designation.equalsIgnoreCase(AgentDesignation.CENTER_HEAD.toString())) {
                            leadSquaredAgent.setSTL(StringUtils.isEmpty(stl) ? StringUtils.EMPTY : stl);
                            leadSquaredAgent.setCH(managerUserId);
                        } else if (designation.equalsIgnoreCase(AgentDesignation.REGIONAL_MANAGER.toString())) {
                            leadSquaredAgent.setSTL(StringUtils.isEmpty(stl) ? StringUtils.EMPTY : stl);
                            leadSquaredAgent.setCH(StringUtils.isEmpty(ch) ? StringUtils.EMPTY : ch);
                            leadSquaredAgent.setRM(managerUserId);

                        } else if ((designation.equalsIgnoreCase(AgentDesignation.ZONAL_HEAD.toString()))) {
                            leadSquaredAgent.setSTL(StringUtils.isEmpty(stl) ? StringUtils.EMPTY : stl);
                            leadSquaredAgent.setCH(StringUtils.isEmpty(ch) ? StringUtils.EMPTY : ch);
                            leadSquaredAgent.setRM(StringUtils.isEmpty(rm) ? StringUtils.EMPTY : rm);
                            leadSquaredAgent.setZM(managerUserId);
                        }
                        break;

                    case CENTER_HEAD:
                        ch = leadSquaredAgent.getCH();
                        rm = leadSquaredAgent.getRM();
                        zm = leadSquaredAgent.getZM();
                        if (designation.equalsIgnoreCase(AgentDesignation.CENTER_HEAD.toString())) {
                            leadSquaredAgent.setCH(managerUserId);
                        } else if (designation.equalsIgnoreCase(AgentDesignation.REGIONAL_MANAGER.toString())) {
                            leadSquaredAgent.setCH(StringUtils.isEmpty(ch) ? StringUtils.EMPTY : ch);
                            leadSquaredAgent.setRM(managerUserId);
                        } else if ((designation.equalsIgnoreCase(AgentDesignation.ZONAL_HEAD.toString()))) {
                            leadSquaredAgent.setCH(StringUtils.isEmpty(ch) ? StringUtils.EMPTY : ch);
                            leadSquaredAgent.setRM(StringUtils.isEmpty(rm) ? StringUtils.EMPTY : rm);
                            leadSquaredAgent.setZM(managerUserId);
                        }
                        break;
                    case REGIONAL_MANAGER:
                        rm = leadSquaredAgent.getRM();
                        zm = leadSquaredAgent.getZM();
                        if (designation.equalsIgnoreCase(AgentDesignation.REGIONAL_MANAGER.toString())) {
                            leadSquaredAgent.setRM(managerUserId);
                        } else if ((designation.equalsIgnoreCase(AgentDesignation.ZONAL_HEAD.toString()))) {
                            leadSquaredAgent.setRM(StringUtils.isEmpty(rm) ? StringUtils.EMPTY : rm);
                            leadSquaredAgent.setZM(managerUserId);
                        }
                        break;
                    case ZONAL_HEAD:
                        if ((designation.equalsIgnoreCase(AgentDesignation.ZONAL_HEAD.toString()))) {
                            leadSquaredAgent.setZM(managerUserId);
                        }
                        break;

                }
                break;
            case IS:
                switch (agentDesignation) {
                    case TEAM_LEADER:
                        if (designation.equalsIgnoreCase(AgentDesignation.TEAM_LEADER.toString()) || designation.equalsIgnoreCase(AgentDesignation.SENIOR_TEAM_LEADER.toString())) {
                            leadSquaredAgent.setTL(managerUserId);
                        } else if (designation.equalsIgnoreCase(AgentDesignation.REVENUE_MANAGER.toString())) {
                            leadSquaredAgent.setTL(StringUtils.EMPTY);
                            leadSquaredAgent.setSTL(StringUtils.EMPTY);
                            leadSquaredAgent.setCH(managerUserId);
                        } else if (designation.equalsIgnoreCase(AgentDesignation.CENTER_HEAD.toString())) {
                            leadSquaredAgent.setTL(StringUtils.EMPTY);
                            leadSquaredAgent.setSTL(StringUtils.EMPTY);
                            leadSquaredAgent.setCH(StringUtils.EMPTY);
                            leadSquaredAgent.setRM(managerUserId);
                        }
                        break;
                    case SENIOR_TEAM_LEADER:
                        stl = leadSquaredAgent.getSTL();
                        ch = leadSquaredAgent.getCH();
                        rm = leadSquaredAgent.getRM();
                        if (designation.equalsIgnoreCase(AgentDesignation.TEAM_LEADER.toString())
                                || designation.equalsIgnoreCase(AgentDesignation.SENIOR_TEAM_LEADER.toString())) {
                            leadSquaredAgent.setSTL(managerUserId);
                        } else if (designation.equalsIgnoreCase(AgentDesignation.REVENUE_MANAGER.toString())) {
                            leadSquaredAgent.setSTL(StringUtils.isEmpty(stl) ? StringUtils.EMPTY : stl);
                            leadSquaredAgent.setCH(managerUserId);
                        } else if (designation.equalsIgnoreCase(AgentDesignation.CENTER_HEAD.toString())) {
                            leadSquaredAgent.setSTL(StringUtils.isEmpty(stl) ? StringUtils.EMPTY : stl);
                            leadSquaredAgent.setCH(StringUtils.isEmpty(ch) ? StringUtils.EMPTY : ch);
                            leadSquaredAgent.setRM(managerUserId);
                        }
                        break;
                    case REVENUE_MANAGER:
                        ch = leadSquaredAgent.getCH();
                        rm = leadSquaredAgent.getRM();
                        if (designation.equalsIgnoreCase(AgentDesignation.REVENUE_MANAGER.toString())) {
                            leadSquaredAgent.setCH(managerUserId);
                        } else if (designation.equalsIgnoreCase(AgentDesignation.CENTER_HEAD.toString())) {
                            leadSquaredAgent.setCH(StringUtils.isEmpty(ch) ? StringUtils.EMPTY : ch);
                            leadSquaredAgent.setRM(managerUserId);
                        }
                        break;
                    case CENTER_HEAD:
                        if (designation.equalsIgnoreCase(AgentDesignation.CENTER_HEAD.toString())) {
                            leadSquaredAgent.setRM(managerUserId);
                        }
                        break;
                    case ZONAL_HEAD:
                        leadSquaredAgent.setZM(managerUserId);
                        break;
                }
                break;

        }

        return leadSquaredAgent;


    }

    /*
     * Converting leadsquared object custom values to buisness related values
     */
    public LeadSquaredAgent getLeadSquaredAgentMXCustomFieldMapping(CustomLeadSquaredAgent customLeadSquaredAgent, LeadSquaredAgent agent) {
        agent.setOperator(customLeadSquaredAgent.getMx_custom_1());
        agent.setDid(customLeadSquaredAgent.getMx_custom_2());
        agent.setEmpID(customLeadSquaredAgent.getMx_custom_3());
        agent.setPhonename(customLeadSquaredAgent.getMx_custom_4());
        agent.setAgentID(customLeadSquaredAgent.getMx_custom_5());
        agent.setDialerprocess(customLeadSquaredAgent.getMx_custom_6());
        agent.setJoiningdate(customLeadSquaredAgent.getMx_custom_7());
        agent.setDol(customLeadSquaredAgent.getMx_custom_8());
        agent.setTeacheriD(customLeadSquaredAgent.getMx_custom_9());
        agent.setMx_custom_10(customLeadSquaredAgent.getMx_custom_10());
        return agent;
    }


    /**
     * executing the task in thread pool
     */
    public PlatformBasicResponse executeDataMigrationUpdationOfLeadSquaredAgentsAsync() {
        Map<String, Object> payload = new HashMap<>();
        AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.TRIGGER_LS_USERS_DATA_MIGRATION, payload);
        asyncTaskFactory.executeTask(params);

        return new PlatformBasicResponse(true, "Success", "");
    }

    public Boolean addAgentsEmailForIntermittentScreen(AddAgentsEmailReq addAgentsEmailReq) throws VException {

        List<LeadSquaredAgent> leadSquaredAgents = leadSquaredAgentDao.getAgentByEmailIds(new ArrayList<>(addAgentsEmailReq.getTeacherEmailIds()));
        if (CollectionUtils.isNotEmpty(leadSquaredAgents)) {
            List<String> emailIds = leadSquaredAgents.parallelStream().filter(p -> com.vedantu.util.StringUtils.isNotEmpty(p.getEmailaddress())).map(p -> p.getEmailaddress()).collect(Collectors.toList());
            Set<String> incomingAgentList = addAgentsEmailReq.getTeacherEmailIds();
            incomingAgentList.removeAll(emailIds);
            if (CollectionUtils.isNotEmpty(incomingAgentList)) {
                throw new VException(ErrorCode.BAD_REQUEST_ERROR, "Agents : " + incomingAgentList + " not present in system");
            }
            for (LeadSquaredAgent leadSquaredAgent : leadSquaredAgents) {
                leadSquaredAgent.setIsIntermittentScreenAllowed(addAgentsEmailReq.getIsScreenAllowed());
                leadSquaredAgentDao.addAgentsEmailForIntermittentScreen(leadSquaredAgent);
            }
        } else {
            throw new VException(ErrorCode.BAD_REQUEST_ERROR, "Agents : " + addAgentsEmailReq.getTeacherEmailIds() + " not present in system");
        }
        return true;
    }

    public Boolean isIntermittentScreenAllowed(String emailId) {
        emailId = emailId.replaceAll("\\+.*@", "@");
        logger.info("isIntermittentScreenAllowed req email: {}", emailId);
        List<LeadSquaredAgent> leadSquaredAgents = leadSquaredAgentDao.getIntermittentScreenAllowedAgent(emailId);
        logger.info("Agent Info: {}", leadSquaredAgents);
        if (CollectionUtils.isEmpty(leadSquaredAgents)) {
            return false;
        } else {
            return true;
        }
    }

    public void assignCohortToLeadSquaredAgent(LeadSquaredAgent leadSquaredAgent) throws VException, ParseException {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date(System.currentTimeMillis()));

            logger.info("Day of the month is {}",calendar.get(Calendar.DAY_OF_MONTH));
            logger.info("Date of first of this month {}",CommonUtils.calculateTimeStampIst( LocalDate.now(ZoneId.of(DateTimeUtils.TIME_ZONE_IN))
                    .with(TemporalAdjusters.firstDayOfMonth()).atStartOfDay().toInstant(ZoneOffset.UTC).toEpochMilli()));
            String dateOfJoining = leadSquaredAgent.getJoiningdate();
            if (StringUtils.isNotEmpty(dateOfJoining)) {
                leadSquaredAgent
                        .setCohort(
                                getCohortFromDaysDiff(CommonUtils
                                        .getDaysDiffernceFromMillis(CommonUtils.parseTimeIST(dateOfJoining),
                                                LocalDate.now(ZoneId.of(DateTimeUtils.TIME_ZONE_IN))
                                                        .with(TemporalAdjusters.firstDayOfMonth()).atStartOfDay().toInstant(ZoneOffset.UTC).toEpochMilli())
                                        .intValue()));
            } else {
                leadSquaredAgent.setCohort("NA");
            }

    }

    public String getCohortFromDaysDiff(Integer noOfDays) {
    	logger.info("No of days:"+noOfDays);
        if (noOfDays <= 0) {
            return cohots.M0;
        } else {

            switch (AgentCohort.from(noOfDays)) {
                case M1:
                    return cohots.M1;
                case M2:
                    return cohots.M2;
                case M3:
                    return cohots.M3;
                case M4:
                    return cohots.M4;
                case M5:
                    return cohots.M5;
                case M5Plus:
                    return cohots.M5Plus;
            }

        }
        return "";

    }

    private static class cohots {
        private static final String M0 = "M0";
        private static final String M1 = "M1";
        private static final String M2 = "M2";
        private static final String M3 = "M3";
        private static final String M4 = "M4";
        private static final String M5 = "M5";
        private static final String M5Plus = "M5+";
    }
}
