package com.vedantu.vedantudata.managers.leadsquared;

import java.util.List;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.google.gson.Gson;
import com.vedantu.util.LogFactory;
import com.vedantu.vedantudata.dao.serializers.LeadsquaredTaskDAO;
import com.vedantu.vedantudata.entities.leadsquared.LeadTask;
import com.vedantu.vedantudata.utils.LeadsquaredConfig;

@Service
public class LeadsquaredTaskManager extends LeadsquaredConfig {

	@Autowired
	private LogFactory logFactory;

	@SuppressWarnings("static-access")
	private Logger logger = logFactory.getLogger(LeadsquaredTaskManager.class);

	@Autowired
	private LeadsquaredTaskDAO leadsquaredTaskDAO;

	private static Gson gson = new Gson();

	public void updateLeadTask(LeadTask leadTask) throws Exception {
		logger.info("leadTask:" + leadTask.toString());
		LeadTask existingEntry = fetchByTaskId(leadTask.getTaskId());

		// Update the entries
		if (existingEntry != null) {
			leadTask.setId(existingEntry.getId());
			leadTask.setCreationTime(existingEntry.getCreationTime());
		}

		leadTask.updateTimings();
		leadsquaredTaskDAO.save(leadTask);
	}

	public LeadTask fetchByTaskId(String taskId) {
		Query query = new Query();
		query.addCriteria(Criteria.where(LeadTask.Constants.TASK_ID).is(taskId));

		List<LeadTask> leads = leadsquaredTaskDAO.runQuery(query, LeadTask.class);
		if (!CollectionUtils.isEmpty(leads)) {
			return leads.get(0);
		}

		return null;
	}
}
