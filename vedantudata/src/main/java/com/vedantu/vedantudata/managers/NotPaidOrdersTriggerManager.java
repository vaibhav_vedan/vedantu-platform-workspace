package com.vedantu.vedantudata.managers;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.User.User;
import com.vedantu.User.UserBasicInfo;
import com.vedantu.async.AsyncTaskFactory;
import com.vedantu.async.AsyncTaskParams;
import com.vedantu.dinero.enums.PaymentStatus;
import com.vedantu.dinero.pojo.Orders;
import com.vedantu.dinero.request.GetOrdersReq;
import com.vedantu.exception.VException;
import com.vedantu.exception.VExceptionFactory;
import com.vedantu.util.*;
import com.vedantu.vedantudata.async.AsyncTaskName;
import com.vedantu.vedantudata.dao.ActivityDao;
import com.vedantu.vedantudata.entities.ActivityDetails;
import com.vedantu.vedantudata.utils.ActivityEventConstants;
import com.vedantu.vedantudata.utils.LeadsquaredActivityHelper;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

import java.lang.reflect.Type;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class NotPaidOrdersTriggerManager {

    @Autowired
    private ActivityManager activityManager;

    @Autowired
    private ActivityDao activityDao;

    @Autowired
    private FosUtils fosUtils;

    @Autowired
    private AsyncTaskFactory asyncTaskFactory;

    @Autowired
    private LeadsquaredActivityHelper leadsquaredActivityHelper;

    private Logger logger = LogFactory.getLogger(NotPaidOrdersTriggerManager.class);

    private static final String DINERO_ENDPOINT = ConfigUtils.INSTANCE.getStringValue("DINERO_ENDPOINT");

    public void processAsync() {
        Map<String, Object> payload = new HashMap<>();
        AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.TRIGGER_LS_NOT_PAID_ORDERS_ACTIVITIES, payload);
        asyncTaskFactory.executeTask(params);
    }

    public void process() throws VException {

        ActivityDetails lastActivityDetails = activityDao.getLastActivity(ActivityEventConstants.EVENT_ORDERS);

        //Default start time
        Long lastOrderTime = System.currentTimeMillis() - ((ActivityEventConstants.MILLI_HOUR));

        //Get the previous triggered order time
        if(lastActivityDetails != null) {
            // 2 minutes buffer added
            lastOrderTime = lastActivityDetails.getCreationTime() - ActivityEventConstants.MILLI_ORDER_NOT_PAID_TIMEOUT - (ActivityEventConstants.MILLI_MINUTE * 5);

            if(lastOrderTime < (System.currentTimeMillis() - (ActivityEventConstants.MILLI_DAY*2)))
                lastOrderTime = System.currentTimeMillis() - (ActivityEventConstants.MILLI_DAY*2);
        }

        List<Orders> notPaidOrders = null;
        int skip = 0;
        int size = ActivityEventConstants.ORDER_CHUNK_SIZE;
        do {
            GetOrdersReq getOrdersReq = new GetOrdersReq();
            getOrdersReq.setPaymentStatus(PaymentStatus.NOT_PAID);
            getOrdersReq.setStartTime(lastOrderTime);
            getOrdersReq.setEndTime(System.currentTimeMillis() - ActivityEventConstants.MILLI_ORDER_NOT_PAID_TIMEOUT);
            getOrdersReq.setStart(skip);
            getOrdersReq.setSize(size);
            ClientResponse resp = WebUtils.INSTANCE.doCall(DINERO_ENDPOINT + "/payment/getOrders", HttpMethod.POST,
                    new Gson().toJson(getOrdersReq), true);
            VExceptionFactory.INSTANCE.parseAndThrowException(resp);
            String jsonString = resp.getEntity(String.class);

            logger.info("\n\n\njsonString : {}", jsonString);

            Type listType = new TypeToken<ArrayList<Orders>>() {
            }.getType();

            notPaidOrders = new Gson().fromJson(jsonString, listType);
            skip += ActivityEventConstants.ORDER_CHUNK_SIZE;
            size = skip + ActivityEventConstants.ORDER_CHUNK_SIZE;
            processActivities(notPaidOrders);

        }while (notPaidOrders !=null && notPaidOrders.size() == ActivityEventConstants.ORDER_CHUNK_SIZE);

        leadsquaredActivityHelper.triggerAllActivities();
    }

    private void processActivities(List<Orders> notPaidOrders) throws VException {

        if(CollectionUtils.isEmpty(notPaidOrders))
            return;

        List<String> orderIds = notPaidOrders.parallelStream()
                .map(order -> order.getId()).collect(Collectors.toList());

        logger.info("======Processing orders with following ids : {}", orderIds);

        List<ActivityDetails> triggeredOrders = activityDao.getOrderNotPaidActivityByOrderIds(ActivityEventConstants.EVENT_ORDERS, orderIds);

        List<String> triggeredOrderIds = triggeredOrders.parallelStream()
                .filter(order -> order.getActivityNote() != null)
                .map(order -> order.getActivityNote())
                .collect(Collectors.toList());

        notPaidOrders = notPaidOrders.stream().
                filter(order -> !triggeredOrderIds.contains(order.getId()))
                .collect(Collectors.toList());

        if(CollectionUtils.isEmpty(notPaidOrders))
            return;

        Set<String> userIds =  notPaidOrders.parallelStream()
                .map(order -> order.getUserId().toString()).collect(Collectors.toSet());

        List<UserBasicInfo>  users = fosUtils.getUserBasicInfosSet(userIds,true);

        Map<Long, List<UserBasicInfo>> usersMap = users.parallelStream()
                .collect(Collectors.groupingBy(UserBasicInfo::getUserId));

        activityManager.pushOrderActivity(ActivityEventConstants.EVENT_ORDERS,notPaidOrders, usersMap);

    }
}
