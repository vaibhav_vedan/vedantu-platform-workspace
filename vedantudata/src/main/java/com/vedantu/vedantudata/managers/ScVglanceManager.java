package com.vedantu.vedantudata.managers;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.User.UserBasicInfo;
import com.vedantu.board.pojo.Board;
import com.vedantu.dinero.enums.PaymentStatus;
import com.vedantu.dinero.pojo.Orders;
import com.vedantu.dinero.request.GetOrdersReq;
import com.vedantu.exception.InternalServerErrorException;
import com.vedantu.exception.VException;
import com.vedantu.exception.VExceptionFactory;
import com.vedantu.isl.response.GetLatestVSATTestDetailsByUserIdRes;
import com.vedantu.lms.request.GetContentsReq;
import com.vedantu.onetofew.enums.*;
import com.vedantu.onetofew.pojo.GTTAttendeeDetailsInfo;
import com.vedantu.onetofew.pojo.OTFSessionInfo;
import com.vedantu.scheduling.request.session.GetOTFSessionByIdsReq;
import com.vedantu.session.pojo.EntityType;
import com.vedantu.subscription.pojo.BundleInfo;
import com.vedantu.subscription.request.GTTAttendeeRequest;
import com.vedantu.util.*;
import com.vedantu.vedantudata.dao.ScVglanceDao;
import com.vedantu.vedantudata.dao.serializers.LeadSquaredAgentDao;
import com.vedantu.vedantudata.entities.leadsquared.LeadDetails;
import com.vedantu.vedantudata.entities.leadsquared.LeadSquaredAgent;
import com.vedantu.vedantudata.entities.leadsquared.LeadsquaredUser;
import com.vedantu.vedantudata.entities.salesconversion.UserSessionDetails;
import com.vedantu.vedantudata.managers.leadsquared.LeadsquaredManager;
import com.vedantu.vedantudata.pojos.AttendedSessionInfo;
import com.vedantu.vedantudata.pojos.ContentInfo;
import com.vedantu.vedantudata.pojos.request.GetLatestVSATTestDetailsReq;
import com.vedantu.vedantudata.pojos.request.SalesVglanceReq;
import com.vedantu.vedantudata.pojos.response.*;
import com.vedantu.vedantudata.utils.AwsSQSManager;
import org.apache.commons.lang.time.DateUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

import java.lang.reflect.Type;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class ScVglanceManager {

    @Autowired
    private LogFactory logFactory;

    @Autowired
    private FosUtils fosUtils;

    @Autowired
    private LeadsquaredManager leadsquaredManager;

    @Autowired
    private AwsSQSManager awsSQSManager;

    @Autowired
    private ScVglanceDao scVglanceDao;

    @Autowired
    private LeadSquaredAgentDao leadSquaredAgentDao;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(ScVglanceManager.class);

    private static final String DINERO_ENDPOINT = ConfigUtils.INSTANCE.getStringValue("DINERO_ENDPOINT");
    private static final String SCHEDULING_ENDPOINT = ConfigUtils.INSTANCE.getStringValue("SCHEDULING_ENDPOINT");
    private static final String ISL_ENDPOINT = ConfigUtils.INSTANCE.getStringValue("ISL_ENDPOINT");
    private static final String SUBSCRIPTION_ENDPOINT = ConfigUtils.INSTANCE.getStringValue("SUBSCRIPTION_ENDPOINT");
    private static final String LMS_ENDPOINT = ConfigUtils.INSTANCE.getStringValue("LMS_ENDPOINT");
    private static final Integer HOUR = 1000 * 60 * 60;
    private static final Integer MINUTE = 1000 * 60;
    private static final DecimalFormat DECIMAL_FORMAT = new DecimalFormat("#.##");

    public Boolean isStudentBelongstoCounsellor(String studentEmail, String studentContactNumber,String counsellorEmail) throws InterruptedException, InternalServerErrorException {
        //to remove +t,+t1 from agent email
        counsellorEmail = counsellorEmail.replaceAll("\\+.*@", "@");
        List<LeadDetails> studentLeadDetails = leadsquaredManager.fetchLeadsByPhoneOrEmail(studentContactNumber);
        List<LeadsquaredUser> counsellorDetails = leadsquaredManager.fetchAgentsByEmailId(counsellorEmail);

        if (CollectionUtils.isNotEmpty(studentLeadDetails) && CollectionUtils.isNotEmpty(counsellorDetails) &&
                (counsellorDetails.get(0).getUserId().equals(studentLeadDetails.get(0).getOwnerId()) || isUserCounsellorsSupervisor(counsellorEmail, studentLeadDetails.get(0).getOwnerId()))) {
            logger.info("counsellor: {},", counsellorDetails.get(0));
            logger.info("student: {},", studentLeadDetails.get(0));
            logger.info("Info details student ownerId: {}, counserlorr Id: {}", studentLeadDetails.get(0).getOwnerId(), counsellorDetails.get(0).getUserId());
            return true;
        } else {
            return false;
        }
    }

    private boolean isUserCounsellorsSupervisor(String supervisorEmailId, String counsellorId) {
        logger.info("Inside isUserCounsellorsSupervisor");
        List<LeadSquaredAgent> leadSquaredAgents = leadSquaredAgentDao.getAgentByUserIds(Arrays.asList(counsellorId),
                Arrays.asList(LeadSquaredAgent.Constants.CH, LeadSquaredAgent.Constants.TL, LeadSquaredAgent.Constants.RM, LeadSquaredAgent.Constants.ZM));
        if (CollectionUtils.isNotEmpty(leadSquaredAgents)) {
            LeadSquaredAgent leadSquaredAgent = leadSquaredAgents.get(0);
            Set<String> agentUserIds = new HashSet<>();
            if (StringUtils.isNotEmpty(leadSquaredAgent.getCH())) {
                agentUserIds.add(leadSquaredAgent.getCH());
            }
            if (StringUtils.isNotEmpty(leadSquaredAgent.getTL())) {
                agentUserIds.add(leadSquaredAgent.getTL());
            }
            if (StringUtils.isNotEmpty(leadSquaredAgent.getRM())) {
                agentUserIds.add(leadSquaredAgent.getRM());
            }
            if (StringUtils.isNotEmpty(leadSquaredAgent.getZM())) {
                agentUserIds.add(leadSquaredAgent.getZM());
            }
            logger.info("supervisor ids : {}", agentUserIds);
            if (CollectionUtils.isNotEmpty(agentUserIds)) {
                List<LeadSquaredAgent> leadSquaredAgentSupervisor = leadSquaredAgentDao.getAgentByUserIds(new ArrayList<>(agentUserIds), Arrays.asList(LeadSquaredAgent.Constants.EMAIL_ADDRESS));
                if (CollectionUtils.isNotEmpty(leadSquaredAgentSupervisor)) {
                    List<LeadSquaredAgent> filterLeadSquaredAgentSupervisor = leadSquaredAgentSupervisor.parallelStream().filter(p -> supervisorEmailId.equals(p.getEmailaddress())).collect(Collectors.toList());
                    logger.info("lead agent details: {}", filterLeadSquaredAgentSupervisor);
                    if (CollectionUtils.isNotEmpty(filterLeadSquaredAgentSupervisor) && filterLeadSquaredAgentSupervisor.size() > 0) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public GetSessionChecksRes getSessionChecks(SalesVglanceReq salesVglanceReq) throws VException {

        Long userId = salesVglanceReq.getUserId();
        boolean isLiveClass = false;
        boolean isMasterClass = false;
        Boolean isMicroCourse = isMicrocourseAttended(userId);
        Boolean isFreeTrial = getFreeTrialInfo(userId, salesVglanceReq.getStartTime());

        logger.info("*****Master/Live class started*****");
        List<GTTAttendeeDetailsInfo> gttAttendeeDetailsInfos = getGttAttendeeDetails(userId, salesVglanceReq.getStartTime());
        if (CollectionUtils.isNotEmpty(gttAttendeeDetailsInfos)) {

            List<String> sessionIds = gttAttendeeDetailsInfos.parallelStream().
                    filter(p -> p.getTimeInSession() > 0 && !AttendeeContext.LATE_JOIN.equals(p.getAttendeeType())).
                    map(GTTAttendeeDetailsInfo::getSessionId).filter(Objects::nonNull).
                    collect(Collectors.toList());

            List<OTFSessionInfo> otfSessionInfos = getSessionDetails(sessionIds, Arrays.asList("_id", "sessionContextType", "state", "flags", "boardId"));
            if (CollectionUtils.isNotEmpty(otfSessionInfos)) {

                List<OTFSessionInfo> commonSessionFilters = otfSessionInfos.parallelStream().
                        filter(p -> SessionState.SCHEDULED.equals(p.getState()) && !p.isSalesDemoSession() &&
                                !com.vedantu.onetofew.enums.EntityType.BIG_SESSION.equals(p.getType())).
                        collect(Collectors.toList());

                if (CollectionUtils.isNotEmpty(commonSessionFilters)) {
                    List<OTFSessionInfo> masterSessions = commonSessionFilters.parallelStream().
                            filter(OTFSessionInfo::isWebinarSession).
                            collect(Collectors.toList());
                    if (CollectionUtils.isNotEmpty(masterSessions)) {
                        isMasterClass = true;
                    }
                    List<OTFSessionInfo> liveSessions = commonSessionFilters.parallelStream().
                            filter(p -> null != p.getBoardId()).
                            collect(Collectors.toList());
                    if (CollectionUtils.isNotEmpty(liveSessions)) {
                        isLiveClass = true;
                    }
                }
            }
        }
        logger.info("*****Master/Live class ended*****");
        return new GetSessionChecksRes(isLiveClass, isMasterClass, isMicroCourse, isFreeTrial);
    }

    private Boolean isMicrocourseAttended(Long userId) throws VException {
        logger.info("*****Micro course started*****");
        String url = DINERO_ENDPOINT + "/payment/getOrders";

        GetOrdersReq getOrdersReq = new GetOrdersReq();
        getOrdersReq.setUserId(userId);
        getOrdersReq.setCreationTimeLowerLimit(Timestamp.valueOf(getFromDate()).getTime());
        getOrdersReq.setCreationTimeUpperLimit(System.currentTimeMillis());
        getOrdersReq.setPaymentStatuses(Arrays.asList(PaymentStatus.PAID, PaymentStatus.PARTIALLY_PAID, PaymentStatus.FORFEITED, PaymentStatus.PAYMENT_SUSPENDED));

        ClientResponse resp = WebUtils.INSTANCE.doCall(url, HttpMethod.POST, new Gson().toJson(getOrdersReq));
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        Type orderList = new TypeToken<List<Orders>>() {
        }.getType();
        List<Orders> orders = new Gson().fromJson(jsonString, orderList);
        if (CollectionUtils.isNotEmpty(orders)) {

            Set<String> entityIds = orders.parallelStream().filter(p -> CollectionUtils.isNotEmpty(p.getItems())).
                    filter(p -> EntityType.BUNDLE.equals(p.getItems().get(0).getEntityType())).
                    map(p -> p.getItems().get(0).getEntityId()).
                    filter(Objects::nonNull).collect(Collectors.toSet());
            logger.info("entityIds: {}", entityIds);

            if (CollectionUtils.isNotEmpty(entityIds)) {
                String req = SUBSCRIPTION_ENDPOINT + "/bundle/get/getPublicBundleInfosForBundleIds?bundleIds=" + org.springframework.util.StringUtils.collectionToDelimitedString(entityIds, ",");
                ClientResponse bundleResp = WebUtils.INSTANCE.doCall(req, HttpMethod.GET, null);
                VExceptionFactory.INSTANCE.parseAndThrowException(bundleResp);
                String bundkeJsonString = bundleResp.getEntity(String.class);
                Type bundleInfo = new TypeToken<List<BundleInfo>>() {
                }.getType();
                List<BundleInfo> bundleInfos = new Gson().fromJson(bundkeJsonString, bundleInfo);
                logger.info("bundle : {}", new Gson().toJson(bundleInfos));
                if (CollectionUtils.isNotEmpty(bundleInfos)) {
                    List<BundleInfo> filterBundleInfos = bundleInfos.parallelStream().filter(p -> CollectionUtils.isNotEmpty(p.getSearchTerms())).
                            filter(p -> p.getSearchTerms().contains(CourseTerm.CO_CURRICULAR) || p.getSearchTerms().contains(CourseTerm.MICRO_COURSES)).
                            filter(Objects::nonNull).collect(Collectors.toList());
                    logger.info("filterBundles : {}", filterBundleInfos);
                    if (CollectionUtils.isNotEmpty(filterBundleInfos)) {
                        return true;
                    }
                }

            }
        }
        logger.info("*****Micro course Ended*****");
        return false;
    }

    private List<OTFSessionInfo> getSessionDetails(List<String> sessionIds, List<String> includeSet) throws
            VException {
        GetOTFSessionByIdsReq getOTFSessionByIdsReq = new GetOTFSessionByIdsReq(sessionIds, includeSet);
        //TODO:: Need to add proper mechanism
        getOTFSessionByIdsReq.setStart(0);
        getOTFSessionByIdsReq.setSize(50);
        String url = SCHEDULING_ENDPOINT + "/onetofew/session/getSessionByIds";
        ClientResponse resp = WebUtils.INSTANCE.doCall(url, HttpMethod.POST, new Gson().toJson(getOTFSessionByIdsReq));
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        Type sessionInfo = new TypeToken<List<OTFSessionInfo>>() {
        }.getType();
        List<OTFSessionInfo> otfSessionInfos = new Gson().fromJson(jsonString, sessionInfo);
        if (CollectionUtils.isNotEmpty(otfSessionInfos)) {
            return otfSessionInfos;
        } else {
            return null;
        }
    }

    private Boolean getFreeTrialInfo(Long userId, Long startTime) throws VException {
        logger.info("*****Free Trial Info started*****");
        String url = DINERO_ENDPOINT + "/payment/getLatestTrialOrderByUserId?userId=" + userId + "&startTime=" + startTime;
        ClientResponse resp = WebUtils.INSTANCE.doCall(url, HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        Orders order = new Gson().fromJson(jsonString, Orders.class);
        logger.info("*****Free Trial Info ended*****");
        if (order != null) {
            return true;
        } else {
            return false;
        }
    }

    private List<GTTAttendeeDetailsInfo> getGttAttendeeDetails(Long userId, Long startTime) throws VException {

        GTTAttendeeRequest gttAttendeeRequest = new GTTAttendeeRequest();
        gttAttendeeRequest.setFromTime(startTime);
        gttAttendeeRequest.setThruTime(System.currentTimeMillis());
        gttAttendeeRequest.setUserIds(Arrays.asList(String.valueOf(userId)));

        String url = SCHEDULING_ENDPOINT + "/gttattendee/getGttAttendeeForUsers";
        ClientResponse resp = WebUtils.INSTANCE.doCall(url, HttpMethod.POST, new Gson().toJson(gttAttendeeRequest));
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        Type gttAttendeeInfo = new TypeToken<List<GTTAttendeeDetailsInfo>>() {
        }.getType();
        List<GTTAttendeeDetailsInfo> gttAttendeeDetailsInfos = new Gson().fromJson(jsonString, gttAttendeeInfo);
        if (CollectionUtils.isNotEmpty(gttAttendeeDetailsInfos)) {
            return gttAttendeeDetailsInfos;
        } else {
            return null;
        }
    }

    private LocalDateTime getFromDate() {
        LocalDateTime localDateTo = Instant.ofEpochMilli(System.currentTimeMillis())
                .atZone(ZoneId.systemDefault())
                .toLocalDateTime();
        return localDateTo.minusDays(31);
    }

    public GetMasterLiveClassDetailsRes getMasterLiveClassDetails(SalesVglanceReq salesVglanceReq) throws
            VException {

        Long userId = salesVglanceReq.getUserId();
        GetMasterLiveClassDetailsRes getMasterLiveClassDetailsRes = new GetMasterLiveClassDetailsRes();
        List<GTTAttendeeDetailsInfo> gttAttendeeDetailsInfos = getGttAttendeeDetails(userId, salesVglanceReq.getStartTime());
        if (CollectionUtils.isNotEmpty(gttAttendeeDetailsInfos)) {

            List<String> sessionIds = gttAttendeeDetailsInfos.parallelStream().
                    filter(p -> p.getTimeInSession() > 0 && !AttendeeContext.LATE_JOIN.equals(p.getAttendeeType())).
                    map(GTTAttendeeDetailsInfo::getSessionId).filter(Objects::nonNull).
                    collect(Collectors.toList());

            List<OTFSessionInfo> otfSessionInfos = getSessionDetails(sessionIds, Arrays.asList("_id", "sessionContextType", "state", "type", "flags", "boardId", "title", "presenter"));
            logger.info("session infos: {}", new Gson().toJson(otfSessionInfos));

            if (CollectionUtils.isNotEmpty(otfSessionInfos)) {

                List<OTFSessionInfo> otfSessionFilters = otfSessionInfos.parallelStream().
                        filter(p -> SessionState.SCHEDULED.equals(p.getState()) && !com.vedantu.onetofew.enums.EntityType.BIG_SESSION.equals(p.getType()) &&
                                !p.isSalesDemoSession() && (null != p.getBoardId() || p.isWebinarSession())).
                        collect(Collectors.toList());
                logger.info("filtered session infos: {}", new Gson().toJson(otfSessionFilters));

                if (CollectionUtils.isNotEmpty(otfSessionFilters)) {

                    OTFSessionInfo otfSessionInfo = otfSessionFilters.get(0);
                    GTTAttendeeDetailsInfo gttAttendeeDetailsInfo = getGttAttendeeOfSession(gttAttendeeDetailsInfos, otfSessionInfo.getId());

                    if (gttAttendeeDetailsInfo.getHotspotNumbers() != null &&
                            gttAttendeeDetailsInfo.getHotspotNumbers().getNumberAttempted() != null &&
                            gttAttendeeDetailsInfo.getHotspotNumbers().getNumberAttempted() > 0) {
                        getMasterLiveClassDetailsRes.setIsHotspotAttempted(true);
                    }
                    if (gttAttendeeDetailsInfo.getQuizNumbers() != null &&
                            gttAttendeeDetailsInfo.getQuizNumbers().getNumberAttempted() != null &&
                            gttAttendeeDetailsInfo.getQuizNumbers().getNumberAttempted() > 0) {
                        getMasterLiveClassDetailsRes.setIsQuizAttempted(true);
                    }
                    if (gttAttendeeDetailsInfo.getSessionEndTime() != null) {
                        getMasterLiveClassDetailsRes.setAttendedOn(gttAttendeeDetailsInfo.getSessionEndTime());
                    }
                    getMasterLiveClassDetailsRes.setTitle(org.apache.commons.lang.StringUtils.defaultString(otfSessionInfo.getTitle(), ""));
                    if (otfSessionInfo.getPresenter() != null) {
                        UserBasicInfo presenterInfo = fosUtils.getUserBasicInfo(otfSessionInfo.getPresenter(), true);
                        getMasterLiveClassDetailsRes.setTeacher(presenterInfo.getFullName());
                    }
                    if (otfSessionInfo.getBoardId() != null) {
                        Set<Long> boardId = new HashSet<>();
                        boardId.add(otfSessionInfo.getBoardId());
                        Map<Long, Board> idToBoardMap = fosUtils.getBoardInfoMap(boardId);
                        getMasterLiveClassDetailsRes.setSubject(idToBoardMap.get(otfSessionInfo.getBoardId()).getName());
                    }
                }
            }
        }
        return getMasterLiveClassDetailsRes;
    }

    private GTTAttendeeDetailsInfo getGttAttendeeOfSession(List<GTTAttendeeDetailsInfo> gttAttendeeDetailsInfos, String id) {
        for (GTTAttendeeDetailsInfo gtt : gttAttendeeDetailsInfos) {
            if (StringUtils.equals(id, gtt.getSessionId())) {
                return gtt;
            }
        }
        return null;
    }

    public GetVSATDetailsRes getVSATDetails(SalesVglanceReq salesVglanceReq) throws VException {

        Long userId = salesVglanceReq.getUserId();
        GetVSATDetailsRes getVSATDetailsRes = new GetVSATDetailsRes();
        String subUrl = ISL_ENDPOINT + "/getLatestVSATTestDetailsByUserId";
        GetLatestVSATTestDetailsReq getLatestVSATTestDetailsReq = new GetLatestVSATTestDetailsReq();
        getLatestVSATTestDetailsReq.setUserId(userId);
        getLatestVSATTestDetailsReq.setStartTime(Timestamp.valueOf(getFromDate()).getTime());
        getLatestVSATTestDetailsReq.setEndTime(System.currentTimeMillis());

        ClientResponse resp = WebUtils.INSTANCE.doCall(subUrl, HttpMethod.POST, new Gson().toJson(getLatestVSATTestDetailsReq));
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        PlatformBasicResponse platformBasicResponse = new Gson().fromJson(jsonString, PlatformBasicResponse.class);
        logger.info("platfom response: {}", platformBasicResponse);
        if (platformBasicResponse == null || !platformBasicResponse.isSuccess()) {
            logger.info("ISL response for user: {}, is {}", userId, platformBasicResponse);
            getVSATDetailsRes.setIsVSATAttended(false);
            return getVSATDetailsRes;
        }
        GetLatestVSATTestDetailsByUserIdRes getLatestVSATTestDetailsByUserIdRes = new Gson().fromJson(platformBasicResponse.getResponse(), GetLatestVSATTestDetailsByUserIdRes.class);
        logger.info("get : {}", new Gson().toJson(getLatestVSATTestDetailsByUserIdRes));

        getVSATDetailsRes.setIsVSATAttended(true);
        getVSATDetailsRes.setScholarship(calculateDiscount(getLatestVSATTestDetailsByUserIdRes.getRank()));
        getVSATDetailsRes.setScore(getLatestVSATTestDetailsByUserIdRes.getInfo().getMarks());
        getVSATDetailsRes.setAttemptedOn(getLatestVSATTestDetailsByUserIdRes.getAttemptedOn());
        getVSATDetailsRes.setTopScore(getLatestVSATTestDetailsByUserIdRes.getTopScorerValue());
        if (getLatestVSATTestDetailsByUserIdRes.getInfo() != null) {
            if (getLatestVSATTestDetailsByUserIdRes.getInfo().getDuration() != 0) {
                Double duration = Double.valueOf(getLatestVSATTestDetailsByUserIdRes.getInfo().getDuration());
                Integer attemptedQuestion = getLatestVSATTestDetailsByUserIdRes.getInfo().getAttempted();
                Double speed = Math.ceil(attemptedQuestion / (duration / HOUR));
                getVSATDetailsRes.setSpeed(speed);
            }
            if (getLatestVSATTestDetailsByUserIdRes.getInfo().getCorrect() != 0 && getLatestVSATTestDetailsByUserIdRes.getInfo().getAttempted() != 0) {
                Double correct = Double.valueOf(getLatestVSATTestDetailsByUserIdRes.getInfo().getCorrect());
                Double attempt = Double.valueOf(getLatestVSATTestDetailsByUserIdRes.getInfo().getAttempted());
                Double accuracy = Math.ceil((correct / attempt) * 100);
                getVSATDetailsRes.setAccuracy(accuracy);
            }
        }

        return getVSATDetailsRes;
    }

    private Integer calculateDiscount(Integer rank) {
        if (rank == 1) {
            return 100;
        } else if (rank >= 2 && rank <= 3) {
            return 90;
        } else if (rank >= 4 && rank <= 10) {
            return 75;
        } else if (rank >= 11 && rank <= 20) {
            return 50;
        } else if (rank >= 21 && rank <= 100) {
            return 40;
        } else if (rank >= 101 && rank <= 500) {
            return 30;
        } else if (rank >= 501 && rank <= 1000) {
            return 20;
        } else {
            return 15;
        }
    }

    public void addUserSessionDetails(String text) throws VException {

        UserSessionDetails userSessionDetails = null;
        GTTAttendeeDetailsInfo gttAttendeeDetailsInfo = new Gson().fromJson(text, GTTAttendeeDetailsInfo.class);

        if (gttAttendeeDetailsInfo != null) {

            Long userId = Long.valueOf(gttAttendeeDetailsInfo.getUserId());
            Date creationDate = new Date(gttAttendeeDetailsInfo.getCreationTime());
            Date creationDateWithTimeZero = DateUtils.truncate(creationDate, Calendar.DATE);
            logger.info("creation date with zero time: {}, actual time: {}", creationDateWithTimeZero, creationDate);

            AttendedSessionInfo attendedSessionInfo = getattendedSessionInfo(gttAttendeeDetailsInfo);
            if (attendedSessionInfo != null) {
                userSessionDetails = scVglanceDao.getUserSessionDetailsByUserDate(userId, creationDateWithTimeZero.getTime());
                logger.info("userSessionDetails : {}", userSessionDetails);

                if (userSessionDetails != null) {
                    logger.info("existing timeInSessions: {}, class attended: {}", userSessionDetails.getTotalTimeInSessions(), userSessionDetails.getTotalClassAttended());
                    Long totalTimeInSession = gttAttendeeDetailsInfo.getTimeInSession() + userSessionDetails.getTotalTimeInSessions();
                    Integer totalClassAttended = userSessionDetails.getTotalClassAttended() + 1;
                    logger.info("updated timeInSessions: {}, class attended: {}", totalTimeInSession, totalClassAttended);
                    userSessionDetails.setTotalClassAttended(totalClassAttended);
                    userSessionDetails.setTotalTimeInSessions(totalTimeInSession);
                    userSessionDetails.getAttendedSessionInfos().add(attendedSessionInfo);
                    userSessionDetails.setLastUpdatedDate(new Date(System.currentTimeMillis() + DateTimeUtils.IST_TIME_DIFFERENCE_NEGATIVE));

                } else {
                    userSessionDetails = new UserSessionDetails();
                    userSessionDetails.setDate(creationDateWithTimeZero.getTime());
                    userSessionDetails.setUserId(userId);
                    userSessionDetails.setTotalClassAttended(1);
                    userSessionDetails.setTotalTimeInSessions(gttAttendeeDetailsInfo.getTimeInSession());
                    userSessionDetails.setAttendedSessionInfos(Arrays.asList(attendedSessionInfo));
                    userSessionDetails.setLastUpdatedDate(new Date(System.currentTimeMillis() + DateTimeUtils.IST_TIME_DIFFERENCE_NEGATIVE));
                }
                scVglanceDao.addUserSessionDetails(userSessionDetails);
                logger.info("userSessionDetails added successfully..");
            }
        }
    }

    private AttendedSessionInfo getattendedSessionInfo(GTTAttendeeDetailsInfo gttAttendeeDetailsInfo) throws
            VException {
        AttendedSessionInfo attendedSessionInfo = new AttendedSessionInfo();
        attendedSessionInfo.setSessionId(gttAttendeeDetailsInfo.getSessionId());
        attendedSessionInfo.setHotspotNumbers(gttAttendeeDetailsInfo.getHotspotNumbers());
        attendedSessionInfo.setQuizNumbers(gttAttendeeDetailsInfo.getQuizNumbers());
        attendedSessionInfo.setTimeInSession(gttAttendeeDetailsInfo.getTimeInSession());
        attendedSessionInfo.setCreationTime(gttAttendeeDetailsInfo.getCreationTime());
        attendedSessionInfo.setCreatedBy(gttAttendeeDetailsInfo.getCreatedBy());
        attendedSessionInfo.setLastUpdated(gttAttendeeDetailsInfo.getLastUpdated());
        attendedSessionInfo.setLastUpdatedBy(gttAttendeeDetailsInfo.getLastUpdatedBy());

        List<OTFSessionInfo> otfSessionInfos = getSessionDetails(Arrays.asList(gttAttendeeDetailsInfo.getSessionId()), Arrays.asList("title", "presenter", "boardId", "sessionContextType"));
        OTFSessionInfo otfSessionInfo = otfSessionInfos.get(0);
        if (otfSessionInfo.getBoardId() != null) {
            Set<Long> boardId = new HashSet<>();
            boardId.add(otfSessionInfo.getBoardId());
            Map<Long, Board> idToBoardMap = fosUtils.getBoardInfoMap(boardId);
            attendedSessionInfo.setBoardId(otfSessionInfo.getBoardId());
            attendedSessionInfo.setSubject(idToBoardMap.get(otfSessionInfo.getBoardId()).getName());
        } else if (otfSessionInfo.isWebinarSession()) {
            if (com.vedantu.onetofew.enums.EntityType.BIG_SESSION.equals(otfSessionInfo.getType())) {
                attendedSessionInfo.setSubject("MASTER_TALK");
            } else {
                attendedSessionInfo.setSubject("WEBINAR_MASTER_CLASS");
            }
        } else {
            logger.info("No Board id present and session context type also not WEBINAR for Session : {}", gttAttendeeDetailsInfo.getSessionId());
            return null;
        }
        return attendedSessionInfo;
    }

    public GetTotalLearningDetailsRes getTotalLearningDetails(SalesVglanceReq salesVglanceReq) throws VException {

        Long totalSessionAttendedTime = 0L;
        Integer totalClassAttended = 0;
        List<ContentInfo> filteredTestAttempts = new ArrayList<>();
        List<ContentInfo> contentInfos = getTestAttempted(salesVglanceReq);
        if (CollectionUtils.isNotEmpty(contentInfos)) {
            filteredTestAttempts = contentInfos.parallelStream().filter(p -> com.vedantu.util.StringUtils.isNotEmpty(p.getTestId())).collect(Collectors.toList());
        }
        List<UserSessionDetails> userSessionDetails = scVglanceDao.getUserSessionDetailsByDate(salesVglanceReq.getUserId(), salesVglanceReq.getStartTime(),
                Arrays.asList(UserSessionDetails.Constants.TOTAL_TIME_IN_SESSION, UserSessionDetails.Constants.TOTAL_CLASS_ATTENDED), true);
        if (CollectionUtils.isNotEmpty(userSessionDetails)) {
            totalSessionAttendedTime = userSessionDetails.stream().filter(p -> p.getTotalTimeInSessions() > 0).mapToLong(p -> p.getTotalTimeInSessions()).sum();
            totalClassAttended = userSessionDetails.stream().filter(p -> p.getTotalClassAttended() > 0).mapToInt(p -> p.getTotalClassAttended()).sum();
        }

        return new GetTotalLearningDetailsRes(Math.ceil(Double.valueOf(totalSessionAttendedTime) / MINUTE), totalClassAttended, filteredTestAttempts.size());
    }

    private List<ContentInfo> getTestAttempted(SalesVglanceReq salesVglanceReq) throws VException {
        GetContentsReq getContentsReq = new GetContentsReq();
        getContentsReq.setStudentIds(Arrays.asList(String.valueOf(salesVglanceReq.getUserId())));
        getContentsReq.setFromTime(salesVglanceReq.getStartTime());
        getContentsReq.setThruTime(System.currentTimeMillis());

        String url = LMS_ENDPOINT + "/getContentsForStudents";
        ClientResponse resp = WebUtils.INSTANCE.doCall(url, HttpMethod.POST, new Gson().toJson(getContentsReq));
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        Type contentInfo = new TypeToken<List<ContentInfo>>() {
        }.getType();
        List<ContentInfo> contentInfos = new Gson().fromJson(jsonString, contentInfo);
        if (CollectionUtils.isNotEmpty(contentInfos)) {
            return contentInfos;
        } else {
            return new ArrayList<>();
        }
    }

    public GetTimeSpentDataMetricsRes getTimeSpentDataMetrics(SalesVglanceReq salesVglanceReq) {
        GetTimeSpentDataMetricsRes getTimeSpentDataMetricsRes = new GetTimeSpentDataMetricsRes();
        Map<String, Long> subjectCountMap = new HashMap<>();
        List<UserSessionDetails> userSessionDetails = scVglanceDao.getUserSessionDetailsByDate(salesVglanceReq.getUserId(), salesVglanceReq.getStartTime(),
                Arrays.asList(UserSessionDetails.Constants.ATTENDED_SESSION_INFO), true);

        if (CollectionUtils.isNotEmpty(userSessionDetails)) {

            List<List<AttendedSessionInfo>> attendedSessionInfos = userSessionDetails.parallelStream().filter(p -> CollectionUtils.isNotEmpty(p.getAttendedSessionInfos())).map(p -> p.getAttendedSessionInfos()).collect(Collectors.toList());
            for (List<AttendedSessionInfo> attendedSessionInfosDays : attendedSessionInfos) {
                if (CollectionUtils.isNotEmpty(attendedSessionInfosDays)) {
                    for (AttendedSessionInfo attendedSessionInfo : attendedSessionInfosDays) {
                        if (subjectCountMap.containsKey(attendedSessionInfo.getSubject())) {
                            Long timeInSession = subjectCountMap.get(attendedSessionInfo.getSubject());
                            subjectCountMap.put(attendedSessionInfo.getSubject(), timeInSession + attendedSessionInfo.getTimeInSession());
                        } else {
                            subjectCountMap.put(attendedSessionInfo.getSubject(), attendedSessionInfo.getTimeInSession());
                        }
                    }
                }
            }
            logger.info("Map with subject: {}", subjectCountMap);

            LinkedHashMap<String, Long> sortedMap = subjectCountMap.entrySet()
                    .stream()
                    .sorted((Map.Entry.<String, Long>comparingByValue().reversed()))
                    .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));

            logger.info("Sorted Map with subject: {}", sortedMap);

            List<String> sortedKeysList = new LinkedList<>(sortedMap.keySet());
            String mostTimeSpentSubject = sortedKeysList.get(0);
            Long mostTimeSpentCount = sortedMap.get(mostTimeSpentSubject);

            String leastTimeSpentSubject = "";
            Long leastTimeSpentCount = 0L;
            if (sortedKeysList.size() > 1) {
                leastTimeSpentSubject = sortedKeysList.get(sortedKeysList.size() - 1);
                leastTimeSpentCount = sortedMap.get(leastTimeSpentSubject);
            }

            Double mostQuestionSolved = 0.0;
            Double leastQuestionSolved = 0.0;
            Double mostQuestionCorrect = 0.0;
            Double leastQuestionCorrect = 0.0;
            Double mostAccuracy = 0.0;
            Double leastAccuracy = 0.0;

            Integer mostLiveAttendedCount = 0;
            Integer leastLiveAttendedCount = 0;

            for (List<AttendedSessionInfo> attendedSessionInfosDays : attendedSessionInfos) {
                logger.info("inside 1 if");
                if (CollectionUtils.isNotEmpty(attendedSessionInfosDays)) {
                    for (AttendedSessionInfo attendedSessionInfo : attendedSessionInfosDays) {
                        logger.info("inside 1 if");
                        if (StringUtils.equals(mostTimeSpentSubject, attendedSessionInfo.getSubject())) {
                            mostLiveAttendedCount += 1;
                            if (attendedSessionInfo.getHotspotNumbers() != null) {
                                mostQuestionSolved += attendedSessionInfo.getHotspotNumbers().getNumberAttempted();
                                mostQuestionCorrect += attendedSessionInfo.getHotspotNumbers().getNumberCorrect();
                            }
                            if (attendedSessionInfo.getQuizNumbers() != null) {
                                mostQuestionSolved += attendedSessionInfo.getQuizNumbers().getNumberAttempted();
                                mostQuestionCorrect += attendedSessionInfo.getQuizNumbers().getNumberCorrect();
                            }
                        } else if (StringUtils.equals(leastTimeSpentSubject, attendedSessionInfo.getSubject())) {
                            leastLiveAttendedCount += 1;
                            if (attendedSessionInfo.getHotspotNumbers() != null) {
                                leastQuestionSolved += attendedSessionInfo.getHotspotNumbers().getNumberAttempted();
                                leastQuestionCorrect += attendedSessionInfo.getHotspotNumbers().getNumberCorrect();
                            }
                            if (attendedSessionInfo.getQuizNumbers() != null) {
                                leastQuestionSolved += attendedSessionInfo.getQuizNumbers().getNumberAttempted();
                                leastQuestionCorrect += attendedSessionInfo.getQuizNumbers().getNumberCorrect();
                            }
                        }
                    }
                }
            }
            logger.info("mostTimeSpentCount: {}, leastTimeSpentCount: {}", mostTimeSpentCount, leastTimeSpentCount);
            logger.info("mostQuestionSolved: {}, mostQuestionCorrect: {}, leastQuestionSolved: {}, leastQuestionCorrect: {}", mostQuestionSolved, mostQuestionCorrect, leastQuestionSolved, leastQuestionCorrect);
            if (mostQuestionSolved != 0) {
                mostAccuracy = (mostQuestionCorrect / mostQuestionSolved) * 100;
            }
            if (leastQuestionSolved != 0) {
                leastAccuracy = (leastQuestionCorrect / leastQuestionSolved) * 100;
            }

            Double mostTimeSpent = Math.ceil(Double.valueOf(mostTimeSpentCount) / MINUTE);
            Double leastTimeSpent = Math.ceil(Double.valueOf(leastTimeSpentCount) / MINUTE);

            getTimeSpentDataMetricsRes.setMostAccuracy(Math.ceil(mostAccuracy));
            getTimeSpentDataMetricsRes.setMostLiveClassAttended(mostLiveAttendedCount);
            getTimeSpentDataMetricsRes.setMostQuestionSolved(mostQuestionSolved);
            getTimeSpentDataMetricsRes.setMostTimeSpent(mostTimeSpent);
            getTimeSpentDataMetricsRes.setMostTimeSpentSubject(mostTimeSpentSubject);

            getTimeSpentDataMetricsRes.setLeastAccuracy(Math.ceil(leastAccuracy));
            getTimeSpentDataMetricsRes.setLeastLiveClassAttended(leastLiveAttendedCount);
            getTimeSpentDataMetricsRes.setLeastQuestionSolved(leastQuestionSolved);
            getTimeSpentDataMetricsRes.setLeastTimeSpent(leastTimeSpent);
            getTimeSpentDataMetricsRes.setLeastTimeSpentSubject(leastTimeSpentSubject);
            logger.info("Data metrics response: {}", getTimeSpentDataMetricsRes);
        } else {
            logger.info("User metrics for user {} not present", salesVglanceReq.getUserId());
        }
        return getTimeSpentDataMetricsRes;
    }

}