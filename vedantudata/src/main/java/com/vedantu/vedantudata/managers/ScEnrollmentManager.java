package com.vedantu.vedantudata.managers;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.User.Role;
import com.vedantu.User.TeacherBasicInfo;
import com.vedantu.User.UserBasicInfo;
import com.vedantu.board.pojo.Board;
import com.vedantu.dinero.enums.InstalmentPurchaseEntity;
import com.vedantu.dinero.pojo.BaseInstalment;
import com.vedantu.dinero.pojo.BaseInstalmentInfo;
import com.vedantu.dinero.pojo.Orders;
import com.vedantu.exception.*;
import com.vedantu.notification.enums.CommunicationType;
import com.vedantu.notification.requests.EmailRequest;
import com.vedantu.onetofew.pojo.CourseBasicInfo;
import com.vedantu.scheduling.pojo.session.BatchWiseSessionInfo;
import com.vedantu.scheduling.request.GetSessionsByBatchIdsReq;
import com.vedantu.subscription.enums.EnrollmentType;
import com.vedantu.subscription.pojo.AutoEnrollCourseInfo;
import com.vedantu.subscription.pojo.BundleInfo;
import com.vedantu.subscription.pojo.bundle.BundleEntityInfo;
import com.vedantu.subscription.pojo.bundle.GetBundlePackageReferenceRequest;
import com.vedantu.util.*;
import com.vedantu.util.enums.SQSMessageType;
import com.vedantu.util.enums.SQSQueue;
import com.vedantu.vedantudata.entities.leadsquared.LeadDetails;
import com.vedantu.vedantudata.managers.leadsquared.LeadsquaredManager;
import com.vedantu.vedantudata.pojos.LeadActivitySerialized;
import com.vedantu.vedantudata.pojos.LeadSquaredActivityFields;
import com.vedantu.vedantudata.pojos.response.BundleDetailsRes;
import com.vedantu.vedantudata.pojos.response.GetCourseInfoRes;
import com.vedantu.vedantudata.pojos.response.SalesConfimrationRes;
import com.vedantu.vedantudata.utils.AwsSQSManager;
import com.vedantu.vedantudata.utils.RedisDAO;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAdjusters;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class ScEnrollmentManager {

    @Autowired
    private FosUtils fosUtils;

    @Autowired
    private LeadsquaredManager leadsquaredManager;

    @Autowired
    private RedisDAO redisDAO;

    @Autowired
    private LogFactory logFactory;

    @Autowired
    private AwsSQSManager awsSQSManager;

    @Autowired
    private CommunicationManager communicationManager;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(ScEnrollmentManager.class);

    private static final String USER_ENDPOINT = ConfigUtils.INSTANCE.getStringValue("USER_ENDPOINT");

    private static final String SUBSCRIPTION_ENDPOINT = ConfigUtils.INSTANCE.getStringValue("SUBSCRIPTION_ENDPOINT");

    private static final String SCHEDULING_ENDPOINT = ConfigUtils.INSTANCE.getStringValue("SCHEDULING_ENDPOINT");

    private static final String DINERO_ENDPOINT = ConfigUtils.INSTANCE.getStringValue("DINERO_ENDPOINT");

    private static final String REDIS_BUNDLE_KEY = "SC_BUNDLE_ID_";

    private static final String IST_ZONE_ID = "Asia/Kolkata";

    private static final String TEACHER_SUBJECT_SUFFIX = " Expert";

    private static final DateFormat DATE_FORMAT = new SimpleDateFormat("MMMMyyyy");

    /**
     * Gets the Bundle basic info.
     *
     * @param entityId
     * @return
     * @throws VException
     */
    public BundleDetailsRes getBundleDetails(String entityId) throws VException {

        BundleDetailsRes bundleDetailsRes = new BundleDetailsRes();
        BundleInfo bundleInfo = getBundleInfo(entityId);
        bundleDetailsRes.setGrade(bundleInfo.getGrade());
        bundleDetailsRes.setTitle(bundleInfo.getTitle());
        bundleDetailsRes.setValidTill(bundleInfo.getValidTill());
        bundleDetailsRes.setHasLiveCourses(true);
        bundleDetailsRes.setHasVideo(true);
        bundleDetailsRes.setHasTest(true);
        if (bundleInfo.getUnLimitedDoubtsIncluded() != null) {
            bundleDetailsRes.setUnLimitedDoubtsIncluded(bundleInfo.getUnLimitedDoubtsIncluded());
        }
        if (bundleInfo.getAmIncluded() != null) {
            bundleDetailsRes.setAmIncluded(bundleInfo.getAmIncluded());
        }

        //get batchids from bundleid
        List<String> batchIds = getAutoEnrollBatchesFromBundle(entityId);

        //Get first class/session from bundle/entity/aio id
        BatchWiseSessionInfo firstClassInfo = getFirstClassInfo(batchIds);
        bundleDetailsRes.setFirstClass(firstClassInfo);

        return bundleDetailsRes;
    }

    /**
     * gets courseInfo from Bundle id
     *
     * @param entityId
     * @return
     * @throws VException
     */
    public GetCourseInfoRes getCourseInfo(String entityId) throws VException {

        GetCourseInfoRes getCourseInfoRes = new GetCourseInfoRes();
        List<String> batchIds = getAutoEnrollBatchesFromBundle(entityId);
        logger.info("batches for courseInfo: {}", batchIds);
        if (CollectionUtils.isNotEmpty(batchIds)) {
            List<AutoEnrollCourseInfo> autoEnrollCourseInfos = getAutoEnrollCourses(batchIds);
            logger.info("course info: {} {}", autoEnrollCourseInfos, System.currentTimeMillis());
            if (CollectionUtils.isNotEmpty(autoEnrollCourseInfos)) {

//                Set<String> courseIds = autoEnrollCourseInfos.parallelStream().filter(p -> {
//                    if (System.currentTimeMillis() < p.getEndTime()) {
//                        return true;
//                    }
//                    return false;
//                }).filter(p -> p.getCourseId() != null).map(p -> p.getCourseId()).collect(Collectors.toSet());
                Set<String> courseIds = new HashSet<>();
                for (AutoEnrollCourseInfo autoEnrollCourseInfo : autoEnrollCourseInfos) {

                    if (autoEnrollCourseInfo.getEndTime() > System.currentTimeMillis() && autoEnrollCourseInfo.getCourseId() != null) {
                        logger.info("inside if");
                        courseIds.add(autoEnrollCourseInfo.getCourseId());
                    }
                }

                if (CollectionUtils.isNotEmpty(courseIds)) {

                    Map<String, CourseBasicInfo> courseMap = getCourseDetailsFromId(courseIds);
                    if (courseMap.size() > 0) {
                        for (AutoEnrollCourseInfo autoEnrollCourseInfo : autoEnrollCourseInfos) {
                            if (StringUtils.isNotEmpty(autoEnrollCourseInfo.getCourseId())) {
                                CourseBasicInfo courseBasicInfo = courseMap.get(autoEnrollCourseInfo.getCourseId());
                                if (Objects.nonNull(courseBasicInfo)) {
                                    Set<String> subjects = courseBasicInfo.getNormalizeSubjects();
                                    autoEnrollCourseInfo.setSubject(subjects);
                                }
                            }
                        }
                    }
                }
            }
            getCourseInfoRes.setCourses(autoEnrollCourseInfos);
        }
        return getCourseInfoRes;
    }

    /**
     * gets sessionInfo based on courses
     *
     * @param batchId
     * @param displayDate
     * @param validTill
     * @param validFrom
     * @return
     * @throws VException
     */
    public Map<String, List<BatchWiseSessionInfo>> getCourseWiseSessionInfo(String batchId, Long displayDate, Long validTill, Long validFrom) throws VException {

        List<String> errors = new ArrayList<>();
        //To check date validation
        Boolean isValidFromSameMonth = false;
        if (displayDate < validFrom) {
            //should allow if year and month is same for display and valid date
            isValidFromSameMonth = isDateOfSameMonth(displayDate, validFrom);
            if (!isValidFromSameMonth) {
                errors.add("Selected month is before course startDate ");
            }
        }
        Long lastDayOfCurrentMonth = getLastDateOfGivenDate(displayDate, IST_ZONE_ID);
        Boolean isValidTillSameMonth = false;
        if (lastDayOfCurrentMonth > validTill) {
            //should allow if year and month is same for display and valid date
            isValidTillSameMonth = isDateOfSameMonth(lastDayOfCurrentMonth, validTill);
            if (!isValidTillSameMonth) {
                errors.add("Selected month is after course endDate ");
            }
        }
        if (CollectionUtils.isNotEmpty(errors)) {
            throw new VException(ErrorCode.FORBIDDEN_ERROR, "Date error : " + errors);
        }

        List<String> weekWiseDateRange = new ArrayList<>();

        if (isValidFromSameMonth && isValidTillSameMonth) {
            weekWiseDateRange = DateTimeUtils.getWeekWiseDateRange(validFrom, validTill, IST_ZONE_ID);
        } else if (isValidFromSameMonth) {
            weekWiseDateRange = DateTimeUtils.getWeekWiseDateRange(validFrom, lastDayOfCurrentMonth, IST_ZONE_ID);
        } else if (isValidTillSameMonth) {
            weekWiseDateRange = DateTimeUtils.getWeekWiseDateRange(displayDate, validTill, IST_ZONE_ID);
        } else {
            weekWiseDateRange = DateTimeUtils.getWeekWiseDateRange(displayDate, lastDayOfCurrentMonth, IST_ZONE_ID);
        }

        Map<String, List<BatchWiseSessionInfo>> weekWiseSessionMap = getWeekWiseDateRangeData(weekWiseDateRange);
        logger.info("Weekwise date Map: {}", weekWiseSessionMap);

        logger.info("Getting session data for range, startTime : {}, EndTime: {}", displayDate, lastDayOfCurrentMonth);
        List<BatchWiseSessionInfo> batchWiseSessionInfos = getSessionsByBatchIds(Arrays.asList(batchId), displayDate, lastDayOfCurrentMonth);
        logger.info("Session data count: {}", batchWiseSessionInfos.size());

        if (CollectionUtils.isNotEmpty(batchWiseSessionInfos)) {
            for (BatchWiseSessionInfo batchWiseSessionInfo : batchWiseSessionInfos) {
                for (String dateRange : weekWiseDateRange) {
                    String[] dateArray = dateRange.split("-");
                    Long start = Long.parseLong(dateArray[0]);
                    Long end = Long.parseLong(dateArray[1]);
                    if (batchWiseSessionInfo.getStartTime() >= start && batchWiseSessionInfo.getStartTime() <= end) {
                        String key = start + "-" + end;
                        if (weekWiseSessionMap.containsKey(key)) {
                            weekWiseSessionMap.get(key).add(batchWiseSessionInfo);
                        } else {
                            weekWiseSessionMap.put(key, Arrays.asList(batchWiseSessionInfo));
                        }
                        //As once session is assign to specific range, then no need to go further..
                        break;
                    }
                }
            }
        }

        return weekWiseSessionMap;
    }

    /**
     * gets session timings for a week
     *
     * @param entityId
     * @return
     * @throws VException
     */
    public Map<String, List<BatchWiseSessionInfo>> getClassTimings(String entityId) throws VException {

        Map<String, List<BatchWiseSessionInfo>> weekDaySessionMap = new LinkedHashMap<>();
        LocalDate incomingDate = LocalDate.now(ZoneId.of(IST_ZONE_ID));
        LocalDate firstDayOfWeek = incomingDate.with(DayOfWeek.MONDAY);
        LocalDate populateWeekData = incomingDate.with(DayOfWeek.MONDAY);
        LocalDate lastDayOfWeek = firstDayOfWeek.plusDays(6);

        while (populateWeekData.isBefore(lastDayOfWeek) || populateWeekData.isEqual(lastDayOfWeek)) {

            logger.info(populateWeekData.getDayOfWeek());
            String key = String.valueOf(populateWeekData.getDayOfWeek());
            weekDaySessionMap.put(key, new LinkedList<>());
            populateWeekData = populateWeekData.plusDays(1);
        }

        //get batchids from bundleid
        List<String> batchIds = getAutoEnrollBatchesFromBundle(entityId);
        logger.info("getClassTimings, batches: {}", batchIds);

        if (CollectionUtils.isNotEmpty(batchIds)) {

            Long startDate = firstDayOfWeek.atTime(0, 0, 0, 0).atZone(ZoneId.of(IST_ZONE_ID)).toInstant().toEpochMilli();
            Long endDate = firstDayOfWeek.plusDays(6).atTime(23, 59, 59, 999).atZone(ZoneId.of(IST_ZONE_ID)).toInstant().toEpochMilli();
            logger.info("getClassTimings, sessions for range, startDate: {}, endDate: {}", startDate, endDate);
            //Get the class from batchIds
            List<BatchWiseSessionInfo> sessionInfo = getSessionsByBatchIds(batchIds, startDate, endDate);

            if (CollectionUtils.isNotEmpty(sessionInfo)) {
                //sorting filterlist as per startTime date
                List<BatchWiseSessionInfo> sortedSessionInfos = sessionInfo.parallelStream().sorted(Comparator.comparing(p -> {
                    LocalDateTime dateTime = null;
                    try {
                        dateTime = Instant.ofEpochMilli(p.getStartTime())
                                .atZone(ZoneId.systemDefault())
                                .toLocalDateTime();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    return dateTime;
                })).collect(Collectors.toList());

                while (firstDayOfWeek.isBefore(lastDayOfWeek) || firstDayOfWeek.isEqual(lastDayOfWeek)) {

                    Long startTime = firstDayOfWeek.atTime(0, 0, 0, 0).atZone(ZoneId.of(IST_ZONE_ID)).toInstant().toEpochMilli();
                    Long endTime = firstDayOfWeek.atTime(23, 59, 59, 999).atZone(ZoneId.of(IST_ZONE_ID)).toInstant().toEpochMilli();
                    logger.info(firstDayOfWeek.getDayOfWeek());
                    String key = String.valueOf(firstDayOfWeek.getDayOfWeek());

                    for (BatchWiseSessionInfo batchWiseSessionInfo : sortedSessionInfos) {
                        if (batchWiseSessionInfo.getStartTime() >= startTime && batchWiseSessionInfo.getStartTime() <= endTime) {
                            Map<Long, Board> boardMap = getSubjectFromBoardId(Arrays.asList(batchWiseSessionInfo.getBoardId()));
                            if (boardMap != null && boardMap.size() > 0) {
                                batchWiseSessionInfo.setSubject(boardMap.get(batchWiseSessionInfo.getBoardId()).getName());
                            }
                            if (weekDaySessionMap.containsKey(key)) {
                                weekDaySessionMap.get(key).add(batchWiseSessionInfo);
                            } else {
                                weekDaySessionMap.put(key, Arrays.asList(batchWiseSessionInfo));
                            }
                        }
                    }

                    firstDayOfWeek = firstDayOfWeek.plusDays(1);
                }
                logger.info("Week wise session infos: {}", weekDaySessionMap);
            } else {
                logger.info("No session present for Batches: {}", batchIds);
            }
        }
        return weekDaySessionMap;
    }

    /**
     * gets upcoming first class scheduled
     *
     * @param batchIds
     * @return
     * @throws VException
     */
    private BatchWiseSessionInfo getFirstClassInfo(List<String> batchIds) throws VException {

        BatchWiseSessionInfo batchWiseSessionInfo = null;

        if (CollectionUtils.isNotEmpty(batchIds)) {
            //Get the first class from batchIds
            List<BatchWiseSessionInfo> sessionInfo = getSessionsByBatchIds(batchIds, System.currentTimeMillis(), null);
            if (ArrayUtils.isNotEmpty(sessionInfo)) {
                Map<Long, BatchWiseSessionInfo> startTimetoSessionMap = new HashMap<>();
                for (BatchWiseSessionInfo session : sessionInfo) {
                    startTimetoSessionMap.put(session.getStartTime(), session);
                }
                Long minStartTime = Collections.min(startTimetoSessionMap.keySet());
                batchWiseSessionInfo = startTimetoSessionMap.get(minStartTime);
                logger.info("firstClass info: {}", batchWiseSessionInfo);
                batchWiseSessionInfo.setPresenter(fosUtils.getUserBasicInfo(batchWiseSessionInfo.getPresenter(), false).getFullName());

                //to get Subject
                Map<Long, Board> boardMap = getSubjectFromBoardId(Arrays.asList(batchWiseSessionInfo.getBoardId()));
                if (boardMap != null && boardMap.size() > 0) {
                    batchWiseSessionInfo.setSubject(boardMap.get(batchWiseSessionInfo.getBoardId()).getName());
                }
            } else {
                logger.info("No session present for Batches: {}", batchIds);
            }
        }
        return batchWiseSessionInfo;
    }

    private Boolean isDateOfSameMonth(Long date1, Long date2) {
        Boolean flag = false;
        LocalDate localDate1 = Instant.ofEpochMilli(date1)
                .atZone(ZoneId.of(IST_ZONE_ID))
                .toLocalDate();
        LocalDate localDate2 = Instant.ofEpochMilli(date2)
                .atZone(ZoneId.of(IST_ZONE_ID))
                .toLocalDate();
        if (localDate1.getYear() == localDate2.getYear() && localDate1.getMonth().name().equals(localDate2.getMonth().name())) {
            flag = true;
        }
        return flag;
    }

    private Map<String, List<BatchWiseSessionInfo>> getWeekWiseDateRangeData(List<String> weekWiseDateRange) {
        Map<String, List<BatchWiseSessionInfo>> weekWiseDateMap = new LinkedHashMap<>();
        for (String date : weekWiseDateRange) {
            weekWiseDateMap.put(date, new LinkedList<>());
        }
        return weekWiseDateMap;
    }

    private Map<Long, Board> getSubjectFromBoardId(List<Long> boardIds) {
        Map<Long, Board> boardInfoMap = new HashMap<>();
        if (CollectionUtils.isNotEmpty(boardIds)) {
            boardInfoMap = fosUtils.getBoardInfoMap(boardIds);
            return boardInfoMap;
        }
        return boardInfoMap;
    }

    private Long getLastDateOfGivenDate(Long displayDate, String istZoneId) {
        LocalDate incomingDate = Instant.ofEpochMilli(displayDate)
                .atZone(ZoneId.of(istZoneId))
                .toLocalDate();
        LocalDate lastDayOfMonth = incomingDate.with(TemporalAdjusters.lastDayOfMonth());
        return lastDayOfMonth.atTime(23, 59, 59, 999).atZone(ZoneId.of(IST_ZONE_ID)).toInstant().toEpochMilli();
    }

    private List<BatchWiseSessionInfo> getSessionsByBatchIds(List<String> batchIds, Long startDate, Long endTime) {

        GetSessionsByBatchIdsReq getSessionsByBatchIdsReq = new GetSessionsByBatchIdsReq();
        if (endTime != null) {
            getSessionsByBatchIdsReq.setEndTime(endTime);
        }
        getSessionsByBatchIdsReq.setStartTime(startDate);

        List<BatchWiseSessionInfo> sessionInfos = new ArrayList<>();
        if (batchIds.size() <= 20) {
            getSessionsByBatchIdsReq.setBatchIds(batchIds);
            sessionInfos = getSessionInfo(getSessionsByBatchIdsReq);
        } else {
            List<String> batchSize = new ArrayList<>();
            List<BatchWiseSessionInfo> sessionsBatchWise = new ArrayList<>();

            for (String batchId : batchIds) {
                batchSize.add(batchId);
                if (batchSize.size() > 20) {
                    getSessionsByBatchIdsReq.setBatchIds(batchIds);
                    sessionsBatchWise = getSessionInfo(getSessionsByBatchIdsReq);
                    sessionInfos.addAll(sessionsBatchWise);
                    batchSize = new ArrayList<>();
                }
            }

            if (batchSize.size() > 0) {
                getSessionsByBatchIdsReq.setBatchIds(batchIds);
                sessionsBatchWise = getSessionInfo(getSessionsByBatchIdsReq);
                sessionInfos.addAll(sessionsBatchWise);
            }
        }
        logger.info("sessionInfo : {}", sessionInfos);
        return sessionInfos;
    }

    private List<BatchWiseSessionInfo> getSessionInfo(GetSessionsByBatchIdsReq getSessionsByBatchIdsReq) {

        getSessionsByBatchIdsReq.setStart(0);
        getSessionsByBatchIdsReq.setSize(20);
        List<BatchWiseSessionInfo> sessionInfos = new ArrayList<>();
        while (true) {
            String getSessionsUrl = SCHEDULING_ENDPOINT + "/onetofew/session/getSessionsByBatchIds";
            ClientResponse sessionsResponse = WebUtils.INSTANCE.doCall(getSessionsUrl,
                    HttpMethod.POST, new Gson().toJson(getSessionsByBatchIdsReq));
            String sessionRes = sessionsResponse.getEntity(String.class);
            List<BatchWiseSessionInfo> sessionInfoRes = new Gson().fromJson(sessionRes, new TypeToken<List<BatchWiseSessionInfo>>() {
            }.getType());
            if (CollectionUtils.isEmpty(sessionInfoRes)) {
                break;
            } else {
                sessionInfos.addAll(sessionInfoRes);
            }
            getSessionsByBatchIdsReq.setStart(getSessionsByBatchIdsReq.getStart() + getSessionsByBatchIdsReq.getSize());
        }
        return sessionInfos;
    }

    private List<String> getAutoEnrollBatchesFromBundle(String entityId) throws VException {

        List<String> batchIds = new ArrayList<>();
        String redisBatchValue = redisDAO.get(REDIS_BUNDLE_KEY + entityId);
        logger.info("getAutoEnrollBatchesFromBundle: {}", redisBatchValue);
        if (StringUtils.isNotEmpty(redisBatchValue)) {
            batchIds = new Gson().fromJson(redisBatchValue, new TypeToken<List>() {
            }.getType());
        } else {
            GetBundlePackageReferenceRequest getBundlePackageReferenceRequest = new GetBundlePackageReferenceRequest();
            getBundlePackageReferenceRequest.setBundleId(entityId);
            getBundlePackageReferenceRequest.setStart(0);
            getBundlePackageReferenceRequest.setSize(20);

            List<BundleEntityInfo> bundleEntityInfos = new ArrayList<>();
            while (true) {
                String batchIdsUrl = SUBSCRIPTION_ENDPOINT + "/bundleEntity/getBundleEntityDataForBundleForSalesConversion";
                ClientResponse batchidsResp = WebUtils.INSTANCE.doCall(batchIdsUrl, HttpMethod.POST, new Gson().toJson(getBundlePackageReferenceRequest));
                VExceptionFactory.INSTANCE.parseAndThrowException(batchidsResp);
                String batchidsRespString = batchidsResp.getEntity(String.class);
                List<BundleEntityInfo> bundleEntityRes = new Gson().fromJson(batchidsRespString, new TypeToken<List<BundleEntityInfo>>() {
                }.getType());
                if (CollectionUtils.isEmpty(bundleEntityRes)) {
                    break;
                } else {
                    bundleEntityInfos.addAll(bundleEntityRes);
                }
                getBundlePackageReferenceRequest.setStart(getBundlePackageReferenceRequest.getStart() + getBundlePackageReferenceRequest.getSize());
            }

            if (CollectionUtils.isNotEmpty(bundleEntityInfos)) {
                batchIds = bundleEntityInfos.parallelStream().filter(p -> EnrollmentType.AUTO_ENROLL.equals(p.getEnrollmentType())).map(BundleEntityInfo::getEntityId).collect(Collectors.toList());
                if (ArrayUtils.isEmpty(batchIds)) {
                    throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Courses are missing for your subscription. Please reach out to your academic counsellor");
                }
            }
            //expiry in 30mins
            redisDAO.setex(REDIS_BUNDLE_KEY + entityId, new Gson().toJson(batchIds), 1800);
        }
        return batchIds;
    }

    private List<AutoEnrollCourseInfo> getAutoEnrollCourses(List<String> batchIds) throws VException {
        List<AutoEnrollCourseInfo> batchDashboardInfos = new ArrayList<>();
        String queryString = String.join(",", batchIds);
        logger.info("queryString" + queryString);
        ClientResponse resp = WebUtils.INSTANCE.doCall(
                SUBSCRIPTION_ENDPOINT + "/dashboard/getBatches?batchIds=" + queryString, HttpMethod.GET, null);

        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        batchDashboardInfos = new Gson().fromJson(jsonString, new TypeToken<List<AutoEnrollCourseInfo>>() {
        }.getType());
        return batchDashboardInfos;
    }

    private BundleInfo getBundleInfo(String entityId) throws VException {
        String url = SUBSCRIPTION_ENDPOINT + "/bundle/" + entityId;
        ClientResponse resp = WebUtils.INSTANCE.doCall(url, HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        return new Gson().fromJson(jsonString, BundleInfo.class);
    }

    public List<TeacherBasicInfo> getTeacherInfo(List<String> teacherIds) throws VException {
        String url = USER_ENDPOINT + "/info/getTeacherBasicInfos";
        ClientResponse resp = WebUtils.INSTANCE.doCall(url, HttpMethod.POST, new Gson().toJson(teacherIds));
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        List<TeacherBasicInfo> teacherBasicInfos = new Gson().fromJson(jsonString, new TypeToken<List<TeacherBasicInfo>>() {
        }.getType());
        return teacherBasicInfos;
    }

    private Map<String, CourseBasicInfo> getCourseDetailsFromId(Set<String> courseIds) throws VException {
        Map<String, CourseBasicInfo> courseBasicInfoMap = new HashMap<>();
        String queryString = String.join(",", courseIds);
        logger.info("queryString" + queryString);
        String url = SUBSCRIPTION_ENDPOINT + "/course/getCoursesBasicInfos?ids=" + queryString;
        ClientResponse resp = WebUtils.INSTANCE.doCall(url, HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        List<CourseBasicInfo> courseBasicInfos = new Gson().fromJson(jsonString, new TypeToken<List<CourseBasicInfo>>() {
        }.getType());
        if (CollectionUtils.isNotEmpty(courseBasicInfos)) {
            for (CourseBasicInfo courseBasicInfo : courseBasicInfos) {
                courseBasicInfoMap.put(courseBasicInfo.getId(), courseBasicInfo);
            }
        }
        return courseBasicInfoMap;
    }

    /**
     * gets SCF details from LS
     *
     * @param leadId
     * @param entityId
     * @param userId
     * @return
     * @throws InterruptedException
     * @throws VException
     */
    public SalesConfimrationRes getSalesConfirmationDetails(String leadId, String entityId, Long userId) throws
            InterruptedException, VException {

        SalesConfimrationRes salesConfimrationRes = new SalesConfimrationRes();
        Boolean isInstallmentEntryAdded = true;

        UserBasicInfo userBasicInfo = fosUtils.getUserBasicInfo(userId, true);

        List<LeadDetails> leadDetails = leadsquaredManager.fetchLeadsByLeadId(leadId);

        if (CollectionUtils.isNotEmpty(leadDetails) && userBasicInfo != null) {

            if (!org.apache.commons.lang.StringUtils.equals(leadDetails.get(0).getEmailAddress(), userBasicInfo.getEmail()) &&
                    !org.apache.commons.lang.StringUtils.equals(leadDetails.get(0).getPhone(), userBasicInfo.getContactNumber())) {
                throw new VException(ErrorCode.BAD_REQUEST_ERROR, "This link is generated for Mobile number: " + leadDetails.get(0).getPhone().substring(0, 6) + "XXXX" + " and email id: " + leadDetails.get(0).getEmailAddress() + ". Please reach out to your academic counsellor for further assistance");
            }

            LeadSquaredActivityFields leadSquaredActivityFields = getEligibleLSActivityFields(leadId, 331, entityId);

            if (Objects.nonNull(leadSquaredActivityFields)) {

                //getting bundle info from subscription for price..
                BundleInfo bundleInfo = getBundleInfo(entityId);
                if (bundleInfo == null || bundleInfo.getPrice() == null) {
                    throw new VException(ErrorCode.BUNDLE_NOT_FOUND, "Bundle not present or price not set..");
                }
                Integer price = bundleInfo.getPrice();

                //generating coupon code as per price..
                String couponCode = generateCouponCode(leadSquaredActivityFields.getMx_Custom_10(), price);
                logger.info("Coupon code: {}, generated for bundle/AIO id: {}, with price: {}", couponCode, entityId, price);

                Integer remainingAmount = calculateRemainingAmount(leadSquaredActivityFields.getMx_Custom_10(), leadSquaredActivityFields.getMx_Custom_13());
                if (!"Bulk".equals(leadSquaredActivityFields.getMx_Custom_11())) {
                    logger.info("inside installment");
                    List<String> installmentDates = getInstallmentDates(leadSquaredActivityFields.getMx_Custom_22());
                    isInstallmentEntryAdded = addEditStudentBaseInstalment(leadId, price, leadSquaredActivityFields.getMx_Custom_10(), leadSquaredActivityFields.getMx_Custom_22(), leadSquaredActivityFields.getMx_Custom_13(), leadSquaredActivityFields.getMx_Custom_23(), userId, entityId);
                    salesConfimrationRes.setDownPayment(leadSquaredActivityFields.getMx_Custom_13());
                    salesConfimrationRes.setNoOfInstallments(leadSquaredActivityFields.getMx_Custom_22());
                    salesConfimrationRes.setInstallmentAmount(leadSquaredActivityFields.getMx_Custom_23());
                    salesConfimrationRes.setRemainingAmount(String.valueOf(remainingAmount));
                    salesConfimrationRes.setInstallmentDates(installmentDates);
                    salesConfimrationRes.setIsBaseInstallmentAdded(isInstallmentEntryAdded);
                }
                salesConfimrationRes.setSubscriptionFee(leadSquaredActivityFields.getMx_Custom_10());
                salesConfimrationRes.setAgentEmployeeCode(leadSquaredActivityFields.getMx_Custom_38());
                salesConfimrationRes.setCouponCode(couponCode);
                salesConfimrationRes.setPaymentType(leadSquaredActivityFields.getMx_Custom_11());
                salesConfimrationRes.setPrice(price / 100);
                if (StringUtils.isNotEmpty(leadSquaredActivityFields.getMx_Custom_3())) {
                    if (leadSquaredActivityFields.getMx_Custom_3().contains("English")) {
                        salesConfimrationRes.setMedium("English");
                    } else if (leadSquaredActivityFields.getMx_Custom_3().contains("Hindi")) {
                        salesConfimrationRes.setMedium("Hindi");
                    }
                }
            } else {
                throw new VException(ErrorCode.BAD_REQUEST_ERROR, "The course link selected is incorrect. Please reach out to your academic counsellor for further assistance");
            }
        } else {
            logger.info("leadInfo not present");
        }
        return salesConfimrationRes;
    }

    private LeadSquaredActivityFields getEligibleLSActivityFields(String leadId, Integer activityCode, String entityId) throws InterruptedException, VException {

        LeadSquaredActivityFields leadSquaredActivityFields = null;
        //gets SCF activity details from LS for lead id
        List<LeadActivitySerialized> leadActivityList = leadsquaredManager.getLeadActivity(leadId, activityCode);

        if (CollectionUtils.isNotEmpty(leadActivityList)) {
            //filtering list as per incoming bundle/aio id
            List<LeadActivitySerialized> filterList = leadActivityList.parallelStream().
                    filter(p -> entityId.equals(getEntityValue(p))).collect(Collectors.toList());

            if (CollectionUtils.isEmpty(filterList)) {
                return leadSquaredActivityFields;
            }

            if (filterList.size() == 1) {
                leadSquaredActivityFields = filterList.get(0).getFields();
            } else {

                //sorting filterlist as per createdOn date
                List<LeadActivitySerialized> sortedFilterList = filterList.parallelStream().sorted(Comparator.comparing(p -> {
                    Date date = null;
                    try {
                        date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(p.getCreatedOn());
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    return date;
                })).collect(Collectors.toList());
                leadSquaredActivityFields = sortedFilterList.get(sortedFilterList.size() - 1).getFields();
            }
        }
        return leadSquaredActivityFields;
    }


    /**
     * Generates coupon code as per price
     *
     * @param offeredPrice
     * @param price
     * @return
     * @throws VException
     */
    private String generateCouponCode(String offeredPrice, Integer price) throws VException {
        String couponCode = null;
        try {
            Integer offeredPriceValue = Integer.parseInt(offeredPrice);
            if (price != null && (price / 100) > offeredPriceValue) {
                Integer couponValue = (price / 100) - offeredPriceValue;

                String code = "VME" + System.currentTimeMillis() + UUID.randomUUID().toString();
                JSONObject couponReq = new JSONObject();
                couponReq.put("code", code.toUpperCase());
                couponReq.put("redeemValue", couponValue * 100);
                couponReq.put("type", "VEDANTU_DISCOUNT");
                couponReq.put("redeemType", "FIXED");
                couponReq.put("forPaymentType", "BULK");
                couponReq.put("usesCount", 1);
                couponReq.put("usesCountPerUser", 1);
                couponReq.put("validFrom", 0);
                couponReq.put("validTill", 0);
                couponReq.put("targetOrder", new ArrayList<>());
                couponReq.put("minOrderValue", 0);
                couponReq.put("maxOrderValue", 0);
                couponReq.put("maxRedeemValue", 0);

                String createCouponURL = DINERO_ENDPOINT + "/coupons/createCoupon";
                ClientResponse clientResponse = WebUtils.INSTANCE.doCall(createCouponURL, HttpMethod.POST, couponReq.toString(), true);
                VExceptionFactory.INSTANCE.parseAndThrowException(clientResponse);
                String jsonString = clientResponse.getEntity(String.class);
                JSONObject couponRes = new JSONObject(jsonString);
                couponCode = couponRes.optString("code");
            }
        } catch (NumberFormatException ne) {
            throw new VException(ErrorCode.BAD_REQUEST_ERROR, "Offered price not in proper format");
        }
        return couponCode;
    }

    /**
     * Gets bundle/aio id from LS activity field
     *
     * @param p
     * @return
     */
    private String getEntityValue(LeadActivitySerialized p) {
        if (p != null && p.getFields() != null && p.getFields().getMx_Custom_4() != null) {
            String[] urlArray = p.getFields().getMx_Custom_4().split("/");
            return urlArray[urlArray.length - 1];
        }
        return null;
    }

    /**
     * Adding entry in baseInstallment for installment as per LS
     *
     * @param leadId
     * @param price
     * @param subscriptionFee
     * @param installmentCount
     * @param downpayment
     * @param installmentAmount
     * @param userId
     * @param entityId
     * @return
     * @throws VException
     */
    private Boolean addEditStudentBaseInstalment(String leadId, Integer price, String subscriptionFee, String
            installmentCount, String downpayment, String installmentAmount, Long userId, String entityId) throws VException {
        BaseInstalment baseInstalment = new BaseInstalment();
        Boolean flag = false;
        try {
            Integer noOfInstallments = Integer.parseInt(installmentCount);
            Integer downPaymentValue = Integer.parseInt(downpayment);
            Integer installmentAmountValue = Integer.parseInt(installmentAmount);
            Integer subscriptionFeeValue = Integer.parseInt(subscriptionFee);
            Integer couponPrice = (price / 100) - subscriptionFeeValue;
            Double priceTotal = Double.valueOf(price / 100);

            LocalDate localDate = LocalDate.now();
            List<BaseInstalmentInfo> baseInstalmentInfos = new ArrayList<>();

            Double instaAmount = 0.0;
            Double couponAmount = 0.0;
            Double amount = 0.0;
            Integer installmentWithDownpayment = noOfInstallments + 1;
            Integer installmentSum = 0;

            //calculating installment with coupon amount.
            for (int i = 0; i < installmentWithDownpayment; i++) {
                //1st entry is for downpayment by default
                if (i == 0) {
                    instaAmount = downPaymentValue.doubleValue() / subscriptionFeeValue.doubleValue();
                    couponAmount = instaAmount * couponPrice;
                    amount = downPaymentValue + couponAmount;
                } else if (i == installmentWithDownpayment - 1) {
                    amount = priceTotal;
                } else {
                    instaAmount = installmentAmountValue.doubleValue() / subscriptionFeeValue.doubleValue();
                    couponAmount = instaAmount * couponPrice;
                    amount = installmentAmountValue + couponAmount;
                }

                priceTotal -= amount;
                Integer baseInstallmentAmount = (int) (amount * 100);
                installmentSum += baseInstallmentAmount;
                //to check if the installment sum is < actual price of bundle/aio
                if (i == installmentWithDownpayment - 1 && installmentSum < price) {
                    //if yes then adding the diff in last installmet amt.
                    Integer installmentDiff = price - installmentSum;
                    baseInstallmentAmount += installmentDiff;
                }
                BaseInstalmentInfo baseInstalmentInfo = new BaseInstalmentInfo();
                baseInstalmentInfo.setAmount(baseInstallmentAmount);
                baseInstalmentInfo.setDueTime(Timestamp.valueOf(localDate.atStartOfDay()).getTime());
                baseInstalmentInfos.add(baseInstalmentInfo);
                localDate = localDate.plusMonths(1);
            }
            baseInstalment.setPurchaseEntityId(entityId);
            baseInstalment.setPurchaseEntityType(InstalmentPurchaseEntity.BUNDLE);
            baseInstalment.setUserId(userId);
            baseInstalment.setInfo(baseInstalmentInfos);
            logger.info("baseinstallment details: {}", new Gson().toJson(baseInstalment));

            String addEditBaseInstallmentURL = DINERO_ENDPOINT + "/payment/addEditStudentBaseInstalmentApp";
            String baseInstallmentReq = new Gson().toJson(baseInstalment);
            ClientResponse baseInstallmentRes = WebUtils.INSTANCE.doCall(addEditBaseInstallmentURL, HttpMethod.POST, baseInstallmentReq, true);
            logger.info("BaseInstallment res: {}", baseInstallmentRes);
            VExceptionFactory.INSTANCE.parseAndThrowException(baseInstallmentRes);
            if (baseInstallmentRes.getStatus() == 200) {
                flag = true;
            }
        } catch (NumberFormatException e) {
            throw new VException(ErrorCode.BAD_REQUEST_ERROR, "LS Payment info not in proper format..");
        }
        return flag;
    }

    private List<String> getInstallmentDates(String mx_custom_22) throws VException {
        List<String> installmentDates = new ArrayList<>();
        LocalDate localDate = LocalDate.now();
        try {
            Integer noOfInstallments = Integer.parseInt(mx_custom_22);
            installmentDates.add(localDate.format(DateTimeFormatter.ofPattern("dd MMM yyyy")));
            for (int i = 0; i < noOfInstallments; i++) {
                localDate = localDate.plusMonths(1);
                installmentDates.add(localDate.format(DateTimeFormatter.ofPattern("dd MMM yyyy")));
            }
        } catch (NumberFormatException e) {
            throw new VException(ErrorCode.BAD_REQUEST_ERROR, "Installment number wrong");
        }
        return installmentDates;
    }

    private Integer calculateRemainingAmount(String subscriptionFee, String downPayment) {
        Integer remainingAmount = 0;
        try {
            if (StringUtils.isNotEmpty(subscriptionFee) && StringUtils.isNotEmpty(downPayment)) {
                remainingAmount = Integer.parseInt(subscriptionFee) - Integer.parseInt(downPayment);
            }
        } catch (Exception e) {

        }
        return remainingAmount;
    }

    /**
     * sending Auto-enrollment details to student,
     * once the order is confimed
     *
     * @param text
     * @throws VException
     * @throws InterruptedException
     */
    public void sendAutoEnrollmentUpdate(String text) throws VException, InterruptedException {
        logger.info("Incoming request: {}", text);
        Orders order = new Gson().fromJson(text, Orders.class);

        String entityId = order.getItems().get(0).getEntityId();
        Long userId = order.getUserId();
        UserBasicInfo userBasicInfo = fosUtils.getUserBasicInfo(userId, true);

        //Get bundle/aio info from bundle
        BundleInfo bundleInfo = getBundleInfo(entityId);

        //get batchids from bundleid
        List<String> batchIds = getAutoEnrollBatchesFromBundle(entityId);

        //Get first class/session from bundle/entity/aio id
        BatchWiseSessionInfo firstClassInfo = getFirstClassInfo(batchIds);
        String firstClassStartTime = null;
        String firstClassDayName = null;
        String firstClassDayValue = null;
        String firstClassMonth = null;
        String firstClassTitle = null;
        String firstClassSubject = null;
        String firstClassPresenter = null;
        Boolean isFirstClass = false;
        if (firstClassInfo != null) {
            isFirstClass = true;
            DateTimeFormatter MAIL_DATE_FORMAT = DateTimeFormatter.ofPattern("E dd MMM hh:mma");
            LocalDateTime localStartTime = Instant.ofEpochMilli(firstClassInfo.getStartTime())
                    .atZone(ZoneId.of(IST_ZONE_ID))
                    .toLocalDateTime();
            String[] localStartTimeArray = localStartTime.format(MAIL_DATE_FORMAT).toUpperCase().split(" ");
            firstClassStartTime = localStartTimeArray[3];
            firstClassDayName = localStartTimeArray[0];
            firstClassDayValue = localStartTimeArray[1];
            firstClassMonth = localStartTimeArray[2];
            firstClassTitle = firstClassInfo.getTitle();
            firstClassSubject = firstClassInfo.getSubject();
            firstClassPresenter = firstClassInfo.getPresenter();

        }

        //Gets course titles
        List<String> cousreTitle = new ArrayList<>();
        List<AutoEnrollCourseInfo> autoEnrollCourseInfos = getAutoEnrollCourses(batchIds);
        List<TeacherBasicInfo> teacherBasicInfos = new ArrayList<>();
        if (CollectionUtils.isNotEmpty(autoEnrollCourseInfos)) {
            cousreTitle = autoEnrollCourseInfos.parallelStream().map(p -> p.getCourseTitle()).filter(Objects::nonNull).collect(Collectors.toList());
            List<String> teacherIds = autoEnrollCourseInfos.parallelStream().filter(p -> CollectionUtils.isNotEmpty(p.getTeacherIds()))
                    .flatMap(e -> e.getTeacherIds().stream())
                    .collect(Collectors.toList());
            if (CollectionUtils.isNotEmpty(teacherIds)) {
                teacherBasicInfos = getTeacherInfo(teacherIds);
            }
        }

        List<Map<String, String>> teacherSubjectList = new ArrayList<>();
        if (CollectionUtils.isNotEmpty(teacherBasicInfos)) {

            Set<Long> boardIds = teacherBasicInfos.parallelStream().filter(p -> p.getPrimarySubject() != null).map(p -> p.getPrimarySubject()).collect(Collectors.toSet());
            if (CollectionUtils.isNotEmpty(boardIds)) {
                Map<Long, Board> boardMap = getSubjectFromBoardId(new ArrayList<>(boardIds));

                for (TeacherBasicInfo teacherBasicInfo : teacherBasicInfos) {
                    String subject = null;
                    if (boardMap.containsKey(teacherBasicInfo.getPrimarySubject())) {
                        subject = boardMap.get(teacherBasicInfo.getPrimarySubject()).getName();
                    }
                    Map<String, String> teacherMap = new HashMap<>();
                    if (StringUtils.isNotEmpty(subject)) {
                        teacherMap.put("teacherName", teacherBasicInfo.getFullName());
                        teacherMap.put("subject", subject + TEACHER_SUBJECT_SUFFIX);
                    } else {
                        teacherMap.put("teacherName", teacherBasicInfo.getFullName());
                        teacherMap.put("subject", "");
                    }
                    teacherSubjectList.add(teacherMap);
                }
            }
        }

        HashMap<String, Object> bodyScopes = new LinkedHashMap<>();
        //Bundle details
        bodyScopes.put("bundleTitle", StringUtils.defaultIfEmpty(bundleInfo.getTitle()));
        if (CollectionUtils.isNotEmpty(bundleInfo.getGrade())) {
            bodyScopes.put("grade", org.apache.commons.lang3.StringUtils.join(bundleInfo.getGrade(), ","));
        } else {
            bodyScopes.put("grade", "");
        }
        bodyScopes.put("validTill", DATE_FORMAT.format(new Date(bundleInfo.getValidTill())));
        bodyScopes.put("userName", StringUtils.defaultIfEmpty(userBasicInfo.getFullName()));
        bodyScopes.put("userEmail", StringUtils.defaultIfEmpty(userBasicInfo.getEmail()));
        bodyScopes.put("userPhone", StringUtils.defaultIfEmpty(userBasicInfo.getContactNumber()));
        bodyScopes.put("isLiveClass", "true");
        bodyScopes.put("isDoubtSolving", bundleInfo.getUnLimitedDoubtsIncluded() != null ? bundleInfo.getUnLimitedDoubtsIncluded() : false);
        bodyScopes.put("isUnlimitedAccess", "true");
        bodyScopes.put("isUnlimitedPerformanceImprovement", "true");
        bodyScopes.put("isPersonalTeacherIncluded", bundleInfo.getAmIncluded() != null ? bundleInfo.getAmIncluded() : false);

        if (isFirstClass) {
            //first class details
            bodyScopes.put("isFirstClass", isFirstClass);
            bodyScopes.put("firstClassTitle", StringUtils.defaultIfEmpty(firstClassTitle));
            bodyScopes.put("firstClassSubject", StringUtils.defaultIfEmpty(firstClassSubject));
            bodyScopes.put("firstClassPresenter", StringUtils.defaultIfEmpty(firstClassPresenter));
            bodyScopes.put("firstClassBundleTitle", StringUtils.defaultIfEmpty(bundleInfo.getTitle()));
            bodyScopes.put("firstClassStartTime", StringUtils.defaultIfEmpty(firstClassStartTime));
            bodyScopes.put("firstClassDayName", StringUtils.defaultIfEmpty(firstClassDayName));
            bodyScopes.put("firstClassDayValue", StringUtils.defaultIfEmpty(firstClassDayValue));
            bodyScopes.put("firstClassMonth", StringUtils.defaultIfEmpty(firstClassMonth));
        } else {
            bodyScopes.put("isFirstClass", isFirstClass);
        }

        //courseTitles
        if (CollectionUtils.isNotEmpty(cousreTitle)) {
            bodyScopes.put("courseTitles", cousreTitle);
        } else {
            bodyScopes.put("courseTitles", "");
        }
        bodyScopes.put("teacherSubjectList", teacherSubjectList);

        //Adding PaymentDetails
        List<LeadDetails> studentLeadDetails = null;
        Boolean isVEMI = false;
        Boolean isPaymentInfo = false;

        if (StringUtils.isNotEmpty(userBasicInfo.getContactNumber())) {
            logger.info("fetch lead details by Phone");
            studentLeadDetails = leadsquaredManager.fetchLeadsByPhoneOrEmail(userBasicInfo.getContactNumber());
        }
        if (CollectionUtils.isEmpty(studentLeadDetails)) {
            logger.info("fetch lead details by Email, as Phone not present");
            studentLeadDetails = leadsquaredManager.fetchLeadsByPhoneOrEmail(userBasicInfo.getEmail());
        }
        if (CollectionUtils.isEmpty(studentLeadDetails)) {
            logger.info("No Lead present in LS with userId: {}", userBasicInfo.getUserId());
        } else {
            String leadId = studentLeadDetails.get(0).getProspectID();
            if (StringUtils.isNotEmpty(leadId)) {
                LeadSquaredActivityFields leadSquaredActivityFields = getEligibleLSActivityFields(leadId, 331, entityId);

                if (Objects.nonNull(leadSquaredActivityFields)) {
                    isPaymentInfo = true;
                    bodyScopes.put("subscriptionFee", StringUtils.defaultIfEmpty(leadSquaredActivityFields.getMx_Custom_10()));
                    if (!"Bulk".equals(leadSquaredActivityFields.getMx_Custom_11())) {
                        logger.info("inside installment");
                        Integer remainingAmount = calculateRemainingAmount(leadSquaredActivityFields.getMx_Custom_10(), leadSquaredActivityFields.getMx_Custom_13());
                        bodyScopes.put("amountPaid", StringUtils.defaultIfEmpty(leadSquaredActivityFields.getMx_Custom_13()));
                        bodyScopes.put("remainingAmount", remainingAmount != null ? String.valueOf(remainingAmount) : "");
                        bodyScopes.put("installmentAmount", StringUtils.defaultIfEmpty(leadSquaredActivityFields.getMx_Custom_23()));
                        bodyScopes.put("remainingInstallment", StringUtils.defaultIfEmpty(leadSquaredActivityFields.getMx_Custom_22()));
                        isVEMI = true;
                    }
                }
            }
        }
        bodyScopes.put("isPaymentInfo", isPaymentInfo);
        bodyScopes.put("isVEMI", isVEMI);
        logger.info("Body scopes for sending email: {}", new Gson().toJson(bodyScopes));
        Boolean isEmailSent = sendAutoEnrollmentConfirmationEmail(bodyScopes, userBasicInfo.getEmail());
        logger.info("Auto Enrollment Confirmation email sent for user : {}, bundle: {}, isSuccess: {}", userBasicInfo.getUserId(), entityId, isEmailSent);
    }

    private Boolean sendAutoEnrollmentConfirmationEmail(HashMap<String, Object> bodyScopes, String email) throws
            VException {

        try {
            //sending email
            CommunicationType emailType = CommunicationType.AUTO_ENROLLMENT_UPDATE;
            HashMap<String, Object> subjectScopes = new HashMap<>();
            ArrayList<InternetAddress> toAddress = new ArrayList<>();

            if (StringUtils.isNotEmpty(email)) {
                toAddress.add(new InternetAddress(email));
                EmailRequest request = new EmailRequest(toAddress, subjectScopes, bodyScopes, emailType, Role.STUDENT);
                request.setIncludeHeaderFooter(Boolean.FALSE);
                communicationManager.sendEmail(request);
            }
        } catch (AddressException e) {
            logger.error(e);
            throw new VException(ErrorCode.SERVICE_ERROR, "Error while sending Enrollment Confirmation email" + e);
        }
        return true;
    }

}
