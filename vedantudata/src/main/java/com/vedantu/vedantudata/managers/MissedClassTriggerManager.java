package com.vedantu.vedantudata.managers;

import com.vedantu.User.StudentInfo;
import com.vedantu.User.User;
import com.vedantu.exception.VException;
import com.vedantu.onetofew.pojo.GTTAttendeeDetailsInfo;
import com.vedantu.util.LogFactory;
import com.vedantu.vedantudata.controllers.ActivityController;
import com.vedantu.vedantudata.utils.ActivityEventConstants;
import com.vedantu.vedantudata.utils.LeadsquaredActivityHelper;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class MissedClassTriggerManager extends LeadsquaredActivityHelper{


    @Autowired
    private ActivityManager activityManager;

    private Map<Long, User> studentAmMapping;

    @Autowired
    private LogFactory logFactory;

    private Logger logger = logFactory.getLogger(MissedClassTriggerManager.class);


    public void process(List<User> activeEnrolledUsers, Map<Long, User> studentAmMapping) throws VException {

        if(activeEnrolledUsers.isEmpty())
            return;

        this.studentAmMapping = studentAmMapping;
        //6-8 grade
        processMissedClassTriggerForBatch(activeEnrolledUsers, 6, 8, 2);

        //9-10 grade
        processMissedClassTriggerForBatch(activeEnrolledUsers, 9, 10, 3);

        //11-13 grade
        processMissedClassTriggerForBatch(activeEnrolledUsers, 11, 13, 4);
    }

    public void processEarlyEducation(List<User> activeEnrolledUsers, Map<Long, User> studentAmMapping) throws VException {

        if (activeEnrolledUsers.isEmpty())
            return;

        this.studentAmMapping = studentAmMapping;

        //early education
        processMissedClassTriggerForBatch(activeEnrolledUsers, 4, 8, 1);
    }

    private void processMissedClassTriggerForBatch(List<User> activeEnrolledUsers, int fromGrade, int toGrade, int sessionCount) throws VException {

        List<User> firstGradeUsersBatch = activityManager.getFilteredUserByGradeRange(activeEnrolledUsers, fromGrade, toGrade);

        checkTheAttendanceAndPushToQueue(firstGradeUsersBatch, sessionCount);
    }



    private void checkTheAttendanceAndPushToQueue(List<User> usersBatch, int sessionCount) throws VException {
        List<String> userIdList = usersBatch.parallelStream()
                .filter(user -> user.getId() != null)
                .map(user -> user.getId().toString())
                .distinct()
                .collect(Collectors.toList());

        Long thruTime = Instant.now().toEpochMilli();

        //past 15 days
        Long fromTime = thruTime - ActivityEventConstants.MILLI_DAY * 15;
        List<GTTAttendeeDetailsInfo> gttAttendeeInfos = activityManager.getGttAttendeeDetails(userIdList, fromTime, thruTime);

        logger.info("======fetched gttdattendee for batch users : {}", gttAttendeeInfos.size());

        Map<String,List<GTTAttendeeDetailsInfo>> gttAttendeeStudentMap = gttAttendeeInfos.stream().collect(Collectors.groupingBy(GTTAttendeeDetailsInfo::getUserId));

        for (User user : usersBatch) {

            List<GTTAttendeeDetailsInfo> filteredAttendeeDetails = gttAttendeeStudentMap.get(user.getId().toString());

            if(filteredAttendeeDetails==null || filteredAttendeeDetails.size() < sessionCount)
                continue;

            logger.info("======fetched gttdattendee for user : {}", filteredAttendeeDetails.size());

            filteredAttendeeDetails = filteredAttendeeDetails.subList(0, sessionCount);
            boolean isAttendedAny = filteredAttendeeDetails.parallelStream().
                    filter(gttAttendeeInfo -> (gttAttendeeInfo.getJoinTimes() != null && !gttAttendeeInfo.getJoinTimes().isEmpty()))
                    .count() > 0;

            logger.info("======Check if student missed any class");
            logger.info("======isAttendedAny {}", isAttendedAny);

            if (isAttendedAny) {
                continue;
            }

            Map<String, String> param = new HashMap<>();
            String grade = ActivityEventConstants.getGradeValue(user.getStudentInfo().getGrade());
            param.put(ActivityEventConstants.GRADE_VALUE, grade);
            param.put(ActivityEventConstants.NOTE,ActivityEventConstants.ACTIVITY_EVENT_MISSED_CLASS_NOTE);

            activityManager.pushActivity(user, studentAmMapping, ActivityEventConstants.EVENT_MISSED_CLASS, param);
        }
    }


}
