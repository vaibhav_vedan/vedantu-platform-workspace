package com.vedantu.vedantudata.managers;

import com.vedantu.vedantudata.dao.DemoOTPValidationDao;
import com.vedantu.vedantudata.entities.DemoOTPValidation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DemoOTPValidationManager {

    @Autowired
    private DemoOTPValidationDao demoOTPValidationDao;

    public void save(String prospectActivityId, String leadEmail, String phoneNumber, String otp, Long otpExpiryTime) {
        demoOTPValidationDao.save(prospectActivityId, leadEmail, phoneNumber, otp,otpExpiryTime);
    }

    public DemoOTPValidation getLatestActiveOtpDetails(String leadEmail) {
        return demoOTPValidationDao.getLatestActiveOtpDetails(leadEmail);
    }
}
