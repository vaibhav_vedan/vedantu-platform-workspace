package com.vedantu.vedantudata.managers.leadsquared;

import com.vedantu.User.Role;
import com.vedantu.User.User;
import com.vedantu.User.request.DemoTriggerCallToAgentReq;
import com.vedantu.User.response.ConnectSessionCallRes;
import com.vedantu.exception.VException;
import com.vedantu.notification.enums.CommunicationType;
import com.vedantu.notification.requests.TextSMSRequest;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.vedantudata.entities.ActivityDetails;
import com.vedantu.vedantudata.entities.leadsquared.LeadDetails;
import com.vedantu.vedantudata.entities.leadsquared.LeadsquaredUser;
import com.vedantu.vedantudata.managers.ActivityManager;
import com.vedantu.vedantudata.managers.CommunicationManager;
import com.vedantu.vedantudata.managers.DemoOTPValidationManager;
import com.vedantu.vedantudata.pojos.*;
import com.vedantu.vedantudata.utils.ActivityEventConstants;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class LeadSquaredWebhooksManager {

    @Autowired
    private ActivityManager activityManager;

    @Autowired
    private LeadsquaredManager leadsquaredManager;

    @Autowired
    private DemoOTPValidationManager demoOTPValidationManager;

    @Autowired
    public CommunicationManager communicationManager;

    @Autowired
    private LogFactory logFactory;

    private Logger logger = logFactory.getLogger(LeadSquaredWebhooksManager.class);


    public void updateActivity(ActivityCallbackPojoMapper activityCallbackPojoMapper) {

        ActivityDetailsCallbackPojo activityDetailsBefore = activityCallbackPojoMapper.getBefore();
        ActivityDetailsCallbackPojo activityDetailsAfter = activityCallbackPojoMapper.getAfter();

        logger.info("Before data : {}", activityDetailsBefore.toString());
        logger.info("After data : {}", activityDetailsAfter.toString());

        updateActivityDetails(activityDetailsBefore, activityDetailsAfter);
    }

    /*   private void recordFormChanges(ActivityDetailsCallbackPojo activityDetailsBefore, ActivityDetailsCallbackPojo activityDetailsAfter) {

           Integer event = Integer.parseInt(activityDetailsAfter.getActivityEvent());

           switch (event) {
               case ActivityEventConstants.FORM_SALES_TEAM_CONFIRMATION:
                   persistFormSalesTeamConfirmation(activityDetailsAfter);
                   break;
               case ActivityEventConstants.FORM_VERIFICATION_STATUS:
                   persistFormVerificationStatus(activityDetailsAfter);
                   break;
               case ActivityEventConstants.FORM_INTRODUCTION_TEAM:
                   persistFormIntroductionTeam(activityDetailsAfter);
                   break;
               case ActivityEventConstants.FORM_DEMO_TEAM:
                   persistFormDemoTeam(activityDetailsAfter);
                   break;
               case ActivityEventConstants.FORM_FINTECH:
                   persistFintech(activityDetailsAfter);
                   break;
           }

       }

       private void persistFintech(ActivityDetailsCallbackPojo activityDetailsAfter) {

           try {

               String enrollmentVerifiedBy = activityDetailsAfter.getOwner();
               String acivityDateTime = activityDetailsAfter.getActivityDateTime();
               String studentEmail = activityDetailsAfter.getEmailAddress();
               String loanStatus = activityDetailsAfter.getMx_Custom_5();
               String loanId = activityDetailsAfter.getMx_Custom_1();
               String fintechPartnerName = activityDetailsAfter.getMx_Custom_2();
               String noOfInstallments = activityDetailsAfter.getMx_Custom_3();
               String approvedLoanValue = activityDetailsAfter.getMx_Custom_4();
               String couponCode = activityDetailsAfter.getMx_Custom_6();
               String notes = activityDetailsAfter.getActivityEvent_Note();

               String insertStatement = "INSERT INTO fintech_form(" +
                       "enrollment_verified_by,acivity_date_time,student_email,loan_status,loan_id," +
                       "fintech_partner_name,number_of_installments,approved_loan_value,coupon_code,notes)values('" +
                       enrollmentVerifiedBy + "','" +
                       acivityDateTime + "','" +
                       studentEmail + "','" +
                       loanStatus + "','" +
                       loanId + "','" +
                       fintechPartnerName + "','" +
                       noOfInstallments + "','" +
                       approvedLoanValue + "','" +
                       couponCode + "','" +
                       notes + "'"
                       + ");";


               logger.debug("Query :: {}", insertStatement);
               postgressHandler.execute(insertStatement);

           } catch (Exception e) {
               logger.error("error persisting record");
           }
       }

       private void persistFormDemoTeam(ActivityDetailsCallbackPojo activityDetailsAfter) {

           try {
               String activityDateTime = activityDetailsAfter.getActivityDateTime();
               String demoExpert = activityDetailsAfter.getOwner();
               String studentEmailAddress = activityDetailsAfter.getEmailAddress();
               String parentName = activityDetailsAfter.getMx_Parent_name();
               String parentEmailId = activityDetailsAfter.getMx_Parent_emailId();
               String parentContactNumber = activityDetailsAfter.getMx_Parent_Contact_number();
               String prefModeOfComunication = activityDetailsAfter.getMx_Mode_Of_Communication_for_Parents();
               String studentContactNumber = activityDetailsAfter.getMx_Custom_2();
               String dayToComunicate = activityDetailsAfter.getMx_Custom_3();
               String studentAddress = activityDetailsAfter.getMx_Student_Address();
               String isStudentLivingWithParent = activityDetailsAfter.getMx_Custom_6();
               String studentSchoolName = activityDetailsAfter.getMx_Student_School_Name();
               String skypeId = activityDetailsAfter.getMx_Student_Skype_ID();
               String reconfirmedCourseDetails = activityDetailsAfter.getMx_Custom_9();
               String mismatch = activityDetailsAfter.getMx_Custom_10();
               String howToLoginExplntn = activityDetailsAfter.getMx_Custom_11();
               String howToAttend = activityDetailsAfter.getMx_Custom_12();
               String differenceOfTest = activityDetailsAfter.getMx_Custom_13();
               String detailsExplained = activityDetailsAfter.getMx_Custom_5();
               String howTheDoubtsCanBeSolved = activityDetailsAfter.getMx_Custom_8();
               String oneToOneSession = activityDetailsAfter.getMx_Custom_14();
               String statusOfTheStudentAttendingSessions = activityDetailsAfter.getMx_Status_Of_The_Student_Attending_The_Session();
               String constructOfWhatsapp = activityDetailsAfter.getMx_Custom_15();
               String followUpCall = activityDetailsAfter.getMx_Custom_1();
               String followUpDate = activityDetailsAfter.getMx_Custom_7();
               String status = activityDetailsAfter.getStatus();
               String salesAgent = activityDetailsAfter.getMx_Custom_4();
               String note = activityDetailsAfter.getActivityEvent_Note();


               String insertStatement = "INSERT INTO demo_team(" +
                       "acivity_date_time,demo_expert_name,student_registered_email_id,parent_name,parent_email_id,parent_contact_number," +
                       "preferred_mode_of_comunication_for_parent,contact_number_for_communicating_with_the_student," +
                       "best_days_and_time_to_communicate_with_the_student,complete_postal_address_of_the_student,is_the_student_living_with_parent," +
                       "student_school_name,skype_id_to_reach_student,reconfirmed_the_course_related_details," +
                       "was_there_any_mismatch_in_the_expectation_of_the_parent_or_student_regarding_course_details," +
                       "explained_on_how_to_login__post_login_page_of_the_platform," +
                       "explained_them_on_how_to_attend_the_session_and_the_class_taking_platform," +
                       "explained_the_difference_between_live_courses_concept_videos_and_practice_tests,above_mentioned_details_has_been_explained," +
                       "explained_about_how_they_can_get_their_doubts_solved_outside_the_class," +
                       "told_them_if_required_their_personal_teacher_will_conduct_one_to_one_sessions,status_of_the_student_attending_the_session," +
                       "explain_the_construct_of_whatsapp_group_and_above_mentioned_criteria,need_a_followup_call_on_demo," +
                       "follow_up_date_for_demo,demo_status,sales_agent_name,additional_comments_from_student_Parent)values('" +
                       activityDateTime + "','" +
                       demoExpert + "','" +
                       studentEmailAddress + "','" +
                       parentName + "','" +
                       parentEmailId + "','" +
                       parentContactNumber + "','" +
                       prefModeOfComunication + "','" +
                       studentContactNumber + "','" +
                       dayToComunicate + "','" +
                       studentAddress + "','" +
                       isStudentLivingWithParent + "','" +
                       studentSchoolName + "','" +
                       skypeId + "','" +
                       reconfirmedCourseDetails + "','" +
                       mismatch + "','" +
                       howToLoginExplntn + "','" +
                       howToAttend + "','" +
                       differenceOfTest + "','" +
                       detailsExplained + "','" +
                       howTheDoubtsCanBeSolved + "','" +
                       oneToOneSession + "','" +
                       statusOfTheStudentAttendingSessions + "','" +
                       constructOfWhatsapp + "','" +
                       followUpCall + "','" +
                       followUpDate + "','" +
                       status + "','" +
                       salesAgent + "','" +
                       note + "'" +
                       ");";

               logger.debug("Query :: {}", insertStatement);
               postgressHandler.execute(insertStatement);
           } catch (Exception e) {
               logger.error("error persisting record");
           }
       }

       private void persistFormIntroductionTeam(ActivityDetailsCallbackPojo activityDetailsAfter) {

           try {
               String activityDateTime = activityDetailsAfter.getActivityDateTime();
               String owner = activityDetailsAfter.getOwner();
               String firstName = activityDetailsAfter.getFirstName();
               String mobile = activityDetailsAfter.getMobile();
               String teacherName = activityDetailsAfter.getMx_Custom_1();
               String teacherEmail = activityDetailsAfter.getMx_Custom_2();
               String teacherContactnumber = activityDetailsAfter.getMx_Custom_3();
               String aboutVedantu = activityDetailsAfter.getMx_Custom_4();
               String reconfirmedCourseDetails = activityDetailsAfter.getMx_Custom_5();
               String wrongExpectation = activityDetailsAfter.getMx_Custom_6();
               String status = activityDetailsAfter.getStatus();
               String overallScore = activityDetailsAfter.getMx_Overall_Score();
               String otherTution = activityDetailsAfter.getMx_Custom_9();
               String collectedRequiredDetails = activityDetailsAfter.getMx_Custom_11();
               String droppedVideoUrl = activityDetailsAfter.getMx_Custom_12();
               String followupCall = activityDetailsAfter.getMx_Custom_8();
               String sharedAgenda = activityDetailsAfter.getMx_Custom_14();
               String productDemoDate = activityDetailsAfter.getMx_Custom_13();
               String studentDevice = activityDetailsAfter.getMx_Student_Device();
               String internetServiceProvider = activityDetailsAfter.getMx_Internet_Service_Provider();
               String preferredLanguage = activityDetailsAfter.getMx_User_Preferred_Language();
               String callStatus = activityDetailsAfter.getMx_Custom_15();
               String salesAgent = activityDetailsAfter.getMx_Custom_10();
               String reasonForNoCall = activityDetailsAfter.getActivityEvent_Note();


               String insertStatement = "INSERT INTO introduction_team_form(" +
                       "acivity_date_time,owner,first_name,mobile_number,personal_teacher_name,personal_teacher_email_id," +
                       "personal_teacher_contact_number,how_did_the_user_get_to_know_about_vedantu,reconfirmed_the_course_related_details," +
                       "was_there_any_wrong_expectations_set_by_the_sales_agent,what_student_is_preparing_for,overall_score_in_the_previous_grade," +
                       "is_the_student_taking_anything_other_tuition_apart_from_vedantu,collected_the_required_details_from_the_student_or_parent," +
                       "dropped_the_video_tutorials_url_on_the_parent_whatsApp_number,need_a_follow_up_call_to_get_remaining_data," +
                       "if_this_call_was_successful_shared_the_agenda_for_the_next_call,date_and_time_for_product_demo," +
                       "which_device_would_the_student_use_for_the_sessions,which_among_these_is_the_internet_service_provider_for_the_student," +
                       "preferred_language_for_the_interaction,introduction_call_status,sales_agent_name,reason_for_no_introduction" +
                       ")values('" +
                       activityDateTime + "','" + owner + "','" + firstName + "','" + mobile + "','" +
                       teacherName + "','" +
                       teacherEmail + "','" +
                       teacherContactnumber + "','" +
                       aboutVedantu + "','" +
                       reconfirmedCourseDetails + "','" + wrongExpectation + "','" + status + "','" + overallScore + "','" + otherTution + "','" +
                       collectedRequiredDetails + "','" + droppedVideoUrl + "','" + followupCall + "','" + sharedAgenda + "','" + productDemoDate + "','" +
                       studentDevice + "','" + internetServiceProvider + "','" + preferredLanguage + "','" + callStatus + "','" +
                       salesAgent + "','" + reasonForNoCall + "');";

               logger.debug("Query :: {}", insertStatement);
               postgressHandler.execute(insertStatement);
           } catch (Exception e) {
               logger.error("error persisting record");
           }
       }

       private void persistFormSalesTeamConfirmation(ActivityDetailsCallbackPojo activityDetailsAfter) {

           try {
               String activityDateTime = activityDetailsAfter.getActivityDateTime();
               String studentName = activityDetailsAfter.getMx_Student_Name();
               String emailAddress = activityDetailsAfter.getEmailAddress();
               String studentMobileNumber = activityDetailsAfter.getMobile();
               String parentName = activityDetailsAfter.getMx_Parent_name();
               String parentEmailId = activityDetailsAfter.getMx_Parent_emailId();
               String parentContactNumber = activityDetailsAfter.getMx_Parent_Contact_number();
               String alternateNumbers = activityDetailsAfter.getMx_PhoneNumberList();
               String typeOfCourse = activityDetailsAfter.getMx_Custom_2();
               String nameOfTheCourse = activityDetailsAfter.getMx_Custom_3();
               String courseLink = activityDetailsAfter.getMx_Custom_4();
               String batchStartDate = activityDetailsAfter.getMx_Custom_5();
               String numberOfSessionsPerWeek = activityDetailsAfter.getMx_Custom_6();
               String sessionTimings = activityDetailsAfter.getMx_Custom_7();
               String additionalOfferings = activityDetailsAfter.getMx_Custom_8();
               String sessionDuration = activityDetailsAfter.getMx_Custom_1();
               String courseValue = activityDetailsAfter.getMx_Custom_9();
               String courseOfferedPrice = activityDetailsAfter.getMx_Custom_10();
               String paymentType = activityDetailsAfter.getMx_Custom_11();
               String thirdPartyLoanType = activityDetailsAfter.getStatus();
               String loanAmount = activityDetailsAfter.getMx_Custom_12();
               String downPaymentAmount = activityDetailsAfter.getMx_Custom_13();
               String modeOfPaymentDp = activityDetailsAfter.getMx_ModeOfPaymentDP();
               String vedantuInstallmentCount = activityDetailsAfter.getMx_Custom_14();
               String installmentAmount = activityDetailsAfter.getMx_InstallmentAmount();
               String additionalComments = activityDetailsAfter.getActivityEvent_Note();
               String inductionCallDateTime = activityDetailsAfter.getMx_Custom_15();
               String owner = activityDetailsAfter.getOwner();


               String insertStatement = "INSERT INTO sales_team_confirmation_form(" +
                       "acivity_date_time,student_name,email_address,student_mobile_number,parent_name,parent_email_id," +
                       "parent_contact_number,alternate_numbers,type_of_course,name_of_the_course,course_link,batch_start_date," +
                       "number_of_sessions_per_week,session_timings,additional_offerings,session_duration,course_value,course_offered_price," +
                       "payment_type,third_party_loan_type,loan_amount,down_payment_amount,mode_of_payment_dp,vedantu_installment_count," +
                       "installment_amount,additional_comments,induction_call_date_time,owner" +
                       ")values('" +
                       activityDateTime + "','" + studentName + "','" + emailAddress + "','" + studentMobileNumber + "','" + parentName + "','" +
                       parentEmailId + "','" + parentContactNumber + "','" + alternateNumbers + "','" + typeOfCourse + "','" + nameOfTheCourse + "','" +
                       courseLink + "','" + batchStartDate + "','" + numberOfSessionsPerWeek + "','" + sessionTimings + "','" +
                       additionalOfferings + "','" + sessionDuration + "','" + courseValue + "','" + courseOfferedPrice + "','" +
                       paymentType + "','" + thirdPartyLoanType + "','" + loanAmount + "','" + downPaymentAmount + "','" +
                       modeOfPaymentDp + "','" + vedantuInstallmentCount + "','" + installmentAmount + "','" + additionalComments + "','" +
                       inductionCallDateTime + "','" + owner + "');";


               logger.debug("Query :: {}", insertStatement);
               postgressHandler.execute(insertStatement);
           } catch (Exception e) {
               logger.error("error persisting record");
           }
       }

       private void persistFormVerificationStatus(ActivityDetailsCallbackPojo activityDetailsAfter) {

           try {
               String note = activityDetailsAfter.getActivityEvent_Note();
               String status = activityDetailsAfter.getStatus();
               String owner = activityDetailsAfter.getOwner();
               String createdBy = activityDetailsAfter.getCreatedBy();
               String createdByName = activityDetailsAfter.getCreatedByName();
               String createdOn = activityDetailsAfter.getCreatedOn();
               String modofiedBy = activityDetailsAfter.getModifiedBy();
               String modifiedByName = activityDetailsAfter.getModifiedByName();
               String modifiedOn = activityDetailsAfter.getModifiedOn();

               String callBackDate = activityDetailsAfter.getMx_Custom_1();
               String doubtApp = activityDetailsAfter.getMx_Custom_2();
               String inductionCallConfirmation = activityDetailsAfter.getMx_Custom_3();
               String inductionCallDateTime = activityDetailsAfter.getMx_Custom_4();
               String refundPolicy = activityDetailsAfter.getMx_Custom_5();
               String salesAgentName = activityDetailsAfter.getMx_Custom_6();
               String reasonForNotVerified = activityDetailsAfter.getMx_Custom_7();
               String typeOfCourse = activityDetailsAfter.getMx_Custom_8();
               String teamType = activityDetailsAfter.getMx_Custom_9();

               String insertStatement = "INSERT INTO verification_status(" +
                       "status,owner,note,created_by,created_by_name,created_on,modofied_by," +
                       "modified_by_name,modified_on,call_back_date,doubt_app,induction_call_confirmation," +
                       "induction_call_date_time,refund_policy,sales_agent_name,reason_for_not_verified,type_of_course," +
                       "team_type" +
                       ")values('" + status + "','" + owner + "','" + note + "','" + createdBy + "','" +
                       createdByName + "','" + createdOn + "','" + modofiedBy + "','" + modifiedByName + "','" +
                       modifiedOn + "','" + callBackDate + "','" + doubtApp + "','" + inductionCallConfirmation + "','" +
                       inductionCallDateTime + "','" + refundPolicy + "','" + salesAgentName + "','" +
                       reasonForNotVerified + "','" + typeOfCourse + "','" + teamType + "');";


               logger.debug("Query :: {}", insertStatement);
               postgressHandler.execute(insertStatement);
           } catch (Exception e) {
               logger.error("error persisting record");
           }
       }
   */
    private void updateActivityDetails(ActivityDetailsCallbackPojo activityDetailsBefore, ActivityDetailsCallbackPojo activityDetailsAfter) {

        String prospectActivityId = activityDetailsAfter.getProspectActivityId();
        String modifiedBy = activityDetailsAfter.getModifiedBy();
        String modifiedByName = activityDetailsAfter.getModifiedByName();

        ActivityDetails activityDetails = activityManager.getActivityById(prospectActivityId);

        if(activityDetails==null){
            logger.warn("No Record found with the prospect activity id : {}", prospectActivityId);
            return;
        }

        logger.warn("Record found with the prospect activity id : {}", prospectActivityId);

        activityDetails.setModifiedBy(modifiedBy);
        activityDetails.setModifiedByName(modifiedByName);

        List<LeadsquaredActivityField> leadsquaredActivityFieldsList = activityDetails.getFields();

        for (LeadsquaredActivityField leadsquaredActivityField : leadsquaredActivityFieldsList) {
            if (ActivityEventConstants.OWNER.equalsIgnoreCase(leadsquaredActivityField.getSchemaName())) {
                if(StringUtils.isNotEmpty(activityDetailsAfter.getOwner()))
                    leadsquaredActivityField.setValue(activityDetailsAfter.getOwner());
            } else if (ActivityEventConstants.STATUS.equalsIgnoreCase(leadsquaredActivityField.getSchemaName())) {
                if(StringUtils.isNotEmpty(activityDetailsAfter.getStatus()))
                    leadsquaredActivityField.setValue(activityDetailsAfter.getStatus());
            }
        }

        //Check if status changed
        if (activityDetailsBefore.getStatus()!=null && activityDetailsAfter.getStatus()!=null && !activityDetailsBefore.getStatus().equalsIgnoreCase(activityDetailsAfter.getStatus())) {

            //Maintan status change history
            ActivityStatusChangeHistory statusChangeHistory = new ActivityStatusChangeHistory();
            statusChangeHistory.setStatusBefore(activityDetailsBefore.getStatus());
            statusChangeHistory.setStatusAfter(activityDetailsAfter.getStatus());
            statusChangeHistory.setModifiedBy(modifiedBy);
            statusChangeHistory.setModifiedByName(modifiedByName);
            statusChangeHistory.setModifiedOn(new Date().getTime());

            List<ActivityStatusChangeHistory> statusHistories;
            if (activityDetails.getStatusChangeHistory() == null)
                statusHistories = new ArrayList<>();
            else
                statusHistories = activityDetails.getStatusChangeHistory();

            statusHistories.add(statusChangeHistory);

            activityDetails.setStatusChangeHistory(statusHistories);
        }

        //Check if owner changed
        if (activityDetailsBefore.getOwner()!=null && activityDetailsAfter.getOwner()!=null && !activityDetailsBefore.getOwner().equalsIgnoreCase(activityDetailsAfter.getOwner())) {

            //Maintan owner change history
            ActivityOwnerChangeHistory ownerChangeHistory = new ActivityOwnerChangeHistory();
            ownerChangeHistory.setOwnerBefore(activityDetailsBefore.getOwner());
            ownerChangeHistory.setOwnerAfter(activityDetailsAfter.getOwner());
            ownerChangeHistory.setModifiedBy(modifiedBy);
            ownerChangeHistory.setModifiedByName(modifiedByName);
            ownerChangeHistory.setModifiedOn(new Date().getTime());

            List<ActivityOwnerChangeHistory> ownerChangeHistories;
            if (activityDetails.getOwnerChangeHisory() == null)
                ownerChangeHistories = new ArrayList<>();
            else
                ownerChangeHistories = activityDetails.getOwnerChangeHisory();

            ownerChangeHistories.add(ownerChangeHistory);

            activityDetails.setOwnerChangeHisory(ownerChangeHistories);

        }

        customFieldUpdate(activityDetailsAfter.getMx_Custom_1(), ActivityEventConstants.MX_CUSTOM_1, leadsquaredActivityFieldsList);
        customFieldUpdate(activityDetailsAfter.getMx_Custom_2(), ActivityEventConstants.MX_CUSTOM_2, leadsquaredActivityFieldsList);
        customFieldUpdate(activityDetailsAfter.getMx_Custom_3(), ActivityEventConstants.MX_CUSTOM_3, leadsquaredActivityFieldsList);
        customFieldUpdate(activityDetailsAfter.getMx_Custom_4(), ActivityEventConstants.MX_CUSTOM_4, leadsquaredActivityFieldsList);
        customFieldUpdate(activityDetailsAfter.getMx_Custom_5(), ActivityEventConstants.MX_CUSTOM_5, leadsquaredActivityFieldsList);
        customFieldUpdate(activityDetailsAfter.getMx_Custom_6(), ActivityEventConstants.MX_CUSTOM_6, leadsquaredActivityFieldsList);
        customFieldUpdate(activityDetailsAfter.getMx_Custom_7(), ActivityEventConstants.MX_CUSTOM_7, leadsquaredActivityFieldsList);
        customFieldUpdate(activityDetailsAfter.getMx_Custom_8(), ActivityEventConstants.MX_CUSTOM_8, leadsquaredActivityFieldsList);
        customFieldUpdate(activityDetailsAfter.getMx_Custom_9(), ActivityEventConstants.MX_CUSTOM_9, leadsquaredActivityFieldsList);
        customFieldUpdate(activityDetailsAfter.getMx_Custom_10(), ActivityEventConstants.MX_CUSTOM_10, leadsquaredActivityFieldsList);
        customFieldUpdate(activityDetailsAfter.getMx_Custom_10(), ActivityEventConstants.MX_CUSTOM_11, leadsquaredActivityFieldsList);
        customFieldUpdate(activityDetailsAfter.getMx_Custom_10(), ActivityEventConstants.MX_CUSTOM_12, leadsquaredActivityFieldsList);
        customFieldUpdate(activityDetailsAfter.getMx_Custom_10(), ActivityEventConstants.MX_CUSTOM_13, leadsquaredActivityFieldsList);
        customFieldUpdate(activityDetailsAfter.getMx_Custom_10(), ActivityEventConstants.MX_CUSTOM_14, leadsquaredActivityFieldsList);
        customFieldUpdate(activityDetailsAfter.getMx_Custom_10(), ActivityEventConstants.MX_CUSTOM_15, leadsquaredActivityFieldsList);

        activityDetails.setFields(leadsquaredActivityFieldsList);
        activityManager.updateActivity(activityDetails);
        logger.warn("Updated changes");
    }

    private void customFieldUpdate(String fieldValue, String field, List<LeadsquaredActivityField> leadsquaredActivityFieldsList) {
        if (fieldValue != null) {
            boolean isFound = false;
            for (LeadsquaredActivityField leadsquaredActivityField : leadsquaredActivityFieldsList) {
                if (field.equalsIgnoreCase(leadsquaredActivityField.getSchemaName())) {
                    leadsquaredActivityField.setValue(fieldValue);
                    isFound = true;
                    break;
                }
            }

            if (!isFound) {
                LeadsquaredActivityField leadsquaredActivityField = new LeadsquaredActivityField();
                leadsquaredActivityField.setSchemaName(field);
                leadsquaredActivityField.setValue(fieldValue);
                leadsquaredActivityFieldsList.add(leadsquaredActivityField);
            }
        }
    }

    public void captureDemoCallback(ActivityCreatedCallbackPojo activityDetailsCallbackPojo) throws VException, InterruptedException {

        String status = activityDetailsCallbackPojo.getData().getStatus();

        if(!status.equalsIgnoreCase("Start"))
            return;

        String prospectId = activityDetailsCallbackPojo.getRelatedProspectId();
        String ownerId = activityDetailsCallbackPojo.getData().getOwner();
        String prospectActivityId = activityDetailsCallbackPojo.getProspectActivityId();

        logger.info("Fetching lead details for the lead id:: {}",prospectId);
        List<LeadDetails> leadDetailsList =leadsquaredManager.fetchLeadsByLeadId(prospectId);

        if(leadDetailsList ==null || leadDetailsList.isEmpty()) {
            logger.error("Lead details not found with the given id::{}", prospectId);
            return;
        }

        LeadDetails  leadDetails = leadDetailsList.iterator().next();
        logger.info("Lead details :: {}", leadDetails);

        String leadEmail = leadDetails.getEmailAddress();

        List<LeadsquaredUser> LeadsquaredUserList =leadsquaredManager.fetchAgentsById(ownerId);

        if(LeadsquaredUserList ==null || LeadsquaredUserList.isEmpty()) {
            logger.error("Owner details not found with the given id::{}", ownerId);
            return;
        }

        LeadsquaredUser  leadsquaredUser = LeadsquaredUserList.iterator().next();
        logger.info("Owner details :: {}", leadsquaredUser);

        String ownerAssociatedPhoneNumbers = leadsquaredUser.getAssociatedPhoneNumbers();

        String phoneNumber = leadDetails.getMobile();

        if(StringUtils.isEmpty(phoneNumber))
            phoneNumber = leadDetails.getPhone();

        Random rnd = new Random();
        int number = rnd.nextInt(999999);
        // this will convert any number sequence into 6 character.
        String OTP =  String.format("%06d", number);

        //30 minutes
        Long OTPExpiryTime = System.currentTimeMillis() + (60000*30);

        demoOTPValidationManager.save(prospectActivityId,leadEmail, phoneNumber,OTP,OTPExpiryTime);

        Map<String, Object> scopeParams = new HashMap<String, Object>();
        scopeParams.put("code", OTP);
        TextSMSRequest textSMSRequest = new TextSMSRequest(phoneNumber, "91", scopeParams, CommunicationType.PHONE_VERIFICATION, Role.STUDENT);

        communicationManager.sendSMSViaRest(textSMSRequest);

        if(StringUtils.isEmpty(ownerAssociatedPhoneNumbers)){
            logger.warn("Agent phone number missing for the agent :  {}", leadsquaredUser.getEmail());
            return;
        }

        DemoTriggerCallToAgentReq demoTriggerCallToAgentReq = new DemoTriggerCallToAgentReq();
        demoTriggerCallToAgentReq.setFromNumber(ownerAssociatedPhoneNumbers);

        String voiceMessageNumber = ConfigUtils.INSTANCE.getStringValue("exotel.voice.mail.number");
        demoTriggerCallToAgentReq.setToNumber(voiceMessageNumber);

        demoTriggerCallToAgentReq.setContextId(prospectActivityId);
        communicationManager.callAgent(demoTriggerCallToAgentReq);

    }

}