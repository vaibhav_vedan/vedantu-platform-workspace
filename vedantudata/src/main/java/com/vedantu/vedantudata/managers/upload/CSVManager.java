package com.vedantu.vedantudata.managers.upload;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.List;

import javax.annotation.PostConstruct;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.univocity.parsers.csv.CsvParser;
import com.univocity.parsers.csv.CsvParserSettings;
import com.vedantu.util.LogFactory;

/*
 * Using univocity parser based on some recommendations 
 * http://www.univocity.com/pages/parsers-tutorial
 */
@Service
public class CSVManager {

	@Autowired
	private LogFactory logFactory;

	@SuppressWarnings("static-access")
	private Logger logger = logFactory.getLogger(CSVManager.class);

	private static CsvParserSettings csvParserSettings;

	@PostConstruct
	public void init() {
		csvParserSettings = new CsvParserSettings();
		csvParserSettings.getFormat().setLineSeparator("\n");
	}

	public List<String[]> parseCSVFile(File file) throws FileNotFoundException {

		// creates a CSV parser
		CsvParser parser = new CsvParser(csvParserSettings);

		// parses all rows in one go.
		List<String[]> allRows = parser.parseAll(new FileInputStream(file));
		logger.info("Count(Including header): " + allRows.size());
		return allRows;
	}

}
