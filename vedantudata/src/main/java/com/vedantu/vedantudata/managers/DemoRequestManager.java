package com.vedantu.vedantudata.managers;

import com.google.gson.Gson;
import com.vedantu.User.StudentInfo;
import com.vedantu.User.User;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.ForbiddenException;
import com.vedantu.util.FosUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.enums.SQSMessageType;
import com.vedantu.util.enums.SQSQueue;
import com.vedantu.util.security.HttpSessionUtils;
import com.vedantu.vedantudata.dao.DemoRequestDao;
import com.vedantu.vedantudata.enums.DemoRequestType;
import com.vedantu.vedantudata.pojos.DemoRequestPojo;
import com.vedantu.vedantudata.pojos.LeadActivityReq;
import com.vedantu.vedantudata.pojos.SchemaValuePair;
import com.vedantu.vedantudata.request.DemoRequest;
import com.vedantu.vedantudata.utils.AwsSQSManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Service
public class DemoRequestManager {

    @Autowired
    private HttpSessionUtils httpSessionUtils;

    @Autowired
    private DemoRequestDao demoRequestDao;

    @Autowired
    private LogFactory logFactory;

    @Autowired
    private AwsSQSManager awsSQSManager;

    @Autowired
    private FosUtils fosUtils;

    private final Logger logger = LogFactory.getLogger(DemoRequestManager.class);

    private final Gson gson = new Gson();

    public void registerDemo(DemoRequestPojo demoRequestPojo) throws ForbiddenException, BadRequestException {

        logger.info("registerDemo : {}", demoRequestPojo );

        Long userId = demoRequestPojo.getUserId();

        if(userId == null || demoRequestPojo.getDemoRequestType() == null)
            throw new BadRequestException(ErrorCode.INCORRECT_ID,"User id and type cannot be empty");

        if(httpSessionUtils.getCurrentSessionData() == null || !httpSessionUtils.getCallingUserId().equals(userId))
            throw new ForbiddenException(ErrorCode.FORBIDDEN_ERROR,"Logged in user and requesting user id is mismatching :" + userId);

        DemoRequest existingDemoRequest = getRegisteredDemo(userId, demoRequestPojo.getDemoRequestType());

        if(existingDemoRequest != null){
            throw new ForbiddenException(ErrorCode.ALREADY_REGISTERED,"You are already registered for demo");
        }

        DemoRequest demoRequest = new DemoRequest();
        demoRequest.setUserId(userId);
        demoRequest.setDemoRequestType(DemoRequestType.IIT_JEE);

        demoRequestDao.registerDemo(demoRequest);

        logger.info("IIT_JEE demo registered");

        logger.info("IIT_JEE demo triggering activity ");
        pushActivity(demoRequest);

    }

    private void pushActivity(DemoRequest demoRequest) {

        ExecutorService cachedThreadPool = Executors.newSingleThreadExecutor();
        try {
            cachedThreadPool.submit(
                    () -> {
                        try {
                            User userBasicInfo = fosUtils.getUserInfo(demoRequest.getUserId(), true);

                            LeadActivityReq leadActivityReq = new LeadActivityReq();
                            leadActivityReq.setActivityNote("User requested for demo Jee Neet Land, userId : " + demoRequest.getUserId());

                            if(StringUtils.isNotEmpty(userBasicInfo.getEmail()))
                                leadActivityReq.setEmailAddress(userBasicInfo.getEmail());

                            leadActivityReq.setPhone(userBasicInfo.getContactNumber());

                            StudentInfo studentInfo = userBasicInfo.getStudentInfo();
                            if(studentInfo != null && StringUtils.isNotEmpty(studentInfo.getTarget())) {
                                SchemaValuePair schemaValuePair = new SchemaValuePair();
                                schemaValuePair.setSchemaName("mx_Custom_1");

                                String target = StringUtils.isNotEmpty(studentInfo.getTarget()) ? studentInfo.getTarget() : "";
                                schemaValuePair.setValue(target);

                                leadActivityReq.setFields(Arrays.asList(schemaValuePair));
                            }

                            awsSQSManager.sendToSQS(SQSQueue.LEADSQUARED_QUEUE, SQSMessageType.DEMO_REQUESTED, gson.toJson(leadActivityReq), String.valueOf(demoRequest.getUserId()) + DemoRequestType.IIT_JEE);
                        }catch (Exception e){
                            logger.error("Error pushing demo message to queue", e);
                        }
                    }
            );
        } finally {
            cachedThreadPool.shutdown();
        }

    }

    public DemoRequest getRegisteredDemo(Long userId, DemoRequestType demoRequestType) throws ForbiddenException {

        DemoRequest demoRequest = demoRequestDao.getRegisteredDemo(userId,demoRequestType);

        return demoRequest;

    }
}
