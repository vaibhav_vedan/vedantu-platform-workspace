package com.vedantu.vedantudata.managers.leadsquared;

import com.amazonaws.services.workspaces.model.ConnectionState;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.User.User;
import com.vedantu.User.request.LeadSquaredRequest;
import com.vedantu.exception.*;
import com.vedantu.leadsquared.LeadsquaredHelper;
import com.vedantu.util.WebUtils;
import com.vedantu.util.*;
import com.vedantu.util.enums.SQSMessageType;
import com.vedantu.util.enums.SQSQueue;
import com.vedantu.util.logger.LoggingMarkers;
import com.vedantu.vedantudata.dao.SalesWaveSessionJoinedLSRequestDao;
import com.vedantu.vedantudata.dao.serializers.LeadsquaredDAO;
import com.vedantu.vedantudata.entities.*;
import com.vedantu.vedantudata.entities.leadsquared.LeadActivity;
import com.vedantu.vedantudata.entities.leadsquared.LeadDetails;
import com.vedantu.vedantudata.entities.leadsquared.LeadsquaredUser;
import com.vedantu.vedantudata.enums.SessionConnectionState;
import com.vedantu.vedantudata.enums.SessionRole;
import com.vedantu.vedantudata.enums.UploadFileStatus;
import com.vedantu.vedantudata.enums.UploadFileType;
import com.vedantu.vedantudata.managers.AwsS3Manager;
import com.vedantu.vedantudata.managers.UserManager;
import com.vedantu.vedantudata.managers.upload.CSVManager;
import com.vedantu.vedantudata.managers.upload.UploadIdentifierManager;
import com.vedantu.vedantudata.pojos.GetLeadActivityResp;
import com.vedantu.vedantudata.pojos.KeyValueObject;
import com.vedantu.vedantudata.pojos.LeadActivitySerialized;
import com.vedantu.vedantudata.pojos.ValueHolder;
import com.vedantu.vedantudata.pojos.request.OnlineSalesDemoSessionJoinedLSRequest;
import com.vedantu.vedantudata.request.LeadVcCodeReq;
import com.vedantu.vedantudata.request.leadsquared.GetLeadsByTimeRangeReq;
import com.vedantu.vedantudata.request.leadsquared.PostLeadActivitiyReq;
import com.vedantu.vedantudata.request.leadsquared.PushLeadsquaredReq;
import com.vedantu.vedantudata.request.leadsquared.SalesWaveSessionJoinedLSRequest;
import com.vedantu.vedantudata.utils.CommonUtils;
import com.vedantu.vedantudata.utils.*;
import org.apache.commons.io.IOUtils;
import org.apache.logging.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import org.jsoup.select.Collector;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Type;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.concurrent.*;
import java.util.stream.Collectors;

import static org.apache.commons.text.StringEscapeUtils.escapeCsv;

@Service
public class LeadsquaredManager extends LeadsquaredConfig {

    @Autowired
    private LeadsquaredActivityManager leadsquaredActivityManager;

    @Autowired
    private UserManager userManager;

    @Autowired
    private UploadIdentifierManager uploadIdentifierManager;

    @Autowired
    private CSVManager csvManager;

    @Autowired
    private LeadsquaredDAO leadsquaredDAO;

    @Autowired
    private LogFactory logFactory;

    @Autowired
    private RedisDAO redisDAO;

    @Autowired
    private PostgressHandler postgressHandler;

    @Autowired
    public AwsSQSManager awsSQSManager;

    @Autowired
    private AwsS3Manager awsS3Manager;

    @Autowired
    private FosUtils fosUtils;

    @Autowired
    private LSWebUtils lsWebUtils;

    @Autowired
    private SalesWaveSessionJoinedLSRequestDao salesWaveSessionJoinedLSRequestDao;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(LeadsquaredManager.class);

    private static Gson gson = new Gson();

    private static final Type leadDetailsListType = new TypeToken<List<LeadDetails>>() {
    }.getType();

    private static final Type userDetailsListType = new TypeToken<List<LeadsquaredUser>>() {
    }.getType();

    private static List<String> PUSH_LS_EXCLUDED_FIELDS = new ArrayList<>(
            Arrays.asList("ContextField", "ProspectStage", "LastVisitDate", "mx_Last_payment_time", "mx_User_Id",
                    "ExistingOwner", "TotalCalls", "LastCallDate", "ProspectID", "AllLeadIds"));
    private static final String USER_ENDPOINT = ConfigUtils.INSTANCE.getStringValue("USER_ENDPOINT");
    private final int nThreads = 2;
    private static final String S3_BUCKET = ConfigUtils.INSTANCE.getStringValue("aws.s3.bucket");


    private static final String DRY_RUN_LS_API_COUNT_REDIS_KEY = "DRY_RUN_LS_API_COUNT";
    private static final String DRY_RUN_LS_API_COUNT_REDIS_KEY_TIME_TRACKER = "DRY_RUN_LS_API_COUNT_REDIS_KEY_TIME_TRACKER";
    private static final Integer DRY_RUN_LS_API_MAX_COUNT = 50;
    private static final Integer DRY_RUN_LS_API_MID_NIGHT_MAX_COUNT = 125;
    private static final Integer DRY_RUN_LS_API_QUOTA_THRESHOLD_TIME = 5;
    private Integer dryrunEligibleQuota;

    @PostConstruct
    public void init() {
        accessKey = ConfigUtils.INSTANCE.getStringValue("leadsquared.access.id");
        secretKey = ConfigUtils.INSTANCE.getStringValue("leadsquared.secret.key");
        try {
            new File("lead").mkdir();
        } catch (Exception ex) {

        }
    }


    public String uploadStatus(File file) throws NoSuchAlgorithmException, IOException, BadRequestException, InternalServerErrorException {
        String checksum = FileUtils.getFileChecksum(MessageDigest.getInstance("SHA-1"), file);
        String key = "PROCESS_" + checksum.toUpperCase();
        logger.info("STATUS KEY: " + key);
        String s = redisDAO.get(key);
        return s == null ? "File key not present. Is file contents changed after upload?" : s + "% completed";
    }

    public List<LeadDetails> fetchLeadsByEmailId(String emailId) throws InterruptedException {
        List<LeadDetails> leads = new ArrayList<>();
        for (int i = 0; i < 3; i++) {
            String serverUrl = appendAccessKeys(getQuickSearchLeadsUrl) + "&key=" + emailId;
            ClientResponse cRes = WebUtils.INSTANCE.doCall(serverUrl, HttpMethod.GET, null);
            logger.info(LoggingMarkers.JSON_MASK,"cRes:" + cRes.toString());
            String response = cRes.getEntity(String.class);
            if (cRes.getStatus() != 200 && cRes.getStatus() != 429) {
                logger.error(LoggingMarkers.JSON_MASK,"InvalidStatusCodeGetUsers - get lead users not 200 - " + cRes.getStatus() + " cRes:"
                        + cRes.toString());
            } else if (cRes.getStatus() == 429) {
                logger.error(LoggingMarkers.JSON_MASK,"Too many requests for fetchLeadsByEmailId " + emailId + " , response: " + response);
                Thread.sleep(1000);
                continue;
            } else {
                leads = gson.fromJson(response, leadDetailsListType);
                return leads;
            }
        }
        return leads;
    }

    public List<LeadsquaredUser> fetchAgentsByEmailId(String emailId) throws InterruptedException, InternalServerErrorException {

        String key = getAgentDetailsByEmailIdKey(emailId);
        try {
            String resp = redisDAO.get(key);
            if (StringUtils.isNotEmpty(resp)) {
                logger.info("======found am  in cache");
                List<LeadsquaredUser> ags = gson.fromJson(resp, userDetailsListType);
                return ags;
            }
        } catch (Exception e) {
            //swallow
        }

        logger.info("======not found am  in cache, fetching from LS");

        List<LeadsquaredUser> agents = new ArrayList<>();
        for (int i = 0; i < 3; i++) {
            String serverUrl = appendAccessKeys(getUsersByEmailUrl) + "&emailaddress=" + emailId;
            ClientResponse cRes = WebUtils.INSTANCE.doCall(serverUrl, HttpMethod.GET, null);
            logger.info(LoggingMarkers.JSON_MASK,"cRes:" + cRes.toString());
            String response = cRes.getEntity(String.class);
            if (cRes.getStatus() != 200 && cRes.getStatus() != 429) {
                logger.error(LoggingMarkers.JSON_MASK, "InvalidStatusCodeGetUsers - get lead users not 200 - " + cRes.getStatus() + " cRes:"
                        + cRes.toString());
            } else if (cRes.getStatus() == 429) {
                logger.error(LoggingMarkers.JSON_MASK, "Too many requests for fetchAgentsByEmailId " + emailId + " , response: " + response);
                Thread.sleep(1000);
                continue;
            } else {
                try {
                    redisDAO.set(key, response);
                } catch (Exception e) {
                    //swallow
                }
                agents = gson.fromJson(response, userDetailsListType);
                return agents;
            }
        }
        return agents;
    }

    public List<LeadsquaredUser> fetchAgentsById(String ownerId) throws InterruptedException, InternalServerErrorException {

        String key = getAgentDetailsByIdKey(ownerId);
        try {
            String resp = redisDAO.get(key);
            if (StringUtils.isNotEmpty(resp)) {
                logger.info("======found am  in cache");
                List<LeadsquaredUser> ags = gson.fromJson(resp, userDetailsListType);
                return ags;
            }
        } catch (Exception e) {
            //swallow
        }

        logger.info("======not found am  in cache, fetching from LS");

        List<LeadsquaredUser> agents = new ArrayList<>();
        for (int i = 0; i < 3; i++) {
            String serverUrl = appendAccessKeys(getUsersByIdUrl) + "&userId=" + ownerId;
            ClientResponse cRes = WebUtils.INSTANCE.doCall(serverUrl, HttpMethod.GET, null);
            logger.info(LoggingMarkers.JSON_MASK,"cRes:" + cRes.toString());
            String response = cRes.getEntity(String.class);
            if (cRes.getStatus() != 200 && cRes.getStatus() != 429) {
                logger.error(LoggingMarkers.JSON_MASK, "InvalidStatusCodeGetUsers - get lead users not 200 - " + cRes.getStatus() + " cRes:"
                        + cRes.toString());
            } else if (cRes.getStatus() == 429) {
                logger.error(LoggingMarkers.JSON_MASK, "Too many requests for fetchAgentsById " + ownerId + " , response: " + response);
                Thread.sleep(1000);
                continue;
            } else {
                try {
                    redisDAO.set(key, response);
                } catch (Exception e) {
                    //swallow
                }
                agents = gson.fromJson(response, userDetailsListType);
                return agents;
            }
        }
        return agents;
    }

    public String getAgentDetailsByEmailIdKey(String email) {
        return "LS_AGENT_EMAIL_" + email;
    }

    public String getAgentDetailsByIdKey(String id) {
        return "LS_AGENT_ID_" + id;
    }

    public LeadDetails fetchLeadsByVedantuUserId(String emailId, String userId) throws InterruptedException {
        List<LeadDetails> leads = fetchLeadsByEmailId(emailId);
        if (!CollectionUtils.isEmpty(leads)) {
            for (LeadDetails leadDetails : leads) {
                if (userId.equals(leadDetails.getMx_User_Id())) {
                    return leadDetails;
                }
            }
            return leads.get(0);
        }
        return null;
    }

    public List<LeadDetails> fetchLeadsByPhone(String phoneNumber) throws InterruptedException {
        List<LeadDetails> leads = new ArrayList<>();
        for (int i = 0; i < 3; i++) {
            String serverUrl = appendAccessKeys(getQuickSearchLeadsUrl) + "&key=" + phoneNumber;
            ClientResponse cRes = WebUtils.INSTANCE.doCall(serverUrl, HttpMethod.GET, null);
            logger.info(LoggingMarkers.JSON_MASK,"cRes:" + cRes.toString());
            String response = cRes.getEntity(String.class);
            if (cRes.getStatus() != 200 && cRes.getStatus() != 429) {
                logger.error(LoggingMarkers.JSON_MASK, "InvalidStatusCodeGetUsers - get lead users not 200 - " + cRes.getStatus() + " cRes:"
                        + cRes.toString());
            } else if (cRes.getStatus() == 429) {
                logger.warn(LoggingMarkers.JSON_MASK, "Too many requests for fetchLeadsByPhone " + phoneNumber + " , response: " + response);
                Thread.sleep(1000);
                continue;
            } else {
                leads = gson.fromJson(response, leadDetailsListType);
                return leads;
            }
        }
        return leads;
    }

    public List<LeadDetails> fetchLeadsByPhoneOrEmail(String value) throws InterruptedException {
        List<LeadDetails> leads = new ArrayList<>();
        for (int i = 0; i < 3; i++) {
            String serverUrl = appendAccessKeys(getQuickSearchLeadsUrl) + "&key=" + value;
            ClientResponse cRes = WebUtils.INSTANCE.doCall(serverUrl, HttpMethod.GET, null);
            logger.info(LoggingMarkers.JSON_MASK,"cRes:" + cRes.toString());
            String response = cRes.getEntity(String.class);
            if (cRes.getStatus() != 200 && cRes.getStatus() != 429) {
                logger.warn(LoggingMarkers.JSON_MASK, "InvalidStatusCodeGetUsers - get lead users not 200 - " + cRes.getStatus() + " cRes:"
                        + cRes.toString());
                return leads;
            } else if (cRes.getStatus() == 429) {
                logger.warn(LoggingMarkers.JSON_MASK, "Too many requests for fetchLeadsByPhoneOrEmail " + value + " , response: " + response);
                Thread.sleep(1000);
                continue;
            } else {
                leads = gson.fromJson(response, leadDetailsListType);
                return leads;
            }
        }
        return leads;
    }

    public List<LeadDetails> fetchLeadsByLeadId(String leadId) {
        String serverUrl = appendAccessKeys(getLeadByLeadIdUrl) + "&id=" + leadId;
        ClientResponse cRes = WebUtils.INSTANCE.doCall(serverUrl, HttpMethod.GET, null);
        logger.info(LoggingMarkers.JSON_MASK,"cRes:" + cRes.toString());
        List<LeadDetails> leads = new ArrayList<>();
        if (cRes.getStatus() != 200) {
            logger.error(LoggingMarkers.JSON_MASK, "InvalidStatusCodeGetUsers - get lead users not 200 - " + cRes.getStatus() + " cRes:"
                    + cRes.toString());
            return leads;
        }

        String response = cRes.getEntity(String.class);
        leads = gson.fromJson(response, leadDetailsListType);
        return leads;
    }

    public void fetchAllLeads(long startTime, long endTime) {
        long currentTime = startTime;
        long currentDayStartTime = currentTime % DateTimeUtils.MILLIS_PER_DAY == 0 ? currentTime
                : (currentTime - DateTimeUtils.MILLIS_PER_DAY);
        logger.info("currentDayTime :" + currentDayStartTime + " currentTime:" + currentTime);
        if (endTime <= 0) {
            endTime = currentDayStartTime - (Long.valueOf(DateTimeUtils.MILLIS_PER_DAY) * 365 * 2);
        }
        for (long time = currentDayStartTime; time > endTime; time -= DateTimeUtils.MILLIS_PER_DAY) {
            try {
                fetchLeadsByDay(time);
            } catch (Exception ex) {
                logger.error("Error occured:" + ex.toString());
            }
        }
    }

    public List<LeadDetails> fetchLeadsInBulk(Collection<String> values, String lookupName) throws BadRequestException, InternalServerErrorException, InterruptedException {
        if (values == null || values.isEmpty() || StringUtils.isEmpty(lookupName)) {
            return new ArrayList<>();
        }

        String serverUrl = appendAccessKeys(getBulkLeads);
        List<LeadDetails> leads = new ArrayList<>();
        List<LeadDetails> allLeads = new ArrayList<>();
        int page = 1;
        do {
            JSONArray jsonarr = new JSONArray();
            for (String val : values) {
                jsonarr.put(val);
            }
            JSONObject parameterObj = new JSONObject();
            parameterObj.put("LookupName", lookupName);
            parameterObj.put("LookupValue", String.join(",", values));
            parameterObj.put("SqlOperator", "IN");

            JSONObject pagingObj = new JSONObject();
            pagingObj.put("PageIndex", page);
            pagingObj.put("PageSize", 200);
            page++;

            JSONObject queryObj = new JSONObject();
            queryObj.put("Parameter", parameterObj);
            queryObj.put("Paging", pagingObj);
            JSONObject columns = new JSONObject();
            columns.put("Include_CSV", "");
            queryObj.put("Columns", columns);
            for (int i = 0; i <= 5; i++) {

                checkLSDryRunQuota();

                ClientResponse cRes = WebUtils.INSTANCE.doCall(serverUrl, HttpMethod.POST, queryObj.toString(), false);
                logger.info(LoggingMarkers.JSON_MASK,"cRes:" + cRes.toString());
                String response = cRes.getEntity(String.class);
                if (cRes.getStatus() != 200 && cRes.getStatus() != 429) {
                    logger.error(LoggingMarkers.JSON_MASK, "InvalidStatusCodeGetUsers - get lead users not 200 - " + cRes.getStatus() + " cRes:"
                            + cRes.toString());
                } else if (cRes.getStatus() == 429) {
                    logger.error("Too many requests for fetchLeadsInBulk  , response: " + response);
                    try {
                        TimeUnit.MILLISECONDS.sleep(1500);
                    } catch (InterruptedException e) {
                        logger.error(e.getMessage(), e);
                    }
                } else {
                    leads = gson.fromJson(response, leadDetailsListType);
                    for (LeadDetails lead : leads) {
                        lead.success = true;
                    }
                    allLeads.addAll(leads);
                }
            }

        } while (leads.size() == 200);
        return allLeads.size() == 0 ? createEmptyLeadDetailsList(lookupName, values) : allLeads;
    }

    private void checkLSDryRunQuota() throws BadRequestException, InternalServerErrorException, InterruptedException {

        int waitCount = 1;
        while(true){

            String count = "0";

            String timeTracker = redisDAO.get(DRY_RUN_LS_API_COUNT_REDIS_KEY_TIME_TRACKER);
            if(StringUtils.isEmpty(timeTracker)){
                redisDAO.del(DRY_RUN_LS_API_COUNT_REDIS_KEY);
                redisDAO.setex(DRY_RUN_LS_API_COUNT_REDIS_KEY_TIME_TRACKER, "TRUE", DRY_RUN_LS_API_QUOTA_THRESHOLD_TIME);
            }else {
                count = redisDAO.get(DRY_RUN_LS_API_COUNT_REDIS_KEY);
            }

            if(StringUtils.isEmpty(count)) {
                count = "0";
            }
            Integer countInt = Integer.parseInt(count);

            getEligibleQuota();

            logger.info("Dryrun eligible quota at the moment : {}", dryrunEligibleQuota);

            if( dryrunEligibleQuota!= null && countInt < dryrunEligibleQuota) {
                redisDAO.setex(DRY_RUN_LS_API_COUNT_REDIS_KEY, String.valueOf(countInt+1) , DRY_RUN_LS_API_QUOTA_THRESHOLD_TIME);
                break;
            }

            Thread.sleep(1000);

            if(waitCount % 30 ==0){
                logger.error("Dryrun waiting from {} seconds", waitCount);
            }

            waitCount++;
        }
    }

    private void getEligibleQuota() throws BadRequestException, InternalServerErrorException {

        if(dryrunEligibleQuota != null){
            return ;
        }

        Date date = new Date();
        TimeZone asiaKolkata = TimeZone.getTimeZone("Asia/Kolkata");

        Calendar startTime = Calendar.getInstance(asiaKolkata);
        startTime.setTime(date);
        startTime.set(Calendar.HOUR_OF_DAY, 10);
        startTime.set(Calendar.MINUTE, 00);
        startTime.set(Calendar.SECOND, 00);

        Calendar endTime = Calendar.getInstance(asiaKolkata);
        endTime.setTime(date);
        endTime.set(Calendar.HOUR_OF_DAY, 21);
        endTime.set(Calendar.MINUTE, 00);
        endTime.set(Calendar.SECOND, 00);

        if(date.after(startTime.getTime()) && date.before(endTime.getTime())){
            dryrunEligibleQuota = DRY_RUN_LS_API_MAX_COUNT;
        }else {
            dryrunEligibleQuota = DRY_RUN_LS_API_MID_NIGHT_MAX_COUNT;
        }

        logger.info("Start time : {}", startTime.getTime());
        logger.info("EndTIme time : {}", endTime.getTime());

    }

    private List<LeadDetails> createEmptyLeadDetailsList(String lookupName, Collection<String> values) {
        List<LeadDetails> leadDetails = new ArrayList<>();

        for (String value : values) {
            LeadDetails leadDetail = new LeadDetails();
            switch (lookupName) {
                case "EmailAddress":
                    leadDetail.setEmailAddress(value);
                    break;
                case "Phone":
                    leadDetail.setPhone(value);
                    break;
                case "Mobile":
                    leadDetail.setMobile(value);
                    break;
                default:
                    throw new RuntimeException("Unknown lookupName field");
            }
            leadDetails.add(leadDetail);
        }
        return leadDetails;
    }

    public List<String> fetchLeadsByDay(long startTime) throws IOException {
        long startDate = startTime - (startTime % DateTimeUtils.MILLIS_PER_DAY);
        long duration = DateTimeUtils.MILLIS_PER_HOUR;
        logger.info("Request:" + startDate + " duration:" + duration);
        List<String> leadsFetched = new ArrayList<>();
        for (long time = startDate; time < (startDate + DateTimeUtils.MILLIS_PER_DAY); time += duration) {
            try {
                List<LeadDetails> results = fetchLeadsByTimeRange(time, time + duration);
                long fetchedCount = results.size();
                leadsFetched.add(String.valueOf(fetchedCount));
            } catch (Exception ex) {
                logger.error("Error:" + time + " ex:" + ex.toString());
                leadsFetched.add("time:" + time);
            }
        }

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        sdf.setTimeZone(TimeZone.getTimeZone(DateTimeUtils.TIME_ZONE_GMT));
        String startDateStr = sdf.format(new Date(startDate));
        File file = new File("lead", "Lead_" + startDateStr + "_" + System.currentTimeMillis());
        IOUtils.write(Arrays.toString(leadsFetched.toArray()), new FileOutputStream(file));
        return leadsFetched;
    }

    public List<LeadDetails> fetchLeadsByTimeRange(long startTime, long endTime) throws Exception {
        List<LeadDetails> responseLeads = new ArrayList<LeadDetails>();
        if (startTime > System.currentTimeMillis()) {
            return responseLeads;
        }

        if (endTime > System.currentTimeMillis()) {
            endTime = System.currentTimeMillis();
        }

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        sdf.setTimeZone(TimeZone.getTimeZone(DateTimeUtils.TIME_ZONE_GMT));
        String startDate = sdf.format(new Date(startTime));
        String endDate = sdf.format(new Date(endTime));

        int page = 1;
        int recordCount = 0;
        while (true) {
            GetLeadsByTimeRangeReq req = new GetLeadsByTimeRangeReq(startDate, endDate, page, 1000);
            String serverUrl = appendAccessKeys(getLeadByTimeIntervalUrl);
            ClientResponse cRes = WebUtils.INSTANCE.doCall(serverUrl, HttpMethod.POST, gson.toJson(req));
            logger.info("cRes:" + cRes.getStatus());
            String response = cRes.getEntity(String.class);
            if (cRes.getStatus() != 200 && cRes.getStatus() != 429) {
                logger.error("InvalidStatusCode getLeadByTimeIntervalUrl - not 200 - "
                        + cRes.getStatus() + " cRes:" + cRes.toString());
                throw new InternalServerErrorException(ErrorCode.SERVICE_ERROR, "InvalidStatusCode getLeadByTimeIntervalUrl - not 200 - "
                        + cRes.getStatus() + " cRes:" + cRes.toString());
            } else if (cRes.getStatus() == 429) {
                logger.error("Too many requests for fetchLeadsByTimeRange  , response: " + response);
                Thread.sleep(5000);
                continue;
            }

            JSONObject jsonObject = new JSONObject(response);
            int records = jsonObject.getInt("RecordCount");
            logger.info("Records:" + records);
            if (jsonObject.isNull("Leads")) {
                break;
            }

            JSONArray leads = jsonObject.getJSONArray("Leads");
            if (leads.length() <= 0) {
                break;
            }

            logger.info("lead size:" + leads.length());
            List<LeadDetails> results = new ArrayList<>();
            for (int i = 0; i < leads.length(); i++) {
                JSONObject leadObject = leads.getJSONObject(i);// leads.get("LeadPropertyList");
                JSONArray leadProperties = leadObject.getJSONArray("LeadPropertyList");

                if (leadProperties != null && leadProperties.length() > 0) {
                    JSONObject jsObject = new JSONObject();
                    for (int j = 0; j < leadProperties.length(); j++) {
                        JSONObject leadProperty = leadProperties.getJSONObject(j);
                        String attributeName = leadProperty.getString("Attribute");
                        if (leadProperty.isNull("Value")) {
                            continue;
                        }

                        String value = leadProperty.getString("Value");
                        if (!StringUtils.isEmpty(value)) {
                            jsObject.put(attributeName, value);
                        }
                    }

                    String jsonString = jsObject.toString();
                    try {
                        LeadDetails leadDetails = gson.fromJson(jsonString, LeadDetails.class);
                        leadDetails.setLeadId(leadDetails.getProspectID());
                        results.add(leadDetails);
                    } catch (Exception ex) {
                        logger.error("leadDetails gson conversion failure");
                    }
                }
            }

            saveAllLeads(results);
            int fetchRecordCount = !CollectionUtils.isEmpty(results) ? results.size() : 0;
            if (fetchRecordCount > 0) {
                responseLeads.addAll(results);
            }
            recordCount += fetchRecordCount;
            page++;
        }

        logger.info("Records:" + recordCount);
        return responseLeads;
    }

    public String leadsDryRun(File file) throws Exception {

        dryrunEligibleQuota = null;

        String s3Key = ConfigUtils.INSTANCE.getEnvironmentSlug() + "/temp/dryrun/" +
                LocalDate.now().format(DateTimeFormatter.BASIC_ISO_DATE) + "/" + UUID.randomUUID().toString() + ".csv";
        ExecutorService service = Executors.newFixedThreadPool(nThreads);
        try {
            List<String[]> rows = csvManager.parseCSVFile(file);
            if (CollectionUtils.isEmpty(rows) || rows.size() <= 1) {
                logger.info("No lead fields found");
                throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, "No leads found to push");
            }

            String[] fields = rows.get(0);

            // Validate the fields
            validateFieldsNames(fields);

            int emailFieldIndex = getFieldIndex(fields, "EmailAddress");
            int phoneFieldIndex = getFieldIndex(fields, "Phone");
            if ((emailFieldIndex < 0) && (phoneFieldIndex < 0)) {
                throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "EmailAddress or Phone should be a column");
            }

            String checksum = FileUtils.getFileChecksum(MessageDigest.getInstance("SHA-1"), file);
            String key = "PROCESS_" + checksum.toUpperCase();
            logger.info("DRY RUN REDIS KEY :" + key);

            String existingProcess = redisDAO.get(key);

            if(StringUtils.isNotEmpty(existingProcess)){
                if(!existingProcess.toLowerCase().contains("error")){
                    Double percentage = Double.parseDouble(existingProcess);

                    if(percentage < 100d){
                        throw new BadRequestException(ErrorCode.REQUEST_ALREADY_PROCESSING, "Request for this file is already processing, please wait until completes");
                    }
                }
            }

            redisDAO.setex(key, "0", 10000);

            List<String> emailAddressToFetchBy = new ArrayList<>();
            List<String> phoneNumebersToFetchBy = new ArrayList<>();
            int bulkFetchLength = 50;

            Queue<Future<ValueHolder>> futures = new PriorityBlockingQueue<>(32, (v1, v2) -> {
                if (v2.isDone() && !v1.isDone()) {
                    return 1;
                }
                if (v1.isDone() && !v2.isDone()) {
                    return -1;
                }
                return 0;
            });

            for (int i = 1; i < rows.size(); i++) {
                String[] row = rows.get(i);
                if (!validRow(row)) {
                    continue;
                }
                if (emailFieldIndex >= 0 && StringUtils.isNotEmpty(row[emailFieldIndex])) {
                    emailAddressToFetchBy.add(row[emailFieldIndex].toLowerCase().trim());
                }
                if (phoneFieldIndex >= 0 && StringUtils.isNotEmpty(row[phoneFieldIndex])) {
                    phoneNumebersToFetchBy.add(row[phoneFieldIndex].trim());
                }
                if (i % bulkFetchLength == 0) {
                    logger.info("i " + i + " now fetching leads");

                    ArrayList<String> emailList = new ArrayList<>(emailAddressToFetchBy);
                    ArrayList<String> phoneList = new ArrayList<>(phoneNumebersToFetchBy);
                    Future<ValueHolder> future = service.submit(() -> populateLeadsInMap(emailList, phoneList));
                    futures.add(future);

                    emailAddressToFetchBy.clear();
                    phoneNumebersToFetchBy.clear();
                }
            }


            ArrayList<String> emailList = new ArrayList<>(emailAddressToFetchBy);
            ArrayList<String> phoneList = new ArrayList<>(phoneNumebersToFetchBy);
            futures.add(service.submit(() -> populateLeadsInMap(emailList, phoneList)));

            ExecutorService cachedThreadPool = Executors.newSingleThreadExecutor();
            try {
                cachedThreadPool.submit(
                        () -> {
                            try {
                                writeCsv(futures, futures.size(), s3Key, fields[0], key);
                            } catch (Exception e) {
                                logger.error(e.getMessage(), e);
                            }
                        }
                );
            } finally {
                cachedThreadPool.shutdown();
            }

        } finally {
            service.shutdown();
        }

        return awsS3Manager.getPublicBucketDownloadUrl(S3_BUCKET, s3Key);
    }

    private void writeCsv(Queue<Future<ValueHolder>> futures, int size, String s3Key, String searchKey, String redisKey) throws IOException, ExecutionException, InterruptedException, InternalServerErrorException, BadRequestException {
        Path tempFile = Files.createTempFile("dry_run", ".csv");
        Files.write(tempFile, (searchKey + ",FetchMessage,mx_Phone,mx_Mobile,ProspectStage,LastVisitDate,mx_User_Id,ExistingOwner,ProspectID,mx_Vedantu_data_source," +
                "Grade,LastActivity,LastActivityDate,AllLeadIds,mx_City,mx_SAM_Allocation,ModifiedOn,mx_Sub_Stage,mx_User_Target,mx_Last_Assigned_Date," +
                "mx_VCLead,Lead Age,Assigned Count,mx_Core_Noncore,mx_GPS_City,mx_GPS_lat,mx_GPS_Long,mx_Location_type,mx_Nearby_City,mx_CC_LT," +
                "mx_TaskName,mx_TaskType,mx_TaskOwnerName,mx_TaskDueDate" + System.lineSeparator()).getBytes(), StandardOpenOption.CREATE);

        double count = 0;
        logger.info("TOTAL FUTURES: " + size);
        try {
            while (futures.size() > 0) {
                // Updating state changes
                Queue<Future<ValueHolder>> queue = new ArrayDeque<>();
                while (!futures.isEmpty()) {
                    queue.add(futures.poll());
                }
                while (!queue.isEmpty()) {
                    futures.add(queue.poll());
                }

                if (futures.peek().isDone()) {
                    Future<ValueHolder> future = futures.poll();
                    double percent = (++count / size) * 100;
                    logger.info("DRY RUN RUNNING FOR FUTURE " + count);
                    redisDAO.setex(redisKey, String.valueOf(percent), 3600);
                    ValueHolder holder = future.get();
                    Set<String> ids = new HashSet<>();
                    StringBuilder builder = new StringBuilder();

                    for (String key : holder.leadDetails.keySet()) {
                        List<LeadDetails> leadDetailsList = holder.leadDetails.get(key);
                        sortLeads(leadDetailsList);

                        do {

                            if (!CollectionUtils.isEmpty(leadDetailsList)) {
                                LeadDetails leadDetails = leadDetailsList.get(leadDetailsList.size() - 1);
                                leadDetailsList = leadDetailsList.subList(0, leadDetailsList.size() - 1);
                                logger.info(leadDetails);
                                String prospectID = leadDetails.getProspectID();
                                if (ids.contains(prospectID)) {
                                    logger.info("ALREADY EXISTING: " + leadDetails);
                                    continue;
                                }
                                if (StringUtils.isNotEmpty(prospectID)) {
                                    ids.add(prospectID);
                                }
                                builder.append(escapeCsv(key)).append(",");

                                if (leadDetails.success) {
                                    builder.append("Success").append(",");

                                    builder.append(escapeCsv(leadDetails.getPhone())).append(",");
                                    builder.append(escapeCsv(leadDetails.getMobile())).append(",");
                                    builder.append(escapeCsv(leadDetails.getProspectStage())).append(",");
                                    builder.append(escapeCsv(leadDetails.getLastVisitDate())).append(",");
                                    builder.append(escapeCsv(leadDetails.getMx_User_active())).append(",");
                                    builder.append(escapeCsv(leadDetails.getOwnerIdName())).append(",");
                                    builder.append(escapeCsv(leadDetails.getProspectID())).append(",");
                                    builder.append(escapeCsv(leadDetails.getMx_Vedantu_data_source())).append(",");
                                    builder.append(escapeCsv(leadDetails.getGrade())).append(",");
                                    builder.append(escapeCsv(leadDetails.getProspectActivityName_Max())).append(",");
                                    builder.append(escapeCsv(leadDetails.getMx_LastActivityDate())).append(",");
                                    builder.append(escapeCsv(Arrays.toString(extractAllLeadIds(leadDetailsList).toArray()))).append(",");
                                    builder.append(escapeCsv(leadDetails.getMx_City())).append(",");
                                    builder.append(escapeCsv(leadDetails.getMx_SAM_Allocation())).append(",");
                                    builder.append(escapeCsv(leadDetails.getModifiedOn())).append(",");
                                    builder.append(escapeCsv(leadDetails.getMx_Sub_Stage())).append(",");
                                    builder.append(escapeCsv(leadDetails.getMx_User_Target())).append(",");
                                    builder.append(escapeCsv(leadDetails.getMx_Last_Assigned_Date())).append(",");
                                    builder.append(escapeCsv(leadDetails.getMx_VCLead())).append(",");
                                    builder.append(escapeCsv(leadDetails.getLeadAge())).append(",");
                                    builder.append(escapeCsv(leadDetails.getMx_Assigned_Count())).append(",");
                                    builder.append(escapeCsv(leadDetails.getMx_Core_Noncore())).append(",");
                                    builder.append(escapeCsv(leadDetails.getMx_GPS_City())).append(",");
                                    builder.append(escapeCsv(leadDetails.getMx_GPS_lat())).append(",");
                                    builder.append(escapeCsv(leadDetails.getMx_GPS_Long())).append(",");
                                    builder.append(escapeCsv(leadDetails.getMx_Location_type())).append(",");
                                    builder.append(escapeCsv(leadDetails.getMx_Nearby_City())).append(",");
                                    builder.append(escapeCsv(leadDetails.getMx_CC_LT())).append(",");

                                    Map<String, String> tasks = holder.taskDetails.get(prospectID);
                                    if (tasks != null) {
                                        builder.append(escapeCsv(tasks.get("name"))).append(",");
                                        builder.append(escapeCsv(tasks.get("tasktypename"))).append(",");
                                        builder.append(escapeCsv(tasks.get("ownername"))).append(",");
                                        builder.append(escapeCsv(tasks.get("duedate"))).append(",");
                                    }
                                } else {
                                    builder.append("Fetch Failed. Retry again for this row");
                                }
                            }
                            builder.append(System.lineSeparator());

                        } while (ArrayUtils.isNotEmpty(leadDetailsList));

                    }
                    writeSync(tempFile, builder);
                }
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            redisDAO.setex(redisKey, "Error processing. 0", 3600);
        } finally {
            awsS3Manager.uploadFile(S3_BUCKET, s3Key, tempFile.toFile());
            tempFile.toFile().delete();
            if (redisDAO.get(redisKey) == null || !redisDAO.get(redisKey).contains("Error processing")) {
                redisDAO.setex(redisKey, "100", 3600);
            }
        }

    }

    private synchronized void writeSync(Path tempFile, StringBuilder builder) throws IOException {
        logger.info("IN SYNC WRITE");
        Files.write(tempFile, builder.toString().getBytes(), StandardOpenOption.APPEND);
        logger.info("OUT SYNC WRITE");
    }

    private void sortLeads(List<LeadDetails> leadDetailsList) {
        leadDetailsList.sort((v1, v2) -> {
            if (v1 == null) {
                return -1;
            }
            if (v2 == null) {
                return 1;
            }
            if (v1 == v2 || Objects.equals(v1.getLeadCreatedOn(), v2.getLeadCreatedOn())) {
                return 0;
            }
            try {
                if (!CommonUtils.compareTimeStamps(v1.getLeadCreatedOn(),
                        v2.getLeadCreatedOn())) {
                    return 1;
                }
            } catch (ParseException e) {
                logger.error(e.getMessage(), e);
            }
            return -1;
        });
    }

    private Map<String, Map<String, String>> getTaskDataForLeads(Collection<List<LeadDetails>> values) {
        List<String> leadIds = new ArrayList<>();
        for (List<LeadDetails> value : values) {
            LeadDetails leadDetails = getLatestLeadDetails(value);
            if (leadDetails != null) {
                leadIds.add(leadDetails.getProspectID());
            }
        }

        Map<String, Map<String, String>> data = new HashMap<>();
        Map<String, List<Map<String, String>>> listOfTasksMap = new HashMap<>();
        List<List<String>> partition = org.apache.commons.collections4.ListUtils.partition(leadIds, 300);
        for (List<String> list : partition) {
            String ids = list.stream().collect(Collectors.joining("','", "('", "')"));
            String query = "SELECT leadid,name,tasktypename,ownername,duedate FROM leadsquared.tasks where " +
                    "leadid IN " + ids + " and statuscode = 0";
            try {
                ResultSet resultSet = postgressHandler.executeQuery(query);
                while (resultSet.next()) {
                    String leadid = resultSet.getString("leadid");
                    Map<String, String> map = new HashMap<>();
                    map.put("name", resultSet.getString("name"));
                    map.put("tasktypename", resultSet.getString("tasktypename"));
                    map.put("ownername", resultSet.getString("ownername"));
                    map.put("duedate", resultSet.getString("duedate"));
                    map.put("leadid", leadid);
                    List<Map<String, String>> tasks = CollectionUtils.putIfAbsentAndGet(listOfTasksMap, leadid, ArrayList::new);
                    tasks.add(map);
                }
            } catch (SQLException e) {
                logger.error(e.getMessage(), e);
            }
        }
        for (Map.Entry<String, List<Map<String, String>>> entry : listOfTasksMap.entrySet()) {
            List<Map<String, String>> value = entry.getValue();
            Map<String, String> map = value.stream()
                    .reduce((v1, v2) -> {
                        String v1DueDate = v1.get("duedate");
                        String v2DueDate = v2.get("duedate");
                        if (v1DueDate == null) {
                            return v1;
                        }
                        if (v2DueDate == null) {
                            return v2;
                        }
                        int i = v1DueDate.compareTo(v2DueDate);
                        return i > 0 ? v1 : v2;
                    }).orElseGet(HashMap::new);
            data.put(entry.getKey(), map);
        }
        return data;
    }

    private ValueHolder populateLeadsInMap(List<String> emailAddressToFetchBy,
                                           List<String> phoneNumbersToFetchBy) throws BadRequestException, InternalServerErrorException, InterruptedException {
        ValueHolder holder = new ValueHolder();

        emailAddressToFetchBy.stream().filter(Objects::nonNull).forEach(e -> holder.leadDetails.putIfAbsent(e.toLowerCase().trim(), new ArrayList<>()));
        phoneNumbersToFetchBy.stream().filter(Objects::nonNull).forEach(e -> holder.leadDetails.putIfAbsent(e.toLowerCase().trim(), new ArrayList<>()));
        List<LeadDetails> emleads = fetchLeadsInBulk(emailAddressToFetchBy, "EmailAddress");
        List<LeadDetails> phleads = fetchLeadsInBulk(phoneNumbersToFetchBy, "Phone");
        if (ArrayUtils.isNotEmpty(emleads)) {
            for (LeadDetails ld : emleads) {
                if (StringUtils.isNotEmpty(ld.getEmailAddress())) {
                    ld.setEmailAddress(ld.getEmailAddress().toLowerCase().trim());
                    holder.leadDetails.putIfAbsent(ld.getEmailAddress().toLowerCase().trim(), new ArrayList<>());
                    holder.leadDetails.get(ld.getEmailAddress()).add(ld);
                }
            }
        }
        Set<String> phoneNumbersFetched = new HashSet<>();
        if (ArrayUtils.isNotEmpty(phleads)) {
            for (LeadDetails ld : phleads) {
                if (!StringUtils.isEmpty(ld.getEmailAddress())) {
                    ld.setEmailAddress(ld.getEmailAddress().toLowerCase());
                }

                holder.leadDetails.putIfAbsent(ld.getPhone(), new ArrayList<>());
                holder.leadDetails.get(ld.getPhone()).add(ld);
                if (ld.getPhone() != null) {
                    phoneNumbersFetched.add(ld.getPhone());
                }
            }
            Set<String> phoneNumberAll = new HashSet<>(phoneNumbersToFetchBy);
            if (phoneNumbersFetched.size() != phoneNumberAll.size()) {
                phoneNumberAll.removeAll(phoneNumbersFetched);
                if (ArrayUtils.isNotEmpty(phoneNumberAll)) {
                    List<LeadDetails> mobileleads = fetchLeadsInBulk(phoneNumberAll, "Mobile");
                    for (LeadDetails ld : mobileleads) {
                        if (!StringUtils.isEmpty(ld.getEmailAddress())) {
                            ld.setEmailAddress(ld.getEmailAddress().toLowerCase());
                        }
                        holder.leadDetails.putIfAbsent(ld.getMobile(), new ArrayList<>());
                        holder.leadDetails.get(ld.getMobile()).add(ld);
                    }
                }
            }
        }
        emailAddressToFetchBy.clear();
        phoneNumbersToFetchBy.clear();
        holder.taskDetails = getTaskDataForLeads(holder.leadDetails.values());
        return holder;
    }

    private LeadDetails getLatestLeadDetails(List<LeadDetails> leadDetailsList) {
        LeadDetails leadDetails = null;
        if (!CollectionUtils.isEmpty(leadDetailsList)) {
            for (LeadDetails lead : leadDetailsList) {
                try {
                    // leadsquaredDAO.createLead(lead);
                    // userManager.updateUserData(leadDetails);

                    if (leadDetails == null) {
                        leadDetails = lead;
                    } else if (!CommonUtils.compareTimeStamps(leadDetails.getLeadCreatedOn(),
                            lead.getLeadCreatedOn())) {
                        leadDetails = lead;
                    }
                } catch (Exception ex) {
                    logger.error("Error comparing lead details curLead:{}, prevLead: {}  ",
                            lead.toString(), leadDetails != null ? leadDetails.toString() : "null", ex);
                }
            }
        }

        return leadDetails;
    }

    private Set<String> extractAllLeadIds(List<LeadDetails> leadDetailsList) {
        Set<String> allLeadIds = new HashSet<>();
        if (!CollectionUtils.isEmpty(leadDetailsList)) {
            for (LeadDetails lead : leadDetailsList) {
                allLeadIds.add(lead.getProspectID());
            }
        }

        return allLeadIds;
    }
    //
    // public long getLeadCallActivityCount(String emailValue, String
    // phoneValue)
    // throws ParseException, InterruptedException {
    // List<LeadDetails> leadDetails = getLeadDetails(emailValue, phoneValue);
    // return getLeadCallActivityCount(leadDetails.get(0));
    // }

    public void mergeLeadFields(String parentId, String childId) throws Exception {
        // Fetch all leads with field1

    }

    public long fixPhoneNumberField(String leadIds) throws InterruptedException {
        long leadsUpdated = 0;
        if (StringUtils.isEmpty(leadIds)) {
            return leadsUpdated;
        }

        Set<String> leadIdArr = CommonUtils.getAllElementSet(leadIds, ",");
        leadIdArr = CommonUtils.trimAllElements(leadIdArr);
        for (String leadId : leadIdArr) {
            List<LeadDetails> results = fetchLeadsByLeadId(leadId);
            if (!CollectionUtils.isEmpty(results)) {
                LeadDetails entry = results.get(0);
                updateMergeLeadsPojo(entry);
                logger.info("Lead updated:" + entry.getProspectID());
                leadsUpdated++;
            }
        }

        return leadsUpdated;
    }

    public String getLeadContextField(String leadIds) throws InterruptedException {
        if (StringUtils.isEmpty(leadIds)) {
            return "";
        }

        Set<String> leadIdArr = CommonUtils.getAllElementSet(leadIds, ",");
        leadIdArr = CommonUtils.trimAllElements(leadIdArr);
        JSONArray jsonArray = new JSONArray();
        /*
         * { "Parameter": {"ActivityEvent": 201} "Paging": {"Offset": "0","RowCount":
         * "10"} }
         *
         */
        JSONObject body = new JSONObject();
        JSONObject activityEvent = new JSONObject();
        activityEvent.put("ActivityEvent", "244");
        body.put("Parameter", activityEvent);
        JSONObject paging = new JSONObject();
        paging.put("offSet", 0);
        paging.put("RowCount", 10);
        body.put("Paging", paging);
        for (String leadId : leadIdArr) {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("leadId", leadId);

            String contextField = "";
            for (int i = 0; i < 3; i++) {
                List<LeadActivity> activities = leadsquaredActivityManager.fetchLeadActivityByLeadId(leadId);
                if (activities != null) {
                    if (activities.size() > 1) {
                        contextField += "MultipleActivites";
                    }
                    for (LeadActivity leadActivity : activities) {
                        contextField += getNotableEventDescription(leadActivity.getData());
                    }

                    jsonObject.put("ContextField", contextField);
                    jsonArray.put(jsonObject);
                    i = 3;
                } else {
                    Thread.sleep(1000);
                    continue;
                }
            }
        }

        return jsonArray.toString();
    }

    private String getNotableEventDescription(List<KeyValueObject> data) {
        if (!CollectionUtils.isEmpty(data)) {
            for (KeyValueObject keyValueObject : data) {
                if ("NotableEventDescription".equals(keyValueObject.getKey())) {
                    return keyValueObject.getValue();
                }
            }
        }

        return "";
    }

    public void updateMergeLeadsPojo(LeadDetails leadDetails) throws InterruptedException {
        JSONArray jsonArray = new JSONArray();

        // Grade mx_StudentGrade mx_Class mx_Student_Class
        if (StringUtils.isEmpty(leadDetails.getGrade())) {
            if (!StringUtils.isEmpty(leadDetails.getMx_StudentGrade())) {
                jsonArray.put(genereateAttributeValueJSONObject("Grade", leadDetails.getMx_StudentGrade()));
            } else if (!StringUtils.isEmpty(leadDetails.getMx_Student_Class())) {
                jsonArray.put(genereateAttributeValueJSONObject("Grade", leadDetails.getMx_Student_Class()));
            } else if (!StringUtils.isEmpty(leadDetails.getMx_Class())) {
                jsonArray.put(genereateAttributeValueJSONObject("Grade", leadDetails.getMx_Class()));
            }
        }

        // School name
        if (StringUtils.isEmpty(leadDetails.getMx_School())) {
            if (!StringUtils.isEmpty(leadDetails.getMx_Student_School_Name())) {
                jsonArray.put(genereateAttributeValueJSONObject("mx_School", leadDetails.getMx_Student_School_Name()));
            }
        }

        // Phone numbers
        String phone = "";
        if (StringUtils.isEmpty(leadDetails.getPhone())) {
            if (!StringUtils.isEmpty(leadDetails.getMobile())) {
                jsonArray.put(genereateAttributeValueJSONObject("Phone", leadDetails.getMobile()));
                phone = leadDetails.getMobile();
            } else if (!StringUtils.isEmpty(leadDetails.getMx_Mobile())) {
                jsonArray.put(genereateAttributeValueJSONObject("Phone", leadDetails.getMx_Mobile()));
                phone = leadDetails.getMx_Mobile();
            } else if (!StringUtils.isEmpty(leadDetails.getMx_activeNumber())) {
                jsonArray.put(genereateAttributeValueJSONObject("Phone", leadDetails.getMx_activeNumber()));
                phone = leadDetails.getMx_activeNumber();
            } else if (!StringUtils.isEmpty(leadDetails.getMx_ParentContactNumber())) {
                jsonArray.put(genereateAttributeValueJSONObject("Phone", leadDetails.getMx_ParentContactNumber()));
                phone = leadDetails.getMx_ParentContactNumber();
            } else if (!StringUtils.isEmpty(leadDetails.getMx_Parent_Contact_Number())) {
                jsonArray.put(genereateAttributeValueJSONObject("Phone", leadDetails.getMx_Parent_Contact_Number()));
                phone = leadDetails.getMx_Parent_Contact_Number();
            } else if (!StringUtils.isEmpty(leadDetails.getMx_Father_contact_Number_KYC())) {
                jsonArray
                        .put(genereateAttributeValueJSONObject("Phone", leadDetails.getMx_Father_contact_Number_KYC()));
                phone = leadDetails.getMx_Father_contact_Number_KYC();
            } else if (!StringUtils.isEmpty(leadDetails.getMx_Mother_contact_number_KYC())) {
                jsonArray
                        .put(genereateAttributeValueJSONObject("Phone", leadDetails.getMx_Mother_contact_number_KYC()));
                phone = leadDetails.getMx_Mother_contact_number_KYC();
            }
        } else {
            phone = leadDetails.getPhone();
        }

        // Update mobile if not present
        if (StringUtils.isEmpty(leadDetails.getMobile()) && !StringUtils.isEmpty(phone)) {
            jsonArray.put(genereateAttributeValueJSONObject("Mobile", phone));
        }

        // Update parent contact details
        if ("1".equals(leadDetails.getMx_IsParentRegistration())) {
            jsonArray.put(genereateAttributeValueJSONObject("mx_Parent_Contact_Number", phone));

            if (!StringUtils.isEmpty(leadDetails.getEmailAddress())) {
                jsonArray.put(genereateAttributeValueJSONObject("mx_Parent_emailId", leadDetails.getEmailAddress()));
            }

            if (!StringUtils.isEmpty(leadDetails.getFirstName())) {
                jsonArray.put(genereateAttributeValueJSONObject("mx_Parent_name", leadDetails.getFirstName()));
            }
        }

        // Update all the phones
        Set<String> phonesList = leadDetails.fetchAllPhones();
        if (!CollectionUtils.isEmpty(phonesList)) {
            Set<String> results = CommonUtils.trimAllElements(phonesList);
            jsonArray.put(genereateAttributeValueJSONObject("mx_PhoneNumberList", String.join(",", results)));
        }

        // SourceCampaign
        if (StringUtils.isEmpty(leadDetails.getSourceCampaign())
                && !StringUtils.isEmpty(leadDetails.getMx_SourceCampaign())) {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("SourceCampaign", leadDetails.getMx_SourceCampaign());
            jsonArray.put(genereateAttributeValueJSONObject("SourceCampaign", leadDetails.getMx_SourceCampaign()));
        }

        // SourceMedium
        if (StringUtils.isEmpty(leadDetails.getSourceMedium())
                && !StringUtils.isEmpty(leadDetails.getMx_SourceMedium())) {
            jsonArray.put(genereateAttributeValueJSONObject("SourceMedium", leadDetails.getMx_SourceMedium()));
        }

        // Twitter facebook and linkedn
        if (StringUtils.isEmpty(leadDetails.getFacebookId()) && !StringUtils.isEmpty(leadDetails.getMx_FaceBookURL())) {
            jsonArray.put(genereateAttributeValueJSONObject("FacebookId", leadDetails.getMx_FaceBookURL()));
        }
        if (StringUtils.isEmpty(leadDetails.getLinkedInId()) && !StringUtils.isEmpty(leadDetails.getMx_LinkedInURL())) {
            jsonArray.put(genereateAttributeValueJSONObject("LinkedInId", leadDetails.getMx_LinkedInURL()));
        }
        if (jsonArray.length() > 0) {
            // Append prospect id
            jsonArray.put(genereateAttributeValueJSONObject("ProspectID", leadDetails.getProspectID()));

            // Need to update lead squared
            boolean needToBeUpdated = true;
            for (int i = 0; (i < 3) && needToBeUpdated; i++) {
                String serverUrl = appendAccessKeys(captureLeadUrl);
                ClientResponse cRes = WebUtils.INSTANCE.doCall(serverUrl, HttpMethod.POST, jsonArray.toString());
                logger.info("cRes:" + cRes.getStatus());
                String response = cRes.getEntity(String.class);
                if (cRes.getStatus() != 200 && cRes.getStatus() != 429) {
                    logger.error(LoggingMarkers.JSON_MASK, "InvalidStatusCodeGetUsers - get lead users not 200 - " + cRes.getStatus() + " cRes:"
                            + cRes.toString());
                } else if (cRes.getStatus() == 429) {
                    logger.error(LoggingMarkers.JSON_MASK, "Too many requests for updateMergeLeadsPojo  , response: " + response);
                    Thread.sleep(1000);
                    continue;
                } else {
                    needToBeUpdated = false;
                }
            }
        }
    }

    public String getLatestLeadCallActivity(LeadDetails leadDetails) {
        Query query = new Query();
        query.addCriteria(Criteria.where(LeadActivity.Constants.LEAD_ID).is(leadDetails.getLeadId()));
        query.addCriteria(Criteria.where(LeadActivity.Constants.EVENT_CODE).is("226"));
        query.with(Sort.by(Direction.DESC, LeadActivity.Constants.CREATION_TIME));
        List<LeadActivity> leadActivities = leadsquaredDAO.runQuery(query, LeadActivity.class);
        String formattedDate = "";
        if (!CollectionUtils.isEmpty(leadActivities)) {
            formattedDate = leadActivities.get(0).getCreatedOn();
        }

        return formattedDate;
    }

    public int getFieldIndex(String[] fields, String fieldName) {
        int index = -1;
        if (fields.length > 0) {
            for (int i = 0; i < fields.length; i++) {
                if (fieldName.toLowerCase().equals(fields[i].toLowerCase())) {
                    index = i;
                }
            }
        }

        return index;
    }

    public UploadIdentifier pushLeadsToLeadsquared(File file, PushLeadsquaredReq pushLeadsquaredReq) throws Exception {

        String leadPushProcessCountString = redisDAO.get(LeadsquaredHelper.BULK_LEAD_PUSH_PROCESS_COUNT);

        if(StringUtils.isEmpty(leadPushProcessCountString)){
            redisDAO.setex(LeadsquaredHelper.BULK_LEAD_PUSH_PROCESS_COUNT , "1", DateTimeUtils.SECONDS_PER_HOUR * 3);
        } else {
            Integer countInt = Integer.parseInt(leadPushProcessCountString);
            redisDAO.setex(LeadsquaredHelper.BULK_LEAD_PUSH_PROCESS_COUNT , String.valueOf(countInt+1), DateTimeUtils.SECONDS_PER_HOUR * 3);
        }

        List<String[]> rows = csvManager.parseCSVFile(file);
        if (CollectionUtils.isEmpty(rows) || rows.size() <= 1) {
            logger.info("No lead fields found");
            throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, "No leads found to push");
        }
//        ExecutorService service = Executors.newFixedThreadPool(nThreads);
        UploadIdentifier uploadIdentifier;

        String checksum = FileUtils.getFileChecksum(MessageDigest.getInstance("SHA-1"), file);
        String key = "PROCESS_" + checksum.toUpperCase();
        logger.info("PUSH TO LEAD SQUARED REDIS KEY :" + key);
        redisDAO.setex(key, "0", 1000);
        try {
            // Validate the fields
            String[] fields = rows.get(0);
            validateFieldsNames(fields);
            int emailFieldIndex = getFieldIndex(fields, "EmailAddress");
            int phoneFieldIndex = getFieldIndex(fields, "Phone");
            int contextIndex = getFieldIndex(fields, "ContextField");
            if ((emailFieldIndex < 0) && (phoneFieldIndex < 0)) {
                throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "EmailAddress or Phone should be a column");
            }
            if (contextIndex < 0) {
                throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Context index should be mandatory column");
            }

            logger.info("leadVcCode save into mongo db");
            setVcCodesToLeads(file);

            // Create upload identifier
            uploadIdentifier = new UploadIdentifier();
            uploadIdentifier.setFileName(file.getName());
            uploadIdentifier.setActivityKey(CommonUtils.generateRandomUUID());
            uploadIdentifier.addUploadTime();
            uploadIdentifier.setUploadFileStatus(UploadFileStatus.PROCESSING);
            uploadIdentifier.setUploadFileType(UploadFileType.PUSH_LEADSQUARED);
            uploadIdentifier.setFieldNames(Arrays.toString(fields));
            uploadIdentifier.setNoActivity(pushLeadsquaredReq.getNoActivity());
            uploadIdentifier.setSource(pushLeadsquaredReq.getSource());
            UploadLeadProperties uploadLeadProperties = new UploadLeadProperties();
            // Dont count the header
            uploadLeadProperties.setTotalLeads(rows.size() - 1);
            uploadLeadProperties.setLeadsPushed(0);
            uploadIdentifier.setLeadProperties(uploadLeadProperties);
            uploadIdentifierManager.updateUploadIdentifier(uploadIdentifier);

            // Create the request entries
            int updatedEntriesCount = 0;
            int end = rows.size() - 1;

            List<Future<Integer>> futures = new ArrayList<>();
            for (int start = 1; start <= end; start += 25) {
                JSONArray bulkUploadRequest = new JSONArray();
                List<String> contextList = new ArrayList<>();
                for (int i = start; (i < (start + 25)) && (i < rows.size()); i++) {
                    String[] row = rows.get(i);
                    logger.info("Row:" + Arrays.toString(row));
                    if (!validRow(row)) {
                        logger.info("InvalidRow:" + i);
                        continue;
                    }

                    JSONArray jsonArray = createAttributeValueJSONObject(fields, row);
                    if (jsonArray.length() > 0) {
                        if (phoneFieldIndex >= 0) {
                            JSONObject jsonObject = new JSONObject();
                            jsonObject.put("Attribute", "SearchBy");
                            jsonObject.put("Value", "Phone");
                            jsonArray.put(jsonObject);
                        }
                        bulkUploadRequest.put(jsonArray);
                    }
                    String contextValue = row[contextIndex];
                    if (!StringUtils.isEmpty(contextValue)) {
                        contextList.add(contextValue);
                    } else {
                        contextList.add("");
                    }
                }
//                int finalStart = start;
//                Future<Integer> future = service.submit(() -> pushDataToLeadSquared(pushLeadsquaredReq, uploadIdentifier, finalStart, bulkUploadRequest, contextList));
//                futures.add(future);
                updatedEntriesCount += pushDataToLeadSquared(pushLeadsquaredReq, uploadIdentifier, start, bulkUploadRequest, contextList);

                double percent = (((double) start) / rows.size()) * 100;
                redisDAO.setex(key, String.valueOf(percent), 3600);
            }

            /*checkFutureCompleted(futures, key);

            for (Future<Integer> future : futures) {
                updatedEntriesCount += future.get();
            }*/

            if (updatedEntriesCount > 0) {
                uploadIdentifier.setUploadFileStatus(UploadFileStatus.SUCCESSFUL);
            } else {
                uploadIdentifier.setUploadFileStatus(UploadFileStatus.FAILED);
            }
            uploadIdentifierManager.updateUploadIdentifier(uploadIdentifier);
        } finally {
//            service.shutdownNow();
            redisDAO.setex(key, String.valueOf(100), 3600);

            String redisKeyAfterProcess = redisDAO.get(LeadsquaredHelper.BULK_LEAD_PUSH_PROCESS_COUNT);

            if(StringUtils.isNotEmpty(redisKeyAfterProcess)) {

                Integer countInt = Integer.parseInt(redisKeyAfterProcess);

                if (countInt > 1) {
                    redisDAO.setex(LeadsquaredHelper.BULK_LEAD_PUSH_PROCESS_COUNT , String.valueOf(countInt-1), DateTimeUtils.SECONDS_PER_HOUR * 3);
                } else {
                    redisDAO.del(LeadsquaredHelper.BULK_LEAD_PUSH_PROCESS_COUNT);
                }
            }
        }
        return uploadIdentifier;
    }

    private <E> void checkFutureCompleted(List<Future<E>> futures, String key) throws InterruptedException, InternalServerErrorException {
        while (true) {
            boolean done = true;
            double count = 0;
            for (Future<E> future : futures) {
                done &= future.isDone();
                if (future.isDone()) {
                    count++;
                }
            }
            if (done) {
                redisDAO.setex(key, "100", 600);
                break;
            } else {
                int size = futures.size() == 0 ? 1 : futures.size();
                double percent = (count / size) * 100;
                redisDAO.setex(key, String.valueOf(percent), 3600);
                Thread.sleep(10000);
            }
        }
    }

    private int pushDataToLeadSquared(PushLeadsquaredReq pushLeadsquaredReq, UploadIdentifier uploadIdentifier, int start, JSONArray bulkUploadRequest, List<String> contextList) throws InterruptedException, VException {
        String response = "";
        int count = 0;
        boolean success = false, activitySuccess = false;

        if(Boolean.TRUE.equals(pushLeadsquaredReq.getNoActivity()))
            activitySuccess = true;

        List<String> leadIds = new ArrayList<>();

        for (int retryCount = 0; retryCount < 7; retryCount++) {
            String serverUrl = appendAccessKeys(updateLeadDetailsBulkUrl);
            //changed to routing url as per threshold
            ClientResponse cRes =  lsWebUtils.leadsquaredDoBulkCallWithRetries(serverUrl, HttpMethod.POST,
                    bulkUploadRequest.toString(),true);
            logger.info("cRes:" + cRes.getStatus());
            response = cRes.getEntity(String.class);
            if (cRes.getStatus() == 429) {
                logger.warn("Too many requests for pushLeadsToLeadsquared  , response: " + response);
                Thread.sleep(2000);
                continue;
            } else if (cRes.getStatus() != 200) {

                logger.error("Error occured updating request: {} the error: {}", bulkUploadRequest.toString(), response);

                if (StringUtils.isNotEmpty(response) && response.contains("Please contact administrator")) {
                    Thread.sleep(2000);
                    continue;
                }

                for (int errorRowNumber = start; errorRowNumber < (start + 25); errorRowNumber++) {
                    uploadIdentifier.addErrorRow(errorRowNumber + ":BatchUpdateError-" + cRes.getStatus()
                            + "-errorResponse:" + response);
                }
                break;
            } else {
                // Success

                logger.info("Response:" + response);

                if (StringUtils.isEmpty(response)) {
                    for (int errorRowNumber = start; errorRowNumber < (start + 25); errorRowNumber++) {
                        uploadIdentifier.addErrorRow(errorRowNumber + ":BatchUpdateEmptyResponse");
                    }
                    break;
                } else {

                    try {
                        if(response.startsWith("{")) {
                            JSONObject json = new JSONObject(response);
                            if (json.has("ExceptionType")) {
                                String exceptionType = json.optString("ExceptionType");
                                if ("MXAPIRateLimitExceededException".equalsIgnoreCase(exceptionType)) {
                                    Thread.sleep(2000);
                                    continue;
                                }
                            }
                        }
                    }catch (Exception e){
                        logger.error("Ignoring Json parse error : {}",response,e);
                    }

                    success = true;
                }

                // Update the error fields in the extract lead ids
                leadIds = extractLeadIds(response, uploadIdentifier, start);

                break;
            }
        }

        if (CollectionUtils.isNotEmpty(leadIds)) {
            for (int activityRetry = 0; (activityRetry < 7)
                    && !Boolean.TRUE.equals(pushLeadsquaredReq.getNoActivity()); activityRetry++) {
                JSONArray jsonArray = getPushedActivityList(leadIds, contextList,
                        uploadIdentifier.getActivityKey());
                String postActivityUrl = appendAccessKeys(postLeadActivityBulkUrl);
                //changed to routing url as per threshold
                ClientResponse postActivityRes = lsWebUtils.leadsquaredDoBulkCallWithRetries(postActivityUrl, HttpMethod.POST,
                        jsonArray.toString(),true);
                logger.info("cRes:" + postActivityRes.getStatus());
                String postActivityResponse = postActivityRes.getEntity(String.class);
                if (postActivityRes.getStatus() == 429) {
                    logger.warn("Too many requests for postActivityResponse  , response: " + postActivityResponse);
                    Thread.sleep(2000);
                    continue;
                } else if (postActivityRes.getStatus() != 200) {
                    logger.error("Error updating the activities");
                    for (int errorRowNumber = start; errorRowNumber < (start + 25); errorRowNumber++) {
                        uploadIdentifier.addErrorRow(errorRowNumber + ":ActivityPostUpdateError-cRes:"
                                + postActivityRes.getStatus() + "-Response:" + postActivityResponse);
                    }
                    break;
                }

                logger.info("Activity Response:" + postActivityResponse);
                if (!StringUtils.isEmpty(postActivityResponse)) {
                    activitySuccess = prossesBulkActivityResponse(postActivityResponse, uploadIdentifier);
                } else {

                    for (int i = 0; i < leadIds.size(); i++) {
                        for (int errorRowNumber = start; errorRowNumber < (start + 25); errorRowNumber++) {
                            uploadIdentifier
                                    .addErrorRow(errorRowNumber + ":ActivityPostError:" + leadIds.get(i));
                        }
                    }
                }

                count += jsonArray == null ? 0 : jsonArray.length();
                break;
            }
        }

        logger.info("POST LS DATA success : {}",success);
        logger.info("POST LS DATA activitySuccess : {}",activitySuccess);

        if (!success || !activitySuccess) {
            for (int errorRowNumber = start; errorRowNumber < (start + 25); errorRowNumber++) {
                uploadIdentifier
                        .addErrorRow(errorRowNumber + ":RetryError:" + " Please retry again for this row");
            }
        }
        return count;
    }

    public boolean prossesBulkActivityResponse(String jsonResponse, UploadIdentifier uploadIdentifier) {
        boolean success = true;
        JSONObject jsonObject = new JSONObject(jsonResponse);
        if (!jsonObject.isNull(jsonResponse)) {
            JSONArray jsonArray = jsonObject.getJSONArray("Response");
            if (jsonArray == null || jsonArray.length() <= 0) {
                return success;
            }

            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject activityRes = jsonArray.getJSONObject(i);
                if (activityRes.has("ErrorMessage")) {
                    success = false;
                    uploadIdentifier.addErrorRow("ActivityPostError:" + activityRes.getString("ErrorMessage"));
                }
            }
        }
        return success;
    }

    public String uploadVedantuDataSource(String leadIdsString, String sourceName) throws VException {
        String[] leadIds = leadIdsString.split(",");
        if (leadIds.length <= 0) {
            return "";
        }

        String response = "";
        for (int i = 0; i < leadIds.length; i += 45) {
            JSONArray leadArray = new JSONArray();
            for (int j = i; (j < (i + 45)) && (j < leadIds.length); j++) {
                leadArray.put(leadIds[j]);
            }

            JSONObject leadIdsField = new JSONObject();
            leadIdsField.put("LeadIds", leadArray);

            JSONObject vedantuDataAttributeJSONObject = genereateAttributeValueJSONObject("mx_Vedantu_data_source",
                    sourceName);
            leadIdsField.put("LeadFields", vedantuDataAttributeJSONObject);
            String postActivityUrl = appendAccessKeys(updateBulkLeadFeildsUrl);
            //changed to routing url as per threshold
            ClientResponse postActivityRes = lsWebUtils.leadsquaredDoBulkCallWithRetries(postActivityUrl, HttpMethod.POST,
                    leadIdsField.toString(),true);
            logger.info("cRes:" + postActivityRes.getStatus());
            String postActivityResponse = postActivityRes.getEntity(String.class);
            logger.info("Response:" + postActivityResponse);
            response += postActivityResponse;
        }
        return response;
    }

    public List<LeadDetails> getLeadDetails(String emailAddress, String phone)
            throws ParseException, InterruptedException {
        // Fetch from lead squared
        List<LeadDetails> leads = new ArrayList<>();
        if (!StringUtils.isEmpty(emailAddress)) {
            List<LeadDetails> results = fetchLeadsByEmailId(emailAddress);
            if (!CollectionUtils.isEmpty(results)) {
                leads.addAll(results);
            }
        }
        if (!StringUtils.isEmpty(phone)) {
            List<LeadDetails> results = fetchLeadsByPhone(phone);
            if (!CollectionUtils.isEmpty(results)) {
                leads.addAll(results);
            }
        }
        return leads;
    }

    public JSONArray createAttributeValueJSONObject(String[] fieldNames, String[] values) {
        JSONArray jsonArray = new JSONArray();
        for (int i = 0; i < fieldNames.length; i++) {
            if (!StringUtils.isEmpty(values[i]) && (!PUSH_LS_EXCLUDED_FIELDS.contains(fieldNames[i]))
                    || "ProspectStage".equals(fieldNames[i])) {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("Attribute", fieldNames[i]);
                jsonObject.put("Value", values[i]);
                jsonArray.put(jsonObject);
            }
        }
        return jsonArray;
    }

    public List<String> getLeadSchemaNames() throws VException {
        String serverUrl = appendAccessKeys(getLeadMetadataUrl);
        ClientResponse cRes = WebUtils.INSTANCE.doCall(serverUrl, HttpMethod.GET, null);
        logger.info("cRes:" + cRes.getStatus());
        String response = cRes.getEntity(String.class);
        logger.info("Response:" + response);
        List<String> schemaNames = new ArrayList<String>();
        try {
            JSONArray jsonArray = new JSONArray(response);
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject schemaObject = jsonArray.getJSONObject(i);
                schemaNames.add(schemaObject.getString("SchemaName"));
            }
            return schemaNames;
        } catch (Exception ex) {
            throw new VException(ErrorCode.SERVICE_ERROR,
                    "Error fetching lead schema names:" + ex.getMessage() + " toString:" + ex.toString());
        }
    }

    public long uploadLeadNotes(File file) throws FileNotFoundException, BadRequestException {
        logger.info("Request:" + file.getName());
        List<String[]> rows = csvManager.parseCSVFile(file);
        String[] fields = rows.get(0);
        int prospectIDIndex = getFieldIndex(fields, "ProspectID");
        int notesIndex = getFieldIndex(fields, "Notes");

        long leadsUpdated = 0;
        if (prospectIDIndex < 0 || notesIndex < 0) {
            logger.info("Invalid field names:" + prospectIDIndex + " notes:" + notesIndex);
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR,
                    "Invalid field names:" + prospectIDIndex + " notes:" + notesIndex);
        }
        for (int i = 1; i < rows.size(); i++) {
            String[] row = rows.get(i);
            logger.info("Row : " + row[prospectIDIndex] + " notes:" + row[notesIndex]);
            try {
                if (StringUtils.isEmpty(row[prospectIDIndex]) || StringUtils.isEmpty(row[notesIndex])) {
                    continue;
                }
                pushLeadNotes(row[prospectIDIndex], row[notesIndex]);
                leadsUpdated++;
            } catch (Exception ex) {
                logger.error("Error:" + ex.toString() + " messagge:" + ex.getMessage());
            }
        }
        return leadsUpdated;
    }

    public Set<String> getLeadsForMerging(String email, String phoneNumber)
            throws ParseException, InterruptedException {
        // Fetch from leadsquared
        List<LeadDetails> leads = getLeadDetails(email, phoneNumber);
        Set<String> leadSquaredIds = extractAllLeadIds(leads);

        // Fetch from vedantu data
        List<UserData> userData = userManager.getUserData(email, phoneNumber);
        if (!CollectionUtils.isEmpty(userData)) {
            for (UserData entry : userData) {
                leadSquaredIds.addAll(entry.getLeadSquaredIds());
            }
        }

        return leadSquaredIds;
    }

    public void pushWebinarRegisterActivity(WebinarUserRegistrationInfo webinarUserRegistrationInfo,
                                            WebinarInfo webinarInfo) {

        PostLeadActivitiyReq req = PostLeadActivitiyReq.createWebinarRegisteredActivitiyReq(webinarUserRegistrationInfo,
                webinarInfo);
        leadsquaredActivityManager.PostActivityOnLead(req);
    }

    public void pushAttendedActivity(WebinarUserRegistrationInfo webinarUserRegistrationInfo, WebinarInfo webinarInfo) {

        PostLeadActivitiyReq req = PostLeadActivitiyReq.createWebinarAttendedActivitiyReq(webinarUserRegistrationInfo,
                webinarInfo);
        leadsquaredActivityManager.PostActivityOnLead(req);
    }

    public void pushNotAttendedActivity(WebinarUserRegistrationInfo webinarUserRegistrationInfo,
                                        WebinarInfo webinarInfo) {

        PostLeadActivitiyReq req = PostLeadActivitiyReq
                .createWebinarNotAttendedActivitiyReq(webinarUserRegistrationInfo, webinarInfo);
        leadsquaredActivityManager.PostActivityOnLead(req);
    }

    private void pushLeadNotes(String prospectID, String notes) throws InterruptedException {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("RelatedProspectId", prospectID);
        jsonObject.put("Note", notes);
        for (int i = 0; i < 3; i++) {
            String serverUrl = appendAccessKeys(createNotesUrl);
            ClientResponse cRes = WebUtils.INSTANCE.doCall(serverUrl, HttpMethod.POST, jsonObject.toString());
            logger.info(LoggingMarkers.JSON_MASK,"cRes:" + cRes.toString());
            String response = cRes.getEntity(String.class);
            if (cRes.getStatus() != 200 && cRes.getStatus() != 429) {
                logger.error(LoggingMarkers.JSON_MASK, "InvalidStatusCodeGetUsers - get lead users not 200 - " + cRes.getStatus() + " cRes:"
                        + cRes.toString());
            } else if (cRes.getStatus() == 429) {
                logger.error(LoggingMarkers.JSON_MASK, "Too many requests for pushLeadNotes  , response: " + response);
                Thread.sleep(1000);
                continue;
            } else {
                return;
            }
        }
    }

    private void validateFieldsNames(String[] fields) throws VException {
        List<String> schemaNames = getLeadSchemaNames();
        for (String field : fields) {
            if (!schemaNames.contains(field) && !PUSH_LS_EXCLUDED_FIELDS.contains(field)) {
                String errorMessage = "Invalid field name:" + field + " Allowed fields:"
                        + Arrays.toString(schemaNames.toArray());
                logger.info(errorMessage);
                throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, errorMessage);
            }
        }
    }

    private void saveAllLeads(List<LeadDetails> leadDetailsList) throws Exception {
        if (CollectionUtils.isEmpty(leadDetailsList)) {
            return;
        }

        logger.info("leadSize:" + leadDetailsList.size());
        Query query = new Query();
        List<String> leadIdList = getAllLeadIdList(leadDetailsList);
        query.addCriteria(Criteria.where(LeadDetails.Constants.LEAD_ID).in(leadIdList));
        List<LeadDetails> existingLeads = leadsquaredDAO.runQuery(query, LeadDetails.class);
        Map<String, LeadDetails> leadDetailsMap = getAllLeadIdMap(existingLeads);
        for (LeadDetails leadDetails : leadDetailsList) {
            if (leadDetailsMap.containsKey(leadDetails.getLeadId())) {
                LeadDetails existingEntry = leadDetailsMap.get(leadDetails.getLeadId());
                leadDetails.setId(existingEntry.getId());
                leadDetails.setCreationTime(existingEntry.getCreationTime());
                leadDetails.setLastUpdated(existingEntry.getLastUpdated());
                logger.info("id found : " + leadDetails.getId());
            }
            leadsquaredDAO.createLead(leadDetails);
        }
    }

    private List<String> getAllLeadIdList(List<LeadDetails> leadDetailsList) {
        List<String> leadIdList = new ArrayList<>();
        if (!CollectionUtils.isEmpty(leadDetailsList)) {
            for (LeadDetails leadDetails : leadDetailsList) {
                leadIdList.add(leadDetails.getLeadId());
            }
        }

        return leadIdList;
    }

    private Map<String, LeadDetails> getAllLeadIdMap(List<LeadDetails> leadDetailsList) {
        Map<String, LeadDetails> leadMap = new HashMap<>();
        if (!CollectionUtils.isEmpty(leadDetailsList)) {
            for (LeadDetails leadDetails : leadDetailsList) {
                leadMap.put(leadDetails.getLeadId(), leadDetails);
            }
        }

        return leadMap;
    }

    private List<String> extractLeadIds(String jsonResponse, UploadIdentifier uploadIdentifier, int startRow) {
        List<String> leadIds = new ArrayList<>();
        JSONArray jsonArray = new JSONArray(jsonResponse);
        if (jsonArray == null || jsonArray.length() <= 0) {
            return leadIds;
        }

        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject jsonObject = jsonArray.getJSONObject(i);
            if (!jsonObject.isNull("LeadId")) {
                // {"RowNumber":1,"LeadId":"f83b9fee-f0ca-4d57-b1a2-793ddca96918","LeadCreated":false,"LeadUpdated":true,"AffectedRows":1}
                String leadId = jsonObject.getString("LeadId");
                if (!StringUtils.isEmpty(leadId)) {
                    leadIds.add(leadId);
                    Boolean leadCreated = null;
                    if (!jsonObject.isNull("LeadCreated")) {
                        leadCreated = jsonObject.getBoolean("LeadCreated");
                    }

                    Boolean leadUpdated = null;
                    if (!jsonObject.isNull("LeadUpdated")) {
                        leadUpdated = jsonObject.getBoolean("LeadUpdated");
                    }
                    uploadIdentifier.addUploadLeadStatus(leadId, leadCreated, leadUpdated);
                } else {
                    String errorField = (startRow + i + 1) + ":";
                    if (!jsonObject.isNull("ExceptionType")) {
                        String exceptionType = jsonObject.getString("ExceptionType");
                        errorField += "ExceptionType:" + exceptionType;
                    } else {
                        errorField += "Exception:NotProvided";
                    }

                    if (!jsonObject.isNull("ErrorMessage")) {
                        String errorMessage = jsonObject.getString("ErrorMessage");
                        errorField += " Message:" + errorMessage;
                    }
                    uploadIdentifier.addErrorRow(errorField);
                }
            }
        }

        return leadIds;
    }

    private JSONArray getPushedActivityList(List<String> leadIds, List<String> contextList, String activityKey) {
        JSONArray jsonArray = new JSONArray();
        if (CollectionUtils.isEmpty(leadIds)) {
            return jsonArray;
        }

        if (StringUtils.isEmpty(activityKey)) {
            activityKey = StringUtils.EMPTY;
        }

        for (int i = 0; i < leadIds.size(); i++) {
            JSONObject jsonObject = generatePushActivity(leadIds.get(i),
                    contextList.get(i) + " ActivityKey:" + activityKey);
            jsonArray.put(jsonObject);
        }

        return jsonArray;
    }

    private JSONObject genereateAttributeValueJSONObject(String key, String value) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("Attribute", key);
        jsonObject.put("Value", value);
        return jsonObject;
    }

    private JSONObject generatePushActivity(String leadId, String contextStr) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        sdf.setTimeZone(TimeZone.getTimeZone(DateTimeUtils.TIME_ZONE_GMT));
        String dataString = sdf.format(new Date(System.currentTimeMillis()));

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("RelatedProspectId", leadId);
        jsonObject.put("ActivityEvent", ConfigUtils.INSTANCE.getStringValue("leadsquared.activity.pushed.from.data"));
        jsonObject.put("ActivityNote", "Pushed on : " + dataString + ". Context:" + contextStr);
        jsonObject.put("ActivityOwnerEmail", "admin@vedantu.com");
        return jsonObject;
    }

    private String appendAccessKeys(String url) {
        if ("PROD".equals(ConfigUtils.INSTANCE.properties.getProperty("environment"))
                || "LOCAL".equals(ConfigUtils.INSTANCE.properties.getProperty("environment"))
        ) {
            if (url.contains("?")) {
                return url + "&accessKey=" + accessKey + "&secretKey=" + secretKey;
            } else {
                return url + "?accessKey=" + accessKey + "&secretKey=" + secretKey;
            }
        } else {
            return url;
        }
    }

    private boolean validRow(String[] row) {
        if (row.length > 0) {
            for (String value : row) {
                if (!StringUtils.isEmpty(value)) {
                    return true;
                }
            }
        }

        return false;
    }

    private void setVcCodesToLeads(File file) throws Exception {
        List<String[]> rows = csvManager.parseCSVFile(file);
        logger.info("csv file List : " + rows.toString());
        String[] fields = rows.get(0);
        validateFieldsNames(fields);
        int emailFieldIndex = getFieldIndex(fields, "EmailAddress");
        int phoneFieldIndex = getFieldIndex(fields, "Phone");
        int mx_VCLeadIndex = getFieldIndex(fields, "mx_VCLead");
        int mx_VC_CodeIndex = getFieldIndex(fields, "mx_VC_Code");
        List<LeadVcCode> leadVcCodes = new ArrayList<>();
        logger.info("given rows size : " + rows.size());
        for (int i = 1; i < rows.size(); i++) {
            String[] row = rows.get(i);
            if (validRow(row)) {
                LeadVcCode leadVcCode = new LeadVcCode();
                if (emailFieldIndex > -1 && StringUtils.isNotEmpty(row[emailFieldIndex])) {
                    leadVcCode.setEmail(row[emailFieldIndex]);
                }
                if (phoneFieldIndex > -1 && StringUtils.isNotEmpty(row[phoneFieldIndex])) {
                    leadVcCode.setPhone(row[phoneFieldIndex]);
                }
                if (mx_VCLeadIndex > -1 && StringUtils.isNotEmpty(row[mx_VCLeadIndex])) {
                    leadVcCode.setMx_VCLead(row[mx_VCLeadIndex]);

                }
                if (mx_VC_CodeIndex > -1 && StringUtils.isNotEmpty(row[mx_VC_CodeIndex])) {
                    leadVcCode.setVcCode(row[mx_VC_CodeIndex]);

                }

                if (StringUtils.isNotEmpty(leadVcCode.getVcCode())) {
                    leadVcCodes.add(leadVcCode);
                }
                logger.info("row number : " + i + "leadVcCode : " + leadVcCode.toString());
            }
        }
        if (ArrayUtils.isNotEmpty(leadVcCodes)) {
            logger.info("leads Detaisl with LeadVcCode : " + leadVcCodes.toString());
            leadsquaredDAO.insertAllEntities(leadVcCodes, "LeadVcCode");
        }
    }

    public boolean getLeadsquaredLeadsByVcCode(String vcCode) {
        if (StringUtils.isNotEmpty(vcCode)) {
            Query query = new Query();
            query.addCriteria(Criteria.where("vcCode").is(vcCode.trim()));
            List<LeadVcCode> leadVcCodes = leadsquaredDAO.runQuery(query, LeadVcCode.class);
            logger.info("leadVcCodes :" + leadVcCodes.toString());
            if (leadVcCodes != null && !leadVcCodes.isEmpty()) {
                logger.info("leadVcCodes.get(0) : " + leadVcCodes.get(0));
//                return new PlatformBasicResponse(true, leadVcCodes.get(0).getEmail(), null);
                return true;
            }
        }
//        return new PlatformBasicResponse(false, null, "VcCode Not Found");
        return false;
    }

    public PlatformBasicResponse setVcCode(String vcCode, String email, String phone, String mx_VCLead) throws Exception {
        PlatformBasicResponse platformBasicResponse = new PlatformBasicResponse(false, null, null);
        LeadVcCode leadVcCode = new LeadVcCode();
        if (StringUtils.isNotEmpty(vcCode)) {
            leadVcCode.setVcCode(vcCode);
            platformBasicResponse.setSuccess(true);
        }
        if (StringUtils.isNotEmpty(email)) {
            leadVcCode.setEmail(email);
            platformBasicResponse.setSuccess(true);
        }
        if (StringUtils.isNotEmpty(phone)) {
            leadVcCode.setPhone(phone);
            platformBasicResponse.setSuccess(true);
        }
        if (StringUtils.isNotEmpty(mx_VCLead)) {
            leadVcCode.setMx_VCLead(mx_VCLead);
            platformBasicResponse.setSuccess(true);
        }
        leadsquaredDAO.createVcCode(leadVcCode);
        return platformBasicResponse;
    }

    public PlatformBasicResponse setVcCodes(List<LeadVcCodeReq> leadVcCodeReqs) throws Exception {
        List<LeadVcCode> leadVcCodeList = new ArrayList<>();
        for (LeadVcCodeReq leadVcCodeReq : leadVcCodeReqs) {
            LeadVcCode leadVcCode = new LeadVcCode();
            if (StringUtils.isNotEmpty(leadVcCodeReq.getVcCode())) {
                leadVcCode.setVcCode(leadVcCodeReq.getVcCode());
            }
            if (StringUtils.isNotEmpty(leadVcCodeReq.getEmail())) {
                leadVcCode.setEmail(leadVcCodeReq.getEmail());
            }
            if (StringUtils.isNotEmpty(leadVcCodeReq.getPhone())) {
                leadVcCode.setPhone(leadVcCodeReq.getPhone());
            }
            if (StringUtils.isNotEmpty(leadVcCodeReq.getMx_VCLead())) {
                leadVcCode.setMx_VCLead(leadVcCodeReq.getMx_VCLead());
            }
            if (leadVcCode != null) {
                leadVcCodeList.add(leadVcCode);
            }
        }
        if (ArrayUtils.isNotEmpty(leadVcCodeList)) {
            leadsquaredDAO.insertAllEntities(leadVcCodeList, "LeadVcCode");
            return new PlatformBasicResponse(true, "inserted successfully", null);
        } else {
            return new PlatformBasicResponse(false, "", "Not Found Any Request Data");
        }
    }

    public void updatePaymentThroughSNS(String request) {
        if (StringUtils.isNotEmpty(request)) {
            logger.info("calling sqs for updatePaymentThroughSNS with request : " + request);
            awsSQSManager.sendToSQS(SQSQueue.UPDATE_LEADSQUARED_QUEUE, SQSMessageType.UPDATE_LEAD_PAYMENT_LEADSQUARED, request, SQSMessageType.UPDATE_LEAD_PAYMENT_LEADSQUARED.name());
        }
    }

    public void updatePaymentFromSQS(String text) throws VException {
        if (StringUtils.isNotEmpty(text)) {
            logger.info("sqs triggered for updatePaymentFromSQS with : " + text);
            Type type = new TypeToken<Map<String, String>>() {
            }.getType();
            Map<String, String> data = gson.fromJson(text, type);
            logger.info("inputData from queue : " + data.toString());
            if (StringUtils.isNotEmpty(data.get("userId")) && StringUtils.isNotEmpty(data.get("amount"))) {
                logger.info("getting user Details by using userid : " + data.get("userId"));
                ClientResponse cResp = WebUtils.INSTANCE.doCall(USER_ENDPOINT + "/getUserById?userId=" + data.get("userId"),
                        HttpMethod.GET, null, true);
                String jsonResp = cResp.getEntity(String.class);
                User user = gson.fromJson(jsonResp, User.class);
                if (user == null && StringUtils.isEmpty(user.getEmail())) {
                    throw new NotFoundException(ErrorCode.USER_NOT_FOUND, "user not found");
                }
                logger.info("user Details : " + user.toString());

                JSONArray jsonArray = new JSONArray();

                JSONObject jsonObject1 = new JSONObject();
                jsonObject1.put("Attribute", "Phone");
                jsonObject1.put("Value", user.getContactNumber());
                jsonArray.put(jsonObject1);
                logger.info("jsonObject1 : " + jsonObject1.toString());

                JSONObject jsonObject2 = new JSONObject();
                jsonObject2.put("Attribute", "mx_first_payment_amount");
                jsonObject2.put("Value", data.get("amount"));
                jsonArray.put(jsonObject2);
                logger.info("jsonObject2 : " + jsonObject2.toString());

                JSONObject jsonObject3 = new JSONObject();
                jsonObject3.put("Attribute", "SearchBy");
                jsonObject3.put("Value", "Phone");
                jsonArray.put(jsonObject3);
                logger.info("jsonObject3 : " + jsonObject3.toString());

                logger.info("calling the LeadUpsertUrl with : ");
                logger.info(jsonArray);
                String serverUrl = appendAccessKeys(getLeadUpsertUrl);
                logger.info(LoggingMarkers.JSON_MASK, "serverUrl : " + serverUrl);
                if (ConfigUtils.INSTANCE.properties.getProperty("environment").equals("PROD")) {

                    for(int retryCount = 0; retryCount < 5; retryCount++) {
                        ClientResponse cRes = WebUtils.INSTANCE.doCall(serverUrl, HttpMethod.POST,
                                jsonArray.toString());
                        logger.info(LoggingMarkers.JSON_MASK, "cRes:" + cRes.toString());
                        logger.info("cRes status : " + cRes.getStatus());
                        String response = cRes.getEntity(String.class);

                        if (cRes.getStatus() != 200 && cRes.getStatus() != 429) {
                            throw new RuntimeException("not updated Lead Details Bulk Client Response status : " + cRes.getStatus());
                        } else if (cRes.getStatus() == 429) {
                            try {
                                Thread.sleep(1000);
                            }catch (Exception e){

                            }
                            continue;
                        }
                        break;
                    }

                } else {
                    logger.info(ConfigUtils.INSTANCE.properties.getProperty("environment") + " environment not calling the leadsquared update api");
                }
            }
        }

    }

    public void createLeadIfDeleted(String email, String phone) {

        try {

            if(StringUtils.isEmpty(email) && StringUtils.isEmpty(phone))
                return;

            //Check if present in rediss
            if(StringUtils.isNotEmpty(phone)) {
                String key = phone + "_LS_LEAD_EXIST";
                String isExist = redisDAO.get(key);

                //return if lead exist
                if(StringUtils.isNotEmpty(isExist))
                    return;
            }

            if(StringUtils.isNotEmpty(email)) {
                String key = email + "_LS_LEAD_EXIST";

                String isExist = redisDAO.get(key);

                //return if lead exist
                if(StringUtils.isNotEmpty(isExist))
                    return;
            }

            User user = null;
            if(StringUtils.isNotEmpty(phone))
                user= userManager.getUserByPhone(phone);

            if(user==null && StringUtils.isNotEmpty(email))
                user= userManager.getUserByEmail(email);

            if(user==null){
                logger.warn("User not found:: email: {}, phone: {}", email, phone);
                return;
            }

            if(StringUtils.isNotEmpty(user.getOrgId())) {

                //Store in rediss for 30 days
                String key = "";
                if (StringUtils.isNotEmpty(phone))
                    key = phone + "_LS_LEAD_EXIST";
                else
                    key = email + "_LS_LEAD_EXIST";

                redisDAO.setex(key, "TRUE", 30 * 24 * 60 * 60);

                return;
            }

            for(int i = 0; i<3; i++) {
                try {

                    logger.info("Checking if lead present email: {}, phone: {}", email, phone);
                    List<LeadDetails> leadDetails = null;

                    //Avoiding 1 additional call to LS
                    if(StringUtils.isNotEmpty(phone))
                        leadDetails = getLeadDetails(null, phone);

                    if(CollectionUtils.isEmpty(leadDetails) && StringUtils.isNotEmpty(email))
                        leadDetails = getLeadDetails(email, null);

                    if (CollectionUtils.isEmpty(leadDetails)) {

                        logger.info("Lead not found creating new lead email: {}, phone: {}", email, phone);

                        LeadSquaredRequest req = new LeadSquaredRequest();
                        req.setUser(user);
                        String platformEndpoint = ConfigUtils.INSTANCE.getStringValue("PLATFORM_ENDPOINT");
                        ClientResponse resp = WebUtils.INSTANCE.doCall(platformEndpoint + "/leadsquared/createLeadSync",
                                HttpMethod.POST, new Gson().toJson(req));

                        logger.info("Reponse : {}", resp.getStatus());

                        if(resp.getStatus() != 200){
                            throw new VException(ErrorCode.PERSISTENCE_ERROR, "Error saving lead:" + new Gson().toJson(req));
                        }

                    }

                    //Store in rediss for 30 days
                    String key = "";
                    if(StringUtils.isNotEmpty(phone))
                        key = phone + "_LS_LEAD_EXIST";
                    else
                        key = email + "_LS_LEAD_EXIST";

                    redisDAO.setex(key, "TRUE", 30*24*60*60);

                    break;
                } catch (Exception e) {
                    logger.error("Error saving lead details email: {}, phone: {}", email, phone, e);
                    try {
                        Thread.sleep(3000);
                    } catch (InterruptedException ex) {
                        logger.error(ex);
                    }
                }
            }
        }catch (Exception e){
            logger.error("Error: createLeadIfDeleted", e);
        }
    }

    public List<LeadActivitySerialized> getLeadActivity(String leadId, Integer activityCode) throws InterruptedException, BadRequestException, ConflictException {

        List<LeadActivitySerialized> responseLeads = new ArrayList<>();
        Boolean emptyFlag = false;

        String serverUrl = appendAccessKeys(getLeadActivityByLeadId);
        serverUrl = serverUrl + "&leadId=" + leadId;
        if (activityCode == null) {
            activityCode = 331;
        }

        int offset = 0;
        int size = 10;
        while (true) {
            JSONObject pageLimits = new JSONObject();
            pageLimits.put("Offset", offset);
            pageLimits.put("RowCount", size);
            JSONObject fetchReq = new JSONObject();
            fetchReq.put("Paging", pageLimits);

            JSONObject activityEvent = new JSONObject();
            activityEvent.put("ActivityEvent", activityCode);
            fetchReq.put("Parameter", activityEvent);

            for (int i = 0; i < 3; i++) {

                ClientResponse cRes = WebUtils.INSTANCE.doCall(serverUrl, HttpMethod.POST, fetchReq.toString(), false);
                logger.info("cRes:" + cRes.getStatus());
                if (cRes.getStatus() != 200 && cRes.getStatus() != 429) {
                    logger.warn(LoggingMarkers.JSON_MASK, "InvalidStatusCodeGetUsers - get lead users not 200 - " + cRes.getStatus() + " cRes:"
                            + cRes.toString());

                    //TODO: remove this retry once the api is fixed from LS
                    Thread.sleep(1000);
                    continue;
                } else if (cRes.getStatus() == 429) {
                    logger.warn(LoggingMarkers.JSON_MASK, "TooManyRequests :" + serverUrl + " " + fetchReq.toString());
                    Thread.sleep(1000);
                    continue;
                }

                String response = cRes.getEntity(String.class);
                logger.info("response LS: {}",response);

                if (StringUtils.isEmpty(response)) {
                    emptyFlag = true;
                    break;
                }

                GetLeadActivityResp getLeadActivityResp = gson.fromJson(response, GetLeadActivityResp.class);
                if (getLeadActivityResp != null && ArrayUtils.isNotEmpty(getLeadActivityResp.getProspectActivities())) {
                    responseLeads.addAll((getLeadActivityResp.getProspectActivities()));
                    if(Integer.parseInt(getLeadActivityResp.getRecordCount()) < size){
                        emptyFlag = true;
                    }
                    break;
                }
                emptyFlag =true;
                break;

            }
            if (emptyFlag) {
                break;
            }

            // Handle loop params
            offset += size;
        }
        return responseLeads;
    }

    public void updateSalesUsersSessionData(String message) throws VException, InterruptedException {
        SalesWaveSessionJoinedLSRequest salesWaveSessionJoinedLSRequest = gson.fromJson(message,
                SalesWaveSessionJoinedLSRequest.class);
        logger.info("Recieved request for updating sales wave session data:{}", salesWaveSessionJoinedLSRequest);

        Boolean isTeacherActivityPushed;
        Boolean isTAActivityPushed;
        Boolean isNewSessionUser = Boolean.TRUE;

        List<SalesWaveSessionJoinedLSRequest> salesWaveSessionJoinedLSRequests = salesWaveSessionJoinedLSRequestDao
                .findSalesWaveSessionDataBySessionId(salesWaveSessionJoinedLSRequest.getSessionId());
        if (CollectionUtils.isNotEmpty(salesWaveSessionJoinedLSRequests)) {
            for (SalesWaveSessionJoinedLSRequest request : salesWaveSessionJoinedLSRequests) {
                if (request.getUserId().equals(salesWaveSessionJoinedLSRequest.getUserId())) {
                    request.setConnectionState(salesWaveSessionJoinedLSRequest.getConnectionState());
                    isNewSessionUser = Boolean.FALSE;
                }
            }
        } else {
            salesWaveSessionJoinedLSRequests = new ArrayList<>();
        }
        if (isNewSessionUser) {
            salesWaveSessionJoinedLSRequests.add(salesWaveSessionJoinedLSRequest);
        }
        isTeacherActivityPushed = salesWaveSessionJoinedLSRequests.stream().filter(x -> x.getIsTeacherActivityPushed() != null).anyMatch(x -> x.getIsTeacherActivityPushed());
        isTAActivityPushed = salesWaveSessionJoinedLSRequests.stream().filter(x -> x.getIsTAActivityPushed() != null).anyMatch(x -> x.getIsTAActivityPushed());
        if (!isTAActivityPushed || !isTeacherActivityPushed) {
            Set<SessionRole> studentTeacherRoles = new HashSet<>(Arrays.asList(SessionRole.STUDENT, SessionRole.TEACHER));
            Set<SessionRole> studentTeacherTARoles = new HashSet<>(
                    Arrays.asList(SessionRole.STUDENT, SessionRole.TEACHER, SessionRole.TA));
            Map<SessionRole, List<SalesWaveSessionJoinedLSRequest>> map = new HashMap<>();

            Set<SalesWaveSessionJoinedLSRequest> connectedSessionRequests = salesWaveSessionJoinedLSRequests.stream()
                    .filter(x -> (x.getConnectionState().equals(SessionConnectionState.CONNECTED))).collect(Collectors.toSet());
            connectedSessionRequests.forEach(p -> {
                studentTeacherRoles.remove(p.getSessionRole());
                studentTeacherTARoles.remove(p.getSessionRole());
                map.putIfAbsent(p.getSessionRole(), new ArrayList<>());
                map.get(p.getSessionRole()).add(p);
            });
            if (CollectionUtils.isEmpty(studentTeacherTARoles) && !isTAActivityPushed) {
                leadsquaredActivityManager.convertAndPushLeadActivitiesMessagesToLeadSquaredTA(map);
                salesWaveSessionJoinedLSRequests.forEach(p -> {
                    p.setIsTAActivityPushed(Boolean.TRUE);
                });
                if (!isTeacherActivityPushed) {
                    convertAndPushLeadActivitiesMessagesToLeadSquaredTeacher(map, salesWaveSessionJoinedLSRequests);
                }

            } else if (CollectionUtils.isEmpty(studentTeacherRoles) && !isTeacherActivityPushed) {
                convertAndPushLeadActivitiesMessagesToLeadSquaredTeacher(map, salesWaveSessionJoinedLSRequests);
            }
        }
        salesWaveSessionJoinedLSRequests.forEach(p -> {
            salesWaveSessionJoinedLSRequestDao.saveSalesWaveSessionJoinedLSRequest(p);
        });

    }

    public void convertAndPushLeadActivitiesMessagesToLeadSquaredTeacher(Map<SessionRole, List<SalesWaveSessionJoinedLSRequest>> map, List<SalesWaveSessionJoinedLSRequest> salesWaveSessionJoinedLSRequests) throws VException, InterruptedException {
        leadsquaredActivityManager.convertAndPushLeadActivitiesMessagesToLeadSquaredTeacher(map);
        salesWaveSessionJoinedLSRequests.forEach(p -> {
            p.setIsTeacherActivityPushed(Boolean.TRUE);
        });
    }

}