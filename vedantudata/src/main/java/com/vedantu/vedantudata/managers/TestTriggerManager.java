package com.vedantu.vedantudata.managers;

import com.vedantu.User.User;
import com.vedantu.exception.VException;
import com.vedantu.lms.cmds.enums.EnumBasket;
import com.vedantu.lms.cmds.pojo.CMDSTestAttemptPojo;
import com.vedantu.lms.cmds.pojo.ContentInfoResp;
import com.vedantu.lms.request.CMDSTestInfo;
import com.vedantu.util.LogFactory;
import com.vedantu.vedantudata.dao.ActivityDao;
import com.vedantu.vedantudata.entities.ActivityDetails;
import com.vedantu.vedantudata.utils.ActivityEventConstants;
import com.vedantu.vedantudata.utils.LeadsquaredActivityHelper;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.SQLException;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class TestTriggerManager extends LeadsquaredActivityHelper {

    @Autowired
    private ActivityManager activityManager;

    @Autowired
    private ActivityDao activityDao;

    @Autowired
    private LogFactory logFactory;

    private Logger logger = logFactory.getLogger(TestTriggerManager.class);

    public void process(List<User> activeEnrolledUsers, Map<Long, User> studentAmMapping) throws VException, SQLException {

        if(activeEnrolledUsers.isEmpty())
            return;

        List<String> activeUserIds = activeEnrolledUsers.parallelStream()
                .map(user -> user.getId().toString())
                .distinct()
                .collect(Collectors.toList());

        //2months
        Long now = new Date().getTime();
        Long fromTime = now - ActivityEventConstants.MILLI_MONTH * 2;
        List<ContentInfoResp> contentInfos = activityManager.getContentInfo(activeUserIds, fromTime, now);

        logger.info("======get content info for last 2 months :: {}", contentInfos.size());

        List<String> testIds = contentInfos.parallelStream().map(contentInfoResp -> contentInfoResp.getMetadata().getTestId()).collect(Collectors.toList());
        List<CMDSTestInfo> cmdsTests = activityManager.getTestsDetails(testIds);

        Map<String, List<ContentInfoResp>> filteredContentInfosMap = contentInfos.stream().collect(Collectors.groupingBy(ContentInfoResp::getStudentId));

        for (User user : activeEnrolledUsers) {

            List<ContentInfoResp> filteredContentInfos = filteredContentInfosMap.get(user.getId().toString());

            if (filteredContentInfos == null || filteredContentInfos.isEmpty())
                continue;
            logger.info("======content size for student : {} is :: {} ", user.getEmail(), filteredContentInfos.size());

            List<String> filteredTestIds = filteredContentInfos.parallelStream().map(contentInfoResp -> contentInfoResp.getMetadata().getTestId()).collect(Collectors.toList());

            ///Last content info
            ContentInfoResp contentInfo = filteredContentInfos.get(0);

            List<CMDSTestInfo> filteredCmdsTests = cmdsTests.parallelStream().filter(cmdsTestInfo -> filteredTestIds.contains(cmdsTestInfo.getId())).collect(Collectors.toList());

            if (contentInfo != null) {
                List<CMDSTestAttemptPojo> cmdsTestAttemptPojos = contentInfo.getMetadata().getTestAttempts();

                if (cmdsTestAttemptPojos != null && !cmdsTestAttemptPojos.isEmpty()) {

                    logger.info("======checking test performance of student : {}", user.getEmail());
                    checkAndCreateActivitybasedOnPerformance(user, filteredContentInfos, filteredCmdsTests, studentAmMapping);
                    continue;
                }

                logger.info("======Student {} missed the previous test :: {}", user.getEmail(), contentInfo.getTestId());
                Map<String, String> param = new HashMap<>();
                param.put(ActivityEventConstants.MISSED_TEST, "YES");
                param.put(ActivityEventConstants.NOTE, ActivityEventConstants.ACTIVITY_EVENT_TEST_NOTE_MISSED_TEST);

                Float previousScore = 0f;

                for (ContentInfoResp contentInfoResp : filteredContentInfos) {

                    if (contentInfoResp.getMetadata() != null &&
                            contentInfoResp.getMetadata().getTestAttempts() != null &&
                            !contentInfoResp.getMetadata().getTestAttempts().isEmpty()) {
                        previousScore = contentInfoResp.getMetadata().getTestAttempts().get(0).getMarksAcheived();
                        break;
                    }
                }
                param.put(ActivityEventConstants.LAST_SCORE, previousScore.toString());
                activityManager.pushActivity(user, studentAmMapping, ActivityEventConstants.EVENT_TEST, param);
            }
        }
    }

    private void checkAndCreateActivitybasedOnPerformance(User user, List<ContentInfoResp> contentInfos, List<CMDSTestInfo> cmdsTests, Map<Long, User> studentAmMapping) throws SQLException {

        int gradeValue = activityManager.getGrade(user.getStudentInfo().getGrade());
        if (gradeValue >= 6 && gradeValue <= 8) {

            Double totalAverage = 0.0;

            for (int i = 0; i < contentInfos.size(); i++) {

                ContentInfoResp contentInfoResp = contentInfos.get(i);
                if (contentInfoResp.getMetadata() != null &&
                        contentInfoResp.getMetadata().getTestAttempts() != null &&
                        !contentInfoResp.getMetadata().getTestAttempts().isEmpty()) {
                    Float marksAcheived = contentInfoResp.getMetadata().getTestAttempts().get(0).getMarksAcheived();
                    Float totalMarks = contentInfoResp.getMetadata().getTestAttempts().get(0).getTotalMarks();
                    Float average = marksAcheived / totalMarks;

                    totalAverage += average;
                }

                //Considering only last 3 tests of student
                if (i == ActivityEventConstants.TEST_NUMBER_OF_TESTS_FOR_PERFORMANCE - 1)
                    break;
            }

            totalAverage = totalAverage / ActivityEventConstants.TEST_NUMBER_OF_TESTS_FOR_PERFORMANCE;

            ActivityDetails previousActivityDetails = activityDao.getLastActivity(user.getId(), ActivityEventConstants.EVENT_TEST);
            Long lastActivityTriggerTime = previousActivityDetails != null ? previousActivityDetails.getCreationTime() : null;
            Long currentTime = new Date().getTime();

            Map<String, String> param = new HashMap<>();
            String testLevel = ActivityEventConstants.getTestLevel(gradeValue, totalAverage);
            param.put(ActivityEventConstants.TEST_LEVEL, testLevel);
            param.put(ActivityEventConstants.MISSED_TEST, "NO");

            ContentInfoResp contentInfoResp = contentInfos.get(0);
            Float marksAchieved = contentInfoResp.getMetadata().getTestAttempts().get(0).getMarksAcheived();
            param.put(ActivityEventConstants.LAST_SCORE, marksAchieved.toString());

            if (totalAverage < 60) {

                param.put(ActivityEventConstants.NOTE, ActivityEventConstants.ACTIVITY_EVENT_TEST_HT);
                //alternate days
                if (lastActivityTriggerTime == null || (currentTime - lastActivityTriggerTime) < (ActivityEventConstants.MILLI_DAY * 2))
                    activityManager.pushActivity(user, studentAmMapping, ActivityEventConstants.EVENT_TEST, param);
            } else if (totalAverage < 80) {
                param.put(ActivityEventConstants.NOTE, ActivityEventConstants.ACTIVITY_EVENT_TEST_MT);
                //every week
                if (lastActivityTriggerTime == null || (currentTime - lastActivityTriggerTime) < ActivityEventConstants.MILLI_WEEK)
                    activityManager.pushActivity(user, studentAmMapping, ActivityEventConstants.EVENT_TEST, param);
            } else {
                param.put(ActivityEventConstants.NOTE, ActivityEventConstants.ACTIVITY_EVENT_TEST_LT);
                //every 2 weeks
                if (lastActivityTriggerTime == null || (currentTime - lastActivityTriggerTime) < (ActivityEventConstants.MILLI_WEEK * 2))
                    activityManager.pushActivity(user, studentAmMapping, ActivityEventConstants.EVENT_TEST, param);
            }


        } else if (activityManager.getGrade(user.getStudentInfo().getGrade()) >= 9 && activityManager.getGrade(user.getStudentInfo().getGrade()) <= 12) {

            List<String> phaseTestIds = cmdsTests.parallelStream().filter(cmdsTestInfo -> {
                return cmdsTestInfo.getTestTag() == EnumBasket.TestTagType.PHASE ||
                        cmdsTestInfo.getTestTag() == EnumBasket.TestTagType.ADVANCE;
            }).map(cmdsTestInfo -> cmdsTestInfo.getId()).collect(Collectors.toList());

            List<ContentInfoResp> filteredContentInfosPhase = contentInfos.stream().filter(contentInfoResp -> phaseTestIds.contains(contentInfoResp.getMetadata().getTestId())).collect(Collectors.toList());

            if (filteredContentInfosPhase.size() <= 0)
                return;

            if (filteredContentInfosPhase.size() >= 2) {

                ContentInfoResp contentInfoLast = filteredContentInfosPhase.get(0);
                ContentInfoResp contentInfoLastButOne = filteredContentInfosPhase.get(1);

                Float lastTestPercentage = getTestPercentage(contentInfoLast);
                Float lastButOneTestPercentage = getTestPercentage(contentInfoLastButOne);

                if (lastTestPercentage - lastButOneTestPercentage >= 5) {
                    Map<String, String> param = new HashMap<>();
                    param.put(ActivityEventConstants.MISSED_TEST, "NO");
                    param.put(ActivityEventConstants.NOTE, ActivityEventConstants.ACTIVITY_EVENT_TEST_REGULAR);
                    activityManager.pushActivity(user, studentAmMapping, ActivityEventConstants.EVENT_TEST, param);
                }
            }
        }
    }

    private Float getTestPercentage(ContentInfoResp contentInfoLast) {

        Float testMarksAchieved = 0f;
        if(contentInfoLast !=null && contentInfoLast.getMetadata() !=null
                && contentInfoLast.getMetadata().getTestAttempts() !=null
                && contentInfoLast.getMetadata().getTestAttempts().get(0)!=null
                && contentInfoLast.getMetadata().getTestAttempts().get(0).getMarksAcheived()!=null)
            testMarksAchieved = contentInfoLast.getMetadata().getTestAttempts().get(0).getMarksAcheived();
        Float testTotalMarks = contentInfoLast.getMetadata().getTotalMarks();

        return testTotalMarks>0 ? (testMarksAchieved / testTotalMarks) * 100 : 0;
    }
}
