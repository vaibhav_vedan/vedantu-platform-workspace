package com.vedantu.vedantudata.managers;

import com.vedantu.User.User;
import com.vedantu.exception.VException;
import com.vedantu.onetofew.pojo.EnrollmentPojo;
import com.vedantu.subscription.response.CoursePlanBasicInfo;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.vedantudata.controllers.ActivityController;
import com.vedantu.vedantudata.dao.ActivityDao;
import com.vedantu.vedantudata.utils.ActivityEventConstants;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.SQLException;
import java.text.ParseException;
import java.time.Instant;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class SamAllocationTriggerManager {

    private static final String[] OTO_SAM_LIST = ConfigUtils.INSTANCE.getStringValue("oto_sam_list").split(",");
    private static final String[] OTM_SAM_LIST_6_9 = ConfigUtils.INSTANCE.getStringValue("otm_sam_6_9").split(",");
    private static final String[] OTM_SAM_LIST_10 = ConfigUtils.INSTANCE.getStringValue("otm_sam_10").split(",");
    private static final String[] OTM_SAM_LIST_11 = ConfigUtils.INSTANCE.getStringValue("otm_sam_11").split(",");
    private static final String[] OTM_SAM_LIST_12 = ConfigUtils.INSTANCE.getStringValue("otm_sam_12").split(",");
    private static final String[] OTM_SAM_LIST_13 = ConfigUtils.INSTANCE.getStringValue("otm_sam_13").split(",");
    private static String[] SAM_LIST = {""};
    @Autowired
    private ActivityManager activityManager;
    private LogFactory logFactory;
    private Logger logger = logFactory.getLogger(ActivityController.class);

    //Default course type OTM
    private String COURSE_TYPE = ActivityEventConstants.CARE_COURSE_TYPE_OTM;

    public void process(List<User> activeEnrolledUsers, Map<Long, User> studentSAMMapping, List<EnrollmentPojo> enrolmentInfos) throws VException, SQLException, ParseException {

        //6 to 9 grade
        logger.info("======SAM allocation for  OTM_SAM_6_9");
        SAM_LIST = OTM_SAM_LIST_6_9;
        List<User> firstGradeUsersBatch = activityManager.getFilteredUserByGradeRange(activeEnrolledUsers, 6, 9);
        assignSamBasedOnGrade(studentSAMMapping, enrolmentInfos, firstGradeUsersBatch);

        //10 grade
        SAM_LIST = OTM_SAM_LIST_10;
        logger.info("======SAM allocation for  OTM_SAM_10");
        firstGradeUsersBatch = activityManager.getFilteredUserByGradeRange(activeEnrolledUsers, 10, 10);
        assignSamBasedOnGrade(studentSAMMapping, enrolmentInfos, firstGradeUsersBatch);

        //11 grade
        SAM_LIST = OTM_SAM_LIST_11;
        logger.info("======SAM allocation for  OTM_SAM_11");
        firstGradeUsersBatch = activityManager.getFilteredUserByGradeRange(activeEnrolledUsers, 11, 11);
        assignSamBasedOnGrade(studentSAMMapping, enrolmentInfos, firstGradeUsersBatch);

        //12 grade
        SAM_LIST = OTM_SAM_LIST_12;
        logger.info("======SAM allocation for  OTM_SAM_12");
        firstGradeUsersBatch = activityManager.getFilteredUserByGradeRange(activeEnrolledUsers, 12, 12);
        assignSamBasedOnGrade(studentSAMMapping, enrolmentInfos, firstGradeUsersBatch);

        //13 grade
        SAM_LIST = OTM_SAM_LIST_13;
        logger.info("======SAM allocation for  OTM_SAM_13");
        firstGradeUsersBatch = activityManager.getFilteredUserByGradeRange(activeEnrolledUsers, 13, 13);
        assignSamBasedOnGrade(studentSAMMapping, enrolmentInfos, firstGradeUsersBatch);

        Long thruTime = Instant.now().toEpochMilli();

        Long fromTime = thruTime - ActivityEventConstants.MILLI_DAY * 1;
        List<CoursePlanBasicInfo> coursePlanBasicInfos = activityManager.getAllOtoRecords(fromTime, thruTime);

        List<Long> studentIds = coursePlanBasicInfos.parallelStream().map(coursePlanBasicInfo -> coursePlanBasicInfo.getStudentId()).collect(Collectors.toList());

        List<User> otousers = activityManager.getUsers(studentIds);
        COURSE_TYPE = ActivityEventConstants.CARE_COURSE_TYPE_OTO;
        SAM_LIST = OTO_SAM_LIST;
        assignSamBasedOnGrade(studentSAMMapping, null, otousers);
    }

    private void assignSamBasedOnGrade(Map<Long, User> studentSAMMapping, List<EnrollmentPojo> enrolmentInfos, List<User> firstGradeUsersBatch) {


        logger.info("======SAM allocation");

        int I = 0;
        for (User user : firstGradeUsersBatch) {

            String enrollmentId = "";
            if (enrolmentInfos != null) {

                Optional<EnrollmentPojo> enrollmentOptional = enrolmentInfos.parallelStream().filter(enrollmentPojo -> enrollmentPojo.getUserId().equalsIgnoreCase(user.getId().toString())).findFirst();

                enrollmentId = enrollmentOptional.isPresent() ? enrollmentOptional.get().getEnrollmentId() : "";
            }

            User sam = null;
            if (studentSAMMapping.get(user.getId()) != null) {
                sam = studentSAMMapping.get(user.getId());
            }

            String samEmail;
            String retained = "NO";
            if (sam != null) {
                samEmail = sam.getEmail();
                retained = "YES";
            } else {
                samEmail = SAM_LIST[I++];
            }


            Map<String, String> param = new HashMap<>();
            int grade = activityManager.getGrade(user.getStudentInfo().getGrade());
            param.put(ActivityEventConstants.MX_CUSTOM_1, COURSE_TYPE);
            param.put(ActivityEventConstants.NOTE, ActivityEventConstants.ACTIVITY_EVENT_CARE_SAM);
            param.put(ActivityEventConstants.MX_CUSTOM_2, String.valueOf(grade));
            param.put(ActivityEventConstants.MX_CUSTOM_3, retained);
            param.put(ActivityEventConstants.MX_CUSTOM_4, enrollmentId);
            param.put(ActivityEventConstants.MX_CUSTOM_5, "System Assigned");
            param.put(ActivityEventConstants.OWNER, samEmail);
            param.put(ActivityEventConstants.MX_SAM_OWNER, samEmail);

            if (I >= SAM_LIST.length)
                I = 0;

            activityManager.pushActivity(user, studentSAMMapping, ActivityEventConstants.EVENT_SAM_ALLOCATION, param);
        }
    }
}
