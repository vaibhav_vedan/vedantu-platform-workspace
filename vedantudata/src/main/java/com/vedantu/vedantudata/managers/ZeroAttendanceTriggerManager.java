package com.vedantu.vedantudata.managers;

import com.google.gson.Gson;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.User.User;
import com.vedantu.exception.VException;
import com.vedantu.exception.VExceptionFactory;
import com.vedantu.subscription.request.GTTAttendeeRequest;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.WebUtils;
import com.vedantu.vedantudata.controllers.ActivityController;
import com.vedantu.vedantudata.utils.ActivityEventConstants;
import com.vedantu.vedantudata.utils.LeadsquaredActivityHelper;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class ZeroAttendanceTriggerManager extends LeadsquaredActivityHelper {

    private static final String SCHEDULING_ENDPOINT = ConfigUtils.INSTANCE.getStringValue("SCHEDULING_ENDPOINT");
    Map<Long, User> studentAmMapping;
    @Autowired
    private ActivityManager activityManager;

    @Autowired
    private LogFactory logFactory;

    private Logger logger = logFactory.getLogger(ZeroAttendanceTriggerManager.class);

    public void process(List<User> activeEnrolledUsers, Map<Long, User> studentAmMapping) throws VException {

        if(activeEnrolledUsers.isEmpty())
            return;

        this.studentAmMapping = studentAmMapping;

        logger.info("====== precessing zero attendance for new users");
        processForNewUsers(activeEnrolledUsers);

        logger.info("====== precessing zero attendance for old users");
        processForoldUsers(activeEnrolledUsers);

    }

    private void processForoldUsers(List<User> activeEnrolledUsers) throws VException {

        Long now = new Date().getTime();
        Long fromTime = now - ActivityEventConstants.MILLI_DAY * 15;

        List<User> users = activeEnrolledUsers.parallelStream().filter(user -> user.getCreationTime() <= fromTime).collect(Collectors.toList());

        calculateAttendanceAndTriggerActivity(users, null);
    }

    private void calculateAttendanceAndTriggerActivity(List<User> users, Long fromTime) throws VException {


        List<String> userIds = users.parallelStream()
                .map(user -> user.getId().toString())
                .distinct()
                .collect(Collectors.toList());

        Long now = new Date().getTime();
        Long attendanceFromTime = null;

        if(fromTime!=null)
            attendanceFromTime = now - ActivityEventConstants.MILLI_WEEK;

        Map<String, Integer> gttAttendeeCountMap = getGttAttendeeCountMap(userIds, attendanceFromTime, new Date().getTime());

        for (User user : users) {

            logger.info("======Checking if student attended any session");
            if (gttAttendeeCountMap.get(user.getId().toString()) != null) {
                continue;
            }

            Map<String, String> param = new HashMap<>();
            if (fromTime != null)
                param.put(ActivityEventConstants.STUDENT_TYPE, ActivityEventConstants.STUDENT_TYPE_NEW);
            else
                param.put(ActivityEventConstants.STUDENT_TYPE, ActivityEventConstants.STUDENT_TYPE_REGULAR);

            param.put(ActivityEventConstants.NOTE, ActivityEventConstants.ACTIVITY_EVENT_ZERO_ATTENDANCE_NOTE);

            activityManager.pushActivity(user, studentAmMapping, ActivityEventConstants.EVENT_ZERO_ATTENDANCE, param);
        }

    }

    private void processForNewUsers(List<User> activeEnrolledUsers) throws VException {

        Long now = new Date().getTime();
        Long fromTime = now - ActivityEventConstants.MILLI_DAY * 15;
        Long thruTime = now - ActivityEventConstants.MILLI_WEEK;

        List<User> users = activeEnrolledUsers.parallelStream()
                .filter(user -> user.getCreationTime() >= fromTime && user.getCreationTime() <= thruTime)
                .collect(Collectors.toList());

        if(users.isEmpty())
            return;

        calculateAttendanceAndTriggerActivity(users, fromTime);
    }

    private Map<String, Integer> getGttAttendeeCountMap(List<String> activeUserIds, Long fromTime, Long thruTime) throws VException {

        GTTAttendeeRequest gttAttendeeRequest = new GTTAttendeeRequest();
        gttAttendeeRequest.setUserIds(activeUserIds);
        gttAttendeeRequest.setFromTime(fromTime);
        gttAttendeeRequest.setThruTime(thruTime);

        ClientResponse resp = WebUtils.INSTANCE.doCall(SCHEDULING_ENDPOINT + "/gttattendee/getAttendanceStatus", HttpMethod.POST,
                new Gson().toJson(gttAttendeeRequest), true);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);

        Map<String, Integer> gttAttendeeInfos = new Gson().fromJson(jsonString, Map.class);
        return gttAttendeeInfos;
    }
}
