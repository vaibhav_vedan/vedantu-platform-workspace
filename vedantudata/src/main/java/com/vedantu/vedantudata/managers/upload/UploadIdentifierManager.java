package com.vedantu.vedantudata.managers.upload;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import com.vedantu.vedantudata.dao.serializers.UploadIdentifierDAO;
import com.vedantu.vedantudata.entities.UploadIdentifier;

@Service
public class UploadIdentifierManager {

	@Autowired
	private UploadIdentifierDAO uploadIdentifierDAO;

	public void updateUploadIdentifier(UploadIdentifier uploadIdentifier) {
		uploadIdentifierDAO.create(uploadIdentifier);
	}

	public List<UploadIdentifier> getUploadIdentifier(String activityKey) {
		Query query = new Query();
		query.addCriteria(Criteria.where(UploadIdentifier.Constants.ACTIVITY_KEY).is(activityKey));
		return uploadIdentifierDAO.runQuery(query, UploadIdentifier.class);
	}
}
