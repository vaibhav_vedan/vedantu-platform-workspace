package com.vedantu.vedantudata.managers;

import com.google.gson.Gson;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.User.request.DemoTriggerCallToAgentReq;
import com.vedantu.exception.VException;
import com.vedantu.exception.VExceptionFactory;
import com.vedantu.notification.enums.CommunicationType;
import com.vedantu.notification.requests.EmailRequest;
import com.vedantu.notification.requests.TextSMSRequest;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.WebUtils;
import com.vedantu.util.enums.SQSMessageType;
import com.vedantu.util.enums.SQSQueue;
import com.vedantu.vedantudata.utils.AwsSQSManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

import java.util.Objects;

@Service
public class CommunicationManager {

    @Autowired
    private LogFactory logFactory;

    @Autowired
    private AwsSQSManager awsSQSManager;

    private Gson gson = new Gson();
    private Logger logger = logFactory.getLogger(CommunicationManager.class);

    public String sendSMSViaRest(TextSMSRequest request) throws VException {
        if(StringUtils.isEmpty(request.getTo())) {
            logger.warn("To field is empty: "+ request);
            return null;
        }
        String notificationEndpoint = ConfigUtils.INSTANCE.getStringValue("NOTIFICATION_ENDPOINT");
        ClientResponse resp = WebUtils.INSTANCE.doCall(notificationEndpoint + "/SMS/sendSMS", HttpMethod.POST,
                new Gson().toJson(request));
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        logger.info("Response from notification-centre " + jsonString);
        return jsonString;
    }

    public String callAgent(DemoTriggerCallToAgentReq request) throws VException {

        String notificationEndpoint = ConfigUtils.INSTANCE.getStringValue("PLATFORM_ENDPOINT");
        ClientResponse resp = WebUtils.INSTANCE.doCall(notificationEndpoint + "/vedantudata/trigger-call-to-agent", HttpMethod.POST,
                new Gson().toJson(request));
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        logger.info("Response from platform " + jsonString);
        return jsonString;
    }

    public String sendEmail(EmailRequest request) throws VException {
        logger.info("Inside sendEmail - {} ", gson.toJson(request));
        if (Objects.nonNull(request.getType()) && request.getType().equals(CommunicationType.OTF_POST_SESSION_TO_STUDENT_NEW_WAVE)) {
            awsSQSManager.sendToSQS(SQSQueue.POST_SESSION_SUMMARY_EMAILS, SQSMessageType.OTF_SESSION_SUMMARY_EMAIL, gson.toJson(request));
        } else {
            awsSQSManager.sendToSQS(SQSQueue.NOTIFICATION_QUEUE, SQSMessageType.SEND_EMAIL, gson.toJson(request));
        }return null;
    }
}
