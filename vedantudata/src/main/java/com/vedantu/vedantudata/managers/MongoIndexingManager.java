package com.vedantu.vedantudata.managers;

import com.google.gson.Gson;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.aws.pojo.AWSSNSSubscriptionRequest;
import com.vedantu.util.LogFactory;
import com.vedantu.util.PlatformBasicResponse;
import com.vedantu.util.WebUtils;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import com.vedantu.vedantudata.dao.LeadAutoAssignmentDAO;
import com.vedantu.vedantudata.dao.serializers.MongoClientFactory;
import com.vedantu.vedantudata.entities.leadsquared.AssignedLeads;
import com.vedantu.vedantudata.entities.leadsquared.LeadActualAssignment;
import com.vedantu.vedantudata.entities.salesconversion.UserSessionDetails;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

@Service
public class MongoIndexingManager extends AbstractMongoDAO {

    @Autowired
    private MongoClientFactory mongoClientFactory;

    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(MongoIndexingManager.class);


    public PlatformBasicResponse createIndexWithTTL(String entityName, String fieldName, Long expireTimeInSeconds) {
        Long startTime = System.currentTimeMillis();
        logger.info("creating Index for - {} ", entityName);
        createIndex(entityName,fieldName,expireTimeInSeconds);
        logger.info("creating Index is done. time taken - {} ", (System.currentTimeMillis() - startTime));
        return new PlatformBasicResponse();
    }

    public PlatformBasicResponse droppingIndex(String entityName, String fieldName) {
        Long startTime = System.currentTimeMillis();
        logger.info("dropping index for - {}" , entityName);
        dropIndex(entityName,fieldName);
        logger.info("dropping Index is done. Time taken - {} ", (System.currentTimeMillis() - startTime));
        return new PlatformBasicResponse();
    }

    public void createIndexWithTTLFromCron(String request, String messageType, String entityName, String fieldName, Long expireTimeInSeconds) {
        Gson gson = new Gson();
        logger.info("params - {},{},{}",entityName,fieldName,expireTimeInSeconds);
        AWSSNSSubscriptionRequest subscriptionRequest = gson.fromJson(request, AWSSNSSubscriptionRequest.class);
        if (messageType.equals("SubscriptionConfirmation")) {
            String json = null;
            logger.info("SubscribeURL = " + subscriptionRequest.getSubscribeURL());
            ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionRequest.getSubscribeURL(), HttpMethod.GET, json, false);
            logger.info(resp.getEntity(String.class));
        } else if (messageType.equals("Notification")) {
            logger.info("Notification received - SNS");
            logger.info(subscriptionRequest.toString());
            createIndexWithTTL(entityName,fieldName,expireTimeInSeconds);
        }
    }
    public void droppingIndexFromCron(String request,String messageType,String entityName,String fieldName) {
        Gson gson = new Gson();
        logger.info("params - {},{},{}",entityName,fieldName);
        AWSSNSSubscriptionRequest subscriptionRequest = gson.fromJson(request, AWSSNSSubscriptionRequest.class);
        if (messageType.equals("SubscriptionConfirmation")) {
            String json = null;
            logger.info("SubscribeURL = " + subscriptionRequest.getSubscribeURL());
            ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionRequest.getSubscribeURL(), HttpMethod.GET, json, false);
            logger.info(resp.getEntity(String.class));
        } else if (messageType.equals("Notification")) {
            logger.info("Notification received - SNS");
            logger.info(subscriptionRequest.toString());
            droppingIndex(entityName,fieldName);
        }
    }
}
