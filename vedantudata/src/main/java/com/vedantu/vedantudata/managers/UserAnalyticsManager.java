package com.vedantu.vedantudata.managers;

import java.util.List;

import org.apache.logging.log4j.Logger;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.util.CollectionUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.WebUtils;
import com.vedantu.vedantudata.dao.serializers.LeadsquaredDAO;
import com.vedantu.vedantudata.entities.analytics.UserProfileData;
import com.vedantu.vedantudata.entities.leadsquared.LeadDetails;

@Service
public class UserAnalyticsManager {

	@Autowired
	private LeadsquaredDAO leadsquaredDAO;

	@Autowired
	private LogFactory logFactory;

	@SuppressWarnings("static-access")
	private Logger logger = logFactory.getLogger(UserAnalyticsManager.class);

	private static Gson gson = new Gson();

	private static final String USER_ANALYTICS_BASE_URL = "https://analytics.vedantu.com";
	private static final String USER_ANALYTICS_GET_USER_INFO_URL = USER_ANALYTICS_BASE_URL + "/getUserInfo";

	public UserProfileData getUserProfileDetailsById(String userId) {
		String serverUrl = USER_ANALYTICS_GET_USER_INFO_URL + "?userId=" + userId;
		ClientResponse cRes = WebUtils.INSTANCE.doCall(serverUrl, HttpMethod.GET, null);
		logger.info("cRes:" + cRes.toString());
		if (cRes.getStatus() != 200) {
			logger.error("InvalidStatusCodeGetUsers - get lead users not 200 - " + cRes.getStatus() + " cRes:"
					+ cRes.toString());
		}

		String response = cRes.getEntity(String.class);
		logger.info("Response:" + response);

		JSONObject jsonObject = new JSONObject(response);
		if (jsonObject.isNull("errorCode") || StringUtils.isEmpty(jsonObject.getString("errorCode"))
				|| jsonObject.isNull("result")) {
			logger.error("Error fetching user info : " + jsonObject.getString("errorCode") + " userId:" + userId);
		}

		JSONObject result = jsonObject.getJSONObject("result");
		UserProfileData userProfileData = gson.fromJson(result.toString(), UserProfileData.class);
		logger.info("user profile data:" + userProfileData.toString());
		return userProfileData;
	}

	public String updateUserProfileData(long startTime, long endTime) {
		long recordsUpdated = 0;
		Query query = new Query();
		query.addCriteria(Criteria.where(LeadDetails.Constants.VEDANTU_USER_ID).exists(true));
		query.addCriteria(Criteria.where(LeadDetails.Constants.LAST_UPDATED).gte(startTime).lte(endTime));

		long recordCount = leadsquaredDAO.queryCount(query, LeadDetails.class);
		for (int i = 0; i < recordCount; i += 1000) {
			query.skip(0);
			query.limit(1000);
			List<LeadDetails> results = leadsquaredDAO.runQuery(query, LeadDetails.class);
			if (!CollectionUtils.isEmpty(results)) {
				for (LeadDetails result : results) {
					try {
						UserProfileData userProfileData = getUserProfileDetailsById(result.getMx_User_Id());
						logger.info("user profile data :" + userProfileData.toString());
						result.updateUserProfileData(userProfileData);
						leadsquaredDAO.createLead(result);
					} catch (Exception ex) {
						logger.error("Error occured:" + result.toString());
					}
				}
			}
		}

		return "RecordsUpdated:" + recordsUpdated;
	}
}
