package com.vedantu.vedantudata.managers;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.opencsv.CSVReader;
import com.opencsv.CSVWriter;
import com.sun.jersey.api.client.ClientHandlerException;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.UniformInterfaceException;
import com.vedantu.User.User;
import com.vedantu.User.request.AddUserAddressReq;
import com.vedantu.async.AsyncTaskFactory;
import com.vedantu.async.AsyncTaskParams;
import com.vedantu.dinero.enums.PaymentInvoiceType;
import com.vedantu.dinero.pojo.BaseInstalmentInfo;
import com.vedantu.dinero.pojo.Orders;
import com.vedantu.dinero.pojo.SalesTransactionInfo;
import com.vedantu.dinero.request.AddAccountPancardReq;
import com.vedantu.dinero.request.AddPaymentInvoiceReq;
import com.vedantu.dinero.request.GetOrdersReq;
import com.vedantu.dinero.request.SalesTransactionReq;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.VException;
import com.vedantu.exception.VExceptionFactory;
import com.vedantu.lms.cmds.pojo.TestAndAttemptDetails;
import com.vedantu.notification.requests.SMSPriorityType;
import com.vedantu.notification.requests.TextSMSRequest;
import com.vedantu.onetofew.enums.CourseTerm;
import com.vedantu.onetofew.enums.SessionState;
import com.vedantu.onetofew.pojo.BatchInfo;
import com.vedantu.onetofew.pojo.CourseInfo;
import com.vedantu.session.pojo.OTFSessionPojoUtils;
import com.vedantu.subscription.enums.EnrollmentType;
import com.vedantu.subscription.enums.bundle.BundleState;
import com.vedantu.subscription.enums.bundle.CourseType;
import com.vedantu.subscription.enums.bundle.PackageType;
import com.vedantu.subscription.pojo.BundleInfo;
import com.vedantu.subscription.pojo.TestBundleContentInfo;
import com.vedantu.subscription.pojo.TestContentData;
import com.vedantu.subscription.pojo.VideoContentData;
import com.vedantu.subscription.pojo.bundle.BatchDetails;
import com.vedantu.subscription.pojo.bundle.BundleDetails;
import com.vedantu.subscription.pojo.bundle.Classes;
import com.vedantu.subscription.request.AddEditBundleReq;
import com.vedantu.subscription.request.BundleDetailsUpdateReq;
import com.vedantu.subscription.request.BundleEntityRequest;
import com.vedantu.subscription.response.BundleEnrolmentInfo;
import com.vedantu.subscription.response.CoursePlanInfo;
import com.vedantu.util.*;
import com.vedantu.util.enums.SortType;
import com.vedantu.util.scheduling.CommonCalendarUtils;
import com.vedantu.vedantudata.async.AsyncTaskName;
import com.vedantu.vedantudata.dao.serializers.LeadsquaredDAO;
import com.vedantu.vedantudata.dao.serializers.WebinarInfoDAO;
import com.vedantu.vedantudata.dao.serializers.WebinarUserRegistrationInfoDAO;
import com.vedantu.vedantudata.entities.WebinarInfo;
import com.vedantu.vedantudata.entities.WebinarUserRegistrationInfo;
import com.vedantu.vedantudata.entities.leadsquared.LeadActivity;
import com.vedantu.vedantudata.enums.WebinarUserTag;
import com.vedantu.vedantudata.googlesheets.GTTAttendeeSheetManager;
import com.vedantu.vedantudata.googlesheets.OrdersGoogleSheetManager;
import com.vedantu.vedantudata.googlesheets.WebinarReportGoogleSheetManager;
import com.vedantu.vedantudata.managers.leadsquared.LeadsquaredManager;
import com.vedantu.vedantudata.managers.upload.CSVManager;
import com.vedantu.vedantudata.pojos.VedantuDataOrder;
import com.vedantu.vedantudata.pojos.WebinarReport;
import org.apache.logging.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.http.HttpMethod;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.PostConstruct;
import java.io.*;
import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
public class VedantuManager {

    @Autowired
    private WebinarUserRegistrationInfoDAO webinarUserRegistrationInfoDAO;

    @Autowired
    private WebinarInfoDAO webinarInfoDAO;

    @Autowired
    private LeadsquaredDAO leadsquaredDAO;

    @Autowired
    private LeadsquaredManager leadsquaredManager;

    @Autowired
    private UserEventManager userEventManager;

    @Autowired
    private FosUtils fosUtils;

    @Autowired
    private OrdersGoogleSheetManager ordersGoogleSheetManager;

    @Autowired
    private GTTAttendeeSheetManager gttAttendeeSheetManager;

    @Autowired
    private WebinarReportGoogleSheetManager webinarReportGoogleSheetManager;

    @Autowired
    private GTWManager gtwManager;

    @Autowired
    private CSVManager csvManager;

    @Autowired
    private LogFactory logFactory;

    @Autowired
    private AsyncTaskFactory asyncTaskFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(VedantuManager.class);

    private String DINERO_ENDPOINT;
    private String SUBSCRIPTION_ENDPOINT;
    private String OTF_ENDPOINT;
    private String LMS_ENDPOINT;
    private String USER_ENDPOINT;
    private String NOTIFICATION_ENDPOINT;
    private String PLATFORM_ENDPOINT;
    private static final String URL_SHORTNER_API_KEY = ConfigUtils.INSTANCE.getStringValue("google.url.shortner.api.key");

    private static Gson gson = new Gson();

    private static final long ORDER_SYNC_DELAY_HOURLY = DateTimeUtils.MILLIS_PER_HOUR;

    private static final Type ORDERS_LIST_TYPE = new TypeToken<List<Orders>>() {
    }.getType();

    private static final Type OTF_SESSION_POJO_LIST_TYPE = new TypeToken<List<OTFSessionPojoUtils>>() {
    }.getType();

    private static final Type SALES_TRANSACTION_LIST_TYPE = new TypeToken<ArrayList<SalesTransactionInfo>>() {
    }.getType();

    private static final Type BUNDLE_LIST_TYPE = new TypeToken<ArrayList<BundleInfo>>() {
    }.getType();

    private static final Type BUNDLE_ENROLLMENT_LIST_TYPE = new TypeToken<ArrayList<BundleEnrolmentInfo>>() {
    }.getType();

    private static final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
    private static final SimpleDateFormat sdfWebinarReg = new SimpleDateFormat("hh:mma, dd MMM yyyy");
    public static final String TIME_ZONE_IN = "Asia/Kolkata";

    @PostConstruct
    public void init() {
        DINERO_ENDPOINT = ConfigUtils.INSTANCE.getStringValue("DINERO_ENDPOINT");
        OTF_ENDPOINT = ConfigUtils.INSTANCE.getStringValue("OTF_ENDPOINT");
        SUBSCRIPTION_ENDPOINT = ConfigUtils.INSTANCE.getStringValue("SUBSCRIPTION_ENDPOINT");
        LMS_ENDPOINT = ConfigUtils.INSTANCE.getStringValue("LMS_ENDPOINT");
        USER_ENDPOINT = ConfigUtils.INSTANCE.getStringValue("USER_ENDPOINT");
        NOTIFICATION_ENDPOINT = ConfigUtils.INSTANCE.getStringValue("NOTIFICATION_ENDPOINT");
        PLATFORM_ENDPOINT = ConfigUtils.INSTANCE.getStringValue("PLATFORM_ENDPOINT");
    }

//    public List<VedantuDataOrder> generateOrderSheets(long startTime, long endTime, Boolean fetchAgent) {
//        logger.info("Request:" + startTime + " endTime:" + endTime);
//        List<Orders> orders = getAllOrders(startTime, endTime);
//        List<VedantuDataOrder> vedantuOrders = getVedantuDataOrders(orders, Boolean.TRUE.equals(fetchAgent));
//        userEventManager.updateOrderData(vedantuOrders);
//
//        // Update sheet
//        try {
//            ordersGoogleSheetManager.updateOrders(vedantuOrders, startTime, endTime);
//        } catch (Exception ex) {
//            logger.error("Error : " + ex.toString() + " messagE:" + ex.getMessage());
//        }
//        return vedantuOrders;
//    }

//    public List<VedantuDataOrder> updateOrderSheetHourly() {
//        long time = System.currentTimeMillis();
//        time -= (time % DateTimeUtils.MILLIS_PER_MINUTE);
//        time -= ORDER_SYNC_DELAY_HOURLY;
//
//        List<Orders> orders = getAllOrders(time - DateTimeUtils.MILLIS_PER_HOUR, time);
//        List<VedantuDataOrder> vedantuOrders = getVedantuDataOrders(orders, true);
//        userEventManager.updateOrderData(vedantuOrders);
//
//        // Update sheet
//        try {
//            ordersGoogleSheetManager.updateOrders(vedantuOrders);
//        } catch (Exception ex) {
//            logger.error("Error : " + ex.toString() + " messagE:" + ex.getMessage());
//        }
//        return vedantuOrders;
//    }

    private List<Orders> getAllOrders(long startTime, long endTime) {
        List<Orders> orders = new ArrayList<Orders>();
        int start = 0;
        int size = 20;
        while (true) {
            List<Orders> results = getOrders(startTime, endTime, start, size);
            if (!CollectionUtils.isEmpty(results)) {
                orders.addAll(results);
            } else {
                break;
            }
            start += size;
        }
        return orders;
    }

    private List<Orders> getOrders(long startTime, long endTime, int start, int size) {
        logger.info("Request:" + startTime + " endTime:" + endTime + " start:" + start + " size:" + size);
        GetOrdersReq ordersReq = new GetOrdersReq();
        ordersReq.setStart(start);
        ordersReq.setSize(start + size);
        ordersReq.setStartTime(startTime);
        ordersReq.setEndTime(endTime);
        ordersReq.setOrderSortType(SortType.LAST_UPDATED_DESC);
        ClientResponse resp = WebUtils.INSTANCE.doCall(DINERO_ENDPOINT + "/payment/getOrders", HttpMethod.POST,
                new Gson().toJson(ordersReq));
        String response = resp.getEntity(String.class);
        List<Orders> results = gson.fromJson(response, ORDERS_LIST_TYPE);
        logger.info("ResponsE:" + resp.getStatus() + " resp:" + response);
        return results;
    }

//    public List<OTFSessionPojoUtils> otfAttendeeReportDaily() throws Exception {
//        long currentTime = System.currentTimeMillis();
//        long startTime = getDayStartTimeIST(currentTime) - DateTimeUtils.MILLIS_PER_DAY;
//        long endTime = getDayStartTimeIST(currentTime);
//        return otfAttendeeReport(startTime, endTime);
//    }

//    public List<OTFSessionPojoUtils> otfAttendeeReport(long startTime, long endTime) throws Exception {
//        String getSessionsUrl =  OTF_ENDPOINT+ "/onetofew/session/getUpcomingSessionsWithAttendeeInfos?startTime=" + startTime
//                + "&endTime=" + endTime;
//        ClientResponse resp = WebUtils.INSTANCE.doCall(getSessionsUrl, HttpMethod.GET, null);
//        String response = resp.getEntity(String.class);
//        List<OTFSessionPojoUtils> results = gson.fromJson(response, OTF_SESSION_POJO_LIST_TYPE);
//        logger.info("ResponsE:" + resp.getStatus() + " resp:" + response);
//
//        List<GTTAttendeeInfo> attendeeInfos = getGTTAttendeeInfos(results);
//        updateUserInfos(attendeeInfos);
//        gttAttendeeSheetManager.updateGTTAttendees(attendeeInfos, startTime, endTime);
//        return results;
//    }

    public String searchLeadsByActivity(String searchString, Long start, Long end) {
        // db.getCollection('LeadActivity').find({"eventCode":"206",
        // "data.2.value":{"$regex":"tution"}})
        String result = "No entries found";
        if (StringUtils.isEmpty(searchString)) {
            return result;
        }

        String[] searchArr = searchString.split(",");
        Set<String> leadIds = new HashSet<>();
        for (String entry : searchArr) {
            Query query = new Query();
            query.addCriteria(Criteria.where(LeadActivity.Constants.EVENT_CODE).is("206"));
            query.addCriteria(Criteria.where("data.2.value").regex(entry, "i"));

            query.fields().include(LeadActivity.Constants.LEAD_ID);
            List<LeadActivity> activities = leadsquaredDAO.runQuery(query, LeadActivity.class);
            if (!CollectionUtils.isEmpty(activities)) {
                for (LeadActivity leadActivity : activities) {
                    leadIds.add(leadActivity.getRelatedProspectId());
                }
            }
        }

        if (!CollectionUtils.isEmpty(leadIds)) {
            result = Arrays.toString(leadIds.toArray());
        }

        return result;
    }

//
//    private List<VedantuDataOrder> getVedantuDataOrders(List<Orders> orders, boolean fetchAgent) {
//        logger.info("creating vedantu data order");
//        List<VedantuDataOrder> results = new ArrayList<VedantuDataOrder>();
//        if (!CollectionUtils.isEmpty(orders)) {
//            logger.info("Request size:" + orders.size());
//            Set<Long> userIds = new HashSet<>();
//            for (Orders order : orders) {
//                userIds.add(order.getUserId());
//            }
//
//            Map<Long, UserBasicInfo> userInfoMap = fosUtils.getUserBasicInfosMapFromLongIds(userIds);
//            Map<Long, String> userOrderDataMap = new HashMap<>();
//
//            logger.info("User info size:" + userInfoMap.size());
//            for (Orders order : orders) {
//                // Filter out unnecessary orders
//                if (PaymentStatus.FORFEITED.equals(order.getPaymentStatus())
//                        || PaymentStatus.NOT_PAID.equals(order.getPaymentStatus())) {
//                    continue;
//                }
//
//                // Create vedantu data from user and order pojos
//                UserBasicInfo userBasicInfo = userInfoMap.get(order.getUserId());
//                VedantuDataOrder vedantuDataOrder = new VedantuDataOrder(order, userBasicInfo, 0);
//                if ("PLAN".equals(vedantuDataOrder.getEntityType())) {
//                    continue;
//                }
//
//                // Update lead agent name
//                try {
//                    if (fetchAgent) {
//                        LeadDetails leadDetails = leadsquaredManager.fetchLeadsByVedantuUserId(userBasicInfo.getEmail(),
//                                String.valueOf(userBasicInfo.getUserId()));
//                        if (leadDetails != null) {
//                            vedantuDataOrder.setAgentName(leadDetails.getOwnerIdName());
//                        }
//                    }
//                } catch (Exception ex) {
//                    logger.info("ex:" + ex.toString());
//                }
//
//                /*
//				 * private long gmvPotential;
//                 */
//                // Update the transaction details
//                long totalAmount = 0;
//                Long firstTransactionTime = null;
//                if (!userOrderDataMap.containsKey(order.getUserId())) {
//                    List<SalesTransactionInfo> transactions = getAllExternalTransactions(userBasicInfo.getUserId());
//                    if (!CollectionUtils.isEmpty(transactions)) {
//                        for (SalesTransactionInfo salesTransactionInfo : transactions) {
//                            totalAmount += salesTransactionInfo.amount;
//                            if (firstTransactionTime == null
//                                    || firstTransactionTime > salesTransactionInfo.getCreationTime()) {
//                                firstTransactionTime = salesTransactionInfo.getCreationTime();
//                            }
//                        }
//                    }
//                } else {
//                    String orderValue = userOrderDataMap.get(order.getUserId());
//                    if (!StringUtils.isEmpty(orderValue)) {
//                        try {
//                            String[] values = orderValue.split("_");
//                            if (!StringUtils.isEmpty(values[0]) && !StringUtils.isEmpty(values[1])) {
//                                long value1 = Long.parseLong(values[0]);
//                                long value2 = Long.parseLong(values[1]);
//                                if (value1 > 0 && value2 > 0) {
//                                    totalAmount = value1;
//                                    firstTransactionTime = value2;
//                                }
//                            }
//                        } catch (Exception ex) {
//                            logger.error("ex:" + ex.toString());
//                        }
//                    }
//                }
//                vedantuDataOrder.setTotalAmountPaid(totalAmount / 100);
//                if (firstTransactionTime != null) {
//                    vedantuDataOrder.setFirstTranasactionTime(CommonUtils.calculateTimeStampIst(firstTransactionTime));
//                }
//
//                userOrderDataMap.put(order.getUserId(), getOrderDateValue(totalAmount, firstTransactionTime));
//                // Update the gmv potential
//                try {
//                    //commented by ajith
////                    updateGMVPotential(vedantuDataOrder);
//                } catch (Exception ex) {
//                    logger.info("Error");
//                }
//
//                results.add(vedantuDataOrder);
//            }
//        }
//
//        logger.info("Response size:" + results.size());
//        return results;
//    }

    
    private List<SalesTransactionInfo> getAllExternalTransactions(long userId) {
        SalesTransactionReq salesTransactionReq = new SalesTransactionReq();
        salesTransactionReq.setStart(0);
        salesTransactionReq.setLimit(1000);
        salesTransactionReq.setUserId(userId);
        salesTransactionReq.setReason("RECHARGE");
        ClientResponse resp = WebUtils.INSTANCE.doCall(DINERO_ENDPOINT + "/account/getTransactions", HttpMethod.POST,
                gson.toJson(salesTransactionReq));
        String response = resp.getEntity(String.class);
        return gson.fromJson(response, SALES_TRANSACTION_LIST_TYPE);
    }

    private void updateGMVPotential(VedantuDataOrder vedantuDataOrder) {
        // TODO : write this
        /*
		 * COURSE_PLAN, price COURSE_PLAN_REGISTRATION; price OTF, purchasePrice - batch
		 * OTF_COURSE_REGISTRATION, start price - course OTF_BATCH_REGISTRATION,
		 * purchasePrice - batch
		 * 
         */
        if (!StringUtils.isEmpty(vedantuDataOrder.getEntityType())
                && !StringUtils.isEmpty(vedantuDataOrder.getEntityId())) {
            switch (vedantuDataOrder.getEntityType()) {
                case "COURSE_PLAN":
                case "COURSE_PLAN_REGISTRATION":
                    // http://54.255.141.229/subscription/courseplan/59031191e4b019ce35b8db33
                    ClientResponse respCourse = WebUtils.INSTANCE.doCall(
                            SUBSCRIPTION_ENDPOINT + "/courseplan/" + vedantuDataOrder.getEntityId(), HttpMethod.GET, null);
                    String jsonRespCourse = respCourse.getEntity(String.class);
                    CoursePlanInfo coursePlanInfo = gson.fromJson(jsonRespCourse, CoursePlanInfo.class);
                    if (coursePlanInfo.getPrice() != null) {
                        vedantuDataOrder.setGmvPotential(coursePlanInfo.getPrice() / 100);
                    }
                    break;
                case "OTF_COURSE_REGISTRATION":
                    // http://54.169.137.222/onetofew/course/57a2d706162811e2df1532b3
                    ClientResponse respOTF = WebUtils.INSTANCE
                            .doCall(OTF_ENDPOINT + "/course/" + vedantuDataOrder.getEntityId(), HttpMethod.GET, null);
                    String jsonRespOTF = respOTF.getEntity(String.class);
                    CourseInfo courseInfo = gson.fromJson(jsonRespOTF, CourseInfo.class);
                    vedantuDataOrder.setGmvPotential(courseInfo.getStartPrice() / 100);
                    break;
                case "OTF":
                case "OTF_BATCH_REGISTRATION":
                    ClientResponse respOTFBatch = WebUtils.INSTANCE
                            .doCall(OTF_ENDPOINT + "/batch/" + vedantuDataOrder.getEntityId(), HttpMethod.GET, null);
                    String jsonRespOTFBatch = respOTFBatch.getEntity(String.class);
                    BatchInfo batchInfo = gson.fromJson(jsonRespOTFBatch, BatchInfo.class);
                    vedantuDataOrder.setGmvPotential(batchInfo.getPurchasePrice() / 100);
                    break;
                default:
                    break;
            }
        }
    }

    public String webinarTrackExport(long startTime, long endTime) throws Exception {
        logger.info("Request:" + startTime + " endTime:" + endTime);
        // https://qa1-platform.vedantu.com/subscription/bundle/get?callingUserId=4670056944631808&size=25&start=0&state=ACTIVE&states=ACTIVE

        List<WebinarReport> webinarReports = new ArrayList<>();

        // Fetch bundle tracks
        List<BundleInfo> bundleInfos = getAllBundleInfos();

        // Fetch enrollments
        if (!CollectionUtils.isEmpty(bundleInfos)) {
            for (BundleInfo bundleInfo : bundleInfos) {
                List<BundleEnrolmentInfo> bundleEnrolmentInfos = getBundleEnrollments(bundleInfo.getId());
                if (CollectionUtils.isEmpty(bundleEnrolmentInfos)) {
                    continue;
                }

                List<OTFSessionPojoUtils> webinars = getBundleWebinars(bundleInfo.getId(), startTime, endTime);
                if (CollectionUtils.isEmpty(webinars)) {
                    continue;
                }

                webinars.removeIf(w -> !SessionState.SCHEDULED.equals(w.getState()));
                for (BundleEnrolmentInfo bundleEnrolmentInfo : bundleEnrolmentInfos) {
                    WebinarReport webinarReport = new WebinarReport();
                    webinarReport.setUserId(bundleEnrolmentInfo.getUserId());
                    webinarReport.setBundleId(bundleEnrolmentInfo.getBundleId());

                    // Populate session data
                    webinarReport.setSessionsScheduled(webinars.size());
                    int sessionCount = 0;
                    for (OTFSessionPojoUtils w : webinars) {
                        if (!CollectionUtils.isEmpty(w.getAttendeeInfos())) {
                            sessionCount += w.getAttendeeInfos().stream()
                                    .filter(wa -> (bundleEnrolmentInfo.getUserId()
                                    .equals(String.valueOf(wa.getUserId())) && wa.getTimeInSession() > 0))
                                    .count();
                        }
                    }
                    webinarReport.setSessionsAttended(sessionCount);

                    // Populate content related data
                    Map<String, TestAndAttemptDetails> attemptDetails = getTestAttemptDetails(bundleInfo.getId(),
                            bundleEnrolmentInfo.getUserId());
                    if (attemptDetails != null && attemptDetails.size() > 0) {
                        webinarReport.setTestsShared(attemptDetails.size());
                        int attemptCount = 0;
                        for (TestAndAttemptDetails test : attemptDetails.values()) {
                            if (!CollectionUtils.isEmpty(test.getAttemptDetails())) {
                                attemptCount++;
                            }
                        }

                        webinarReport.setTestsAttempted(attemptCount);
                    }
                    webinarReports.add(webinarReport);
                }
            }
        }

        webinarReportGoogleSheetManager.updateWebinarEntries(webinarReports, startTime, endTime);
        return "";
    }

    public List<BundleInfo> getAllBundleInfos() {
        List<BundleInfo> bundleInfos = new ArrayList<>();
        int start = 0;
        int size = 50;
        while (true) {
            ClientResponse bundleListResponseJSON = WebUtils.INSTANCE.doCall(SUBSCRIPTION_ENDPOINT + "/bundle/get?size="
                    + size + "&start=" + start + "&state=ACTIVE&states=ACTIVE", HttpMethod.GET, null);
            String jsonRespOTFBatch = bundleListResponseJSON.getEntity(String.class);
            List<BundleInfo> results = gson.fromJson(jsonRespOTFBatch, BUNDLE_LIST_TYPE);
            if (CollectionUtils.isEmpty(results)) {
                break;
            } else {
                bundleInfos.addAll(results);
            }
            start += size;
        }

        return bundleInfos;
    }

    public List<BundleEnrolmentInfo> getBundleEnrollments(String bundleId) {
        // https://qa1-platform.vedantu.com/subscription/bundle/getEnrolments?status=ACTIVE&bundleId=5916f5e460b2259a5a03fb95
        List<BundleEnrolmentInfo> bundleEnrollementInfos = new ArrayList<>();
        while (true) {
            ClientResponse bundleListResponseJSON = WebUtils.INSTANCE.doCall(
                    SUBSCRIPTION_ENDPOINT + "/bundle/getEnrolments?status=ACTIVE&bundleId=" + bundleId, HttpMethod.GET,
                    null);
            String jsonRespOTFBatch = bundleListResponseJSON.getEntity(String.class);
            List<BundleEnrolmentInfo> results = gson.fromJson(jsonRespOTFBatch, BUNDLE_ENROLLMENT_LIST_TYPE);
            if (CollectionUtils.isEmpty(results)) {
            } else {
                bundleEnrollementInfos.addAll(results);
            }
            break;
        }

        return bundleEnrollementInfos;

    }

    public List<OTFSessionPojoUtils> getBundleWebinars(String bundleId, long startTime, long endTime) {
        // https://qa1-platform.vedantu.com/subscription/bundle/getEnrolments?status=ACTIVE&bundleId=5916f5e460b2259a5a03fb95
        List<OTFSessionPojoUtils> sessions = new ArrayList<>();
        int start = 0;
        int size = 50;
        while (true) {
            ClientResponse bundleListResponseJSON = WebUtils.INSTANCE.doCall(OTF_ENDPOINT + "/session/getPastSessions?"
                    + size + "&start=" + start + "&sessionContextType=WEBINAR&sessionContextId=" + bundleId
                    + "&startTime=" + startTime + "&endTime=" + endTime, HttpMethod.GET, null);
            String jsonRespOTFBatch = bundleListResponseJSON.getEntity(String.class);
            List<OTFSessionPojoUtils> results = gson.fromJson(jsonRespOTFBatch, OTF_SESSION_POJO_LIST_TYPE);
            if (CollectionUtils.isEmpty(results)) {
                break;
            } else {
                sessions.addAll(results);
            }
            start += size;
        }

        return sessions;

    }

    public Map<String, TestAndAttemptDetails> getTestAttemptDetails(String bundleId, String userId) throws VException {
        String url = SUBSCRIPTION_ENDPOINT + "/bundle/" + bundleId + "?returnContentTypes=TEST";
        ClientResponse resp = WebUtils.INSTANCE.doCall(url, HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        BundleInfo bundleInfo = new Gson().fromJson(jsonString, BundleInfo.class);
        List<String> testIds = new ArrayList<>();
        if (bundleInfo != null && bundleInfo.getTests() != null) {
            if (CollectionUtils.isNotEmpty(bundleInfo.getTests().getChildren())) {
                for (TestContentData testContentData : bundleInfo.getTests().getChildren()) {
                    if (testContentData != null && CollectionUtils.isNotEmpty(testContentData.getContents())) {
                        for (TestBundleContentInfo testBundleContentInfo : testContentData.getContents()) {
                            if (testBundleContentInfo != null && testBundleContentInfo.getContentId() != null) {
                                testIds.add(testBundleContentInfo.getContentId());
                            }
                        }
                    }
                }
            }
        }

        Map<String, TestAndAttemptDetails> response = new HashMap<>();
        if (CollectionUtils.isNotEmpty(testIds)) {
            String testUrl = LMS_ENDPOINT + "/cmds/test/getTestAndAttemptDetails?bundleId=" + bundleId + "&userId="
                    + userId;
            for (String test : testIds) {
                testUrl += "&testIds=" + test;
            }

            ClientResponse testResp = WebUtils.INSTANCE.doCall(testUrl, HttpMethod.GET, null, true);
            VExceptionFactory.INSTANCE.parseAndThrowException(resp);
            String testJsonString = testResp.getEntity(String.class);
            Type listType1 = new TypeToken<Map<String, TestAndAttemptDetails>>() {
            }.getType();
            response = new Gson().fromJson(testJsonString, listType1);
        }
        return response;
    }

    public void registerWebinar(WebinarUserRegistrationInfo webinarUserRegistrationInfo) throws VException {
        
         ClientResponse resp = WebUtils.INSTANCE.doCall(PLATFORM_ENDPOINT + "/cms/webinar/register", HttpMethod.POST,
                new Gson().toJson(webinarUserRegistrationInfo));
         VExceptionFactory.INSTANCE.parseAndThrowException(resp);

//        String trainingId = webinarUserRegistrationInfo.getTrainingId();
//        if (StringUtils.isEmpty(trainingId)) {
//            return;
//        }
//        String[] trainingIds = trainingId.split(",");
//        for (int k = 0; k < trainingIds.length; k++) {
//            // Register user in gtw
//            String _newTrId = trainingIds[k];
//            if (StringUtils.isEmpty(_newTrId)) {
//                continue;
//            }
//            try {
//                webinarUserRegistrationInfo.setTrainingId(_newTrId.trim());
//                webinarUserRegistrationInfo = gtwManager.registerWebinar(webinarUserRegistrationInfo, webinarUserRegistrationInfo.getTrainingId());
//                logger.info("gtw response : " + webinarUserRegistrationInfo.toString());
//
//                WebinarInfo webinarInfo = null;
//                try {
//                    // Fetch webinar info
//                    webinarInfo = gtwManager.getWebinarInfo(webinarUserRegistrationInfo.getTrainingId());
//                } catch (Exception ex) {
//                    logger.info("Exception:" + ex.toString() + " message:" + ex.getMessage());
//                }
//
//                // Push activity to ls
//                if (!webinarUserRegistrationInfo.containsTag(WebinarUserTag.LS_REGISTER_ACTIVITY_PUSHED)) {
//                    leadsquaredManager.pushWebinarRegisterActivity(webinarUserRegistrationInfo, webinarInfo);
//                    webinarUserRegistrationInfo.addTag(WebinarUserTag.LS_REGISTER_ACTIVITY_PUSHED);
//                    webinarUserRegistrationInfoDAO.create(webinarUserRegistrationInfo);
//                    logger.info("ls activity posted");
//                } else {
//                    logger.info("ls already activity posted earlier");
//                }
//
//                logger.info("sending sms to user registered");
//                String joinUrl = webinarUserRegistrationInfo.getRegisterJoinUrl();
//                if (!StringUtils.isEmpty(joinUrl)) {
//                    String shortenedUrl = shortenUrl(joinUrl);
//                    sdfWebinarReg.setTimeZone(TimeZone.getTimeZone(TIME_ZONE_IN));
//                    String timestr = sdfWebinarReg.format(new Date(webinarInfo.getStartTime()));
//                    String smsText = "You have been successfully registered for the "
//                            + "webinar on " + webinarInfo.getName()
//                            + ". Please click on the link "+shortenedUrl
//                            + " to JOIN the session 5 mins before the scheduled time on "
//                            + timestr;
//                    sendSMS(smsText, webinarUserRegistrationInfo.getPhone());
//                }
//            } catch (Exception e) {
//                logger.error("Error in registering to webinar " + e.getMessage());
//            }
//        }

    }

    public void registerWebinarFormData(String jsonstr) throws VException {
        WebinarUserRegistrationInfo webinarUserRegistrationInfo = new WebinarUserRegistrationInfo();
        JSONObject jsonObject = new JSONObject(jsonstr);
        // firstName, lastName, emailId, phone, utm_source, utm_campaign, utm_medium

        if (jsonObject.has("firstName")) {
            webinarUserRegistrationInfo.setFirstName(jsonObject.getJSONArray("firstName").getString(0));
        }
        if (jsonObject.has("lastName")) {
            webinarUserRegistrationInfo.setLastName(jsonObject.getJSONArray("lastName").getString(0));
        }
        if (jsonObject.has("emailId")) {
            webinarUserRegistrationInfo.setEmailId(jsonObject.getJSONArray("emailId").getString(0));
        }
        if (jsonObject.has("phone")) {
            webinarUserRegistrationInfo.setPhone(jsonObject.getJSONArray("phone").getString(0));
        }

        if (jsonObject.has("utm_source")) {
            webinarUserRegistrationInfo.setUtm_source(jsonObject.getJSONArray("utm_source").getString(0));
        }
        if (jsonObject.has("utm_campaign")) {
            webinarUserRegistrationInfo.setUtm_campaign(jsonObject.getJSONArray("utm_campaign").getString(0));
        }
        if (jsonObject.has("utm_medium")) {
            webinarUserRegistrationInfo.setUtm_medium(jsonObject.getJSONArray("utm_medium").getString(0));
        }
        if (jsonObject.has("trainingId")) {
            webinarUserRegistrationInfo.setTrainingId(jsonObject.getJSONArray("trainingId").getString(0));
        }

        registerWebinar(webinarUserRegistrationInfo);
    }

    public void userAddressUpload(File file) throws FileNotFoundException, BadRequestException {
        List<String[]> rows = csvManager.parseCSVFile(file);
        String[] fields = rows.get(0);

        //
        int emailIndex = getFieldIndex(fields, "email");
        int pancardIndex = getFieldIndex(fields, "pancard");
        int addressIndex = getFieldIndex(fields, "address");

        if (emailIndex < 0 || pancardIndex < 0) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "user id and pan card are required fields");
        }

        List<String> errors = new ArrayList<>();
        for (int i = 1; i < rows.size(); i++) {
            String[] values = rows.get(i);
            logger.info("Values : " + Arrays.toString(values));
            try {
                String email = values[emailIndex];
                String pancard = values[pancardIndex];
                String address = values[addressIndex];

                // Fetch user from email;
                User user = getUserByEmail(email);

                // Update pan card
                updateUserPancard(user.getId(), pancard);

                // Update address
                updateUserAddress(user.getId(), address);
                logger.info("Processing done");
            } catch (Exception ex) {
                errors.add("row:" + Arrays.toString(values) + " ex:" + ex.getMessage());
            }
        }
    }

    public String user_VT_OTOInvoices(File file) throws FileNotFoundException, BadRequestException {
        List<String[]> rows = csvManager.parseCSVFile(file);
        String[] fields = rows.get(0);

        int emailIndex = getFieldIndex(fields, "teacherEmail");
        if (emailIndex < 0) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "teacherEmail not found");
        }
        List<String> errors = new ArrayList<>();
        int processed = 0;
        int errorCount = 0;
        for (int i = 1; i < rows.size(); i++) {
            try {
                String[] values = rows.get(i);
                String email = values[emailIndex];
                User user = getUserByEmail(email);
                for (int columnCount = 1; columnCount < fields.length; columnCount++) {
                    try {
                        long invoiceTime = Long.parseLong(fields[columnCount]);
                        String amountString = values[columnCount];
                        if (StringUtils.isEmpty(amountString)) {
                            continue;
                        }
                        long actualAmount = (long) Double.parseDouble(amountString);
                        if (actualAmount <= 0) {
                            continue;
                        }

                        AddPaymentInvoiceReq req = new AddPaymentInvoiceReq();
                        req.setTeacherId(user.getId());
                        req.setStudentId(null);
                        req.setActualAmount(actualAmount * 100);
                        req.setInvoiceTime(invoiceTime);
                        req.setInvoiceTitle("Being Charges for Usage of  Vedantu Platform for the month of "
                                + getMonthYear(invoiceTime));
                        req.setPaymentInvoiceType(PaymentInvoiceType.VEDANTU_TEACHER);
                        req.setDeliverableEntityType(null);
                        req.setDeliverableEntityId(null);
                        req.setOrderId(null);
                        req.setTags(Arrays.asList("VT_OTO", "Added on:" + System.currentTimeMillis()));
                        createPaymentInvoice(req);
                        logger.info("Values : " + Arrays.toString(values));
                        processed++;
                    } catch (Exception ex) {
                        errors.add("row:" + Arrays.toString(values) + " ex:" + ex.getMessage());
                        errorCount++;
                    }
                }
            } catch (Exception ex) {
                errors.add("GlobalRow:" + i + " ex:" + ex.getMessage());
                errorCount++;
            }
        }

        logger.info("errorCount:" + errorCount);
        if (errorCount > 0) {
            logger.info("ErrorsString:" + Arrays.toString(errors.toArray()));
        }

        String result = processed + "_" + errorCount;
        logger.info("Result:" + result + " totalRows:" + rows.size() + " columuns:" + fields.length);
        return result;
    }

    public String user_VT_OTFInvoices(File file) throws FileNotFoundException, BadRequestException {
        List<String[]> rows = csvManager.parseCSVFile(file);
        String[] fields = rows.get(0);

        int emailIndex = getFieldIndex(fields, "teacherEmail");
        if (emailIndex < 0) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "teacherEmail not found");
        }
        List<String> errors = new ArrayList<>();
        int processed = 0;
        int errorCount = 0;
        for (int i = 1; i < rows.size(); i++) {
            try {
                String[] values = rows.get(i);
                String email = values[emailIndex];
                User user = getUserByEmail(email);
                for (int columnCount = 1; columnCount < fields.length; columnCount++) {
                    try {
                        long invoiceTime = Long.parseLong(fields[columnCount]);
                        String amountString = values[columnCount];
                        if (StringUtils.isEmpty(amountString)) {
                            continue;
                        }
                        long actualAmount = (long) Double.parseDouble(amountString);
                        if (actualAmount <= 0) {
                            continue;
                        }

                        AddPaymentInvoiceReq req = new AddPaymentInvoiceReq();
                        req.setTeacherId(user.getId());
                        req.setStudentId(null);
                        req.setActualAmount(actualAmount * 100);
                        req.setInvoiceTime(invoiceTime);
                        req.setInvoiceTitle("Being Charges for Usage of  Vedantu Platform for the month of "
                                + getMonthYear(invoiceTime));
                        req.setPaymentInvoiceType(PaymentInvoiceType.VEDANTU_TEACHER);
                        req.setDeliverableEntityType(null);
                        req.setDeliverableEntityId(null);
                        req.setOrderId(null);
                        req.setTags(Arrays.asList("VT_OTF", "Added on:" + System.currentTimeMillis()));
                        createPaymentInvoice(req);
                        logger.info("Values : " + Arrays.toString(values));
                        processed++;
                    } catch (Exception ex) {
                        errors.add("row:" + Arrays.toString(values) + " ex:" + ex.getMessage());
                        errorCount++;
                    }
                }
            } catch (Exception ex) {
                errors.add("GlobalRow:" + i + " ex:" + ex.getMessage());
                errorCount++;
            }
        }

        logger.info("errorCount:" + errorCount);
        if (errorCount > 0) {
            logger.info("ErrorsString:" + Arrays.toString(errors.toArray()));
        }

        String result = processed + "_" + errorCount;
        logger.info("Result:" + result + " totalRows:" + rows.size() + " columuns:" + fields.length);
        return result;
    }

    public String getMonthYear(long time) {
        return new SimpleDateFormat("MMMM, YYYY").format(new Date(time));
    }

    public String user_ST_OTFInvoices(File file) throws FileNotFoundException, BadRequestException {
        List<String[]> rows = csvManager.parseCSVFile(file);
        String[] fields = rows.get(0);

        int teacherEmailIndex = getFieldIndex(fields, "teacherEmail");
        int studentEmailIndex = getFieldIndex(fields, "studentEmail");
        if (teacherEmailIndex < 0 || studentEmailIndex < 0) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "teacherEmail not found");
        }
        List<String> errors = new ArrayList<>();
        int processed = 0;
        int errorCount = 0;
        for (int i = 1; i < rows.size(); i++) {
            try {
                String[] values = rows.get(i);
                String studentEmail = values[studentEmailIndex];
                String teacherEmail = values[teacherEmailIndex];
                User student = getUserByEmail(studentEmail);
                User teacher = getUserByEmail(teacherEmail);
                for (int columnCount = 1; columnCount < fields.length; columnCount++) {
                    try {
                        long invoiceTime = Long.parseLong(fields[columnCount]);
                        String amountString = values[columnCount];
                        if (StringUtils.isEmpty(amountString)) {
                            continue;
                        }
                        long actualAmount = (long) Double.parseDouble(amountString);
                        if (actualAmount <= 0) {
                            continue;
                        }

                        AddPaymentInvoiceReq req = new AddPaymentInvoiceReq();
                        req.setTeacherId(teacher.getId());
                        req.setStudentId(student.getId());
                        req.setActualAmount(actualAmount * 100);
                        req.setInvoiceTime(invoiceTime);
                        req.setInvoiceTitle(
                                "Being Fees for Teaching Students for the month of " + getMonthYear(invoiceTime));
                        req.setPaymentInvoiceType(PaymentInvoiceType.TEACHER_STUDENT);
                        req.setDeliverableEntityType(null);
                        req.setDeliverableEntityId(null);
                        req.setOrderId(null);
                        req.setTags(Arrays.asList("ST_OTF", "Added on:" + System.currentTimeMillis()));
                        createPaymentInvoice(req);
                        logger.info("Values : " + Arrays.toString(values));
                        processed++;
                    } catch (Exception ex) {
                        errors.add("row:" + Arrays.toString(values) + " ex:" + ex.getMessage());
                        errorCount++;
                    }
                }
            } catch (Exception ex) {
                errors.add("GlobalRow:" + i + " ex:" + ex.getMessage());
                errorCount++;
            }
        }

        logger.info("errorCount:" + errorCount);
        if (errorCount > 0) {
            logger.info("ErrorsString:" + Arrays.toString(errors.toArray()));
        }

        String result = processed + "_" + errorCount;
        logger.info("Result:" + result + " totalRows:" + rows.size() + " columuns:" + fields.length);
        return result;
    }

    public String user_ST_OTOInvoices(File file) throws FileNotFoundException, BadRequestException {
        List<String[]> rows = csvManager.parseCSVFile(file);
        String[] fields = rows.get(0);

        int teacherEmailIndex = getFieldIndex(fields, "teacherEmail");
        int studentEmailIndex = getFieldIndex(fields, "studentEmail");
        if (teacherEmailIndex < 0 || studentEmailIndex < 0) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "teacherEmail not found");
        }
        List<String> errors = new ArrayList<>();
        int processed = 0;
        int errorCount = 0;
        for (int i = 1; i < rows.size(); i++) {
            try {
                String[] values = rows.get(i);
                String studentEmail = values[studentEmailIndex];
                String teacherEmail = values[teacherEmailIndex];
                User student = getUserByEmail(studentEmail);
                User teacher = getUserByEmail(teacherEmail);
                for (int columnCount = 2; columnCount < fields.length; columnCount++) {
                    try {
                        long invoiceTime = Long.parseLong(fields[columnCount]);
                        String amountString = values[columnCount];
                        if (StringUtils.isEmpty(amountString) || "null".equals(amountString)) {
                            continue;
                        }
                        long actualAmount = (long) Double.parseDouble(amountString);
                        if (actualAmount <= 0) {
                            continue;
                        }

                        AddPaymentInvoiceReq req = new AddPaymentInvoiceReq();
                        req.setTeacherId(teacher.getId());
                        req.setStudentId(student.getId());
                        req.setActualAmount(actualAmount * 100);
                        req.setInvoiceTime(invoiceTime);
                        req.setInvoiceTitle(
                                "Being Fees for Teaching Students for the month of " + getMonthYear(invoiceTime));
                        req.setPaymentInvoiceType(PaymentInvoiceType.TEACHER_STUDENT);
                        req.setDeliverableEntityType(null);
                        req.setDeliverableEntityId(null);
                        req.setOrderId(null);
                        req.setTags(Arrays.asList("ST_OTF", "Added on:" + System.currentTimeMillis()));
                        createPaymentInvoice(req);
                        logger.info("Values : " + Arrays.toString(values));
                        processed++;
                    } catch (Exception ex) {
                        errors.add("row:" + Arrays.toString(values) + " ex:" + ex.getMessage());
                        errorCount++;
                    }
                }
            } catch (Exception ex) {
                errors.add("GlobalRow:" + i + " ex:" + ex.getMessage());
                errorCount++;
            }
        }

        logger.info("errorCount:" + errorCount);
        if (errorCount > 0) {
            logger.info("ErrorsString:" + Arrays.toString(errors.toArray()));
        }

        String result = processed + "_" + errorCount;
        logger.info("Result:" + result + " totalRows:" + rows.size() + " columuns:" + fields.length);
        return result;
    }

    public void createPaymentInvoice(AddPaymentInvoiceReq req) throws VException {
        logger.info("Req:" + req.toString());
        ClientResponse cResp = WebUtils.INSTANCE.doCall(DINERO_ENDPOINT + "/invoice/create", HttpMethod.POST,
                gson.toJson(req));
        String jsonRespOTFBatch = cResp.getEntity(String.class);
        logger.info("jsonresp:" + jsonRespOTFBatch);
        VExceptionFactory.INSTANCE.parseAndThrowException(cResp);
    }

    public User getUserByEmail(String email) {
        ClientResponse cResp = WebUtils.INSTANCE.doCall(USER_ENDPOINT + "/getUserByEmail?email=" + email,
                HttpMethod.GET, null, true);
        String jsonRespOTFBatch = cResp.getEntity(String.class);
        User user = gson.fromJson(jsonRespOTFBatch, User.class);
        return user;
    }

    public void updateUserPancard(long userId, String pancard) throws VException {
        AddAccountPancardReq pancardReq = new AddAccountPancardReq(userId, pancard);
        ClientResponse cResp = WebUtils.INSTANCE.doCall(DINERO_ENDPOINT + "/account/addAccountPancard", HttpMethod.POST,
                gson.toJson(pancardReq), true);
        String jsonRespOTFBatch = cResp.getEntity(String.class);
        logger.info("jsonresp:" + jsonRespOTFBatch);
        VExceptionFactory.INSTANCE.parseAndThrowException(cResp);
    }

    public void updateUserAddress(long userId, String completeAddress) throws VException {
        AddUserAddressReq addressReq = new AddUserAddressReq();
        addressReq.setUserId(userId);
        addressReq.setStreetAddress(completeAddress);
        ClientResponse cResp = WebUtils.INSTANCE.doCall(USER_ENDPOINT + "/addAddress", HttpMethod.POST,
                gson.toJson(addressReq));
        String jsonRespOTFBatch = cResp.getEntity(String.class);
        logger.info("jsonresp:" + jsonRespOTFBatch);
        VExceptionFactory.INSTANCE.parseAndThrowException(cResp);
    }

    public int getFieldIndex(String[] fields, String fieldName) {
        int index = -1;
        if (fields.length > 0) {
            for (int i = 0; i < fields.length; i++) {
                if (fieldName.toLowerCase().equals(fields[i].toLowerCase())) {
                    index = i;
                }
            }
        }

        return index;
    }

    @Async
    public void syncWebinarInfosAsync() {
        syncWebinarInfos();
    }

    public void syncWebinarInfos() {
        long currentTime = System.currentTimeMillis();
        long startTime = currentTime - 3 * DateTimeUtils.MILLIS_PER_DAY;
        long endTime = currentTime + 4 * DateTimeUtils.MILLIS_PER_DAY;
        syncWebinarInfos(startTime, endTime);
    }

    public void syncWebinarInfos(long startTime, long endTime) {
        gtwManager.syncWebinarInfos(startTime, endTime);
    }

    @Async
    public void processWebinarAttendenceAsync() throws ParseException {
        processWebinarAttendence();
    }

    public void processWebinarAttendence() throws ParseException {
        long currentTime = System.currentTimeMillis();
        long startTime = CommonCalendarUtils.getDayStartTime_IST(currentTime);
        long endTime = currentTime;
        processWebinarAttendence(startTime, endTime);
    }

    public void processWebinarAttendence(long startTime, long endTime) throws ParseException {

        // Sync the webinars first
        syncWebinarInfos(startTime, endTime);

        // Fetch them
        List<WebinarInfo> webinarInfos = gtwManager.getWebinarInfos(startTime, endTime);
        if (CollectionUtils.isEmpty(webinarInfos)) {
            return;
        }

        // Process attendance for these webinars
        List<WebinarUserRegistrationInfo> updatedEntries = gtwManager.processWebinarAttendance(webinarInfos);

        // Send ls activity
        if (!CollectionUtils.isEmpty(updatedEntries)) {
            logger.info("Updated entries : " + updatedEntries.size());
            Map<String, WebinarInfo> webinarInfoMap = webinarInfos.stream()
                    .filter(w -> !StringUtils.isEmpty(w.getWebinarId()))
                    .collect(Collectors.toMap(WebinarInfo::getWebinarId, Function.identity()));
            for (WebinarUserRegistrationInfo entry : updatedEntries) {
                try {
                    if (!entry.containsTag(WebinarUserTag.LS_ATTENDENCE_ACTIVITY_PUSHED)) {
                        if (entry.getTimeInSession() != null && entry.getTimeInSession() > 0) {
                            // Push attended activity
                            logger.info("Attended:" + entry.toString());
                            leadsquaredManager.pushAttendedActivity(entry, webinarInfoMap.get(entry.getTrainingId()));
                            entry.addTag(WebinarUserTag.LS_ATTENDENCE_ACTIVITY_PUSHED);
                            webinarUserRegistrationInfoDAO.create(entry);
                        } else {
                            // Push not attended activity
                            logger.info("NotAttended:" + entry.toString());
                            leadsquaredManager.pushNotAttendedActivity(entry,
                                    webinarInfoMap.get(entry.getTrainingId()));
                            entry.addTag(WebinarUserTag.LS_ATTENDENCE_ACTIVITY_PUSHED);
                            webinarUserRegistrationInfoDAO.create(entry);
                        }
                    }

                    if (!entry.containsTag(WebinarUserTag.LS_REGISTER_ACTIVITY_PUSHED)) {
                        // Push attended activity
                        logger.info("Attended:" + entry.toString());
                        leadsquaredManager.pushWebinarRegisterActivity(entry,
                                webinarInfoMap.get(entry.getTrainingId()));
                        entry.addTag(WebinarUserTag.LS_REGISTER_ACTIVITY_PUSHED);
                        webinarUserRegistrationInfoDAO.create(entry);
                    }
                } catch (Exception ex) {
                    logger.info("Error occured:" + ex.getMessage() + " ex:" + ex.toString());
                }
            }
        } else {
            logger.info("NoUpdated entries returned");
        }
    }

    public String cleanUpWebinarUsers(long startTime, long endTime) {
        syncWebinarInfos(startTime, endTime);

        // Fetch webinar infos for the given interval
        Query webinarInfoQuery = new Query();
        webinarInfoQuery.addCriteria(Criteria.where(WebinarInfo.Constants.START_TIME).gte(startTime).lte(endTime));
        List<WebinarInfo> webinarInfos = webinarInfoDAO.runQuery(webinarInfoQuery, WebinarInfo.class);

        int webinarInfosCount = 0;
        int registeredUsersCount = 0;
        int emailMissingCount = 0;
        int duplicateCount = 0;
        if (!CollectionUtils.isEmpty(webinarInfos)) {
            webinarInfosCount = webinarInfos.size();
            for (WebinarInfo webinarInfo : webinarInfos) {
                // Clean up for each webinar
                Query registerUsersQuery = new Query();
                registerUsersQuery.addCriteria(Criteria.where(WebinarUserRegistrationInfo.Constants.TRAINING_ID)
                        .is(webinarInfo.getWebinarId()));
                List<WebinarUserRegistrationInfo> webinarUserRegistrationInfos = webinarUserRegistrationInfoDAO
                        .runQuery(registerUsersQuery, WebinarUserRegistrationInfo.class);
                if (!CollectionUtils.isEmpty(webinarUserRegistrationInfos)) {
                    registeredUsersCount += webinarUserRegistrationInfos.size();
                    Set<String> emailIdSet = new HashSet<>();
                    for (WebinarUserRegistrationInfo entry : webinarUserRegistrationInfos) {
                        if (StringUtils.isEmpty(entry.getEmailId())) {
                            emailMissingCount++;
                            webinarUserRegistrationInfoDAO.deleteById(entry.getId());
                        } else if (emailIdSet.contains(entry.getEmailId())) {
                            duplicateCount++;
                            webinarUserRegistrationInfoDAO.deleteById(entry.getId());
                        } else {
                            emailIdSet.add(entry.getEmailId());
                        }
                    }
                }
            }
        }

        return "webinarInfosCount:" + webinarInfosCount + "_registeredUsersCount:" + registeredUsersCount
                + "_emailMissingCount:" + emailMissingCount + "_duplicateCount:" + duplicateCount;

    }

    public WebinarInfo getWebinarInfo(String webinarId) throws ParseException {
        return gtwManager.syncWebinarInfos(webinarId);
    }

    public static String getOrderDateValue(long totalAmount, Long firstTransactionTime) {
        if (totalAmount > 0 && firstTransactionTime != null) {
            return totalAmount + "_" + firstTransactionTime;
        } else {
            return "";
        }
    }

    public static String calculateDateIst(Long time) {
        if (time == null) {
            return null;
        }

        sdf.setTimeZone(TimeZone.getTimeZone(DateTimeUtils.TIME_ZONE_IN));
        return sdf.format(new Date(time));
    }

    public static class LeadActivitySearchResult {

        private String relatedProspectId;
        private List<String> queryStrings = new ArrayList<>();

        public LeadActivitySearchResult() {
            super();
        }

        public String getRelatedProspectId() {
            return relatedProspectId;
        }

        public void setRelatedProspectId(String relatedProspectId) {
            this.relatedProspectId = relatedProspectId;
        }

        public List<String> getQueryStrings() {
            return queryStrings;
        }

        public void setQueryStrings(List<String> queryStrings) {
            this.queryStrings = queryStrings;
        }

        @Override
        public String toString() {
            return "LeadActivitySearchResult [relatedProspectId=" + relatedProspectId + ", queryStrings=" + queryStrings
                    + ", toString()=" + super.toString() + "]";
        }
    }

    public static Long getDayStartTimeIST(Long time) {
        Long startTime = time;
        if (0l != (time % DateTimeUtils.MILLIS_PER_DAY - DateTimeUtils.IST_TIME_DIFFERENCE)) {
            Long gmtDiff = time % DateTimeUtils.MILLIS_PER_DAY;
            Long gmtTime = time - gmtDiff;
            startTime = gmtTime + DateTimeUtils.IST_TIME_DIFFERENCE_NEGATIVE;
            if (gmtDiff > DateTimeUtils.IST_TIME_DIFFERENCE) {
                startTime = time - gmtDiff + DateTimeUtils.MILLIS_PER_DAY - DateTimeUtils.IST_TIME_DIFFERENCE_NEGATIVE;
            } else {
                startTime = time - gmtDiff - DateTimeUtils.IST_TIME_DIFFERENCE_NEGATIVE;
            }
        }
        return startTime;
    }

    public PlatformBasicResponse registerviamissedcall(String mobileno,
            String trainingId, String smstext) throws Exception {

        ClientResponse resp = WebUtils.INSTANCE.doCall(USER_ENDPOINT + "/getUsersByContactNumber?contactNumber="
                + mobileno, HttpMethod.GET, null, true);
        // handle error correctly
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        Type filterListType = new TypeToken<List<User>>() {
        }.getType();
        List<User> users = new Gson().fromJson(jsonString, filterListType);

        logger.info("registering to the webinar for users " + users);
        if (ArrayUtils.isNotEmpty(users)) {
            for (User user : users) {
                WebinarUserRegistrationInfo req = new WebinarUserRegistrationInfo();
                req.setTrainingId(trainingId);
                req.setFirstName(user.getFirstName());
                req.setLastName(user.getLastName());
                req.setEmailId(user.getEmail());
                req.setPhone(mobileno);
                registerWebinar(req);
            }
        } else {
            String dummyEmail = generateRandomChars(7)
                    + trainingId.substring(trainingId.length() - 4)
                    + "@vedantu.com";
            WebinarUserRegistrationInfo newreq = new WebinarUserRegistrationInfo();
            newreq.setTrainingId(trainingId);
            newreq.setFirstName("");
            newreq.setEmailId(dummyEmail);
            newreq.setPhone(mobileno);
            registerWebinar(newreq);
        }

//        else if (com.vedantu.util.StringUtils.isNotEmpty(smstext)) {
//            TextSMSRequest textSMSRequest = new TextSMSRequest();
//            textSMSRequest.setBody(smstext);
//            textSMSRequest.setTo(mobileno);
//            ClientResponse respsms = WebUtils.INSTANCE.doCall(NOTIFICATION_ENDPOINT
//                    + "/SMS/sendSMS", HttpMethod.POST, gson.toJson(textSMSRequest), true);
//            VExceptionFactory.INSTANCE.parseAndThrowException(respsms);
//            String respsmsJson = respsms.getEntity(String.class);
//            logger.info("respone of sendsms " + respsmsJson);
//        }
        return new PlatformBasicResponse();
    }

    public void webinarReminderScheduler() {
        Integer SIZE = 5;
        Integer start = 0;
        Integer BATCH_SIZE = 500;
        Long fromTime = System.currentTimeMillis() + DateTimeUtils.MILLIS_PER_MINUTE;
        Long tillTime = fromTime + (DateTimeUtils.MILLIS_PER_MINUTE * 30);
        logger.info("Checking for webinars from " + new Date(fromTime) + " to " + new Date(tillTime));
        while (true) {
            List<WebinarInfo> webinarInfos = webinarInfoDAO.getWebinarInfos(fromTime, tillTime, start, SIZE);
            if (ArrayUtils.isEmpty(webinarInfos)) {
                break;
            }
            for (WebinarInfo webinarInfo : webinarInfos) {
                try {
                    String trainingId = webinarInfo.getWebinarId();
                    Long regCount = webinarUserRegistrationInfoDAO
                            .getWebinarUserRegistrationInfosCount(trainingId);
                    logger.info("total registrations count for " + trainingId + " " + regCount);
                    if (regCount > 0) {
                        for (int k = 0; k < regCount; k += BATCH_SIZE) {
                            Map<String, Object> payload = new HashMap<>();
                            payload.put("webinarTitle", webinarInfo.getName());
                            payload.put("trainingId", trainingId);
                            payload.put("start", k);
                            payload.put("size", BATCH_SIZE);
                            AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.WEBINAR_REMINDER_SMS,
                                    payload);
                            logger.info("scheduling a task for trainingId "
                                    + trainingId + " start " + start + ", size " + BATCH_SIZE);
                            asyncTaskFactory.executeTask(params);
                        }
                    }
                } catch (JSONException | ClientHandlerException | UniformInterfaceException e) {
                    logger.error("Error in sending reminder sms for webinar "
                            + webinarInfo.getId() + ", Error: " + e.getMessage());
                }

            }
            start += SIZE;
        }
    }

    public void webinarReminder(String webinarTitle, String trainingId,
            Integer start, Integer size) throws VException {
        logger.info("Sending webinar reminder to " + webinarTitle
                + ", trainingId " + trainingId + ", start " + start + ", size " + size);
        List<WebinarUserRegistrationInfo> regUsers = webinarUserRegistrationInfoDAO
                .getWebinarUserRegistrationInfos(trainingId, start, size);
        logger.info("sending sms to " + regUsers.size());
        if (ArrayUtils.isNotEmpty(regUsers)) {
            for (int i = 0; i < regUsers.size(); i++) {
                logger.info("sending sms to batch start" + start + ", count " + i);
                WebinarUserRegistrationInfo regInfo = regUsers.get(i);
                try {
                    String joinUrl = regInfo.getRegisterJoinUrl();
                    if (!StringUtils.isEmpty(joinUrl)) {
                        String shortenedUrl = shortenUrl(joinUrl);
                        String smsText = "Reminder: Your MasterClass on " + webinarTitle
                                + " will start in 30 Mins. Join the session through this link: "
                                + shortenedUrl;
                        sendSMS(smsText, regInfo.getPhone());
                    }
                } catch (Exception e) {
                    logger.info("error in sending sms, " + e.getMessage());
                    //swallow
                }
            }
        }
    }

    private void sendSMS(String smsText, String phone) throws VException {
        logger.info("sending sms to " + phone);
        TextSMSRequest textSMSRequest = new TextSMSRequest();
        textSMSRequest.setBody(smsText);
        textSMSRequest.setTo(phone);
        textSMSRequest.setPriorityType(SMSPriorityType.HIGH);
        ClientResponse respsms = WebUtils.INSTANCE.doCall(NOTIFICATION_ENDPOINT
                + "/SMS/sendSMS", HttpMethod.POST, gson.toJson(textSMSRequest), true);
        VExceptionFactory.INSTANCE.parseAndThrowException(respsms);
        String respsmsJson = respsms.getEntity(String.class);
        logger.info("respone of sendsms " + respsmsJson);
    }

    private String shortenUrl(String url) {
        String finalUrl = url;
//        if (StringUtils.isNotEmpty(url)) {
//            try {
//                String urlShortner = "https://www.googleapis.com/urlshortener/v1/url?key="
//                        + URL_SHORTNER_API_KEY;
//                JSONObject json = new JSONObject();
//                json.put("longUrl", url);
//                ClientResponse urlshortresp = WebUtils.INSTANCE.doCall(urlShortner, HttpMethod.POST,
//                        json.toString(), false);
//                VExceptionFactory.INSTANCE.parseAndThrowException(urlshortresp);
//                String urlshortrespStr = urlshortresp.getEntity(String.class);
//                logger.info("respone of urlshortner " + urlshortrespStr);
//                JSONObject responseObj = new JSONObject(urlshortrespStr);
//                if (!StringUtils.isEmpty(responseObj.getString("id"))) {
//                    finalUrl = responseObj.getString("id");
//                }
//            } catch (ClientHandlerException | UniformInterfaceException | VException | JSONException e) {
//                //swallow
//                  logger.info("error in sending sms, "+e.getMessage());
//            }
//        }
        return finalUrl;
    }

    public static String generateRandomChars(int length) {
        String candidateChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".toLowerCase();
        StringBuilder sb = new StringBuilder();
        Random random = new Random();
        for (int i = 0; i < length; i++) {
            sb.append(candidateChars.charAt(random.nextInt(candidateChars
                    .length())));
        }

        return sb.toString();
    }

    public File createBundleBulk(MultipartFile multipart) throws IOException {
        File file = CommonUtils.multipartToFile(multipart);
        List<String[]> list;
        try (FileReader reader = new FileReader(file); CSVReader csvReader = new CSVReader(reader)) {
            list = csvReader.readAll();
            boolean headerRow = true;
            Map<String, Integer> headers = getHeaderMap(list.get(0));
            int lastCellNum = headers.size();
            for (int z = 0; z < list.size(); z++) {
                String[] cells = list.get(z);
                if (Arrays.stream(cells).allMatch(e -> e == null || e.trim().isEmpty())) {
                    continue;
                }
                int cellCount = 0;
                try {
                    if (!headerRow) {
                        AddEditBundleReq req = new AddEditBundleReq();
                        final String id = getCellValue(cells, headers.get("ID"), null);
                        req.setId(id);
                        req.setTitle(getCellValue(cells, headers.get("Title"), null));
                        req.setDescription(getCellValue(cells, headers.get("Description"), null));
                        req.setDemoVideoUrl(getCellValue(cells, headers.get("Demo Video ID"), null));

                        String validTillDate = getCellValue(cells, headers.get("Valid Till Date"), null);
                        req.setValidTill(DateTimeUtils.getEpochMillis(validTillDate, DateTimeFormatter.ISO_DATE));

                        req.setState(BundleState.valueOf(getCellValue(cells, headers.get("Course State"), "DRAFT").toUpperCase()));

                        req.setIsTrail("Yes".equalsIgnoreCase(getCellValue(cells, headers.get("Has Trial"), null)));
                        req.setAmIncluded("Yes".equalsIgnoreCase(getCellValue(cells, headers.get("AM"), null)));
                        req.setUnLimitedDoubtsIncluded("Yes".equalsIgnoreCase(getCellValue(cells, headers.get("Doubts"), null)));
                        req.setTabletIncluded("Yes".equalsIgnoreCase(getCellValue(cells, headers.get("Tablet"), null)));

                        req.setCutPrice((int) Double.parseDouble(getCellValue(cells, headers.get("Cut Price"), "0")));
                        int totalPrice = (int) Double.parseDouble(getCellValue(cells, headers.get("Total Price"), "0"));
                        req.setPrice(totalPrice);
                        req.setGrade(Collections.singletonList(Integer.parseInt(getCellValue(cells, headers.get("grade"), "0"))));
                        req.setValidDays((int) Double.parseDouble(getCellValue(cells, headers.get("Valid Days"), "0")));

                        req.setSubject(Arrays.stream(getCellValue(cells, headers.get("subject"), "").split("/")).map(String::trim).collect(Collectors.toList()));
                        req.setTarget(Arrays.stream(getCellValue(cells, headers.get("target"), "").split("/")).map(String::trim).map(String::toLowerCase).collect(Collectors.toList()));
                        req.setSuggestionPackages(Arrays.stream(getCellValue(cells, headers.get("AIO Package suggestions"), "").split("/")).map(String::trim).collect(Collectors.toList()));
                        req.setSearchTerms(Arrays.stream(getCellValue(cells, headers.get("searchTerms"), "").split("/")).map(String::trim)
                                .map(String::toUpperCase)
                                .filter(e -> !e.trim().isEmpty())
                                .map(CourseTerm::valueOf)
                                .collect(Collectors.toList()));

                        // ----------------- INSTALLMENT DETAILS ------------------------
                        int numberOfInstallments = (int) Double.parseDouble(getCellValue(cells, headers.get("Number of installments"), "0"));
                        String startDate = getCellValue(cells, headers.get("Installment start date"), null);

                        ZonedDateTime installmentStartDate = DateTimeUtils.getZonedDate(startDate, DateTimeFormatter.ISO_DATE);
                        int installmentAmount = numberOfInstallments == 0 ? 0 : (totalPrice / numberOfInstallments);
                        List<BaseInstalmentInfo> instalmentInfos = new ArrayList<>();
                        for (int i = 0; i < numberOfInstallments; i++) {
                            BaseInstalmentInfo instalmentInfo = new BaseInstalmentInfo();
                            instalmentInfo.setAmount(installmentAmount);
                            instalmentInfo.setDueTime(installmentStartDate.toInstant().toEpochMilli());
                            ZonedDateTime date = installmentStartDate.plusMonths(1);
                            installmentStartDate = installmentStartDate.getDayOfMonth() == date.getDayOfMonth() ? date : installmentStartDate.plusDays(31);
                            instalmentInfos.add(instalmentInfo);
                        }
                        req.setInstalmentDetails(instalmentInfos);

                        // -------------------- AIO Package Details ----------------------
                        List<String> aioCourseGrades = Arrays.stream(getCellValue(cells, headers.get("add course-grade"), "").split("/")).map(String::trim).filter(e -> !e.isEmpty()).collect(Collectors.toList());
                        List<String> aioCourseTypes = Arrays.stream(getCellValue(cells, headers.get("add course-type"), "").split("/")).map(String::trim).filter(e -> !e.isEmpty()).collect(Collectors.toList());
                        List<String> aioCourseStatus = Arrays.stream(getCellValue(cells, headers.get("add course-enrollment status"), "").split("/")).map(String::trim).filter(e -> !e.isEmpty()).collect(Collectors.toList());
                        List<String> aioCourseEntityIds = Arrays.stream(getCellValue(cells, headers.get("add course-entity ID"), "").split("/")).map(String::trim).filter(e -> !e.isEmpty()).collect(Collectors.toList());

                        if (aioCourseGrades.size() != aioCourseTypes.size() || aioCourseStatus.size() != aioCourseEntityIds.size()
                                || aioCourseGrades.size() != aioCourseStatus.size()) {
                            cellCount = addCellData(list, lastCellNum, z, cellCount, "-", "FAILED", "AIO Packages variable have varying no of arguments (aioCourseGrades,aioCourseTypes,aioCourseStatus,aioCourseEntityIds)");
                        } else if (req.getValidTill() != null && req.getValidTill() > 0 && req.getValidDays() > 0) {
                            cellCount = addCellData(list, lastCellNum, z, cellCount, "-", "FAILED", "Please set only one of valid till date or vaild days)");
                        } else if (req.getId() != null && StringUtils.isNotEmpty(req.getId().trim())) {
                            cellCount = addCellData(list, lastCellNum, z, cellCount, "-", "FAILED", "this api only used for creating new bundles. not used to updated existing one!!");
                        } else if (!getErrorList(req).isEmpty()) {
                            cellCount = addCellData(list, lastCellNum, z, cellCount, "-", "FAILED", String.join(",", getErrorList(req)) + " fields have empty values(for numbers val should be > 0)");
                        } else {
                            // Default Values (NPE Occur if we omit this process)
                            req.setWebinarCategories(new ArrayList<>());
                            req.setTeacherIds(new ArrayList<>());
                            req.setCourses(new ArrayList<>());
                            VideoContentData videos = new VideoContentData();
                            videos.setChildren(new ArrayList<>());
                            videos.setContents(new ArrayList<>());
                            req.setVideos(videos);
                            TestContentData tests = new TestContentData();
                            tests.setChildren(new ArrayList<>());
                            tests.setContents(new ArrayList<>());
                            req.setTests(tests);
                            HashMap<String, Object> metadata = new HashMap<>();
                            metadata.put("priceCard", new HashMap<>());
                            req.setMetadata(metadata);
                            String bundleUrl = ConfigUtils.INSTANCE.getSubscriptionEndpoint() + "/bundle/addEditBundle";
                            ClientResponse bundleResponse = WebUtils.INSTANCE.doCall(bundleUrl, HttpMethod.POST, gson.toJson(req));
                            VExceptionFactory.INSTANCE.parseAndThrowException(bundleResponse);
                            String bundleInfoJson = bundleResponse.getEntity(String.class);
                            BundleInfo bundleInfo = gson.fromJson(bundleInfoJson, BundleInfo.class);


                            String bundleEntityUrl = ConfigUtils.INSTANCE.getSubscriptionEndpoint() + "/bundleEntity/addEditBundleEntity";
                            for (int i = 0; i < aioCourseGrades.size(); i++) {
                                BundleEntityRequest request = new BundleEntityRequest();
                                request.setGrade((int) Double.parseDouble(aioCourseGrades.get(i)));
                                request.setEntityId(aioCourseEntityIds.get(i));
                                request.setPackageType(PackageType.valueOf(aioCourseTypes.get(i).toUpperCase()));
                                request.setEnrollmentType(EnrollmentType.valueOf(aioCourseStatus.get(i).toUpperCase()));
                                request.setBundleId(bundleInfo.getId());
                                WebUtils.INSTANCE.doCall(bundleEntityUrl, HttpMethod.POST, gson.toJson(request));
                            }

                            cellCount = addCellData(list, lastCellNum, z, cellCount, bundleInfo.getId(), "SUCCESS", "-");
                        }
                    } else {
                        // First row of the File
                        cellCount = addCellData(list, lastCellNum, z, cellCount, "Updated ID", "Updated Status", "Error Message");
                    }
                } catch (Exception e) {
                    logger.error(e.getMessage(), e);
                    cellCount = addCellData(list, lastCellNum, z, cellCount, "-", "FAILED", e.getMessage());
                }
                headerRow = false;
            }
        }

        File currDir = File.createTempFile(String.valueOf(System.currentTimeMillis()), ".csv");
        String path = currDir.getAbsolutePath();

        CSVWriter writer = new CSVWriter(new FileWriter(path));
        for (String[] array : list) {
            writer.writeNext(array);
        }

        writer.close();

        return currDir;
    }

    private int addCellData(List<String[]> list, int lastCellNum, int index, int cellCount, String firstCol, String secondCol, String thirdCol) {
        list.set(index, createCell(list.get(index), lastCellNum + (++cellCount), firstCol));
        list.set(index, createCell(list.get(index), lastCellNum + (++cellCount), secondCol));
        list.set(index, createCell(list.get(index), lastCellNum + (++cellCount), thirdCol));
        return cellCount;
    }

    private String getCellValue(String[] cells, Integer index, String defaultValue) {
        if (index == null) {
            return defaultValue;
        }
        String cellValue = cells.length <= index ? null : cells[index];
        if (cellValue == null || cellValue.trim().isEmpty()) {
            return defaultValue;
        } else {
            return cellValue;
        }
    }

    private Collection<String> getErrorList(AddEditBundleReq req) {
        List<String> errors = new ArrayList<>();

        if (req.getTitle() == null || req.getTitle().trim().isEmpty()) {
            errors.add("title");
        }
        if (req.getPrice() == null || req.getPrice() == 0) {
            errors.add("price");
        }
        if (req.getCutPrice() == null || req.getCutPrice() == 0) {
            errors.add("cut price");
        }
        if (req.getGrade() == null) {
            errors.add("grade");
        }
        if (req.getSearchTerms() == null || req.getSearchTerms().isEmpty()) {
            errors.add("search terms");
        }
        if (req.getState() == null) {
            errors.add("course state");
        }
        return errors;
    }

    private String[] createCell(String[] row, int index, String value) {
        String[] dest = new String[index + 1];
        System.arraycopy(row, 0, dest, 0, row.length);
        dest[index] = value;
        return dest;
    }


    public String updateAIOBundleTagDetails(MultipartFile multipartFile) throws IOException, VException {
        File file = CommonUtils.multipartToFile(multipartFile);
        List<String[]> list;
        BundleDetailsUpdateReq req = new BundleDetailsUpdateReq();

        try (FileReader reader = new FileReader(file); CSVReader csvReader = new CSVReader(reader)) {
            list = csvReader.readAll();

            Map<String, Integer> headers = getHeaderMap(list.get(0));
            Map<String, Map<String, BatchDetails>> batchDetailsMap = new HashMap<>();
            Map<String, BundleDetails> bundleDetailsMap = new HashMap<>();
            boolean firstRow = true;
            for (String[] cells : list) {
                if (Arrays.stream(cells).allMatch(e -> e == null || e.trim().isEmpty()) || firstRow) {
                    firstRow = false;
                    continue;
                }

                String id = cells[headers.get("aioId")];
                Map<String, BatchDetails> aioMap = CollectionUtils.putIfAbsentAndGet(batchDetailsMap, id, HashMap::new);

                if (headers.get("batchId") != null) {
                    String aioId = cells[headers.get("batchId")];
                    BatchDetails details = getBatchDetails(headers, cells);
                    aioMap.put(aioId, details);
                }
                batchDetailsMap.put(id, aioMap);
                bundleDetailsMap.put(id, getBundleDetails(headers, cells));
            }

            req.setBundleDetails(bundleDetailsMap);
            req.setBacthDetails(batchDetailsMap);
            req.setEmptyDisplayTags(true);
        }


        String url = SUBSCRIPTION_ENDPOINT + "/bundle/update/aio/bundle-details";
        logger.info("URL: " + url);
        ClientResponse response = WebUtils.INSTANCE.doCall(url, HttpMethod.POST, gson.toJson(req));
        VExceptionFactory.INSTANCE.parseAndThrowException(response);
        return response.getEntity(String.class);
    }

    private Map<String, Integer> getHeaderMap(String[] row) {
        int i = 0;
        Map<String, Integer> headers = new TreeMap<>(String::compareToIgnoreCase);
        for (String cell : row) {
            String cellValue = org.apache.commons.lang.StringUtils.trimToEmpty(cell);
            headers.put(cellValue, i);
            i++;
        }
        return headers;
    }

    private BundleDetails getBundleDetails(Map<String, Integer> headers, String[] cells) throws VException {
        BundleDetails details = new BundleDetails();
        if (headers.get("BundleImageUrl") != null) {
            String imageUrl = cells[headers.get("BundleImageUrl")];
            details.setBundleImageUrl(imageUrl);
        }

        if (headers.get("PositionInUI") != null) {
            String data = cells[headers.get("PositionInUI")];
            if (data != null && !data.trim().isEmpty()) {
                details.setPosition(Long.parseLong(data.trim()));
            }
        }


        if (headers.get("CourseType") != null) {

            String courseType = cells[headers.get("CourseType")];
            details.setCourseType(CourseType.valueOf(courseType.trim()));
        }

        if (headers.get("CourseStartTime") != null) {
            String courseHours = cells[headers.get("CourseStartTime")];

            if (courseHours != null && !courseHours.trim().isEmpty()) {
                details.setCourseStartTime(DateTimeUtils.getEpochMillis(courseHours.trim(), DateTimeFormatter.ISO_DATE));
            }
        }


        if (headers.get("PromotionTag") != null) {
            String data = cells[headers.get("PromotionTag")];
            if (data != null && !data.trim().isEmpty()) {
                details.setPromotionTag((data.trim()));
            }
        }

        if (headers.get("CollectionTitle") != null) {
            String displayTags = cells[headers.get("CollectionTitle")];
            if (displayTags != null) {
                LinkedHashSet<String> hashSet = Arrays.stream(displayTags.split("/")).map(String::trim)
                        .filter(StringUtils::isNotEmpty)
                        .collect(Collectors.toCollection(LinkedHashSet::new));

                Map<String, Integer> collectionRanks = new HashMap<>();
                LinkedHashSet<String> tags = details.getDisplayTags();
                for (String tag : hashSet) {
                    if (StringUtils.isNotEmpty(tag)) {
                        // Format for tag is =>> <NUMBER><SPACE><TITLE> Ex. '1 Featured'
                        Integer rank = null;
                        try {
                            rank = Integer.parseInt(tag.trim().substring(0, tag.indexOf(" ")).trim());
                        } catch (NumberFormatException e) {
                            logger.warn("BUNDLE CSV COLLECTION ORDER EMPTY : " + tag , e);
                            throw new BadRequestException(ErrorCode.SERVICE_ERROR, "BUNDLE CSV COLLECTION ORDER EMPTY : " + tag + " FOR ID = " + cells[0]);
                        }
                        tag = tag.trim().substring(tag.indexOf(" ")).trim();
                        tags.add(tag);
                        collectionRanks.putIfAbsent(tag.replaceAll("\\.", ""), rank);
                    }
                }

                details.setDisplayTags(tags);
                details.setDisplayTagsRanks(collectionRanks);
            }
        }
        return details;
    }

    private BatchDetails getBatchDetails(Map<String, Integer> headers, String[] cells) {
        BatchDetails details = new BatchDetails();

        if (headers.get("CourseSyllabus") != null) {
            String courseSyllabus = cells[headers.get("CourseSyllabus")];
            if (courseSyllabus != null) {
                LinkedHashSet<Classes> syllabus = details.getCourseSyllabus();

                for (String title : courseSyllabus.split("/")) {
                    if (org.springframework.util.StringUtils.isEmpty(title)) {
                        continue;
                    }

                    Classes classes = new Classes();
                    classes.setTitle(title);
                    syllabus.add(classes);
                }
                details.setCourseSyllabus(syllabus);
            }
        }
        if (headers.get("CoursePeriodInMinutes") != null) {
            String courseHours = cells[headers.get("CoursePeriodInMinutes")];
            details.setCoursePeriodMillis(Long.parseLong(courseHours.trim()) * DateTimeUtils.MILLIS_PER_MINUTE );
        }


        if (headers.get("CoursePeriodInMinutes") != null) {
            String courseHours = cells[headers.get("CoursePeriodInMinutes")];
            details.setCoursePeriodMillis(Long.parseLong(courseHours.trim()) * DateTimeUtils.MILLIS_PER_MINUTE );
        }
        if (headers.get("CourseType") != null) {
            String courseType = cells[headers.get("CourseType")];
            details.setCourseType(CourseType.valueOf(courseType.trim()));
        }

        if (headers.get("CourseStartTime") != null) {
            String courseHours = cells[headers.get("CourseStartTime")];
            if (courseHours != null && !courseHours.trim().isEmpty()) {
                details.setCourseStartTime(DateTimeUtils.getEpochMillis(courseHours.trim(), DateTimeFormatter.ISO_DATE));
            }
        }

        Map<String, Object> teacherInfo = new HashMap<>();
        if (headers.get("TeacherImageUrlHigh") != null) {
            String data = cells[headers.get("TeacherImageUrlHigh")];
            teacherInfo.put("teacherImageUrlHigh", data);
        }

        if (headers.get("TeacherImageUrlLow") != null) {
            String data = cells[headers.get("TeacherImageUrlLow")];
            teacherInfo.put("teacherImageUrlLow", data);
        }

        if (headers.get("TeacherName") != null) {
            String data = cells[headers.get("TeacherName")];
            teacherInfo.put("teacherName", data);
        }

        if (headers.get("TeacherDegree") != null) {
            String data = cells[headers.get("TeacherDegree")];
            teacherInfo.put("teacherDegree", data);
        }

        if (headers.get("TeacherExperience") != null) {
            String data = cells[headers.get("TeacherExperience")];
            teacherInfo.put("teacherExperience", data);
        }

        if (headers.get("TeacherCollege") != null) {
            String data = cells[headers.get("TeacherCollege")];
            teacherInfo.put("teacherCollege", data);
        }

        if (headers.get("TeacherEmail") != null) {
            String data = cells[headers.get("TeacherEmail")];
            teacherInfo.put("teacherEmail", data);
        }

        if (headers.get("TeacherInfoLine1") != null) {
            String data = cells[headers.get("TeacherInfoLine1")];
            teacherInfo.put("teacherInfoLine1", data);
        }

        if (headers.get("TeacherInfoLine2") != null) {
            String data = cells[headers.get("TeacherInfoLine2")];
            teacherInfo.put("teacherInfoLine2", data);
        }

        if (headers.get("TeacherRating") != null) {
            String data = cells[headers.get("TeacherRating")];
            teacherInfo.put("teacherRating", data);
        }

        if (headers.get("TeacherAvgRating") != null) {
            String data = cells[headers.get("TeacherAvgRating")];
            teacherInfo.put("teacherAvgRating", data);
        }

        if (headers.get("TeacherIntroVideoLink") != null) {
            String data = cells[headers.get("TeacherIntroVideoLink")];
            teacherInfo.put("teacherIntroVideoLink", data);
        }

        details.setTeacherInfo(teacherInfo);
        return details;
    }
}
