package com.vedantu.vedantudata.managers;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.User.User;
import com.vedantu.exception.VException;
import com.vedantu.exception.VExceptionFactory;
import com.vedantu.util.*;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

import com.vedantu.vedantudata.dao.serializers.LeadsquaredDAO;
import com.vedantu.vedantudata.dao.serializers.UserDAO;
import com.vedantu.vedantudata.entities.UserData;
import com.vedantu.vedantudata.entities.leadsquared.LeadDetails;

@Service
public class UserManager {

	@Autowired
	private LeadsquaredDAO leadsquaredDAO;

	@Autowired
	private UserDAO userDAO;

	@Autowired
	private LogFactory logFactory;

	@SuppressWarnings("static-access")
	private Logger logger = logFactory.getLogger(UserManager.class);

	private static final String USER_ENDPOINT = ConfigUtils.INSTANCE.getStringValue("USER_ENDPOINT");

	public int updateUserData(long startTime, long endTime) {
		int leadsUpdated = 0;
		Query query = new Query();
		query.addCriteria(Criteria.where(LeadDetails.Constants.LAST_UPDATED).gte(startTime).lte(endTime));
		long totalCount = leadsquaredDAO.queryCount(query, LeadDetails.class);
		for (int i = 0; i < totalCount; i += 100) {
			query.skip(i);
			query.limit(100);
			List<LeadDetails> leads = leadsquaredDAO.runQuery(query, LeadDetails.class);
			if (!CollectionUtils.isEmpty(leads)) {
				logger.info("Leads found: " + leads.size());
				for (LeadDetails leadDetails : leads) {
					try {
						updateUserData(leadDetails);
						leadsUpdated++;
					} catch (Exception ex) {
						logger.error("Exception occured while saving userdata:" + leadDetails.toString());
					}
				}
			}
		}

		return leadsUpdated;
	}

	public void updateUserData(LeadDetails leadDetails) throws Exception {
		Query query = new Query();

		Set<String> allPhones = leadDetails.fetchAllPhones();
		// Lead phone and email
		if (!StringUtils.isEmpty(leadDetails.getPhone())) {
			query.addCriteria(Criteria.where(UserData.Constants.PRIMARY_CONTACT_NUMBER).in(allPhones));
			query.addCriteria(Criteria.where(UserData.Constants.ALL_PHONE_NUMBERS).in(allPhones));
		}

		// Lead email
		Set<String> allEmails = leadDetails.fetchAllEmails();
		if (!StringUtils.isEmpty(leadDetails.getEmailAddress())) {
			query.addCriteria(Criteria.where(UserData.Constants.USER_EMAIL).is(allEmails));
			query.addCriteria(Criteria.where(UserData.Constants.ALL_EMAILS).in(allEmails));
		}

		if (CollectionUtils.isEmpty(allPhones) && CollectionUtils.isEmpty(allEmails)) {
			return;
		}

		List<UserData> users = userDAO.runQuery(query, UserData.class);
		if (CollectionUtils.isEmpty(users)) {
			// Create a new User entry
			UserData userData = new UserData();
			userData.setEmailId(leadDetails.getEmailAddress());
			userData.setAllEmails(allEmails);
			userData.setAllPhoneNumber(allPhones);
			userData.addLeadSquaredId(leadDetails.getProspectID());
			userDAO.save(userData);
			return;
		}

		if (users.size() > 1) {
			logger.error("Duplicate user entries found fo2r same user" + leadDetails.toString());
			// TODO : introduce merging
		}

		UserData userData = users.get(0);
		userData.addAllEmails(allEmails);
		userData.addAllPhoneNumbers(allPhones);
		userData.addLeadSquaredId(leadDetails.getProspectID());
		userDAO.save(userData);
	}

	public List<UserData> getUserData(String emailAddress, String phone) {
		if (StringUtils.isEmpty(emailAddress) && StringUtils.isEmpty(phone)) {
			return null;
		}

		Query query = new Query();
		if (!StringUtils.isEmpty(emailAddress)) {
			query.addCriteria(Criteria.where(UserData.Constants.ALL_EMAILS).in(emailAddress));
		}
		if (!StringUtils.isEmpty(phone)) {
			query.addCriteria(Criteria.where(UserData.Constants.ALL_PHONE_NUMBERS).in(phone));
		}

		List<UserData> userData = userDAO.runQuery(query, UserData.class);
		if (CollectionUtils.isEmpty(userData)) {
			return null;
		}

		if (userData.size() > 1) {
			logger.error("Duplicate user data entries found:" + Arrays.toString(userData.toArray()));
		}

		return userData;
	}

	public User getUserByEmail(String email) throws VException {

		ClientResponse resp = WebUtils.INSTANCE.doCall(USER_ENDPOINT + "/getUserByEmail?email=" + email, HttpMethod.GET,
				null, true);
		VExceptionFactory.INSTANCE.parseAndThrowException(resp);
		String jsonString = resp.getEntity(String.class);

		User user = new Gson().fromJson(jsonString, User.class);

		return user;
	}

	public User getUserByPhone(String phone) throws VException {

		ClientResponse resp = WebUtils.INSTANCE.doCall(USER_ENDPOINT + "/getUsersByContactNumber?contactNumber=" + phone, HttpMethod.GET,
				null, true);
		VExceptionFactory.INSTANCE.parseAndThrowException(resp);
		String jsonString = resp.getEntity(String.class);

		Type listType = new TypeToken<List<User>>() {
		}.getType();

		List<User> users = new Gson().fromJson(jsonString, listType);

		if(CollectionUtils.isNotEmpty(users))
			return users.get(0);

		return null;
	}
}
