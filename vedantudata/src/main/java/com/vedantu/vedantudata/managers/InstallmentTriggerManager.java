package com.vedantu.vedantudata.managers;

import com.vedantu.async.AsyncTaskFactory;
import com.vedantu.async.AsyncTaskParams;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.vedantudata.async.AsyncTaskName;
import com.vedantu.vedantudata.controllers.ActivityController;
import com.vedantu.vedantudata.managers.leadsquared.LeadSquaredUsers;
import com.vedantu.vedantudata.utils.ActivityEventConstants;
import com.vedantu.vedantudata.utils.LeadsquaredActivityHelper;
import com.vedantu.vedantudata.utils.PostgressHandler;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Service
public class InstallmentTriggerManager {

    private static final String CARE_TEAM_EMAIL = ConfigUtils.INSTANCE.getStringValue("care_team_email");
    @Autowired
    private PostgressHandler postgressHandler;
    @Autowired
    private ActivityManager activityManager;
    @Autowired
    private LeadsquaredActivityHelper leadsquaredActivityHelper;
    @Autowired
    private LeadSquaredUsers leadsquaredUsers; ;

    @Autowired
    private AsyncTaskFactory asyncTaskFactory;

    private LogFactory logFactory;
    private Logger logger = logFactory.getLogger(ActivityController.class);

    public void process() {

        String query = "SELECT INST.useremail,\n" +
                "       totalamount,\n" +
                "       paymentstatus,\n" +
                "       duetime_istdate,\n" +
                "       total_due,\n" +
                "       pending_installments,\n" +
                "       previous_month_pending_installments,\n" +
                "       lastpaymentmade\n" +
                "FROM ((select useremail, totalamount, paymentstatus, duetime_istdate, entitystate\n" +
                "       from instalment\n" +
                "       where paymentstatus IN ('NOT_PAID', 'PAYMENT_SUSPENDED')\n" +
                "         AND entitystate = 'ACTIVE'\n" +
                "         AND duetime_istdate >= dateadd(month, -1, GETDATE())\n" +
                "         AND duetime_istdate <= dateadd(day, 4, GETDATE())) INST\n" +
                "    left join (\n" +
                "        select useremail,\n" +
                "               COALESCE(SUM(totalamount), 0) as total_due,\n" +
                "               count(totalamount) as            pending_installments,\n" +
                "               COUNT(CASE\n" +
                "                         WHEN duetime_istdate <= DATEADD(day, -EXTRACT(day FROM getdate()), getdate())\n" +
                "                             THEN 1 END) as     previous_month_pending_installments\n" +
                "        from instalment\n" +
                "        where paymentstatus IN ('NOT_PAID', 'PAYMENT_SUSPENDED')\n" +
                "          AND duetime_istdate <= dateadd(day, 4, GETDATE())\n" +
                "          AND entitystate = 'ACTIVE'\n" +
                "        group by useremail) AGG on INST.useremail = AGG.useremail)\n" +
                "         left join (\n" +
                "    select useremail, max(paidtime_istdate) as lastpaymentmade\n" +
                "    from instalment\n" +
                "    where paymentstatus = 'PAID'\n" +
                "      AND duetime_istdate <= dateadd(day, 4, GETDATE())\n" +
                "      AND entitystate = 'ACTIVE'\n" +
                "    group by useremail) LATEST on INST.useremail = LATEST.useremail\n" +
                "order by duetime_istdate asc;";

        Map<String, Map> userInstallments = new HashMap<>();
        try {
            ResultSet rs = postgressHandler.executeQuery(query);
            if (rs != null) {
                while (rs.next()) {
                    Float amount = rs.getFloat("totalamount");
                    Float total_due = rs.getFloat("total_due");
                    Float pending_installment_count = rs.getFloat("pending_installments");
                    Float previous_month_installemts = rs.getFloat("previous_month_pending_installments");
                    Date duetimeIstDate = rs.getDate("duetime_istdate");
                    Date lastpaymentmade = rs.getDate("lastpaymentmade");
                    String email = rs.getString("useremail");
                    String paymentstatus = rs.getString("paymentstatus");
//                    String email = "tes11t@test.com"; //Testing code

                    Map<String, String> installment = new HashMap<>();
                    if (userInstallments.containsKey(email)) {
                        installment = userInstallments.get(email);
                    }

                    installment.put("amount", amount.toString());
                    installment.put("total_due", total_due.toString());
                    installment.put("duetimeIstDate", duetimeIstDate.toString());
                    installment.put("paymentstatus", paymentstatus);

                    installment.put("lastpaymentmade", lastpaymentmade.toString());

                    installment.put("pending_installment_count", pending_installment_count.toString());
                    installment.put("previous_month_installemts", previous_month_installemts.toString());

                    userInstallments.put(email, installment);

                }
            }
        } catch (SQLException e) {
            logger.error("Error while processing installment");
            return;
        }
//        int count = 0; //Testing code
        for (String email : userInstallments.keySet()) {

            Map<String, String> installment = userInstallments.get(email);

            Map<String, String> param = new HashMap<>();
            param.put(ActivityEventConstants.EMAIL_ADDRESS, email);
            String lsOwnerEmail = leadsquaredUsers.getCallingUserEmailForLead(email);
            param.put(ActivityEventConstants.OWNER, lsOwnerEmail);
            param.put(ActivityEventConstants.NOTE, "Installment Reminder");
            param.put(ActivityEventConstants.MX_CUSTOM_1, installment.get("amount") != null ? installment.get("amount") : "0");
            param.put(ActivityEventConstants.MX_CUSTOM_2, installment.get("duetimeIstDate"));
            param.put(ActivityEventConstants.MX_CUSTOM_3, installment.get("pending_installment_count"));
            param.put(ActivityEventConstants.MX_CUSTOM_5, installment.get("previous_month_installemts") != null ? installment.get("previous_month_installemts") : "0");
            param.put(ActivityEventConstants.MX_CUSTOM_6, installment.get("total_due") != null ? installment.get("total_due") : "0");
            param.put(ActivityEventConstants.MX_CUSTOM_7, installment.get("lastpaymentmade"));
            param.put(ActivityEventConstants.MX_CUSTOM_8, installment.get("paymentstatus"));

            if (installment.get("amount") == null)
                continue;
            logger.info("Data to push into LS" + param.toString());
            activityManager.pushCareActivity(ActivityEventConstants.EVENT_INSTALLMENT_REMINDER, param);
//            if (count >= 1)   //Testing code
//                break;        //Testing code
//            count++;          //Testing code
        }

        leadsquaredActivityHelper.triggerAllActivities();


    }

    public void processAsync() {
        Map<String, Object> payload = new HashMap<>();
        AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.TRIGGER_LS_CARE_INSTALLMENT_ACTIVITIES, payload);
        asyncTaskFactory.executeTask(params);
    }
}
