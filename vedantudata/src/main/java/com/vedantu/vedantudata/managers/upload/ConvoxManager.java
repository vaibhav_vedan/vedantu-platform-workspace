package com.vedantu.vedantudata.managers.upload;

import java.io.File;
import java.io.FileNotFoundException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import javax.annotation.PostConstruct;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.NotFoundException;
import com.vedantu.util.CollectionUtils;
import com.vedantu.util.DateTimeUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.vedantudata.dao.serializers.ConvoxUploadDetailsDAO;
import com.vedantu.vedantudata.entities.ConvoxUploadDetails;
import com.vedantu.vedantudata.entities.UserEventData;
import com.vedantu.vedantudata.enums.UploadFileStatus;
import com.vedantu.vedantudata.enums.UserEventName;
import com.vedantu.vedantudata.enums.UserEventType;
import com.vedantu.vedantudata.managers.UserEventManager;
import com.vedantu.vedantudata.managers.leadsquared.LeadsquaredUserManager;
import com.vedantu.vedantudata.request.ConvoxUploadReq;

@Service
public class ConvoxManager {

	@Autowired
	private UserEventManager userEventManager;

	@Autowired
	private LeadsquaredUserManager leadsquaredUserManager;

	@Autowired
	private ConvoxUploadDetailsDAO convoxUploadDetailsDAO;

	@Autowired
	private CSVManager csvManager;

	@Autowired
	private LogFactory logFactory;

	@SuppressWarnings("static-access")
	private Logger logger = logFactory.getLogger(ConvoxManager.class);

	// 2017-03-27 11:00:15
	private static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

	private Map<String, Integer> CONVOX_UPLOAD_FIELDS_ALLOWED = new HashMap<>();

	private static final String CALL_DATE = "Call Date";
	private static final String AGENT_ID = "Agent ID";
	private static final String CUSTOMER_NAME = "Customer Name";
	private static final String PHONE_NUMBER = "Phone Number";
	private static final String STATUS = "Status";
	private static final String DURATION = "Duration";
	private static final String LIST_ID = "List Id";
	private static final String PHONE_ORDER_NO = "Phone Order No";
	private static final String DIALER_STATUS = "Dialer Status";

	@PostConstruct
	public void init() {
		logger.info("Initializing convox manager");
		sdf.setTimeZone(TimeZone.getTimeZone(DateTimeUtils.TIME_ZONE_GMT));

		CONVOX_UPLOAD_FIELDS_ALLOWED.put(CALL_DATE, null);
		CONVOX_UPLOAD_FIELDS_ALLOWED.put(AGENT_ID, null);
		CONVOX_UPLOAD_FIELDS_ALLOWED.put(CUSTOMER_NAME, null);
		CONVOX_UPLOAD_FIELDS_ALLOWED.put(PHONE_NUMBER, null);
		CONVOX_UPLOAD_FIELDS_ALLOWED.put(STATUS, null);
		CONVOX_UPLOAD_FIELDS_ALLOWED.put(DURATION, null);
		CONVOX_UPLOAD_FIELDS_ALLOWED.put(LIST_ID, null);
		CONVOX_UPLOAD_FIELDS_ALLOWED.put(PHONE_ORDER_NO, null);
		CONVOX_UPLOAD_FIELDS_ALLOWED.put(DIALER_STATUS, null);
	}

	public ConvoxUploadDetails uploadCallData(File file, ConvoxUploadReq convoxUploadReq)
			throws FileNotFoundException, NotFoundException, BadRequestException {
		logger.info("File name:" + file.getName() + " convoxUploadReq:" + convoxUploadReq.toString());
		List<String[]> rows = csvManager.parseCSVFile(file);
		if (CollectionUtils.isEmpty(rows) || rows.size() <= 1) {
			logger.info("No lead fields found");
			throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, "No entries found to update");
		}

		String[] fields = rows.get(0);

		// Validate the fields
		updateFieldIndexes(fields);

		ConvoxUploadDetails convoxUploadDetails = new ConvoxUploadDetails();
		convoxUploadDetails.setCount(rows.size() - 1);
		if (convoxUploadReq.getDate() != null) {
			convoxUploadDetails.setDate(timeToMillis(convoxUploadReq.getDate()));
		}
		convoxUploadDetails.setProcessName(convoxUploadReq.getProcessName());
		convoxUploadDetails.setFileName(file.getName());
		convoxUploadDetails.setUploadFileStatus(UploadFileStatus.PROCESSING);
		convoxUploadDetails.setFieldNames(Arrays.toString(fields));
		convoxUploadDetailsDAO.create(convoxUploadDetails);

		int rowsUpdated = 0;
		for (int i = 1; i < rows.size(); i++) {
			String[] row = rows.get(i);
			UserEventData userEventData = new UserEventData();
			userEventData.setUserEventName(UserEventName.OUTBOUND_CALL);
			userEventData.setUserEventType(UserEventType.CONVOX);
			if (CONVOX_UPLOAD_FIELDS_ALLOWED.get(DIALER_STATUS) != null
					&& CONVOX_UPLOAD_FIELDS_ALLOWED.get(DIALER_STATUS) >= 0
					&& !StringUtils.isEmpty(row[CONVOX_UPLOAD_FIELDS_ALLOWED.get(DIALER_STATUS)])) {
				userEventData.setUserEventStatus(row[CONVOX_UPLOAD_FIELDS_ALLOWED.get(DIALER_STATUS)]);
			} else {
				convoxUploadDetails.addErrorRowNumbers(i + ":" + DIALER_STATUS + " is not found or invalid");
				continue;
			}

			if (CONVOX_UPLOAD_FIELDS_ALLOWED.get(PHONE_NUMBER) != null
					&& CONVOX_UPLOAD_FIELDS_ALLOWED.get(PHONE_NUMBER) >= 0
					&& !StringUtils.isEmpty(row[CONVOX_UPLOAD_FIELDS_ALLOWED.get(PHONE_NUMBER)])) {
				userEventData.setReferenceId(row[CONVOX_UPLOAD_FIELDS_ALLOWED.get(PHONE_NUMBER)]);
			} else {
				convoxUploadDetails.addErrorRowNumbers(i + ":" + PHONE_NUMBER + " is not found or invalid");
				continue;
			}

			String leadsquaredId = null;
			if (CONVOX_UPLOAD_FIELDS_ALLOWED.get(AGENT_ID) != null && CONVOX_UPLOAD_FIELDS_ALLOWED.get(AGENT_ID) >= 0
					&& !StringUtils.isEmpty(row[CONVOX_UPLOAD_FIELDS_ALLOWED.get(AGENT_ID)])
					&& (leadsquaredId = leadsquaredUserManager.getLeadsquareduserIdByConvoxAgentId(
							row[CONVOX_UPLOAD_FIELDS_ALLOWED.get(AGENT_ID)])) != null) {
				userEventData.setReferenceAgentId(leadsquaredId);
			} else {
				convoxUploadDetails.addErrorRowNumbers(i + ":" + AGENT_ID + " is not found or invalid");
				continue;
			}

			if (CONVOX_UPLOAD_FIELDS_ALLOWED.get(DURATION) != null && CONVOX_UPLOAD_FIELDS_ALLOWED.get(DURATION) >= 0
					&& !StringUtils.isEmpty(row[CONVOX_UPLOAD_FIELDS_ALLOWED.get(DURATION)])) {
				userEventData.setReferenceValue(parseDuration(row[CONVOX_UPLOAD_FIELDS_ALLOWED.get(DURATION)]));
			} else {
				convoxUploadDetails.addErrorRowNumbers(i + ":" + DURATION + " is not found or invalid");
				continue;
			}

			// private String eventNote;
			// private Long eventTime;

			if (CONVOX_UPLOAD_FIELDS_ALLOWED.get(CALL_DATE) != null && CONVOX_UPLOAD_FIELDS_ALLOWED.get(CALL_DATE) >= 0
					&& !StringUtils.isEmpty(row[CONVOX_UPLOAD_FIELDS_ALLOWED.get(CALL_DATE)])) {
				try {
					userEventData.setEventTime(parseCallDate(row[CONVOX_UPLOAD_FIELDS_ALLOWED.get(CALL_DATE)]));
				} catch (ParseException ex) {
					convoxUploadDetails.addErrorRowNumbers(i + ":" + CALL_DATE + " is cannot be parsed");
					continue;
				}
			} else {
				convoxUploadDetails.addErrorRowNumbers(i + ":" + CALL_DATE + " is not found or invalid");
				continue;
			}

			userEventData.setEventNote(
					"Pushed via file:" + file.getName() + " convoxUploadDetails:" + convoxUploadDetails.getId());
			userEventManager.updateConvoxEvent(userEventData);
			rowsUpdated++;
		}

		convoxUploadDetails.setUpdatedRows(rowsUpdated);
		if (!CollectionUtils.isEmpty(convoxUploadDetails.getErrorRowNumbers())) {
			convoxUploadDetails.setUploadFileStatus(UploadFileStatus.PROCESSED);
		} else {
			convoxUploadDetails.setUploadFileStatus(UploadFileStatus.SUCCESSFUL);
		}
		convoxUploadDetailsDAO.create(convoxUploadDetails);
		return convoxUploadDetails;
	}

	private void updateFieldIndexes(String[] fields) throws BadRequestException {
		// Validate convox field names
		if (fields.length > 0) {
			for (int i = 0; i < fields.length; i++) {
				if (CONVOX_UPLOAD_FIELDS_ALLOWED.containsKey(fields[i])) {
					CONVOX_UPLOAD_FIELDS_ALLOWED.put(fields[i], Integer.valueOf(i));
				} else if (!"S. No.".equals(fields[i])) {
					throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR,
							"Invalid field name: " + fields[i] + " allowed field names:" + Arrays.toString(fields));
				}
			}
		}
	}

	private long parseCallDate(String callDate) throws ParseException {
		Date date = sdf.parse(callDate);
		return date.getTime();
	}

	private long timeToMillis(String date) throws BadRequestException {
		try {
			Date dateObject = sdf.parse(date);
			return dateObject.getTime();
		} catch (Exception ex) {
			String message = "Error with the time specified : " + ex.toString() + ". Date format should be :"
					+ sdf.toPattern();
			logger.info(message);
			throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, message);
		}
	}

	private long parseDuration(String duration) {
		long totalDuration = 0;
		String[] durationArr = duration.split(":");
		if (durationArr != null) {
			for (int i = durationArr.length - 1, j = 0; i >= 0; i--, j++) {
				totalDuration += Long.valueOf(durationArr[j]) * Math.pow(60, i);
			}
		}

		return totalDuration;
	}
}
