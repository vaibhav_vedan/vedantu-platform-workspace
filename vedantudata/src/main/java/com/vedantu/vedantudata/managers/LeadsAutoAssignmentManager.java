package com.vedantu.vedantudata.managers;


import com.google.gson.Gson;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.User.Role;
import com.vedantu.async.AsyncTaskFactory;
import com.vedantu.async.AsyncTaskParams;
import com.vedantu.aws.pojo.AWSSNSSubscriptionRequest;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.InternalServerErrorException;
import com.vedantu.exception.VException;
import com.vedantu.notification.enums.CommunicationType;
import com.vedantu.notification.requests.EmailRequest;
import com.vedantu.util.*;
import com.vedantu.vedantudata.async.AsyncTaskName;
import com.vedantu.vedantudata.dao.LeadAutoAssignmentDAO;
import com.vedantu.vedantudata.entities.leadsquared.AssignedLeads;
import com.vedantu.vedantudata.pojos.CounsellorDetails;
import com.vedantu.vedantudata.pojos.FreshLeadDetails;
import com.vedantu.vedantudata.entities.leadsquared.LeadActualAssignment;
import com.vedantu.vedantudata.utils.LeadAutoAssignmentQueries;
import com.vedantu.vedantudata.utils.RedisDAO;
import com.vedantu.vedantudata.utils.VedantudataPostgressHandler;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;


@Service
public class LeadsAutoAssignmentManager {

    @Autowired
    private LogFactory logFactory;

    @Autowired
    private CommunicationManager communicationManager;

    @Autowired
    private VedantudataPostgressHandler vedantudataPostgressHandler;

    @Autowired
    private LeadAutoAssignmentDAO leadAutoAssignmentDAO;

    @Autowired
    private AwsS3Manager awsS3Manager;

    @Autowired
    private AsyncTaskFactory asyncTaskFactory;

    @Autowired
    private RedisDAO redisDAO;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(LeadsAutoAssignmentManager.class);

    private static final String ENV = ConfigUtils.INSTANCE.getStringValue("environment").toLowerCase();
    private static final String S3_BUCKET = ConfigUtils.INSTANCE.getStringValue("aws.s3.bucket");
    private static final String adminsString = ConfigUtils.INSTANCE.getStringValue("auto.lead.assignment.admins");
    private static final String mainAdmin = ConfigUtils.INSTANCE.getStringValue("auto.lead.assignment.main.admin");
    private static final String chAdmin = ConfigUtils.INSTANCE.getStringValue("auto.lead.assignment.ch.admin");
    private static final String csvKey = ENV + "/temp/sales/autoleadassignment/csv/" + LocalDate.now().format(DateTimeFormatter.BASIC_ISO_DATE) + "/" +
            UUID.randomUUID().toString() + ".csv";
    private static final int loopingCount = 6;
    private static final int queryLimit = 20000;
    private static final int queryLimitNonCore = 50000;
    private static final DateFormat dtf = new SimpleDateFormat("yyyy-MM-dd");
    private static final DateFormat dtf1 = new SimpleDateFormat("ddMMMyyyy");

    public void assignLeadstoCounsellorsFromCron(String request, String messageType) throws VException, SQLException, AddressException, ParseException {
        Gson gson = new Gson();
        AWSSNSSubscriptionRequest subscriptionRequest = gson.fromJson(request, AWSSNSSubscriptionRequest.class);
        if (messageType.equals("SubscriptionConfirmation")) {
            String json = null;
            logger.info("SubscribeURL = " + subscriptionRequest.getSubscribeURL());
            ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionRequest.getSubscribeURL(), HttpMethod.GET, json, false);
            logger.info(resp.getEntity(String.class));
        } else if (messageType.equals("Notification")) {
            logger.info("Notification received - SNS");
            logger.info(subscriptionRequest.toString());
            if (StringUtils.isEmpty(redisDAO.get("IS_AUTO_LEAD_ASSIGNMENT_COMPLETED")) || !org.apache.commons.lang.StringUtils.equals(redisDAO.get("IS_AUTO_LEAD_ASSIGNMENT_COMPLETED"), "true")) {
                processAsync();
            }
        }

    }

    public void processAsync() {
        Map<String, Object> payload = new HashMap<>();
        AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.TRIGGER_AUTO_LEAD_ASSIGNMENT, payload);
        asyncTaskFactory.executeTask(params);
    }

    /**
     * Pushes the assigned data to mongo db asynchronously
     *
     * @param assignedLeads
     */
    public void processAsync(List<AssignedLeads> assignedLeads) {
        Map<String, Object> payload = new HashMap<>();
        payload.put("assignLead", assignedLeads);
        AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.TRIGGER_AUTO_LEAD_ASSIGNMENT_DB, payload);
        asyncTaskFactory.executeTask(params);
    }

    /**
     * Gets adminId by email
     *
     * @param email
     * @return
     * @throws SQLException
     * @throws VException
     */
    private String getUserIdByEmail(String email) throws SQLException, VException {
        String queryToGetTheAdminId = "select userid from leadsquared.user_base where emailaddress = '" + email + "';";
        ResultSet adminResultSet = vedantudataPostgressHandler.executeQuery(queryToGetTheAdminId);

        String adminuserId = null;
        if (adminResultSet != null && adminResultSet.next()) {
            adminuserId = adminResultSet.getString("userid");
        }
        adminResultSet.close();
        return adminuserId;
    }

    /**
     * Assigns pulled back leads from non-active counsellors and assign to respective admins and managers
     *
     * @return
     * @throws SQLException
     * @throws VException
     */
    private List<AssignedLeads> assignPulledBackLeadsOfInactiveCounsellors(Path page, Map<String, String> headerMap) throws SQLException, VException {
        String query = LeadAutoAssignmentQueries.queryToFetchPulledBackLeads + "limit " + queryLimit;

        String adminuserId = getUserIdByEmail(mainAdmin);
        String chAdminUserId = getUserIdByEmail(chAdmin);
        List<AssignedLeads> assignedLeadsList = new ArrayList<>();
        int count = 0;

        Calendar currentDate = Calendar.getInstance();
        currentDate.setTimeInMillis(System.currentTimeMillis() + DateTimeUtils.IST_TIME_DIFFERENCE_NEGATIVE);
        Date now = currentDate.getTime();
        boolean isContinue = true;
        do {
            int start = count * queryLimit;
            ResultSet rs = vedantudataPostgressHandler.executeQuery(query + " offset " + start);
            List<FreshLeadDetails> freshLeadDetailsList = new ArrayList<>();

            if (Objects.nonNull(rs)) {
                while (rs.next()) {
                    addDataToFreshLeadsList(rs, freshLeadDetailsList, null, true, false);
                }
            }
            rs.close();
            if (ArrayUtils.isEmpty(freshLeadDetailsList)) {
                isContinue = false;
            }

            for (FreshLeadDetails freshLeadDetails : freshLeadDetailsList) {
                AssignedLeads assignedLeads = new AssignedLeads();
                if (StringUtils.isNotEmpty(freshLeadDetails.getManagerUserId())) {
                    if ("CENTER_HEAD".equals(freshLeadDetails.getOwnerDesignation())) {
                        assignedLeads.setOwnerId(chAdminUserId);
                    } else {
                        assignedLeads.setOwnerId(freshLeadDetails.getManagerUserId());
                    }
                } else {
                    assignedLeads.setOwnerId(adminuserId);
                }
                addAssignLeadDetails(now, freshLeadDetails, assignedLeads, freshLeadDetails.getCentreCode(), null);

                assignedLeadsList.add(assignedLeads);
            }
            count += 1;
        } while (isContinue);
        generateCsv(assignedLeadsList, page, headerMap);
        processAsync(assignedLeadsList);
        return assignedLeadsList;
    }

    /**
     * Sets the assigned lead details into a object
     *
     * @param now
     * @param freshLeadDetails
     * @param assignedLeads
     * @param centreCode
     * @param query
     */
    private void addAssignLeadDetails(Date now, FreshLeadDetails freshLeadDetails, AssignedLeads assignedLeads, String centreCode, Integer query) {

        assignedLeads.setPhone(freshLeadDetails.getPhone());
        assignedLeads.setGrade(org.apache.commons.lang.StringUtils.defaultString(freshLeadDetails.getGrade(), ""));
        String firstName = null;
        if (StringUtils.isNotEmpty(freshLeadDetails.getFirstName())) {
            firstName = freshLeadDetails.getFirstName().replaceAll(",", "");
        }
        assignedLeads.setMx_VC_code("");
        assignedLeads.setFirstName(org.apache.commons.lang.StringUtils.defaultString(firstName, ""));
        assignedLeads.setMx_Location_type("");
        assignedLeads.setMx_city("");
        if (StringUtils.isEmpty(freshLeadDetails.getAssignmentType())) {
            freshLeadDetails.setAssignmentType("AppNC");
        }

        assignedLeads.setMx_Sub_Stage("");
        assignedLeads.setProspectStage("1b. Fresh Sign up");
        assignedLeads.setMx_Last_Assigned_Date(dtf.format(now) + "");
        assignedLeads.setMx_User_Target("");
        assignedLeads.setMx_Core_Noncore(freshLeadDetails.getMxCoreNonCore());
        assignedLeads.setMx_Week("");
        assignedLeads.setMx_CC_LT("");
        assignedLeads.setMx_Assigned_Count("");
        assignedLeads.setMxScore(org.apache.commons.lang.StringUtils.defaultString(freshLeadDetails.getLeadScore(), ""));
        assignedLeads.setScoreId(org.apache.commons.lang.StringUtils.defaultString(freshLeadDetails.getScoreId(), ""));
        assignedLeads.setLangugage(org.apache.commons.lang.StringUtils.defaultString(freshLeadDetails.getLanguage(), ""));
        assignedLeads.setLastUpdatedDate(now);
        assignedLeads.setLocation(org.apache.commons.lang.StringUtils.defaultString(freshLeadDetails.getLocation(), ""));
        if (Objects.nonNull(query)) {
            if (query == 1) {
                assignedLeads.setMx_Board(org.apache.commons.lang.StringUtils.defaultString(freshLeadDetails.getStudentInfoBoard(), ""));
                assignedLeads.setCategory("admin bucket");
            } else if (query == 2) {
                assignedLeads.setMx_Board(org.apache.commons.lang.StringUtils.defaultString(freshLeadDetails.getStudentInfoBoard(), ""));
                assignedLeads.setCategory("counsellor bucket");
            } else if (query == 3) {
                assignedLeads.setMx_Board(org.apache.commons.lang.StringUtils.defaultString(freshLeadDetails.getStudentInfoBoard(), ""));
                assignedLeads.setCategory("parth's bucket");
            } else if (query == 4) {
                assignedLeads.setMx_Board("");
                assignedLeads.setCategory("non core");
            }
            String vcLead = freshLeadDetails.getAssignmentType() + "_" + centreCode + "_" + dtf1.format(now);
            assignedLeads.setMx_vc_lead(vcLead);
        } else {
            assignedLeads.setMx_Board("");
            assignedLeads.setCategory("pulled Back");
            assignedLeads.setMx_vc_lead("");
            assignedLeads.setProspectStage("");
        }
    }

    /**
     * pushing leads data to a List
     *
     * @param rs
     * @param freshLeadDetailsList
     * @param isPulledBack
     * @param isInActiveCounsellors
     * @param isNonCore
     * @throws SQLException
     */
    private void addDataToFreshLeadsList(ResultSet rs, List<FreshLeadDetails> freshLeadDetailsList, Boolean isPulledBack, boolean isInActiveCounsellors, boolean isNonCore) throws SQLException {

        FreshLeadDetails freshLeadDetails = new FreshLeadDetails();
        freshLeadDetails.setGrade(rs.getString("grade"));
        freshLeadDetails.setPhone(rs.getString("phone"));
        freshLeadDetails.setFirstName(rs.getString("firstName"));
        freshLeadDetails.setLeadScore(rs.getString("lead_score"));
        freshLeadDetails.setProspectId(rs.getString("prospectid"));
        freshLeadDetails.setLanguage(rs.getString("lead_language"));
        if (isInActiveCounsellors) {
            freshLeadDetails.setOwnerDesignation(rs.getString("designation"));
            if (rs.getString("isFreshLead").equals("f") && isInActiveCounsellors) {
                freshLeadDetails.setManagerUserId(rs.getString("manageruserid"));
            }
        }
        freshLeadDetails.setAssignmentType(rs.getString("assignment_type"));
        freshLeadDetails.setMxCoreNonCore(rs.getString("mx_core_nonCore"));
        freshLeadDetails.setScoreId(rs.getString("scoreid"));


        if (Objects.nonNull(isPulledBack)) {
            if (isPulledBack) {
                freshLeadDetails.setIsFreshLead(false);
            } else {
                freshLeadDetails.setIsFreshLead(true);
                freshLeadDetails.setLocation(rs.getString("locationinfo_state"));
            }
            if ("state board".equals(rs.getString("studentinfo_board"))) {
                freshLeadDetails.setIsStateBoard(true);
            } else {
                freshLeadDetails.setIsStateBoard(false);
            }
            if (!isNonCore) {
                freshLeadDetails.setStudentInfoBoard(rs.getString("studentinfo_board"));
            } else {
                freshLeadDetails.setStudentInfoBoard("");
            }
        }
        freshLeadDetailsList.add(freshLeadDetails);
    }

    /**
     * Gets the leads from redshift using a query
     *
     * @param query
     * @param count
     * @param isPulledBack
     * @param isNonCore
     * @return
     * @throws SQLException
     * @throws VException
     */
    private List<FreshLeadDetails> getLeadsFromRedshiftUsingQuery(String query, int count, boolean isPulledBack, boolean isNonCore, String assignedLeadsString) throws SQLException, VException {

        List<FreshLeadDetails> freshLeadDetailsList = new ArrayList<>();
        int start = 0;
        if (isNonCore) {
            start = count * queryLimitNonCore;
        } else {
            start = count * queryLimit;
        }
        if (StringUtils.isNotEmpty(assignedLeadsString)) {
            query = query.replace("{}", assignedLeadsString);
        }

        String finalQuery = query + " offset " + start + ";";

        ResultSet rs = vedantudataPostgressHandler.executeQuery(finalQuery);

        if (Objects.nonNull(rs)) {
            while (rs.next()) {
                addDataToFreshLeadsList(rs, freshLeadDetailsList, isPulledBack, false, isNonCore);
            }
        }
        rs.close();
        return freshLeadDetailsList;
    }

    private LeadActualAssignment leadActualAssignment(CounsellorDetails counsellorDetails, Long dateInMills) {

        LeadActualAssignment leadActualAssignment = new LeadActualAssignment();
        leadActualAssignment.setCounsellorId(counsellorDetails.getCounsellor_id());
        leadActualAssignment.setBucketCount(counsellorDetails.getBuckentCount());
        leadActualAssignment.setAssignedLeadsCount(counsellorDetails.getAssignLeads());
        leadActualAssignment.setFreshAssignedCount(counsellorDetails.getFreshLeadsAssigned());
        leadActualAssignment.setChurnAssignmentCount(counsellorDetails.getChurnLeadsAssigned());
        leadActualAssignment.setStateBoardCount(counsellorDetails.getStateBoardLeadsCount());
        leadActualAssignment.setNonStateBoardCount(counsellorDetails.getNonStateBoardLeadsCount());
        leadActualAssignment.setRequirement(Integer.parseInt(counsellorDetails.getDay_wise_requirement()));
        leadActualAssignment.setDate(dateInMills);
        leadActualAssignment.setLanguagePreference1(counsellorDetails.getLanguage_preference_1());
        leadActualAssignment.setLanguagePreference2(counsellorDetails.getLanguage_preference_2());
        leadActualAssignment.setGradePreference1(counsellorDetails.getGrade_preference_1());
        leadActualAssignment.setGradePreference2(counsellorDetails.getGrade_preference_2());
        leadActualAssignment.setGradePreference3(counsellorDetails.getGrade_preference_3());
        leadActualAssignment.setLeadTypePreference1(counsellorDetails.getLead_type_preference_1());
        leadActualAssignment.setLeadTypePreference2(counsellorDetails.getLead_type_preference_2());
        leadActualAssignment.setLeadTypePreference3(counsellorDetails.getLead_type_preference_3());
        leadActualAssignment.setDefaultLanguage(counsellorDetails.getDefaultLanguage());
        leadActualAssignment.setLastUpdatedDate(new Date(System.currentTimeMillis() + DateTimeUtils.IST_TIME_DIFFERENCE_NEGATIVE));
        return leadActualAssignment;
    }

    @Async
    void pushLeadActualAssignmentDataToDB(Map<String, CounsellorDetails> counsellorDetailsMap) throws ParseException {
        Date date = new Date();
        Long dateInMills = date.getTime();
        List<LeadActualAssignment> leadActualAssignments = counsellorDetailsMap.entrySet().parallelStream()
                .filter(Objects::nonNull)
                .map(data -> leadActualAssignment(data.getValue(), dateInMills))
                .collect(Collectors.toList());

        leadAutoAssignmentDAO.pushLeadActualAssignmentDateToDB(leadActualAssignments);

    }


    public void pushAssignedleadsToDb(List<AssignedLeads> assignedLeadsList) {
        for (AssignedLeads assignedLeads : assignedLeadsList) {
            leadAutoAssignmentDAO.pushDataToDb(assignedLeads);
        }

    }

    private void generateCsv(List<AssignedLeads> assignedLeadsList, Path page, Map<String, String> headerMap) {

        try {
            Files.write(page, getCsvBytes(assignedLeadsList, headerMap), StandardOpenOption.CREATE, StandardOpenOption.APPEND);
        } catch (IOException e) {
            logger.warn("Exception while writing data into csv");
        }
    }

    private byte[] getCsvBytes(List<AssignedLeads> assignedLeadsList, Map<String, String> headerMap) {
        List<Map<String, String>> list = new ArrayList<>();
        for (AssignedLeads assignedLeads : assignedLeadsList) {
            Map<String, String> map = new LinkedHashMap<>();
            for (Map.Entry<String, String> entry : headerMap.entrySet()) {
                String key = entry.getKey();
                if ("firstName".equals(key)) {
                    map.put(key, assignedLeads.getFirstName());
                }
                if ("ownerId".equals(key)) {
                    map.put(key, assignedLeads.getOwnerId());
                }
                if ("phone".equals(key)) {
                    map.put(key, assignedLeads.getPhone());
                }
                if ("mx_VC_Code".equals(key)) {
                    map.put(key, assignedLeads.getMx_VC_code());
                }
                if ("grade".equals(key)) {
                    map.put(key, assignedLeads.getGrade());
                }
                if ("mx_Location_type".equals(key)) {
                    map.put(key, assignedLeads.getMx_Location_type());
                }
                if ("mx_City".equals(key)) {
                    map.put(key, assignedLeads.getMx_city());
                }
                if ("mx_VCLead".equals(key)) {
                    map.put(key, assignedLeads.getMx_vc_lead());
                }
                if ("mx_Sub_Stage".equals(key)) {
                    map.put(key, assignedLeads.getMx_Sub_Stage());
                }
                if ("prospectStage".equals(key)) {
                    map.put(key, assignedLeads.getProspectStage());
                }
                if ("mx_Last_Assigned_Date".equals(key)) {
                    map.put(key, assignedLeads.getMx_Last_Assigned_Date());
                }
                if ("mx_User_Target".equals(key)) {
                    map.put(key, assignedLeads.getMx_User_Target());
                }
                if ("mx_Core_Noncore".equals(key)) {
                    map.put(key, assignedLeads.getMx_Core_Noncore());
                }
                if ("mx_Week".equals(key)) {
                    map.put(key, assignedLeads.getMx_Week());
                }
                if ("mx_CC_LT".equals(key)) {
                    map.put(key, assignedLeads.getMx_CC_LT());
                }
                if ("mx_Assigned_Count".equals(key)) {
                    map.put(key, assignedLeads.getMx_Assigned_Count());
                }
                if ("mx_Board".equals(key)) {
                    map.put(key, assignedLeads.getMx_Board());
                }
                if ("mx_Score_Of_Lead".equals(key)) {
                    map.put(key, assignedLeads.getMxScore());
                }
                if ("mx_ScoreID".equals(key)) {
                    map.put(key, assignedLeads.getScoreId());
                }
                if ("language".equals(key)) {
                    map.put(key, assignedLeads.getLangugage());
                }
                if ("category".equals(key)) {
                    map.put(key, assignedLeads.getCategory());
                }
                if ("location".equals(key)) {
                    map.put(key, assignedLeads.getLocation());
                }

            }
            list.add(map);
        }
        StringBuilder builder = new StringBuilder();
        for (Map<String, String> map : list) {
            builder.append(map.values()
                    .stream()
                    .map(e -> "\"" + e + "\"")
                    .collect(Collectors.joining(",")))
                    .append(System.lineSeparator());
        }
        return builder.toString().getBytes();
    }

    private Map<String, String> getCsvHeaderMap() {
        Map<String, String> map = new LinkedHashMap<>();
        map.put("phone", "Phone");
        map.put("grade", "Grade");
        map.put("firstName", "FirstName");
        map.put("mx_VC_Code", "mx_VC_Code");
        map.put("ownerId", "OwnerId");
        map.put("mx_Location_type", "mx_Location_type");
        map.put("mx_City", "mx_City");
        map.put("mx_VCLead", "mx_VCLead");
        map.put("mx_Sub_Stage", "mx_Sub_Stage");
        map.put("prospectStage", "ProspectStage");
        map.put("mx_Last_Assigned_Date", "mx_Last_Assigned_Date");
        map.put("mx_User_Target", "mx_User_Target");
        map.put("mx_Core_Noncore", "mx_Core_Noncore");
        map.put("mx_Week", "mx_Week");
        map.put("mx_CC_LT", "mx_CC_LT");
        map.put("mx_Assigned_Count", "mx_Assigned_Count");
        map.put("mx_Board", "mx_Board");
        map.put("mx_Score_Of_Lead", "mx_Score_Of_Lead");
        map.put("mx_ScoreID", "mx_ScoreID");
        map.put("language", "language");
        map.put("category", "category");
        map.put("location", "location");
        return map;
    }

    /**
     * It categorizes the counsellors w.r.t language,grade and cohort value
     *
     * @param preferenceMaps
     * @param gradeSet
     * @throws SQLException
     * @throws VException
     */
    private void categorizeCounsellors(List<Map<String, Map<String, List<String>>>> preferenceMaps, Set<String> gradeSet, Map<String, Map<String, List<String>>> locationMap) throws SQLException, VException {
        Set<String> languageSet = new HashSet();

        //Getting all the distinct grades of counsellors
        ResultSet rsForGrades = vedantudataPostgressHandler.executeQuery(LeadAutoAssignmentQueries.queryToGetAllGrades);
        if (Objects.nonNull(rsForGrades)) {
            while (rsForGrades.next()) {
                if (StringUtils.isNotEmpty(rsForGrades.getString("grade_preference_1"))) {
                    gradeSet.add(rsForGrades.getString("grade_preference_1"));
                }
            }
            rsForGrades.close();
        }

        ResultSet rs = vedantudataPostgressHandler.executeQuery(LeadAutoAssignmentQueries.queryToGetAllLanguages);
        while (rs.next()) {
            if (StringUtils.isNotEmpty(rs.getString("language_preference_1")) && !"0".equals(rs.getString("language_preference_1")) && !"#N/A".equals(rs.getString("language_preference_1"))) {
                languageSet.add(rs.getString("language_preference_1"));
            }
        }
        rs.close();
        Set<String> locationsSet = new HashSet<>();
        ResultSet rsForLocations = vedantudataPostgressHandler.executeQuery(LeadAutoAssignmentQueries.queryToGetAllLocations);
        while (rsForLocations.next()) {
            locationsSet.add(rsForLocations.getString("location_preference"));
        }
        rsForLocations.close();

        for (int i = 0; i < 3; i++) {
            Map<String, Map<String, List<String>>> locationGradeMap = new HashMap<>();
            for (String location : locationsSet) {
                if (i == 0) {
                    Map<String, List<String>> cohortMap = new HashMap<>();
                    List<String> counsellorIdsList = new ArrayList<>();
                    cohortMap.put("M6", counsellorIdsList);
                    cohortMap.put("M5+", counsellorIdsList);
                    cohortMap.put("M5", counsellorIdsList);
                    cohortMap.put("M4", counsellorIdsList);
                    cohortMap.put("M3", counsellorIdsList);
                    cohortMap.put("M2", counsellorIdsList);
                    cohortMap.put("M1", counsellorIdsList);
                    cohortMap.put("M0", counsellorIdsList);
                    cohortMap.put("noCohort", counsellorIdsList);
                    locationMap.put(location, cohortMap);
                }


                for (String grade : gradeSet) {
                    //creating hashmaps for cohort value and counsellors mapping
                    Map<String, List<String>> cohortMap = new HashMap<>();
                    List<String> counsellorIdsList = new ArrayList<>();
                    cohortMap.put("M6", counsellorIdsList);
                    cohortMap.put("M5+", counsellorIdsList);
                    cohortMap.put("M5", counsellorIdsList);
                    cohortMap.put("M4", counsellorIdsList);
                    cohortMap.put("M3", counsellorIdsList);
                    cohortMap.put("M2", counsellorIdsList);
                    cohortMap.put("M1", counsellorIdsList);
                    cohortMap.put("M0", counsellorIdsList);
                    cohortMap.put("noCohort", counsellorIdsList);
                    locationGradeMap.put(location + "_" + grade, cohortMap);

                }
            }
            preferenceMaps.add(locationGradeMap);
        }

        //adding language_grade keys to preference sets as hierarchy needs to be considered
        for (int i = 0; i < loopingCount; i++) {

            //creating hashmaps key : language_grade ,value :  Map<String, List<CounsellorDetails>>
            Map<String, Map<String, List<String>>> languageGradeMap = new HashMap<>();
            for (String language : languageSet) {
                if (i == 0) {
                    Map<String, List<String>> cohortMap = new HashMap<>();
                    List<String> counsellorIdsList = new ArrayList<>();
                    cohortMap.put("M6", counsellorIdsList);
                    cohortMap.put("M5+", counsellorIdsList);
                    cohortMap.put("M5", counsellorIdsList);
                    cohortMap.put("M4", counsellorIdsList);
                    cohortMap.put("M3", counsellorIdsList);
                    cohortMap.put("M2", counsellorIdsList);
                    cohortMap.put("M1", counsellorIdsList);
                    cohortMap.put("M0", counsellorIdsList);
                    cohortMap.put("noCohort", counsellorIdsList);
                    locationMap.put(language, cohortMap);
                }
                for (String grade : gradeSet) {
                    //creating hashmaps for cohort value and counsellors mapping
                    Map<String, List<String>> cohortMap = new HashMap<>();
                    List<String> counsellorIdsList = new ArrayList<>();
                    cohortMap.put("M6", counsellorIdsList);
                    cohortMap.put("M5+", counsellorIdsList);
                    cohortMap.put("M5", counsellorIdsList);
                    cohortMap.put("M4", counsellorIdsList);
                    cohortMap.put("M3", counsellorIdsList);
                    cohortMap.put("M2", counsellorIdsList);
                    cohortMap.put("M1", counsellorIdsList);
                    cohortMap.put("M0", counsellorIdsList);
                    cohortMap.put("noCohort", counsellorIdsList);
                    languageGradeMap.put(language + "_" + grade, cohortMap);


                }
            }
            preferenceMaps.add(languageGradeMap);
        }


    }

    /**
     * Fetches the counsellors from redshift
     *
     * @param counsellorDetailsMap
     * @param preferenceMaps
     * @param counsellorsSet
     * @throws SQLException
     * @throws VException
     */
    private void fetchCounsellors(Map<String, CounsellorDetails> counsellorDetailsMap, List<Map<String, Map<String, List<String>>>> preferenceMaps, Set<String> counsellorsSet, Map<String, Map<String, List<String>>> locaionMap) throws SQLException, VException {
        logger.info("fetchCounsellors entry...");
        String query = LeadAutoAssignmentQueries.queryToFetchCounsellors;
        ResultSet rs = vedantudataPostgressHandler.executeQuery(query);

        if (rs != null && rs.isBeforeFirst()) {
            while (rs.next()) {
                CounsellorDetails counsellorDetails = getCounsellorDetails(rs);
                counsellorDetailsMap.put(counsellorDetails.getCounsellor_id(), counsellorDetails);
                if (StringUtils.isNotEmpty(counsellorDetails.getLocationPreference())) {
                    Map<String, List<String>> cohortMap = locaionMap.get(counsellorDetails.getLocationPreference());
                    List<String> counsellorIds = cohortMap.get(counsellorDetails.getCohort());
                    counsellorIds.add(counsellorDetails.getCounsellor_id());
                    cohortMap.put(counsellorDetails.getCohort(), counsellorIds);
                    locaionMap.put(counsellorDetails.getLocationPreference(), cohortMap);
                }

                if (StringUtils.isNotEmpty(counsellorDetails.getLanguage_preference_1())) {
                    Map<String, List<String>> cohortMap = locaionMap.get(counsellorDetails.getLanguage_preference_1());
                    List<String> counsellorIds = cohortMap.get(counsellorDetails.getCohort());
                    counsellorIds.add(counsellorDetails.getCounsellor_id());
                    cohortMap.put(counsellorDetails.getCohort(), counsellorIds);
                    locaionMap.put(counsellorDetails.getLanguage_preference_1(), cohortMap);
                }
                if (StringUtils.isNotEmpty(counsellorDetails.getLanguage_preference_2())) {
                    Map<String, List<String>> cohortMap = locaionMap.get(counsellorDetails.getLanguage_preference_2());
                    List<String> counsellorIds = cohortMap.get(counsellorDetails.getCohort());
                    counsellorIds.add(counsellorDetails.getCounsellor_id());
                    cohortMap.put(counsellorDetails.getCohort(), counsellorIds);
                    locaionMap.put(counsellorDetails.getLanguage_preference_2(), cohortMap);
                }

                if (StringUtils.isNotEmpty(counsellorDetails.getLocationPreference()) || StringUtils.isNotEmpty(counsellorDetails.getLanguage_preference_1()) || StringUtils.isNotEmpty(counsellorDetails.getLanguage_preference_2())) {
                    counsellorsSet.add(counsellorDetails.getCounsellor_id());
                }

                if (StringUtils.isNotEmpty(counsellorDetails.getLocationPreference()) && StringUtils.isNotEmpty(counsellorDetails.getGrade_preference_1())) {
                    String key = counsellorDetails.getLocationPreference() + "_" + counsellorDetails.getGrade_preference_1();
                    addCounsellorIdToLanguageGradeMap(counsellorDetails, key, preferenceMaps.get(0));

                }
                if (StringUtils.isNotEmpty(counsellorDetails.getLocationPreference()) && StringUtils.isNotEmpty(counsellorDetails.getGrade_preference_2())) {
                    String key = counsellorDetails.getLocationPreference() + "_" + counsellorDetails.getGrade_preference_2();
                    addCounsellorIdToLanguageGradeMap(counsellorDetails, key, preferenceMaps.get(1));
                }
                if (StringUtils.isNotEmpty(counsellorDetails.getLocationPreference()) && StringUtils.isNotEmpty(counsellorDetails.getGrade_preference_3())) {
                    String key = counsellorDetails.getLocationPreference() + "_" + counsellorDetails.getGrade_preference_3();
                    addCounsellorIdToLanguageGradeMap(counsellorDetails, key, preferenceMaps.get(2));
                }

                //since there are 2 language prefernces and 3 grade preferences we have six conditions/combinations
                if (StringUtils.isNotEmpty(counsellorDetails.getLanguage_preference_1()) && StringUtils.isNotEmpty(counsellorDetails.getGrade_preference_1())) {
                    String key1 = counsellorDetails.getLanguage_preference_1() + "_" + counsellorDetails.getGrade_preference_1();
                    addCounsellorIdToLanguageGradeMap(counsellorDetails, key1, preferenceMaps.get(3));
                }
                if (StringUtils.isNotEmpty(counsellorDetails.getLanguage_preference_1()) && StringUtils.isNotEmpty(counsellorDetails.getGrade_preference_2())) {
                    String key = counsellorDetails.getLanguage_preference_1() + "_" + counsellorDetails.getGrade_preference_2();
                    addCounsellorIdToLanguageGradeMap(counsellorDetails, key, preferenceMaps.get(4));
                }
                if (StringUtils.isNotEmpty(counsellorDetails.getLanguage_preference_1()) && StringUtils.isNotEmpty(counsellorDetails.getGrade_preference_3())) {
                    String key = counsellorDetails.getLanguage_preference_1() + "_" + counsellorDetails.getGrade_preference_3();
                    addCounsellorIdToLanguageGradeMap(counsellorDetails, key, preferenceMaps.get(5));
                }
                if (StringUtils.isNotEmpty(counsellorDetails.getLanguage_preference_2()) && StringUtils.isNotEmpty(counsellorDetails.getGrade_preference_1())) {
                    String key = counsellorDetails.getLanguage_preference_2() + "_" + counsellorDetails.getGrade_preference_1();
                    addCounsellorIdToLanguageGradeMap(counsellorDetails, key, preferenceMaps.get(6));
                }
                if (StringUtils.isNotEmpty(counsellorDetails.getLanguage_preference_2()) && StringUtils.isNotEmpty(counsellorDetails.getGrade_preference_2())) {
                    String key = counsellorDetails.getLanguage_preference_2() + "_" + counsellorDetails.getGrade_preference_2();
                    addCounsellorIdToLanguageGradeMap(counsellorDetails, key, preferenceMaps.get(7));
                }
                if (StringUtils.isNotEmpty(counsellorDetails.getLanguage_preference_2()) && StringUtils.isNotEmpty(counsellorDetails.getGrade_preference_3())) {
                    String key = counsellorDetails.getLanguage_preference_2() + "_" + counsellorDetails.getGrade_preference_2();
                    addCounsellorIdToLanguageGradeMap(counsellorDetails, key, preferenceMaps.get(8));
                }


            }
        }

        rs.close();
    }

    private CounsellorDetails getCounsellorDetails(ResultSet rs) throws SQLException {
        CounsellorDetails counsellorDetails = new CounsellorDetails();
        counsellorDetails.setCounsellor_id(rs.getString("userid"));
        counsellorDetails.setDay_wise_requirement(rs.getString("required_leads"));
        counsellorDetails.setCounsellor_email_id(rs.getString("counsellor_emailid"));
        counsellorDetails.setUpper_limit_of_lead_score(rs.getString("upper_limit_of_lead_score"));
        counsellorDetails.setLower_limit_of_lead_score(rs.getString("lower_limit_of_lead_score"));
        if (StringUtils.isNotEmpty(rs.getString("language_preference_1"))) {
            counsellorDetails.setLanguage_preference_1(rs.getString("language_preference_1"));
        }
        if (StringUtils.isNotEmpty(rs.getString("language_preference_2"))) {
            counsellorDetails.setLanguage_preference_2(rs.getString("language_preference_2"));
        }
        if (StringUtils.isNotEmpty(rs.getString("grade_preference_1"))) {
            counsellorDetails.setGrade_preference_1(rs.getString("grade_preference_1"));
        }
        if (StringUtils.isNotEmpty(rs.getString("grade_preference_2"))) {
            counsellorDetails.setGrade_preference_2(rs.getString("grade_preference_2"));
        }
        if (StringUtils.isNotEmpty(rs.getString("grade_preference_3"))) {
            counsellorDetails.setGrade_preference_3(rs.getString("grade_preference_3"));
        }
        if (StringUtils.isNotEmpty(rs.getString("lead_type_preference_1"))) {
            counsellorDetails.setLead_type_preference_1(rs.getString("lead_type_preference_1"));
        }
        if (StringUtils.isNotEmpty(rs.getString("lead_type_preference_2"))) {
            counsellorDetails.setLead_type_preference_2(rs.getString("lead_type_preference_2"));
        }
        if (StringUtils.isNotEmpty(rs.getString("lead_type_preference_3"))) {
            counsellorDetails.setLead_type_preference_3(rs.getString("lead_type_preference_3"));
        }
        counsellorDetails.setAssignLeads(0);
        counsellorDetails.setBuckentCount(Integer.parseInt(rs.getString("count")));
        if (StringUtils.isNotEmpty(rs.getString("cohort_july"))) {
            counsellorDetails.setCohort(rs.getString("cohort_july"));
        } else {
            counsellorDetails.setCohort("noCohort");
        }
        counsellorDetails.setLocationPreference(rs.getString("location_preference"));
        counsellorDetails.setCentreCode(rs.getString("center_code"));
        counsellorDetails.setDefaultLanguage(rs.getString("default_language"));
        return counsellorDetails;
    }

    /**
     * puts the counsellor at the right place w.r.t their language,grade and cohort preferences
     *
     * @param counsellorDetails
     * @param key
     * @param languageGradeMap
     * @throws InternalServerErrorException
     * @throws BadRequestException
     */
    private void addCounsellorIdToLanguageGradeMap(CounsellorDetails counsellorDetails, String key, Map<String, Map<String, List<String>>> languageGradeMap) throws InternalServerErrorException, BadRequestException {
        Map<String, List<String>> cohortMap = languageGradeMap.get(key);
        if (cohortMap != null) {
            List<String> counsellorDetailsList = cohortMap.get(counsellorDetails.getCohort());
            counsellorDetailsList.add(counsellorDetails.getCounsellor_id());
            cohortMap.put(counsellorDetails.getCohort(), counsellorDetailsList);
            languageGradeMap.put(key, cohortMap);
        }
    }

    public void assignLeadstoCounsellors() throws SQLException, VException, AddressException, ParseException, IOException {

//        List<Set<String>> preferenceMaps = new ArrayList<>();
        List<Map<String, Map<String, List<String>>>> preferenceMaps = new ArrayList<>();
        //language_grade-> cohort->[counsellordetails]
        Map<String, Map<String, List<String>>> locationMap = new HashMap<>();
        //id->counsellorsetails
        Map<String, CounsellorDetails> counsellorDetailsMap = new HashMap<>();
        //set of counsellorIds
        Set<String> counsellorsSet = new HashSet<>();
        //set of unique grades
        Set<String> gradeSet = new HashSet<>();
        //TODO:: we can use pojo to set counsellorvalues

        categorizeCounsellors(preferenceMaps, gradeSet, locationMap);
        fetchCounsellors(counsellorDetailsMap, preferenceMaps, counsellorsSet, locationMap);

        Long startTime = System.currentTimeMillis();
        String adminId = getUserIdByEmail(mainAdmin);

        Path page = Files.createTempFile("page", ".csv");
        Map<String, String> headerMap = getCsvHeaderMap();
        List<String> header = new ArrayList<>(headerMap.values());
        Files.write(page, String.join(",", header).concat(System.lineSeparator()).getBytes(),
                StandardOpenOption.CREATE, StandardOpenOption.APPEND);
        String queryToGetAdminBucketNonStateBoardLeads = LeadAutoAssignmentQueries.queryToGetAdminBucketNonStateBoardLeads + " limit " + queryLimit;
        String queryToGetAdminBucketStateBoardLeads = LeadAutoAssignmentQueries.queryToGetAdminBucketStateBoardLeads + " limit " + queryLimit;
        String queryToFetchCounsellorBucketNonStateBoardleads = LeadAutoAssignmentQueries.queryToFetchCounsellorBucketNonStateBoardLeads + " limit " + queryLimit;
        String queryToFetchCounsellorBucketStateBoardleads = LeadAutoAssignmentQueries.queryToFetchCounsellorBucketStateBoardLeads + " limit " + queryLimit;
        String queryToFetchNonCoreLeads = LeadAutoAssignmentQueries.queryToFetchNonCoreLeads + " limit " + queryLimitNonCore;
        String queryToFetchParthBucketLeads = LeadAutoAssignmentQueries.queryToFetchParthBucketLeads + " limit " + queryLimit;
        List<AssignedLeads> assignedLeadsList = new ArrayList<>();

        //first need to assign pulled back leads
        assignedLeadsList.addAll(assignPulledBackLeadsOfInactiveCounsellors(page, headerMap));

        boolean isContinue = true;
        int category = 0;
        int count = 0;

        Calendar currentDate = Calendar.getInstance();
        currentDate.setTimeInMillis(System.currentTimeMillis() + DateTimeUtils.IST_TIME_DIFFERENCE_NEGATIVE);
        Date now = currentDate.getTime();
        int i = 0;
        long stime = System.currentTimeMillis();
        Set<String> assignedLeadIds = new HashSet<>();

        logger.info("Core lead preferences: {}", preferenceMaps.size());
        while (i < preferenceMaps.size()) {
            boolean isAdminBucketNonStateBoardLead = true;
            boolean isAdminBucketStateBoardLead = false;
            boolean isCounsellorBucketNonStateBoardLead = false;
            boolean isCounsellorBucketStateBoardLead = false;
            boolean isParthBucket = false;
            List<FreshLeadDetails> freshLeadDetailsList = new ArrayList<>();

            int iterationcount = 0;
            Map<String, Map<String, List<String>>> lgmap = preferenceMaps.get(i);
            //loop keep on iterating till the no of leads coming from redshift is 0
            while (true) {
                //first priority is for adminBucket leads
                String assignedLeadIdsString = String.join("','", assignedLeadIds);
                Long assignTimeForCore = System.currentTimeMillis();
                if (isAdminBucketNonStateBoardLead) {
                    category = 1;
                    logger.info("isAdminBucketNonStateBoardLead: {}", iterationcount);
                    Long queryStartTime = System.currentTimeMillis();
                    freshLeadDetailsList = getLeadsFromRedshiftUsingQuery(queryToGetAdminBucketNonStateBoardLeads, iterationcount, false, false, assignedLeadIdsString);
                    logger.info("Data Size: {}, Time taken: {}", freshLeadDetailsList.size(), System.currentTimeMillis() - queryStartTime);
                    if (ArrayUtils.isEmpty(freshLeadDetailsList) || freshLeadDetailsList.size() < queryLimit) {
                        iterationcount = 0;
                        isAdminBucketNonStateBoardLead = false;
                        isCounsellorBucketNonStateBoardLead = true;
                        logger.info(" Admin bucket not state leads completed.. leadsCount" + assignedLeadsList.size());
                    } else {
                        iterationcount += 1;
                    }
                } else if (isCounsellorBucketNonStateBoardLead) {
                    category = 2;
                    logger.info("isCounsellorBucketNonStateBoardLead: {}", iterationcount);
                    Long queryStartTime = System.currentTimeMillis();
                    freshLeadDetailsList = getLeadsFromRedshiftUsingQuery(queryToFetchCounsellorBucketNonStateBoardleads, iterationcount, false, false, assignedLeadIdsString);
                    logger.info("Data Size: {}, Time taken: {}", freshLeadDetailsList.size(), System.currentTimeMillis() - queryStartTime);
                    if (ArrayUtils.isEmpty(freshLeadDetailsList) || freshLeadDetailsList.size() < queryLimit) {
                        iterationcount = 0;
                        isCounsellorBucketNonStateBoardLead = false;
                        isAdminBucketStateBoardLead = true;
                        logger.info(" Counsellor bucket non state leads completed.. leadsCount" + assignedLeadsList.size());
                    } else {
                        iterationcount += 1;
                    }
                } else if (isAdminBucketStateBoardLead) {
                    category = 1;
                    logger.info("isAdminBucketStateBoardLead: {}", iterationcount);
                    Long queryStartTime = System.currentTimeMillis();
                    freshLeadDetailsList = getLeadsFromRedshiftUsingQuery(queryToGetAdminBucketStateBoardLeads, iterationcount, false, false, assignedLeadIdsString);
                    logger.info("Data Size: {}, Time taken: {}", freshLeadDetailsList.size(), System.currentTimeMillis() - queryStartTime);
                    if (ArrayUtils.isEmpty(freshLeadDetailsList) || freshLeadDetailsList.size() < queryLimit) {
                        iterationcount = 0;
                        isAdminBucketStateBoardLead = false;
                        isCounsellorBucketStateBoardLead = true;
                        logger.info(" Admin bucket state leads completed.. leadsCount" + assignedLeadsList.size());
                    } else {
                        iterationcount += 1;
                    }
                } else if (isCounsellorBucketStateBoardLead) {
                    category = 2;
                    logger.info("isCounsellorBucketStateBoardLead: {}", iterationcount);
                    Long queryStartTime = System.currentTimeMillis();
                    freshLeadDetailsList = getLeadsFromRedshiftUsingQuery(queryToFetchCounsellorBucketStateBoardleads, iterationcount, false, false, assignedLeadIdsString);
                    logger.info("Data Size: {}, Time taken: {}", freshLeadDetailsList.size(), System.currentTimeMillis() - queryStartTime);
                    if (ArrayUtils.isEmpty(freshLeadDetailsList) || freshLeadDetailsList.size() < queryLimit) {
                        iterationcount = 0;
                        isCounsellorBucketStateBoardLead = false;
                        isParthBucket = true;
                        logger.info(" Counsellor bucket state leads completed.. leadsCount" + assignedLeadsList.size());
                    } else {
                        iterationcount += 1;
                    }
                } else if (isParthBucket) {
                    category = 3;
                    logger.info("isParthBucket: {}", iterationcount);
                    Long queryStartTime = System.currentTimeMillis();
                    freshLeadDetailsList = getLeadsFromRedshiftUsingQuery(queryToFetchParthBucketLeads, iterationcount, false, false, assignedLeadIdsString);
                    logger.info("Data Size: {}, Time taken: {}", freshLeadDetailsList.size(), System.currentTimeMillis() - queryStartTime);
                    if (ArrayUtils.isEmpty(freshLeadDetailsList) || freshLeadDetailsList.size() < queryLimit) {
                        iterationcount = 0;
                        isParthBucket = false;
                        logger.info(" Parth bucket state leads completed.. leadsCount" + assignedLeadsList.size());
                    } else {
                        iterationcount += 1;
                    }
                } else {
                    logger.info("Every flag for interation: {} is false-> isAdminBucketNonStateBoardLead: {}, isAdminBucketStateBoardLead: {}, isCounsellorBucketNonStateBoardLead: {}, " +
                            "isCounsellorBucketStateBoardLead: {}, isParthBucket: {}", i + 1, isAdminBucketNonStateBoardLead, isAdminBucketStateBoardLead, isCounsellorBucketNonStateBoardLead, isCounsellorBucketStateBoardLead, isParthBucket);
                    break;
                }
                if (CollectionUtils.isNotEmpty(freshLeadDetailsList)) {
                    List<AssignedLeads> assignedLeadsListTemp = new ArrayList<>();
                    for (FreshLeadDetails freshLeadDetails : freshLeadDetailsList) {
                        count++;
                        if (freshLeadDetails == null) {
                            continue;
                        }

                        String suitableHasSetName;
                        if (i < 3) {
                            suitableHasSetName = freshLeadDetails.getLocation() + "_" + freshLeadDetails.getGrade();
                        } else {
                            suitableHasSetName = freshLeadDetails.getLanguage() + "_" + freshLeadDetails.getGrade();
                        }

                        Map<String, List<String>> suitableCounsellors = new HashMap<>();
                        if (category == 1 || category == 2 || category == 3) {
                            //getting suitable counsellors map with suitable key
                            suitableCounsellors = lgmap.get(suitableHasSetName);
                        }
                        //this will be the counselor id for current lead
                        String requiredCounsellorId = null;
                        if (suitableCounsellors != null) {
                            requiredCounsellorId = getOneSuitableCounsellor(suitableCounsellors, freshLeadDetails, counsellorDetailsMap, adminId, false, counsellorsSet);
                        }
                        if (StringUtils.isNotEmpty(requiredCounsellorId)) {
                            assigningLeads(assignedLeadsList, assignedLeadsListTemp, now, freshLeadDetails, counsellorDetailsMap.get(requiredCounsellorId), counsellorDetailsMap, counsellorsSet, adminId, category, assignedLeadIds);
                        }
                        if (ArrayUtils.isEmpty(counsellorsSet)) {
                            isContinue = false;
                            break;
                        }
                    }
                    logger.info("leads fetched - {} ", count);
                    logger.info("assignedLeadIds - " + assignedLeadIds.size());
                    generateCsv(assignedLeadsListTemp, page, headerMap);
                    processAsync(assignedLeadsListTemp);
                }
                if (!isContinue) {
                    break;
                }
            }
            if (!isContinue) {
                break;
            }
            i++;
            logger.info("Core Iteration count {}, time taken: {}", i, (System.currentTimeMillis() - stime));
        }
        i = 0;
        long noncorestime = System.currentTimeMillis();
        while (isContinue && i < 2) {
            int iterationcount = 0;
            Boolean isFetchLeadEnough = true;
            while (isContinue) {
                String assignedLeadIdsString = String.join("','", assignedLeadIds);
                List<FreshLeadDetails> freshLeadDetailsList = new ArrayList<>();

                category = 4;
                boolean isNonCore = true;
                if (!isFetchLeadEnough) {
                    logger.info("Iteration complete: {}", isFetchLeadEnough);
                    break;
                }
                logger.info("Noncore leads iteration: {}, queryIteration: {}", i, iterationcount);
                Long queryStartTime = System.currentTimeMillis();
                freshLeadDetailsList = getLeadsFromRedshiftUsingQuery(queryToFetchNonCoreLeads, iterationcount, false, isNonCore, assignedLeadIdsString);
                logger.info("Data Size: {}, Time taken: {}", freshLeadDetailsList.size(), System.currentTimeMillis() - queryStartTime);
                if (ArrayUtils.isEmpty(freshLeadDetailsList)) {
                    logger.info(" Non core leads completed.. leadsCount" + assignedLeadsList.size());
                    break;
                } else if (freshLeadDetailsList.size() < queryLimitNonCore) {
                    isFetchLeadEnough = false;
                } else {
                    iterationcount += 1;
                }

                if (CollectionUtils.isNotEmpty(freshLeadDetailsList)) {
                    List<AssignedLeads> assignedLeadsListTemp = new ArrayList<>();
                    for (FreshLeadDetails freshLeadDetails : freshLeadDetailsList) {

                        count++;
                        if (freshLeadDetails == null) {
                            continue;
                        }
                        Map<String, List<String>> suitableCounsellors = new HashMap<>();
                        if (i == 0) {
                            if (StringUtils.isNotEmpty(freshLeadDetails.getLocation())) {
                                suitableCounsellors = locationMap.get(freshLeadDetails.getLocation());
                            }
                        } else {
                            if (StringUtils.isNotEmpty(freshLeadDetails.getLanguage())) {
                                suitableCounsellors = locationMap.get(freshLeadDetails.getLanguage());
                            }
                        }

                        //this will be the counselor id for current lead
                        String requiredCounsellorId = null;
                        if (suitableCounsellors != null) {
                            requiredCounsellorId = getOneSuitableCounsellor(suitableCounsellors, freshLeadDetails, counsellorDetailsMap, adminId, isNonCore, counsellorsSet);
                        }
                        if (StringUtils.isNotEmpty(requiredCounsellorId)) {
                            assigningLeads(assignedLeadsList, assignedLeadsListTemp, now, freshLeadDetails, counsellorDetailsMap.get(requiredCounsellorId), counsellorDetailsMap, counsellorsSet, adminId, category, assignedLeadIds);
                        }
                        if (ArrayUtils.isEmpty(counsellorsSet)) {
                            isContinue = false;
                            break;
                        }
                    }
                    logger.info("leads fetched - {} ", count);
                    logger.info("assignedLeadIds" + assignedLeadIds.size());
                    generateCsv(assignedLeadsListTemp, page, headerMap);
                    processAsync(assignedLeadsListTemp);
                }
            }
            logger.info("Noncore Iteration count {}, time taken: {}", i, (System.currentTimeMillis() - noncorestime));
            i++;
        }

        pushLeadActualAssignmentDataToDB(counsellorDetailsMap);
        awsS3Manager.uploadFile(S3_BUCKET, csvKey, page.toFile());
        logger.info(page.toAbsolutePath());
        page.toFile().delete();
        String csvLink = awsS3Manager.getPublicBucketDownloadUrl(S3_BUCKET, csvKey);
        logger.info("csv link - {} " + csvLink);
        logger.info("time taken " + (System.currentTimeMillis() - startTime));
        logger.info("time taken for assignment - {}", (System.currentTimeMillis() - stime));
        logger.info("remaining counsellors " + counsellorsSet.size());
        logger.info("remaining counsellors " + counsellorsSet);

        String emailString = ConfigUtils.INSTANCE.getStringValue("auto.lead.email.adresses");
        ArrayList<String> emails = new ArrayList<String>(Arrays.asList(emailString.split(",")));
        for (String emailId : emails) {
            sendingEmail(csvLink, emailId, dtf.format(now));
        }
        //setting redis key for 4hrs
        redisDAO.setex("IS_AUTO_LEAD_ASSIGNMENT_COMPLETED", "true", 14400);
    }

    /**
     * Gets the suitable counsellors for non core leads
     *
     * @param cohortCounsellors
     * @param suitableCounsellors
     */
    private void getSuitableCounsellorsForNonCore(Map<String, List<String>> cohortCounsellors, Map<String, List<String>> suitableCounsellors) {

        for (String cohort : cohortCounsellors.keySet()) {
            List<String> counsellorsList = suitableCounsellors.get(cohort);
            if (ArrayUtils.isEmpty(counsellorsList)) {
                counsellorsList = new ArrayList<>();
            }
            counsellorsList.addAll(cohortCounsellors.get(cohort));
            suitableCounsellors.put(cohort, counsellorsList);
        }

    }

    public void sendingEmail(String link, String email, String date) throws AddressException, VException {
        CommunicationType emailType = CommunicationType.AUTO_LEAD_ASSIGNMENT;

        HashMap<String, Object> bodyScopes = new HashMap<>();
        bodyScopes.put("csvLink", link);
        bodyScopes.put("date", date);

        HashMap<String, Object> subjectScopes = new HashMap<>();
        ArrayList<InternetAddress> toAddress = new ArrayList<>();
        toAddress.add(new InternetAddress(email));
        EmailRequest request = new EmailRequest(toAddress, subjectScopes, bodyScopes, emailType, Role.STUDENT);
        request.setIncludeHeaderFooter(Boolean.FALSE);
        communicationManager.sendEmail(request);
    }

    /**
     * Gives one suitable counsellor on required conditions
     *
     * @param suitableCounsellors
     * @param freshLeadDetails
     * @param counsellorDetailsMap
     * @param adminId
     * @param isNonCore
     * @return
     */
    private String getOneSuitableCounsellor(Map<String, List<String>> suitableCounsellors, FreshLeadDetails freshLeadDetails, Map<String, CounsellorDetails> counsellorDetailsMap, String adminId, Boolean isNonCore, Set<String> counsellorsSet) {
        int cnt = 0;
        for (String cohort : suitableCounsellors.keySet()) {
            if (ArrayUtils.isEmpty(suitableCounsellors.get(cohort))) {
                cnt++;
                if (cnt == suitableCounsellors.size()) {
                    return null;
                }
            } else {
                break;
            }
        }

        //adding all counsellor lists w.r.t  cohort value preference
        List<List<String>> counsellorLists = new ArrayList<>();
        counsellorLists.add(suitableCounsellors.get("M6"));
        counsellorLists.add(suitableCounsellors.get("M5+"));
        counsellorLists.add(suitableCounsellors.get("M5"));
        counsellorLists.add(suitableCounsellors.get("M4"));
        counsellorLists.add(suitableCounsellors.get("M3"));
        counsellorLists.add(suitableCounsellors.get("M2"));
        counsellorLists.add(suitableCounsellors.get("M1"));
        counsellorLists.add(suitableCounsellors.get("M0"));
        counsellorLists.add(suitableCounsellors.get("noCohort"));

        Map<Integer, String> counsellorIdBucketCountMap = new HashMap<>();

        Set<CounsellorDetails> suitableCohortCounsellors = new HashSet<>();
        for (int i = 0; i < counsellorLists.size(); i++) {
            if (ArrayUtils.isNotEmpty(counsellorLists.get(i))) {

                for (String counsellorId : counsellorLists.get(i)) {
                    CounsellorDetails counsellorDetails = counsellorDetailsMap.get(counsellorId);
                    //assignedLeads always should be less than or equal to dayWiseRequirement
                    if ((counsellorDetails.getBuckentCount() + counsellorDetails.getAssignLeads()) < Integer.parseInt(counsellorDetails.getDay_wise_requirement())) {
                        if ((StringUtils.isNotEmpty(counsellorDetails.getUpper_limit_of_lead_score()) && StringUtils.isNotEmpty(counsellorDetails.getUpper_limit_of_lead_score()) && StringUtils.isNotEmpty(freshLeadDetails.getLeadScore()))) {
                            //leadScore of lead should be in range of counsellor's leadsocre
                            if ((Integer.parseInt(counsellorDetails.getLower_limit_of_lead_score()) <= Integer.parseInt(freshLeadDetails.getLeadScore())) && (Integer.parseInt(counsellorDetails.getUpper_limit_of_lead_score()) >= Integer.parseInt(freshLeadDetails.getLeadScore()))) {
                                suitableCohortCounsellors.add(counsellorDetails);
                            }
                            //for non-core leads we didnot consider leadscore
                            else if (isNonCore) {
                                suitableCohortCounsellors.add(counsellorDetails);
                            }
                        } else {
                            suitableCohortCounsellors.add(counsellorDetails);
                        }
                    } else {
                        counsellorsSet.remove(counsellorDetails.getCounsellor_id());
                    }
                }
                if (ArrayUtils.isNotEmpty(suitableCohortCounsellors)) {
                    break;
                }
            }
        }
        if (ArrayUtils.isEmpty(suitableCohortCounsellors)) {
            return null;
        }
        return getSuitableCounsellorWithLeadTypePreference(suitableCohortCounsellors, freshLeadDetails, counsellorIdBucketCountMap);

    }

    /**
     * Gets the suitable counsellor with lead type preference
     *
     * @param suitableCohortCounsellors
     * @param freshLeadDetails
     * @param counsellorIdBucketCountMap
     * @return
     */
    private String getSuitableCounsellorWithLeadTypePreference(Set<CounsellorDetails> suitableCohortCounsellors, FreshLeadDetails freshLeadDetails, Map<Integer, String> counsellorIdBucketCountMap) {

        if (ArrayUtils.isNotEmpty(suitableCohortCounsellors) && StringUtils.isNotEmpty(freshLeadDetails.getAssignmentType())) {
            //since we have 3 leadTypePreference we have 3 conditions
            for (CounsellorDetails counsellorDetails : suitableCohortCounsellors) {
                if (StringUtils.isNotEmpty(counsellorDetails.getLead_type_preference_1()) && counsellorDetails.getLead_type_preference_1().equals(freshLeadDetails.getAssignmentType())) {
                    counsellorIdBucketCountMap.put(counsellorDetails.getBuckentCount() + counsellorDetails.getAssignLeads(), counsellorDetails.getCounsellor_id());
                }
            }
            if (counsellorIdBucketCountMap.size() == 0) {
                for (CounsellorDetails counsellorDetails : suitableCohortCounsellors) {
                    if (StringUtils.isNotEmpty(counsellorDetails.getLead_type_preference_2()) && counsellorDetails.getLead_type_preference_2().equals(freshLeadDetails.getAssignmentType())) {
                        counsellorIdBucketCountMap.put(counsellorDetails.getBuckentCount() + counsellorDetails.getAssignLeads(), counsellorDetails.getCounsellor_id());
                    }
                }
            }
            if (counsellorIdBucketCountMap.size() == 0) {
                for (CounsellorDetails counsellorDetails : suitableCohortCounsellors) {
                    if (StringUtils.isNotEmpty(counsellorDetails.getLead_type_preference_3()) && counsellorDetails.getLead_type_preference_3().equals(freshLeadDetails.getAssignmentType())) {
                        counsellorIdBucketCountMap.put(counsellorDetails.getBuckentCount() + counsellorDetails.getAssignLeads(), counsellorDetails.getCounsellor_id());
                    }
                }
            }
        }
        if (counsellorIdBucketCountMap.size() <= 0) {
            //if no leadtype prefernce matched
            for (CounsellorDetails counsellorDetails : suitableCohortCounsellors) {
                counsellorIdBucketCountMap.put(counsellorDetails.getBuckentCount() + counsellorDetails.getAssignLeads(), counsellorDetails.getCounsellor_id());
            }
        }
        //getting the counsellorId which have lease no of leads in their bucket
        return counsellorIdBucketCountMap.get(Collections.min(counsellorIdBucketCountMap.keySet()));
    }

    /**
     * Assigns the leads to suitable counsellors
     *
     * @param assignedLeadsList
     * @param now
     * @param freshLeadDetails
     * @param counsellorDetails
     * @param counsellorDetailsMap
     * @param counsellorsSet
     * @param adminId
     * @param query
     */
    private void assigningLeads(List<AssignedLeads> assignedLeadsList, List<AssignedLeads> assignedLeadsListTemp, Date now, FreshLeadDetails freshLeadDetails, CounsellorDetails counsellorDetails, Map<String, CounsellorDetails> counsellorDetailsMap, Set<String> counsellorsSet, String adminId, int query, Set<String> assignedLeadIds) {
        AssignedLeads assignedLeads = new AssignedLeads();
        assignedLeads.setOwnerId(counsellorDetails.getCounsellor_id());
        addAssignLeadDetails(now, freshLeadDetails, assignedLeads, counsellorDetails.getCentreCode(), query);
        assignedLeadsListTemp.add(assignedLeads);
        assignedLeadsList.add(assignedLeads);
        assignedLeadIds.add(freshLeadDetails.getProspectId());

        if (!counsellorDetails.getCounsellor_id().equals(adminId)) {
            counsellorDetails.setAssignLeads(counsellorDetails.getAssignLeads() + 1);
            if (freshLeadDetails.getIsStateBoard()) {
                counsellorDetails.setStateBoardLeadsCount(counsellorDetails.getStateBoardLeadsCount() + 1);
            } else {
                counsellorDetails.setNonStateBoardLeadsCount(counsellorDetails.getNonStateBoardLeadsCount() + 1);
            }
            if (freshLeadDetails.getIsFreshLead()) {
                counsellorDetails.setFreshLeadsAssigned(counsellorDetails.getFreshLeadsAssigned() + 1);
            } else {
                counsellorDetails.setChurnLeadsAssigned(counsellorDetails.getChurnLeadsAssigned() + 1);
            }
            counsellorDetailsMap.put(counsellorDetails.getCounsellor_id(), counsellorDetails);
            if ((counsellorDetails.getBuckentCount() + counsellorDetails.getAssignLeads()) >= Integer.parseInt(counsellorDetails.getDay_wise_requirement())) {
                if (ArrayUtils.isNotEmpty(counsellorsSet) && counsellorsSet.contains(counsellorDetails.getCounsellor_id())) {
                    counsellorsSet.remove(counsellorDetails.getCounsellor_id());
                }
            }
        }
    }

}