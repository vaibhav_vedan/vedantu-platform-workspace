package com.vedantu.vedantudata.managers;

import com.vedantu.User.User;
import com.vedantu.exception.VException;
import com.vedantu.lms.cmds.enums.EnumBasket;
import com.vedantu.lms.cmds.pojo.CMDSTestAttemptPojo;
import com.vedantu.lms.cmds.pojo.ContentInfoResp;
import com.vedantu.lms.request.CMDSTestInfo;
import com.vedantu.onetofew.pojo.GTTAttendeeDetailsInfo;
import com.vedantu.util.LogFactory;
import com.vedantu.vedantudata.dao.ActivityDao;
import com.vedantu.vedantudata.entities.ActivityDetails;
import com.vedantu.vedantudata.utils.ActivityEventConstants;
import com.vedantu.vedantudata.utils.LeadsquaredActivityHelper;
import com.vedantu.vedantudata.utils.PostgressHandler;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class EngagementTriggerManager extends LeadsquaredActivityHelper {

    @Autowired
    private ActivityDao activityDao;

    @Autowired
    private ActivityManager activityManager;

    @Autowired
    private PostgressHandler postgressHandler;

    private Calendar calendar = Calendar.getInstance();

    @Autowired
    private LogFactory logFactory;

    private Logger logger = logFactory.getLogger(EngagementTriggerManager.class);

    public void process(List<User> activeEnrolledUsers, Map<Long, User> studentAmMapping) throws VException, SQLException, ParseException {

        if(activeEnrolledUsers.isEmpty())
            return;

        List<User> matchedUsers = getALLUsersToBeEngaged(activeEnrolledUsers);

        logger.info("======no of matched users for engagement :: {}", matchedUsers.size());

        List<Long> matchedUserIds = matchedUsers.parallelStream()
                .map(User::getId)
                .distinct()
                .collect(Collectors.toList());

        List<Integer> activityCodes = new ArrayList<>();
        activityCodes.add(ActivityEventConstants.EVENT_PARENT_ENGAGEMENT);

        calendar.setTime(new Date());
        calendar.set(Calendar.DATE, -1);
        List<ActivityDetails> activityDetails = activityDao.getActivitydetails(activityCodes, calendar.getTimeInMillis(), new Date().getTime());

        Map<Long, Integer> acadMentorParentTriggerCountmap = new HashMap<>();
        for (ActivityDetails activityDetails1 : activityDetails) {

            if (acadMentorParentTriggerCountmap.containsKey(activityDetails1.getOwnerVedantuId())) {
                Integer count = acadMentorParentTriggerCountmap.get(activityDetails1.getOwnerVedantuId());
                acadMentorParentTriggerCountmap.put(activityDetails1.getOwnerVedantuId(), count++);
            } else {
                acadMentorParentTriggerCountmap.put(activityDetails1.getOwnerVedantuId(), 1);
            }
        }

        calendar.setTime(new Date());
        calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMinimum(Calendar.DAY_OF_MONTH));

        Date date = calendar.getTime();

        DateFormat dateFormat = new SimpleDateFormat(ActivityEventConstants.SIMPLE_DATE_FORMAT);
        String dateString = dateFormat.format(date);

        //TODO:change this when am mapping removed
        User amTempObject = studentAmMapping.values().iterator().next();

        for (User user : activeEnrolledUsers) {

            ActivityDetails previousActivityDetails = activityDao.getLastActivity(user.getId(), ActivityEventConstants.EVENT_STUDENT_ENGAGEMENT);

            if (previousActivityDetails != null && !activityManager.isClosedActivity(previousActivityDetails)) {
                //NOTE: previous activity is still in pending state
                continue;
            }

            Map<String, String> param = new HashMap<>();
            if (previousActivityDetails != null && activityManager.isClosedActivity(previousActivityDetails)) {

                Long lastUpdated = previousActivityDetails.getLastUpdated();

                LocalDate localeDate = Instant.ofEpochMilli(lastUpdated).atZone(ZoneId.systemDefault()).toLocalDate();
                Long noOfDaysBetween = ChronoUnit.DAYS.between(localeDate, LocalDate.now());
                param.put(ActivityEventConstants.NO_OF_DAYS, noOfDaysBetween.toString());
            }else{
                param.put(ActivityEventConstants.NO_OF_DAYS, "0");
            }

            if (studentAmMapping.containsKey(user.getId())) {
                User am = studentAmMapping.get(user.getId());

                if (acadMentorParentTriggerCountmap.containsKey(am.getId())) {
                    if (acadMentorParentTriggerCountmap.get(am.getId()) < ActivityEventConstants.ENGAGEMENT_ACTIVITY_DEFAULT_CHUNK_SIZE) {
                        triggerActivityForparent(studentAmMapping, user, param);
                        Integer count = acadMentorParentTriggerCountmap.get(am.getId());
                        acadMentorParentTriggerCountmap.put(am.getId(), count++);
                    }
                } else {
                    triggerActivityForparent(studentAmMapping, user, param);
                    acadMentorParentTriggerCountmap.put(am.getId(), 1);
                }
            }

            if (matchedUserIds.contains(user.getId())) {

                logger.info("======raising activity for student engagement :: {}", user.getEmail());
                activityManager.pushActivity(user, studentAmMapping, ActivityEventConstants.EVENT_STUDENT_ENGAGEMENT, param);

            }
        }

    }

    private void triggerActivityForparent(Map<Long, User> studentAmMapping, User user, Map<String, String> param) {
        logger.info("======raising activity for parent engagement :: {}", user.getEmail());
        param.put(ActivityEventConstants.NOTE, ActivityEventConstants.ACTIVITY_EVENT_PARENT_ENGAGEMENT_NOTE);
        activityManager.pushActivity(user, studentAmMapping, ActivityEventConstants.EVENT_PARENT_ENGAGEMENT, param);
    }

    private List<User> getALLUsersToBeEngaged(List<User> activeEnrolledUsers) throws VException {

        Date date = new Date();
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(Calendar.DAY_OF_MONTH, cal.getActualMinimum(Calendar.DAY_OF_MONTH));

        Long currentTime = date.getTime();
        Long startOfMonth = cal.getTimeInMillis();

        List<String> userIdList = activeEnrolledUsers.parallelStream()
                .filter(user -> user.getId() != null)
                .map(user -> user.getId().toString()).collect(Collectors.toList());

        List<ContentInfoResp> contentInfos = activityManager.getContentInfo(userIdList, startOfMonth, currentTime);

        List<String> testIds = contentInfos.parallelStream()
                .map(contentInfoResp -> contentInfoResp.getMetadata().getTestId())
                .collect(Collectors.toList());

        List<CMDSTestInfo> cmdsTests = activityManager.getTestsDetails(testIds);

        List<CMDSTestInfo> filteredTest = cmdsTests.parallelStream()
                .filter(cmdsTestInfo -> {
                    return EnumBasket.TestTagType.ADVANCE == cmdsTestInfo.getTestTag() ||
                            EnumBasket.TestTagType.PHASE == cmdsTestInfo.getTestTag();
                })
                .collect(Collectors.toList());

        List<String> mainsAndAdvanceTestIds = filteredTest.parallelStream()
                .map(cmdsTestInfo -> cmdsTestInfo.getId())
                .collect(Collectors.toList());

        List<User> matchedUsers = new ArrayList<>();

        for (User user : activeEnrolledUsers) {
            List<ContentInfoResp> filteredContentInfos = contentInfos.parallelStream()
                    .filter(contentInfo -> {
                        return contentInfo.getStudentId() == user.getId().toString() ||
                                mainsAndAdvanceTestIds.contains(contentInfo.getMetadata().getTestId());
                    })
                    .collect(Collectors.toList());

            Long missedClasses = filteredContentInfos.parallelStream()
                    .filter(contentInfo -> {
                        return contentInfo.getMetadata() == null ||
                                contentInfo.getMetadata().getTestAttempts() == null ||
                                contentInfo.getMetadata().getTestAttempts().isEmpty();
                    }).count();

            if (missedClasses > 0) {
                continue;
            }

            boolean isNotLT = false;

            for (ContentInfoResp filteredContentInfo : filteredContentInfos) {

                Float totalMarks = filteredContentInfo.getTotalMarks() ==null ? 0f : filteredContentInfo.getTotalMarks();

                CMDSTestAttemptPojo firstAttempt = filteredContentInfo.getMetadata().getTestAttempts().get(0);

                Float marksAchieved = (firstAttempt==null || firstAttempt.getMarksAcheived()==null) ? 0f :firstAttempt.getMarksAcheived();

                if(marksAchieved==null || totalMarks==null)
                    continue;;

                Float percentage = (marksAchieved / totalMarks) * 100;

                if (percentage < 80) {
                    isNotLT=true;
                    break;
                }
            }

            if(isNotLT)
                continue;

            matchedUsers.add(user);
        }
        return matchedUsers;
    }

    public void processEarlyEducation(List<User> usersBatch, Map<Long, User> studentAmMapping) throws VException {

        int numberOfClassesToCheck = 3;

        List<String> userIdList = usersBatch.parallelStream()
                .filter(user -> user.getId() != null)
                .map(user -> user.getId().toString())
                .distinct()
                .collect(Collectors.toList());

        Long thruTime = Instant.now().toEpochMilli();

        //past 15 days
        Long fromTime = thruTime - ActivityEventConstants.MILLI_MONTH;
        List<GTTAttendeeDetailsInfo> gttAttendeeInfos = activityManager.getGttAttendeeDetails(userIdList, fromTime, thruTime);
        logger.info("======fetched gttdattendee for batch users : {}", gttAttendeeInfos.size());

        Map<String,List<GTTAttendeeDetailsInfo>> gttAttendeeStudentMap = gttAttendeeInfos.stream().collect(Collectors.groupingBy(GTTAttendeeDetailsInfo::getUserId));

        for (User user : usersBatch) {

            List<GTTAttendeeDetailsInfo> filteredAttendeeDetails = gttAttendeeStudentMap.get(user.getId().toString());

            if (filteredAttendeeDetails == null || filteredAttendeeDetails.size() < numberOfClassesToCheck)
                continue;

            ActivityDetails previousActivityDetails = activityDao.getLastActivity(user.getId(), ActivityEventConstants.EVENT_STUDENT_ENGAGEMENT);

            Map<String, String> param = new HashMap<>();
            boolean canTrigger = false;

            if(previousActivityDetails == null) {
                param.put(ActivityEventConstants.NO_OF_DAYS, "0");
                canTrigger=true;
            }else if(activityManager.isClosedActivity(previousActivityDetails)){

                Long previousActivityCreationTime = previousActivityDetails.getCreationTime();

                GTTAttendeeDetailsInfo attendeeDetailsInfo = filteredAttendeeDetails.get(2);
                if(attendeeDetailsInfo==null || previousActivityCreationTime >= attendeeDetailsInfo.getCreationTime()){
                    continue;
                }

                Long lastUpdated = previousActivityDetails.getLastUpdated();

                LocalDate localeDate = Instant.ofEpochMilli(lastUpdated).atZone(ZoneId.systemDefault()).toLocalDate();
                Long noOfDaysBetween = ChronoUnit.DAYS.between(localeDate, LocalDate.now());
                param.put(ActivityEventConstants.NO_OF_DAYS, noOfDaysBetween.toString());
                canTrigger=true;
            }

            if(canTrigger) {
                param.put(ActivityEventConstants.NOTE, ActivityEventConstants.ACTIVITY_EVENT_EARLY_EDUCATION_PARENT_ENGAGEMENT_NOTE);
                activityManager.pushActivity(user, studentAmMapping, ActivityEventConstants.EVENT_PARENT_ENGAGEMENT, param);
            }

            logger.info("======fetched gttdattendee for user : {}", filteredAttendeeDetails.size());
        }
    }
}
