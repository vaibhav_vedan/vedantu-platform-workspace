package com.vedantu.vedantudata.managers;

import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import org.apache.logging.log4j.Logger;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.util.Constants;
import com.vedantu.util.DateTimeUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.WebUtils;
import com.vedantu.vedantudata.dao.serializers.WebinarInfoDAO;
import com.vedantu.vedantudata.dao.serializers.WebinarUserRegistrationInfoDAO;
import com.vedantu.vedantudata.entities.WebinarInfo;
import com.vedantu.vedantudata.entities.WebinarUserRegistrationInfo;
import com.vedantu.vedantudata.enums.WebinarUserTag;
import com.vedantu.vedantudata.pojos.GTWSessionAttendeeInfo;
import com.vedantu.vedantudata.pojos.GTWSessionInfo;
import com.vedantu.vedantudata.pojos.WebinarInfoByIdRes;
import com.vedantu.vedantudata.pojos.WebinarInfoRes;
import com.vedantu.vedantudata.request.RegisterWebinarUserReq;
import org.springframework.data.mongodb.core.query.Update;

@Service
public class GTWManager {

    @Autowired
    private WebinarUserRegistrationInfoDAO webinarUserRegistrationInfoDAO;

    @Autowired
    private WebinarInfoDAO webinarInfoDAO;

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(GTWManager.class);

    private static Gson gson = new Gson();

    private static final String[] SUPPORTED_GTW_ACCOUNT_IDS = {"academics@vedantu.com"};

    private static final Type WEBINAR_INFO_RES_TYPE = new TypeToken<List<WebinarInfoRes>>() {
    }.getType();

    private static final Type WEBINAR_SESSIONS_TYPE = new TypeToken<List<GTWSessionInfo>>() {
    }.getType();

    private static final Type WEBINAR_SESSION_ATTENDANCE_TYPE = new TypeToken<List<GTWSessionAttendeeInfo>>() {
    }.getType();

    private static Map<String, String> GTW_ACCESS_TOKENS = Collections.unmodifiableMap(new HashMap<String, String>() {
        private static final long serialVersionUID = -6665886557479988046L;

        {
            put("academics@vedantu.com", "6EDKuyO2v4yPrI35lkaKIjIMjYcW"); // academics
        }
    });

    private static Map<String, String> GTW_ORGANIZER_IDS = Collections.unmodifiableMap(new HashMap<String, String>() {
        private static final long serialVersionUID = -6665886557479988046L;

        {
            put("academics@vedantu.com", "7470416618302296069"); // academics
        }
    });

    private static Map<String, String> GTW_ACCOUNT_KEYS = Collections.unmodifiableMap(new HashMap<String, String>() {
        private static final long serialVersionUID = -6665886557479988046L;

        {
            put("academics@vedantu.com", "94810155808804869"); // academics
        }
    });

    public WebinarUserRegistrationInfo registerWebinar(WebinarUserRegistrationInfo webinarUserRegistrationInfo, String trainingId) {
        logger.info("Request:" + webinarUserRegistrationInfo.toString());
        RegisterWebinarUserReq registerWebinarUserReq = new RegisterWebinarUserReq(webinarUserRegistrationInfo);
        ClientResponse respOTFBatch = WebUtils.INSTANCE.doCall(getRegisterWebinarUrl(trainingId), HttpMethod.POST,
                gson.toJson(registerWebinarUserReq));
        String jsonRespOTFBatch = respOTFBatch.getEntity(String.class);
        logger.info(jsonRespOTFBatch);
        logger.info("Status code of response " + respOTFBatch.getStatus());
        if (respOTFBatch.getStatus() == 200 || respOTFBatch.getStatus() == 201) {
            JSONObject jsonObject = new JSONObject(jsonRespOTFBatch);
            webinarUserRegistrationInfo.updateRegisterResponse(jsonObject);
            WebinarUserRegistrationInfo existingEntry = webinarUserRegistrationInfoDAO
                    .getWebinarUserInfo(webinarUserRegistrationInfo.getEmailId(), trainingId);
            if (existingEntry == null) {
                webinarUserRegistrationInfo.addTag(WebinarUserTag.GTW_REGISTRATION_DONE);
                webinarUserRegistrationInfoDAO.create(webinarUserRegistrationInfo);
                logger.info(" saved entity:" + webinarUserRegistrationInfo.toString());
            } else {
                existingEntry.addTag(WebinarUserTag.GTW_REGISTRATION_DONE);
                existingEntry.updateRegisterResponse(webinarUserRegistrationInfo);
                webinarUserRegistrationInfoDAO.create(existingEntry);
                webinarUserRegistrationInfo = existingEntry;
                logger.info(" saved entity:" + webinarUserRegistrationInfo.toString());
            }
        } else {
            logger.error("error in registering to webinar " + jsonRespOTFBatch);
        }
        return webinarUserRegistrationInfo;
    }

    public WebinarInfo syncWebinarInfos(String webinarId) throws ParseException {
        for (String emailId : SUPPORTED_GTW_ACCOUNT_IDS) {
            String serverUrl = getWebinarInfoByIdUrl(webinarId, emailId);
            ClientResponse syncWebinarInfoByIdRes = WebUtils.INSTANCE.doCall(serverUrl, HttpMethod.GET, null,
                    GTW_ACCESS_TOKENS.get(emailId));
            String jsonResp = syncWebinarInfoByIdRes.getEntity(String.class);
            if (syncWebinarInfoByIdRes.getStatus() >= 400) {
                break;
            }

            WebinarInfoByIdRes webinarInfoRes = gson.fromJson(jsonResp, WebinarInfoByIdRes.class);
            WebinarInfo webinarInfo = new WebinarInfo(webinarInfoRes, emailId);

            Query query = new Query();
            query.addCriteria(Criteria.where(WebinarInfo.Constants.WEBINAR_ID).is(webinarId));
            Update update = new Update();
            update.set("webinarSubId", webinarInfo.getWebinarSubId());
            update.set("accountKey", webinarInfo.getAccountKey());
            update.set("organizerKey", webinarInfo.getOrganizerKey());
            update.set("accountEmailId", webinarInfo.getAccountEmailId());
            update.set("registrationUrl", webinarInfo.getRegistrationUrl());
            update.set("name", webinarInfo.getName());
            update.set("subject", webinarInfo.getSubject());
            update.set("description", webinarInfo.getDescription());
            update.set("startTime", webinarInfo.getStartTime());
            update.set("endTime", webinarInfo.getEndTime());
            update.set("timeZone", webinarInfo.getTimeZone());
            update.set(WebinarInfo.Constants.LAST_UPDATED, System.currentTimeMillis());
            webinarInfoDAO.upsertEntity(query, update, WebinarInfo.class);
            return webinarInfo;
        }
        return null;
    }

    public void syncWebinarInfos(long startTime, long endTime) {
        final int size = Constants.DEFAULT_ASYNC_BATCH_SIZE;

        for (String emailId : SUPPORTED_GTW_ACCOUNT_IDS) {

            // Process for each account
            int page = 0;
            while (true) {
                try {
                    String serverUrl = getWebinarInfosBaseUrl(emailId);
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
                    sdf.setTimeZone(TimeZone.getTimeZone(DateTimeUtils.TIME_ZONE_GMT));
                    serverUrl += "?fromTime=" + sdf.format(new Date(startTime)) + "Z" + "&toTime="
                            + sdf.format(new Date(endTime)) + "Z" + "&size=" + size + "&page=" + page;
                    ClientResponse webinarInfosResponse = WebUtils.INSTANCE.doCall(serverUrl, HttpMethod.GET, null,
                            GTW_ACCESS_TOKENS.get(emailId));
                    String jsonString = webinarInfosResponse.getEntity(String.class);
                    logger.info("status:" + webinarInfosResponse.toString() + "response:" + jsonString);

                    JSONObject jsonObject = new JSONObject(jsonString);
                    if (!jsonObject.has("_embedded")) {
                        break;
                    }

                    org.json.JSONArray tempArray = jsonObject.getJSONObject("_embedded").getJSONArray("webinars");
                    logger.info("webinarResponseString");
                    List<WebinarInfoRes> webinarInfos = gson.fromJson(tempArray.toString(), WEBINAR_INFO_RES_TYPE);
                    if (CollectionUtils.isEmpty(webinarInfos)) {
                        break;
                    }

                    webinarInfoDAO.updateWebinarInfos(webinarInfos, emailId);
                    page++;
                } catch (Exception ex) {
                    logger.info("Exception :" + ex.toString() + " message:" + ex.getMessage());
                    break;
                }
            }
        }
    }

    public List<WebinarUserRegistrationInfo> processWebinarAttendance(List<WebinarInfo> webinarInfos)
            throws ParseException {
        List<WebinarUserRegistrationInfo> results = new ArrayList<>();
        if (!CollectionUtils.isEmpty(webinarInfos)) {
            for (WebinarInfo webinarInfo : webinarInfos) {
                try {
                    Query query = new Query();
                    query.addCriteria(Criteria.where(WebinarUserRegistrationInfo.Constants.TRAINING_ID)
                            .is(webinarInfo.getWebinarId()));
                    List<WebinarUserRegistrationInfo> registeredUsers = webinarUserRegistrationInfoDAO.runQuery(query,
                            WebinarUserRegistrationInfo.class);
                    Map<String, List<GTWSessionAttendeeInfo>> attendanceMap = fetchGTWAttendenceInfoMap(
                            webinarInfo.getWebinarId(), webinarInfo.getAccountEmailId());
                    if (!CollectionUtils.isEmpty(registeredUsers)) {
                        // Fetch the attendance info from gtw
                        for (WebinarUserRegistrationInfo entry : registeredUsers) {
                            if (attendanceMap.containsKey(entry.getEmailId())) {
                                entry.updateAttendance(attendanceMap.get(entry.getEmailId()));
                            } else {
                                entry.setTimeInSession(0);
                                entry.setAttendanceIntervals(new ArrayList<>());
                            }

                            entry.addTag(WebinarUserTag.GTW_ATTENDENCE_FETCHED);
                            webinarUserRegistrationInfoDAO.create(entry);
                            attendanceMap.remove(entry.getEmailId());
                        }
                        results.addAll(registeredUsers);
                    }

                    for (String userEmailId : attendanceMap.keySet()) {
                        List<GTWSessionAttendeeInfo> sessionAttendeeInfos = attendanceMap.get(userEmailId);
                        if (!CollectionUtils.isEmpty(sessionAttendeeInfos)) {
                            WebinarUserRegistrationInfo webinarUserRegistrationInfo = new WebinarUserRegistrationInfo();
                            webinarUserRegistrationInfo.setFirstName(sessionAttendeeInfos.get(0).getFirstName());
                            webinarUserRegistrationInfo.setLastName(sessionAttendeeInfos.get(0).getLastName());
                            webinarUserRegistrationInfo.setEmailId(sessionAttendeeInfos.get(0).getEmail());
                            webinarUserRegistrationInfo.setTrainingId(webinarInfo.getWebinarId());
                            webinarUserRegistrationInfo.updateAttendance(sessionAttendeeInfos);
                            webinarUserRegistrationInfo.addTag(WebinarUserTag.GTW_REGISTRATION_DONE);
                            webinarUserRegistrationInfo.addTag(WebinarUserTag.GTW_ATTENDENCE_FETCHED);
                            webinarUserRegistrationInfo.addTag(WebinarUserTag.OTHER_REGISTRATION_SOURCE);
                            webinarUserRegistrationInfoDAO.create(webinarUserRegistrationInfo);
                            results.add(webinarUserRegistrationInfo);
                        }
                    }
                } catch (Exception ex) {
                    logger.error("ProcessWebinarAttendanceError : " + ex.getMessage() + " ex:" + ex.toString()
                            + " webinarInfo:" + webinarInfo.toString());
                }
            }
        }

        return results;
    }

    public Map<String, List<GTWSessionAttendeeInfo>> fetchGTWAttendenceInfoMap(String webinarId, String emailId) {
        List<GTWSessionInfo> sessionInfos = fetchGTWSessions(webinarId, emailId);

        Map<String, List<GTWSessionAttendeeInfo>> attendenceMap = new HashMap<>();
        if (!CollectionUtils.isEmpty(sessionInfos)) {
            for (GTWSessionInfo sessionInfo : sessionInfos) {
                List<GTWSessionAttendeeInfo> attendanceInfos = fetchGTWSessionAttendence(sessionInfo.getSessionKey(),
                        webinarId, emailId);
                if (!CollectionUtils.isEmpty(attendanceInfos)) {
                    for (GTWSessionAttendeeInfo sessionAttendeeInfo : attendanceInfos) {
                        List<GTWSessionAttendeeInfo> entry = new ArrayList<>();
                        if (attendenceMap.containsKey(sessionAttendeeInfo.getEmail())) {
                            entry = attendenceMap.get(sessionAttendeeInfo.getEmail());
                        }
                        entry.add(sessionAttendeeInfo);
                        attendenceMap.put(sessionAttendeeInfo.getEmail(), entry);
                    }
                }
            }
        }

        return attendenceMap;
    }

    public List<GTWSessionInfo> fetchGTWSessions(String webinarId, String emailId) {
        String serverUrl = getWebinarSessionsUrl(webinarId, emailId);
        ClientResponse webinarInfosResponse = WebUtils.INSTANCE.doCall(serverUrl, HttpMethod.GET, null,
                GTW_ACCESS_TOKENS.get(emailId));
        String jsonString = webinarInfosResponse.getEntity(String.class);
        logger.info("status:" + webinarInfosResponse.toString() + "response:" + jsonString);

        List<GTWSessionInfo> results = new ArrayList<>();
        if (webinarInfosResponse.getStatus() != 200) {
            return results;
        }

        return gson.fromJson(jsonString, WEBINAR_SESSIONS_TYPE);
    }

    public List<GTWSessionAttendeeInfo> fetchGTWSessionAttendence(String sessionId, String webinarId, String emailId) {
        String serverUrl = getWebinarSessionAttendanceUrl(sessionId, webinarId, emailId);
        ClientResponse webinarInfosResponse = WebUtils.INSTANCE.doCall(serverUrl, HttpMethod.GET, null,
                GTW_ACCESS_TOKENS.get(emailId));
        String jsonString = webinarInfosResponse.getEntity(String.class);
        logger.info("status:" + webinarInfosResponse.toString() + "response:" + jsonString);

        List<GTWSessionAttendeeInfo> results = new ArrayList<>();
        if (webinarInfosResponse.getStatus() != 200) {
            return results;
        }

        return gson.fromJson(jsonString, WEBINAR_SESSION_ATTENDANCE_TYPE);
    }

    public WebinarInfo getWebinarInfo(String webinarId) throws ParseException {
        Query query = new Query();
        query.addCriteria(Criteria.where(WebinarInfo.Constants.WEBINAR_ID).is(webinarId));
        List<WebinarInfo> results = webinarInfoDAO.runQuery(query, WebinarInfo.class);
        if (!CollectionUtils.isEmpty(results)) {
            return results.get(0);
        } else {
            // Sync webinarId
            return syncWebinarInfos(webinarId);
        }
    }

    public List<WebinarInfo> getWebinarInfos(long startTime, long endTime) throws ParseException {
        Query query = new Query();
        query.addCriteria(Criteria.where(WebinarInfo.Constants.START_TIME).lte(endTime));
        query.addCriteria(Criteria.where(WebinarInfo.Constants.END_TIME).gte(startTime));
        return webinarInfoDAO.runQuery(query, WebinarInfo.class);
    }

    public WebinarUserRegistrationInfo getWebinarInfo(String emailId, String trainingId) {
        return getWebinarInfo(emailId, trainingId);
    }

    private static String getRegisterWebinarUrl(String traningId) {
        // https://globalattspa.gotowebinar.com/api/webinars/150586194716221441/registrants?requireLicense=true&client=spa
        return "https://globalattspa.gotowebinar.com/api/webinars/" + traningId
                + "/registrants?requireLicense=true&client=spa";
    }

    private static String getWebinarInfosBaseUrl(String emailId) {
        // curl -X GET --header "Accept: application/json" --header "Authorization:
        // 6EDKuyO2v4yPrI35lkaKIjIMjYcW"
        // "https://api.getgo.com/G2W/rest/accounts/94810155808804869/webinars?fromTime=2017-08-01T10%3A00%3A00Z&toTime=2017-08-11T10%3A00%3A00Z&size=50"

        String accountKey = GTW_ACCOUNT_KEYS.get(emailId);
        return "https://api.getgo.com/G2W/rest/accounts/" + accountKey + "/webinars";
    }

    private static String getWebinarInfoByIdUrl(String webinarId, String emailId) {
        // curl -X GET --header "Accept: application/json" --header "Authorization:
        // 6EDKuyO2v4yPrI35lkaKIjIMjYcW"
        // "https://api.getgo.com/G2W/rest/organizers/7470416618302296069/webinars/5935334575730303234"

        return "https://api.getgo.com/G2W/rest/organizers/" + GTW_ORGANIZER_IDS.get(emailId) + "/webinars/" + webinarId;
    }

    private static String getWebinarSessionsUrl(String webinarId, String emailId) {
        // curl -X GET --header "Accept: application/json" --header "Authorization:
        // 6EDKuyO2v4yPrI35lkaKIjIMjYcW"
        // "https://api.getgo.com/G2W/rest/organizers/7470416618302296069/webinars/7194267692821624065/sessions"

        return "https://api.getgo.com/G2W/rest/organizers/" + GTW_ORGANIZER_IDS.get(emailId) + "/webinars/" + webinarId
                + "/sessions";
    }

    private static String getWebinarSessionAttendanceUrl(String sessionId, String webinarId, String emailId) {
        // curl -X GET --header "Accept: application/json" --header "Authorization:
        // 6EDKuyO2v4yPrI35lkaKIjIMjYcW"
        // "https://api.getgo.com/G2W/rest/organizers/7470416618302296069/webinars/7194267692821624065/sessions/10770772/attendees"

        return "https://api.getgo.com/G2W/rest/organizers/" + GTW_ORGANIZER_IDS.get(emailId) + "/webinars/" + webinarId
                + "/sessions/" + sessionId + "/attendees";
    }
}
