package com.vedantu.vedantudata.managers.dialers;

import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.VException;
import com.vedantu.util.DateTimeUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.vedantudata.dao.SlashRTCDialerDao;
import com.vedantu.vedantudata.entities.SlashRTCActivityDetails;
import com.vedantu.vedantudata.utils.AwsSQSManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.TimeZone;

@Service
public class SlashRTCManager {

    @Autowired
    private AwsSQSManager awsSQSManager;

    @Autowired
    private LogFactory logFactory;

    @Autowired
    public SlashRTCDialerDao slashRTCDialerDao;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(SlashRTCManager.class);

//   Sample JSON from SlashRTC
//   {"dispose":"10076","dispose_name":"Not Answered","first_dispose_id":"10072","first_dispose_name":"FOS DNP (Did not pick)",
//   "second_dispose_id":"10076","second_dispose_name":"Not Answered","third_dispose_id":"0","third_dispose_name":"","comment":"",
//   "date":"2020-04-25 15:48:14","customer_name":"","customer_email":"","lead_json":"","process_json":"","leadset_id":"1",
//   "leadset_name":"default","phone_number":"7982984105","did_number":"false","process":"93",
//   "process_name":"DelhiSouthAnkurTyagi","campaign":"22","campaign_name":"DelhiSouth","callback":"0000-00-00 00:00:00",
//   "agent":"1622","agent_username":"GarishmaArora","agent_name":"Garishma Arora","accesslevel":"4",
//   "cdrid":"c361b16b-93d9-4d3d-bf5f-18a1bffc833f","agent_talktime_sec":"0","mute_time":"0","hold_time":"0","transfer_time":"0",
//   "conference_time":"0","dispose_time":"495","in_queue_time":"0","ringing_time_sec":"0","mode_of_calling":"manual",
//   "disconnected_by":"AGENT","start_stamp":"2020-04-25 15:39:12"}

    public void addDialerActivityFromSQS(String text) throws VException, ParseException {
        logger.info("Incoming text {}", text);
        JSONObject activityData = new JSONObject(text);
        SlashRTCActivityDetails activity = new SlashRTCActivityDetails();

        String cdrid = activityData.optString("cdrid");
        if (StringUtils.isNotEmpty(cdrid)) {
            activity.setCdrid(cdrid);
        } else {
            throw new VException(ErrorCode.BAD_REQUEST_ERROR, "cdrid not present");
        }

        String dispose = activityData.optString("dispose");
        String dispose_name = activityData.optString("dispose_name");
        String first_dispose_id = activityData.optString("first_dispose_id");
        String first_dispose_name = activityData.optString("first_dispose_name");
        String second_dispose_id = activityData.optString("second_dispose_id");
        String second_dispose_name = activityData.optString("second_dispose_name");
        String third_dispose_id = activityData.optString("third_dispose_id");
        String third_dispose_name = activityData.optString("third_dispose_name");
        String comment = activityData.optString("comment");
        String customer_name = activityData.optString("customer_name");
        String customer_email = activityData.optString("customer_email");
        String lead_json = activityData.optString("lead_json");
        String process_json = activityData.optString("process_json");
        String leadset_id = activityData.optString("leadset_id");
        String leadset_name = activityData.optString("leadset_name");
        String phone_number = activityData.optString("phone_number");
        String did_number = activityData.optString("did_number");
        String process = activityData.optString("process");
        String process_name = activityData.optString("process_name");
        String campaign = activityData.optString("campaign");
        String campaign_name = activityData.optString("campaign_name");
        String callback = activityData.optString("callback");
        String agent = activityData.optString("agent");
        String agent_username = activityData.optString("agent_username");
        String agent_name = activityData.optString("agent_name");
        String accesslevel = activityData.optString("accesslevel");
        String resourceUrl = activityData.optString("ResourceURL");
        String agent_talktime_sec = activityData.optString("agent_talktime_sec");
        String mute_time = activityData.optString("mute_time");
        String hold_time = activityData.optString("hold_time");
        String transfer_time = activityData.optString("transfer_time");
        String conference_time = activityData.optString("conference_time");
        String dispose_time = activityData.optString("dispose_time");
        String in_queue_time = activityData.optString("in_queue_time");
        String ringing_time_sec = activityData.optString("ringing_time_sec");
        String mode_of_calling = activityData.optString("mode_of_calling");
        String disconnected_by = activityData.optString("disconnected_by");
        String start_stamp = activityData.optString("start_stamp");
        String date = activityData.optString("date");
        String status = activityData.optString("Status");
        String employeeId = activityData.optString("employeeId");
        String employeeEmail = activityData.optString("employeeEmail");
        String hangupCause = activityData.optString("HangupCause");


        if (StringUtils.isNotEmpty(dispose)) {
            activity.setDispose(dispose);
        }

        if (StringUtils.isNotEmpty(dispose_name)) {
            activity.setDisposeName(dispose_name);
        }
        if (StringUtils.isNotEmpty(first_dispose_id)) {
            activity.setFirstDisposeId(first_dispose_id);
        }
        if (StringUtils.isNotEmpty(first_dispose_name)) {
            activity.setFirstDisposeName(first_dispose_name);
        }
        if (StringUtils.isNotEmpty(second_dispose_id)) {
            activity.setSecondDisposeId(second_dispose_id);
        }
        if (StringUtils.isNotEmpty(second_dispose_name)) {
            activity.setSecondDisposeName(second_dispose_name);
        }
        if (StringUtils.isNotEmpty(third_dispose_id)) {
            activity.setThirdDisposeId(third_dispose_id);
        }
        if (StringUtils.isNotEmpty(third_dispose_name)) {
            activity.setThirdDisposeName(third_dispose_name);
        }
        if (StringUtils.isNotEmpty(comment)) {
            activity.setComment(comment);
        }
        if (StringUtils.isNotEmpty(customer_name)) {
            activity.setCustomeName(customer_name);
        }
        if (StringUtils.isNotEmpty(customer_email)) {
            activity.setCustomerEmail(customer_email);
        }
        if (StringUtils.isNotEmpty(lead_json)) {
            activity.setLeadJson(lead_json);
        }
        if (StringUtils.isNotEmpty(process_json)) {
            activity.setProcessJson(process_json);
        }
        if (StringUtils.isNotEmpty(leadset_id)) {
            activity.setLeadsetId(leadset_id);
        }
        if (StringUtils.isNotEmpty(leadset_name)) {
            activity.setLeadsetName(leadset_name);
        }
        if (StringUtils.isNotEmpty(phone_number)) {
            activity.setPhoneNumber(phone_number);
        }
        if (StringUtils.isNotEmpty(did_number)) {
            activity.setDidNumber(did_number);
        }
        if (StringUtils.isNotEmpty(process)) {
            activity.setProcess(process);
        }
        if (StringUtils.isNotEmpty(process_name)) {
            activity.setProcessName(process_name);
        }
        if (StringUtils.isNotEmpty(campaign)) {
            activity.setCampaign(campaign);
        }
        if (StringUtils.isNotEmpty(campaign_name)) {
            activity.setCampaignName(campaign_name);
        }
        if (StringUtils.isNotEmpty(callback)) {
            activity.setCallback(callback);
        }
        if (StringUtils.isNotEmpty(agent)) {
            activity.setAgent(agent);
        }
        if (StringUtils.isNotEmpty(agent_username)) {
            activity.setAgentUsername(agent_username);
        }
        if (StringUtils.isNotEmpty(agent_name)) {
            activity.setAgentName(agent_name);
        }
        if (StringUtils.isNotEmpty(accesslevel)) {
            activity.setAccessLevel(accesslevel);
        }
        if (StringUtils.isNotEmpty(agent_talktime_sec)) {
            activity.setAgentTalktimeSec(agent_talktime_sec);
        }
        if (StringUtils.isNotEmpty(mute_time)) {
            activity.setMuteTime(mute_time);
        }
        if (StringUtils.isNotEmpty(hold_time)) {
            activity.setHoldTime(hold_time);
        }
        if (StringUtils.isNotEmpty(transfer_time)) {
            activity.setTransferTime(transfer_time);
        }
        if (StringUtils.isNotEmpty(conference_time)) {
            activity.setConferenceTime(conference_time);
        }
        if (StringUtils.isNotEmpty(dispose_time)) {
            activity.setDisposeTime(dispose_time);
        }
        if (StringUtils.isNotEmpty(in_queue_time)) {
            activity.setInQueueTime(in_queue_time);
        }
        if (StringUtils.isNotEmpty(ringing_time_sec)) {
            activity.setRingingTimeSec(ringing_time_sec);
        }
        if (StringUtils.isNotEmpty(mode_of_calling)) {
            activity.setModeOfCalling(mode_of_calling);
        }
        if (StringUtils.isNotEmpty(disconnected_by)) {
            activity.setDisconnectedBy(disconnected_by);
        }
        if (StringUtils.isNotEmpty(resourceUrl)) {
            activity.setResourceURL(resourceUrl);
        }
        if (StringUtils.isNotEmpty(status)) {
            activity.setStatus(status);
        }
        if (StringUtils.isNotEmpty(date)) {
            activity.setIncomingDate(date);
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            sdf.setTimeZone(TimeZone.getTimeZone(DateTimeUtils.TIME_ZONE_IN));
            activity.setDate(sdf.parse(date).getTime());
        }
        if (StringUtils.isNotEmpty(start_stamp)) {
            Long timestampStartStamp = Long.parseLong(start_stamp);
            activity.setStartStamp(timestampStartStamp);
        }
        if(StringUtils.isNotEmpty(employeeId)){
            activity.setEmployeeId(employeeId);
        }
        if(StringUtils.isNotEmpty(employeeEmail)){
            activity.setEmployeeEmail(employeeEmail);
        }
        if(StringUtils.isNotEmpty(hangupCause)){
            activity.setHangupCause(hangupCause);
        }
        logger.info("SlashActivity req {}", activity);
        slashRTCDialerDao.creatActivity(activity);
    }

}
