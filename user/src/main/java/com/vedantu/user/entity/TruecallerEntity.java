package com.vedantu.user.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


/**
 *
 * @author Aditya Rathi
 */


@Data
@AllArgsConstructor
@NoArgsConstructor
public class TruecallerEntity {
    private  String avatarUrl;
    private  String city;
    private  String companyName;
    private  String countryCode;
    private  String email;
    private  String facebookId;
    private  String firstName;
    private  String gender;
    private  boolean isAmbassador;
    private  boolean isSimChanged;
    private  boolean isTrueName;
    private  String jobTitle;
    private  String lastName;
    private  String phoneNumber;
    private  String requestNonce;
    private  String street;
    private  String twitterId;
    private  String url;
    private  String verificationMode;
    private  String zipcode;
    private  long requestTime;
}
