/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.user.entity;

import com.vedantu.User.enums.LoginTokenContext;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 *
 * @author jeet
 */
@Document(collection = "UserLoginToken")
public class UserLoginToken extends AbstractMongoStringIdEntity {

    private LoginTokenContext contextType;
    private String contextId;
    private Long expirationTime;
    @Indexed(background=true)
    private Long userId;
    private String testLink;

    public UserLoginToken() {
        super();
    }

    public UserLoginToken(Long userId,LoginTokenContext contextType, String contextId, Long expirationTime) {
        this.contextType = contextType;
        this.contextId = contextId;
        this.expirationTime = expirationTime;
        this.userId=userId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }
    
    public LoginTokenContext getContextType() {
        return contextType;
    }

    public void setContextType(LoginTokenContext contextType) {
        this.contextType = contextType;
    }

    public String getContextId() {
        return contextId;
    }

    public void setContextId(String contextId) {
        this.contextId = contextId;
    }

    public Long getExpirationTime() {
        return expirationTime;
    }

    public void setExpirationTime(Long expirationTime) {
        this.expirationTime = expirationTime;
    }

    public String getTestLink() {
        return testLink;
    }

    public void setTestLink(String testLink) {
        this.testLink = testLink;
    }
}
