/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.user.entity;

/**
 *
 * @author somil
 */
public interface IPhoneNumber {

    public String _getNumber();

    public String _getPhoneCode();

    public Boolean _getIsVerified();

    public Boolean _getIsDND();

    public Boolean _getIsWhitelisted();
}
