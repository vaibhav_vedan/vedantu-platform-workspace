package com.vedantu.user.entity;

import com.vedantu.User.response.UserSystemIntroRes;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "UserSystemIntroData")
public class UserSystemIntroData extends AbstractMongoStringIdEntity {

	// unique key userId+type+identifier
	private Long userId;
	private String type;
	private String status;
	private String data; //TODO Type changed from Text to String
	private String identifier;

	public UserSystemIntroData() {
		super();
	}

	public UserSystemIntroData(Long userId, String type, String status,
			String data, String identifier) {
		super();
		this.userId = userId;
		this.type = type.toUpperCase();
		this.status = status.toUpperCase();
		this.data = data;
		this.identifier = identifier.toUpperCase();
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type.toUpperCase();
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status.toUpperCase();
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public String getIdentifier() {
		return identifier;
	}

	public void setIdentifier(String identifier) {
		this.identifier = identifier.toUpperCase();
	}
        
        public UserSystemIntroRes toUserSystemIntroRes(){
            return new UserSystemIntroRes(getId(),getUserId(),getType(),getStatus(),getData(),getIdentifier());
        }

	public static class Constants extends AbstractMongoStringIdEntity.Constants {
		public static final String STATUS = "status";
		public static final String DATA = "data";
		public static final String IDENTIFIER = "identifier";
                public static final String USER_ID = "userId";
                public static final String TYPE = "type";

	}

}
