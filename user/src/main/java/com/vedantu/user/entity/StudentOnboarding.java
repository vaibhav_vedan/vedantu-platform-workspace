package com.vedantu.user.entity;

import com.vedantu.User.LocationInfo;
import com.vedantu.dinero.enums.PaymentType;
import com.vedantu.session.pojo.EntityType;
import com.vedantu.user.enums.ABTestingType;
import com.vedantu.user.enums.OnboardingCompletionStatus;
import com.vedantu.user.pojo.onboarding.*;
import com.vedantu.util.dbentities.mongo.AbstractMongoEntity;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import com.vedantu.util.enums.RequestSource;
import com.vedantu.util.pojo.DeviceDetails;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Objects;

@Data
@NoArgsConstructor
@Document(collection = "StudentOnboarding")
public class StudentOnboarding extends AbstractMongoStringIdEntity {
    // Student Details
    @Indexed(background = true, unique = true)
    private Long studentId;
    private String studentName;
    private String studentEmail;
    private String studentGrade;

    // Bundle Details
    @Indexed(background = true)
    private String bundleId;
    private String bundleName;
    private EntityType entityType;

    // Payment Details
    private PaymentType paymentType;
    private Long bookingPotential;
    private Long downPayment;

    // Device details
    private RequestSource requestSource;
    private String ipAddress;
    private String geoCountry;
    private String userAgent;
    private String appVersionCode;
    private String appDeviceId;
    private DeviceDetails deviceDetails;
    private LocationInfo locationInfo;

    private ABTestingType abTestingType=ABTestingType.A;

    // Onboarding Steps
    private Introduction introduction = new Introduction();
    private ProfileCreation profileCreation = new ProfileCreation();
    private CourseEnroll courseEnroll = new CourseEnroll();
    private OnboardingCompletionStatus completionStatus = OnboardingCompletionStatus.PENDING;

    // Sales verification
    private SalesVerificationInfo salesVerificationInfo = new SalesVerificationInfo();

    // SAM Introduction
    private SAMIntroductionInfo samIntroductionInfo = new SAMIntroductionInfo();

    public StudentOnboarding(StudentOnboarding other) {
        this.studentId = other.studentId;
        this.studentName = other.studentName;
        this.studentEmail = other.studentEmail;
        this.studentGrade = other.studentGrade;
        this.bundleId = other.bundleId;
        this.bundleName = other.bundleName;
        this.entityType = other.entityType;
        this.paymentType = other.paymentType;
        this.bookingPotential = other.bookingPotential;
        this.downPayment = other.downPayment;
        this.requestSource = other.requestSource;
        this.ipAddress = other.ipAddress;
        this.geoCountry = other.geoCountry;
        this.userAgent = other.userAgent;
        this.appVersionCode = other.appVersionCode;
        this.appDeviceId = other.appDeviceId;
        this.deviceDetails = other.deviceDetails;
        this.locationInfo = other.locationInfo;
        this.introduction = other.introduction;
        this.profileCreation = other.profileCreation;
        this.courseEnroll = other.courseEnroll;
        this.completionStatus = other.completionStatus;
        this.salesVerificationInfo = other.salesVerificationInfo;
        this.samIntroductionInfo = other.samIntroductionInfo;
        this.abTestingType = other.abTestingType;
    }

    public static class Constants extends AbstractMongoEntity.Constants {
        public static final String STUDENT_ID = "studentId";
        public static final String STUDENT_GRADE = "studentGrade";
        public static final String BUNDLE_ID = "bundleId";
        public static final String INTRODUCTION = "introduction";
        public static final String COMPATIBILITY_CHECK = "compatibilityCheck";
        public static final String EXPERIENCE_PLATFORM = "experiencePlatform";
        public static final String PROFILE_CREATION = "profileCreation";
        public static final String COURSE_ENROLL = "courseEnroll";
        public static final String COMPLETED = "completed";
        public static final String COMPLETION_STATUS = "completionStatus";
        public static final String SALES_VERIFICATION_STATUS = "salesVerificationInfo.salesVerificationStatus";
        public static final String SALES_VERIFIER_ID = "salesVerificationInfo.salesVerifierId";
        public static final String ID_OF_SAM = "samIntroductionInfo.idOfSAM";
        public static final String SAM_INTRODUCTION_STATUS = "samIntroductionInfo.samIntroductionStatus";
        public static final String SAM_INTRODUCTION_INFO = "samIntroductionInfo";
        public static final String SALES_VERIFICATION_INFO = "salesVerificationInfo";
        public static final String DEVICE_DETAILS = "deviceDetails";
        public static final String LOCATION_INFO = "locationInfo";
        public static final String USER_AGENT = "userAgent";
        public static final String IP_ADDRESS = "ipAddress";
        public static final String REQUEST_SOURCE = "requestSource";
        public static final String GEO_COUNTRY = "geoCountry";
        public static final String APP_VERSION_CODE = "appVersionCode";
        public static final String APP_DEVICE_ID = "appDeviceId";
    }
}
