package com.vedantu.user.entity;

import com.vedantu.util.dbentities.mongo.AbstractMongoEntity;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import com.vedantu.util.dbutils.DBUtils;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.persistence.Id;
import org.springframework.util.StringUtils;

@Document(collection = "Org")
public class Org extends AbstractMongoEntity {

    @Id
    private String id;

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public void setEntityDefaultProperties(String callingUserId) {
        if (StringUtils.isEmpty(callingUserId)) {
            callingUserId = DBUtils.getCallingUserId();
        }
        if (!StringUtils.isEmpty(this.getId())) {
            this.setCreationTime(System.currentTimeMillis());
            this.setCreatedBy(callingUserId);
        }
        this.setLastUpdated(System.currentTimeMillis());
        this.setLastUpdatedBy(callingUserId);
    }

    public static class Constants extends AbstractMongoStringIdEntity.Constants {
        public static final String CREATION_TIME = "creationTime";
    }
}
