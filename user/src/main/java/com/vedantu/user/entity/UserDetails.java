package com.vedantu.user.entity;

import com.vedantu.User.*;
import com.vedantu.User.enums.VoltStatus;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import com.vedantu.User.enums.UserParentType;
import com.vedantu.User.response.UserDetailsInfo;
import com.vedantu.util.StringUtils;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import software.amazon.ion.Decimal;

import java.util.List;
import java.util.Map;

@Document(collection = "UserDetails")
@CompoundIndexes({
        @CompoundIndex(name = "creationTime_1", def = "{'creationTime' : 1}", background = true),
        @CompoundIndex(name = "event_1_creationTime_1", def = "{'event' : 1, 'creationTime' : 1}", background = true)})
public class UserDetails extends AbstractMongoStringIdEntity {

    @Indexed(background = true)
    private Long userId;
    @Indexed(background = true)
    private String studentName;


    //roll number for revise india
    private String rollNumber;
    @Indexed(background = true)
    private String email;
    private String studentEmail;
    @Indexed(background = true)
    private String studentPhoneNo;
    private String studentPhoneCode;
    private String school;
    private Integer grade;
    private String board;
    private String country;
    private String parentFirstName;
    private String parentLastName;
    private String parentName;
    @Indexed(background = true)
    private String parentEmail;
    private String parentPhoneNo;
    private String parentPhoneCode;
    private UserParentType parentType;
    private String address;
    private String city;
    private String event;
    private String category;
    private String utm_source;
    private String utm_medium;
    private String utm_campaign;
    private String utm_term;
    private String utm_content;
    private String channel;
    private String agentName;
    private String agentEmail;
    private List<String> examPreps;
    private String exam;
    private String studentFirstName;
    private String studentLastName;
    private String registerUrl;
    private String socialSource;
    private Gender gender;
    private List<ParentInfo> parentInfo;


    private String state;
    @Indexed(background = true)
    private String voltId;
    private VoltStatus voltStatus;
    private List<String> achievements;
    private List<String> achievementDocments;
    private List<Map<String, String>> idproofs;
    private Long approvedTime;
    private String approvalReason;
    private Boolean testAttempted;
    private String testId;

    // Revise India
    private String dob;
    private String admitCardNumber;
    private String schoolNo;
    private String centreNo;

    // Revise Jee
    private String jeeApplicationNumber;
    private Float jeeMainPercentile;



    public UserDetails() {
        super();
    }

    public UserDetails(User user) {
        this.rollNumber = user.getRollNumber();
        this.userId = user.getId();
        this.studentName = user.getFullName();
        this.email = user.getEmail();
        this.studentEmail = user.getEmail();
        this.parentEmail = user.getEmail();
        this.parentPhoneNo = user.getContactNumber();
        this.parentPhoneCode = user.getPhoneCode();
        this.studentPhoneNo = user.getContactNumber();
        this.studentPhoneCode = user.getPhoneCode();
        StudentInfo studentInfo = user.getStudentInfo();
        if (studentInfo != null) {
            this.parentFirstName = studentInfo.getParentFirstName();
            this.parentLastName = studentInfo.getParentLastName();
            this.parentName = (parentFirstName + " " + StringUtils.defaultIfEmpty(parentLastName)).trim();
            this.board = studentInfo.getBoard();
            this.school = studentInfo.getSchool();
            this.parentInfo = studentInfo.getParentInfos();
        }
        LocationInfo locationInfo = user.getLocationInfo();
        if (locationInfo != null) {
            this.city = locationInfo.getCity();
            this.country = locationInfo.getCountry();
            this.state = locationInfo.getState();
            String address = "";
            if (StringUtils.isNotEmpty(locationInfo.getStreetAddress())) {
                address += locationInfo.getStreetAddress().trim();
            }
            if (StringUtils.isNotEmpty(locationInfo.getStreetAddressLine2())) {
                address += " " + locationInfo.getStreetAddressLine2();
            }
            if (StringUtils.isNotEmpty(address.trim())) {
                this.address = address;
            }
        }
        this.utm_campaign = user.getUtm_campaign();
        this.utm_medium = user.getUtm_medium();
        this.utm_source = user.getUtm_source();
        this.utm_term = user.getUtm_term();
        this.utm_content = user.getUtm_content();
        this.channel = user.getChannel();
    }

    public UserDetailsInfo toUserDetailsInfo(UserDetails userDetails, UserBasicInfo userInfo) {
        return toUserDetailsInfo(userDetails, userInfo, false);
    }

    public UserDetailsInfo toUserDetailsInfo(UserDetails userDetails, UserBasicInfo userInfo, boolean includeOtherInfo) {
        UserDetailsInfo info = new UserDetailsInfo();
        info.setUserId(userDetails.getUserId());
        info.setAddress(userDetails.getAddress());
        info.setCategory(userDetails.getCategory());
        info.setBoard(userDetails.getBoard());
        info.setCity(userDetails.getCity());
        info.setCountry(userDetails.getCountry());
        info.setGrade(userDetails.getGrade());
        info.setParentEmail(userDetails.getParentEmail());
        info.setParentFirstName(userDetails.getParentFirstName());
        info.setParentLastName(userDetails.getParentLastName());
        info.setParentName(userDetails.getParentName());
        info.setParentPhoneCode(userDetails.getParentPhoneCode());
        info.setParentPhoneNo(userDetails.getParentPhoneNo());
        info.setSchool(userDetails.getSchool());
        info.setStudentEmail(userDetails.getEmail());
        info.setStudentName(userDetails.getStudentName());
        if(StringUtils.isEmpty(userDetails.getEmail()))
            info.setStudentEmail(userDetails.getStudentEmail());
        info.setStudentPhoneCode(userDetails.getStudentPhoneCode());
        info.setStudentPhoneNo(userDetails.getStudentPhoneNo());
        info.setStudent(userInfo);
        info.setExamPreps(userDetails.getExamPreps());
        info.setExam(userDetails.getExam());
        info.setVoltId(userDetails.getVoltId());
        info.setVoltStatus(userDetails.getVoltStatus());
        info.setAchievements(userDetails.getAchievements());
        info.setAchievementDocments(userDetails.getAchievementDocments());
        info.setIdproofs(userDetails.getIdproofs());
        info.setState(userDetails.getState());
        info.setApprovedTime(userDetails.getApprovedTime());
        info.setApprovalReason(userDetails.getApprovalReason());
        info.setParentInfo(userDetails.getParentInfo());
        info.setCreationTime(userDetails.getCreationTime());
        info.setGender(userDetails.getGender());
        info.setTestAttempted(userDetails.getTestAttempted());
        info.setTestId(userDetails.getTestId());
        info.setAdmitCardNumber(userDetails.getAdmitCardNumber());
        info.setSchoolNo(userDetails.getSchoolNo());
        info.setCentreNo(userDetails.getCentreNo());
        info.setRollNumber(userDetails.getRollNumber());
        info.setDob(userDetails.getDob());
        info.setJeeApplicationNumber(userDetails.getJeeApplicationNumber());
        info.setJeeMainPercentile(userDetails.getJeeMainPercentile());

        if (includeOtherInfo) {
            info.setEmail(userDetails.getEmail());
            info.setId(userDetails.getId());
        }
        return info;
    }


    public String getJeeApplicationNumber() {
        return jeeApplicationNumber;
    }

    public void setJeeApplicationNumber(String jeeApplicationNumber) {
        this.jeeApplicationNumber = jeeApplicationNumber;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getAdmitCardNumber() {
        return admitCardNumber;
    }

    public void setAdmitCardNumber(String admitCardNumber) {
        this.admitCardNumber = admitCardNumber;
    }

    public String getSchoolNo() {
        return schoolNo;
    }

    public void setSchoolNo(String schoolNo) {
        this.schoolNo = schoolNo;
    }

    public String getCentreNo() {
        return centreNo;
    }

    public void setCentreNo(String centreNo) {
        this.centreNo = centreNo;
    }

    public String getRollNumber() {
        return rollNumber;
    }

    public void setRollNumber(String rollNumber) {
        this.rollNumber = rollNumber;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getStudentName() {
        return studentName;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    public String getStudentEmail() {
        return studentEmail;
    }

    public void setStudentEmail(String studentEmail) {
        this.studentEmail = studentEmail.toLowerCase().trim();
    }

    public String getStudentPhoneNo() {
        return studentPhoneNo;
    }

    public void setStudentPhoneNo(String studentPhoneNo) {
        this.studentPhoneNo = studentPhoneNo;
    }

    public String getStudentPhoneCode() {
        return studentPhoneCode;
    }

    public void setStudentPhoneCode(String studentPhoneCode) {
        this.studentPhoneCode = studentPhoneCode;
    }

    public String getSchool() {
        return school;
    }

    public void setSchool(String school) {
        this.school = school;
    }

    public Integer getGrade() {
        return grade;
    }

    public void setGrade(Integer grade) {
        this.grade = grade;
    }

    public String getBoard() {
        return board;
    }

    public void setBoard(String board) {
        this.board = board;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getParentFirstName() {
        return parentFirstName;
    }

    public void setParentFirstName(String parentFirstName) {
        this.parentFirstName = parentFirstName;
    }

    public String getParentLastName() {
        return parentLastName;
    }

    public void setParentLastName(String parentLastName) {
        this.parentLastName = parentLastName;
    }

    public String getParentName() {
        return parentName;
    }

    public void setParentName(String parentName) {
        this.parentName = parentName;
    }

    public String getParentEmail() {
        return parentEmail;
    }

    public void setParentEmail(String parentEmail) {
        this.parentEmail = parentEmail.toLowerCase().trim();
    }

    public String getParentPhoneNo() {
        return parentPhoneNo;
    }

    public void setParentPhoneNo(String parentPhoneNo) {
        this.parentPhoneNo = parentPhoneNo;
    }

    public String getParentPhoneCode() {
        return parentPhoneCode;
    }

    public void setParentPhoneCode(String parentPhoneCode) {
        this.parentPhoneCode = parentPhoneCode;
    }

    public UserParentType getParentType() {
        return parentType;
    }

    public void setParentType(UserParentType parentType) {
        this.parentType = parentType;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getEvent() {
        return event;
    }

    public void setEvent(String event) {
        this.event = event;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getUtm_source() {
        return utm_source;
    }

    public void setUtm_source(String utm_source) {
        this.utm_source = utm_source;
    }

    public String getUtm_medium() {
        return utm_medium;
    }

    public void setUtm_medium(String utm_medium) {
        this.utm_medium = utm_medium;
    }

    public String getUtm_campaign() {
        return utm_campaign;
    }

    public void setUtm_campaign(String utm_campaign) {
        this.utm_campaign = utm_campaign;
    }

    public String getUtm_term() {
        return utm_term;
    }

    public void setUtm_term(String utm_term) {
        this.utm_term = utm_term;
    }

    public String getUtm_content() {
        return utm_content;
    }

    public void setUtm_content(String utm_content) {
        this.utm_content = utm_content;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public String getAgentName() {
        return agentName;
    }

    public void setAgentName(String agentName) {
        this.agentName = agentName;
    }

    public String getAgentEmail() {
        return agentEmail;
    }

    public void setAgentEmail(String agentEmail) {
        this.agentEmail = agentEmail;
    }

    public List<String> getExamPreps() {
        return examPreps;
    }

    public void setExamPreps(List<String> examPreps) {
        this.examPreps = examPreps;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email.toLowerCase().trim();
    }

    public String getExam() {
        return exam;
    }

    public void setExam(String exam) {
        this.exam = exam;
    }

    public String getStudentFirstName() {
        return studentFirstName;
    }

    public void setStudentFirstName(String studentFirstName) {
        this.studentFirstName = studentFirstName;
    }

    public String getStudentLastName() {
        return studentLastName;
    }

    public void setStudentLastName(String studentLastName) {
        this.studentLastName = studentLastName;
    }

    public String getRegisterUrl() {
        return registerUrl;
    }

    public void setRegisterUrl(String registerUrl) {
        this.registerUrl = registerUrl;
    }

    public String getSocialSource() {
        return socialSource;
    }

    public void setSocialSource(String socialSource) {
        this.socialSource = socialSource;
    }

    public String getVoltId() {
        return voltId;
    }

    public void setVoltId(String voltId) {
        this.voltId = voltId;
    }

    public VoltStatus getVoltStatus() {
        return voltStatus;
    }

    public void setVoltStatus(VoltStatus voltStatus) {
        this.voltStatus = voltStatus;
    }

    public List<String> getAchievements() {
        return achievements;
    }

    public void setAchievements(List<String> achievements) {
        this.achievements = achievements;
    }

    public List<String> getAchievementDocments() {
        return achievementDocments;
    }

    public void setAchievementDocments(List<String> achievementDocments) {
        this.achievementDocments = achievementDocments;
    }

    public List<Map<String, String>> getIdproofs() {
        return idproofs;
    }

    public void setIdproofs(List<Map<String, String>> idproofs) {
        this.idproofs = idproofs;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public Long getApprovedTime() {
        return approvedTime;
    }

    public void setApprovedTime(Long approvedTime) {
        this.approvedTime = approvedTime;
    }

    public String getApprovalReason() {
        return approvalReason;
    }

    public void setApprovalReason(String approvalReason) {
        this.approvalReason = approvalReason;
    }

    public List<ParentInfo> getParentInfo() {
        return parentInfo;
    }

    public void setParentInfo(List<ParentInfo> parentInfo) {
        this.parentInfo = parentInfo;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public Boolean getTestAttempted() {
        return testAttempted;
    }

    public void setTestAttempted(Boolean testAttempted) {
        this.testAttempted = testAttempted;
    }

    public String getTestId() {
        return testId;
    }

    public void setTestId(String testId) {
        this.testId = testId;
    }

    public Float getJeeMainPercentile() {
        return jeeMainPercentile;
    }

    public void setJeeMainPercentile(Float jeeMainPercentile) {
        this.jeeMainPercentile = jeeMainPercentile;
    }

    public static class Constants extends AbstractMongoStringIdEntity.Constants {

        public static final String USER_ID = "userId";
        public static final String PARENT_EMAIL = "parentEmail";
        public static final String STUDENT_NAME = "studentName";
        public static final String STUDENT_PHONE_NO = "studentPhoneNo";
        public static final String CATEGORY = "category";
        public static final String CITY = "city";
        public static final String STATE = "state";
        public static final String SCHOOL = "school";

        public static final String EVENT = "event";
        public static final String EMAIL = "email";
        public static final String VOLT_ID = "voltId";
        public static final String VOLT_STATUS = "voltStatus";
    }
}
