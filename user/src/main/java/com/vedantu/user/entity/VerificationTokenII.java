/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.user.entity;

import com.vedantu.User.VerificationLinkStatus;
import com.vedantu.User.VerificationTokenType;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 *
 * @author somil
 */
@Document(collection = "VerificationTokenII")
@CompoundIndexes({
        @CompoundIndex(name = "emailId_1_creationTime", def = "{'emailId': 1, 'creationTime' : -1}", background = true),
        @CompoundIndex(name = "creationTime_-1", def = "{'creationTime' : -1}", background = true),
        @CompoundIndex(name = "lastUpdated_-1", def = "{'lastUpdated' : -1}", background = true)
})
public class VerificationTokenII extends AbstractMongoStringIdEntity {
    
    public static final int VERIFICATION_CODE_LENGTH = 6;
    public static final int EMAIL_VERIFICATION_CODE_LENGTH = 16;

    private Long expiryTime;
    @Indexed(background = true)
    private Long userId;
    private String emailId; // or phone number
    private VerificationTokenType type;
    private VerificationLinkStatus status;

    // code if any --> this will be used for sms verification
    //TODO: For previous email tokens, migrate ids into this
    @Indexed(background = true)
    private String code;

    // code if any --> this will be used for sms verification
    private String phoneCode;

    public VerificationTokenII() {

        super();
    }

    public VerificationTokenII(Long userId, String emailId,
                               VerificationTokenType type, VerificationLinkStatus status) {

        super();
        this.userId = userId;
        this.emailId = emailId;
        this.type = type;
        this.status = status;
    }

    public Long getExpiryTime() {

        return expiryTime;
    }

    public void setExpiryTime(Long expiryTime) {

        this.expiryTime = expiryTime;
    }

    public Long getUserId() {

        return userId;
    }

    public void setUserId(Long userId) {

        this.userId = userId;
    }

    public VerificationTokenType getType() {

        return type;
    }

    public void setType(VerificationTokenType type) {

        this.type = type;
    }

    public VerificationLinkStatus getStatus() {

        return status;
    }

    public void setStatus(VerificationLinkStatus status) {

        this.status = status;
    }

    public String getEmailId() {

        return emailId;
    }

    public void setEmailId(String emailId) {

        this.emailId = emailId;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getPhoneCode() {
        return phoneCode;
    }

    public void setPhoneCode(String phoneCode) {
        this.phoneCode = phoneCode;
    }


    public static class Constants extends AbstractMongoStringIdEntity.Constants {

        public static final String USER_ID = "userId";
        public static final String TYPE = "type";
        public static final String STATUS = "status";
        public static final String EMAIL_ID = "emailId";
        public static final String PHONE_CODE = "phoneCode";
        public static final String CODE = "code";
        public static final String CREATION_TIME = "creationTime";
    }

}
