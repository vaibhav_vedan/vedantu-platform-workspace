package com.vedantu.user.entity.temp;

import com.sun.org.apache.xpath.internal.operations.Bool;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "LoginData")
public class LoginData extends AbstractMongoStringIdEntity {

    private String country;
    private Long lastLoginTime;
    private String ipAddress;
    private Boolean passwordChanged;
    private Long lastPasswordChangedTime;
    private String isp;

    @Indexed(background = true)
    private String email;

    public String getIsp() {
        return isp;
    }

    public void setIsp(String isp) {
        this.isp = isp;
    }

    public Long getLastPasswordChangedTime() {
        return lastPasswordChangedTime;
    }

    public void setLastPasswordChangedTime(Long lastPasswordChangedTime) {
        this.lastPasswordChangedTime = lastPasswordChangedTime;
    }

    public Boolean getPasswordChanged() {
        return passwordChanged;
    }

    public void setPasswordChanged(Boolean passwordChanged) {
        this.passwordChanged = passwordChanged;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public Long getLastLoginTime() {
        return lastLoginTime;
    }

    public void setLastLoginTime(Long lastLoginTime) {
        this.lastLoginTime = lastLoginTime;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public static class Constants extends AbstractMongoStringIdEntity.Constants {
        public static final String COUNTRY = "country";
        public static final String LAST_LOGIN_TIME = "lastLoginTime";
        public static final String IP_ADDRESS = "ipAddress";
        public static final String PASSWORD_CHANGED = "passwordChanged";
        public static final String EMAIL = "email";
        public static final String LAST_PASSWORD_CHANGED_TIME = "lastPasswordChangedTime";
    }

}
