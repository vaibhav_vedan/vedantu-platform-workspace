/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.user.entity;

import java.lang.reflect.InvocationTargetException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.beanutils.BeanUtils;

/**
 *
 * @author somil
 */
public class PhoneNumber implements IPhoneNumber {

    private Long id;
    private String number;
    private String phoneCode;
    private Boolean isVerified;
    private Boolean isDND;
    private Boolean isWhitelisted;
    private Boolean isActiveForCall;

    public PhoneNumber() {
        super();
    }

    public PhoneNumber(String number, String phoneCode, Boolean isVerified, Boolean isDND,
            Boolean isWhitelisted, Boolean isActiveForCall) {
        super();
        this.number = number;
        this.phoneCode = phoneCode;
        this.isVerified = isVerified;
        this.isDND = isDND;
        this.isWhitelisted = isWhitelisted;
        this.isActiveForCall = isActiveForCall;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getPhoneCode() {
        return phoneCode;
    }

    public void setPhoneCode(String phoneCode) {
        this.phoneCode = phoneCode;
    }

    public Boolean getIsVerified() {
        return isVerified;
    }

    public void setIsVerified(Boolean isVerified) {
        this.isVerified = isVerified;
    }

    public Boolean getIsDND() {
        return isDND;
    }

    public void setIsDND(Boolean isDND) {
        this.isDND = isDND;
    }

    public Boolean getIsWhitelisted() {
        return isWhitelisted;
    }

    public void setIsWhitelisted(Boolean isWhitelisted) {
        this.isWhitelisted = isWhitelisted;
    }

    public Boolean getIsActiveForCall() {
        return isActiveForCall;
    }

    public void setIsActiveForCall(Boolean isActiveForCall) {
        this.isActiveForCall = isActiveForCall;
    }

    @Override
    public String _getNumber() {
        return number;
    }

    @Override
    public String _getPhoneCode() {
        return phoneCode;
    }

    @Override
    public Boolean _getIsVerified() {
        return isVerified;
    }

    @Override
    public Boolean _getIsDND() {
        return isDND;
    }

    @Override
    public Boolean _getIsWhitelisted() {
        return isWhitelisted;
    }

    @Override
    public String toString() {
        return String
                .format("{id=%s, number=%s, isVerified=%s, isDND=%s, isWhitelisted=%s, isActiveForCall=%s}",
                        id, number, isVerified, isDND, isWhitelisted,
                        isActiveForCall);
    }

//    public com.vedantu.User.PhoneNumber toPhonePojo() {
//        com.vedantu.User.PhoneNumber phonePojo = new com.vedantu.User.PhoneNumber();
//        try {
//            BeanUtils.copyProperties(phonePojo, this);
//        } catch (IllegalAccessException | InvocationTargetException ex) {
//            Logger.getLogger(User.class.getName()).log(Level.SEVERE, null, ex);
//        }
//        return phonePojo;
//    }
}
