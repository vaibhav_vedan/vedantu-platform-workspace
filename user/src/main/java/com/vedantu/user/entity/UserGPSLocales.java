package com.vedantu.user.entity;

import java.util.List;

import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import com.vedantu.user.pojo.GPSLocalePojo;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;


@Data
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Document(collection = "UserGPSLocales")
public class UserGPSLocales extends AbstractMongoStringIdEntity{

	@Indexed(unique = true, background = true)
	private String userId;

	private GPSLocalePojo latestLocale;

	private List<GPSLocalePojo> historicalLocales;

	public static class Constants extends AbstractMongoStringIdEntity.Constants
	{
		public static final String USER_ID = "userId";

	}

}
