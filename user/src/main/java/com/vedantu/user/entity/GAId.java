package com.vedantu.user.entity;

import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;

import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Map;
import java.util.Set;

/**
 *
 * @author Ausaf
 */

@Document(collection = "GAId")
@CompoundIndexes({
    @CompoundIndex(def = "{'gaids': 1, 'userId': 1}", background = true, useGeneratedName = true)
})
public class GAId extends AbstractMongoStringIdEntity
{
    @Indexed(name = "userId_1", unique = true, background = true)
    private String userId;
    @Indexed(background = true)
    private Set<String> gaids;
    private Map<String, String> appVersionMap;

    public GAId()
    {
        super();
    }

    public Map<String, String> getAppVersionMap() {
        return appVersionMap;
    }

    public void setAppVersionMap(Map<String, String> appVersionMap) {
        this.appVersionMap = appVersionMap;
    }

    public GAId(String userId, Set<String> gaids, Map<String, String> appVersionMap)
    {
        super();
        this.userId = userId;
        this.gaids = gaids;
        this.appVersionMap = appVersionMap;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Set<String> getGaids() {
        return gaids;
    }

    public void setGaids(Set<String> gaids) {
        this.gaids = gaids;
    }

    public static class Constants extends AbstractMongoStringIdEntity.Constants
    {
        public static final String USER_ID = "userId";
        public static final String GAIDS = "gaids";
    }
}
