package com.vedantu.user.entity;

import java.util.Map;

import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import com.vedantu.User.enums.UserLeadsActivityType;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;

@Document(collection = "UserLeadActivity")
@CompoundIndexes({
    @CompoundIndex(name = "userLeadsId_source_eventName_eventValue_1", def = "{'userLeadsId' : 1, 'source': 1, 'eventName':1, 'eventValue':1}", background = true)
})
public class UserLeadActivity extends AbstractMongoStringIdEntity {

    @Indexed(background = true)
    private String userLeadsId;
    private UserLeadsActivityType activity;// create enum

    private Map<String, String> metadata;
    private String utm_source;
    private String utm_campaign;
    private String utm_medium;
    private String utm_term;
    private String utm_content;
    private String channel;
    private String source;
    private String ipAddress;
    private String referenceId;
    private String eventName;
    private String eventValue;
    private String deviceType;
    private String deviceName;

    public String getUserLeadsId() {
        return userLeadsId;
    }

    public void setUserLeadsId(String userLeadsId) {
        this.userLeadsId = userLeadsId;
    }

    public UserLeadsActivityType getActivity() {
        return activity;
    }

    public void setActivity(UserLeadsActivityType activity) {
        this.activity = activity;
    }

    public String getUtm_source() {
        return utm_source;
    }

    public void setUtm_source(String utm_source) {
        this.utm_source = utm_source;
    }

    public String getUtm_campaign() {
        return utm_campaign;
    }

    public void setUtm_campaign(String utm_campaign) {
        this.utm_campaign = utm_campaign;
    }

    public String getUtm_medium() {
        return utm_medium;
    }

    public void setUtm_medium(String utm_medium) {
        this.utm_medium = utm_medium;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public Map<String, String> getMetadata() {
        return metadata;
    }

    public void setMetadata(Map<String, String> metadata) {
        this.metadata = metadata;
    }

    public String getReferenceId() {
        return referenceId;
    }

    public void setReferenceId(String referenceId) {
        this.referenceId = referenceId;
    }

    public String getUtm_term() {
        return utm_term;
    }

    public void setUtm_term(String utm_term) {
        this.utm_term = utm_term;
    }

    public String getUtm_content() {
        return utm_content;
    }

    public void setUtm_content(String utm_content) {
        this.utm_content = utm_content;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public String getEventValue() {
        return eventValue;
    }

    public void setEventValue(String eventValue) {
        this.eventValue = eventValue;
    }

    public String getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType;
    }

    public String getDeviceName() {
        return deviceName;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }

    public static class Constants extends AbstractMongoStringIdEntity.Constants {

        public static final String SOURCE = "source";
        public static final String EVENT_NAME = "eventName";
        public static final String EVENT_VALUE = "eventValue";
        public static final String USER_LEAD_ID = "userLeadsId";
    }
}
