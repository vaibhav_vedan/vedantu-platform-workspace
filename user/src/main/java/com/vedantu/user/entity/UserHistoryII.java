package com.vedantu.user.entity;

import com.vedantu.User.FeatureSource;
import com.vedantu.User.Gender;
import com.vedantu.User.LocationInfo;
import com.vedantu.User.Role;
import com.vedantu.User.SocialInfo;
import com.vedantu.User.StudentInfo;
import com.vedantu.User.TeacherInfo;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 *
 * @author somil
 */
@Data
@NoArgsConstructor
@Document(collection = "UserHistoryII")
@CompoundIndexes(
        {
                @CompoundIndex(name = "teacherInfo.extensionNumber_1",def = "{'teacherInfo.extensionNumber':1}",background = true),
                @CompoundIndex(name = "teacherInfo.dexInfo.mainTags_1",def = "{'teacherInfo.dexInfo.mainTags':1}",background = true)
        }
)
public class UserHistoryII extends AbstractMongoStringIdEntity {
    private Boolean profileEnabled ;
    private String email;
    @Indexed(background = true)
    private Long userId;
    private String contactNumber;
    private String phoneCode;
    private String firstName;
    private String lastName;
    private String fullName;
    private String password;
    private Gender gender;
    private Role role;
    private Boolean isEmailVerified ;
    private Long profilePicId;
    private String profilePicUrl; // this will be used in case of social signu
    private String profilePicPath;
    private List<String> languagePrefs;
    private StudentInfo studentInfo ;
    private TeacherInfo teacherInfo;
    private LocationInfo locationInfo;
    private SocialInfo socialInfo;
    private String tncVersion;
    private String socialSource;
    private String utm_source;
    private String utm_medium;
    private String utm_campaign;
    private String utm_term;
    private String utm_content;
    private String channel;
    private Long appId;
    private Boolean isContactNumberVerified;
    private Boolean isContactNumberDND;
    private Boolean isContactNumberWhitelisted;
    private String referralCode;
    private String referrerCode;// userId of the referrer user
    private Integer referrerBonus; // bonus in paisa
    private Boolean referrerSuccess;
    private List<PhoneNumber> phones = new ArrayList<>();
    private boolean isNewUser;
    private Boolean parentRegistration;
    private String signUpURL;
    private FeatureSource signUpFeature;
    private String ipAddress;
    private Long otfSubscriptionCount = 0l;
    private Long subscriptionRequestCount = 0l;
    private String deviceId;
    private String lastEditedBy;
    private Boolean passwordHashed;
    private Boolean passwordAutogenerated;
    private Long blockedBy;
    private Long blockedAt;
    private Set<String> pendingBundleIdForOnboarding;
    private Set<String> completedBundleIdForOnboarding;
}
