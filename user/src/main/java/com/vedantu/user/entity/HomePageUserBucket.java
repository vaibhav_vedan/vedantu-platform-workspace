package com.vedantu.user.entity;


import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import com.vedantu.util.dbentitybeans.mongo.EntityState;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Builder
@CompoundIndexes({
        @CompoundIndex(name = "lastUpdated_-1", def = "{'lastUpdated': -1}", background = true)})
public class HomePageUserBucket extends AbstractMongoStringIdEntity {
    private Long userId;
    private Boolean canAccessNewHomepage;

    public static class Constants extends AbstractMongoStringIdEntity.Constants {
        public static final String USER_ID = "userId";
        public static final String CAN_ACCESS_NEW_HOMEPAGE = "canAccessNewHomepage";
    }
}