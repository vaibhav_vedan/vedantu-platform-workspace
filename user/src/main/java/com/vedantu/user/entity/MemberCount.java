package com.vedantu.user.entity;

import com.vedantu.User.Role;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "MemberCount")
public class MemberCount extends AbstractMongoStringIdEntity {
    private Role role;
    private Long count;
    private Boolean isEmailVerified;


    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public Long getCount() {
        return count;
    }

    public void setCount(Long count) {
        this.count = count;
    }

    public Boolean getEmailVerified() {
        return isEmailVerified;
    }

    public void setEmailVerified(Boolean emailVerified) {
        isEmailVerified = emailVerified;
    }

    public static class Constants extends AbstractMongoStringIdEntity.Constants {
        public static final String COUNT = "count";
        public static final String ROLE = "role";
        public static final String VERIFIED = "isEmailVerified";
    }
}
