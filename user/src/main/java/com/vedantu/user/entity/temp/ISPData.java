package com.vedantu.user.entity.temp;

import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "ISPData")
public class ISPData extends AbstractMongoStringIdEntity {
    String country;
    String isp;
    String loginParam;
    String ipAddress;


    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getIsp() {
        return isp;
    }

    public void setIsp(String isp) {
        this.isp = isp;
    }

    public String getLoginParam() {
        return loginParam;
    }

    public void setLoginParam(String loginParam) {
        this.loginParam = loginParam;
    }

    public static class Constants extends AbstractMongoStringIdEntity.Constants {
        public static final String COUNTRY = "country";
        public static final String ISP = "isp";
        public static final String LOGIN_PARAM = "loginParam";
        public static final String IP_ADDRESS = "ipAddress";
    }
}
