package com.vedantu.user.entity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Logger;

import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.util.CommonUtils;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import com.vedantu.User.FeatureSource;
import com.vedantu.User.Gender;
import com.vedantu.User.LocationInfo;
import com.vedantu.User.Role;
import com.vedantu.User.SaleClosed;
import com.vedantu.User.SocialInfo;
import com.vedantu.User.StudentInfo;
import com.vedantu.User.TeacherInfo;
import com.vedantu.User.UTMParams;
import com.vedantu.User.enums.SubRole;
import com.vedantu.User.request.SignUpReq;
import com.vedantu.User.request.SocialSource;
import com.vedantu.user.enums.VerifiedType;
import com.vedantu.user.requests.SignUpUserReq;
import com.vedantu.util.StringUtils;
import com.vedantu.util.dbentities.mongo.AbstractMongoLongIdEntity;

/**
 *
 * @author somil
 */
@Document(collection = "User")
@CompoundIndexes({
    @CompoundIndex(name = "contactNumber_1_phoneCode_1", def = "{'contactNumber' : 1, 'phoneCode': 1}", background = true, unique = true)
    ,
    @CompoundIndex(name = "role_1_creationTime_-1", def = "{'role' : 1, 'creationTime': -1}", background = true)
    ,
    @CompoundIndex(name = "role_1_fullName_1_creationTime_-1", def = "{'role': 1, 'fullName': 1, 'creationTime': -1}", background = true)
    ,
    @CompoundIndex(name = "orgId_creationTime", def = "{'orgId': 1, 'creationTime': 1}", background = true),
        @CompoundIndex(name = "emailId_creationTime", def = "{'emailId': 1, 'creationTime': 1}", background = true)
})
public class User extends AbstractMongoLongIdEntity implements IPhoneNumber {

    private Boolean profileEnabled = Boolean.TRUE;

    //@Indexed(name = "email_2", sparse = true, unique = true, background = true)

    @Indexed(name = "email_1", background = true)
    private String email;

    private String rollNumber;

    private String contactNumber;

    private String tempContactNumber;

    private String tempPhoneCode;

    private String phoneCode;

    private String firstName;

    private String lastName;

    @Indexed(name = "fullName_1", background = true)
    private String fullName;

    private String password;

    private String otpPassword;

    private Gender gender;

    private Role role;

    private Boolean isEmailVerified = Boolean.FALSE;

    private Long profilePicId;

    private String profilePicUrl; // this will be used in case of social signup

    private String profilePicPath;

    private List<String> languagePrefs;

    private StudentInfo studentInfo = new StudentInfo();

    private TeacherInfo teacherInfo;

    private LocationInfo locationInfo;

    private SocialInfo socialInfo;

    private String tncVersion;

    private String socialSource;

    private String utm_source;

    private String utm_medium;

    private String utm_campaign;

    private String utm_term;

    private String utm_content;

    private String channel;

    private Long appId;

    private Boolean isContactNumberVerified;

    private VerifiedType contactNumberVerifiedBy;

    private Boolean isContactNumberDND;

    private Boolean isContactNumberWhitelisted;

    private String referralCode;

    private String referrer;

    // userId of the referrer user
    private String referrerCode;

    // bonus in paisa
    private Integer referrerBonus;

    private Boolean referrerSuccess;

    private List<PhoneNumber> phones = new ArrayList<>();

    private boolean isNewUser;

    private Boolean parentRegistration;

    private String signUpURL;

    private FeatureSource signUpFeature;

    private String signUpFeatureRefId;

    private String ipAddress;

    private Long otfSubscriptionCount = 0l;

    private Long subscriptionRequestCount = 0l;

    private String deviceId;

    private String lastEditedBy;

    private Boolean passwordHashed;

    private Boolean passwordAutogenerated;

    private Long blockedBy;

    private Long blockedAt;

    private String blockedReason;

    private String blockedReasonType;

    private SaleClosed saleClosed;

    private Long passwordChangedAt;

    private Long otpPasswordChangedAt;

    private Boolean optIn;

    private List<String> recentHashedPasswords;

    private boolean telegramLinked = false;

    private Long dob;

    private Set<String> pendingBundleIdForOnboarding = new HashSet<>();

    private Set<String> completedBundleIdForOnboarding = new HashSet<>();

    private String orgId;

    public User() {

        super();
    }

    public User(String email, String contactNumber, String firstName, String lastName, String password, Gender gender,
            Role role, LocationInfo locationInfo) {
        super();
        this.email = email;
        this.contactNumber = contactNumber;
        this.firstName = firstName;
        this.lastName = lastName;
        this.password = password;
        this.gender = gender;
        this.role = role;
        this.locationInfo = locationInfo;
    }

    public User(SignUpUserReq signUpReq) throws BadRequestException {
        if (StringUtils.isNotEmpty(signUpReq.getEmail())) {
            if(CommonUtils.isValidEmailId(signUpReq.getEmail())) {
                email = signUpReq.getEmail().toLowerCase().trim();
            }
            else {
                throw new BadRequestException(ErrorCode.INVALID_EMAILID, "email id is not valid");
            }
        }

        fullName = signUpReq.getFullName();
        String[] name = fullName.trim().split("\\s+");
        firstName = name[0];
        if (null != name && name.length > 1) {
            StringBuffer sb = new StringBuffer();

            for (int i = 1; i < name.length; i++) {
                sb.append(name[i] + " ");
            }

            String userLastName = sb.toString();

            lastName = userLastName;
        }

        rollNumber = signUpReq.getRollNumber();
        role = signUpReq.getRole();
        password = signUpReq.getPassword();
        // user.setLanguagePrefs(User.LanguagePref.fromStringList(languagePrefs));
        contactNumber = signUpReq.getPhoneNumber();

        String formattedPhoneCode = (null != signUpReq.getPhoneCode()) ? signUpReq.getPhoneCode().replace("+", "") : null;
        signUpReq.setPhoneCode(formattedPhoneCode);

        studentInfo = signUpReq.getStudentInfo();
        if (StringUtils.isNotEmpty(signUpReq.getStudentInfo().getTarget())) {
            List<String> targets = new ArrayList<>();
            targets.add(signUpReq.getStudentInfo().getTarget().toUpperCase());
            studentInfo.setExamTargets(targets);
        }

        phoneCode = signUpReq.getPhoneCode();
        locationInfo = signUpReq.getLocationInfo();
        socialInfo = signUpReq.getSocialInfo();
        socialSource = signUpReq.getSocialSource();
        profilePicUrl = signUpReq.getPicUrl();
        orgId = signUpReq.getOrgId();

        SocialSource _socialSource = signUpReq.__getSocialSource();
        if (SocialSource.FACEBOOK.equals(_socialSource) || SocialSource.GOOGLE.equals(_socialSource)) {
            isEmailVerified = true;
        } else {
            isEmailVerified = false;
        }
        UTMParams utm = signUpReq.getUtm();
        if (utm != null) {
            utm_campaign = utm.getUtm_campaign();
            utm_medium = utm.getUtm_medium();
            utm_source = utm.getUtm_source();
            utm_term = utm.getUtm_term();
            utm_content = utm.getUtm_content();
            channel = utm.getChannel();
        }
        if (signUpReq.getParentRegistration() == null) {
            parentRegistration = false;
        } else {
            parentRegistration = signUpReq.getParentRegistration();
        }
        referrerCode = signUpReq.getReferrerCode();
        referrerBonus = signUpReq.getReferrerBonus();
        if (signUpReq.getTncVersion() != null) {
            tncVersion = signUpReq.getTncVersion();
        }

        signUpURL = signUpReq.getSignUpURL();
        if (signUpReq.getSignUpFeature() != null) {
            signUpFeature = signUpReq.getSignUpFeature();
        }
        if (StringUtils.isNotEmpty(signUpReq.getSignUpFeatureRefId())) {
            signUpFeatureRefId = signUpReq.getSignUpFeatureRefId();
        }
        if (signUpReq.getIpAddress() != null && !signUpReq.getIpAddress().contains("127.0.0.1")) {
            this.ipAddress = signUpReq.getIpAddress();
        }
        this.deviceId = signUpReq.getDeviceId();
        this.optIn = signUpReq.getOptIn();
    }

    public User(SignUpReq signUpReq) throws BadRequestException {
        if(StringUtils.isNotEmpty(signUpReq.getEmail())){
            if(CommonUtils.isValidEmailId(signUpReq.getEmail())) {
                email = signUpReq.getEmail().toLowerCase().trim();
            }
            else {
                throw new BadRequestException(ErrorCode.INVALID_EMAILID, "email id is not valid");
            }
        }

        firstName = signUpReq.getFirstName();
        lastName = signUpReq.getLastName();
        fullName = (firstName + " " + StringUtils.defaultIfEmpty(lastName)).trim();
        password = signUpReq.getPassword();
        role = signUpReq.getRole();
        gender = signUpReq.getGender();
        // user.setLanguagePrefs(User.LanguagePref.fromStringList(languagePrefs));
        languagePrefs = signUpReq.getLanguagePrefs();

        contactNumber = signUpReq.getContactNumber();
        phoneCode = signUpReq.getPhoneCode();
        locationInfo = signUpReq.getLocationInfo();
        socialInfo = signUpReq.getSocialInfo();
        socialSource = signUpReq.getSocialSource();
        profilePicUrl = signUpReq.getPicUrl();

        if (Role.STUDENT.equals(signUpReq.getRole())) {
            studentInfo = signUpReq.getStudentInfo();
        } else if (Role.TEACHER.equals(signUpReq.getRole())) {
            teacherInfo = signUpReq.getTeacherInfo();
        }

        SocialSource _socialSource = signUpReq.__getSocialSource();
        if (SocialSource.FACEBOOK.equals(_socialSource) || SocialSource.GOOGLE.equals(_socialSource)) {
            isEmailVerified = true;
        } else {
            isEmailVerified = false;
        }
        UTMParams utm = signUpReq.getUtm();
        if (utm != null) {
            utm_campaign = utm.getUtm_campaign();
            utm_medium = utm.getUtm_medium();
            utm_source = utm.getUtm_source();
            utm_term = utm.getUtm_term();
            utm_content = utm.getUtm_content();
            channel = utm.getChannel();
        }
        if (signUpReq.getParentRegistration() == null) {
            parentRegistration = false;
        } else {
            parentRegistration = signUpReq.getParentRegistration();
        }
        referrerCode = signUpReq.getReferrerCode();
        referrerBonus = signUpReq.getReferrerBonus();
        if (signUpReq.getTncVersion() != null) {
            tncVersion = signUpReq.getTncVersion();
        }

        signUpURL = signUpReq.getSignUpURL();
        if (signUpReq.getSignUpFeature() != null) {
            signUpFeature = signUpReq.getSignUpFeature();
        }
        if (StringUtils.isNotEmpty(signUpReq.getSignUpFeatureRefId())) {
            signUpFeatureRefId = signUpReq.getSignUpFeatureRefId();
        }
        if (signUpReq.getIpAddress() != null && !signUpReq.getIpAddress().contains("127.0.0.1")) {
            this.ipAddress = signUpReq.getIpAddress();
        }
        this.deviceId = signUpReq.getDeviceId();
        this.optIn = signUpReq.getOptIn();
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        if (StringUtils.isNotEmpty(orgId)) {
            this.orgId = orgId.toLowerCase();
        } else {
            this.orgId = null;
        }
    }

    public String getRollNumber() {
        return rollNumber;
    }

    public void setRollNumber(String rollNumber) {
        this.rollNumber = rollNumber;
    }

    public boolean isIsNewUser() {
        return isNewUser;
    }

    public void setIsNewUser(boolean isNewUser) {
        this.isNewUser = isNewUser;
    }

    public LocationInfo getLocationInfo() {

        return locationInfo;
    }

    public void setLocationInfo(LocationInfo locationInfo) {

        this.locationInfo = locationInfo;
    }

    public String getTempContactNumber() {

        return tempContactNumber;
    }

    public void setTempContactNumber(String tempContactNumber) {

        this.tempContactNumber = tempContactNumber;
    }

    public String getContactNumber() {

        return contactNumber;
    }

    public void setContactNumber(String phoneNumber) {

        this.contactNumber = phoneNumber;
    }

    public String getPhoneCode() {

        return phoneCode;
    }

    public void setPhoneCode(String phoneCode) {

        this.phoneCode = phoneCode;
    }

    public Boolean getProfileEnabled() {
        if (profileEnabled == null) {
            profileEnabled = true;
        }
        return profileEnabled;
    }

    public void setProfileEnabled(Boolean profileEnabled) {
        this.profileEnabled = profileEnabled;
    }

    public String getEmail() {

        return email;
    }

    public void setEmail(String email) {

        this.email = email.toLowerCase().trim();
    }

    public String getFirstName() {

        return firstName;
    }

    public void setFirstName(String firstName) {

        this.firstName = StringUtils.captialize(firstName).trim();
    }

    public String getLastName() {

        return StringUtils.defaultIfEmpty(lastName);
    }

    public void setLastName(String lastName) {

        this.lastName = StringUtils.captialize(lastName).trim();
    }

    public String getFullName() {
        return (firstName + " " + StringUtils.defaultIfEmpty(lastName)).trim();
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getPassword() {

        return password;
    }

    public void setPassword(String password) {

        this.password = password;
    }

    public Role getRole() {

        return role;
    }

    public void setRole(Role role) {

        this.role = role;
    }

    public VerifiedType getContactNumberVerifiedBy() {
        return contactNumberVerifiedBy;
    }

    public void setContactNumberVerifiedBy(VerifiedType contactNumberVerifiedBy) {
        this.contactNumberVerifiedBy = contactNumberVerifiedBy;
    }

    public Boolean getIsEmailVerified() {

        return isEmailVerified;
    }

    public void setIsEmailVerified(Boolean isEmailVerified) {

        this.isEmailVerified = isEmailVerified;
    }

    public Long getProfilePicId() {

        return profilePicId;
    }

    public void setProfilePicId(Long profilePicId) {

        this.profilePicId = profilePicId;
    }

    public List<String> getLanguagePrefs() {

        return languagePrefs;
    }

    public void setLanguagePrefs(List<String> languagePrefs) {

        this.languagePrefs = languagePrefs;
    }

    public StudentInfo getStudentInfo() {

        return studentInfo;
    }

    public void setStudentInfo(StudentInfo studentInfo) {

        this.studentInfo = studentInfo;
    }

    public TeacherInfo getTeacherInfo() {

        return teacherInfo;
    }

    public void setTeacherInfo(TeacherInfo teacherInfo) {

        this.teacherInfo = teacherInfo;
    }

    public SocialInfo getSocialInfo() {

        return socialInfo;
    }

    public void setSocialInfo(SocialInfo socialInfo) {

        this.socialInfo = socialInfo;
    }

    public String _getFullName() {

        return (firstName + " " + StringUtils.defaultIfEmpty(lastName)).trim();
    }

    public Gender getGender() {

        return gender;
    }

    public void setGender(Gender gender) {

        this.gender = gender;
    }

    public String getTncVersion() {
        return tncVersion;
    }

    public void setTncVersion(String tncVersion) {
        this.tncVersion = tncVersion;
    }

    public String getSocialSource() {
        return socialSource;
    }

    public void setSocialSource(String socialSource) {
        this.socialSource = socialSource;
    }

    public String getProfilePicUrl() {
        return profilePicUrl;
    }

    public void setProfilePicUrl(String profilePicUrl) {
        this.profilePicUrl = profilePicUrl;
    }

    public String getUtm_term() {
        return utm_term;
    }

    public void setUtm_term(String utm_term) {
        this.utm_term = utm_term;
    }

    public String getUtm_content() {
        return utm_content;
    }

    public void setUtm_content(String utm_content) {
        this.utm_content = utm_content;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public String getUtm_source() {
        return utm_source;
    }

    public void setUtm_source(String utm_source) {
        this.utm_source = utm_source;
    }

    public String getUtm_medium() {
        return utm_medium;
    }

    public void setUtm_medium(String utm_medium) {
        this.utm_medium = utm_medium;
    }

    public String getUtm_campaign() {
        return utm_campaign;
    }

    public void setUtm_campaign(String utm_campaign) {
        this.utm_campaign = utm_campaign;
    }

    public Boolean getIsContactNumberVerified() {
        return isContactNumberVerified == null ? false : isContactNumberVerified;
    }

    public void setIsContactNumberVerified(Boolean isContactNumberVerified) {
        this.isContactNumberVerified = isContactNumberVerified;
    }

    public Boolean getIsContactNumberDND() {
        return isContactNumberDND == null ? false : isContactNumberDND;
    }

    public void setIsContactNumberDND(Boolean isContactNumberDND) {
        this.isContactNumberDND = isContactNumberDND;
    }

    public Boolean getIsContactNumberWhitelisted() {
        return isContactNumberWhitelisted == null ? false : isContactNumberWhitelisted;
    }

    public void setIsContactNumberWhitelisted(Boolean isContactNumberWhitelisted) {
        this.isContactNumberWhitelisted = isContactNumberWhitelisted;
    }

    public List<PhoneNumber> getPhones() {
        return phones == null ? new ArrayList<>() : phones;
    }

    public void setPhones(List<PhoneNumber> phones) {
        this.phones = phones;
    }

    public Long getAppId() {
        return appId;
    }

    public void setAppId(Long appId) {
        this.appId = appId;
    }

    public Boolean getParentRegistration() {
        return parentRegistration;
    }

    public void setParentRegistration(Boolean parentRegistration) {
        this.parentRegistration = parentRegistration;
    }

    @Override
    public String _getNumber() {
        return contactNumber;
    }

    @Override
    public String _getPhoneCode() {
        return phoneCode;
    }

    @Override
    public Boolean _getIsVerified() {
        return isContactNumberVerified;
    }

    @Override
    public Boolean _getIsDND() {
        return isContactNumberDND;
    }

    @Override
    public Boolean _getIsWhitelisted() {
        return isContactNumberWhitelisted;
    }

    public String getReferrerCode() {
        return referrerCode;
    }

    public void setReferrerCode(String referrerCode) {
        this.referrerCode = referrerCode;
    }

    public Integer getReferrerBonus() {
        return referrerBonus;
    }

    public void setReferrerBonus(Integer referrerBonus) {
        this.referrerBonus = referrerBonus;
    }

    public Boolean getReferrerSuccess() {
        return referrerSuccess;
    }

    public void setReferrerSuccess(Boolean referrerSuccess) {
        this.referrerSuccess = referrerSuccess;
    }

    public boolean isNewUser() {
        return isNewUser;
    }

    public void setNewUser(boolean isNewUser) {
        this.isNewUser = isNewUser;
    }

    public String getSignUpURL() {
        return signUpURL;
    }

    public void setSignUpURL(String signUpURL) {
        this.signUpURL = signUpURL;
    }

    public FeatureSource getSignUpFeature() {
        return signUpFeature;
    }

    public void setSignUpFeature(FeatureSource signUpFeature) {
        this.signUpFeature = signUpFeature;
    }

    public String getProfilePicPath() {
        return profilePicPath;
    }

    public void setProfilePicPath(String profilePicPath) {
        this.profilePicPath = profilePicPath;
    }

    public IPhoneNumber _getActivePhoneNumberForCall() {

        try {
            for (PhoneNumber phone : getPhones()) {
                if (phone.getIsActiveForCall() != null && phone.getIsActiveForCall()) {
                    return phone;
                }
            }
        } catch (Throwable e) {
            Logger.getLogger("User").throwing("User", "_getActivePhoneNumberForCall", e);
            e.printStackTrace();
        }

        return this;
    }

    public String getReferralCode() {
        return referralCode;
    }

    public void setReferralCode(String referralCode) {
        this.referralCode = referralCode;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public Long getOtfSubscriptionCount() {
        return otfSubscriptionCount;
    }

    public void setOtfSubscriptionCount(Long otfSubscriptionCount) {
        this.otfSubscriptionCount = otfSubscriptionCount;
    }

    public Long getSubscriptionRequestCount() {
        return subscriptionRequestCount;
    }

    public void setSubscriptionRequestCount(Long subscriptionRequestCount) {
        this.subscriptionRequestCount = subscriptionRequestCount;
    }

    public String getOtpPassword() {
        return otpPassword;
    }

    public void setOtpPassword(String otpPassword) {
        this.otpPassword = otpPassword;
    }

    public Long getOtpPasswordChangedAt() {
        return otpPasswordChangedAt;
    }

    public void setOtpPasswordChangedAt(Long otpPasswordChangedAt) {
        this.otpPasswordChangedAt = otpPasswordChangedAt;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getLastEditedBy() {
        return lastEditedBy;
    }

    public void setLastEditedBy(String lastEditedBy) {
        this.lastEditedBy = lastEditedBy;
    }

    public Boolean getPasswordHashed() {
        return passwordHashed;
    }

    public void setPasswordHashed(Boolean passwordHashed) {
        this.passwordHashed = passwordHashed;
        if (StringUtils.isNotEmpty(this.email)) {
            this.email = this.email.toLowerCase();
        }
    }

    public Boolean getPasswordAutogenerated() {
        return passwordAutogenerated;
    }

    public void setPasswordAutogenerated(Boolean passwordAutogenerated) {
        this.passwordAutogenerated = passwordAutogenerated;
    }

    public Long getBlockedBy() {
        return blockedBy;
    }

    public void setBlockedBy(Long blockedBy) {
        this.blockedBy = blockedBy;
    }

    public Long getBlockedAt() {
        return blockedAt;
    }

    public void setBlockedAt(Long blockedAt) {
        this.blockedAt = blockedAt;
    }

    public SaleClosed getSaleClosed() {
        return saleClosed;
    }

    public void setSaleClosed(SaleClosed saleClosed) {
        this.saleClosed = saleClosed;
    }

    public String getSignUpFeatureRefId() {
        return signUpFeatureRefId;
    }

    public void setSignUpFeatureRefId(String signUpFeatureRefId) {
        this.signUpFeatureRefId = signUpFeatureRefId;
    }

    public String getBlockedReason() {
        return blockedReason;
    }

    public void setBlockedReason(String blockedReason) {
        this.blockedReason = blockedReason;
    }

    public String getBlockedReasonType() {
        return blockedReasonType;
    }

    public void setBlockedReasonType(String blockedReasonType) {
        this.blockedReasonType = blockedReasonType;
    }

    public Long getPasswordChangedAt() {
        return passwordChangedAt;
    }

    public void setPasswordChangedAt(Long passwordChangedAt) {
        this.passwordChangedAt = passwordChangedAt;
    }

    public List<String> getRecentHashedPasswords() {
        if (recentHashedPasswords == null) {
            return new ArrayList<>();
        }
        return recentHashedPasswords;
    }

    public void setRecentHashedPasswords(List<String> recentHashedPasswords) {
        this.recentHashedPasswords = recentHashedPasswords;
    }

    public void addRecentHashedPassword(String recentHashedPassword) {
        if (StringUtils.isEmpty(recentHashedPassword)) {
            return;
        }
        if (com.vedantu.util.ArrayUtils.isEmpty(this.recentHashedPasswords)) {
            this.recentHashedPasswords = new ArrayList<>();
        }
        if (this.recentHashedPasswords.size() >= 2) {
            String[] arr = this.recentHashedPasswords.toArray(new String[0]);
            String[] subArr = Arrays.copyOfRange(arr, 1, arr.length);
            this.recentHashedPasswords = new ArrayList<>(Arrays.asList(subArr));
        }
        this.recentHashedPasswords.add(recentHashedPassword);
    }

    public String getTempPhoneCode() {
        return tempPhoneCode;
    }

    public void setTempPhoneCode(String tempPhoneCode) {
        this.tempPhoneCode = tempPhoneCode;
    }

    public String getReferrer() {
        return referrer;
    }

    public Boolean getOptIn() {
        return optIn;
    }

    public void setOptIn(Boolean optIn) {
        this.optIn = optIn;
    }

    public void setReferrer(String referrer) {
        this.referrer = referrer;
    }

    public Set<SubRole> getSubRoles() {
        Set<SubRole> subRoles = null;
        if (teacherInfo != null) {
            subRoles = teacherInfo.getSubRole();
        }
        return subRoles;
    }

    public static class Constants extends AbstractMongoLongIdEntity.Constants {

        public static final String EMAIL = "email";
        public static final String NAME = "name";
        public static final String FIRST_NAME = "firstName";
        public static final String LAST_NAME = "lastName";
        public static final String FULL_NAME = "fullName";
        public static final String PASSWORD = "password";
        public static final String OLD_PASSWORD = "oldPassword";
        public static final String ROLE = "role";
        public static final String CONTACT_NUMBER = "contactNumber";
        public static final String PHONE_CODE = "phoneCode";
        public static final String IS_CONTACT_NUMBER_DND = "isContactNumberDND";
        public static final String IS_CONTACT_NUMBER_WHITELISTED = "isContactNumberWhitelisted";
        public static final String GENDER = "gender";
        public static final String STUDENT_INFO = "studentInfo";
        public static final String TEACHER_INFO = "teacherInfo";

        public static final String IS_EMAIL_VERIFIED = "isEmailVerified";
        public static final String PROFILE_PIC_ID = "profilePicId";
        public static final String PROFILE_PIC_PATH = "profilePicPath";
        public static final String TNC_VERSION = "tncVersion";
        public static final String REFERRER_ID = "referrerId";

        public static final String SIGNUP_FEATURE = "signUpFeature";
        public static final String SIGNUP_URL = "signUpURL";
        public static final String REFERRAL_CODE = "referralCode";
        public static final String PHONES = "phones";
        public static final String PROFILE_ENABLED = "profileEnabled";
        public static final String UTM_SOURCE = "utm_source";
        public static final String UTM_MEDIUM = "utm_medium";
        public static final String UTM_CAMPAIGN = "utm_campaign";
        public static final String UTM_CONTENT = "utm_content";
        public static final String UTM_TERM = "utm_term";
        public static final String CHANNEL = "channel";
        public static String PARENT_REGISTRATION = "parentRegistration";
        public static String IS_CONTACT_NUMBER_VERIFIED = "isContactNumberVerified";
        public static String SOCIAL_INFO = "socialInfo";
        public static String LOCATION_INFO = "locationInfo";
        public static String LANGUAGE_PREFS = "languagePrefs";
        public static String SUBSCRIPTION_REQUEST_COUNT = "subscriptionRequestCount";
        public static String OTF_SUBSCRIPTION_COUNT = "otfSubscriptionCount";
        public static final String CURRENT_STATUS = "teacherInfo.currentStatus";
        public static final String PRIMARY_SUBJECT = "teacherInfo.primarySubject";
        public static final String PASSWORD_CHANGED_AT = "passwordChangedAt";
        public static final String TELEGRAM_LINKED = "telegramLinked";
        public static final String TEACHER_POOL_TYPE = "teacherPoolType";
        public static final String DEX_SUB_ROLE = "teacherInfo.subRole";
        public static final String OTP_PASSWORD = "otpPassword";
        public static final String DUMMY_NUMBER = "DummyNumber";
        public static final String TARGET = "studentInfo.target";
        public static final String TEACHER_CATEGORY_TYPE = "teacherInfo.teacherCategoryTypes";

        public static final String PENDING_BUNDLE_ID_FOR_ONBOARDING = "pendingBundleIdForOnboarding";
        public static final String COMPLETED_BUNDLE_ID_FOR_ONBOARDING = "completedBundleIdForOnboarding";
        public static final String ORG_ID = "orgId";
    }

    /**
     * @return the telegramLinked
     */
    public boolean isTelegramLinked() {
        return telegramLinked;
    }

    /**
     * @param telegramLinked the telegramLinked to set
     */
    public void setTelegramLinked(boolean telegramLinked) {
        this.telegramLinked = telegramLinked;
    }

    public Long getDob() {
        return dob;
    }

    public void setDob(Long dob) {
        this.dob = dob;
    }

    public Set<String> getPendingBundleIdForOnboarding() {
        return pendingBundleIdForOnboarding;
    }

    public void setPendingBundleIdForOnboarding(Set<String> pendingBundleIdForOnboarding) {
        this.pendingBundleIdForOnboarding = pendingBundleIdForOnboarding;
    }

    public Set<String> getCompletedBundleIdForOnboarding() {
        return completedBundleIdForOnboarding;
    }

    public void setCompletedBundleIdForOnboarding(Set<String> completedBundleIdForOnboarding) {
        this.completedBundleIdForOnboarding = completedBundleIdForOnboarding;
    }
}
