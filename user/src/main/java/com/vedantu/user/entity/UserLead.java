package com.vedantu.user.entity;

import java.util.List;

import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import com.vedantu.User.Gender;
import com.vedantu.User.Role;
import com.vedantu.User.enums.UserLeadsActivityType;
import com.vedantu.lms.cmds.pojo.KeyValueObject;
import com.vedantu.util.dbentities.mongo.AbstractMongoLongIdEntity;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;

@Document(collection = "UserLead")
public class UserLead extends AbstractMongoStringIdEntity {

	@Indexed(background = true)
	private String email;
	@Indexed(background = true)
	private String contactNumber;
	private String phoneCode;
	private String firstName;
	private String lastName;
	private String fullName;
	private Gender gender;
	private Role role;
	private String utm_source;
	private String utm_medium;
	private String utm_campaign;
	private String utm_term;
	private String utm_content;
	private String channel;
	private String ipAddress;
	private String source;
	private String grade;
	private String board;
	private String school;
        private String deviceType;
        private String deviceName;
	private String country;
	private String city;
	private List<KeyValueObject> leadMetadata;
	private UserLeadsActivityType activity;
	private String contextType;
	private String contextId;
	private Boolean joined;
	private List<Long> userIds;

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getContactNumber() {
		return contactNumber;
	}

        public String getDeviceType() {
            return deviceType;
        }

        public void setDeviceType(String deviceType) {
            this.deviceType = deviceType;
        }

        public String getDeviceName() {
            return deviceName;
        }

        public void setDeviceName(String deviceName) {
            this.deviceName = deviceName;
        }

	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}

	public String getPhoneCode() {
		return phoneCode;
	}

	public void setPhoneCode(String phoneCode) {
		this.phoneCode = phoneCode;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public Gender getGender() {
		return gender;
	}

	public void setGender(Gender gender) {
		this.gender = gender;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public String getUtm_source() {
		return utm_source;
	}

	public void setUtm_source(String utm_source) {
		this.utm_source = utm_source;
	}

	public String getUtm_medium() {
		return utm_medium;
	}

	public void setUtm_medium(String utm_medium) {
		this.utm_medium = utm_medium;
	}

	public String getUtm_campaign() {
		return utm_campaign;
	}

	public void setUtm_campaign(String utm_campaign) {
		this.utm_campaign = utm_campaign;
	}

	public String getUtm_term() {
		return utm_term;
	}

	public void setUtm_term(String utm_term) {
		this.utm_term = utm_term;
	}

	public String getUtm_content() {
		return utm_content;
	}

	public void setUtm_content(String utm_content) {
		this.utm_content = utm_content;
	}

	public String getChannel() {
		return channel;
	}

	public void setChannel(String channel) {
		this.channel = channel;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getGrade() {
		return grade;
	}

	public void setGrade(String grade) {
		this.grade = grade;
	}

	public String getBoard() {
		return board;
	}

	public void setBoard(String board) {
		this.board = board;
	}

	public String getSchool() {
		return school;
	}

	public void setSchool(String school) {
		this.school = school;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public List<Long> getUserIds() {
		return userIds;
	}

	public void setUserIds(List<Long> userIds) {
		this.userIds = userIds;
	}

	public List<KeyValueObject> getLeadMetadata() {
		return leadMetadata;
	}

	public void setLeadMetadata(List<KeyValueObject> leadMetadata) {
		this.leadMetadata = leadMetadata;
	}

	public UserLeadsActivityType getActivity() {
		return activity;
	}

	public void setActivity(UserLeadsActivityType activity) {
		this.activity = activity;
	}

	public String getContextType() {
		return contextType;
	}

	public void setContextType(String contextType) {
		this.contextType = contextType;
	}

	public String getContextId() {
		return contextId;
	}

	public void setContextId(String contextId) {
		this.contextId = contextId;
	}

	public Boolean getJoined() {
		return joined;
	}

	public void setJoined(Boolean joined) {
		this.joined = joined;
	}

	public static class Constants extends AbstractMongoStringIdEntity.Constants {

		public static final String EMAIL = "email";
		public static final String NAME = "name";
		public static final String FIRST_NAME = "firstName";
		public static final String LAST_NAME = "lastName";
		public static final String FULL_NAME = "fullName";
		public static final String ROLE = "role";
		public static final String CONTACT_NUMBER = "contactNumber";
		public static final String PHONE_CODE = "phoneCode";
		public static final String GENDER = "gender";
		public static final String UTM_SOURCE = "utm_source";
		public static final String UTM_MEDIUM = "utm_medium";
		public static final String UTM_CAMPAIGN = "utm_campaign";
		public static final String UTM_CONTENT = "utm_content";
		public static final String UTM_TERM = "utm_term";
		public static final String CHANNEL = "channel";
		public static final String SOURCE = "source";
		public static final String IPADDRESS = "ipAddress";
		public static final String CITY = "city";
		public static final String COUNTRY = "country";
	}
}
