package com.vedantu.user.listeners;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.vedantu.exception.ConflictException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.NotFoundException;
import com.vedantu.exception.VRuntimeException;
import com.vedantu.user.entity.User;
import com.vedantu.user.managers.UserManager;
import com.vedantu.user.managers.UserReferralManager;
import com.vedantu.user.managers.login.UserLoginManager;
import com.vedantu.user.managers.signup.UserSignUpManager;
import com.vedantu.user.pojo.UserCityUpdate;
import com.vedantu.user.pojo.UserPreLoginDataPojo;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StatsdClient;
import com.vedantu.util.enums.SQSMessageType;
import com.vedantu.util.enums.SQSQueue;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.Queue;
import javax.jms.TextMessage;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;
import org.springframework.stereotype.Service;

@Service
public class SQSListener implements MessageListener {

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(SQSListener.class);

    @Autowired
    private StatsdClient statsdClient;

    @Autowired
    private UserManager userManager;

    @Autowired
    private UserSignUpManager userSignUpManager;

    private String env = ConfigUtils.INSTANCE.getStringValue("environment");

    @Autowired
    private UserLoginManager userLoginManager;

    @Autowired
    private UserReferralManager userReferralManager;

    @Override
    public void onMessage(Message message) {
        TextMessage textMessage = (TextMessage) message;

        try {
            logger.info("MessageId: " + textMessage.getJMSMessageID());
            logger.info("Received message " + textMessage.getText());
            String queueName = ((Queue)textMessage.getJMSDestination()).getQueueName();
            logger.info("queueName "+queueName);
            Long startTime = System.currentTimeMillis();
            SQSMessageType sqsMessageType = SQSMessageType.valueOf(textMessage.getStringProperty("messageType"));
            handleMessage(queueName, sqsMessageType, textMessage.getText());
            message.acknowledge();
            if ("PROD".equals(ConfigUtils.INSTANCE.getStringValue("environment").toUpperCase())) {
                String className = ConfigUtils.INSTANCE.properties.getProperty("application.name") + this.getClass().getSimpleName();
                if(null != sqsMessageType) {
                    statsdClient.recordCount(className, sqsMessageType.name(), "apicount");
                    statsdClient.recordExecutionTime(System.currentTimeMillis() - startTime, className, sqsMessageType.name());
                }
            }

        } catch (Exception e) {
            logger.error("Error processing message ", e);
        }
    }

    public void handleMessage(String queueName, SQSMessageType sqsMessageType, String text) throws Exception {

        if(SQSQueue.USER_CITY_UPDATE_QUEUE.getQueueName(env).equals(queueName)) {
            handleUserCityUpdate(sqsMessageType, text);
        } else if (SQSQueue.SIGNUP_TASKS_QUEUE.getQueueName(env).equals(queueName)) {
            handleUserSignUpTasks(sqsMessageType, text);
        }
        else if(SQSQueue.PRE_LOGIN_DATA_QUEUE.getQueueName(env).equals(queueName)){
            handlePreLoginData(sqsMessageType, text);
        }
        else {
            throw new VRuntimeException(ErrorCode.BAD_REQUEST_ERROR, queueName + " can't process this queue here");
        }
        logger.info("Message handled");
    }

    private void handlePreLoginData(SQSMessageType sqsMessageType, String text) {
        logger.info("got the prelogin data message from sqs with text : " + text + " sqsMessageType : " + sqsMessageType);
        UserPreLoginDataPojo data = new Gson().fromJson(text, UserPreLoginDataPojo.class);
        userLoginManager.savePreLoginData(data);
    }

    private void handleUserCityUpdate(SQSMessageType sqsMessageType, String text) throws ConflictException {
        Type _type = new TypeToken<UserCityUpdate>() {
        }.getType();
        UserCityUpdate payload  = new Gson().fromJson(text, UserCityUpdate.class);
        User user1 = payload.getUser();
        String ip = payload.getIp();
        Long callingUser = payload.getCallingUserId();
        userManager.updateUserLocationFrommIp(user1, ip, callingUser);
    }

    private void handleUserSignUpTasks(SQSMessageType sqsMessageType, String text) throws ConflictException, NotFoundException {
        logger.info("got the singup tasks message from sqs with text : " + text + " sqsMessageType : " + sqsMessageType);
        switch (sqsMessageType) {
            case SIGNUP_TASKS_REQ:
                User user = new Gson().fromJson(text, User.class);
                userSignUpManager.signUpTasksFromQueue(user);
                break;
            case SIGNUP_TASKS_REFERRAL_REQ:
                Type filterType = new TypeToken<Map<String, String>>() {
                }.getType();
                Map<String, String> payload = new Gson().fromJson(text, filterType);
                userReferralManager.singUpReferralFromQueue(payload);
                break;
            default:
                logger.error("sqs message type not found in SIGNUP_TASKS_QUEUE");
        }

    }
}
