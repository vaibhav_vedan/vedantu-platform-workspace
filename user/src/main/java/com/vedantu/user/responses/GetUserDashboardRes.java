/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.user.responses;

import com.vedantu.User.UserInfo;
import com.vedantu.dinero.response.GetUserDashboardAccountInfoRes;
import com.vedantu.scheduling.response.session.GetUserDashboardSessionInfoRes;
import com.vedantu.util.fos.response.AbstractRes;

/**
 *
 * @author jeet
 */
public class GetUserDashboardRes extends AbstractRes{
    private UserInfo userInfo;
    private GetUserDashboardAccountInfoRes accountInfo;
    private GetUserDashboardSessionInfoRes sessionInfo;

    public UserInfo getUserInfo() {
        return userInfo;
    }

    public void setUserInfo(UserInfo userInfo) {
        this.userInfo = userInfo;
    }

    public GetUserDashboardAccountInfoRes getAccountInfo() {
        return accountInfo;
    }

    public void setAccountInfo(GetUserDashboardAccountInfoRes accountInfo) {
        this.accountInfo = accountInfo;
    }

    public GetUserDashboardSessionInfoRes getSessionInfo() {
        return sessionInfo;
    }

    public void setSessionInfo(GetUserDashboardSessionInfoRes sessionInfo) {
        this.sessionInfo = sessionInfo;
    }
    
}
