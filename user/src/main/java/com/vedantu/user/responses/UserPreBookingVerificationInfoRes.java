package com.vedantu.user.responses;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserPreBookingVerificationInfoRes {

    private Boolean emailExists = false;
    private Boolean phoneNumberExists = false;
    private Boolean isContactEmailLinked = false;
    private Boolean userHasEmail = false;
    private Boolean userHasVerifiedPhoneNumber = false;

}
