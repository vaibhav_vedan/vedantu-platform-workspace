/**
 * 
 */
package com.vedantu.user.responses;

import java.util.List;

import com.vedantu.user.pojo.GPSLocalePojo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * @author vedantu
 *
 */

@Data
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class GpsLocationResp {
	
	private String userId;
	
	private Boolean retryRequired;
	
	private GPSLocalePojo latestLocale;
	
	private List<GPSLocalePojo> historicalLocales;
	
}
