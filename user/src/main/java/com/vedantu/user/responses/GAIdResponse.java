package com.vedantu.user.responses;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class GAIdResponse
{
    private boolean isAdIdRegistered;
    private boolean isFirstTimeInstall;
    private Long creationTime;
}
