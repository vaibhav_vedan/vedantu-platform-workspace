package com.vedantu.user.responses;

import com.vedantu.User.Role;
import com.vedantu.User.User;
import com.vedantu.User.UserUtils;
import com.vedantu.util.StringUtils;

public class SignUpUserRes {
    private User user;
    private Long userId;
    private String email;
    private String firstName;
    private String lastName;
    private String fullName;
    private Role role;
    private Long creationTime;
    private String contactNumber;
    private String phoneCode;
    private Boolean appliedCoupon;

    private Boolean isContactNumberVerified;
    private Boolean isEmailVerified;


    public Boolean getIsContactNumberVerified() {
        return isContactNumberVerified;
    }

    public void setIsContactNumberVerified(Boolean contactNumberVerified) {
        isContactNumberVerified = contactNumberVerified;
    }

    public Boolean getIsEmailVerified() {
        return isEmailVerified;
    }

    public void setIsEmailVerified(Boolean emailVerified) {
        isEmailVerified = emailVerified;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public SignUpUserRes(User user){
        if(null == user) {
            return;
        }
        this.userId = user.getId();
        this.email = user.getEmail();
        this.firstName = user.getFirstName();
        this.lastName = user.getLastName();
        this.fullName = user.getFullName();
        this.role = user.getRole();
        this.creationTime = user.getCreationTime();
        this.contactNumber = user.getContactNumber();
        this.phoneCode = user.getPhoneCode();
        this.isContactNumberVerified = user.getIsContactNumberVerified();
        this.isEmailVerified = user.getIsEmailVerified();
    }

    public Boolean getAppliedCoupon() {
        return appliedCoupon;
    }

    public void setAppliedCoupon(Boolean appliedCoupon) {
        this.appliedCoupon = appliedCoupon;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public Long getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(Long creationTime) {
        this.creationTime = creationTime;
    }

    public String getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    public String getPhoneCode() {
        return phoneCode;
    }

    public void setPhoneCode(String phoneCode) {
        this.phoneCode = phoneCode;
    }
}
