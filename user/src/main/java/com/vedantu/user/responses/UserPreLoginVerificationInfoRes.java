package com.vedantu.user.responses;

public class UserPreLoginVerificationInfoRes {

    private Boolean emailExists = false;
    private Boolean hasPassword = false;
    private Boolean isContactExist = false;
    private Boolean isPhoneVerified = false;
    private Boolean isEmailVerified = false;
    private Boolean isprofileCompleted = false;
    private Boolean multipleUsersExists = false;

    private String email;
    private String phone;

    private Boolean emailSent = true;
    private Boolean smsSent = true;

    public Boolean getEmailSent() {
        return emailSent;
    }

    public void setEmailSent(Boolean emailSent) {
        this.emailSent = emailSent;
    }

    public Boolean getSmsSent() {
        return smsSent;
    }

    public void setSmsSent(Boolean smsSent) {
        this.smsSent = smsSent;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Boolean getMultipleUsersExists() {
        return multipleUsersExists;
    }

    public void setMultipleUsersExists(Boolean multipleUsersExists) {
        this.multipleUsersExists = multipleUsersExists;
    }

    public Boolean getEmailExists() {
        return emailExists;
    }

    public void setEmailExists(Boolean emailExists) {
        this.emailExists = emailExists;
    }

    public Boolean getHasPassword() {
        return hasPassword;
    }

    public void setHasPassword(Boolean hasPassword) {
        this.hasPassword = hasPassword;
    }

    public Boolean getContactExist() {
        return isContactExist;
    }

    public void setContactExist(Boolean contactExist) {
        isContactExist = contactExist;
    }

    public Boolean getPhoneVerified() {
        return isPhoneVerified;
    }

    public void setPhoneVerified(Boolean phoneVerified) {
        isPhoneVerified = phoneVerified;
    }

    public Boolean getIsprofileCompleted() {
        return isprofileCompleted;
    }

    public void setIsprofileCompleted(Boolean isprofileCompleted) {
        this.isprofileCompleted = isprofileCompleted;
    }

    public Boolean getEmailVerified() {
        return isEmailVerified;
    }

    public void setEmailVerified(Boolean emailVerified) {
        isEmailVerified = emailVerified;
    }
}
