package com.vedantu.user.responses;

import com.vedantu.User.User;
import com.vedantu.util.security.HttpSessionData;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author Aditya Rathi
 */


@AllArgsConstructor
@NoArgsConstructor
@Data
public class TruecallerLoginRes {
    private User user;
    private HttpSessionData userDetails;
    private boolean signatureVerified;
}
