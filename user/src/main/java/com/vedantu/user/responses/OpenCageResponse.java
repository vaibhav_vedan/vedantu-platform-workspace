package com.vedantu.user.responses;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class OpenCageResponse {
	
	private String userId;
	
	private String latitude;
	
	private String longitude;

	private String streetAddress;
	
	private String city;
	
	private String state;
	
	private String country;
	
	private String pincode;
}
