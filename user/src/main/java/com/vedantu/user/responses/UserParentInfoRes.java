package com.vedantu.user.responses;

import com.vedantu.User.StudentInfo;

public class UserParentInfoRes {
    private Long id;
    private String email;
    private String firstName;
    private StudentInfo studentInfo;

    public UserParentInfoRes(Long id, String email) {
        this.id = id;
        this.email = email;
    }

    public UserParentInfoRes(Long id, String email, String firstName) {
        this.id = id;
        this.email = email;
        this.firstName = firstName;
    }

    public UserParentInfoRes(Long id, String email, String firstName, StudentInfo studentInfo) {
        this.id = id;
        this.email = email;
        this.firstName = firstName;
        this.studentInfo = studentInfo;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public StudentInfo getStudentInfo() {
        return studentInfo;
    }

    public void setStudentInfo(StudentInfo studentInfo) {
        this.studentInfo = studentInfo;
    }

    @Override
    public String toString() {
        return "UserParentInfoRes [email=" + email + ", firstName=" + firstName + ", id=" + id + ", studentInfo="
                + studentInfo + "]";
    }
}