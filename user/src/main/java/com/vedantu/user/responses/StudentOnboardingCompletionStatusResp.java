package com.vedantu.user.responses;

import com.vedantu.user.enums.OnboardingCompletionStatus;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@ToString
@AllArgsConstructor
@Builder
public class StudentOnboardingCompletionStatusResp {
	
	private String userId;
	private OnboardingCompletionStatus status;
	

}
