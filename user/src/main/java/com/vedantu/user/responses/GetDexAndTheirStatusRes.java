/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.user.responses;

import com.vedantu.User.enums.DexAvailabilityStatus;
import com.vedantu.User.enums.TeacherPoolType;

/**
 *
 * @author ajith
 */
public class GetDexAndTheirStatusRes {

    private Long userId;
    private DexAvailabilityStatus dexAvailabilityStatus;
    private TeacherPoolType teacherPoolType;

    public GetDexAndTheirStatusRes() {
    }

    public GetDexAndTheirStatusRes(Long userId, DexAvailabilityStatus dexAvailabilityStatus, TeacherPoolType teacherPoolType) {
        this.userId = userId;
        this.dexAvailabilityStatus = dexAvailabilityStatus;
        this.teacherPoolType = teacherPoolType;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public DexAvailabilityStatus getDexAvailabilityStatus() {
        return dexAvailabilityStatus;
    }

    public void setDexAvailabilityStatus(DexAvailabilityStatus dexAvailabilityStatus) {
        this.dexAvailabilityStatus = dexAvailabilityStatus;
    }

    public TeacherPoolType getTeacherPoolType() {
        return teacherPoolType;
    }

    public void setTeacherPoolType(TeacherPoolType teacherPoolType) {
        this.teacherPoolType = teacherPoolType;
    }

    @Override
    public String toString() {
        return "GetDexAndTheirStatusRes{" + "userId=" + userId + ", dexAvailabilityStatus=" + dexAvailabilityStatus + ", teacherPoolType=" + teacherPoolType + '}';
    }

}
