package com.vedantu.user.responses;

public class GetTestAttemptRes {
    private boolean attempted;
    private String testId;

    public boolean isAttempted() {
        return attempted;
    }

    public void setAttempted(boolean attempted) {
        this.attempted = attempted;
    }

    public String getTestId() {
        return testId;
    }

    public void setTestId(String testId) {
        this.testId = testId;
    }

    @Override
    public String toString() {
        return "GetTestAttemptRes{" +
                "attempted=" + attempted +
                ", testId='" + testId + '\'' +
                '}';
    }
}
