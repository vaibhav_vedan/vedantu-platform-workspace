package com.vedantu.user.responses;

import com.vedantu.User.User;

public class ProfileBuilderPostLoginRes {
    private User user;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
