package com.vedantu.user.responses;

public class NewPhoneVerificationRes {

    private String linkedEmail;
    private Boolean phoneNumberExists;

    public String getLinkedEmail() {
        return linkedEmail;
    }

    public void setLinkedEmail(String linkedEmail) {
        this.linkedEmail = linkedEmail;
    }

    public Boolean getPhoneNumberExists() {
        return phoneNumberExists;
    }

    public void setPhoneNumberExists(Boolean phoneNumberExists) {
        this.phoneNumberExists = phoneNumberExists;
    }
}
