package com.vedantu.user.responses;

import com.vedantu.User.ParentInfo;
import com.vedantu.User.StudentInfo;
import com.vedantu.user.entity.StudentOnboarding;
import com.vedantu.user.entity.User;
import com.vedantu.user.enums.OnboardingCompletionStatus;
import com.vedantu.user.enums.OnboardingStep;
import com.vedantu.user.pojo.onboarding.EducationalDetails;
import com.vedantu.user.pojo.onboarding.PageBasicDetials;
import com.vedantu.user.pojo.onboarding.ProfileCreation;
import com.vedantu.user.pojo.onboarding.StudentDetails;
import com.vedantu.util.ArrayUtils;
import lombok.Data;

import java.util.*;
import java.util.stream.Stream;

@Data
public class StudentOnboardingResponse extends StudentOnboarding {
    private OnboardingStep currentOnboardingStep;
    private OnboardingStep previousOnboardingStep;
    public StudentOnboardingResponse(StudentOnboarding studentOnboarding){
        super(studentOnboarding);
        this.previousOnboardingStep=getCurrentOnboardingStep(studentOnboarding);
        this.currentOnboardingStep=getNextOnboardingStep(previousOnboardingStep);

    }

    private OnboardingStep getNextOnboardingStep(OnboardingStep currentOnboardingStep) {
        if(Objects.isNull(currentOnboardingStep)){
            return OnboardingStep.INTRODUCTION;
        }
        if(OnboardingStep.INTRODUCTION.equals(currentOnboardingStep)){
            return OnboardingStep.PROFILE_CREATION;
        }
        /*if(OnboardingStep.COMPATIBILITY_CHECK.equals(currentOnboardingStep)){
            return OnboardingStep.EXPERICENCE_PLATFORM;
        }
        if(OnboardingStep.EXPERICENCE_PLATFORM.equals(currentOnboardingStep)){
            return OnboardingStep.PROFILE_CREATION;
        }*/
        if(OnboardingStep.PROFILE_CREATION.equals(currentOnboardingStep)){
            return OnboardingStep.COURSE_ENROLL;
        }
        if(OnboardingStep.COURSE_ENROLL.equals(currentOnboardingStep)){
            return OnboardingStep.COURSE_ENROLL;
        }
        /*if(OnboardingStep.COMPLETED.equals(currentOnboardingStep)){
            return OnboardingStep.COMPLETED;
        }*/
        return OnboardingStep.NONE;
    }

    public void setDefaultProfileInformation(User user) {
        ProfileCreation userProfile = super.getProfileCreation();
        userProfile.setStudentDetails(
                new StudentDetails(
                        user.getFirstName(),
                        user.getLastName(),
                        user.getEmail(),
                        user.getPhones(),
                        user.getDob(),
                        user.getLanguagePrefs(),
                        user.getGender()
                )
        );

        StudentInfo studentInfo = user.getStudentInfo();
        if(Objects.nonNull(studentInfo)) {

            List<ParentInfo> parentInfoList = studentInfo.getParentInfos();
            if(ArrayUtils.isNotEmpty(parentInfoList)){
                userProfile.setParentDetails(parentInfoList);
            }

            /*userProfile.setEducationalDetails(
                    new EducationalDetails(
                            studentInfo.getBoard(),
                            studentInfo.getGrade(),
                            studentInfo.getPreviousGradeScore(),
                            studentInfo.getSchool(),
                            studentInfo.getExamTargets(),
                            studentInfo.getPreferredTimeForLiveClasses()
                    )
            );*/
        }

        // userProfile.setLocationInfo(user.getLocationInfo());

    }


    private OnboardingStep getCurrentOnboardingStep(StudentOnboarding studentOnboarding) {
        List<PageBasicDetials> allOnboardingSteps= Arrays.asList(
                studentOnboarding.getIntroduction(),
                // studentOnboarding.getCompatibilityCheck(),
                // studentOnboarding.getExperiencePlatform(),
                studentOnboarding.getProfileCreation(),
                studentOnboarding.getCourseEnroll()
                // studentOnboarding.getCompleted()
        );
        return Optional.ofNullable(allOnboardingSteps)
                .map(Collection::stream)
                .orElseGet(Stream::empty)
                .filter(this::isCompletedPageBasicDetails)
                .sorted(this::sortCriteriaAccordingToStepNo)
                .map(PageBasicDetials::getOnboardingStepType)
                .findFirst()
                .orElse(null);
    }

    private int sortCriteriaAccordingToStepNo(PageBasicDetials o1,PageBasicDetials o2) {
        return Integer.compare(o2.getStepNo(),o1.getStepNo());
    }

    private boolean isCompletedPageBasicDetails(PageBasicDetials pageBasicDetials) {
        return Objects.nonNull(pageBasicDetials) && OnboardingCompletionStatus.COMPLETED.equals(pageBasicDetials.getStepCompletionStatus());
    }
}
