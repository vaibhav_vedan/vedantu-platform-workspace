package com.vedantu.user.responses;

public class UserPreSignUpVerificationInfoRes {

    private boolean emailUnique = false;
    private boolean phoneUnique = false;

    public boolean isEmailUnique() {
        return emailUnique;
    }

    public void setEmailUnique(boolean emailUnique) {
        this.emailUnique = emailUnique;
    }

    public boolean isPhoneUnique() {
        return phoneUnique;
    }

    public void setPhoneUnique(boolean phoneUnique) {
        this.phoneUnique = phoneUnique;
    }
}
