package com.vedantu.user.helper;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.User.LocationInfo;
import com.vedantu.User.request.SignInReq;
import com.vedantu.exception.VExceptionFactory;
import com.vedantu.user.dao.RedisDAO;
import com.vedantu.user.dao.UserIspDataDao;
import com.vedantu.user.entity.User;
import com.vedantu.user.entity.temp.ISPData;
import com.vedantu.user.managers.UserManager;
import com.vedantu.util.*;
import com.vedantu.util.redis.RateLimitingRedisDao;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

@Service
public class BotFilter {

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(BotFilter.class);

    private static List<String> allowedCountriesList = new ArrayList(Arrays.asList("India", "Saudi Arabia", "singapore", "United Arab Emirates", "Kuwait", "Oman", "Qatar", "Bahrain",  "Nigeria", "New Zealand", "Kazakhstan"));

    private static List<String> blockedIspList = new ArrayList(Arrays.asList("OVH Hosting"));
    @Autowired
    private RateLimitingRedisDao rateLimitRedisDAO;

    @Autowired
    private UserIspDataDao ispDataDao;

    @Autowired
    private IPUtil ipUtil;

    @Autowired
    private UserManager userManager;

    @Autowired
    private RedisDAO redisDAO;

    public void getAllowedCountriesList() {
        List<String> allowedCountriesListFromRedis;

        String redisKey = "ALLOWED_COUNTRIES_LIST";
        try {
            String redisCountriesString = rateLimitRedisDAO.get(redisKey);
            String[] redisCountriesArr = redisCountriesString.split(",");
            allowedCountriesListFromRedis = new ArrayList(Arrays.asList(redisCountriesArr));
            allowedCountriesList = allowedCountriesListFromRedis;
        } catch (Exception e) {
            logger.error("Error in getting key from redis " + redisKey);
        }
    }

    public void getBlockedIspList() {
        List<String> blockedIspListFromRedis;

        String redisKey = "BLOCKED_ISP_LIST";
        try {
            String redisCountriesString = rateLimitRedisDAO.get(redisKey);
            String[] redisCountriesArr = redisCountriesString.split(",");
            blockedIspListFromRedis = new ArrayList(Arrays.asList(redisCountriesArr));
            blockedIspList = blockedIspListFromRedis;
        } catch (Exception e) {
            logger.error("Error in getting key from redis " + redisKey);
        }
    }

    public void blockIpAddress(String ipAddress, String email) {
        getAllowedCountriesList();
        getBlockedIspList();
        if(StringUtils.isNotEmpty(ipAddress)) {
            LocationInfo locationFromIp = ipUtil.getLocationFromIpForBotFilter(ipAddress);
            String countryFromIp = null;
            String isp = null;
            if (locationFromIp != null) {
                countryFromIp = locationFromIp.getCountry();
                isp = locationFromIp.getIsp();
            }
            boolean blockIp = true;
            for (String country : allowedCountriesList) {
                if (StringUtils.isNotEmpty(countryFromIp) && country.equalsIgnoreCase(countryFromIp)) {
                    blockIp = false;
                    break;
                }
            }
            if (StringUtils.isEmpty(countryFromIp)) {
                blockIp = false;
            }

            if (null != locationFromIp && "ANONYMOUS_PROXY_OR_VPN".equalsIgnoreCase(locationFromIp.getQueryStatusCode())) {
                blockIp = true;
            }

            for (String ispName : blockedIspList) {
               if(StringUtils.isNotEmpty(isp) && ispName.equalsIgnoreCase(isp)){
                   blockIp = true;
                   break;
               }
            }

            if (blockIp) {
                String key = "RL:" + ipAddress + ":" + "/user/login/authenticateUser";
                int blockTime = Integer.parseInt(ConfigUtils.INSTANCE.getStringValue("rl.blockTime"));
                blockTime = 864000;
                try {
                    User user = null;
                    if(CustomValidator.validEmail(email)){
                        user = userManager.getUserByEmail(email);
                    }
                    // 1 jan 2018
                    if (user == null || user.getCreationTime() < 1514745000000L) {
                        rateLimitRedisDAO.setex("BL:" + key, "", blockTime);
                        logger.warn("USER BLOCKED: " + key + "Requests Blocked from: " + countryFromIp);
                    }
                }catch (Exception e){
                    logger.error("Error in adding value to set: " + " to key: " + key + ",  exception: " + e.getMessage());
                }
            }
        }
    }

    // if no isp data in last n min, send otp. if req isp already present in last n min allow otp.
    public Boolean sendPreLoginOTP(String loginParam, String isp) {
        Integer threshold = 15;

        try {
            String key = "TIME_CHECK_ISP_COUNT_MIN";
            String timeCheckISP = redisDAO.get(key);
            if(StringUtils.isNotEmpty(timeCheckISP)){
                threshold = Integer.parseInt(timeCheckISP);
            }
        }catch(Exception e){
            logger.info("Error in fetching data from redis" + e.getMessage());
        }
        Integer count = ispDataDao.getCountForISP(isp);
        if(count > 0 && count < threshold){
            return false;
        }
        List<ISPData> ispDataList = ispDataDao.getISPData(loginParam);
        getBlockedIspList();
        if(StringUtils.isNotEmpty(isp) && blockedIspList.contains(isp)){
            return false;
        }

        if(CollectionUtils.isEmpty(ispDataList)){
            return true;
        }
        for(ISPData data : ispDataList){
            if(isp.equalsIgnoreCase(data.getIsp())){
                return true;
            }
        }
        return false;
    }

    public boolean verifyGoogleRecaptcha(String token, String ipAddress) {
        boolean allowLogin = false;
        try {
            String url = ConfigUtils.INSTANCE.getStringValue("google.recaptcha.verify");
            url = url + "&response=" + token + "&remoteip=" + ipAddress;
            ClientResponse resp = WebUtils.INSTANCE.doCall(url, HttpMethod.GET, null, true);
            VExceptionFactory.INSTANCE.parseAndThrowException(resp);
            String respString = resp.getEntity(String.class);
            Type _type = new TypeToken<HashMap<String, String>>() {
            }.getType();
            HashMap<String, String> respMap = new Gson().fromJson(respString, _type);
            if (respMap.containsKey("success")) {
                if ("true".equals(respMap.get("success"))) {
                    allowLogin = true;
                }
            }
        } catch (Exception e) {

        }
        return allowLogin;
    }
}
