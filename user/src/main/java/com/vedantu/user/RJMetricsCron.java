package com.vedantu.user;

import com.vedantu.exception.VException;
import com.vedantu.user.dao.UserDAO;
import com.vedantu.user.dao.UserDetailsDAO;
import com.vedantu.user.entity.User;
import com.vedantu.user.entity.UserDetails;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.DateTimeUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.PlatformBasicResponse;
import com.vedantu.util.rjmetrics.RJMetricsExportManager;
import com.vedantu.util.rjmetrics.pojos.UserRjPojo;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.dozer.DozerBeanMapper;

/**
 * Created by somil on 21/02/17.
 */
@RestController
@RequestMapping("/export/rjMetrics")
public class RJMetricsCron {

    @Autowired
    private LogFactory logFactory;

    @Autowired
    private RJMetricsExportManager rJMetricsExportManager;

    @Autowired
    UserDAO userDAO;

    @Autowired
    UserDetailsDAO userDetailsDAO;

    private Logger logger = logFactory.getLogger(RJMetricsCron.class);

    private String RJMETRICS_BASE_URL;

    public RJMetricsCron() {
        RJMETRICS_BASE_URL = "https://connect.rjmetrics.com/v2/client/{{clientId}}/table/{{table}}/data?apikey={{apiKey}}";
        RJMETRICS_BASE_URL = RJMETRICS_BASE_URL.replace("{{clientId}}",
                ConfigUtils.INSTANCE.getStringValue("rjmetrics.client.id"));
        RJMETRICS_BASE_URL = RJMETRICS_BASE_URL.replace("{{apiKey}}",
                ConfigUtils.INSTANCE.getStringValue("rjmetrics.client.apiKey"));
    }

    @Autowired
    private DozerBeanMapper mapper;

    @RequestMapping(value = "/updateRJMetrics", method = RequestMethod.GET)
    @ResponseBody
    public void updateRJMetrics(@RequestParam(value = "fromTime", required = false) Long fromTime, @RequestParam(value = "tillTime", required = false) Long tillTime) throws VException {
        try {
            postUserData(fromTime, tillTime);
        } catch (Exception ex) {
            logger.error("Error in updating RJ : postUserData", ex);
        }

        try {
            postUserDetails(fromTime, tillTime);
        } catch (Exception ex) {
            logger.error("Error in updating RJ : postUserDetails", ex);
        }
    }

    private void postUserData(Long fromTime, Long tillTime) {
        Integer limit = 1000;
        List<User> entities = userDAO.getEntitiesByUpdationTimeAndLimit(fromTime, tillTime, limit, User.class);
        if (entities != null) {
            List<UserRjPojo> rjEntities = new ArrayList<>();
            for (User user : entities) {
                try {
                    UserRjPojo rjPojo = mapper.map(user, UserRjPojo.class);
                    rjEntities.add(rjPojo);
                } catch (Exception ex) {
                    logger.error("Error in updating RJ entry: ", ex);
                }
            }
            rJMetricsExportManager.postData(new ArrayList<>(rjEntities), RJMETRICS_BASE_URL.replace("{{table}}", User.class.getSimpleName()));
        }
    }

    private void postUserDetails(Long fromTime, Long tillTime) {
        Integer limit = 1000;
        List<UserDetails> entities = userDetailsDAO.getEntitiesByUpdationTimeAndLimit(fromTime, tillTime, limit, UserDetails.class);
        if (entities != null) {
            rJMetricsExportManager.postData(new ArrayList<>(entities), RJMETRICS_BASE_URL.replace("{{table}}", UserDetails.class.getSimpleName()));
        }
    }

    @RequestMapping(value = "/updateMigUsers", method = RequestMethod.GET)
    @ResponseBody
    public PlatformBasicResponse updateMigUsers(@RequestParam(value = "startTime", required = true) Long startTime) throws VException {

        try {
            Long currentTime = System.currentTimeMillis();
//            Long startTime = 1489104000000L;

            while (startTime < currentTime) {
                Long tillTime = startTime + DateTimeUtils.MILLIS_PER_HOUR * 2;
                logger.info("====================");
                logger.info("fromTime: " + new Date(startTime) + " tillTime: " + new Date(tillTime));
                List<User> entities = userDAO.getEntitiesByUpdationTimeAndLimit(startTime, tillTime, 1000, User.class);
                if (entities != null) {
                    List<UserRjPojo> rjEntities = new ArrayList<>();
                    for (User user : entities) {
                        try {
                            UserRjPojo rjPojo = mapper.map(user, UserRjPojo.class);
                            rjEntities.add(rjPojo);
                        } catch (Exception ex) {
                            logger.error("Error in updating RJ entry: ", ex);
                        }
                    }
                    rJMetricsExportManager.postData(new ArrayList<>(rjEntities), RJMETRICS_BASE_URL.replace("{{table}}", User.class.getSimpleName()));
                }
                startTime = tillTime;
            }
        } catch (Exception ex) {
            logger.error("Error in updating RJ : updateMigUsers", ex);
        }
        return new PlatformBasicResponse();
    }
}
