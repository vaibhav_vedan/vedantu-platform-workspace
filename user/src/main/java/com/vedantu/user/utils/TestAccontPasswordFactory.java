package com.vedantu.user.utils;

import com.vedantu.util.ConfigUtils;

public class TestAccontPasswordFactory {

    //Test phone numbers
    private static final String TEST_PHONE_NUMBER_ALLOW_DUPLICATE_SIGNUP = "1234512345";
    private static final String TEST_PHONE_NUMBER_ALLOW_DUPLICATE_LOGIN = "1234512346";
    private static final String TEST_PHONE_NUMBER_ALLOW_DUPLICATE_LOGIN_INTERNAL = "1234512347";

    //Test account default phone otp
    private static final String TEST_PHONE_NUMBER_ALLOW_DUPLICATE_SIGNUP_OTP = "4545";
    private static final String TEST_PHONE_NUMBER_ALLOW_DUPLICATE_LOGIN_OTP = "4546";
    private static final String TEST_PHONE_NUMBER_ALLOW_DUPLICATE_LOGIN_INTERNAL_OTP = "4547";

    public static String getTestDefaultOTP(String contactNumber) {

        switch (contactNumber) {
            case TEST_PHONE_NUMBER_ALLOW_DUPLICATE_SIGNUP:
                return TEST_PHONE_NUMBER_ALLOW_DUPLICATE_SIGNUP_OTP;
            case TEST_PHONE_NUMBER_ALLOW_DUPLICATE_LOGIN:
                return TEST_PHONE_NUMBER_ALLOW_DUPLICATE_LOGIN_OTP;
            case TEST_PHONE_NUMBER_ALLOW_DUPLICATE_LOGIN_INTERNAL:
                return TEST_PHONE_NUMBER_ALLOW_DUPLICATE_LOGIN_INTERNAL_OTP;
            default:
                return "";
        }

    }

    public static String getTestContactNumber(String type) {

        switch (type) {
            case Constants.TEST_SIGNUP_NUMBER:
                return TEST_PHONE_NUMBER_ALLOW_DUPLICATE_SIGNUP;
            case Constants.TEST_LOGIN_NUMBER:
                return TEST_PHONE_NUMBER_ALLOW_DUPLICATE_LOGIN;
            case Constants.TEST_LOGIN_INTERNAL_NUMBER:
                return TEST_PHONE_NUMBER_ALLOW_DUPLICATE_LOGIN_INTERNAL;
            default:
                return "";
        }
    }

    public static boolean isTestSignupAccount(String contactNumber) {
        switch (contactNumber) {
            case TEST_PHONE_NUMBER_ALLOW_DUPLICATE_SIGNUP:
            case TEST_PHONE_NUMBER_ALLOW_DUPLICATE_LOGIN_INTERNAL:
                return true;
            default:
                return false;
        }
    }

    public static boolean isTestAccount(String contactNumber) {
        if (contactNumber == null) {
            return false;
        }
        switch (contactNumber) {
            case TEST_PHONE_NUMBER_ALLOW_DUPLICATE_SIGNUP:
            case TEST_PHONE_NUMBER_ALLOW_DUPLICATE_LOGIN:
            case TEST_PHONE_NUMBER_ALLOW_DUPLICATE_LOGIN_INTERNAL:
                return true;
            default:
                return false;
        }
    }

    public static boolean canLoginThroughOtp(String contactNumber) {
        switch (contactNumber) {
            case TEST_PHONE_NUMBER_ALLOW_DUPLICATE_LOGIN_INTERNAL:
                return false;
            default:
                return true;
        }
    }

    public class Constants {

        //Test account type
        public static final String TEST_SIGNUP_NUMBER = "TEST_SIGNUP_NUMBER";
        public static final String TEST_LOGIN_NUMBER = "TEST_LOGIN_NUMBER";
        public static final String TEST_LOGIN_INTERNAL_NUMBER = "TEST_LOGIN_INTERNAL_NUMBER";
    }
}
