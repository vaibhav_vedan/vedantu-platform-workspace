/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.user.utils;

import com.vedantu.user.entity.PhoneNumber;
import com.vedantu.user.entity.User;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.security.HttpSessionData;

import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author ajith
 */
@Service
public class PojoUtils {

    @Autowired
    private DozerBeanMapper mapper;
    
    public HttpSessionData convertToUserSessionPojo(User user) {
    	if (user == null) {
            return null;
        }
    	HttpSessionData _user = mapper.map(user, HttpSessionData.class);
    	_user.setUserInProcessOfOnboarding(ArrayUtils.isNotEmpty(user.getPendingBundleIdForOnboarding()));
        return _user;
    }

    public com.vedantu.User.User convertToUserPojo(User user) {
        if (user == null) {
            return null;
        }
        com.vedantu.User.User _user = mapper.map(user, com.vedantu.User.User.class);
        _user.setUserInProcessOfOnboarding(ArrayUtils.isNotEmpty(user.getPendingBundleIdForOnboarding()));
        return _user;
    }

    public com.vedantu.User.PhoneNumber convertToPhonePojo(PhoneNumber phone) {
        if (phone == null) {
            return null;
        }
        com.vedantu.User.PhoneNumber _phone = mapper.map(phone, com.vedantu.User.PhoneNumber.class);
        return _phone;
    }
}
