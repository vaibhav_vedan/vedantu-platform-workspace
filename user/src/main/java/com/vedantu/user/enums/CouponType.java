package com.vedantu.user.enums;

public enum CouponType {

    CREDIT,
    TEACHER_DISCOUNT,
    VEDANTU_DISCOUNT,
    PASS
}
