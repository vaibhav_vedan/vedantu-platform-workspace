package com.vedantu.user.enums;

public enum OnboardingFeedback {
    UGLY, BAD, OK, GOOD, SUPER, NONE
}
