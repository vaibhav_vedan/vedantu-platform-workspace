package com.vedantu.user.enums;

public enum ItemCheckStatus {
    SUCCESS,
    ERROR,
    PENDING
}
