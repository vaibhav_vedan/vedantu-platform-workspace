package com.vedantu.user.enums;

public enum OnboardingCompletionStatus {
    PENDING,
    STARTED,
    ONGOING,
    COMPLETED,
    SKIPPED
}
