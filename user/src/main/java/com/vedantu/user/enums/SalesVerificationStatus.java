package com.vedantu.user.enums;

public enum SalesVerificationStatus {
    VERIFICATION_PENDING,
    VERIFICATION_SUCCESS,
    VERIFICATION_FAILURE
}
