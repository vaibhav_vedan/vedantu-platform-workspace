package com.vedantu.user.enums;

public enum VerifiedType {
    OTP,
    TRUECALLER,
    PAYTM
}
