package com.vedantu.user.enums;

public enum OnboardingStep {
    NONE,
    INTRODUCTION,
    // COMPATIBILITY_CHECK,
    // EXPERICENCE_PLATFORM,
    PROFILE_CREATION,
    COURSE_ENROLL,
    // COMPLETED
}
