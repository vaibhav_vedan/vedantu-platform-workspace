package com.vedantu.user.requests;

import com.vedantu.user.enums.ReferralStep;

public class ProcessReferralBonusReq {

    private ReferralStep referralStep;
    private Long userId;

    public ReferralStep getReferralStep() {
        return referralStep;
    }

    public void setReferralStep(ReferralStep referralStep) {
        this.referralStep = referralStep;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }
}
