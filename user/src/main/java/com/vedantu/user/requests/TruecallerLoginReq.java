package com.vedantu.user.requests;

import com.vedantu.util.fos.request.AbstractFrontEndReq;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


/**
 *
 * @author Aditya Rathi
 */


@AllArgsConstructor
@NoArgsConstructor
@Data
public class TruecallerLoginReq extends AbstractFrontEndReq {
    private String payload;
    private String signature;
    private String signatureAlgorithm;
}
