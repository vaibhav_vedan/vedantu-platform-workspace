package com.vedantu.user.requests;

import com.vedantu.notification.enums.CommunicationType;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import lombok.Data;

import java.util.List;

/**
 * @author MNPK
 */

@Data
public class SendUserEmailReq extends AbstractFrontEndReq {

    private Long userId;
    private CommunicationType emailType;


    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();
        if (null == userId) {
            errors.add("userId");
        }
        if (null == emailType) {
            errors.add("emailType");
        }
        return errors;
    }

}
