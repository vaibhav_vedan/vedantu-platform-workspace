package com.vedantu.user.requests;

import com.vedantu.util.StringUtils;
import com.vedantu.util.fos.request.AbstractFrontEndReq;

import java.util.List;

public class UserPreLoginVerificationInfoReq extends AbstractFrontEndReq {
    private String email;
    private String phoneNumber;
    private String phoneCode;

    private Boolean councellerAgent = false;
    private Boolean vsatVerification = false;

    private String token;

    private String version;

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Boolean getVsatVerification() {
        return vsatVerification;
    }

    public void setVsatVerification(Boolean vsatVerification) {
        this.vsatVerification = vsatVerification;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getPhoneCode() {
        return phoneCode;
    }

    public void setPhoneCode(String phoneCode) {
        this.phoneCode = phoneCode;
    }

    public Boolean getCouncellerAgent() {
        return councellerAgent;
    }

    public void setCouncellerAgent(Boolean councellerAgent) {
        this.councellerAgent = councellerAgent;
    }

    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();
        if (StringUtils.isNotEmpty(phoneNumber) && !String.valueOf(Long.parseLong(phoneNumber)).equals(phoneNumber)) {
            errors.add("Invalid Phone Number, please Remove the zero before phone Number");
        }
        return errors;
    }
}
