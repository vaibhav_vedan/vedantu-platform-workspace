package com.vedantu.user.requests;

import com.vedantu.util.StringUtils;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import lombok.Data;

import java.util.List;

@Data
public class UserPreBookingVerificationInfoReq extends AbstractFrontEndReq {
    private String email;
    private String phoneNumber;
    private String phoneCode;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getPhoneCode() {
        return phoneCode;
    }

    public void setPhoneCode(String phoneCode) {
        this.phoneCode = phoneCode;
    }

    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();
        if (StringUtils.isEmpty(phoneNumber)) {
            errors.add("phoneNumber is empty");
        }
        if (StringUtils.isNotEmpty(phoneNumber) && !String.valueOf(Long.parseLong(phoneNumber)).equals(phoneNumber)) {
            errors.add("Invalid Phone Number, please Remove the zero before phone Number");
        }
        if (StringUtils.isEmpty(email)) {
            errors.add("email is empty");
        }
        if (StringUtils.isEmpty(phoneCode)) {
            errors.add("phoneCode is empty");
        }
        return errors;
    }
}
