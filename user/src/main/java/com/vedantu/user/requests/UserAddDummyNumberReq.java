package com.vedantu.user.requests;

import com.vedantu.util.fos.request.AbstractFrontEndReq;

public class UserAddDummyNumberReq extends AbstractFrontEndReq {

    private Long userId;

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }
}
