package com.vedantu.user.requests;

import com.vedantu.user.enums.SAMIntroductionStatus;
import com.vedantu.user.enums.SalesVerificationStatus;
import com.vedantu.util.fos.request.AbstractFrontEndListReq;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
public class SalesVerificationAndSAMInformationRequest extends AbstractFrontEndListReq {

    // Student Details
    private Long studentId;
    private String studentGrade;

    // Bundle Details
    private String bundleId;

    // Sales verification
    private SalesVerificationStatus salesVerificationStatus;
    private Long salesVerifierId;

    // SAM Introduction
    private Long idOfSAM;
    private SAMIntroductionStatus samIntroductionStatus;

    // Sort criteria on the basis of creation time and last activity
    private boolean sortAccordingToLastActivity=false;

    @Override
    protected List<String> collectVerificationErrors() {
        return super.collectVerificationErrors();
    }
}
