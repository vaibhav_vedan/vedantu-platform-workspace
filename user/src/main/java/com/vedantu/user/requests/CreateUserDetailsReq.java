package com.vedantu.user.requests;

import com.vedantu.util.fos.request.AbstractFrontEndReq;

import java.util.List;

public class CreateUserDetailsReq extends AbstractFrontEndReq {

    private List<Long> userIds;

    public List<Long> getUserIds() {
        return userIds;
    }

    public void setUserIds(List<Long> userIds) {
        this.userIds = userIds;
    }
}
