package com.vedantu.user.requests;


import com.vedantu.User.*;
import com.vedantu.util.CustomValidator;
import com.vedantu.util.StringUtils;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import com.vedantu.util.fos.request.ReqLimits;
import com.vedantu.util.fos.request.ReqLimitsErMsgs;
import lombok.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

@Data
@Getter
@Setter
public class GetXVedTokenByUserDetailsReq extends AbstractFrontEndReq {
    public String secret;
    @NotNull(message = ReqLimitsErMsgs.RQD)
    @Size(max = ReqLimits.NAME_TYPE_MAX, message = ReqLimitsErMsgs.MAX_NAME_TYPE)
    private String email;
    @Size(max = ReqLimits.NAME_TYPE_MAX, message = ReqLimitsErMsgs.MAX_NAME_TYPE)
    private String contactNumber;
    @Size(max = ReqLimits.NAME_TYPE_MAX, message = ReqLimitsErMsgs.MAX_NAME_TYPE)
    private String phoneCode;
    @Size(max = ReqLimits.NAME_TYPE_MAX, message = ReqLimitsErMsgs.MAX_NAME_TYPE)
    private String password;
    @Size(max = ReqLimits.NAME_TYPE_MAX, message = ReqLimitsErMsgs.MAX_NAME_TYPE)
    private String firstName;
    @Size(max = ReqLimits.NAME_TYPE_MAX, message = ReqLimitsErMsgs.MAX_NAME_TYPE)
    private String lastName;
    private Gender gender;
    @Valid
    private LocationInfo locationInfo = new LocationInfo();

    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();

        if (StringUtils.isEmpty(secret)) {
            errors.add("secret Required");
        }

        if (StringUtils.isEmpty(email) && StringUtils.isEmpty(contactNumber)) {
            errors.add("Required contactNumber or email");
        }

        if (StringUtils.isNotEmpty(email) && !CustomValidator.validEmail(email)) {
            errors.add("Invalid Email");
        }
        return errors;
    }
}
