package com.vedantu.user.requests;

import com.vedantu.util.fos.request.AbstractFrontEndReq;

import java.util.List;

public class UserEmailUpdateReq extends AbstractFrontEndReq {

    private List<Long> userIds;
    private String token;

    public List<Long> getUserIds() {
        return userIds;
    }

    public void setUserIds(List<Long> userIds) {
        this.userIds = userIds;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
