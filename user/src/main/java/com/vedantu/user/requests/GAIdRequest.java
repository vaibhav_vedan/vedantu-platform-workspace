package com.vedantu.user.requests;

import com.vedantu.util.fos.request.AbstractFrontEndReq;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class GAIdRequest extends AbstractFrontEndReq
{
    private String userId;
    private String ga_id;
}
