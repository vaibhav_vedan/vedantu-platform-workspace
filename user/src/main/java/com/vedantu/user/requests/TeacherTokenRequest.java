package com.vedantu.user.requests;

import lombok.Data;

@Data
public class TeacherTokenRequest {
    private String email;
    private String secret;
}
