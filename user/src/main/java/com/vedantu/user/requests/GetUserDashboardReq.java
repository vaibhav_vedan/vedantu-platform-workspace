/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.user.requests;

import com.vedantu.User.User;
import com.vedantu.util.StringUtils;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import java.util.List;

/**
 *
 * @author jeet
 */
public class GetUserDashboardReq extends AbstractFrontEndReq{
    private Long userId;
    private String email;

    public GetUserDashboardReq() {
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    
    @Override
    protected List<String> collectVerificationErrors() {
            List<String> errors = super.collectVerificationErrors();
            if (null == userId  && StringUtils.isEmpty(email)) {
                errors.add(User.Constants.USER_ID);
            }
            return errors;
    }
}
