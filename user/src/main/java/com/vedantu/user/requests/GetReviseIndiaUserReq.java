package com.vedantu.user.requests;

import com.vedantu.util.fos.request.AbstractFrontEndReq;
import lombok.Data;


@Data
public class GetReviseIndiaUserReq extends AbstractFrontEndReq {
    private String event;
}
