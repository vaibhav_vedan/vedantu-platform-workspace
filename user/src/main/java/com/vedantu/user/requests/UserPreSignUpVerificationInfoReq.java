package com.vedantu.user.requests;

import com.vedantu.util.fos.request.AbstractFrontEndReq;

public class UserPreSignUpVerificationInfoReq extends AbstractFrontEndReq {

    private String phoneNumber;
    private String phoneCode;
    private String email;

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getPhoneCode() {
        return phoneCode;
    }

    public void setPhoneCode(String phoneCode) {
        this.phoneCode = phoneCode;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
