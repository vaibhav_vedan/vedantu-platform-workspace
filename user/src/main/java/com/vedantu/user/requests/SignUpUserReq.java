package com.vedantu.user.requests;

import java.util.List;
import java.util.Map;

import javax.validation.Valid;
import javax.validation.constraints.Size;

import com.vedantu.User.FeatureSource;
import com.vedantu.User.LocationInfo;
import com.vedantu.User.Role;
import com.vedantu.User.SocialInfo;
import com.vedantu.User.StudentInfo;
import com.vedantu.User.TeacherInfo;
import com.vedantu.User.UTMParams;
import com.vedantu.User.User;
import com.vedantu.User.request.SocialSource;
import com.vedantu.util.CustomValidator;
import com.vedantu.util.StringUtils;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import com.vedantu.util.fos.request.ReqLimits;
import com.vedantu.util.fos.request.ReqLimitsErMsgs;

public class SignUpUserReq extends AbstractFrontEndReq {

    private String rollNumber;

    private String email;

    private String verificationToken;

    private TruecallerLoginReq truecallerToken;

    private String phoneNumber;

    private String phoneCode;

    private String dob;

    @Size(max = ReqLimits.NAME_TYPE_MAX, message = ReqLimitsErMsgs.MAX_NAME_TYPE)
    private String password;

    @Size(max = ReqLimits.NAME_TYPE_MAX, message = ReqLimitsErMsgs.MAX_NAME_TYPE)
    private String fullName;

    @Size(max = ReqLimits.NAME_TYPE_MAX, message = ReqLimitsErMsgs.MAX_NAME_TYPE)
    private String grade;

    @Size(max = ReqLimits.NAME_TYPE_MAX, message = ReqLimitsErMsgs.MAX_NAME_TYPE)
    private String board;

    private String target;

    @Size(max = ReqLimits.COMMENT_TYPE_MAX, message = ReqLimitsErMsgs.MAX_COMMENT_TYPE)
    private String signUpURL;

    @Valid
    private StudentInfo studentInfo = new StudentInfo();
    @Valid
    private TeacherInfo teacherInfo = new TeacherInfo();
    @Valid
    private LocationInfo locationInfo = new LocationInfo();
    @Valid
    private SocialInfo socialInfo = new SocialInfo();

    @Size(max = ReqLimits.NAME_TYPE_MAX, message = ReqLimitsErMsgs.MAX_NAME_TYPE)
    private String socialSource;

    private String picUrl;

    @Valid
    private UTMParams utm = new UTMParams();

    @Size(max = ReqLimits.NAME_TYPE_MAX, message = ReqLimitsErMsgs.MAX_NAME_TYPE)
    private String couponCode;
    @Size(max = ReqLimits.NAME_TYPE_MAX, message = ReqLimitsErMsgs.MAX_NAME_TYPE)
    private String referrerCode;

    private Long referrerId;

    private Integer referrerBonus;

    private Boolean parentRegistration;

    @Size(max = ReqLimits.NAME_TYPE_MAX, message = ReqLimitsErMsgs.MAX_NAME_TYPE)
    private String tncVersion;

    private FeatureSource signUpFeature;

    private String signUpFeatureRefId;

    private Boolean redirectAfterSignup;

    //        private String ipAddress; is already part of AbstractFrontEndReq.java so not declaring it again
    @Size(max = ReqLimits.REASON_TYPE_MAX, message = ReqLimitsErMsgs.MAX_REASON_TYPE)
    private String deviceId;

    @Size(max = ReqLimits.NAME_TYPE_MAX, message = ReqLimitsErMsgs.MAX_NAME_TYPE)
    private String agentName;
    @Size(max = ReqLimits.NAME_TYPE_MAX, message = ReqLimitsErMsgs.MAX_NAME_TYPE)
    private String agentEmail;

    private String examName;

    private String referrer;
    private Boolean optIn;
    private String voltId;

    private List<String> achievements;
    private List<String> achievementDocments;
    private List<Map<String, String>> idproofs;

    private String orgId;
    
    private String appVersionCode;

    private String referrerEncryptCode;

    private String paytmToken;

    public String getPaytmToken() {
        return paytmToken;
    }

    public void setPaytmToken(String paytmToken) {
        this.paytmToken = paytmToken;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    private String jeeApplicationNumber;

    public String getjeeApplicationNumber() {
        return jeeApplicationNumber;
    }

    public void setjeeApplicationNumber(String jeeApplicationNumber) {
        this.jeeApplicationNumber = jeeApplicationNumber;
    }

    public String getRollNumber() {
        return rollNumber;
    }

    public void setRollNumber(String rollNumber) {
        this.rollNumber = rollNumber;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getCouponCode() {
        return couponCode;
    }

    public void setCouponCode(String couponCode) {
        this.couponCode = couponCode;
    }

    public String getReferrerCode() {
        return referrerCode;
    }

    public void setReferrerCode(String referrerCode) {
        this.referrerCode = referrerCode;
    }

    public Long getReferrerId() {
        return referrerId;
    }

    public void setReferrerId(Long referrerId) {
        this.referrerId = referrerId;
    }

    public Integer getReferrerBonus() {
        return referrerBonus;
    }

    public void setReferrerBonus(Integer referrerBonus) {
        this.referrerBonus = referrerBonus;
    }

    public Boolean getParentRegistration() {
        return parentRegistration;
    }

    public void setParentRegistration(Boolean parentRegistration) {
        this.parentRegistration = parentRegistration;
    }

    public String getTncVersion() {
        return tncVersion;
    }

    public void setTncVersion(String tncVersion) {
        this.tncVersion = tncVersion;
    }

    public FeatureSource getSignUpFeature() {
        return signUpFeature;
    }

    public void setSignUpFeature(FeatureSource signUpFeature) {
        this.signUpFeature = signUpFeature;
    }

    public String getSignUpFeatureRefId() {
        return signUpFeatureRefId;
    }

    public void setSignUpFeatureRefId(String signUpFeatureRefId) {
        this.signUpFeatureRefId = signUpFeatureRefId;
    }

    public Boolean getRedirectAfterSignup() {
        return redirectAfterSignup;
    }

    public void setRedirectAfterSignup(Boolean redirectAfterSignup) {
        this.redirectAfterSignup = redirectAfterSignup;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getAgentName() {
        return agentName;
    }

    public void setAgentName(String agentName) {
        this.agentName = agentName;
    }

    public String getAgentEmail() {
        return agentEmail;
    }

    public void setAgentEmail(String agentEmail) {
        this.agentEmail = agentEmail;
    }

    public String getExamName() {
        return examName;
    }

    public void setExamName(String examName) {
        this.examName = examName;
    }

    public String getReferrer() {
        return referrer;
    }

    public void setReferrer(String referrer) {
        this.referrer = referrer;
    }

    public Boolean getOptIn() {
        return optIn;
    }

    public void setOptIn(Boolean optIn) {
        this.optIn = optIn;
    }

    public UTMParams getUtm() {
        return utm;
    }

    public void setUtm(UTMParams utm) {
        this.utm = utm;
    }

    public String getPicUrl() {
        return picUrl;
    }

    public void setPicUrl(String picUrl) {
        this.picUrl = picUrl;
    }

    public String getSocialSource() {
        return socialSource;
    }

    public SocialSource __getSocialSource() {
        return SocialSource.valueOfKey(socialSource);
    }

    public void setSocialSource(String socialSource) {
        this.socialSource = socialSource;
    }

    public StudentInfo getStudentInfo() {
        return studentInfo;
    }

    public void setStudentInfo(StudentInfo studentInfo) {
        this.studentInfo = studentInfo;
    }

    public TeacherInfo getTeacherInfo() {
        return teacherInfo;
    }

    public void setTeacherInfo(TeacherInfo teacherInfo) {
        this.teacherInfo = teacherInfo;
    }

    public LocationInfo getLocationInfo() {
        return locationInfo;
    }

    public void setLocationInfo(LocationInfo locationInfo) {
        this.locationInfo = locationInfo;
    }

    public SocialInfo getSocialInfo() {
        return socialInfo;
    }

    public void setSocialInfo(SocialInfo socialInfo) {
        this.socialInfo = socialInfo;
    }

    private Role role;

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getPhoneCode() {
        return phoneCode;
    }

    public void setPhoneCode(String phoneCode) {
        this.phoneCode = phoneCode;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public String getSignUpURL() {
        return signUpURL;
    }

    public void setSignUpURL(String signUpURL) {
        this.signUpURL = signUpURL;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getVerificationToken() {
        return verificationToken;
    }

    public void setVerificationToken(String verificationToken) {
        this.verificationToken = verificationToken;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

    public String getBoard() {
        return board;
    }

    public void setBoard(String board) {
        this.board = board;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public String getVoltId() {
        return voltId;
    }

    public void setVoltId(String voltId) {
        this.voltId = voltId;
    }

    public List<String> getAchievements() {
        return achievements;
    }

    public void setAchievements(List<String> achievements) {
        this.achievements = achievements;
    }

    public List<String> getAchievementDocments() {
        return achievementDocments;
    }

    public void setAchievementDocments(List<String> achievementDocments) {
        this.achievementDocments = achievementDocments;
    }

    public List<Map<String, String>> getIdproofs() {
        return idproofs;
    }

    public void setIdproofs(List<Map<String, String>> idproofs) {
        this.idproofs = idproofs;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getReferrerEncryptCode() {
        return referrerEncryptCode;
    }

    public void setReferrerEncryptCode(String referrerEncryptCode) {
        this.referrerEncryptCode = referrerEncryptCode;
    }

    @Override
    protected List<String> collectVerificationErrors() {

        List<String> errors = super.collectVerificationErrors();
        if (!CustomValidator.validPhoneNumber(phoneNumber)) {
            errors.add("Invalid " + User.Constants.CONTACT_NUMBER);
        }

        if (StringUtils.isEmpty(fullName)) {
            errors.add("Mandatory Field missing" + User.Constants.FULL_NAME);
        }

        if (StringUtils.isNotEmpty(rollNumber)) {
            if (rollNumber.length() > 256) {
                errors.add("Invalid roll number" + User.Constants.FULL_NAME);
            }
        }

        if (FeatureSource.ORG_USER_SIGNUP.equals(signUpFeature) && StringUtils.isEmpty(orgId)) {
            errors.add("orgId missing");
        }

        return errors;
    }

    public TruecallerLoginReq getTruecallerToken() {
        return truecallerToken;
    }

    public void setTruecallerToken(TruecallerLoginReq truecallerToken) {
        this.truecallerToken = truecallerToken;
    }

	public String getAppVersionCode() {
		return appVersionCode;
	}

	public void setAppVersionCode(String appVersionCode) {
		this.appVersionCode = appVersionCode;
	}
}
