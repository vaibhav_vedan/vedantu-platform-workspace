package com.vedantu.user.requests;

import java.util.List;

import com.vedantu.util.fos.request.AbstractFrontEndListReq;

public class GetUserLeadReq extends AbstractFrontEndListReq{
	
	private String email;
	private String contactNumber;
	private List<String> cities;
	private String country;
	private String source;
	private Long fromTime;
	private Long tillTime;

	public Long getFromTime() {
		return fromTime;
	}

	public void setFromTime(Long fromTime) {
		this.fromTime = fromTime;
	}

	public Long getTillTime() {
		return tillTime;
	}

	public void setTillTime(Long tillTime) {
		this.tillTime = tillTime;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getContactNumber() {
		return contactNumber;
	}

	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public List<String> getCities() {
		return cities;
	}

	public void setCities(List<String> cities) {
		this.cities = cities;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

}
