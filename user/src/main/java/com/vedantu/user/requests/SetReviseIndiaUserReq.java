package com.vedantu.user.requests;

import com.vedantu.util.fos.request.AbstractFrontEndReq;

public class SetReviseIndiaUserReq extends AbstractFrontEndReq {
    private String rollNumber;
    private String city;
    private String school;
    private Integer grade;
    private String dob;
    private String admitCardNumber;
    private String schoolNo;
    private String centreNo;
    private String event;


    public String getRollNumber() {
        return rollNumber;
    }

    public void setRollNumber(String rollNumber) {
        this.rollNumber = rollNumber;
    }

    public String getEvent() {
        return event;
    }

    public void setEvent(String event) {
        this.event = event;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getSchool() {
        return school;
    }

    public void setSchool(String school) {
        this.school = school;
    }

    public Integer getGrade() {
        return grade;
    }

    public void setGrade(Integer grade) {
        this.grade = grade;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getAdmitCardNumber() {
        return admitCardNumber;
    }

    public void setAdmitCardNumber(String admitCardNumber) {
        this.admitCardNumber = admitCardNumber;
    }

    public String getSchoolNo() {
        return schoolNo;
    }

    public void setSchoolNo(String schoolNo) {
        this.schoolNo = schoolNo;
    }

    public String getCentreNo() {
        return centreNo;
    }

    public void setCentreNo(String centreNo) {
        this.centreNo = centreNo;
    }
}
