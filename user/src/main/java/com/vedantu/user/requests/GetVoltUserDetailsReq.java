package com.vedantu.user.requests;

import com.vedantu.User.enums.VoltStatus;
import com.vedantu.util.fos.request.AbstractFrontEndListReq;
import lombok.Data;

@Data
public class GetVoltUserDetailsReq extends AbstractFrontEndListReq {
    private VoltStatus status;
    private String filterText;
    private String state;
    private String city;
    private String school;
}
