package com.vedantu.user.requests;

import com.vedantu.user.pojo.onboarding.SAMIntroductionInfo;
import com.vedantu.user.pojo.onboarding.SalesVerificationInfo;
import com.vedantu.util.StringUtils;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Objects;

@Data
@NoArgsConstructor
public class SalesVerificationAndSAMDataSetupRequest extends AbstractFrontEndReq {
    private String onboardingId;
    private SalesVerificationInfo salesVerificationInfo;
    private SAMIntroductionInfo samIntroductionInfo;

    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();

        if(StringUtils.isEmpty(onboardingId)){
            errors.add("[onboardingId] cann't be a null parameter");
        }

        if(Objects.isNull(salesVerificationInfo) && Objects.isNull(samIntroductionInfo)){
            errors.add("Both [salesVerificationInfo] and [samIntroductionInfo] cann't be null simultaneously");
        }

        if(Objects.nonNull(salesVerificationInfo) && Objects.nonNull(samIntroductionInfo)){
            errors.add("Both [salesVerificationInfo] and [samIntroductionInfo] cann't be non-null simultaneously");
        }

        if(Objects.nonNull(salesVerificationInfo)){
            if(Objects.isNull(salesVerificationInfo.getSalesVerificationStatus())){
                errors.add("`null` value present in mandatory field of `salesVerificationInfo` field.");
            }
        }

        if(Objects.nonNull(samIntroductionInfo)){
            if(Objects.isNull(samIntroductionInfo.getSamIntroductionStatus()) || Objects.isNull(samIntroductionInfo.getIdOfSAM())){
                errors.add("`null` value present in mandatory field of `samIntroductionInfo` field.");
            }
        }

        return errors;
    }
}
