package com.vedantu.user.requests;

import com.vedantu.dinero.enums.PaymentType;
import com.vedantu.session.pojo.EntityType;
import com.vedantu.user.enums.OnboardingStep;
import com.vedantu.user.pojo.onboarding.CourseEnroll;
import com.vedantu.user.pojo.onboarding.Introduction;
import com.vedantu.user.pojo.onboarding.ProfileCreation;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Objects;

@Data
@NoArgsConstructor
public class StudentOnboardingRequest extends AbstractFrontEndReq {
    // Student Details
    private Long studentId;
    private String studentName;
    private String studentEmail;
    private String studentGrade;
    private String bundleId;
    private String bundleName;
    private EntityType entityType;

    // Payment Details
    private PaymentType paymentType;
    private Long downPayment;
    private Long bookingPotential;

    // Onboarding step to be saved
    private OnboardingStep onboardingStepType;

    // Detail data of onboarding step
    private Introduction introduction;
    private ProfileCreation profileCreation;
    private CourseEnroll courseEnroll;
    private boolean introductionCompleted=false;

    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();
        if(Objects.isNull(studentId)){
            errors.add("[studentId] cann't be null");
        }
        return errors;
    }
}
