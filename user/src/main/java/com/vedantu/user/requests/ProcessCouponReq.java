package com.vedantu.user.requests;

import com.vedantu.user.enums.CouponType;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import org.springframework.util.StringUtils;

import java.util.List;

public class ProcessCouponReq extends AbstractFrontEndReq {
    private String couponCode;
    private Long userId;
    private CouponType credit;

    public String getCouponCode() {
        return couponCode;
    }

    public void setCouponCode(String couponCode) {
        this.couponCode = couponCode;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public CouponType getCredit() {
        return credit;
    }

    public void setCredit(CouponType credit) {
        this.credit = credit;
    }

    public ProcessCouponReq(String couponCode, Long userId, CouponType credit) {
        super();
        this.couponCode = couponCode;
        this.userId = userId;
        this.credit = credit;
    }

    public ProcessCouponReq() {
        super();
        // TODO Auto-generated constructor stub
    }

    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();
        if (StringUtils.isEmpty(couponCode)) {
            errors.add("couponCode");
        }
        if (StringUtils.isEmpty(userId)) {
            errors.add("userId");
        }
        if (StringUtils.isEmpty(credit)) {
            errors.add("credit");
        }
        return errors;
    }

    @Override
    public String toString() {
        return "ProcessCouponReq [couponCode=" + couponCode + ", userId=" + userId + ", credit=" + credit + "]";
    }

}
