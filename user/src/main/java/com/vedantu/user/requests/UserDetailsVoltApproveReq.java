package com.vedantu.user.requests;

import com.vedantu.User.enums.VoltStatus;
import com.vedantu.util.fos.request.AbstractFrontEndUserReq;
import lombok.Data;

@Data
public class UserDetailsVoltApproveReq extends AbstractFrontEndUserReq {
    private String id;
    private VoltStatus status;
    private String reason;
}
