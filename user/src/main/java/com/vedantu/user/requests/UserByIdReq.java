package com.vedantu.user.requests;

import lombok.*;

import java.util.List;

@Data
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class UserByIdReq {

    private List<Long> userIds;
    private String callerType;
}
