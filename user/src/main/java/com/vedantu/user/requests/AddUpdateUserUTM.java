package com.vedantu.user.requests;


import java.util.List;

import com.vedantu.User.UTMParams;
import com.vedantu.util.fos.request.AbstractFrontEndReq;


public class AddUpdateUserUTM extends AbstractFrontEndReq {

	private String name;
	private UTMParams utmParams;
	private String signUpURL;
	private String referrer;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public UTMParams getUtmParams() {
		return utmParams;
	}

	public void setUtmParams(UTMParams utmParams) {
		this.utmParams = utmParams;
	}

	@Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();
        if ( null == getUtmParams()) {
        	errors.add("utmParams null");
        }
        return errors;
    }

	public String getSignUpURL() {
		return signUpURL;
	}

	public void setSignUpURL(String signUpURL) {
		this.signUpURL = signUpURL;
	}

	public String getReferrer() {
		return referrer;
	}

	public void setReferrer(String referrer) {
		this.referrer = referrer;
	}
}