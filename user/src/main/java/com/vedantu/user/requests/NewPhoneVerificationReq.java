package com.vedantu.user.requests;

import com.vedantu.util.StringUtils;
import com.vedantu.util.fos.request.AbstractFrontEndReq;

import java.util.List;

public class NewPhoneVerificationReq extends AbstractFrontEndReq {
    private String userId;
    private String email;
    private String phoneNumber;
    private String phoneCode;


    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getPhoneCode() {
        return phoneCode;
    }

    public void setPhoneCode(String phoneCode) {
        this.phoneCode = phoneCode;
    }

    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();
        if (StringUtils.isNotEmpty(phoneNumber) && !String.valueOf(Long.parseLong(phoneNumber)).equals(phoneNumber)) {
            errors.add("Invalid Phone Number, please Remove the zero before phone Number");
        }
        return errors;
    }
}
