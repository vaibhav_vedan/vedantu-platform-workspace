package com.vedantu.user.requests;

import com.vedantu.user.pojo.GPSLocalePojo;
import com.vedantu.util.fos.request.AbstractFrontEndReq;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class GpsLocationReq extends AbstractFrontEndReq {
	
	private String userId;
	
	private GPSLocalePojo userLocale;

}
