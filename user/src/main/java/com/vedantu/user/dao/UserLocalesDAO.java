/**
 * 
 */
package com.vedantu.user.dao;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import com.vedantu.user.entity.UserGPSLocales;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.dbutils.AbstractMongoDAO;

/**
 * @author vedantu
 *
 */
@Service
public class UserLocalesDAO extends AbstractMongoDAO{

	@Autowired
	private RedisDAO redisDAO;

	@Autowired
	MongoClientFactory mongoClientFactory;

	@Autowired
	private LogFactory logFactory;
	@SuppressWarnings("static-access")
	private final Logger logger = logFactory.getLogger(UserLocalesDAO.class);


	public UserLocalesDAO() {
		super();
	}

	public UserGPSLocales fetchUserGPSLocales(String userId) {
		UserGPSLocales userGPSLocale = new UserGPSLocales();
		if (StringUtils.isNotEmpty(userId)){
			Query query = new Query();
			query.addCriteria(Criteria.where(UserGPSLocales.Constants.USER_ID).is(userId));
			logger.info("GAID Query : {}", query);
			userGPSLocale = findOne(query, UserGPSLocales.class);

		}
		return userGPSLocale;
	}


	@Override
	protected MongoOperations getMongoOperations() {
		return mongoClientFactory.getMongoOperations();
	}

	public void save(UserGPSLocales userGPSLocales) {
		logger.debug("Request to save userGPSLocales : " + userGPSLocales.toString());
		UserGPSLocales entity = new UserGPSLocales(); 
		try {
			if(null != userGPSLocales) {
				if(null != userGPSLocales.getUserId()) {
					entity = findByUserId(userGPSLocales.getUserId());
					if(null != entity) {
						entity.setHistoricalLocales(userGPSLocales.getHistoricalLocales());
						entity.setLatestLocale(userGPSLocales.getLatestLocale());
						entity.setEntityDefaultProperties(userGPSLocales.getUserId());
						saveEntity(entity);
					}else {
						saveEntity(userGPSLocales);
					}
				}

			}
			logger.info("userGPSLocales id after creation: " + userGPSLocales.getId());
		} catch (Exception ex) {
			throw new RuntimeException("UserGPSLocalesUpdateError : Error updating UserGPSLocales " + userGPSLocales.toString(), ex);
		}
	}

	public UserGPSLocales findByUserId(String userId){
		UserGPSLocales entity = new UserGPSLocales();

		if (StringUtils.isNotEmpty(userId)){
			Query query = new Query();
			query.addCriteria(Criteria.where(UserGPSLocales.Constants.USER_ID).is(userId));
			entity = findOne(query, UserGPSLocales.class);
		}
		return entity;
	}

}
