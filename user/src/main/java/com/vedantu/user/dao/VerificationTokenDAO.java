package com.vedantu.user.dao;

import com.vedantu.User.VerificationLinkStatus;
import com.vedantu.User.VerificationTokenType;
import com.vedantu.user.entity.VerificationTokenII;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.dbutils.AbstractMongoDAO;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import static org.aspectj.weaver.Iterators.array;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;

import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

@Service
public class VerificationTokenDAO extends AbstractMongoDAO {

    @Autowired
    private MongoClientFactory mongoClientFactory;

    public VerificationTokenDAO() {
        super();
    }

    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }

    public void create(VerificationTokenII p, Long callingUserId) {
        String callingUserIdString = null;
        if (callingUserId != null) {
            callingUserIdString = callingUserId.toString();
        }
        if (p != null) {
            saveEntity(p, callingUserIdString);
        }
    }

    public VerificationTokenII getById(String id) {
        VerificationTokenII token = null;
        if (!StringUtils.isEmpty(id)) {
            token = getEntityById(id, VerificationTokenII.class);
        }
        return token;
    }

    public VerificationTokenII getActiveVerificationToken(Long userId,
                                                          String contactNumber, String phoneCode) {

        VerificationTokenII token = null;
        Query query;
        query = new Query();
        query.addCriteria(Criteria.where(VerificationTokenII.Constants.USER_ID).is(userId));
        query.addCriteria(Criteria.where(VerificationTokenII.Constants.EMAIL_ID).is(contactNumber));
        query.addCriteria(Criteria.where(VerificationTokenII.Constants.STATUS).is(VerificationLinkStatus.ACTIVE));
        query.addCriteria(Criteria.where(VerificationTokenII.Constants.CREATION_TIME).gt(System.currentTimeMillis() - 1000 * 60 * 15));
        if (!StringUtils.isEmpty(phoneCode)) {
            String formattedPhoneCode = phoneCode.replace("+", "");
            query.addCriteria(Criteria.where(VerificationTokenII.Constants.PHONE_CODE).is(formattedPhoneCode));
        }

        List<VerificationTokenII> results = runQuery(query, VerificationTokenII.class);
        if (null != results && !results.isEmpty()) {
            token = results.get(0);
        }
        return token;
    }

    public VerificationTokenII getTokenByCode(String tokenCode) {

        //For previous tokens from appengine
        Query query = new Query();
        query.addCriteria(Criteria.where(VerificationTokenII.Constants.CODE).is(tokenCode));
        List<VerificationTokenII> results = runQuery(query, VerificationTokenII.class);
        if (null != results && !results.isEmpty()) {
            return results.get(0);
        }

        //For new ids
        return getById(tokenCode);
    }

    public VerificationTokenII getTokenByCode(Long sessionUserId, String verificationCode) {
        Query query = new Query();
        query.addCriteria(Criteria.where(VerificationTokenII.Constants.CODE).is(verificationCode));
        query.addCriteria(Criteria.where(VerificationTokenII.Constants.USER_ID).is(sessionUserId));
        List<VerificationTokenII> results = runQuery(query, VerificationTokenII.class);
        if (null != results && !results.isEmpty()) {
            return results.get(0);
        }
        return null;
    }

    public List<VerificationTokenII> getVerificationTokens(Long userId, VerificationTokenType verificationTokenType, VerificationLinkStatus verificationLinkStatus) {
        Query query = new Query();
        query.addCriteria(Criteria.where(VerificationTokenII.Constants.USER_ID).is(userId));
        query.addCriteria(Criteria.where(VerificationTokenII.Constants.STATUS).is(verificationLinkStatus));
        query.addCriteria(Criteria.where(VerificationTokenII.Constants.TYPE).is(verificationTokenType));
        return runQuery(query, VerificationTokenII.class);
    }

    public Map<Long, String> getPasswordResetTokens(List<Long> userIds) {
        Query query = new Query();
        query.addCriteria(Criteria.where(VerificationTokenII.Constants.USER_ID)
                .in(userIds));
        query.addCriteria(Criteria.where(VerificationTokenII.Constants.TYPE)
                .is(VerificationTokenType.RESET_PASSWORD));

        List<VerificationTokenII> tokens = runQuery(query, VerificationTokenII.class);
        Map<Long, String> result = new HashMap<>();
        if (ArrayUtils.isNotEmpty(tokens)) {
            for (VerificationTokenII token : tokens) {
                result.put(token.getUserId(), token.getId());
            }
        }
        return result;
    }


    public List<VerificationTokenII> getVerificationCodes(Integer start, Integer limit, String emailIdOrContactNumber) {
        Query query = new Query();
        if(!StringUtils.isEmpty(emailIdOrContactNumber)){
            query.addCriteria(Criteria.where(VerificationTokenII.Constants.EMAIL_ID).is(emailIdOrContactNumber));
        }
        query.with(Sort.by(Sort.Direction.DESC, VerificationTokenII.Constants.CREATION_TIME));
        setFetchParameters(query, start, limit);
        return runQuery(query, VerificationTokenII.class);
    }

    public VerificationTokenII getLoginVerificationCode(String email, String phoneNumber , String phoneCode){
        Query query = new Query();

        if(null != email) query.addCriteria(Criteria.where(VerificationTokenII.Constants.EMAIL_ID).is(email));
        else if(null != phoneNumber) query.addCriteria(Criteria.where(VerificationTokenII.Constants.EMAIL_ID).is(phoneNumber));
        else return null;

        query.addCriteria(Criteria.where(VerificationTokenII.Constants.STATUS).is(VerificationLinkStatus.ACTIVE));
        if(null != phoneCode){
            query.addCriteria(Criteria.where(VerificationTokenII.Constants.PHONE_CODE).is(phoneCode));
        }

        query.addCriteria(Criteria.where(VerificationTokenII.Constants.CREATION_TIME).gt(System.currentTimeMillis() - 1000 * 60 * 2));
        query.with(Sort.by(Sort.Direction.DESC, VerificationTokenII.Constants.CREATION_TIME));
        List<VerificationTokenII> results = runQuery(query, VerificationTokenII.class);

        if (null != results && !results.isEmpty()) {
            return results.get(0);
        }
        return null;
    }

    public VerificationTokenII getUsedVerificationToken(String phoneNumber, String phoneCode, String verificationToken) {
        Query query = new Query();
        String formattedPhoneCode = (null != phoneCode) ? phoneCode.replace("+", "") : phoneCode;
        query.addCriteria(Criteria.where(VerificationTokenII.Constants.PHONE_CODE).is(formattedPhoneCode));
        query.addCriteria(Criteria.where(VerificationTokenII.Constants.EMAIL_ID).is(phoneNumber));
        query.addCriteria(Criteria.where(VerificationTokenII.Constants.STATUS).is(VerificationLinkStatus.USED));
        query.addCriteria(Criteria.where(VerificationTokenII.Constants.CODE).is(verificationToken));
        query.addCriteria(Criteria.where(VerificationTokenII.Constants.CREATION_TIME).gt(System.currentTimeMillis() - 1000 * 60 * 20));
        query.with(Sort.by(Sort.Direction.DESC, VerificationTokenII.Constants.CREATION_TIME));
        List<VerificationTokenII> results = runQuery(query, VerificationTokenII.class);

        if (null != results && !results.isEmpty()) {
            return results.get(0);
        }
        return null;

    }

    public VerificationTokenII getEmailLoginVerificationCode(String email) {
        if(null == email){
            return null;
        }
        Query query = new Query();
        query.addCriteria(Criteria.where(VerificationTokenII.Constants.EMAIL_ID).is(email.toLowerCase()));
        query.addCriteria(Criteria.where(VerificationTokenII.Constants.STATUS).is(VerificationLinkStatus.ACTIVE));
        query.addCriteria(Criteria.where(VerificationTokenII.Constants.TYPE).is(VerificationTokenType.EMAIL_VERIFICATION));
        query.addCriteria(Criteria.where(VerificationTokenII.Constants.CREATION_TIME).gt(System.currentTimeMillis() - 1000 * 60 * 5));
        query.with(Sort.by(Sort.Direction.DESC, VerificationTokenII.Constants.CREATION_TIME));
        List<VerificationTokenII> results = runQuery(query, VerificationTokenII.class);

        if (null != results && !results.isEmpty()) {
            return results.get(0);
        }
        return null;
    }

    public VerificationTokenII getPhoneLoginVerificationCode(String phoneNumber, String phoneCode) {
        if(null == phoneCode || null == phoneNumber)
            return null;
        Query query = new Query();
        query.addCriteria(Criteria.where(VerificationTokenII.Constants.EMAIL_ID).is(phoneNumber));
        String formattedPhoneCode = (null != phoneCode) ? phoneCode.replace("+", "") : phoneCode;
        query.addCriteria(Criteria.where(VerificationTokenII.Constants.STATUS).is(VerificationLinkStatus.ACTIVE));
        query.addCriteria(Criteria.where(VerificationTokenII.Constants.PHONE_CODE).is(formattedPhoneCode));
        query.addCriteria(Criteria.where(VerificationTokenII.Constants.TYPE).is(VerificationTokenType.SMS_VERIFICATION));
        query.addCriteria(Criteria.where(VerificationTokenII.Constants.CREATION_TIME).gt(System.currentTimeMillis() - 1000 * 60 * 5));
        query.with(Sort.by(Sort.Direction.DESC, VerificationTokenII.Constants.CREATION_TIME));
        List<VerificationTokenII> results = runQuery(query, VerificationTokenII.class);

        if (null != results && !results.isEmpty()) {
            return results.get(0);
        }
        return null;
    }
}
