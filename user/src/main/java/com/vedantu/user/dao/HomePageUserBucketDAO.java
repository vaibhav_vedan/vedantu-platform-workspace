package com.vedantu.user.dao;

import com.vedantu.user.entity.HomePageUserBucket;
import com.vedantu.util.LogFactory;
import com.vedantu.util.dbentitybeans.mongo.EntityState;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class HomePageUserBucketDAO extends AbstractMongoDAO {

    @Autowired
    private MongoClientFactory mongoClientFactory;

    @Autowired
    private LogFactory logFactory;

    private final Logger logger = LogFactory.getLogger(UserDAO.class);

    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }

    public void create(HomePageUserBucket p) {
        try {
            if (p != null) {
                saveEntity(p);
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage());
        }
    }


    // todo add a partial index db.HomePageUserBucket.createIndex({userId : 1, entityState : 1}, {partialFilterExpression})
    public HomePageUserBucket getUserBucket(Long userId) {
        Query q = new Query();
        q.addCriteria(Criteria.where(HomePageUserBucket.Constants.USER_ID).is(userId));
        q.addCriteria(Criteria.where(HomePageUserBucket.Constants.ENTITY_STATE).is(EntityState.ACTIVE));
        return findOne(q, HomePageUserBucket.class);
    }
}
