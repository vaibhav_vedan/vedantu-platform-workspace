/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.user.dao;

import com.vedantu.user.entity.Org;
import com.vedantu.util.LogFactory;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import java.util.List;

import com.vedantu.util.security.HttpSessionData;
import com.vedantu.util.security.HttpSessionUtils;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

/**
 *
 * @author ajith
 */
@Service
public class OrgDAO extends AbstractMongoDAO {

    @Autowired
    MongoClientFactory mongoClientFactory;

    @Autowired
    private LogFactory logFactory;

    @Autowired
    private HttpSessionUtils sessionUtils;

    private final Logger logger = logFactory.getLogger(OrgDAO.class);

    public OrgDAO() {
        super();
    }

    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }

    public Org save(Org org) {
        HttpSessionData sessionData = sessionUtils.getCurrentSessionData();
        saveEntity(org,sessionData.getUserId().toString());
        return org;
    }

    public Org getOrgById(String orgId) {
        Query query = new Query();
        query.addCriteria(Criteria.where(Org.Constants.ID).is(orgId));
        List<Org> results = runQuery(query, Org.class);
        if (null != results && !results.isEmpty()) {
            return results.get(0);
        } else {
            return null;
        }
    }

    public List<Org> getOrgIds(Integer start, Integer limit) {
        Query query = new Query();
        query.with(Sort.by(Sort.Direction.DESC, Org.Constants.CREATION_TIME));
        setFetchParameters(query,start,limit);
        return runQuery(query,Org.class);
    }

}
