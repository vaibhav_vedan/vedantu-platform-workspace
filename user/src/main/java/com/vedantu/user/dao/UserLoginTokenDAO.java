/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.user.dao;

import com.vedantu.user.entity.UserLoginToken;
import com.vedantu.util.LogFactory;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

/**
 *
 * @author jeet
 */
@Service
public class UserLoginTokenDAO extends AbstractMongoDAO {

    @Autowired
    private LogFactory logFactory;

    private final Logger logger = logFactory.getLogger(UserLoginTokenDAO.class);

    @Autowired
    private MongoClientFactory mongoClientFactory;

    public UserLoginTokenDAO() {
        super();
    }

    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }

    public void create(UserLoginToken p, Long callingUserId) {
        String callingUserIdString = null;
        if (callingUserId != null) {
            callingUserIdString = callingUserId.toString();
        }
        if (p != null) {
            saveEntity(p, callingUserIdString);
        }
    }

    public UserLoginToken getById(String id) {
        UserLoginToken token = null;
        if (!StringUtils.isEmpty(id)) {
            token = getEntityById(id, UserLoginToken.class);
        }
        return token;
    }
}
