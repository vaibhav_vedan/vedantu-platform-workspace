package com.vedantu.user.dao;

import java.util.List;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;

import com.vedantu.async.AsyncTaskFactory;
import com.vedantu.user.entity.UserLead;
import com.vedantu.user.requests.GetUserLeadReq;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.CustomValidator;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.dbutils.AbstractMongoDAO;

@Service
public class UserLeadDAO extends AbstractMongoDAO{

	@Autowired
    private LogFactory logFactory;

    private final Logger logger = logFactory.getLogger(UserLeadDAO.class);

    @Autowired
    private AsyncTaskFactory asyncTaskFactory;
    
    @Autowired
    private MongoClientFactory mongoClientFactory;
	
	@Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }
	
    public UserLeadDAO() {
        super();
    }
    
    public void save(UserLead p, Long callingUserId) {
        //try {
        if (p != null) {
            String callingUserIdString = null;
            if (callingUserId != null) {
                callingUserIdString = callingUserId.toString();
            }
            logger.info("Saving into db" + p);
            saveEntity(p, callingUserIdString);
        }
    }
    
    public UserLead getUserLeadsById(String id) {
        return getEntityById(id, UserLead.class);
    }
    
    public void upsert(Query query, Update update) {
    	logger.info("Saving into db" + update);
    	upsertEntity(query, update, UserLead.class);        
    }
    
    public UserLead getUserLeadByEmail(String email,String source){
    	Query query = new Query();
        query.addCriteria(Criteria.where(UserLead.Constants.SOURCE).is(source));
        query.addCriteria(Criteria.where(UserLead.Constants.EMAIL).is(email));
    	return findOne(query,UserLead.class);
    	
    }
    
    public UserLead getUserLeadByPhone(String phone,String source){
    	Query query = new Query();
        query.addCriteria(Criteria.where(UserLead.Constants.SOURCE).is(source));
        query.addCriteria(Criteria.where(UserLead.Constants.CONTACT_NUMBER).is(phone));
    	return findOne(query,UserLead.class);
    	
    }
    
    public List<UserLead> getUserLeads(GetUserLeadReq req){
    	Query query = new Query();
    	if(ArrayUtils.isNotEmpty(req.getCities())){
    		query.addCriteria(Criteria.where(UserLead.Constants.CITY).in(req.getCities()));
    	}
    	if(req.getSource() != null){
    		query.addCriteria(Criteria.where(UserLead.Constants.SOURCE).is(req.getSource()));
    	}
    	if(StringUtils.isNotEmpty(req.getEmail())){
    		query.addCriteria(Criteria.where(UserLead.Constants.EMAIL).is(req.getEmail()));
    	}
    	if(StringUtils.isNotEmpty(req.getContactNumber())){
    		query.addCriteria(Criteria.where(UserLead.Constants.CONTACT_NUMBER).is(req.getContactNumber()));
    	}
    	if(StringUtils.isNotEmpty(req.getCountry())){
    		query.addCriteria(Criteria.where(UserLead.Constants.COUNTRY).is(req.getCountry()));
    	}
    	if(req.getFromTime() != null){
    		query.addCriteria(Criteria.where(UserLead.Constants.CREATION_TIME).gt(req.getFromTime()));
    	}
    	setFetchParameters(query, req);
    	query.with(Sort.by(Direction.DESC, UserLead.Constants.CREATION_TIME));
    	return runQuery(query,UserLead.class);
    	
    }
}
