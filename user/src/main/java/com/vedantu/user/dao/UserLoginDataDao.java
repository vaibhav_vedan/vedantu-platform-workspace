package com.vedantu.user.dao;

import com.vedantu.user.entity.UserLead;
import com.vedantu.user.entity.temp.LoginData;
import com.vedantu.util.LogFactory;
import com.vedantu.util.dbentities.mongo.AbstractMongoEntity;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

@Service
public class UserLoginDataDao extends AbstractMongoDAO {

    @Autowired
    private LogFactory logFactory;

    private final Logger logger = logFactory.getLogger(UserLoginDataDao.class);

    @Autowired
    private MongoClientFactory mongoClientFactory;

    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }

    public void save(LoginData p) {
        if (p != null) {
            saveEntity(p);
        }
    }


    public LoginData getPasswordChangedTime(String email) {
        Query query = new Query();
        query.addCriteria(Criteria.where(LoginData.Constants.LAST_PASSWORD_CHANGED_TIME).exists(true));
        query.addCriteria(Criteria.where(LoginData.Constants.EMAIL).is(email));

        return findOne(query, LoginData.class);
    }
}
