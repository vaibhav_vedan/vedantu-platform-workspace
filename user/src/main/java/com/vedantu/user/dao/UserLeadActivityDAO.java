package com.vedantu.user.dao;

import java.util.List;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.stereotype.Service;

import com.vedantu.user.entity.UserLeadActivity;
import com.vedantu.util.DateTimeUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

@Service
public class UserLeadActivityDAO extends AbstractMongoDAO{

	@Autowired
    private LogFactory logFactory;

	private final Logger logger = logFactory.getLogger(UserLeadActivityDAO.class);
	
    @Autowired
    private CounterService counterService;
  
    @Autowired
    private MongoClientFactory mongoClientFactory;
	
	@Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }
	
    public UserLeadActivityDAO() {
        super();
    }
    
    public void save(UserLeadActivity p, Long callingUserId) {
        //try {
        if (p != null) {
            String callingUserIdString = null;
            if (callingUserId != null) {
                callingUserIdString = callingUserId.toString();
            }
            logger.info("Saving into db" + p);
            saveEntity(p, callingUserIdString);
        }
    }
    
    public UserLeadActivity getUserLeadsActivityById(String id) {
        return getEntityById(id, UserLeadActivity.class);
    }
    
    public UserLeadActivity getLatestPastUserLeadActivity(String userLeadId, String source, String eventName, String eventValue) {
        Long lastTime= System.currentTimeMillis() - 2* DateTimeUtils.MILLIS_PER_HOUR;
        Query query = new Query();
        query.addCriteria(Criteria.where(UserLeadActivity.Constants.SOURCE).is(source));
        query.addCriteria(Criteria.where(UserLeadActivity.Constants.USER_LEAD_ID).is(userLeadId));
        query.addCriteria(Criteria.where(UserLeadActivity.Constants.EVENT_NAME).is(eventName));
        query.addCriteria(Criteria.where(UserLeadActivity.Constants.EVENT_VALUE).is(eventValue));
        query.addCriteria(Criteria.where(UserLeadActivity.Constants.CREATION_TIME).gte(lastTime));
    	return findOne(query,UserLeadActivity.class);
    }

    
//    public List<UserLeadsActivity> getUserLeadsActivity(GetUserLeadsActivityReq req){
//    	
//    	
//    }
}
