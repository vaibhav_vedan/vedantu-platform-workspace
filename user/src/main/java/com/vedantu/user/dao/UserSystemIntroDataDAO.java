/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.user.dao;

import com.vedantu.user.entity.UserSystemIntroData;
import com.vedantu.util.StringUtils;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;

/**
 *
 * @author jeet
 */
@Service
public class UserSystemIntroDataDAO extends AbstractMongoDAO{
    
    @Autowired
    private MongoClientFactory mongoClientFactory;    
            
    
    public UserSystemIntroDataDAO(){
        super();
    }

    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }    
    
    public void create(UserSystemIntroData p, Long callingUserId) {
            String callingUserIdString = null;
            if(callingUserId!=null) {
                callingUserIdString = callingUserId.toString();
            }
            if (p != null) {
                saveEntity(p, callingUserIdString);
            }
    }

    
    public UserSystemIntroData getUserSystemIntroData(Long userId, String type, String identifier){
        UserSystemIntroData userSystemIntroData = null;
        Query query;
        query = new Query();
        query.addCriteria(Criteria.where(UserSystemIntroData.Constants.USER_ID).is(userId));
        query.addCriteria(Criteria.where(UserSystemIntroData.Constants.TYPE).is(type));
        query.addCriteria(Criteria.where(UserSystemIntroData.Constants.IDENTIFIER).is(identifier));

        List<UserSystemIntroData> results = runQuery(query, UserSystemIntroData.class);
        if (null != results && !results.isEmpty()) {
            userSystemIntroData = results.get(0);
        }
        return userSystemIntroData;
    }
    
     public List<UserSystemIntroData> getUserSystemIntroDataList(Long userId, String type){
        List<UserSystemIntroData> userSystemIntroDataList = null;
        Query query;
        query = new Query();
        query.addCriteria(Criteria.where(UserSystemIntroData.Constants.USER_ID).is(userId));
        if (StringUtils.isNotEmpty(type)) {
            query.addCriteria(Criteria.where(UserSystemIntroData.Constants.TYPE).is(type));
        }
        userSystemIntroDataList = runQuery(query, UserSystemIntroData.class);
        return userSystemIntroDataList;
    }

}
