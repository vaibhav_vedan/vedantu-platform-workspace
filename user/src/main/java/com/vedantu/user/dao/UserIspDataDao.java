package com.vedantu.user.dao;

import com.vedantu.user.entity.temp.ISPData;
import com.vedantu.user.entity.temp.LoginData;
import com.vedantu.util.DateTimeUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class UserIspDataDao extends AbstractMongoDAO {
    @Autowired
    private LogFactory logFactory;

    private final Logger logger = logFactory.getLogger(UserIspDataDao.class);

    @Autowired
    private MongoClientFactory mongoClientFactory;

    @Autowired
    private RedisDAO redisDAO;

    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }

    public void save(ISPData p) {
        if (p != null) {
            saveEntity(p);
        }
    }

    public List<ISPData> getISPData(String loginParam) {
        Integer timeval = 5;

        try {
            String key = "TIME_CHECK_ISP";
            String timeCheckISP = redisDAO.get(key);
            if(StringUtils.isNotEmpty(timeCheckISP)){
                timeval = Integer.parseInt(timeCheckISP);
            }
        }catch(Exception e){
            logger.info("Error in fetching data from redis" + e.getMessage());
        }
        logger.info("getISP data by login param {}", loginParam);

        Query query = new Query();
        query.addCriteria(Criteria.where(ISPData.Constants.LOGIN_PARAM).is(loginParam));
        query.addCriteria(Criteria.where(ISPData.Constants.CREATION_TIME).gte(System.currentTimeMillis() - (DateTimeUtils.MILLIS_PER_MINUTE * timeval)));

        logger.info("query " + query);
        return runQuery(query, ISPData.class);

    }

    public Integer getCountForISP(String isp) {
        Integer timeval = 5;
        Integer count = 0;

        try {
            String key = "TIME_CHECK_ISP_COUNT";
            String timeCheckISP = redisDAO.get(key);
            if(StringUtils.isNotEmpty(timeCheckISP)){
                timeval = Integer.parseInt(timeCheckISP);
            }
        }catch(Exception e){
            logger.info("Error in fetching data from redis" + e.getMessage());
        }
        Query query = new Query();
        query.addCriteria(Criteria.where(ISPData.Constants.ISP).is(isp));
        query.addCriteria(Criteria.where(ISPData.Constants.CREATION_TIME).gte(System.currentTimeMillis() - (DateTimeUtils.MILLIS_PER_MINUTE * timeval)));

        logger.info("query " + query);
        List<ISPData> ispDataList = runQuery(query, ISPData.class);

        HashSet<String> ispSet = new HashSet<>();

        if(null != ispDataList){
            for(ISPData data : ispDataList){
                ispSet.add(data.getLoginParam());
            }
        }
        count = ispSet.size();
        return count;
    }
}
