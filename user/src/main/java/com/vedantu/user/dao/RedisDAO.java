package com.vedantu.user.dao;

import com.google.gson.Gson;
import com.vedantu.User.Pojo.ELTeachersProficiencies;
import com.vedantu.User.enums.TeacherCategory;
import com.vedantu.exception.InternalServerErrorException;
import com.vedantu.onetofew.enums.SessionLabel;
import com.vedantu.subscription.enums.EarlyLearningCourseType;
import com.vedantu.user.managers.AwsSNSManager;
import com.vedantu.util.LogFactory;
import com.vedantu.util.enums.SNSSubject;
import com.vedantu.util.enums.SNSTopic;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vedantu.util.redis.AbstractRedisDAO;
import redis.clients.util.SafeEncoder;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.EnumSet;
import java.util.List;

@Service
public class RedisDAO extends AbstractRedisDAO{

    @Autowired
    private AwsSNSManager awsSNSManager;

    public static final long MAX_USERS_PER_BITMAP_KEY = 10000L;
    public static final String GET_BIT_FIELD_OP = "get";
    public static final String SET_BIT_FIELD_OP = "set";
    public static final String UNSIGNED_BIT = "u1";
    private static final String EL_TEACHERS_SUPER_CODER_KEY = "EARLY_LEARNING_TEACHERS_SUPER_CODER";
    private static final String EL_TEACHERS_SUPER_READER_KEY = "EARLY_LEARNING_TEACHERS_SUPER_READER";

    private Logger logger = LogFactory.getLogger(RedisDAO.class);
    private Gson gson = new Gson();

    public RedisDAO() {
    }

    public String getUserHomepageAccessBucketKey(Long userId) {
        return "NEW_HOMEPAGE_BUCKET_50_PERCENT:" + (userId / MAX_USERS_PER_BITMAP_KEY);
    }

    public long getUserHomepageAccessBucketOffset(Long userId) {
        // user indexed at bits 2n, 2n + 1  where n is the offset of user in the bucket key
        return 2 * (userId % MAX_USERS_PER_BITMAP_KEY);
    }

    public List<Long> getHomepageAccessInfo(final Long userId) {
        final String userBucketKey = getUserHomepageAccessBucketKey(userId);
        final long userBitOffset = getUserHomepageAccessBucketOffset(userId);
        final String[] ops = {
                GET_BIT_FIELD_OP, UNSIGNED_BIT, String.valueOf(userBitOffset),
                GET_BIT_FIELD_OP, UNSIGNED_BIT, String.valueOf(userBitOffset + 1)
        };

        return bitfield(userBucketKey, ops);
    }

    public void setHomepageAccessInfo(final Long userId, String allowedAccess) {
        final String userBucketKey = getUserHomepageAccessBucketKey(userId);
        final long userBitOffset = getUserHomepageAccessBucketOffset(userId);
        final String[] ops = {
                SET_BIT_FIELD_OP, UNSIGNED_BIT, String.valueOf(userBitOffset), "1",
                SET_BIT_FIELD_OP, UNSIGNED_BIT, String.valueOf(userBitOffset + 1), allowedAccess
        };

        bitfield(userBucketKey, ops);
    }

    public void evictELTeachersKeys() {

        List<String> proficiencyKeys = new ArrayList<>();

        try {
            deleteKeys(Arrays.asList(EL_TEACHERS_SUPER_CODER_KEY, EL_TEACHERS_SUPER_READER_KEY).toArray(new String[]{}));
        } catch (InternalServerErrorException e) {
            logger.warn("Error while deleting keys : early learning teacher keys");
        }

        EnumSet.allOf(TeacherCategory.ProficiencyType.class).forEach(proficiencyType -> {
            String superCoderKey = ELTeachersProficiencies.getELRedisKey(SessionLabel.SUPER_CODER, proficiencyType);
            String superReaderKey = ELTeachersProficiencies.getELRedisKey(SessionLabel.SUPER_READER, proficiencyType);
            proficiencyKeys.add(superCoderKey);
            proficiencyKeys.add(superReaderKey);
        });

        // set and orchestrated in scheduling
        awsSNSManager.triggerSNS(SNSTopic.SCHEDULING_REDIS_OPS, SNSSubject.EVICT_EL_TEACHERS.name(), gson.toJson(proficiencyKeys));
    }

}
