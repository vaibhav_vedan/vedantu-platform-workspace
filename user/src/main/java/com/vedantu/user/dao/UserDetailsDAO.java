package com.vedantu.user.dao;

import static org.springframework.data.mongodb.core.aggregation.Aggregation.group;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.newAggregation;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.project;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import com.vedantu.user.requests.GetVoltUserDetailsReq;
import org.apache.logging.log4j.Logger;
import org.elasticsearch.common.recycler.Recycler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.vedantu.lms.cmds.pojo.AttemptedUserIds;
import com.vedantu.user.entity.UserDetails;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.CustomValidator;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import org.bson.Document;
import org.springframework.data.mongodb.core.aggregation.AggregationOptions;

@Service
public class UserDetailsDAO extends AbstractMongoDAO {

    @Autowired
    private LogFactory logFactory;

    @Autowired
    private CustomValidator validator;

    private final Logger logger = logFactory.getLogger(UserDetailsDAO.class);

    @Autowired
    private MongoClientFactory mongoClientFactory;

    @Autowired
    private RedisDAO redisDAO;

    private final Gson gson = new Gson();

    
    public static String env = ConfigUtils.INSTANCE.getStringValue("environment");
    public static Integer redisExpiry = ConfigUtils.INSTANCE.getIntValue("redis.setExpiry");
    
    public UserDetailsDAO() {
        super();
    }

    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }

    public void save(UserDetails p, Long callingUserId) {
        if (p != null) {
            String callingUserIdString = null;
            if (callingUserId != null) {
                callingUserIdString = callingUserId.toString();
            }
            saveEntity(p, callingUserIdString);
            if (p.getUserId() != null) {
                try {
                    String key = getUserDetailsKey(p.getUserId(),p.getEvent());
                    redisDAO.setex(key, gson.toJson(p),redisExpiry);
                } catch (Exception e) {
                    logger.error("Error in redis set " + e.getMessage());
                }
            }
        }
    }
    public UserDetails getUserDetailsByUserId(Long userId, String event) {
        return getUserDetailsByUserId(userId, event, null);
    }

    public UserDetails getUserDetailsByUserId(Long userId, String event, String category) {
        UserDetails user = null;
        if (userId != null) {
            String key = getUserDetailsKey(userId,event);
            try {
                String value = redisDAO.get(key);
                if (StringUtils.isNotEmpty(value)) {
                    UserDetails userDetails = gson.fromJson(value, UserDetails.class);
                    if (userDetails != null) {
                        return userDetails;
                    }
                }
            } catch (Exception e) {
                logger.error("Error in redis get " + e.getMessage());
            }
            Query query = new Query();
            query.addCriteria(Criteria.where(UserDetails.Constants.USER_ID).is(userId));
            if(StringUtils.isNotEmpty(event)) {
                query.addCriteria(Criteria.where(UserDetails.Constants.EVENT).is(event));
            }
            if (StringUtils.isNotEmpty(category)) {
                query.addCriteria(Criteria.where(UserDetails.Constants.CATEGORY).is(category));
            }
            List<UserDetails> results = runQuery(query, UserDetails.class);
            if (ArrayUtils.isNotEmpty(results)) {
                user = results.get(0);
            }
            if (user != null) {
                try {
                    redisDAO.setex(key, gson.toJson(user), redisExpiry);
                } catch (Exception e) {
                    logger.error("Error in redis set " + e.getMessage());
                }
            }
        }
        return user;
    }

    public List<UserDetails> getUserDetailsByUserIds(List<Long> userIds, String event) {
        List<UserDetails> users = null;
        if (ArrayUtils.isNotEmpty(userIds)) {
            Query query;
            query = new Query();
            query.addCriteria(Criteria.where(UserDetails.Constants.USER_ID).in(userIds));
            query.addCriteria(Criteria.where(UserDetails.Constants.EVENT).is(event));
            users = runQuery(query, UserDetails.class);

        }
        return users;
    }

    public List<UserDetails> getUserDetails(String nameQuery, Integer start, Integer size, Long fromTime, Long tillTime) {
        Query query = new Query();

        logger.info("getUserDetails nameQuery:" + nameQuery
                + ", start:" + start + ", size:" + size);

        List<UserDetails> userDetails = null;

        List<Sort.Order> orderList = new ArrayList<>();

        if (!StringUtils.isEmpty(nameQuery)) {
            if (validator.validEmail(nameQuery)) {
                query.addCriteria(new Criteria().orOperator(Criteria.where(UserDetails.Constants.PARENT_EMAIL).is(nameQuery),
                        Criteria.where(UserDetails.Constants.PARENT_EMAIL).is(nameQuery)));
            } else {
                int length = nameQuery.length();
                int index = length < 30 ? length : 30;
                query.addCriteria(Criteria.where(UserDetails.Constants.STUDENT_NAME).regex(Pattern.quote(nameQuery.substring(0, index)), "i"));
                orderList.add(new Sort.Order(Sort.Direction.ASC, UserDetails.Constants.STUDENT_NAME));
            }

        } else if ((fromTime != null && fromTime > 0)
                || (tillTime != null && tillTime > 0)) {

            Criteria criteria = null;
            if (fromTime != null && fromTime > 0) {
                criteria = Criteria.where(UserDetails.Constants.CREATION_TIME).gte(fromTime);
            }

            if (tillTime != null && tillTime > 0) {
                if (criteria != null) {
                    criteria = criteria.andOperator(Criteria.where(UserDetails.Constants.CREATION_TIME).lt(tillTime));
                } else {
                    criteria = Criteria.where(UserDetails.Constants.CREATION_TIME).lt(tillTime);
                }
            }
            query.addCriteria(criteria);
        }

        orderList.add(new Sort.Order(Sort.Direction.DESC, UserDetails.Constants.CREATION_TIME));
        query.with(Sort.by(orderList));

        setFetchParameters(query, start, size);

        userDetails = runQuery(query, UserDetails.class);

        return userDetails;
    }

    public List<AttemptedUserIds> getUsersRegisteredForISL() {
        AggregationOptions aggregationOptions=Aggregation.newAggregationOptions().cursor(new Document()).build();
        Aggregation agg = newAggregation(group(UserDetails.Constants.CATEGORY)
                .addToSet(UserDetails.Constants.USER_ID).as("attemptedUserIds"),
                project("attemptedUserIds").and(UserDetails.Constants.CATEGORY).previousOperation()).withOptions(aggregationOptions);

        AggregationResults<AttemptedUserIds> groupResults
                = getMongoOperations().aggregate(agg, UserDetails.class.getSimpleName(), AttemptedUserIds.class);

        List<AttemptedUserIds> result = groupResults.getMappedResults();
        return result;
    }
    
    public String getUserDetailsKey(Long id, String event){
    	return env + "_USER_DETAILS_" + id + "_EVENT_"+event;
    }

    public List<UserDetails> getByVoltId(String voltId) {
        if (StringUtils.isEmpty(voltId)) {
            return new ArrayList<>();
        }
        Query query = new Query();
        query.addCriteria(Criteria.where(UserDetails.Constants.VOLT_ID).is(voltId));

        return runQuery(query, UserDetails.class);
    }


    public List<UserDetails> getUserDetailsForVolt(GetVoltUserDetailsReq req, boolean sortAsc) {
        Query query = new Query();

        query.addCriteria(Criteria.where(UserDetails.Constants.EVENT).is("VOLT_2020_JAN"));
        if (StringUtils.isNotEmpty(req.getFilterText())) {
            query.addCriteria(new Criteria().orOperator(Criteria.where(UserDetails.Constants.EMAIL).is(req.getFilterText()),
                    Criteria.where(UserDetails.Constants.STUDENT_PHONE_NO).is(req.getFilterText()),
                    Criteria.where(UserDetails.Constants.STUDENT_NAME).is(req.getFilterText()))
            );
        }

        if (StringUtils.isNotEmpty(req.getCity()) && StringUtils.isEmpty(req.getSchool())) {
            query.addCriteria(Criteria.where(UserDetails.Constants.CITY).is(req.getCity()));
        }

        if (StringUtils.isNotEmpty(req.getState())) {
            query.addCriteria(Criteria.where(UserDetails.Constants.STATE).is(req.getState()));
        }

        if (StringUtils.isNotEmpty(req.getSchool())) {
            query.addCriteria(Criteria.where(UserDetails.Constants.SCHOOL).is(req.getSchool()));
        }

        if (req.getStatus() != null) {
            query.addCriteria(Criteria.where(UserDetails.Constants.VOLT_STATUS).is(req.getStatus()));
        }

        setFetchParameters(query, req.getStart(), req.getSize());
        if (sortAsc) {
            query.with(Sort.by(Sort.Direction.ASC, UserDetails.Constants.CREATION_TIME));
        } else {
            query.with(Sort.by(Sort.Direction.DESC, UserDetails.Constants.CREATION_TIME));
        }

        logger.info("QUERY :" + query);
        return runQuery(query, UserDetails.class);
    }


    public List<UserDetails> getUserDetailForReviseJee(Long afterTime, Long beforeTime, String reviseJee2020March) {
        Query query = new Query();
        Criteria andCriteria = new Criteria().andOperator(Criteria.where(UserDetails.Constants.CREATION_TIME).gt(afterTime),
                Criteria.where(UserDetails.Constants.CREATION_TIME).lt(beforeTime));
        query.addCriteria(andCriteria);
        query.addCriteria(Criteria.where(UserDetails.Constants.EVENT).is("REVISE_JEE_2020_MARCH"));
        query.addCriteria(Criteria.where("testAttempted").exists(false));
        logger.info("Reminder List Query : " + query);
        return runQuery(query, UserDetails.class);
    }
}
