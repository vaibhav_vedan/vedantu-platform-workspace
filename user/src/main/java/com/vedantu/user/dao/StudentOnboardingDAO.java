package com.vedantu.user.dao;

import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Stream;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

import com.vedantu.User.LocationInfo;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.user.entity.StudentOnboarding;
import com.vedantu.user.enums.OnboardingCompletionStatus;
import com.vedantu.user.pojo.onboarding.SAMIntroductionInfo;
import com.vedantu.user.pojo.onboarding.SalesVerificationInfo;
import com.vedantu.user.requests.SalesVerificationAndSAMInformationRequest;
import com.vedantu.user.responses.StudentOnboardingCompletionStatusResp;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import com.vedantu.util.pojo.DeviceDetails;
import com.vedantu.util.security.HttpSessionUtils;


@Repository
public class StudentOnboardingDAO extends AbstractMongoDAO {
    @Autowired
    private LogFactory logFactory;

    @Autowired
    private MongoClientFactory mongoClientFactory;

    @Autowired
    private HttpSessionUtils httpSessionUtils;

    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }

    private final Logger logger = logFactory.getLogger(StudentOnboardingDAO.class);

    public StudentOnboarding getOnboardingDataUsingStudentId(Long studentId) {
        return getOnboardingDataUsingStudentId(studentId,null);
    }
    public StudentOnboarding getOnboardingDataUsingStudentId(Long studentId, List<String> includeFields) {
        Query query=new Query();
        query.addCriteria(Criteria.where(StudentOnboarding.Constants.STUDENT_ID).is(studentId));
        query.addCriteria(Criteria.where(StudentOnboarding.Constants.COMPLETION_STATUS).ne(OnboardingCompletionStatus.COMPLETED));

        query.fields().exclude(StudentOnboarding.Constants._ID);

        Optional.ofNullable(includeFields)
                .map(Collection::stream)
                .orElseGet(Stream::empty)
                .forEach(query.fields()::include);

        logger.info("getOnboardingDataUsingStudentId - query - {}",query);

        return findOne(query,StudentOnboarding.class);
    }

    public StudentOnboarding getOnBoardingDataUsingStudentAndBundleId(Long studentId, String bundleId) throws BadRequestException {

        if(Objects.isNull(studentId) || StringUtils.isEmpty(bundleId)){
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR,"[studentId] or [bundleId] cann't be null in their values");
        }

        Query query=new Query();
        query.addCriteria(Criteria.where(StudentOnboarding.Constants.STUDENT_ID).is(studentId));
        query.addCriteria(Criteria.where(StudentOnboarding.Constants.BUNDLE_ID).is(bundleId));

        return findOne(query,StudentOnboarding.class);
    }

    public void save(StudentOnboarding studentOnboarding) {
        Query query=new Query();
        query.addCriteria(Criteria.where(StudentOnboarding.Constants.STUDENT_ID).is(studentOnboarding.getStudentId()));
        query.fields().include(StudentOnboarding.Constants._ID);
        StudentOnboarding existingOnboarding=findOne(query,StudentOnboarding.class);
        if(Objects.isNull(existingOnboarding)) {
            saveEntity(studentOnboarding);
        }
    }

    public void updateRequiredData(Update update, Long studentId, String bundleId) throws BadRequestException {

        if(Objects.isNull(update) || Objects.isNull(studentId) || StringUtils.isEmpty(bundleId)){
            throw new BadRequestException(ErrorCode.MISSING_PARAMETER, "Either [update] or [studentId] or [bundleId] has" +
                    " a missing or invalid value");
        }

        Query query=new Query();
        query.addCriteria(Criteria.where(StudentOnboarding.Constants.STUDENT_ID).is(studentId));
        query.addCriteria(Criteria.where(StudentOnboarding.Constants.BUNDLE_ID).is(bundleId));

        logger.info("updateRequiredData - query - {}",query);
        logger.info("updateRequiredData - update - {}",update);

        updateFirst(query,update,StudentOnboarding.class);
    }

    public List<StudentOnboarding> getOnboardingData(SalesVerificationAndSAMInformationRequest salesVerificationAndSAMInformationRequest) {
        Query query=new Query();

        if(Objects.nonNull(salesVerificationAndSAMInformationRequest.getStudentId())) {
            query.addCriteria(Criteria.where(StudentOnboarding.Constants.STUDENT_ID).is(salesVerificationAndSAMInformationRequest.getStudentId()));
        }

        if(StringUtils.isNotEmpty(salesVerificationAndSAMInformationRequest.getStudentGrade())){
            query.addCriteria(Criteria.where(StudentOnboarding.Constants.STUDENT_GRADE).is(salesVerificationAndSAMInformationRequest.getStudentGrade()));
        }

        if(StringUtils.isNotEmpty(salesVerificationAndSAMInformationRequest.getBundleId())) {
            query.addCriteria(Criteria.where(StudentOnboarding.Constants.BUNDLE_ID).is(salesVerificationAndSAMInformationRequest.getBundleId()));
        }

        if(Objects.nonNull(salesVerificationAndSAMInformationRequest.getSalesVerificationStatus())){
            query.addCriteria(Criteria.where(StudentOnboarding.Constants.SALES_VERIFICATION_STATUS).is(salesVerificationAndSAMInformationRequest.getSalesVerificationStatus()));
        }

        if(Objects.nonNull(salesVerificationAndSAMInformationRequest.getSalesVerifierId())){
            query.addCriteria(Criteria.where(StudentOnboarding.Constants.SALES_VERIFIER_ID).is(salesVerificationAndSAMInformationRequest.getSalesVerifierId()));
        }

        if(Objects.nonNull(salesVerificationAndSAMInformationRequest.getIdOfSAM())){
            query.addCriteria(Criteria.where(StudentOnboarding.Constants.ID_OF_SAM).is(salesVerificationAndSAMInformationRequest.getIdOfSAM()));
        }

        if(Objects.nonNull(salesVerificationAndSAMInformationRequest.getSamIntroductionStatus())){
            query.addCriteria(Criteria.where(StudentOnboarding.Constants.SAM_INTRODUCTION_STATUS).is(salesVerificationAndSAMInformationRequest.getSamIntroductionStatus()));
        }

        if(salesVerificationAndSAMInformationRequest.isSortAccordingToLastActivity()){
            query.with(Sort.by(Sort.Direction.DESC, StudentOnboarding.Constants.LAST_UPDATED));
        }else{
            query.with(Sort.by(Sort.Direction.DESC, StudentOnboarding.Constants.CREATION_TIME));
        }


        if(Objects.nonNull(salesVerificationAndSAMInformationRequest.getStart())){
            query.skip(salesVerificationAndSAMInformationRequest.getStart());
        }else{
            query.skip(0);
        }

        query.limit(salesVerificationAndSAMInformationRequest.getSize());

        logger.info("getOnboardingData - query - {}",query);

        return runQuery(query,StudentOnboarding.class);
    }

    public StudentOnboarding getExistingOnboardingDataForSalesVerification(String onboardingId) throws BadRequestException {
        if(StringUtils.isEmpty(onboardingId)){
            throw new BadRequestException(ErrorCode.MISSING_PARAMETER,"[onboardingId] is empty", Level.INFO);
        }
        StudentOnboarding studentOnboarding = getEntityById(onboardingId, StudentOnboarding.class);
        if(Objects.isNull(studentOnboarding)){
            throw new BadRequestException(ErrorCode.INCORRECT_ID,onboardingId+" is an invalid id for getting student onboarding data",Level.ERROR);
        }
        return studentOnboarding;
    }

    public void updateSalesVerificationInfo(String onboardingId, SalesVerificationInfo salesVerificationInfo, Long callingUserId) {
        Query query=new Query();
        query.addCriteria(Criteria.where(StudentOnboarding.Constants._ID).is(onboardingId));

        Update update=new Update();
        update.set(StudentOnboarding.Constants.SALES_VERIFICATION_INFO,salesVerificationInfo);
        update.set(StudentOnboarding.Constants.LAST_UPDATED,System.currentTimeMillis());
        update.set(StudentOnboarding.Constants.LAST_UPDATED_BY,callingUserId);

        updateFirst(query,update,StudentOnboarding.class);
    }

    public void updateSAMIntroductionInfo(String onboardingId, SAMIntroductionInfo samIntroductionInfo, Long callingUserId) {
        Query query=new Query();
        query.addCriteria(Criteria.where(StudentOnboarding.Constants._ID).is(onboardingId));

        Update update=new Update();
        update.set(StudentOnboarding.Constants.SAM_INTRODUCTION_INFO,samIntroductionInfo);
        update.set(StudentOnboarding.Constants.LAST_UPDATED,System.currentTimeMillis());
        update.set(StudentOnboarding.Constants.LAST_UPDATED_BY,callingUserId);

        updateFirst(query,update,StudentOnboarding.class);
    }

    public StudentOnboarding getDataForUserId(Long currentUser) {
        Query query=new Query();
        query.addCriteria(Criteria.where(StudentOnboarding.Constants.STUDENT_ID).is(currentUser));

        return findOne(query,StudentOnboarding.class);
    }

    public void setOnboardingStateAsStarted(Long currentUser) {
        Query query=new Query();
        query.addCriteria(Criteria.where(StudentOnboarding.Constants.STUDENT_ID).is(currentUser));

        Update update=new Update();
        update.set(StudentOnboarding.Constants.COMPLETION_STATUS,OnboardingCompletionStatus.STARTED);
        update.set(StudentOnboarding.Constants.LAST_UPDATED,System.currentTimeMillis());
        update.set(StudentOnboarding.Constants.LAST_UPDATED_BY,httpSessionUtils.getCallingUserId());

        updateFirst(query,update,StudentOnboarding.class);
    }

    public List<StudentOnboarding> getOnboardingDataForDeviceDataAndLocationMigration() {

        Query query=new Query();
        query.addCriteria(Criteria.where(StudentOnboarding.Constants.DEVICE_DETAILS).is(null));
        query.addCriteria(Criteria.where(StudentOnboarding.Constants.LOCATION_INFO).is(null));

        query.fields().include(StudentOnboarding.Constants.USER_AGENT);
        query.fields().include(StudentOnboarding.Constants.IP_ADDRESS);

        query.limit(1000);

        return runQuery(query,StudentOnboarding.class);
    }

    public void updateDeviceAndLocationData(String id, DeviceDetails deviceDetails, LocationInfo locationInfo) {

        if(StringUtils.isEmpty(id) || Objects.isNull(deviceDetails) || Objects.isNull(locationInfo)){
            return;
        }

        logger.info("Updating for id - {}, with device details - {} and locationInfo - {}",id,deviceDetails,locationInfo);

        Query query=new Query();
        query.addCriteria(Criteria.where(StudentOnboarding.Constants._ID).is(id));

        Update update=new Update();
        if(null != locationInfo) {
            update.set(StudentOnboarding.Constants.LOCATION_INFO, locationInfo);
        }
        update.set(StudentOnboarding.Constants.DEVICE_DETAILS,deviceDetails);
        update.set(StudentOnboarding.Constants.LAST_UPDATED,System.currentTimeMillis());

        updateFirst(query,update,StudentOnboarding.class);
    }
    
	public StudentOnboardingCompletionStatusResp getCompletionStatus(Long userId) {
		StudentOnboardingCompletionStatusResp response = new StudentOnboardingCompletionStatusResp();
		response.setUserId(userId.toString());
		Query query=new Query();
        query.addCriteria(Criteria.where(StudentOnboarding.Constants.STUDENT_ID).is(userId));
        query.fields().include(StudentOnboarding.Constants.COMPLETION_STATUS);
        logger.debug("Get Completion Status for userID - {}, query - {}", userId, query);
        StudentOnboarding studentOnboarding = findOne(query, StudentOnboarding.class);
        logger.debug("studentOnboarding = "+studentOnboarding);
        if(null!= studentOnboarding && null != studentOnboarding.getCompletionStatus()) {
        	response.setStatus(studentOnboarding.getCompletionStatus());
        }
        logger.debug("StudentOnboardingCompletionStatusResp response ="+response);
		return response;
	}
}
