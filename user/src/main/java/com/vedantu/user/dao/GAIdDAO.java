package com.vedantu.user.dao;


import com.vedantu.user.entity.GAId;
import com.vedantu.user.responses.GAIdResponse;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import java.lang.reflect.Field;
import java.util.*;

/**
 * @author Ausaf
 */

@Service
public class GAIdDAO extends AbstractMongoDAO
{
    @Autowired
    MongoClientFactory mongoClientFactory;

    @Autowired
    private LogFactory logFactory;

    private final Logger logger = logFactory.getLogger(GAIdDAO.class);

    public GAIdDAO() { super(); }

    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }

    public void save(GAId gaId) {
        if (gaId != null)
            saveEntity(gaId);
    }

    public GAId findByUserId(String userId, Set<String> includeFields)
    {
        if (!StringUtils.isEmpty(userId))
        {
            Query query = new Query();
            query.addCriteria(Criteria.where(GAId.Constants.USER_ID).is(userId));
            if (includeFields != null && !includeFields.isEmpty())
                setProjection(query, includeFields);
            logger.info("GAID Query : {}", query);
            GAId gaId = findOne(query, GAId.class);
            logger.info("GA Id : {}", gaId);
            if (gaId != null)
                return gaId;
        }
        return null;
    }

    public void setProjection(Query query, Set<String> includeFields)
    {
        logger.info("Setting projection for fields : {}", includeFields);
        if (includeFields != null && !includeFields.isEmpty())
        {
            String fieldName;
            for (Field field : GAId.class.getDeclaredFields())
            {
                fieldName = field.getName();
                if (includeFields.contains(fieldName))
                    query.fields().include(fieldName);
            }
        }
    }

	public GAId findByUserIdAndGaid(String userId, String gaid) {
		
		Query query = new Query();
		if(StringUtils.isNotEmpty(gaid)) {
			query.addCriteria(Criteria.where(GAId.Constants.GAIDS).is(gaid));
		}
		if (StringUtils.isNotEmpty(userId))
        {
           query.addCriteria(Criteria.where(GAId.Constants.USER_ID).is(userId));
        }
		query.fields().include(GAId.Constants.CREATION_TIME);
		query.fields().include(GAId.Constants.LAST_UPDATED);
		query.fields().include(GAId.Constants.GAIDS);
		query.fields().include(GAId.Constants.USER_ID);
		
		
		return findOne(query, GAId.class);
	}
}
