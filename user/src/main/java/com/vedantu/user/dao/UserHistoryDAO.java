/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.user.dao;

import com.vedantu.user.entity.UserHistoryII;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.stereotype.Service;

/**
 *
 * @author jeet
 */
@Service
public class UserHistoryDAO extends AbstractMongoDAO {
    
    @Autowired
    private MongoClientFactory mongoClientFactory;
    
    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }
    
    public void create(UserHistoryII p, Long callingUserId) {
        String callingUserIdString = (callingUserId==null)? null : callingUserId.toString();
        saveEntity(p, callingUserIdString);
    }
}
