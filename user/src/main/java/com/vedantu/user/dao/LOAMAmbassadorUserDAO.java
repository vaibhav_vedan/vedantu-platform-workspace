package com.vedantu.user.dao;

import com.vedantu.User.Role;
import com.vedantu.User.enums.SubRole;
import com.vedantu.user.entity.User;
import com.vedantu.util.LogFactory;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;

/*
@Author: Sanket Jain
@Reviewer: Arun Dhwaj
@Reviewer_comment: Seems ok.
*/

@Service
public class LOAMAmbassadorUserDAO extends AbstractMongoDAO
{
    @Autowired
    private MongoClientFactory mongoClientFactory;

    @Autowired
    private LogFactory logFactory;

    private final Logger logger = logFactory.getLogger(LOAMAmbassadorUserDAO.class);

    public List<User> loam_getUsersById(List<Long> userIds, Set<String> fields, boolean isInclude)
    {
        if (userIds == null)
        {
            return null;
        }

        Query query;
        query = new Query();
        query.addCriteria(Criteria.where(User.Constants.ID).in(userIds));
        loam_manageFieldsInclude(query,fields,isInclude);
        return runQuery(query, User.class);
    }

    public User loam_getUserByEmail(String email, Set<String> fields, boolean isInclude) {
        User user = null;
        Query query;
        query = new Query();
        query.addCriteria(Criteria.where(User.Constants.EMAIL).is(email.toLowerCase()));
        loam_manageFieldsInclude(query,fields,isInclude);
        List<User> results = runQuery(query, User.class);
        if (null != results && !results.isEmpty()) {
            user = results.get(0);
        }
        return user;
    }

    public List<User> loam_getAcadMentorUsers(Set<String> fields, boolean isInclude)
    {
        Query query;
        query = new Query();
        Criteria andCriteria = new Criteria().andOperator(Criteria.where(User.Constants.ROLE).is(Role.TEACHER),
                Criteria.where(User.Constants.DEX_SUB_ROLE).is(SubRole.ACADMENTOR),
                Criteria.where(User.Constants.EMAIL).ne(""));
        query.addCriteria(andCriteria);
        loam_manageFieldsInclude(query,fields,isInclude);
        return runQuery(query, User.class);
    }

    @Override
    protected MongoOperations getMongoOperations()
    {
        return mongoClientFactory.getMongoOperations();
    }

    private void loam_manageFieldsInclude(Query q, Set<String> includeKeySet, boolean isInclude)
    {
        if(includeKeySet !=null && !includeKeySet.isEmpty())
        {
            if(isInclude)
                includeKeySet.parallelStream().forEach(includeKey -> q.fields().include(includeKey));
            else
                includeKeySet.parallelStream().forEach(includeKey -> q.fields().exclude(includeKey));
        }
    }

    public List<User> loam_getDoubtResolverData(Long fromTime, Long thruTime,
                                                Integer start, Integer size,
                                                Set<String> fields, boolean isInclude)
    {
        Query query = new Query();

        Criteria andCriteria = new Criteria().andOperator(Criteria.where(User.Constants.LAST_UPDATED).gte(fromTime),
                Criteria.where(User.Constants.LAST_UPDATED).lte(thruTime));
        query.addCriteria(Criteria.where(User.Constants.ROLE).is(Role.TEACHER));
        query.addCriteria(andCriteria);
        loam_manageFieldsInclude(query, fields, isInclude);
        setFetchParameters(query, start, size);

        return runQuery(query, User.class);
    }
}
