package com.vedantu.user.dao;

import com.google.gson.Gson;
import com.vedantu.User.DexInfo;
import com.vedantu.User.FeatureSource;
import com.vedantu.User.Pojo.ELProficiencyUserPojo;
import com.vedantu.User.Pojo.TeacherProficiency;
import com.vedantu.User.Role;
import com.vedantu.User.StudentInfo;
import com.vedantu.User.TeacherInfo;
import com.vedantu.User.UserBasicInfo;
import com.vedantu.User.UserInfo;
import com.vedantu.User.enums.SubRole;
import com.vedantu.User.enums.TeacherCategoryType;
import com.vedantu.User.enums.TeacherPoolType;
import com.vedantu.User.request.GetDexTeachersReq;
import com.vedantu.async.AsyncTaskFactory;
import com.vedantu.async.AsyncTaskParams;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ConflictException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.InternalServerErrorException;
import com.vedantu.subscription.enums.EarlyLearningCourseType;
import com.vedantu.user.async.AsyncTaskName;
import com.vedantu.user.entity.MemberCount;
import com.vedantu.user.entity.User;
import com.vedantu.user.entity.VerificationTokenII;
import com.vedantu.user.managers.AwsSNSManager;
import com.vedantu.user.pojo.MemberCountPojo;
import com.vedantu.user.responses.GetDexAndTheirStatusRes;
import com.vedantu.user.responses.TeacherInfoMinimal;
import com.vedantu.user.utils.PojoUtils;
import com.vedantu.util.*;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.vedantu.util.enums.SNSTopic;
import com.vedantu.util.fos.request.ReqLimits;
import org.apache.logging.log4j.Logger;
import org.bson.Document;
import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.aggregation.*;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.TextCriteria;
import org.springframework.data.mongodb.core.query.TextQuery;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class UserDAO extends AbstractMongoDAO {

    @Autowired
    private LogFactory logFactory;

    private final Logger logger = logFactory.getLogger(UserDAO.class);

    @Autowired
    private CounterService counterService;

    @Autowired
    private CustomValidator validator;

    @Autowired
    private AsyncTaskFactory asyncTaskFactory;

    @Autowired
    private PojoUtils pojoUtils;

    @Autowired
    private MongoClientFactory mongoClientFactory;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    public RedisDAO redisDAO;

    @Autowired
    private DozerBeanMapper mapper;

    @Autowired
    private FosUtils fosUtils;

    @Autowired
    private AwsSNSManager awsSNSManager;

    public static String env = ConfigUtils.INSTANCE.getStringValue("environment");

    private final Gson gson = new Gson();

    public UserDAO() {
        super();
    }

    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }

    public void create(User p, Long callingUserId) throws ConflictException {
        //try {
        if (p != null) {
            String callingUserIdString = null;
            if (callingUserId != null) {
                callingUserIdString = callingUserId.toString();
            }
            if (StringUtils.isNotEmpty(p.getEmail()) && null != getUserByEmail(p.getEmail())) {
                throw new ConflictException(ErrorCode.USER_ALREADY_EXISTS, "User already exists with email[" + p.getEmail()
                        + "]");
            }

            String[] testingNumbers = ConfigUtils.INSTANCE.getStringValue("testing.mobileNumbers").split(",");
            List<String> testingNumbersList = new ArrayList<>();
            for (String testingNumber : testingNumbers) {
                testingNumbersList.add(testingNumber.trim());
            }
            logger.info("testingNumbersList" + testingNumbersList);

            if (!testingNumbersList.contains(p.getContactNumber()) && p.getContactNumber() != null) {
                List<User> usersByNumber = getUsersByPhoneNumberAndCode(p.getContactNumber(), p.getPhoneCode(), false);
                int maxAllowed = ConfigUtils.INSTANCE.getIntValue("user.maxAllowedAccountsWithSameNumber");
                if (null != usersByNumber && maxAllowed > 0 && usersByNumber.size() >= maxAllowed) {
                    throw new ConflictException(ErrorCode.CONTACT_NUMBER_EXISTS, "More than allowed users already exist with contact number");
                }
            }

            if (p.getRole().equals(Role.TEACHER)) {
                TeacherInfo teacherInfo = p.getTeacherInfo();
                teacherInfo.setPrimaryCallingNumberCode(this
                        .getPrimaryNumberCode());
                teacherInfo.setExtensionNumber(this.getExtensionNumber(p
                        .getTeacherInfo().getPrimaryCallingNumberCode()));
                p.setTeacherInfo(teacherInfo);
            }
            logger.info("Saving into db" + p);
            if (p.getId() == null || p.getId() == 0L) {
                p.setId(counterService.getNextSequence(User.class.getSimpleName()));
            }
            try {
                saveEntity(p, callingUserIdString);
            } catch (org.springframework.dao.DuplicateKeyException | com.mongodb.DuplicateKeyException e) {
                throw new ConflictException(ErrorCode.USER_ALREADY_EXISTS, "Phone Number already exists with [" + p.getContactNumber()
                        + "]");
            }
            // add member count - sns
            try {
                MemberCountPojo memberCountPojo = new MemberCountPojo();
                memberCountPojo.setRole(p.getRole());
                memberCountPojo.setEmailConfirmed(p.getIsEmailVerified());
                awsSNSManager.triggerSNS(SNSTopic.UPDATE_MEMBER_COUNT, SNSTopic.UPDATE_MEMBER_COUNT.name(), new Gson().toJson(memberCountPojo));
            }
            catch(Exception e){
                logger.error("Error in updating member count");
                }


            //Already migrated
            /*
                if (p.getRole().equals(Role.TEACHER)) {
                    //UserManager.INSTANCE.updateTeacherBoardMapping(p);
                    try {
                        CreateElasticSearchTask.INSTANCE.createTask(
                                ElasticSearchTask.ADD_TEACHER,
                                (new Gson().toJson(p)));
                        //UserManager.INSTANCE.updateTeacherBoardMapping(p);
                    } catch (Exception e) {
                        logger.error("Exception in Elastic search add teacher task or updateTeacherBoardMapping", e);
                    }
                }
             */
        }
        //} catch (Exception ex) {
        //    logger.error("Error in creation of user in db", ex);
        //}
    }

    public void update(User p, Long callingUserId) throws ConflictException {
        update(p, callingUserId, true);
    }

    public void update(User p, Long callingUserId, boolean updateLastEditedBy) throws ConflictException {
        if (p != null) {

            logger.info("USERUPDATETRIGGER");

            String callingUserIdString = null;
            if (callingUserId != null) {
                callingUserIdString = callingUserId.toString();
            }
            if (updateLastEditedBy) {
                p.setLastEditedBy(callingUserIdString);
            }
            try {
                saveEntity(p, callingUserIdString);
            } catch (org.springframework.dao.DuplicateKeyException | com.mongodb.DuplicateKeyException e) {
//                logger.info("User Already Exists for phone : " + p.getContactNumber());
                throw new ConflictException(ErrorCode.USER_ALREADY_EXISTS, "Phone Number already exists with [" + p.getContactNumber()
                        + "]");
            }

            if (Role.TEACHER.equals(p.getRole()) || Role.STUDENT.equals(p.getRole())) {
                Map<String, Object> payload = new HashMap<String, Object>();
                payload.put("user", p);
                AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.USER_UPDATION_TRIGGER, payload);
                asyncTaskFactory.executeTask(params);
            }
//                if (p.getRole().equals(Role.TEACHER)) {
//			try {
////				CreateElasticSearchTask.INSTANCE.createTask(
////						ElasticSearchTask.UPDATE_TEACHER_DATA,
////						(new Gson().toJson(user)));
//                        eSManager.updateTeacherData(p);
//			} catch (Exception e) {
//				logger.error(e);
//			}
//		}
        }
    }

    public void updateUserWithoutTrigger(User p) {
        if (p != null) {
            getMongoOperations().save(p, p.getClass().getSimpleName());
        }
    }

    /*
    public User getById(String id) {
        User User = null;
        try {
            if (!StringUtils.isEmpty(id)) {
                User = (User) getEntityById(id, User.class);
            }
        } catch (Exception ex) {
            User = null;
        }
        return User;
    }
     */
    public void updateFirst(Query q, Update u) {
        updateFirst(q, u, User.class);
    }

    public User getUserByUserId(Long userId) {
        User user = null;
        Query query;
        query = new Query();
        query.addCriteria(Criteria.where(User.Constants.ID).is(userId));
        List<User> results = runQuery(query, User.class);
        if (null != results && !results.isEmpty()) {
            user = results.get(0);
        }
        return user;
    }

    public List<User> runQueryUserInfo(Query query) {
        //List<UserInfo> userInfos = null;
        query.fields().include(User.Constants.PROFILE_ENABLED);
        query.fields().include(User.Constants.ID);
        query.fields().include(User.Constants.EMAIL);
        query.fields().include(User.Constants.CONTACT_NUMBER);
        query.fields().include(User.Constants.GENDER);
        query.fields().include(User.Constants.PHONE_CODE);
        query.fields().include(User.Constants.FIRST_NAME);
        query.fields().include(User.Constants.LAST_NAME);
        query.fields().include(User.Constants.FULL_NAME);
        query.fields().include(User.Constants.ROLE);
        //query.fields().include(User.Constants.PROFILE_PIC_ID);
        query.fields().include(User.Constants.PROFILE_PIC_PATH);
        query.fields().include(User.Constants.LANGUAGE_PREFS);
        query.fields().include(User.Constants.LOCATION_INFO);
        query.fields().include(User.Constants.SOCIAL_INFO);
        query.fields().include(User.Constants.TEACHER_INFO);
        query.fields().include(User.Constants.STUDENT_INFO);
        query.fields().include(User.Constants.IS_EMAIL_VERIFIED);
        query.fields().include(User.Constants.IS_CONTACT_NUMBER_VERIFIED);
        query.fields().include(User.Constants.CREATION_TIME);
        query.fields().include(User.Constants.UTM_CAMPAIGN);
        query.fields().include(User.Constants.UTM_SOURCE);
        query.fields().include(User.Constants.UTM_MEDIUM);
        query.fields().include(User.Constants.UTM_CONTENT);
        query.fields().include(User.Constants.UTM_TERM);
        query.fields().include(User.Constants.CHANNEL);
        query.fields().include(User.Constants.SIGNUP_URL);
        query.fields().include(User.Constants.SIGNUP_FEATURE);
        query.fields().include(User.Constants.PARENT_REGISTRATION);
        query.fields().include(User.Constants.REFERRAL_CODE);
        query.fields().include(User.Constants.TELEGRAM_LINKED);
        query.fields().include(User.Constants.ORG_ID);
        query.fields().include(User.Constants.TEACHER_INFO + "." + TeacherInfo.Constants.SLIDER_PIC_URL);

        List<User> users = runQuery(query, User.class);
        return users;
    }

    public List<User> runQueryUserBasicInfo(Query query, boolean exposeEmail) {
        //List<UserBasicInfo> userBasicInfos = null;

        query.fields().include(User.Constants.ID);
        query.fields().include(User.Constants.EMAIL);
        query.fields().include(User.Constants.CONTACT_NUMBER);
        query.fields().include(User.Constants.PHONE_CODE);
        query.fields().include(User.Constants.FIRST_NAME);
        query.fields().include(User.Constants.LAST_NAME);
        query.fields().include(User.Constants.FULL_NAME);
        query.fields().include(User.Constants.ROLE);
        query.fields().include(User.Constants.GENDER);
        query.fields().include(User.Constants.PROFILE_PIC_PATH);
        query.fields().include(User.Constants.ORG_ID);
        //TODO: Check if this is working
        query.fields().include(User.Constants.STUDENT_INFO + "." + StudentInfo.Constants.GRADE);
        query.fields().include(User.Constants.STUDENT_INFO + "." + StudentInfo.Constants.DEVICE_TYPE);
        query.fields().include(User.Constants.TEACHER_INFO + "." + TeacherInfo.Constants.PRIMARY_NUMBER_CODE);
        query.fields().include(User.Constants.TEACHER_INFO + "." + TeacherInfo.Constants.EXTENSION_NUMBER);
        query.fields().include(User.Constants.TEACHER_INFO + "." + TeacherInfo.Constants.DEX_INFO_TEACHER_POOL_TYPE);
        query.fields().include(User.Constants.TEACHER_INFO + "." + TeacherInfo.Constants.DEX_VIEWER_ACCESSES);

        query.fields().include(User.Constants.TEACHER_INFO + "." + TeacherInfo.Constants.SLIDER_PIC_URL);
        query.fields().include(User.Constants.TARGET);
        List<User> users = runQuery(query, User.class);
        return users;

    }

    public User getUserByEmail(String email) {
        User user = null;
        if (StringUtils.isEmpty(email)) {
            return null;
        }
        Query query = new Query();
        query.addCriteria(Criteria.where(User.Constants.EMAIL).is(email.toLowerCase()));
        List<User> results = runQuery(query, User.class);
        if (null != results && !results.isEmpty()) {
            user = results.get(0);
        }
        return user;
    }

    public List<User> getUsersByEmailOrPhone(String email, String contactNumber, String phoneCode) {
        Query query = new Query();
        Criteria criteria = new Criteria();
        Criteria emailCriteria = Criteria.where(User.Constants.EMAIL).is(email.toLowerCase());
        Criteria contactNumberCriteria = Criteria.where(User.Constants.CONTACT_NUMBER).is(contactNumber);
        Criteria phoneCodeCriteria = null;
        if (!StringUtils.isEmpty(phoneCode)) {
            String formattedPhoneCode = phoneCode.replace("+", "");
            phoneCodeCriteria = Criteria.where(VerificationTokenII.Constants.PHONE_CODE).is(formattedPhoneCode);
        }
        criteria.orOperator(emailCriteria, new Criteria().andOperator(contactNumberCriteria,phoneCodeCriteria));
        query.addCriteria(criteria);
        List<User> results = runQuery(query, User.class);
        return results;
    }

    public List<User> getUsersByEmail(String email) {
        Query query;
        query = new Query();
        query.addCriteria(Criteria.where(User.Constants.EMAIL).is(email.toLowerCase()));
        List<User> results = runQuery(query, User.class);

        return results;
    }

    public User getUserBasicInfoByEmail(String email, boolean exposeEmail) {
        User user = null;
        Query query;
        query = new Query();
        query.addCriteria(new Criteria().orOperator(Criteria.where(User.Constants.EMAIL).is(email),
                Criteria.where(User.Constants.EMAIL).is(email.toLowerCase())));
        List<User> results = runQueryUserBasicInfo(query, exposeEmail);
        if (null != results && !results.isEmpty()) {
            user = results.get(0);
        }
        return user;
    }

    public List<User> getUsersByPhoneNumberAndCode(String contactNumber, String phoneCode, Boolean preLoginVerification) {
        Query query;
        query = new Query();
        query.addCriteria(Criteria.where(User.Constants.CONTACT_NUMBER).is(contactNumber));
        if (!preLoginVerification) {
            query.addCriteria(Criteria.where(User.Constants.IS_CONTACT_NUMBER_VERIFIED).is(true));
        }
        if (!org.springframework.util.StringUtils.isEmpty(phoneCode)) {
            String formattedPhoneCode = phoneCode.replace("+", "");
            query.addCriteria(Criteria.where(VerificationTokenII.Constants.PHONE_CODE).is(formattedPhoneCode));
        }
        return runQuery(query, User.class);
    }

    // Modified for unique phone number for one user.
    public User getUserByPhoneNumberAndCode(String contactNumber, String phoneCode) throws BadRequestException {
        User user = null;
        Query query;
        query = new Query();
        query.addCriteria(Criteria.where(User.Constants.CONTACT_NUMBER).is(contactNumber));
        if (!StringUtils.isEmpty(phoneCode)) {
            String formattedPhoneCode = phoneCode.replace("+", "");
            query.addCriteria(Criteria.where(VerificationTokenII.Constants.PHONE_CODE).is(formattedPhoneCode));
        }

        List<User> results = runQuery(query, User.class);

        if (results.size() > 1) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Use a different Phone as it is already linked with multiple accounts");
        }

        if (null != results && !results.isEmpty()) {
            user = results.get(0);
        }
        return user;
    }

    public Integer getPrimaryNumberCode() {
        return 1;
    }

    public Integer getExtensionNumber(Integer primaryNumberCode) {
        Integer extensionValue = 1;
        try {
            if (primaryNumberCode != 0) {
                extensionValue = counterService.getNextSequence(TeacherInfo.Constants.EXTENSION_NUMBER, 1).intValue();
            }
        } catch (Exception e) {
            return extensionValue;
        }
        return extensionValue;
    }

    public User authenticateUser(Long userId, String password) {
        User user = null;
        Query query;
        query = new Query();
        query.addCriteria(Criteria.where(User.Constants.ID).is(userId));
        //query.addCriteria(Criteria.where(User.Constants.PASSWORD).is(password));

        List<User> results = runQuery(query, User.class);
        if (null != results && !results.isEmpty()) {
            user = results.get(0);
        }
        if (user != null && (passwordEncoder.matches(password, user.getPassword()) || password.equals(user.getPassword()))) {
            return user;
        }
        return null;
    }

    public User getUserInfoWithPhones(Long userId) {
        User user = null;
        Query query;
        query = new Query();
        query.addCriteria(Criteria.where(User.Constants.ID).is(userId));
        query.fields().include(User.Constants.PHONES);
        List<User> results = runQueryUserInfo(query);
        if (null != results && !results.isEmpty()) {
            user = results.get(0);
        }
        return user;
    }

    public User getUserInfo(Long userId) {
        User user = null;
        Query query;
        query = new Query();
        query.addCriteria(Criteria.where(User.Constants.ID).is(userId));
        List<User> results = runQueryUserInfo(query);
        if (null != results && !results.isEmpty()) {
            user = results.get(0);
        }
        return user;
    }

    public List<User> getEarlyLearningTeachersWithSearchString(String nameQuery, List<Long> userIds) {

        Query query = new Query();
        query.addCriteria(Criteria.where(User.Constants._ID).in(userIds));
        query.addCriteria(Criteria.where(User.Constants.ROLE).is(Role.TEACHER));

        if (!StringUtils.isEmpty(nameQuery)) {
            logger.info("NAMEQUERY: " + nameQuery);
            if (validator.validPhoneNumber(nameQuery)) {
                logger.info("INVALIDPHONENUMBER");
                query.addCriteria(Criteria.where(User.Constants.CONTACT_NUMBER).is(nameQuery));
            }  else if (validator.validEmail(nameQuery.trim())) {
                logger.info("INVALIDEMAIL");
                query.addCriteria(new Criteria().orOperator(Criteria.where(User.Constants.EMAIL).is(nameQuery),
                        Criteria.where(User.Constants.EMAIL).is(nameQuery.toLowerCase())));
            } else if (nameQuery.contains("+t@vedantu.com")) {
                query.addCriteria(new Criteria().orOperator(Criteria.where(User.Constants.EMAIL).is(nameQuery),
                        Criteria.where(User.Constants.EMAIL).is(nameQuery.toLowerCase())));
            } else {
                    int length = nameQuery.length();
                    int max = (length < ReqLimits.NAME_TYPE_MAX) ? length : ReqLimits.NAME_TYPE_MAX;
                    nameQuery = nameQuery.substring(0, max);
                    int index = length < 30 ? length : 30;
                    query.addCriteria(Criteria.where(User.Constants.FULL_NAME).regex(nameQuery.substring(0, index), "i"));
            }
        }

        query.with(Sort.by(Sort.Direction.DESC, User.Constants.CREATION_TIME));
        logger.info("query : "+query);
        return runQueryUserBasicInfo(query, true);
    }

    //TODO: Create an index on full name
    public List<User> getUsersInfo(Role role, SubRole subRole, String nameQuery, Integer start, Integer size, Long fromTime, Long tillTime,TeacherCategoryType teacherCategoryType) {
        Query query = new Query();

        if (!StringUtils.isEmpty(nameQuery)) {
            nameQuery = nameQuery.toLowerCase();
        }

        logger.info("getUsersInfo role:" + role + ", nameQuery:" + nameQuery
                + ", start:" + start + ", size:" + size);

        if (!StringUtils.isEmpty(nameQuery)) {
            logger.info("NAMEQUERY: " + nameQuery);
            if (validator.validPhoneNumber(nameQuery)) {
                logger.info("INVALIDPHONENUMBER");
                query.addCriteria(Criteria.where(User.Constants.CONTACT_NUMBER).is(nameQuery));
            } else if (validator.validUserId(nameQuery)) {
                logger.info("INVALIDUSERID");
                long id = Long.parseLong(nameQuery);
                query.addCriteria(Criteria.where(User.Constants.ID).is(id));
            } else if (validator.validEmail(nameQuery.trim())) {
                logger.info("INVALIDEMAIL");
                query.addCriteria(new Criteria().orOperator(Criteria.where(User.Constants.EMAIL).is(nameQuery),
                        Criteria.where(User.Constants.EMAIL).is(nameQuery.toLowerCase())));
            } else if (nameQuery.contains("+t@vedantu.com")) {
                query.addCriteria(new Criteria().orOperator(Criteria.where(User.Constants.EMAIL).is(nameQuery),
                        Criteria.where(User.Constants.EMAIL).is(nameQuery.toLowerCase())));
            } else {
                //reference: https://spring.io/blog/2014/07/17/text-search-your-documents-with-spring-data-mongodb
                TextCriteria textcriteria = TextCriteria.forDefaultLanguage();
//                        .matchingAny(nameQuery);
                String[] mustContain = nameQuery.split(" ");
                if (mustContain != null && mustContain.length > 0) {
                    List<String> mustContainList = Arrays.asList(mustContain);
                    if (mustContainList.size() > 4) {
                        mustContainList = mustContainList.subList(0, 4);
                    }
                    for (String ph : mustContainList) {
                        textcriteria.matchingPhrase(ph);
                    }
                }

                query = TextQuery.queryText(textcriteria)
                        .sortByScore(); //               .includeScore()
                //               .with(new PageRequest(0, limit));

//                orderList.add(new Sort.Order(Sort.Direction.ASC, User.Constants.FULL_NAME));
//                query.addCriteria(Criteria.where(User.Constants.FULL_NAME).regex(Pattern.quote(nameQuery), "i"));
            }

        } else if ((fromTime != null && fromTime > 0)
                || (tillTime != null && tillTime > 0)) {

            Criteria criteria = null;
            if (fromTime != null && fromTime > 0) {
                criteria = Criteria.where(User.Constants.CREATION_TIME).gte(fromTime);
            }

            if (tillTime != null && tillTime > 0) {
                if (criteria != null) {
                    criteria = criteria.andOperator(Criteria.where(User.Constants.CREATION_TIME).lt(tillTime));
                } else {
                    criteria = Criteria.where(User.Constants.CREATION_TIME).lt(tillTime);
                }
            }
            query.addCriteria(criteria);
        }

        if (role != null) {
            query.addCriteria(Criteria.where(User.Constants.ROLE).is(role));
        }

        if (subRole != null) {
            if (Role.TEACHER.equals(role)) {
                query.addCriteria(Criteria.where(User.Constants.DEX_SUB_ROLE).in(subRole));
            }
        }

        if (teacherCategoryType != null) {
            if (Role.TEACHER.equals(role)) {
                query.addCriteria(Criteria.where(User.Constants.TEACHER_CATEGORY_TYPE).is(teacherCategoryType));
            }
        }


        List<Sort.Order> orderList = new ArrayList<>();
        orderList.add(new Sort.Order(Sort.Direction.DESC, User.Constants.CREATION_TIME));
        query.with(Sort.by(orderList));

        setFetchParameters(query, start, size);

        return runQueryUserInfo(query);

    }

    public List<User> getNonStudentUsersInfo(String nameQuery, Integer start, Integer size) {
        Query query = new Query();

        if (!StringUtils.isEmpty(nameQuery)) {
            nameQuery = nameQuery.toLowerCase();
        }

        if (!StringUtils.isEmpty(nameQuery)) {
            logger.info("NAMEQUERY: " + nameQuery);
            if (validator.validPhoneNumber(nameQuery)) {
                logger.info("INVALIDPHONENUMBER");
                query.addCriteria(Criteria.where(User.Constants.CONTACT_NUMBER).is(nameQuery));
            } else if (validator.validUserId(nameQuery)) {
                logger.info("INVALIDUSERID");
                long id = Long.parseLong(nameQuery);
                query.addCriteria(Criteria.where(User.Constants.ID).is(id));
            } else if (validator.validEmail(nameQuery.trim())) {
                logger.info("INVALIDEMAIL");
                query.addCriteria(new Criteria().orOperator(Criteria.where(User.Constants.EMAIL).is(nameQuery),
                        Criteria.where(User.Constants.EMAIL).is(nameQuery.toLowerCase())));
            } else {
                //reference: https://spring.io/blog/2014/07/17/text-search-your-documents-with-spring-data-mongodb
                TextCriteria textcriteria = TextCriteria.forDefaultLanguage();
//                        .matchingAny(nameQuery);
                String[] mustContain = nameQuery.split(" ");
                if (mustContain != null && mustContain.length > 0) {
                    List<String> mustContainList = Arrays.asList(mustContain);
                    if (mustContainList.size() > 4) {
                        mustContainList = mustContainList.subList(0, 4);
                    }
                    for (String ph : mustContainList) {
                        textcriteria.matchingPhrase(ph);
                    }
                }

                query = TextQuery.queryText(textcriteria)
                        .sortByScore(); //               .includeScore()
                //               .with(new PageRequest(0, limit));

//                orderList.add(new Sort.Order(Sort.Direction.ASC, User.Constants.FULL_NAME));
//                query.addCriteria(Criteria.where(User.Constants.FULL_NAME).regex(Pattern.quote(nameQuery), "i"));
            }

        }

        query.addCriteria(Criteria.where(User.Constants.ROLE).nin(Arrays.asList(Role.STUDENT, Role.TEACHER)));

        List<Sort.Order> orderList = new ArrayList<>();
        orderList.add(new Sort.Order(Sort.Direction.DESC, User.Constants.CREATION_TIME));
        query.with(Sort.by(orderList));

        setFetchParameters(query, start, size);

        logger.info("query : " + query);
        return runQueryUserInfo(query);
    }

    public List<User> getUsersById(List<Long> userIds) {
        if (userIds == null) {
            return null;
        }
        Query query;
        query = new Query();
        query.addCriteria(Criteria.where(User.Constants.ID).in(userIds));
        //query.fields().include(User.Constants.PHONES);
        return runQuery(query, User.class);
    }

    public List<User> getUserByVerifiedContactNumber(String contactNumber) {
        Query query;
        query = new Query();
        query.addCriteria(Criteria.where(User.Constants.CONTACT_NUMBER).is(contactNumber));
//        query.addCriteria(Criteria.where(User.Constants.IS_CONTACT_NUMBER_VERIFIED).is(true));
        //query.fields().include(User.Constants.PHONES);
        query.with(Sort.by(Sort.Direction.DESC, User.Constants.CREATION_TIME));
        return runQuery(query, User.class);
    }

    public User getUserByContactNumberAndOTPPassword(String contactNumber) {
        Query query;
        query = new Query();
        query.addCriteria(Criteria.where(User.Constants.CONTACT_NUMBER).is(contactNumber));
        query.addCriteria(Criteria.where(User.Constants.OTP_PASSWORD).exists(true));
        return findOne(query, User.class);
    }

    public List<User> getUsersByContactNumber(String contactNumber) {
        Query query;
        query = new Query();
        query.addCriteria(Criteria.where(User.Constants.CONTACT_NUMBER).is(contactNumber));
        //query.fields().include(User.Constants.PHONES);
        return runQuery(query, User.class);
    }

    public List<User> getUsersByContactNumbers(List<String> contactNumbers) {
        Query query;
        query = new Query();
        query.addCriteria(Criteria.where(User.Constants.CONTACT_NUMBER).in(contactNumbers));
        //query.fields().include(User.Constants.PHONES);
        return runQuery(query, User.class);
    }

    public List<User> getUserBasicInfoByIds(List<Long> userIds, boolean exposeEmail) {
        if (userIds == null) {
            return null;
        }
        Query query;
        query = new Query();
        query.addCriteria(Criteria.where(User.Constants.ID).in(userIds));
        //query.fields().include(User.Constants.PHONES);
        return runQueryUserBasicInfo(query, exposeEmail);
    }

    public Role getRole(Long userId) {
        User user = null;
        Role role = null;
        Query query = new Query();
        query.addCriteria(Criteria.where(User.Constants.ID).is(userId));
        query.fields().include(User.Constants.ROLE);
        List<User> results = runQuery(query, User.class);
        if (null != results && !results.isEmpty()) {
            user = results.get(0);
        }
        if (user != null) {
            role = user.getRole();
        }
        return role;
    }

    public UserInfo getUserInfoByReferralCode(String referrerCode) {
        User user = null;
        Query query = new Query();
        query.addCriteria(Criteria.where(User.Constants.REFERRAL_CODE).is(referrerCode));
        List<User> results = runQueryUserInfo(query);
        if (null != results && !results.isEmpty()) {
            user = results.get(0);
        }
        if (user != null) {
            return new UserInfo(pojoUtils.convertToUserPojo(user), null, false);
        }
        return null;
    }

    public Long getMemberCount(Role role, Boolean verifiedCustomersOnly) {
        Query query = new Query();
        if (role != null) {
            query.addCriteria(Criteria.where(User.Constants.ROLE).is(role));
        }
        if (verifiedCustomersOnly != null) {
            query.addCriteria(Criteria.where(User.Constants.IS_EMAIL_VERIFIED).is(verifiedCustomersOnly));
        }
        MemberCount memberCount = findOne(query, MemberCount.class);
        if(memberCount != null){
            return memberCount.getCount();
        }
        return 0L;
    }
    public void incrementMemberCount(Role role, Boolean verifiedCustomersOnly) {
        Query query = new Query();
        if (role != null) {
            query.addCriteria(Criteria.where(User.Constants.ROLE).is(role));
        }
        if (verifiedCustomersOnly != null) {
            query.addCriteria(Criteria.where(User.Constants.IS_EMAIL_VERIFIED).is(verifiedCustomersOnly));
        }

        Update update = new Update();
        update.inc(MemberCount.Constants.COUNT, 1);
        updateMulti(query, update, MemberCount.class);
    }

    public void preStore(User user) {
        user.setFullName((user.getFirstName() + " " + StringUtils.defaultIfEmpty(user.getFullName())).trim());
        user.setNewUser(user.getId() == null);
    }

    public void postStore(User user) {
//        if (Role.STUDENT.equals(user.getRole())) {
//            try {
//                //LeadSquaredTask.createtask(LeadSquaredAction.POST_LEAD_DATA, LeadSquaredDataType.LEAD_UPSERT,
//                //        TaskName.POST_TO_LEADSQUARED, this.getId(), this, null);
//                leadSquaredManager.leadCreate(user.toUserPojo());
//            } catch (Exception ex) {
//                logger.error(ex);
//            }
//        }

        if (Role.STUDENT.equals(user.getRole())) {
            Map<String, Object> payload = new HashMap<>();
            payload.put("user", user);
            AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.USER_UPDATION_TRIGGER, payload);
            asyncTaskFactory.executeTask(params);
        }

    }

    protected void saveEntity(User p) {
        preStore(p);
        super.saveEntity(p);
        postStore(p);
    }

    public List<User> getTutorsWithProfilePic(Integer start, Integer size) {
        logger.info("getUsers", " start:" + start + ", size:" + size);

        Query query = new Query();
        query.addCriteria(Criteria.where(User.Constants.ROLE).is(Role.TEACHER));
        //query.getFetchPlan().setFetchSize(1000);

        query.addCriteria(Criteria.where(User.Constants.PROFILE_PIC_PATH).exists(true).nin("", null));

        List<Sort.Order> orderList = new ArrayList<>();
        orderList.add(new Sort.Order(Sort.Direction.DESC, User.Constants.CREATION_TIME));
        query.with(Sort.by(orderList));

        setFetchParameters(query, start, size);

        List<User> users = runQuery(query, User.class);

        logger.info("getTutorsWithProfilePic", users);
        return users;
    }

    public User getUserViaExtension(Integer primaryNumberCode, Integer extensionNumber) {
        if (primaryNumberCode == null || extensionNumber == null) {
            return null;
        }
        User user = null;
        Query query = new Query();
        query.addCriteria(Criteria.where("teacherInfo." + TeacherInfo.Constants.EXTENSION_NUMBER).is(extensionNumber));
        query.addCriteria(Criteria.where("teacherInfo." + TeacherInfo.Constants.PRIMARY_NUMBER_CODE).is(primaryNumberCode));
        List<User> results = runQuery(query, User.class);
        if (null != results && !results.isEmpty()) {
            user = results.get(0);
        }
        return user;
    }

    public List<User> getUsersByCreationTime(Integer start, Integer size) {
        logger.info("getUsers", " start:" + start + ", size:" + size);

        Query query = new Query();
        List<Sort.Order> orderList = new ArrayList<>();
        orderList.add(new Sort.Order(Sort.Direction.ASC, User.Constants.CREATION_TIME));
        query.with(Sort.by(orderList));

        setFetchParameters(query, start, size);

        List<User> users = runQuery(query, User.class);

        logger.info("getUsersByCreationTime", users);
        return users;
    }

    public List<User> getPasswordEmptyUsersForISL(Integer start, Integer size) {
        Query query = new Query();
        query.addCriteria(Criteria.where(User.Constants.SIGNUP_FEATURE)
                .is(FeatureSource.ISL_REGISTRATION_VIA_TOOLS));
        query.addCriteria(Criteria.where(User.Constants.PASSWORD)
                .exists(false));
        List<Sort.Order> orderList = new ArrayList<>();
        orderList.add(new Sort.Order(Sort.Direction.DESC, User.Constants.CREATION_TIME));
        query.with(Sort.by(orderList));

        setFetchParameters(query, start, size);

        List<User> users = runQueryUserInfo(query);

        return users;
    }

    public User getUserWithSkip(Integer skip) {
        Query query = new Query();
        query.skip(skip);
        query.with(Sort.by(Sort.Direction.ASC, User.Constants._ID));
        User user = findOne(query, User.class);
        return user;
    }

    public List<UserBasicInfo> getUserBasicInfoFromRedis(List<Long> ids) {

        List<UserBasicInfo> infoMap = new ArrayList<>();
        if (ArrayUtils.isNotEmpty(ids)) {
            List<String> keys = new ArrayList<>();
            for (Long id : ids) {
                if (id != null) {
                    keys.add(getUserBasicInfoKey(id));
                }
            }
            Map<String, String> valueMap = new HashMap<>();
            try {
                valueMap = redisDAO.getValuesForKeys(keys);
            } catch (Exception e) {
                // TODO Auto-generated catch block
                logger.info("Error in Redis getValuesForKeys");
            }

            if (valueMap != null && !valueMap.isEmpty()) {
                for (Long id : ids) {
                    if (id != null) {
                        String value = valueMap.get(getUserBasicInfoKey(id));
                        if (StringUtils.isNotEmpty(value)) {
                            UserBasicInfo userBasicInfo = gson.fromJson(value, UserBasicInfo.class);
                            infoMap.add(userBasicInfo);
                        }
                    }
                }
            }
        }

        return infoMap;
    }

    public List<User> getAdminsForPasswordReset() {
        Long passwordCheckTime = System.currentTimeMillis() - 30 * 86400000l;
        Query query = new Query();
        query.addCriteria(Criteria.where(User.Constants.PROFILE_ENABLED).is(true));
        query.addCriteria(Criteria.where(User.Constants.ROLE).in(Arrays.asList(Role.ADMIN, Role.STUDENT_CARE)));
        query.addCriteria(Criteria.where(User.Constants.PASSWORD_CHANGED_AT).lt(passwordCheckTime));
        return runQuery(query, User.class);
    }

    public List<User> getAllAcadMentors(Integer start, Integer size) {
        Query q = new Query();
        q.addCriteria(Criteria.where(User.Constants.ROLE).is("TEACHER"));
        q.addCriteria(Criteria.where("teacherInfo.subRole").is(SubRole.ACADMENTOR));
        setFetchParameters(q, start, size);
        return runQuery(q, User.class);
    }

    public List<User> getDexUsers(GetDexTeachersReq req) {
        Query query = prepareDexQuery(req);
        setFetchParameters(query, req.getStart(), 600);
        List<User> users = runQuery(query, User.class);
        return users;
    }

    public List<GetDexAndTheirStatusRes> getDexAndTheirStatus(GetDexTeachersReq req) {
        Query query = prepareDexQuery(req);
        logger.info("query" + query);
        query.fields().include(User.Constants.ID);
        query.fields().include(User.Constants.TEACHER_INFO + "." + TeacherInfo.Constants.DEX_INFO);
        setFetchParameters(query, req.getStart(), 600);
        List<User> users = runQuery(query, User.class);
        List<GetDexAndTheirStatusRes> res = new ArrayList<>();
        if (ArrayUtils.isNotEmpty(users)) {
            for (User user : users) {
                GetDexAndTheirStatusRes r = new GetDexAndTheirStatusRes();
                r.setUserId(user.getId());
                if (user.getTeacherInfo() != null && user.getTeacherInfo().getDexInfo() != null) {
                    r.setTeacherPoolType(user.getTeacherInfo().getDexInfo().getTeacherPoolType());
                    r.setDexAvailabilityStatus(user.getTeacherInfo().getDexInfo().getAvaiblabilityStatus());
                }
                res.add(r);
            }
        }
        return res;
    }

    private Query prepareDexQuery(GetDexTeachersReq req) {
        Query query = new Query();
        if (ArrayUtils.isNotEmpty(req.getMainTags())) {
            query.addCriteria(Criteria.where(User.Constants.TEACHER_INFO + "." + TeacherInfo.Constants.DEX_INFO + "." + DexInfo.Constants.MAIN_TAGS).all(req.getMainTags()));
        }

        if (req.getTeacherPoolType() != null) {
            query.addCriteria(Criteria.where(User.Constants.TEACHER_INFO + "." + TeacherInfo.Constants.DEX_INFO_TEACHER_POOL_TYPE).is(req.getTeacherPoolType()));
        } else {
            query.addCriteria(Criteria.where(User.Constants.TEACHER_INFO + "." + TeacherInfo.Constants.DEX_INFO_TEACHER_POOL_TYPE).in(Arrays.asList(TeacherPoolType.T1_POOL, TeacherPoolType.T2_POOL)));
        }

        if (ArrayUtils.isNotEmpty(req.getUserIds())) {
            query.addCriteria(Criteria.where(User.Constants.ID).in(req.getUserIds()));
        }
        return query;
    }

    public String getUserBasicInfoKey(Long id) {
        return env + "_USER_BASIC_INFO_" + id;
    }

    public static void main(String[] args) {
        String nameQuery = "Aih eddy gunda ajih asfkj jashfg kjhjhsg jhg kjhdg jh";
        TextCriteria textcriteria = TextCriteria.forDefaultLanguage();
//                        .matchingAny(nameQuery);
        String[] mustContain = nameQuery.split(" ");
        if (mustContain != null && mustContain.length > 0) {
            List<String> mustContainList = Arrays.asList(mustContain);
            if (mustContainList.size() > 4) {
                mustContainList = mustContainList.subList(0, 4);
            }
            for (String ph : mustContainList) {
                textcriteria.matchingPhrase(ph);
            }
        }

        Query query = TextQuery.queryText(textcriteria)
                .sortByScore();
        System.out.println("com.vedantu.user.dao.UserDAO.main()" + query);
    }

    public List<TeacherInfoMinimal> getBasicTeacherInfoByIds(List<Long> userIds) {
        Query query = new Query();
        query.addCriteria(Criteria.where(User.Constants.ROLE).is(Role.TEACHER));
        query.addCriteria(Criteria.where(User.Constants.ID).in(userIds));

        List<User> users = runQuery(query, User.class);
        List<TeacherInfoMinimal> teacherInfoMinimals = new ArrayList<>();

        for (User user : users) {
            TeacherInfoMinimal teacher = mapper.map(user, TeacherInfoMinimal.class);
            teacherInfoMinimals.add(teacher);
        }

        return teacherInfoMinimals;

    }

    public Long getIdByContactNumber(String phoneNumber) {
        Query query = new Query();
        query.addCriteria(Criteria.where(User.Constants.CONTACT_NUMBER).is(phoneNumber));
        query.fields().include(User.Constants._ID);
        logger.info("Query to fetch user : {}", query);
        User user = findOne(query, User.class);
        if (user != null) {
            return user.getId();
        } else {
            return null;
        }
    }

    public List<User> getUsersIncorrectPhoneCode(Integer start, Integer size) {
        Query query = new Query();
        query.addCriteria(Criteria.where(User.Constants.PHONE_CODE).is("+91"));
        setFetchParameters(query, start, size);

        return runQuery(query, User.class);
    }

    public List<User> getUserLocationInfoForUsers(List<Long> userIds) {
        Query query = new Query();
        query.fields().include(User.Constants._ID);
        query.fields().include(User.Constants.LOCATION_INFO);
        query.fields().include(User.Constants.FIRST_NAME);
        query.fields().include(User.Constants.LAST_NAME);
        query.addCriteria(Criteria.where(User.Constants._ID).in(userIds));
        return runQuery(query, User.class);
    }

    // TODO query optimization
    public List<User> getEarlyLearningTeachers(String type) {
        Query query = new Query();
        query.addCriteria(Criteria.where(User.Constants.ROLE).is(Role.TEACHER));
        query.addCriteria(Criteria.where(User.Constants.TEACHER_INFO + "." + TeacherInfo.Constants.TEACHER_CATEGORY_TYPES).in(TeacherCategoryType.EARLY_LEARNING));
        query.addCriteria(Criteria.where(User.Constants.TEACHER_INFO + "." + TeacherInfo.Constants.EARLY_LEARNING_TYPES).in(type));
        query.fields().include(User.Constants._ID);
        logger.info("query for supercoder"+query);

        return runQuery(query, User.class);
    }

    public List<TeacherProficiency> getTeacherProficiency(EarlyLearningCourseType earlyLearningCourseType) {
        List<AggregationOperation> aggregationOperation = new ArrayList<>();
        aggregationOperation.add(Aggregation.match(Criteria.where(User.Constants.ROLE).is(Role.TEACHER)));
        aggregationOperation.add(Aggregation.match(Criteria.where(User.Constants.TEACHER_INFO + "." + TeacherInfo.Constants.TEACHER_CATEGORY_TYPES).is(TeacherCategoryType.EARLY_LEARNING)));
        aggregationOperation.add(Aggregation.match(Criteria.where(User.Constants.TEACHER_INFO + "." + TeacherInfo.Constants.EARLY_LEARNING_TYPES).in(earlyLearningCourseType)));
        aggregationOperation.add(Aggregation.match(Criteria.where(User.Constants.TEACHER_INFO + "." + TeacherInfo.Constants.PROFICIENCY_TYPE).ne(null)));
        aggregationOperation.add(Aggregation.project(TeacherInfo.Constants.ID).and(User.Constants.TEACHER_INFO + "." +TeacherInfo.Constants.PROFICIENCY_TYPE).as(TeacherInfo.Constants.PROFICIENCY_TYPE));
        AggregationOptions aggregationOptions = Aggregation.newAggregationOptions().cursor(new Document()).build();
        Aggregation aggregation = Aggregation.newAggregation(aggregationOperation).withOptions(aggregationOptions);
        logger.info("Aggregation Query User "+aggregation);
        AggregationResults<TeacherProficiency> groupResults = getMongoOperations().aggregate(aggregation,User.class.getSimpleName(), TeacherProficiency.class);
        List<TeacherProficiency> results = groupResults.getMappedResults();
        logger.info("Size of SlotBookingCount : "+results.size());
        logger.info("SlotBookingCount : "+results);
        return results;
    }

    public void updateOnboardingData(Long callingUserId, Set<String> pendingBundleIdForOnboarding, Set<String> completedBundleIdForOnboarding) {

        Query query = new Query();
        query.addCriteria(Criteria.where(User.Constants._ID).is(callingUserId));

        Update update = new Update();
        if (ArrayUtils.isNotEmpty(pendingBundleIdForOnboarding)) {
            update.set(User.Constants.PENDING_BUNDLE_ID_FOR_ONBOARDING, pendingBundleIdForOnboarding);
        }
        if (ArrayUtils.isNotEmpty(completedBundleIdForOnboarding)) {
            update.set(User.Constants.PENDING_BUNDLE_ID_FOR_ONBOARDING, pendingBundleIdForOnboarding);
            update.set(User.Constants.COMPLETED_BUNDLE_ID_FOR_ONBOARDING, completedBundleIdForOnboarding);
        }

        updateFirst(query, update, User.class);
    }

    public List<User> getUsersParentInfoByIds(List<Long> userIds, Integer start, Integer size) {
        Query query = new Query();
        query.fields().include(User.Constants._ID);
        query.fields().include(User.Constants.EMAIL);
        query.fields().include(User.Constants.FIRST_NAME);
        query.fields().include(User.Constants.STUDENT_INFO);
        query.addCriteria(Criteria.where(User.Constants._ID).in(userIds));
        setFetchParameters(query, start, size);
        logger.info("QUERY :" + query);
        return runQuery(query, User.class);
    }

    public List<User> getUsersByOrgId(String orgId, int start, int limit) throws BadRequestException {
        if(StringUtils.isEmpty(orgId)){
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR,"orgId Not found");
        }
        Query query = new Query(Criteria.where(User.Constants.ORG_ID).is(orgId));
        query.with(Sort.by( Sort.Direction.ASC, User.Constants.CREATION_TIME));
        setFetchParameters(query, start, limit);
        logger.info("QUERY :" + query);
        return runQueryUserInfo(query);
    }

    public List<User> getUserInfoByContact(Set<String> contactNumbers, Role role) {
        if (role == null)
            role = Role.STUDENT;

        Query query;
        query = new Query();
        query.addCriteria(Criteria.where(User.Constants.CONTACT_NUMBER).in(contactNumbers));
        query.addCriteria(Criteria.where(User.Constants.ROLE).is(role));
        query.fields().include(User.Constants.ID);
        query.fields().include(User.Constants.CONTACT_NUMBER);
        return runQuery(query, User.class);
    }

    public List<User> getEarlyLearningTeachersById(Set<Long> userIds) {

        Query query = new Query();
        query.addCriteria(Criteria.where(User.Constants._ID).in(userIds));
        query.addCriteria(Criteria.where(User.Constants.ROLE).is(Role.TEACHER));
        query.addCriteria(Criteria.where(User.Constants.TEACHER_INFO + "." + TeacherInfo.Constants.TEACHER_CATEGORY_TYPES).in(TeacherCategoryType.EARLY_LEARNING));

        return runQuery(query, User.class);
    }

    public List<User> getUserIdsByUsingUserIds(List<Long> userIds) {
        List<User> users = null;
        if (ArrayUtils.isNotEmpty(userIds)) {
            Query query = new Query();
            query.fields().include(User.Constants.ID);
            query.addCriteria(Criteria.where(User.Constants.ID).in(userIds));
            users = runQuery(query, User.class);
        }
        return users;
    }

    private String getUserContactNumberKey(String contactNumber) {
        return env + "_USER_CONTACT_" + contactNumber;
    }

    public UserBasicInfo getUserByContactNumber(String contactNumber) throws BadRequestException, InternalServerErrorException {

        UserBasicInfo userBasicInfo = null;

        String key = getUserContactNumberKey(contactNumber);
        String value = redisDAO.get(key);
        Long userId = null;
        if (StringUtils.isNotEmpty(value)) {
            userId = Long.parseLong(value);
        }
        if (userId != null) {
            Map<Long,UserBasicInfo> userBasicInfoMap = fosUtils.getUserBasicInfosMapFromLongIds(Arrays.asList(userId),true);
            if (userBasicInfoMap != null) {
                userBasicInfo = userBasicInfoMap.get(userId);
                if (userBasicInfo != null) {
                    return userBasicInfo;
                }
            }
        }
        if (StringUtils.isNotEmpty(contactNumber)){
            Query query = new Query();
            query.addCriteria(Criteria.where(User.Constants.CONTACT_NUMBER).is(contactNumber));
            User user = runQuery(query,User.class).get(0);
            logger.info("user" + user);
            userBasicInfo = new UserBasicInfo(pojoUtils.convertToUserPojo(user),true);
            if (userBasicInfo != null) {
                redisDAO.set(key,Long.toString(userBasicInfo.getUserId()));
            }
            logger.info("userbasicinfo" +userBasicInfo);

        }
        return userBasicInfo;


    }

    public User getReferralCodeByUsingUserId(Long userId) {
        User user = null;
        if (userId != null) {
            Query query = new Query();
            query.fields().include(User.Constants.ID);
            query.fields().include(User.Constants.REFERRAL_CODE);
            query.addCriteria(Criteria.where(User.Constants.ID).is(userId));
            user = findOne(query, User.class);
        }
        return user;
    }
    public List<User> getTeacherBasicInfoByIds(List<Long> fetchList, Integer start, Integer size) {
    	Query query = new Query();
    	query.addCriteria(Criteria.where(User.Constants.ID).in(fetchList));
    	query.addCriteria(Criteria.where(User.Constants.ROLE).is(Role.TEACHER));
    	query.fields().include(User.Constants.ID);
    	query.fields().include(User.Constants.TEACHER_INFO);
        query.fields().include(User.Constants.FIRST_NAME);
        query.fields().include(User.Constants.LAST_NAME);
        query.fields().include(User.Constants.FULL_NAME);
        setFetchParameters(query, start, size);
    	return runQuery(query, User.class);

    }

    public List<User> getUserByUsingUserIdsForVquizWinnersExport(List<Long> fetchList) {
        if(ArrayUtils.isEmpty(fetchList)){
            return new ArrayList<>();
        }
        Query query = new Query();
        query.addCriteria(Criteria.where(User.Constants.ID).in(fetchList));
        query.addCriteria(Criteria.where(User.Constants.ROLE).is(Role.STUDENT));
        query.fields().include(User.Constants.ID);
        query.fields().include(User.Constants.FULL_NAME);
        query.fields().include(User.Constants.FIRST_NAME);
        query.fields().include(User.Constants.LAST_NAME);
        query.fields().include(User.Constants.EMAIL);
        query.fields().include(User.Constants.CONTACT_NUMBER);
        return runQuery(query, User.class);
    }

}
