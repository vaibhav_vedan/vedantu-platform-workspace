/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.user.async;

import com.vedantu.async.AppContextManager;
import com.vedantu.async.AsyncTaskParams;
import com.vedantu.async.IAsyncQueueExecutor;
import com.vedantu.async.IAsyncQueueName;

/**
 *
 * @author somil
 */
public enum AsyncQueueName implements IAsyncQueueName {
    
    DEFAULT_QUEUE(DefaultQueueExecutor.class),
    EMAIL_QUEUE(EmailQueueExecutor.class);

    private Class<? extends IAsyncQueueExecutor> asyncExecutorClass;
    
    
    private AsyncQueueName(Class<? extends IAsyncQueueExecutor> asyncQueueExecutor) {
        this.asyncExecutorClass = asyncQueueExecutor;
    }
    
    
    @Override
    public void execute(AsyncTaskParams params) throws Exception {
        //TODO: Change this, this is getting the interface proxy bean, hence using this temp approach
        //IAsyncQueueExecutor asyncQueueExecutor = AppContextManager.getAppContext().getBean(asyncExecutorClass);
        String beanName = Character.toLowerCase(asyncExecutorClass.getSimpleName().charAt(0)) + asyncExecutorClass.getSimpleName().substring(1);
        IAsyncQueueExecutor asyncQueueExecutor = (IAsyncQueueExecutor) AppContextManager.getAppContext().getBean(beanName);
        
        asyncQueueExecutor.execute(params);
    }
}
