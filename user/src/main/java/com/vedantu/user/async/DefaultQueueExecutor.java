/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.user.async;


import com.vedantu.User.UserDetailsForSNS;
import com.vedantu.User.response.GeneratePasswordRes;
import com.vedantu.async.AsyncTaskParams;
import com.vedantu.async.IAsyncQueueExecutor;
import com.vedantu.user.entity.User;
import com.vedantu.user.entity.UserLead;
import com.vedantu.user.managers.*;

import com.vedantu.util.LogFactory;
import java.util.Map;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Recover;
import org.springframework.retry.annotation.Retryable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

/**
 *
 * @author somil
 */
@Service
public class DefaultQueueExecutor implements IAsyncQueueExecutor {

    @Autowired
    public LogFactory logFactory;

    @Autowired
    UserManager userManager;

    @Autowired
    UserLeadManager userLeadManager;

    @Autowired
    private CommunicationManager communicationManager;

    @Autowired
    private GAIdManager gaIdManager;

    @Autowired
    private VQuizUserManager vQuizUserManager;

    @SuppressWarnings("static-access")
    public Logger logger = logFactory.getLogger(DefaultQueueExecutor.class);

    @Async("taskExecutor")
    @Retryable(maxAttempts = 1, backoff = @Backoff(delay = 100, maxDelay = 500))
    @Override
    public void execute(AsyncTaskParams params) throws Exception {
        if (!(params.getAsyncTaskName() instanceof AsyncTaskName)) {
            logger.error(params.getAsyncTaskName() + " not supported");
            return;
        }
        AsyncTaskName taskName = (AsyncTaskName) params.getAsyncTaskName();
        Map<String, Object> payload = params.getPayload();
        switch (taskName) {
            case USER_UPDATION_TRIGGER:
                User user = (User) payload.get("user");
                userManager.triggerUserUpdationSNS(user);
                break;
            case USER_UPDATION_TRIGGER_ISL:
                UserDetailsForSNS userInfo = (UserDetailsForSNS) payload.get("user");
                userManager.triggerUserUpdationSNSForISL(userInfo);
                break;
            case USER_CITY_UPDATE:
            	User user1 = (User) payload.get("user");
            	String ip = (String) payload.get("ip");
            	Long callingUser = (Long) payload.get("callingUser");
            	userManager.updateUserLocationFrommIp(user1, ip, callingUser);
            	break;
            case USER_LEAD_EVENT:
                UserLead userLead = (UserLead) payload.get("userLead");
                userLeadManager.checkAndSendSMS(userLead);
                break;
            case USER_LEAD_FILL_LOCATION:
                userLead = (UserLead) payload.get("userLead");
                boolean sendSMS = (boolean) payload.get("sendSMS");
                userLeadManager.fillLocation(userLead,sendSMS);
                break;
            case RESET_ADMIN_PASSWORDS:
                userManager.resetadminpasswords();
                break;
            case GENERATE_PASSWORD_EMAIL:
                GeneratePasswordRes generatePasswordRes = (GeneratePasswordRes) payload.get("generatePasswordRes");
                communicationManager.sendGeneratePasswordEmailAndSMS(generatePasswordRes);
                break;
            case SIGNUP_TASKS:
                User u = (User) payload.get("user");
                Long callingUserId = (Long) payload.get("callingUserId");
                userManager.performSignUpTasks(u, callingUserId);
                break;
            case REGISTER_USER_VGROUP:
                String id = (String) payload.get("userId");
                String grade = (String) payload.get("grade");
                gaIdManager.registerUserInVGroups(id, grade);
                break;
            case SAVE_USER_LOGIN_DATA:
                String email = (String) payload.get("email");
                String ipAddress = (String) payload.get("ipAddress");
                userManager.updateLoginData(email, ipAddress);
                break;
            case EXPORT_VQUIZ_WINNERS:
                Map<String,String> map = (Map<String, String>) payload.get("requestParams");
                vQuizUserManager.exportWinnersCsvFromAsync(map);
                break;
        }
    }

    @Recover
    @Override
    public void recover(Exception exception) {
        logger.error("exception thrown in async task" + Thread.currentThread().getName(), exception);
    }

}
