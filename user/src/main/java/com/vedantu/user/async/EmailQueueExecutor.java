package com.vedantu.user.async;

import com.vedantu.User.User;
import com.vedantu.User.response.UserDetailsInfo;
import com.vedantu.async.AsyncTaskParams;
import com.vedantu.async.IAsyncQueueExecutor;
import com.vedantu.user.managers.CommunicationManager;
import com.vedantu.util.LogFactory;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Recover;
import org.springframework.retry.annotation.Retryable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 *
 * @author MNPK
 */

@Service
public class EmailQueueExecutor implements IAsyncQueueExecutor {

    @Autowired
    public LogFactory logFactory;

    @SuppressWarnings("static-access")
    public Logger logger = logFactory.getLogger(EmailQueueExecutor.class);

    @Autowired
    CommunicationManager emailManager;

    //TODO: Add executor here
    @Async("emailExecutor")
    @Retryable(maxAttempts = 1, backoff = @Backoff(delay = 100, maxDelay = 500))
    @Override
    public void execute(AsyncTaskParams params) throws Exception {
        logger.info("ENTRY " + params);
        if (!(params.getAsyncTaskName() instanceof AsyncTaskName)) {
            logger.error(params.getAsyncTaskName() + " not supported");
            return;
        }
        AsyncTaskName taskName = (AsyncTaskName) params.getAsyncTaskName();
        Map<String, Object> payload = params.getPayload();
        switch (taskName) {
            case USER_DETAILS_CREATED_VIA_ISL_REGISTRATION_TOOLS:
                User userData3 = (User) payload.get("user");
                emailManager.sendEmailUserDetailsCreatedViaTools(userData3);
                break;
            case REGISTRATION_EMAIL_ISL:
                User userData = (User) payload.get("user");
                emailManager.sendRegistrationEmailForISL(userData);
                break;
            case REGISTRATION_EMAIL_VSAT:
                User userDataNew = (User) payload.get("user");
                emailManager.sendRegistrationEmailForVSAT(userDataNew);
                break;
            case REGISTRATION_EMAIL_VOLT:
                User user1 = (User) payload.get("user");
                emailManager.sendRegistrationEmailForVolt(user1);
                break;
            case REGISTERED_EMAIL_JRP:
                User userDataNew2 = (User) payload.get("user");
                UserDetailsInfo userDetails = (UserDetailsInfo) payload.get("userDetails");
                emailManager.sendRegistrationEmailForJRP(userDataNew2,userDetails);
                break;
            case REGISTRATION_EMAIL_REVISEINDIA:
                User u1 = (User) payload.get("user");
                emailManager.sendRegistrationEmailForReviseIndia(u1);
                break;
            case REGISTRATION_EMAIL_REVISEJEE:
                User u2 = (User) payload.get("user");
                emailManager.sendRegistrationEmailForReviseJee(u2);
                break;
        }
    }

    @Recover
    @Override
    public void recover(Exception exception) {
        logger.error("exception thrown in async task" + Thread.currentThread().getName(), exception);
    }

}
