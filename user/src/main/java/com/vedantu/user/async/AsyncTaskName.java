/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.user.async;

import com.vedantu.async.IAsyncTaskName;

/**
 *
 * @author somil
 */
public enum AsyncTaskName implements IAsyncTaskName {
    
    USER_UPDATION_TRIGGER(AsyncQueueName.DEFAULT_QUEUE), 
    USER_UPDATION_TRIGGER_ISL(AsyncQueueName.DEFAULT_QUEUE), 
    USER_CITY_UPDATE(AsyncQueueName.DEFAULT_QUEUE),
    USER_LEAD_EVENT(AsyncQueueName.DEFAULT_QUEUE),
    RESET_ADMIN_PASSWORDS(AsyncQueueName.DEFAULT_QUEUE),
    GENERATE_PASSWORD_EMAIL(AsyncQueueName.DEFAULT_QUEUE),
    USER_LEAD_FILL_LOCATION(AsyncQueueName.DEFAULT_QUEUE),
    SIGNUP_TASKS(AsyncQueueName.DEFAULT_QUEUE),
    REGISTER_USER_VGROUP(AsyncQueueName.DEFAULT_QUEUE),
    USER_DETAILS_CREATED_VIA_ISL_REGISTRATION_TOOLS(AsyncQueueName.EMAIL_QUEUE),
    REGISTRATION_EMAIL_ISL(AsyncQueueName.EMAIL_QUEUE),
    REGISTRATION_EMAIL_VSAT(AsyncQueueName.EMAIL_QUEUE),
    REGISTRATION_EMAIL_VOLT(AsyncQueueName.EMAIL_QUEUE),
    REGISTERED_EMAIL_JRP(AsyncQueueName.EMAIL_QUEUE),
    REGISTRATION_EMAIL_REVISEINDIA(AsyncQueueName.EMAIL_QUEUE),
    REGISTRATION_EMAIL_REVISEJEE(AsyncQueueName.EMAIL_QUEUE),
    SAVE_USER_LOGIN_DATA(AsyncQueueName.DEFAULT_QUEUE),
    EXPORT_VQUIZ_WINNERS(AsyncQueueName.DEFAULT_QUEUE);

    
    private AsyncQueueName queue;

    
    private AsyncTaskName(AsyncQueueName queue) {
        this.queue = queue;
    }
    
    private AsyncTaskName() {
        this.queue = AsyncQueueName.DEFAULT_QUEUE;
    }
    

    @Override
    public AsyncQueueName getQueue() {
        return queue;
    }

}
 