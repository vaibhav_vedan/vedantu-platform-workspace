package com.vedantu.user.controllers;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.ForbiddenException;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.vedantu.User.Role;
import com.vedantu.User.TeacherBasicInfo;
import com.vedantu.User.User;
import com.vedantu.User.UserBasicInfo;
import com.vedantu.User.request.ExtensionNumberReq;
import com.vedantu.User.request.GetActiveTutorsReq;
import com.vedantu.User.request.GetUserSystemIntroReq;
import com.vedantu.User.response.GetActiveTutorsRes;
import com.vedantu.User.response.UserDetailsInfo;
import com.vedantu.exception.VException;
import com.vedantu.user.managers.UserInfoManager;
import com.vedantu.user.managers.UserManager;
import com.vedantu.user.requests.UserPreBookingVerificationInfoReq;
import com.vedantu.user.responses.UserPreBookingVerificationInfoRes;
import com.vedantu.user.utils.PojoUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.PlatformBasicResponse;
import com.vedantu.util.security.HttpSessionData;
import com.vedantu.util.security.HttpSessionUtils;

/**
 * @author MNPK
 */

@RestController
@RequestMapping("/info")
public class UserInfoController {

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(UserPasswordController.class);

    @Autowired
    private HttpSessionUtils sessionUtils;

    @Autowired
    private UserInfoManager userInfoManager;

    @Autowired
    private UserManager userManager;

    @Autowired
    private PojoUtils pojoUtils;

    @RequestMapping(value = "/getUserInfos", method = RequestMethod.GET)
    @ResponseBody
    public List<UserBasicInfo> getUserInfos(@RequestParam(value = "userIdsList") String userIdsList) throws VException {
        sessionUtils.checkIfAllowedList(null, Arrays.asList(Role.ADMIN, Role.STUDENT_CARE), Boolean.FALSE);
        return userInfoManager.getUserInfos(userIdsList, true);
    }

    @Deprecated
    @RequestMapping(value = "/getUserById", method = RequestMethod.GET)
    @ResponseBody
    public User getUserById(@RequestParam(value = "userId") Long userId) throws VException {
        logger.warn("Nowhere using API called. UserInfoController API - info/getUserById");
        if(sessionUtils.isStudent(sessionUtils.getCurrentSessionData())){
            Long callinguserId = sessionUtils.getCallingUserId();
            if(null != callinguserId && !callinguserId.equals(userId)){
                logger.warn("Accessing other user data - UserInfoController - getUserById" + userId);
            }
        }
        //com.vedantu.user.entity.User user = userManager.getUserByUserId(userId);
        //return pojoUtils.convertToUserPojo(user);
        return null;
    }

    @RequestMapping(value = "/checkUserName", method = RequestMethod.GET)
    @ResponseBody
    public PlatformBasicResponse checkUsername(@RequestParam(value = "username") String username)
            throws VException, UnsupportedEncodingException {
        return userInfoManager.checkUsername(username);
    }

    @Deprecated
    @RequestMapping(value = "/getTeacherContactFromExtension", method = RequestMethod.GET)
    public String getTeacherContactFromExtension(ExtensionNumberReq req) throws Exception {
        logger.warn("Nowhere using API called. UserInfoController API - info/getTeacherContactFromExtension");
        req.verify();
        //return userInfoManager.getTeacherContactFromExtension(req);
        return null;
    }

    @RequestMapping(value = "/getUserDetailsForISLInfo", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public UserDetailsInfo getUserDetailsForISLInfo(@RequestParam(value = "event", required = false) String event) throws VException {
        sessionUtils.checkIfUserLoggedIn();
        HttpSessionData httpSessionData = sessionUtils.getCurrentSessionData();
        return userInfoManager.getUserDetailsForISLInfo(httpSessionData.getUserId(), event);
    }

    @RequestMapping(value = "/getUserSystemIntroData", method = RequestMethod.GET)
    @ResponseBody
    public String getUserSystemIntroData(GetUserSystemIntroReq req, HttpServletRequest request) throws VException {
        sessionUtils.checkIfAllowed(req.getUserId(), null, Boolean.TRUE);
        return userInfoManager.getUserSystemIntroData(req, request);
    }

    @RequestMapping(value = "/getActiveTutorsList", method = RequestMethod.GET)
    public GetActiveTutorsRes getActiveTutorsList(HttpServletRequest httpRequest, HttpServletResponse httpResponse,
                                                  GetActiveTutorsReq req) throws VException {
        HttpSessionData sessionData = sessionUtils.getCurrentSessionData();
        if(sessionData != null){
            logger.warn("API - info/getActiveTutorsList is being called by: "+sessionData.getRole());
        }else{
            logger.warn("info/getActiveTutorsList is being called by ADMIN TOKENS");
        }
        try {
            req.verify();
            GetActiveTutorsRes res = userInfoManager.getActiveTutorsList(httpRequest, req);
            httpResponse.setHeader("Cache-Control", "private, max-age=604800, must-revalidate"); // HTTP
            // 1.1
            httpResponse.setHeader("Pragma", "private"); // HTTP 1.0
            httpResponse.setDateHeader("Expires", new Date().getTime() + 604800000); // Proxies.
            return res;
        } catch (Exception exp) {
            // Expiration header cache cleared
            httpResponse.setHeader("Pragma", "no-cache"); // HTTP 1.0
            httpResponse.setDateHeader("Expires", 0); // Proxies.
            httpResponse.setHeader("Cache-Control", "no-cache, no-store, must-revalidate"); // HTTP
            // 1.1
            throw exp;
        }

    }
    @RequestMapping(value = "/getUserByContactNumber", method = RequestMethod.GET)
    @ResponseBody
    public UserBasicInfo getUserByContactNumber(@RequestParam(value = "contactNumber") String contactNumber,HttpServletRequest httpRequest) throws VException {
        sessionUtils.isAllowedApp(httpRequest);
        return userInfoManager.getUserByContactNumber(contactNumber);
    }

    @RequestMapping(value = "/preBookingVerification", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public UserPreBookingVerificationInfoRes preBookingVerification(@RequestBody UserPreBookingVerificationInfoReq req) throws VException, UnsupportedEncodingException {
        logger.info("Inside preBookingVerification Req : {}", req);
        req.verify();
        return userInfoManager.preBookingVerification(req);
    }
    
    @RequestMapping(value = "/getTeacherBasicInfos", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<TeacherBasicInfo> getTeacherBasicInfos(@RequestBody List<String> teacherIdsList, HttpServletRequest servletRequest)
            throws VException {
        sessionUtils.isAllowedApp(servletRequest);
    	List<Long> teacherIdList = new ArrayList<Long>();
    	teacherIdList = teacherIdsList.stream().map(Long::parseLong).collect(Collectors.toList());
        return userInfoManager.getTeacherBasicInfos(teacherIdList);
    }

}
