package com.vedantu.user.controllers;

import com.vedantu.User.Role;
import com.vedantu.User.request.*;
import com.vedantu.User.response.EditProfileRes;
import com.vedantu.User.response.EditUserProfileRes;
import com.vedantu.exception.VException;
import com.vedantu.user.managers.UserManager;
import com.vedantu.user.managers.UserProfileManager;
import com.vedantu.user.utils.PojoUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.security.HttpSessionUtils;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Arrays;

@RestController
@RequestMapping("/profile")
public class UserProfileController {

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(UserPasswordController.class);

    @Autowired
    private HttpSessionUtils sessionUtils;

    @Autowired
    private UserProfileManager userProfileManager;

    @RequestMapping(value = "/editUserProfile", method = RequestMethod.POST)
    @ResponseBody
    public EditUserProfileRes editUserProfile(@Valid @RequestBody EditUserProfileReq req) throws VException {
        logger.info("Inside editUserProfile Req : {}", req);
        sessionUtils.checkIfAllowedList(null, Arrays.asList( Role.STUDENT), Boolean.FALSE);
        return userProfileManager.editUserProfile(req);
    }

}
