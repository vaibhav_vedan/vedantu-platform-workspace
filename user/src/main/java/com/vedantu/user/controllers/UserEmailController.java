package com.vedantu.user.controllers;

import com.vedantu.User.Role;
import com.vedantu.exception.VException;
import com.vedantu.user.managers.UserEmailManager;
import com.vedantu.user.requests.SendUserEmailReq;
import com.vedantu.util.LogFactory;
import com.vedantu.util.PlatformBasicResponse;
import com.vedantu.util.security.HttpSessionUtils;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.io.UnsupportedEncodingException;
import java.util.Arrays;

/**
 * @author MNPK
 */

@RestController
@RequestMapping("/email")
public class UserEmailController {

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(UserPasswordController.class);

    @Autowired
    private UserEmailManager userEmailManager;

    @Autowired
    private HttpSessionUtils sessionUtils;


    @RequestMapping(value = "/sendUserRelatedEmail", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse sendUserRelatedEmail(@RequestBody SendUserEmailReq req) throws VException, UnsupportedEncodingException {
        sessionUtils.checkIfAllowedList(req.getCallingUserId(), Arrays.asList(Role.ADMIN, Role.STUDENT_CARE), Boolean.FALSE);
        return userEmailManager.sendUserRelatedEmail(req);
    }


}
