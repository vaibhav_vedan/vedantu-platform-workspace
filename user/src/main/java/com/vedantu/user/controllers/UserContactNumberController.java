package com.vedantu.user.controllers;

import com.vedantu.User.Role;
import com.vedantu.User.request.AddOrUpdatePhoneNumberReq;
import com.vedantu.User.request.EditContactNumberReq;
import com.vedantu.User.request.ReSendContactNumberVerificationCodeReq;
import com.vedantu.User.request.SetBlockStatusForUserReq;
import com.vedantu.User.response.EditContactNumberRes;
import com.vedantu.User.response.PhoneNumberRes;
import com.vedantu.User.response.ReSendContactNumberVerificationCodeRes;
import com.vedantu.exception.VException;
import com.vedantu.user.managers.UserContactNumberManager;
import com.vedantu.util.LogFactory;
import com.vedantu.util.PlatformBasicResponse;
import com.vedantu.util.security.HttpSessionUtils;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author MNPK
 */

@RestController
@RequestMapping("/contactNumber")
public class UserContactNumberController {


    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(UserPasswordController.class);

    @Autowired
    private UserContactNumberManager userContactNumberManager;

    @Autowired
    private HttpSessionUtils sessionUtils;


//    @RequestMapping(value = "/editContactNumber", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
//    @ResponseBody
//    public EditContactNumberRes editContactNumber(@Valid @RequestBody EditContactNumberReq request, HttpServletRequest httpRequest, HttpServletResponse httpResponse) throws VException, IOException, URISyntaxException {
//        sessionUtils.checkIfUserLoggedIn();
//        return userContactNumberManager.editContactNumber(request, httpRequest, httpResponse);
//    }

    @RequestMapping(value = "/reSendContactNumberVerificationCode", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ReSendContactNumberVerificationCodeRes reSendContactNumberVerificationCode(@RequestBody ReSendContactNumberVerificationCodeReq request) throws VException {
        sessionUtils.checkIfUserLoggedIn();
        return userContactNumberManager.reSendContactNumberVerificationCode(request);
    }

    @RequestMapping(value = "/updatePhoneNumber", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PhoneNumberRes updatePhoneNumber(@Valid @RequestBody AddOrUpdatePhoneNumberReq req) throws VException {
        sessionUtils.checkIfUserLoggedIn();
        return userContactNumberManager.updatePhoneNumber(req);
    }

    @RequestMapping(value = "/updateAndMarkActivePhoneNumber", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PhoneNumberRes updateAndMarkActivePhoneNumber(@Valid @RequestBody AddOrUpdatePhoneNumberReq req) throws VException {
        sessionUtils.checkIfUserLoggedIn();
        return userContactNumberManager.updateAndMarkActivePhoneNumber(req);
    }

    @RequestMapping(value = "/getUserPhoneNumbers", method = RequestMethod.GET)
    @ResponseBody
    public String getUserPhoneNumbers(@RequestParam(value = "userId") Long userId) throws VException {
        sessionUtils.checkIfAllowed(userId, null, Boolean.TRUE);
        return userContactNumberManager.getUserPhoneNumbers(userId);
    }

    @RequestMapping(value = "/markBlockStatus", method = RequestMethod.POST)
    @ResponseBody
    public PlatformBasicResponse markBlockStatus(@Valid @RequestBody SetBlockStatusForUserReq setBlockStatusForUserReq) throws VException, UnsupportedEncodingException {
        sessionUtils.checkIfAllowedList(null, Arrays.asList(Role.ADMIN, Role.STUDENT_CARE, Role.TEACHER), Boolean.FALSE);
        return userContactNumberManager.markBlockStatus(setBlockStatusForUserReq);
    }

}
