package com.vedantu.user.controllers;

import com.google.gson.JsonObject;
import com.vedantu.User.request.SocialSource;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.InternalServerErrorException;
import com.vedantu.exception.VException;
import com.vedantu.user.managers.signup.UserSignUpManager;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;

@RestController
@RequestMapping("/signUp")
public class UserSignUpController {

    @Autowired
    private LogFactory logFactory;

    @Autowired
    private UserSignUpManager userSignUpManager;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(UserSignUpController.class);

    @RequestMapping(value = "/auth.social", method = RequestMethod.GET)
    public void authSocial(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException, VException, UnsupportedEncodingException, URISyntaxException {

        SocialSource source = SocialSource.valueOfKey(request.getParameter("source"));
        if (source == SocialSource.UNKNOWN) {
            response.sendError(HttpServletResponse.SC_BAD_REQUEST,
                    "unknown source : " + request.getParameter("source"));
            return;
        }
        JsonObject jsonObject = new JsonObject();
        request.setAttribute("data", jsonObject);
        if (StringUtils.isEmpty(request.getParameter("code"))) {
            String error = request.getParameter("error");
            if (StringUtils.isNotEmpty(error)) {
                jsonObject.addProperty("error", error);
                request.getRequestDispatcher(UserSignUpManager.STATUS_SOCIAL_LOGIN).forward(request, response);
                return;
            }

            response.sendError(HttpServletResponse.SC_BAD_REQUEST, "missing paramater code");
            return;
        }

        //try {
        userSignUpManager.authenticate(request, response, source);
        //} catch (Exception e) {
        //    jsonObject.addProperty("error", e.getMessage());
        //    logger.warn(e.getMessage(), e);
        //    request.getRequestDispatcher(UserManager.STATUS_SOCIAL_LOGIN).forward(request, response);
        //}
    }

    @RequestMapping(value = "/signUp", method = RequestMethod.GET)
    public void signUp(HttpServletRequest req, HttpServletResponse resp) throws IOException, InternalServerErrorException, BadRequestException {
        SocialSource source = SocialSource.valueOfKey(req.getParameter("source"));
        String utmParams = req.getParameter("utmParams");
        String signUpURL = req.getParameter("signUpURL");
        String referrer = req.getParameter("referrer");
        String optIn = req.getParameter("optIn");
        if (source == SocialSource.UNKNOWN) {
            resp.sendError(HttpServletResponse.SC_BAD_REQUEST, "unknown source");
            return;
        }
        userSignUpManager.authorize(resp, source, utmParams, signUpURL, referrer, optIn);
    }
}
