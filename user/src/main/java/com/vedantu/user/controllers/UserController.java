/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.user.controllers;

import com.google.gson.Gson;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.User.LocationInfo;
import com.vedantu.User.Role;
import com.vedantu.User.SaleClosedUser;
import com.vedantu.User.User;
import com.vedantu.User.UserBasicInfo;
import com.vedantu.User.UserDetailsForSNS;
import com.vedantu.User.UserInfo;
import com.vedantu.User.enums.EventName;
import com.vedantu.User.enums.VoltStatus;
import com.vedantu.User.request.AcceptTncReq;
import com.vedantu.User.request.AddOrUpdatePhoneNumberReq;
import com.vedantu.User.request.AddUserAddressReq;
import com.vedantu.User.request.AddUserSystemIntroReq;
import com.vedantu.User.request.ChangePasswordReq;
import com.vedantu.User.request.CreateUserAuthenticationTokenReq;
import com.vedantu.User.request.EditProfileReq;
import com.vedantu.User.request.EditUserDetailsReq;
import com.vedantu.User.request.ExistingUserISLRegReq;
import com.vedantu.User.request.ExtensionNumberReq;
import com.vedantu.User.request.ForgotPasswordReq;
import com.vedantu.User.request.GeneratePasswordReq;
import com.vedantu.User.request.GetActiveTutorsReq;
import com.vedantu.User.request.GetDexTeachersReq;
import com.vedantu.User.request.GetNonUsersReq;
import com.vedantu.User.request.GetOTPReq;
import com.vedantu.User.request.GetUserInfosByEmailReq;
import com.vedantu.User.request.GetUserSystemIntroReq;
import com.vedantu.User.request.GetUsersReq;
import com.vedantu.User.request.ReSendContactNumberVerificationCodeReq;
import com.vedantu.User.request.ResetPasswordReq;
import com.vedantu.User.request.SetBlockStatusForUserReq;
import com.vedantu.User.request.SetProfilePicReq;
import com.vedantu.User.request.SignInByTokenReq;
import com.vedantu.User.request.SignInReq;
import com.vedantu.User.request.SignUpForISLReq;
import com.vedantu.User.request.SignUpReq;
import com.vedantu.User.request.UserContactList;
import com.vedantu.User.request.VerifyContactNumberReq;
import com.vedantu.User.response.CreateUserAuthenticationTokenRes;
import com.vedantu.User.response.CreateUserResponse;
import com.vedantu.User.response.EditProfileRes;
import com.vedantu.User.response.GeneratePasswordRes;
import com.vedantu.User.response.GetActiveTutorsRes;
import com.vedantu.User.response.GetUserPhoneNumbersRes;
import com.vedantu.User.response.GetUserProfileRes;
import com.vedantu.User.response.GetUsersRes;
import com.vedantu.User.response.IsRegisteredForISLRes;
import com.vedantu.User.response.PhoneNumberRes;
import com.vedantu.User.response.ProcessVerifyContactNumberResponse;
import com.vedantu.User.response.ReSendContactNumberVerificationCodeRes;
import com.vedantu.User.response.ReferralInfoRes;
import com.vedantu.User.response.SignUpForISLRes;
import com.vedantu.User.response.StatusCounterResponse;
import com.vedantu.User.response.TeacherListWithStatusRes;
import com.vedantu.User.response.UserBasicResponse;
import com.vedantu.User.response.UserDetailsInfo;
import com.vedantu.User.response.UserLeadInfo;
import com.vedantu.User.response.UserSystemIntroRes;
import com.vedantu.aws.pojo.AWSSNSSubscriptionRequest;
import com.vedantu.dinero.enums.SnsOrders;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ConflictException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.ForbiddenException;
import com.vedantu.exception.NotFoundException;
import com.vedantu.exception.VException;
import com.vedantu.lms.cmds.pojo.AttemptedUserIds;
import com.vedantu.user.entity.UserLead;
import com.vedantu.user.entity.UserLeadActivity;
import com.vedantu.user.entity.UserLoginToken;
import com.vedantu.user.managers.CommunicationManager;
import com.vedantu.user.managers.UserLeadManager;
import com.vedantu.user.managers.UserLocalesManager;
import com.vedantu.user.requests.AddEditUserLeadReq;
import com.vedantu.user.requests.CreateUserDetailsReq;
import com.vedantu.user.requests.GetUserDashboardReq;
import com.vedantu.user.requests.GetUserLeadReq;
import com.vedantu.user.requests.GetVoltUserDetailsReq;
import com.vedantu.user.requests.GetXVedTokenByUserDetailsReq;
import com.vedantu.user.requests.GpsLocationReq;
import com.vedantu.user.requests.NewPhoneVerificationReq;
import com.vedantu.user.requests.OTPVerificationReq;
import com.vedantu.user.requests.ProfileBuilderPostLoginReq;
import com.vedantu.user.requests.SignUpUserReq;
import com.vedantu.user.requests.TeacherTokenRequest;
import com.vedantu.user.requests.TruecallerLoginReq;
import com.vedantu.user.requests.UserAddDummyNumberReq;
import com.vedantu.user.requests.UserDetailsVoltApproveReq;
import com.vedantu.user.requests.UserEmailUpdateReq;
import com.vedantu.user.requests.UserPreLoginVerificationInfoReq;
import com.vedantu.user.requests.UserPreSignUpVerificationInfoReq;
import com.vedantu.user.responses.GetDexAndTheirStatusRes;
import com.vedantu.user.responses.GetTestAttemptRes;
import com.vedantu.user.responses.GetUserDashboardRes;
import com.vedantu.user.responses.GpsLocationResp;
import com.vedantu.user.responses.NewPhoneVerificationRes;
import com.vedantu.user.responses.ProfileBuilderPostLoginRes;
import com.vedantu.user.responses.SignUpUserRes;
import com.vedantu.user.responses.TeacherInfoMinimal;
import com.vedantu.user.responses.TruecallerLoginRes;
import com.vedantu.user.responses.UserParentInfoRes;
import com.vedantu.user.responses.UserPreLoginVerificationInfoRes;
import com.vedantu.user.responses.UserPreSignUpVerificationInfoRes;
import com.vedantu.user.utils.PojoUtils;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.PlatformBasicResponse;
import com.vedantu.util.StringUtils;
import com.vedantu.util.WebUtils;
import com.vedantu.util.pojo.VsatEventDetailsPojo;
import com.vedantu.util.security.HttpSessionData;
import com.vedantu.util.security.HttpSessionUtils;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author somil
 */
@RestController
public class UserController {

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(UserController.class);

    @Autowired
    com.vedantu.user.managers.UserManager userManager;

    @Autowired
    UserLeadManager userLeadManager;

    @Autowired
    public PojoUtils pojoUtils;

    @Autowired
    HttpSessionUtils sessionUtils;

    @Autowired
    CommunicationManager communicationManager;

    @Autowired
    UserLocalesManager userLocalesManager;

    @RequestMapping(value = "/createUser", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public CreateUserResponse createUser(@Valid @RequestBody SignUpReq signUpReq) throws Exception {
        return userManager.createUser(signUpReq);
    }

    @RequestMapping(value = "/requestOTP", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ReSendContactNumberVerificationCodeRes requestOTP(
            @RequestBody GetOTPReq req) throws VException {
        sessionUtils.checkIfUserLoggedIn();
        return userManager.requestOTP(req);
    }

    @Deprecated
    @RequestMapping(value = "/requestOTPPassword", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ReSendContactNumberVerificationCodeRes requestOTPPassword(
            @RequestBody GetOTPReq req) throws VException {
        logger.warn("Nowhere using API called. UserController API - requestOTPPassword");
//        HttpSessionData sessionData = sessionUtils.getCurrentSessionData();
//        if(sessionData != null && sessionData.getUserId() != null) {
//            return userManager.requestOTPPassword(req, true);
//        } else {
        //return userManager.requestOTPPassword(req);
//        }

        return null;
    }

    @RequestMapping(value = "/forgotPassword", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public CreateUserResponse forgotPassword(@RequestBody ForgotPasswordReq req, HttpServletRequest servletRequest)
            throws VException, UnsupportedEncodingException {
        sessionUtils.isAllowedApp(servletRequest);
        return userManager.forgotPassword(req);
    }

    @RequestMapping(value = "/resetPasswordOtpRequest", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void resetPasswordOtpRequest(@RequestBody ResetPasswordReq req)
            throws VException, UnsupportedEncodingException {
        userManager.resetPasswordOtpRequest(req);
    }


    @RequestMapping(value = "/changePassword", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse changePassword(@Valid @RequestBody ChangePasswordReq req, HttpServletRequest request,
                                                HttpServletResponse response) throws VException, IOException, URISyntaxException {
        return userManager.changePassword(req, request, response);
    }


    @Deprecated
    @RequestMapping(value = "/authenticateUser", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public com.vedantu.User.User authenticateUser(@RequestBody SignInReq signInReq) throws VException {
        logger.warn("Nowhere using API called. UserController API - authenticateUser");
        //return userManager.authenticateUser(signInReq);
        return null;
    }

//    @RequestMapping(value = "/authenticateUserByOTP", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
//    @ResponseBody
//    public com.vedantu.User.User authenticateUserByOTP(@RequestBody SignInReq signInReq) throws VException {
////        String phone = signInReq.getEmail();
////        signInReq.setEmail(phone + "-app@vedantu.com");
//        return userManager.authenticateUserByOTP(signInReq);
//    }

    @RequestMapping(value = "/getUserProfile", method = RequestMethod.GET)
    @ResponseBody
    public GetUserProfileRes getUserProfile(@RequestParam Long userId) throws VException {
        boolean exposeEmail = sessionUtils.isAdminOrStudentCare(sessionUtils.getCurrentSessionData());
        if (!exposeEmail && sessionUtils.getCallingUserId() != null && sessionUtils.getCallingUserId().equals(userId)) {
            exposeEmail = true;
        }
//        if(sessionUtils.isStudent(sessionUtils.getCurrentSessionData())){
//            Long userIdFromSession = sessionUtils.getCallingUserId();
//            if(null != userIdFromSession && !userIdFromSession.equals(userId)){
//                return new GetUserProfileRes();
//            }
//        }
        return userManager.getUserProfile(userId, exposeEmail);
    }

    @RequestMapping(value = "/isPasswordSet", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Boolean> isPasswordSet(@RequestParam Long userId) throws VException {
        return userManager.isPasswordSet(userId);
    }


    @RequestMapping(value = "/acceptTnc", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse acceptTnc(@RequestBody AcceptTncReq req) throws VException {
        return userManager.acceptTnc(req);
    }

    @RequestMapping(value = "/editProfile", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public EditProfileRes editProfile(@Valid @RequestBody EditProfileReq req) throws Exception {
        return userManager.editProfile(req);
    }

    @RequestMapping(value = "/editProfileLocation", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse editProfileLocation(@Valid @RequestBody LocationInfo req) throws Exception {
        Long userId = sessionUtils.getCurrentSessionData().getUserId();
        userManager.editProfileLocation(req, userId);
        return new PlatformBasicResponse();
    }

    @RequestMapping(value = "/editStudentInfo", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public EditProfileRes editStudentInfo(@Valid @RequestBody EditProfileReq req) throws VException {
        return userManager.editStudentInfo(req);
    }

    @RequestMapping(value = "/getUsers", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public GetUsersRes getUsers(@RequestBody GetUsersReq req) throws VException {
        List<Role> allowedRoles = new ArrayList<>();
        allowedRoles.add(Role.ADMIN);
        allowedRoles.add(Role.TEACHER);
        allowedRoles.add(Role.STUDENT_CARE);
        // TODO: check for case if a teacher misuses this api
        sessionUtils.checkIfAllowedList(null, allowedRoles, true);
        return userManager.getUsers(req);
    }

    @RequestMapping(value = "/getNonStudentUsers", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public GetUsersRes getNonStudentUsers(@RequestBody GetNonUsersReq req) throws VException {
        List<Role> allowedRoles = new ArrayList<>();
        allowedRoles.add(Role.ADMIN);
        allowedRoles.add(Role.TEACHER);
        allowedRoles.add(Role.STUDENT_CARE);
        // TODO: check for case if a teacher misuses this api
        sessionUtils.checkIfAllowedList(null, allowedRoles, true);
        return userManager.getNonStudentUsers(req);
    }

//    @Deprecated
//    @RequestMapping(value = "/editContactNumber", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
//    @ResponseBody
//    public EditContactNumberRes editContactNumber(@Valid @RequestBody EditContactNumberReq req) throws VException {
//        return userManager.editContactNumber(req);
//    }

    @Deprecated
    @RequestMapping(value = "/reSendContactNumberVerificationCode", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ReSendContactNumberVerificationCodeRes reSendContactNumberVerificationCode(
            @RequestBody ReSendContactNumberVerificationCodeReq request) throws VException {
        return userManager.reSendContactNumberVerificationCode(request);
    }

    @Deprecated
    @RequestMapping(value = "/updatePhoneNumber", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PhoneNumberRes updatePhoneNumber(@RequestBody AddOrUpdatePhoneNumberReq req) throws VException {
        return userManager.updatePhoneNumber(req);
    }

    @Deprecated
    @RequestMapping(value = "/updateAndMarkActivePhoneNumber", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PhoneNumberRes updateAndMarkActivePhoneNumber(@Valid @RequestBody AddOrUpdatePhoneNumberReq req) throws VException {
        return userManager.updateAndMarkActivePhoneNumber(req);
    }

    @RequestMapping(value = "/verifyContactNumber", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ProcessVerifyContactNumberResponse verifyContactNumber(@RequestBody VerifyContactNumberReq req)
            throws VException {
        return userManager.verifyContactNumber(req);
    }

    @RequestMapping(value = "/getUserPhoneNumbers", method = RequestMethod.GET)
    @ResponseBody
    public GetUserPhoneNumbersRes getUserPhoneNumbers(@RequestParam(value = "userId") Long userId, HttpServletRequest servletRequest) throws VException {
        sessionUtils.isAllowedApp(servletRequest);
        return userManager.getUserPhoneNumbers(userId);
    }

    // @RequestMapping(value = "/updateUser", method = RequestMethod.POST, consumes
    // = MediaType.APPLICATION_JSON_VALUE, produces =
    // MediaType.APPLICATION_JSON_VALUE)
    // @ResponseBody
    // public com.vedantu.user.entity.User updateUser(@RequestBody
    // com.vedantu.user.entity.User user) throws VException {
    // return userManager.updateUser(user);

    // }
    @RequestMapping(value = "/getUserById", method = RequestMethod.GET)
    @ResponseBody
    public User getUserById(@RequestParam(value = "userId") Long userId) throws VException {
        com.vedantu.user.entity.User user = userManager.getUserByUserId(userId);
//        if(sessionUtils.isStudent(sessionUtils.getCurrentSessionData())){
//            Long callinguserId = sessionUtils.getCallingUserId();
//                if (null != callinguserId && null != user
//                        && !callinguserId.equals(userId) && null != user.getRole()
//                        && !user.getRole().equals(Role.STUDENT)) {
//                    logger.warn("Student forbidden to use this API - UserController - getUserById");
//                    //throw new ForbiddenException(ErrorCode.FORBIDDEN_ERROR, "Student forbidden to use this API");
//                }
//        }
        /* TODO: remove after testing */
        HttpSessionData sessionData = sessionUtils.getCurrentSessionData();
        if(sessionData != null){
            logger.warn("getUserById is being called by: "+sessionData.getRole());
            if(sessionData.getUserId() != null &&  userId != null && !userId.equals(sessionData.getUserId()) && sessionData.getRole().equals(Role.STUDENT)){
                logger.warn("API - getUserById: "+sessionData.getRole() + " calling another "+user.getRole());
            }
        }else{
            logger.warn("getUserById is being called by: using ADMIN TOKENS");
        }
        /* upto here */
        return pojoUtils.convertToUserPojo(user);
    }

    @RequestMapping(value = "/getUserByEmail", method = RequestMethod.GET)
    @ResponseBody
    public User getUserById(@RequestParam(value = "email") String email) throws VException {
        com.vedantu.user.entity.User user = userManager.getUserByEmail(email);
        if(sessionUtils.isStudent(sessionUtils.getCurrentSessionData())){
            Long callinguserId = sessionUtils.getCallingUserId();
            if(null != callinguserId && null != user
                    && !callinguserId.equals(user.getId())
                    && null != user.getRole()
                    && !user.getRole().equals(Role.STUDENT)) {
                logger.warn("Student forbidden to use this API - UserController - getUserByEmail");
                //throw new ForbiddenException(ErrorCode.FORBIDDEN_ERROR, "Student forbidden to use this API");
            }
        }

        return pojoUtils.convertToUserPojo(user);
    }

    @RequestMapping(value = "/getUserBasicInfoByEmail", method = RequestMethod.GET)
    @ResponseBody
    public UserBasicInfo getUserBasicInfoByEmail(@RequestParam(value = "email") String email,
                                                 @RequestParam(value = "exposeEmail", required = false) boolean exposeEmail,
                                                 HttpServletRequest servletRequest) throws VException {
        sessionUtils.isAllowedApp(servletRequest);
        UserBasicInfo userBasicInfo =  userManager.getUserBasicInfoByEmail(email, exposeEmail);
        if(sessionUtils.isStudent(sessionUtils.getCurrentSessionData())){
            Long callinguserId = sessionUtils.getCallingUserId();
            if(null != callinguserId && null != userBasicInfo
                    && !callinguserId.equals(userBasicInfo.getUserId())
                    && null != userBasicInfo.getRole()
                    && !userBasicInfo.getRole().equals(Role.STUDENT)){
                logger.warn("Student forbidden to use this API - UserController - getUserBasicInfoByEmail");
                //throw new ForbiddenException(ErrorCode.FORBIDDEN_ERROR, "Student forbidden to use this API");
            }
        }
        return userBasicInfo;
    }

    @RequestMapping(value = "/getUsersById", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<User> getUsersById(@RequestBody List<Long> userIds, @RequestParam(value = "exposeEmail", required = false) boolean exposeEmail, HttpServletRequest servletRequest) throws VException {
        sessionUtils.isAllowedApp(servletRequest);
        List<User> userList =  userManager.getUsersById(userIds, exposeEmail);
        if(sessionUtils.isStudent(sessionUtils.getCurrentSessionData())){
            for(User user : userList){
                if(null != user && null != user.getRole() && user.getRole().equals(Role.STUDENT)
                        && !user.getId().equals(sessionUtils.getCallingUserId())){
                    logger.warn("Student forbidden to use this API - UserController - getUsersById");
                    //throw new ForbiddenException(ErrorCode.FORBIDDEN_ERROR, "Student forbidden to use this API");
                }
            }
        }
        return userList;
    }

    @RequestMapping(value = "/getUsersByContactNumber", method = RequestMethod.GET)
    @ResponseBody
    public List<User> getUsersByContactNumber(@RequestParam(value = "contactNumber") String contactNumber, HttpServletRequest servletRequest)
            throws VException {
        sessionUtils.isAllowedApp(servletRequest);
        List<User> users = userManager.getUsersByContactNumber(contactNumber);
        if(sessionUtils.isStudent(sessionUtils.getCurrentSessionData())){
            for(User user : users){
                if(null != user && null != user.getRole() && user.getRole().equals(Role.STUDENT)
                        && !user.getId().equals(sessionUtils.getCallingUserId())){
                    logger.warn("Student forbidden to use this API - UserController - getUsersByContactNumber");
                    //throw new ForbiddenException(ErrorCode.FORBIDDEN_ERROR, "Student forbidden to use this API");
                }
            }

        }
        return users;
    }

    //TODO anybody can call this api from outside and use exposeEmail to extract email
    //make all apis having exposeEmail param in user subsystem to be accessible by other subsystems only
    @RequestMapping(value = "/getUserBasicInfos", method = RequestMethod.GET)
    @ResponseBody
    public List<UserBasicInfo> getUserBasicInfos(@RequestParam(value = "userIdsList") String userIdsList,
                                                 @RequestParam(value = "exposeEmail", required = false) boolean exposeEmail)
            throws VException {
        HttpSessionData sessionData = sessionUtils.getCurrentSessionData();
        List<UserBasicInfo> userBasicInfos = userManager.getUserBasicInfos(userIdsList, exposeEmail);
        if(sessionData != null){
            logger.warn("getUserBasicInfos is being called by: "+sessionData.getRole());
            for(UserBasicInfo user : userBasicInfos){
                if(null != user && null != user.getRole() && user.getRole().equals(Role.STUDENT)
                        && !user.getUserId().equals(sessionData.getUserId())){
                    logger.warn("API - getUserBasicInfos: "+sessionData.getRole()+" calling another"+user.getRole());
                    //throw new ForbiddenException(ErrorCode.FORBIDDEN_ERROR, "Student forbidden to use this API");
                }
            }
        }else{
            logger.warn("getUserBasicInfos is being called by: using ADMIN TOKENS");
        }
        return userBasicInfos;
    }

    @RequestMapping(value = "/setProfilePic", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse setProfilePic(@Valid @RequestBody SetProfilePicReq req) throws VException {
        return userManager.setProfilePic(req);
    }

    @RequestMapping(value = "/getRole", method = RequestMethod.GET)
    @ResponseBody
    public Role getRole(@RequestParam(value = "userId") Long userId) throws VException {
        return userManager.getRole(userId);
    }

    @RequestMapping(value = "/addSessionDuration", method = RequestMethod.GET)
    @ResponseBody
    public PlatformBasicResponse addSessionDuration(@RequestParam(value = "teacherId") Long teacherId,
                                                    @RequestParam(value = "duration") Long duration, @RequestParam(value = "callingUserId") Long callingUserId, HttpServletRequest servletRequest)
            throws VException {
        sessionUtils.isAllowedApp(servletRequest);
        return userManager.addSessionDuration(teacherId, duration, callingUserId);
    }

    @Deprecated
    @RequestMapping(value = "/markBlockStatus", method = RequestMethod.POST)
    @ResponseBody
    public PlatformBasicResponse markBlockStatus(@Valid @RequestBody SetBlockStatusForUserReq setBlockStatusForUserReq)
            throws VException {
        return userManager.markBlockStatus(setBlockStatusForUserReq);
    }

    @RequestMapping(value = "/getUserInfo", method = RequestMethod.GET)
    @ResponseBody
    public UserInfo getUserInfo(@RequestParam(value = "userId") Long userId) throws VException {
        sessionUtils.checkIfAllowed(null, Role.TEACHER, true);
        UserInfo userInfo = userManager.getUserInfo(userId, false);
        return userInfo;
    }

    @RequestMapping(value = "/getReferralInfo", method = RequestMethod.GET)
    @ResponseBody
    public ReferralInfoRes getReferralInfo(@RequestParam(value = "userId") Long userId, HttpServletRequest servletRequest) throws VException {
        sessionUtils.isAllowedApp(servletRequest);
        return userManager.getReferralInfo(userId);
    }

    @RequestMapping(value = "/getMemberCount", method = RequestMethod.GET)
    @ResponseBody
    public StatusCounterResponse getMemberCount(@RequestParam(value = "role") Role role,
                                                @RequestParam(value = "verifiedCustomersOnly") Boolean verifiedCustomersOnly) throws VException {
        return userManager.getMemberCount(role, verifiedCustomersOnly);
    }

    @RequestMapping(value = "/updateMemberCount", method = RequestMethod.POST, consumes = MediaType.TEXT_PLAIN_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void updateMemberCount(@RequestHeader(value = "x-amz-sns-message-type") String messageType,
                                   @RequestBody String request) throws Exception {

        logger.info(request);
        Gson gson = new Gson();
        AWSSNSSubscriptionRequest subscriptionRequest = gson.fromJson(request, AWSSNSSubscriptionRequest.class);
        if (messageType.equals("SubscriptionConfirmation")) {
            String json = null;
            logger.info("SubscribeURL = " + subscriptionRequest.getSubscribeURL());
            ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionRequest.getSubscribeURL(), HttpMethod.GET, json, false);
            logger.info(resp.getEntity(String.class));
        } else if (messageType.equals("Notification")) {
            logger.info("Notification received - SNS");
            logger.info(subscriptionRequest.toString());
            String message = subscriptionRequest.getMessage();
            userManager.updateMemberCount(message);
        }
    }

    @RequestMapping(value = "/getUserSystemIntroData", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String getUserSystemIntroData(GetUserSystemIntroReq getUserSystemIntroReq) throws VException {
        return userManager.getUserSystemIntroDataRes(getUserSystemIntroReq);
    }

    @RequestMapping(value = "/addUserSystemIntroData", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public UserSystemIntroRes addUserSystemIntroData(@Valid @RequestBody AddUserSystemIntroReq addUserSystemIntroReq, HttpServletRequest servletRequest)
            throws VException {
        sessionUtils.isAllowedApp(servletRequest);
        return userManager.addUserSystemIntroData(addUserSystemIntroReq);
    }

    @RequestMapping(value = "/incSubscriptionRequestCount/{userId}", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse incSubscriptionRequestCount(@PathVariable Long userId) throws VException {
        return userManager.incSubscriptionRequestCount(userId);
    }

    @RequestMapping(value = "/incOtfSubscriptionCount/{userId}", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse incOtfSubscriptionCount(@PathVariable Long userId) throws VException {
        return userManager.incOtfSubscriptionCount(userId);
    }

    @RequestMapping(value = "/getActiveTutorsList", method = RequestMethod.GET)
    @ResponseBody
    public GetActiveTutorsRes getActiveTutorsList(GetActiveTutorsReq req, HttpServletRequest servletRequest) throws VException {
        sessionUtils.isAllowedApp(servletRequest);
        return userManager.getActiveTutorsList(req);
    }

    @RequestMapping(value = "/getTeacherContactFromExtension", method = RequestMethod.GET)
    public String getTeacherContactFromExtension(ExtensionNumberReq req, HttpServletRequest servletRequest) throws VException {
        sessionUtils.isAllowedApp(servletRequest);
        req.verify();
        return userManager.getTeacherContactFromExtension(req);
    }

    @RequestMapping(value = "/getActiveTeacherIdList", method = RequestMethod.GET)
    @ResponseBody
    public List<TeacherListWithStatusRes> getActiveTeacherIdList(HttpServletRequest servletRequest) throws VException {
        sessionUtils.isAllowedApp(servletRequest);
        return userManager.getActiveTeacherIdList();
    }

    @RequestMapping(value = "/migratePassword", method = RequestMethod.GET)
    @ResponseBody
    public PlatformBasicResponse migratePassword(@RequestParam(value = "start") Integer start,
                                                 @RequestParam(value = "size") Integer size, @RequestParam(value = "all") Boolean all) throws Exception {
        logger.info("ENTRY:  start" + start + ", size " + size);
        start = (start == null) ? 0 : start;
        size = (size != null && size != 0) ? size : 100;

        if (all == null) {
            all = true;
        }

        userManager.migratePassword(start, size, all);
        return new PlatformBasicResponse();
    }

    @RequestMapping(value = "/addAddress", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse addAddress(@Valid @RequestBody AddUserAddressReq addUserAddressReq, HttpServletRequest servletRequest) throws Exception {
        sessionUtils.isAllowedApp(servletRequest);
        userManager.addAddress(addUserAddressReq);
        return new PlatformBasicResponse();
    }

    @RequestMapping(value = "/authenticateUserByToken", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public com.vedantu.User.User authenticateUserByToken(@RequestBody SignInByTokenReq signInByTokenReq, HttpServletRequest servletRequest) throws VException {
        sessionUtils.isAllowedApp(servletRequest);
        return userManager.authenticateUserByToken(signInByTokenReq);
    }

    @RequestMapping(value = "/createUserAuthenticationToken", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public CreateUserAuthenticationTokenRes createUserAuthenticationToken(@RequestBody CreateUserAuthenticationTokenReq createUserAuthenticationTokenReq) throws VException {
        return userManager.createUserAuthenticationToken(createUserAuthenticationTokenReq);
    }

    @RequestMapping(value = "/createUserDetailsForExistingUser", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public UserDetailsInfo createUserDetailsForExistingUser(@Valid @RequestBody ExistingUserISLRegReq req) throws VException {
        /* TODO: remove after testing */
        HttpSessionData sessionData = sessionUtils.getCurrentSessionData();
        if(sessionData != null){
            logger.warn("createUserDetailsForExistingUser is being called by: "+sessionData.getRole());
            if(sessionData.getUserId() != null && req.getUserId() != null && !req.getUserId().equals(sessionData.getUserId()) && sessionData.getRole().equals(Role.STUDENT)){
                User calledUser = getUserById(req.getUserId());
                logger.warn("API - createUserDetailsForExistingUser: "+sessionData.getRole() + " calling another "+calledUser.getRole());
            }
        }else{
            logger.warn("createUserDetailsForExistingUser is being called by: using ADMIN TOKENS");
        }
        /* upto here */
        return userManager.createUserDetailsForExistingUser(req);
    }

    @RequestMapping(value = "/createUserDetailsForExistingUserAndSendEmail", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public UserDetailsInfo createUserDetailsForExistingUserAndSendEmail(@Valid @RequestBody ExistingUserISLRegReq req) throws VException {
        sessionUtils.checkIfAllowed(req.getUserId(), Role.STUDENT, true);
        return userManager.createUserDetailsForExistingUserAndSendEmail(req);
    }

    @RequestMapping(value = "/getUserDetailsForISLInfo", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public UserDetailsInfo getUserDetailsForISLInfo(@RequestParam(value = "userId") Long userId,
                                                    @RequestParam(value = "event") String event, HttpServletRequest servletRequest) throws VException {
        sessionUtils.isAllowedApp(servletRequest);
        if(sessionUtils.isStudent(sessionUtils.getCurrentSessionData())){
            if(!sessionUtils.getCallingUserId().equals(userId)){
                logger.warn("Accessing other user data - User Controller - getUserDetailsForISLInfo " + userId);
            }
        }
        return userManager.getUserDetailsForISLInfo(userId, event);
    }

    @RequestMapping(value = "/getUserDetailsForISLInfoByEvent", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public UserDetailsInfo getUserDetailsForISLInfoByEvent(@RequestParam(value = "event", required = false) String event) throws VException {
        sessionUtils.checkIfUserLoggedIn();
        HttpSessionData sessionData = sessionUtils.getCurrentSessionData();
        return userManager.getUserDetailsForISLInfo(sessionData.getUserId(), event);
    }

    @RequestMapping(value = "/isRegisteredForISL", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public IsRegisteredForISLRes isRegisteredForISL(@RequestParam(value = "userId") Long userId,
                                                    @RequestParam(value = "event") String event) throws VException {
        return userManager.isRegisteredForISL(userId, event);
    }

    @RequestMapping(value = "/signUpForISL", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public SignUpForISLRes signUpForISL(@Valid @RequestBody SignUpForISLReq req)
            throws VException, IOException, URISyntaxException {
        return userManager.signUpForISL(req);
    }

    @RequestMapping(value = "/getUsersRegisteredForISL", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<UserDetailsInfo> getUsersRegisteredForISL(GetUsersReq req) {
        return userManager.getUsersRegisteredForISL(req);
    }

    @RequestMapping(value = "/editUserDetailsForEvent", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public UserDetailsInfo editUserDetailsForEvent(@Valid @RequestBody EditUserDetailsReq req)
            throws VException {
        sessionUtils.checkIfAllowedList(req.getUserId(), Arrays.asList(Role.STUDENT), true);
        return userManager.editUserDetailsForEvent(req);
    }

    @RequestMapping(value = "/getUserIdsRegisteredForISL", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<AttemptedUserIds> getUserIdsRegisteredForISL() {
        //return userManager.getUserIdsRegisteredForISL();
        return new ArrayList<>();
    }

    @RequestMapping(value = "/getPasswordEmptyUsersForISL", method = RequestMethod.GET, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public GetUsersRes getPasswordEmptyUsersForISL(@RequestParam Integer start,
                                                   @RequestParam Integer size, HttpServletRequest servletRequest) throws VException {
        sessionUtils.isAllowedApp(servletRequest);
        return userManager.getPasswordEmptyUsersForISL(start, size);
    }

    @RequestMapping(value = "/loginISL", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public UserDetailsForSNS loginISL(@RequestBody SignInReq req, @RequestParam(value = "event", required = false) String event) throws VException {
        return userManager.loginISL(req, event);
    }

    @Deprecated
    @RequestMapping(value = "/getUserByIdForISL", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public UserDetailsForSNS getUserByIdForISL(@RequestParam Long userId, @RequestParam(value = "event", required = false) String event) throws VException {
        logger.warn("Nowhere using API called. UserController API - getUserByIdForISL");
        //return userManager.getUserByIdForISL(userId, event);
        return null;
    }

    @RequestMapping(value = "/generatePassword", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public GeneratePasswordRes generatePassword(@RequestBody GeneratePasswordReq req, HttpServletRequest servletRequest) throws VException, IOException {
        sessionUtils.isAllowedApp(servletRequest);
        return userManager.generatePassword(req);
    }

    @RequestMapping(value = "/getUserISLStatus", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse getUserISLStatus(@RequestParam(value = "email") String email,
                                                  @RequestParam(value = "event", required = false) String event) throws VException, UnsupportedEncodingException {
        return userManager.getUserISLStatus(email, event);
    }

    @Deprecated
    @RequestMapping(value = "/getUserDetailsFromISL", method = RequestMethod.GET, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public UserDetailsForSNS getUserDetailsFromISL(@RequestParam(value = "email") String email,
                                                   @RequestParam(value = "event", required = false) String event) throws VException {
        logger.warn("Nowhere using API called. UserController API - getUserDetailsFromISL");
        //return userManager.getUserDetailsFromISL(email, event);
        return null;
    }

    @RequestMapping(value = "/userLeads/addEditUserLeads", method = RequestMethod.POST, consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    public PlatformBasicResponse addEditUserLeads(HttpServletRequest req) throws Exception {
        String jsonStr = req.getParameter("data.json");
        userLeadManager.addEditUserLeads(jsonStr);
        return new PlatformBasicResponse();
    }

    @RequestMapping(value = "/userLeads/addEditActivity", method = RequestMethod.POST, consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    public PlatformBasicResponse addEditActivity(HttpServletRequest req) throws Exception {
        String jsonStr = req.getParameter("data.json");
        userLeadManager.addEditActivity(jsonStr);
        return new PlatformBasicResponse();
    }

    @RequestMapping(value = "/userLeads/addUserLeadActivity", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public PlatformBasicResponse addUserLeadActivity(@RequestBody UserLeadActivity req) throws Exception {
        userLeadManager.addUserLeadActivity(req);
        return new PlatformBasicResponse();
    }

    @RequestMapping(value = "/userLeads/getUserLeads", method = RequestMethod.GET, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<UserLead> getUserLeads(GetUserLeadReq req) throws VException {
        sessionUtils.checkIfAllowed(null, Role.ADMIN, true);
        return userLeadManager.getUserLeads(req);
    }

    @RequestMapping(value = "/userLeads/addEditUserLeadsSys", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public UserLeadInfo addEditUserLeads(@Valid @RequestBody AddEditUserLeadReq req) throws Exception {
        return userLeadManager.addEditUserLeads(req);
    }

    @Deprecated
    @RequestMapping(value = "/userLeads/fetchUserLead", method = RequestMethod.GET, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public UserLeadInfo fetchUserLead(GetUserLeadReq req) throws VException {
        //sessionUtils.checkIfAllowed(null, Role.ADMIN, true);
        logger.warn("Nowhere using API called. UserController API - userLeads/fetchUserLead");
        //return userLeadManager.fetchUserLead(req);
        return null;
    }

    @RequestMapping(value = "/userLeads/getUserLeadsNumber", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Set<String> getUserLeadsNumber(@RequestParam(value = "key") String key, HttpServletRequest servletRequest) throws VException {
//    	sessionUtils.checkIfAllowed(null, Role.ADMIN, true);
        sessionUtils.isAllowedApp(servletRequest);

        if (StringUtils.isEmpty(key) && !key.equals("jsfvjnfvn#$omds")) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Bad Request");
        }
        return userLeadManager.getUserLeadsNumber();
    }

    @RequestMapping(value = "/getUserInfosByEmail", method = RequestMethod.GET)
    @ResponseBody
    public List<UserInfo> getUserInfosByEmail(GetUserInfosByEmailReq req, HttpServletRequest servletRequest) throws VException {
        sessionUtils.isAllowedApp(servletRequest);
        return userManager.getUserInfosByEmail(req);
    }

    @RequestMapping(value = "/getUserDashboard", method = RequestMethod.GET)
    @ResponseBody
    public GetUserDashboardRes getUserDashboard(GetUserDashboardReq req) throws VException {
        sessionUtils.checkIfAllowed(null, Role.ADMIN, true);
        return userManager.getUserDashboard(req);
    }

    @RequestMapping(value = "/userOrderOperation", method = RequestMethod.POST, consumes = MediaType.TEXT_PLAIN_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void userOrderOperation(@RequestHeader(value = "x-amz-sns-message-type") String messageType,
                                   @RequestBody String request) throws Exception {

        logger.info(request);
        Gson gson = new Gson();
        AWSSNSSubscriptionRequest subscriptionRequest = gson.fromJson(request, AWSSNSSubscriptionRequest.class);
        if (messageType.equals("SubscriptionConfirmation")) {
            String json = null;
            logger.info("SubscribeURL = " + subscriptionRequest.getSubscribeURL());
            ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionRequest.getSubscribeURL(), HttpMethod.GET, json, false);
            logger.info(resp.getEntity(String.class));
        } else if (messageType.equals("Notification")) {
            logger.info("Notification received - SNS");
            logger.info(subscriptionRequest.toString());
            String subject = subscriptionRequest.getSubject();
            SnsOrders.Subject eventType = SnsOrders.Subject.valueOf(subject);
            String message = subscriptionRequest.getMessage();
            if (eventType != null) {
                userManager.updateSalesClosedInfo(message);
            }
        }
    }

    @RequestMapping(value = "/updateUserSaleClosed", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public PlatformBasicResponse updateUserSaleClosed(@Valid @RequestBody SaleClosedUser req) throws Exception {
        return userManager.updateUserSaleClosed(req);
    }

    //    @RequestMapping(value = "/fetchUsersByCreationTimeRandomStringadsfdcfvdfdc", method = RequestMethod.GET)
//    @ResponseBody
//    public List<User> fetchUsersByCreationTime(GetUsersReq req){
//        return userManager.fetchUsersByCreationTime(req);
//    }
    @RequestMapping(value = "/getLoginData", method = RequestMethod.GET)
    @ResponseBody
    public HttpSessionData getLoginData() {

        return sessionUtils.getCurrentSessionData();
    }

    @RequestMapping(value = "/getRandomUserName", method = RequestMethod.GET)
    @ResponseBody
    public PlatformBasicResponse getRandomUserName() throws VException {
        return userManager.getRandomUserName();
    }

    /*@RequestMapping(value = "/resetadminpasswords", method = RequestMethod.POST, consumes = MediaType.TEXT_PLAIN_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void resetadminpasswords(@RequestHeader(value = "x-amz-sns-message-type") String messageType,
                                    @RequestBody String request) throws Exception {

        logger.info(request);
        Gson gson = new Gson();
        AWSSNSSubscriptionRequest subscriptionRequest = gson.fromJson(request, AWSSNSSubscriptionRequest.class);
        if (messageType.equals("SubscriptionConfirmation")) {
            String json = null;
            logger.info("SubscribeURL = " + subscriptionRequest.getSubscribeURL());
            ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionRequest.getSubscribeURL(), HttpMethod.GET, json, false);
            logger.info(resp.getEntity(String.class));
        } else if (messageType.equals("Notification")) {
            logger.info("Notification received - SNS");
            logger.info(subscriptionRequest.toString());
            userManager.resetadminpasswordsAsync();
        }
    }*/

    @RequestMapping(value = "/sendKYCLinkEmail", method = RequestMethod.GET)
    @ResponseBody
    public void sendKYCLinkEmail(@RequestParam Long userId) throws VException, UnsupportedEncodingException {
        sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
        communicationManager.sendKYCLink(userId);
    }

    @RequestMapping(value = "/markTelegramLinked", method = RequestMethod.GET)
    @ResponseBody
    public void markTelegram(@RequestParam(value = "userId", required = true) Long userId) throws VException, UnsupportedEncodingException {
        userManager.markTelegramLinked(userId);
    }

    @RequestMapping(value = "/getDEXTeachers", method = RequestMethod.GET)
    @ResponseBody
    public List<User> getDexTeachers(GetDexTeachersReq req, HttpServletRequest servletRequest) throws VException {
        sessionUtils.isAllowedApp(servletRequest);
        return userManager.getDexTeachers(req);
    }

    @RequestMapping(value = "/getDexAndTheirStatus", method = RequestMethod.GET)
    @ResponseBody
    public List<GetDexAndTheirStatusRes> getDexAndTheirStatus(GetDexTeachersReq req) {
        logger.warn("Nowhere using API called. UserController API - getDexAndTheirStatus");
        return userManager.getDexAndTheirStatus(req);
    }

    @RequestMapping(value = "/getXVedToken", method = RequestMethod.GET, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse getXVedToken(@RequestParam(value = "email") String email,
                                              @RequestParam(value = "secret") String secret) throws VException, IOException, URISyntaxException {
        if (org.apache.commons.lang3.StringUtils.equals("CM4At2ycpBSfSReH", secret)) {
            return userManager.getXVedToken(email);
        } else {
            throw new ForbiddenException(ErrorCode.ACTION_NOT_ALLOWED, "secret does not match");
        }
    }

    // TODO: Add RequestParams start and limit
    @RequestMapping(value = "/getAllAcadMentors", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public List<UserBasicInfo> getAllAcadMentors(@RequestParam(value = "start") Integer start, @RequestParam(value = "size") Integer size, HttpServletRequest servletRequest) throws VException {
        sessionUtils.isAllowedApp(servletRequest);
        logger.info("ENTRY:  start" + start + ", size " + size);
        start = (start == null) ? 0 : start;
        size = (size != null && size != 0) ? size : 100;
        return userManager.getAllAcadMentors(start, size);
    }

    @RequestMapping(value = "/getAllAcadMentorsUserObject", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public List<com.vedantu.user.entity.User> getAllAcadMentorsUserObject(@RequestParam(value = "start") Integer start, @RequestParam(value = "size") Integer size, HttpServletRequest servletRequest) throws VException {
        sessionUtils.isAllowedApp(servletRequest);
        logger.info("ENTRY:  start" + start + ", size " + size);
        start = (start == null) ? 0 : start;
        size = (size != null && size != 0) ? size : 100;
        return userManager.getAllAcadMentorsUserObject(start, size);
    }

    @RequestMapping(value = "/getBasicTeacherInfoByIds", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<TeacherInfoMinimal> getBasicTeacherInfoByIds(@RequestBody List<Long> userIds) throws VException {
        return userManager.getBasicTeacherInfoByIds(userIds);
    }

    @RequestMapping(value = "/getTestLinkByuserLoginTokenId", method = RequestMethod.GET)
    @ResponseBody
    public UserLoginToken getTestLinkByuserLoginTokenId(@RequestParam(value = "userLoginTokenId") String userLoginTokenId) throws VException {
        return userManager.getTestLinkByuserLoginTokenId(userLoginTokenId);
    }

    @RequestMapping(value = "/changeEmailByUserId", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse changeEmailByUserId(@RequestParam(value = "userId") Long userId, @RequestParam(value = "email") String email) throws VException {
        sessionUtils.checkIfAllowed(null, Role.ADMIN, true);
        return userManager.changeEmailByUserId(userId, email);
    }


    @RequestMapping(value = "/removeEmailUserId", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse removeEmailUserId(@RequestParam(value = "userId") Long userId) throws VException {
        sessionUtils.checkIfAllowed(null, Role.ADMIN, true);
        return userManager.removeEmailUserId(userId);
    }

    @RequestMapping(value = "/editUserRoleByUserId", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse editUserRoleByUserId(@RequestParam(value = "userId") Long userId, @RequestParam(value = "role") Role role) throws VException {
        sessionUtils.checkIfAllowed(null, Role.ADMIN, true);
        return userManager.editUserRoleByUserId(userId, role);
    }


    @RequestMapping(value = "/login/auth", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse getAuthTokenLogin(@RequestBody UserPreLoginVerificationInfoReq req, HttpServletRequest httpServletRequest,
                                                                HttpServletResponse httpServletResponse) throws VException, UnsupportedEncodingException {
        return userManager.getAuthTokenLogin(req, httpServletRequest, httpServletResponse);
    }

    @RequestMapping(value = "/preLoginVerification", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public UserPreLoginVerificationInfoRes preLoginVerification(@RequestBody UserPreLoginVerificationInfoReq req, HttpServletRequest httpServletRequest,
                                                                HttpServletResponse httpServletResponse) throws VException, UnsupportedEncodingException {
        return userManager.preLoginVerification(req, httpServletRequest, httpServletResponse);
    }

    @RequestMapping(value = "/preSignUpVerification", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public UserPreSignUpVerificationInfoRes preSignUpVerification(@RequestBody UserPreSignUpVerificationInfoReq req) throws VException, UnsupportedEncodingException {
        return userManager.preSignUpVerification(req);
    }

    @RequestMapping(value = "/resendPreLoginVerificationOTP", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse resendPreLoginVerificationOTP(@RequestBody UserPreLoginVerificationInfoReq req) throws VException, UnsupportedEncodingException {
        return userManager.resendPreLoginVerificationOTP(req);
    }

    @RequestMapping(value = "/verifyOTP", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ProcessVerifyContactNumberResponse verifyOTP(@RequestBody OTPVerificationReq req, HttpServletRequest request,
                                                        HttpServletResponse response) throws VException, IOException, URISyntaxException {
        return userManager.verifyOTP(req, request, response);
    }

    @RequestMapping(value = "/getLoginTokenForTeacher", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse getLoginTokenForTeacher(@RequestBody TeacherTokenRequest teacherTokenRequest,
                                                         HttpServletRequest request,
                                                         HttpServletResponse response) throws VException, IOException, URISyntaxException {

        sessionUtils.isAllowedApp(request);
        return userManager.getLoginTokenForTeacher(teacherTokenRequest.getEmail(), teacherTokenRequest.getSecret(), request, response);
    }

    @RequestMapping(value = "/verifyCounsellerOTP", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ProcessVerifyContactNumberResponse verifyCounsellerOTP(@RequestBody OTPVerificationReq req, HttpServletRequest request,
                                                                  HttpServletResponse response) throws VException, IOException, URISyntaxException {
        return userManager.verifyCounsellerOTP(req, request, response);
    }

    @RequestMapping(value = "/truecallerLogin", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public TruecallerLoginRes truecallerLogin(@RequestBody TruecallerLoginReq req, HttpServletRequest request,
                                              HttpServletResponse response) throws Exception {
        return userManager.truecallerLogin(req, request, response);
    }

    @RequestMapping(value = "/newPhoneVerification", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public NewPhoneVerificationRes newPhoneVerification(@RequestBody NewPhoneVerificationReq req) throws VException, IOException, URISyntaxException {
        return userManager.newPhoneVerification(req);
    }

    @RequestMapping(value = "/signUpUser", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public SignUpUserRes signUpUser(@RequestBody SignUpUserReq req, HttpServletRequest request,
                                    HttpServletResponse response) throws Exception {
        return userManager.signUpUser(req, request, response);
    }

    @RequestMapping(value = "/profileBuilderPostLogin", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ProfileBuilderPostLoginRes profileBuilderPostLogin(@RequestBody ProfileBuilderPostLoginReq profileBuilderReq, HttpServletRequest request,
                                                              HttpServletResponse response) throws VException, IOException, URISyntaxException {
        return userManager.profileBuilderPostLogin(profileBuilderReq, request, response);
    }

    @RequestMapping(value = "/addUserEmailSeoPdf", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public User addUserEmailSeoPdf(@RequestParam String email, @RequestParam String token) throws VException {
        if (!token.equals("4hK6958ubW7AHaWa")) {
            throw new ForbiddenException(ErrorCode.FORBIDDEN_ERROR, "Operation is not permitted");
        }
        return userManager.addUserEmailSeoPdf(email);
    }


    @RequestMapping(value = "/addEmailToUser", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse addEmailToUser(@RequestParam(value = "userId", required = true) Long userId, @RequestParam(value = "email", required = true) String email) throws VException {
        return userManager.addEmailToUser(userId, email);
    }


    @RequestMapping(value = "/getIdByContactNumber/{phoneNumber}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public UserBasicInfo getIdByContactNumber(@PathVariable("phoneNumber") String phoneNumber) {
        Long studentId = userManager.getIdByContactNumber(phoneNumber);
        UserBasicInfo userBasicInfo = new UserBasicInfo();
        userBasicInfo.setUserId(studentId);
        return userBasicInfo;
    }

    @RequestMapping(value = "/volt/validate", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse isValidVoltId(@RequestParam("id") String voltId) throws VException {
        if (userManager.isValidVoltId(voltId)) {
            if (userManager.isVoltIdUsed(voltId)) {
                throw new ConflictException(ErrorCode.USED_VOLT_ID, "VOLT ID ALREADY USED");
            }
            return new PlatformBasicResponse();
        } else {
            throw new ConflictException(ErrorCode.INVALID_VOLT_ID, "VOLT ID IS INVALID");
        }
    }

    @RequestMapping(value = "/isRegisteredForVolt", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public IsRegisteredForISLRes isRegisteredForVolt(@RequestParam(value = "eventCategory", required = false) String eventCategory,
                                                     @RequestParam("userId") Long userId,
                                                     @RequestParam(value = "event", required = false) String eventName) throws VException {
        if (StringUtils.isNotEmpty(eventName) && !eventName.equals("VOLT_2020_JAN")) {
            throw new BadRequestException(ErrorCode.SERVICE_ERROR, "EVENT IS NOT VOLT");
        }
        UserDetailsInfo info = userManager.getUserDetailsForISLInfo(userId, "VOLT_2020_JAN", eventCategory);

        if (info != null) {
            return new IsRegisteredForISLRes(VoltStatus.REGISTERED == info.getVoltStatus(), userId, info.getVoltStatus());
        } else {
            return new IsRegisteredForISLRes(false, userId, null);
        }
    }

    @RequestMapping(value = "/volt/userdetails", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<UserDetailsInfo> getUserDetailsForVolt(GetVoltUserDetailsReq req) throws VException {
        sessionUtils.checkIfAllowedList(sessionUtils.getCallingUserId(), Arrays.asList(Role.STUDENT_CARE, Role.ADMIN_VOLT, Role.ADMIN), true);
        return userManager.getUserDetailsForVolt(req);
    }


    @RequestMapping(value = "/volt/userdetails/csv", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse getUserDetailsForVoltCsv(GetVoltUserDetailsReq req) throws VException, IOException {
        sessionUtils.checkIfAllowedList(sessionUtils.getCallingUserId(), Arrays.asList(Role.ADMIN_VOLT, Role.ADMIN), false);
        String url = userManager.getUserDetailsForVoltCsv(req);
        return new PlatformBasicResponse(true, url, "");
    }

    @RequestMapping(value = "/volt/userdetails/action", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse approveActionForUserDetails(@RequestBody UserDetailsVoltApproveReq req) throws VException, IOException {
        sessionUtils.checkIfAllowedList(sessionUtils.getCallingUserId(), Arrays.asList(Role.ADMIN_VOLT, Role.ADMIN), false);
        userManager.approveActionForUserDetails(req);
        return new PlatformBasicResponse(true, "Status updated", "");
    }

    @RequestMapping(value = "/addDummyNumber", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse addDummyNumber(@RequestBody UserAddDummyNumberReq req) throws VException, IOException {
        sessionUtils.checkIfAllowedList(sessionUtils.getCallingUserId(), Arrays.asList(Role.ADMIN), false);
        userManager.addDummyNumber(req);
        return new PlatformBasicResponse(true, "Dummy number updated", "");
    }

    @RequestMapping(value = "/volt/userdetail/attempt", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public GetTestAttemptRes getUserDetailsForVolt(@RequestParam(value = "userId") Long id) throws VException {
        return userManager.getUserDetailForVolt(id);
    }

    @RequestMapping(value = "/volt/userdetails/attempt", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse setVoltTestAttemptStatus(@RequestParam(value = "userId") Long userId,
                                                          @RequestParam(value = "testId") String testId) throws VException, IOException {
        userManager.setVoltTestAttemptStatus(userId, testId);
        return new PlatformBasicResponse(true, "Attempt updated", "");
    }

    @RequestMapping(value = "/revisejee/userdetails/attempt", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse setReviseJeeTestAttemptStatus(@RequestParam(value = "userId") Long userId,
                                                          @RequestParam(value = "testId") String testId) throws VException, IOException {
        userManager.setReviseJeeTestAttemptStatus(userId, testId, EventName.REVISE_JEE_2020_MARCH.toString());
        return new PlatformBasicResponse(true, "Attempt updated", "");
    }

    @RequestMapping(value = "/revisetargetjeeneet/userdetails/attempt", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse setReviseTargetJeeNeetTestAttemptStatus(@RequestParam(value = "userId") Long userId,
                                                               @RequestParam(value = "testId") String testId) throws VException, IOException {
        userManager.setReviseJeeTestAttemptStatus(userId, testId, EventName.TARGET_JEE_NEET.toString());
        return new PlatformBasicResponse(true, "Attempt updated", "");
    }

    @RequestMapping(value = "temp/updateDummyNumberForUser", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse updateDummyNumberForUser(@RequestBody UserEmailUpdateReq req) throws VException, IOException {
        if (!req.getToken().equals("9tz7AVt7mx2nkX4p")) {
            return new PlatformBasicResponse(false, null, "Invalid token");
        }
        sessionUtils.checkIfAllowedList(sessionUtils.getCallingUserId(), Arrays.asList(Role.ADMIN), false);
        userManager.updateDummyNumberForUsers(req);
        return new PlatformBasicResponse(true, "User updated", "");
    }

    @RequestMapping(value = "temp/rectifyPhoneCode", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse rectifyPhoneCode(@RequestParam(value = "token") String token,
                                                  @RequestParam(value = "start") Integer start,
                                                  @RequestParam(value = "size") Integer size) throws VException, IOException {
        if (!"9tz7AVt7mx2nkX4p".equals(token)) {
            return new PlatformBasicResponse(false, null, "Invalid token");
        }
        sessionUtils.checkIfAllowedList(sessionUtils.getCallingUserId(), Arrays.asList(Role.ADMIN), false);
        userManager.rectifyPhoneCode(start, size);
        return new PlatformBasicResponse(true, "User updated", "");
    }

    @RequestMapping(value = "/gps/fetchCurrentLocale", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public GpsLocationResp fetchCurrentLocale(@RequestBody GpsLocationReq request) throws VException {
        GpsLocationResp gpsLocationResp = new GpsLocationResp();
        gpsLocationResp = userLocalesManager.fetchCurrentLocale(request);
        return gpsLocationResp;
    }


    @RequestMapping(value = "/temp/updateUserDetailsForVsat", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public PlatformBasicResponse createUserDetailsForVsat(@RequestBody CreateUserDetailsReq req) throws VException {
        sessionUtils.checkIfAllowed(req.getCallingUserId(), Role.ADMIN, true);

        userManager.updateUserDetailsForVsat(req);
        return new PlatformBasicResponse();
    }

    // should be accessible only to admin
    @RequestMapping(value = "/getUserLocationInfos", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<UserInfo> getUserLocationInfoForUsers(@RequestParam(value = "userIds") String userIds,
                                                      HttpServletRequest request) throws VException {
        List<Role> allowedRoles = Collections.singletonList(Role.ADMIN);
        HttpSessionData sessionData = sessionUtils.getCurrentSessionData();
        if (sessionData != null) {
            sessionUtils.checkIfAllowedList(sessionUtils.getCallingUserId(), allowedRoles, true);
        } else {
            sessionUtils.isAllowedApp(request);
        }
        return userManager.getUserLocationInfoForUsers(userIds);
    }

    @RequestMapping(value = "/getUsersParentInfoByIds", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public List<UserParentInfoRes> getUsersParentInfoByIds(@RequestParam(value = "userIds", required = true) List<Long> userIds,
                                                        @RequestParam(value = "size", required = false) Integer size,
                                                        @RequestParam(value = "start", required = false) Integer start,
                                                        HttpServletRequest request) throws VException {
        sessionUtils.isAllowedApp(request);
        return userManager.getUsersParentInfoByIds(userIds, start, size);
    }

    @RequestMapping(value = "/sendReviseJeeReminderSMS", method = RequestMethod.POST, consumes = MediaType.TEXT_PLAIN_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void sendReviseJeeReminderSMS(@RequestHeader(value = "x-amz-sns-message-type") String messageType,
                                    @RequestBody String request) throws Exception {

        logger.info(request);
        Gson gson = new Gson();
        AWSSNSSubscriptionRequest subscriptionRequest = gson.fromJson(request, AWSSNSSubscriptionRequest.class);
        if (messageType.equals("SubscriptionConfirmation")) {
            String json = null;
            logger.info("SubscribeURL = " + subscriptionRequest.getSubscribeURL());
            ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionRequest.getSubscribeURL(), HttpMethod.GET, json, false);
            logger.info(resp.getEntity(String.class));
        } else if (messageType.equals("Notification")) {
            logger.info("Notification received - SNS");
            logger.info(subscriptionRequest.toString());
            userManager.sendReviseJeeReminderSMS();
        }
    }

    @RequestMapping(value = "/orgUserSignUp", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public PlatformBasicResponse orgUserSignUp(@RequestBody SignUpUserReq req) throws VException, UnsupportedEncodingException {
        return userManager.orgUserSignUp(req);
    }

    // TODO authorize request
    @RequestMapping(value = "/getEarlyLearningTeachers", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<String> getEarlyLearningTeachers(@RequestParam(value = "type", required = false) String type, HttpServletRequest servletRequest) throws VException {
        sessionUtils.isAllowedApp(servletRequest);
        sessionUtils.checkIfAllowedList(sessionUtils.getCallingUserId(), Arrays.asList(Role.ADMIN, Role.STUDENT), true);
        return userManager.getEarlyLearningTeachers(type);
    }

    @RequestMapping(value = "/getUsersByOrgId", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<UserBasicInfo> getUsersByOrgId(@RequestParam("orgId") String orgId, @RequestParam("start") int start, @RequestParam("limit") int limit) throws VException {
        sessionUtils.checkIfAllowedList(sessionUtils.getCallingUserId(), Arrays.asList(Role.ADMIN), false);
        return userManager.getUserByOrgId(orgId, start, limit);
    }

    @RequestMapping(value = "/syncUserAndLead", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void syncUserAndLead(@RequestBody List<String> phoneList) throws VException {
        userManager.syncUserAndLead(phoneList);
    }

    @RequestMapping(value = "/getUsersByContactVGroup", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<UserBasicResponse> getUsersByContactVGroup(@RequestBody UserContactList userContactList) {
        return ResponseEntity.ok(userManager.getUserInfoByContactNumbers(userContactList.getContactNumbers(), userContactList.getRole()));

    }

    @RequestMapping(value = "/getXVedTokenUsingUserDetails", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse getXVedTokenUsingUserDetails(@RequestBody GetXVedTokenByUserDetailsReq req) throws BadRequestException, ForbiddenException, IOException, URISyntaxException, ConflictException {
        req.verify();
        if (!Arrays.asList(ConfigUtils.INSTANCE.getStringValue("instasolver.user.creation.secrets").split(",")).contains(req.getSecret())) {
            throw new ForbiddenException(ErrorCode.ACTION_NOT_ALLOWED, "secret does not match");
        }
        return userManager.getXVedTokenUsingUserDetails(req);

    }

//    @RequestMapping(value = "/migrationOfEarlyLearningTeachers", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
//    @ResponseBody
//    public PlatformBasicResponse migrationOfEarlyLearningTeachers(@RequestBody TeachersTagsUpdateReq teacherTagsUpdateReq) throws VException {
//        sessionUtils.checkIfAllowedList(sessionUtils.getCallingUserId(), Arrays.asList(Role.ADMIN), false);
//        return userManager.migrationOfELTeachers(teacherTagsUpdateReq);
//    }

    @RequestMapping(value = "/getEarlyLearningTeachersWithSearchString", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<UserBasicInfo> getEarlyLearningTeachersWithSearchString(@RequestParam(value = "earlyLearningteacherIds") List<String> earlyLearningteacherIds,
                                                                 @RequestParam(value = "nameQuery") String nameQuery) throws VException, IOException {
        sessionUtils.checkIfAllowedList(sessionUtils.getCallingUserId(), Arrays.asList(Role.ADMIN), true);
        return userManager.getEarlyLearningTeachersWithSearchString(earlyLearningteacherIds,nameQuery);
    }

    @RequestMapping(value = "/checkGivenUserIdsValidOrNot", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Boolean checkGivenUserIdsValidOrNot(@RequestBody List<Long> userIds) throws VException, IOException {
        sessionUtils.checkIfAllowedList(sessionUtils.getCallingUserId(), Arrays.asList(Role.ADMIN), Boolean.TRUE);
        return userManager.checkGivenUserIdsValidOrNot(userIds);
    }

    @RequestMapping(value = "/canAccessNewHomepage", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Boolean canAccessNewHomepage(@RequestParam("userId") Long userId) throws VException {
        sessionUtils.checkIfAllowedList(sessionUtils.getCallingUserId(), Collections.singletonList(Role.STUDENT), false);
        return userManager.getHomepageAccessBucket(userId);
    }

    @RequestMapping(value = "/vsat/clearRedisVsatEventDetails", method = RequestMethod.DELETE)
    @ResponseBody
    public PlatformBasicResponse clearRedisVsatEventDetails() throws VException {
        sessionUtils.checkIfAllowed(null, Role.ADMIN, true);
        userManager.clearRedisVsatEventDetails();
        return new PlatformBasicResponse();
    }

    @RequestMapping(value = "/vsat/getRedisVsatEventDetails", method = RequestMethod.GET)
    @ResponseBody
    public PlatformBasicResponse getRedisVsatEventDetails() throws VException {
        sessionUtils.checkIfAllowed(null, Role.ADMIN, true);
        VsatEventDetailsPojo vsatEventDetailsPojo =  userManager.getRedisVsatEventDetails();
        return new PlatformBasicResponse(true, vsatEventDetailsPojo, "");
    }

    @RequestMapping(value = "/vsat/changeRedisVsatEventDetails", method = RequestMethod.POST, consumes = org.springframework.http.MediaType.TEXT_PLAIN_VALUE ,produces = javax.ws.rs.core.MediaType.APPLICATION_JSON)
    @ResponseBody
    public void changeRedisVsatEventDetails(@RequestHeader(value = "x-amz-sns-message-type") String messageType,
                                            @RequestBody String req) throws Exception {
        Gson gson = new Gson();
        AWSSNSSubscriptionRequest subscriptionRequest = gson.fromJson(req, AWSSNSSubscriptionRequest.class);
        if (messageType.equals("SubscriptionConfirmation")) {
            String json = null;
            logger.info("SubscribeURL = " + subscriptionRequest.getSubscribeURL());
            ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionRequest.getSubscribeURL(), HttpMethod.GET, json, false);
            logger.info(resp.getEntity(String.class));
        } else if (messageType.equals("Notification")) {
            logger.info("Notification received - SNS");
            logger.info(subscriptionRequest.toString());
            if (StringUtils.isNotEmpty(subscriptionRequest.getSubject())) {
                String message = subscriptionRequest.getMessage();
                userManager.setRedisVsatEventDetails(message);
            }
        }
    }
}
