package com.vedantu.user.controllers;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.vedantu.User.Role;
import com.vedantu.exception.VException;
import com.vedantu.user.managers.StudentOnboardingManager;
import com.vedantu.user.requests.SalesVerificationAndSAMDataSetupRequest;
import com.vedantu.user.requests.SalesVerificationAndSAMInformationRequest;
import com.vedantu.user.requests.StudentOnboardingRequest;
import com.vedantu.user.responses.StudentOnboardingCompletionStatusResp;
import com.vedantu.user.responses.StudentOnboardingResponse;
import com.vedantu.util.PlatformBasicResponse;
import com.vedantu.util.security.HttpSessionUtils;

@RestController
@RequestMapping("/onboarding")
public class StudentOnboardingController {

    @Autowired
    private StudentOnboardingManager studentOnboardingManager;

    @Autowired
    private HttpSessionUtils sessionUtils;

    @RequestMapping(value = "/getOnboardingData",method = RequestMethod.POST,consumes = MediaType.APPLICATION_JSON_VALUE,produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public StudentOnboardingResponse getOnboardingData(@RequestBody StudentOnboardingRequest studentOnboardingRequest,HttpServletRequest request, HttpServletResponse response)
            throws VException, IOException, URISyntaxException {
        return studentOnboardingManager.getOnboardingData(studentOnboardingRequest,request,response);
    }

    @RequestMapping(value = "/setOnboardingData",method = RequestMethod.POST,consumes = MediaType.APPLICATION_JSON_VALUE,produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public StudentOnboardingResponse setOnboardingData(@RequestBody StudentOnboardingRequest studentOnboardingRequest,HttpServletRequest request, HttpServletResponse response)
                                                        throws VException, IOException, URISyntaxException {
        return studentOnboardingManager.setOnboardingData(studentOnboardingRequest,request,response);
    }

    @RequestMapping(value = "/insertInitialOnboardingInformation",method = RequestMethod.POST,consumes = MediaType.APPLICATION_JSON_VALUE,produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse insertInitialOnboardingInformation(@RequestBody StudentOnboardingRequest studentOnboardingRequest, HttpServletRequest request, HttpServletResponse response)
                                                                    throws VException, IOException, URISyntaxException {
        sessionUtils.checkIfAllowed(studentOnboardingRequest.getCallingUserId(), Role.STUDENT, Boolean.TRUE);
        return studentOnboardingManager.insertInitialOnboardingInformation(studentOnboardingRequest,request,response);
    }

    @RequestMapping(value = "/getSalesVerificationAndSAMData",method = RequestMethod.POST,consumes = MediaType.APPLICATION_JSON_VALUE,produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<StudentOnboardingResponse> getSalesVerificationAndSAMData(@RequestBody SalesVerificationAndSAMInformationRequest salesVerificationAndSAMInformationRequest) throws VException {
        sessionUtils.checkIfAllowed(salesVerificationAndSAMInformationRequest.getCallingUserId(), Role.ADMIN, Boolean.TRUE);
        salesVerificationAndSAMInformationRequest.verify();
        return studentOnboardingManager.getSalesVerificationAndSAMData(salesVerificationAndSAMInformationRequest);
    }

    @RequestMapping(value = "/setSalesVerificationAndSAMData",method = RequestMethod.POST,consumes = MediaType.APPLICATION_JSON_VALUE,produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse setSalesVerificationAndSAMData(@RequestBody SalesVerificationAndSAMDataSetupRequest salesVerificationAndSAMDataSetupRequest) throws VException {
        sessionUtils.checkIfAllowed(salesVerificationAndSAMDataSetupRequest.getCallingUserId(), Role.ADMIN, Boolean.TRUE);
        salesVerificationAndSAMDataSetupRequest.verify();
        return studentOnboardingManager.setSalesVerificationAndSAMData(salesVerificationAndSAMDataSetupRequest);
    }

    @RequestMapping(value = "/migrateDeviceAndLocationData",method = RequestMethod.POST,consumes = MediaType.APPLICATION_JSON_VALUE,produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void migrateDeviceAndLocationData() {
        studentOnboardingManager.migrateDeviceAndLocationData();
    }
    
    @RequestMapping(value = "/getCompletionStatus/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public StudentOnboardingCompletionStatusResp getCompletionStatus(@PathVariable("id") String id) throws VException {
    	sessionUtils.checkIfAllowed(Long.valueOf(id), Role.STUDENT, Boolean.FALSE);
    	return studentOnboardingManager.getCompletionStatus(Long.valueOf(id));
    }

}
