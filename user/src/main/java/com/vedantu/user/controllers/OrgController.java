/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.user.controllers;

import com.vedantu.User.Role;
import com.vedantu.User.response.CreateUserResponse;
import com.vedantu.User.response.GetUserProfileRes;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.VException;
import com.vedantu.exception.NotFoundException;
import com.vedantu.exception.ConflictException;
import com.vedantu.user.entity.Org;
import com.vedantu.user.managers.OrgManager;
import com.vedantu.util.PlatformBasicResponse;
import com.vedantu.util.security.HttpSessionUtils;
import javax.validation.Valid;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.regex.Pattern;

/**
 *
 * @author ajith
 */
@RestController
@RequestMapping("/org")
public class OrgController {

    @Autowired
    private HttpSessionUtils sessionUtils;

    @Autowired
    private OrgManager orgManager;

    private static final Pattern ORG_ID_REGEX_Pattern = Pattern.compile("^[a-zA-Z0-9\\-]{1,128}$");

    @RequestMapping(value = "/add", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public  Org add(@Valid @RequestBody Org org) throws VException {
        sessionUtils.checkIfAllowed(null, Role.ADMIN, true);  
        if (StringUtils.isEmpty(org.getId()) || StringUtils.isEmpty(org.getName())) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "orgId and name cannot be empty");
        } else if (org.getId().contains(" ")) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "please do not give any spaces in orgId");
        } else if(!ORG_ID_REGEX_Pattern.matcher(org.getId()).matches()){
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "org id can consist of alphanumeric characters and hyphen only");
        }
        return orgManager.add(org);
    }

    @RequestMapping(value = "/getById", method = RequestMethod.GET)
    @ResponseBody
    public Org getById(@RequestParam String orgId) throws VException {
        return orgManager.getOrg(orgId);
    }

    @RequestMapping(value = "/getOrgIds", method = RequestMethod.GET)
    @ResponseBody
    public List<Org> getOrgIds(@RequestParam Integer start, @RequestParam Integer limit) throws VException {
        sessionUtils.checkIfAllowedList(null, Arrays.asList(Role.ADMIN, Role.STUDENT_CARE), Boolean.TRUE);
        return orgManager.getOrgIds(start,limit);
    }

    @RequestMapping(value = "/addEditOrgIdToUser", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse addEditOrgIdToUser(@RequestParam(value = "orgId", required = true) String orgId, @RequestParam(value = "userId", required = true) Long userId) throws VException {
        sessionUtils.checkIfAllowedList(null, Arrays.asList(Role.ADMIN, Role.STUDENT_CARE), Boolean.TRUE);
        return orgManager.addEditOrgIdToUser(orgId, userId);
    }

}
