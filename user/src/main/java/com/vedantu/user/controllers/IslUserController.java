package com.vedantu.user.controllers;

import com.vedantu.User.Role;
import com.vedantu.User.request.GetUsersReq;
import com.vedantu.User.response.IsRegisteredForISLRes;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.VException;
import com.vedantu.user.managers.IslUserManager;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.security.HttpSessionData;
import com.vedantu.util.security.HttpSessionUtils;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;

/**
 * @author MNPK
 */

@RestController
@RequestMapping("/isl")
public class IslUserController {
    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(UserPasswordController.class);

    @Autowired
    private HttpSessionUtils sessionUtils;

    @Autowired
    private IslUserManager islUserManager;

    @RequestMapping(value = "/isRegisteredForISL", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public IsRegisteredForISLRes isRegisteredForISL(@RequestParam(value = "event", required = false) String event) throws VException {
        sessionUtils.checkIfUserLoggedIn();
        HttpSessionData httpSessionData = sessionUtils.getCurrentSessionData();
        return islUserManager.isRegisteredForISL(httpSessionData.getUserId(), event);
    }

    @RequestMapping(value = "/isRegisteredForVolt", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public IsRegisteredForISLRes isRegisteredForVolt(@RequestParam(value = "event", required = false) String event) throws VException {
        sessionUtils.checkIfUserLoggedIn();
        HttpSessionData httpSessionData = sessionUtils.getCurrentSessionData();
        if (StringUtils.isNotEmpty(event) && !event.equals("VOLT_2020_JAN")) {
            throw new BadRequestException(ErrorCode.SERVICE_ERROR, "EVENT IS NOT VOLT");
        }
        return islUserManager.isRegisteredForVolt(httpSessionData.getUserId(), event);
    }

    @RequestMapping(value = "/getUsersRegisteredForISL", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String getUsersRegisteredForISL(GetUsersReq req) throws VException {
        sessionUtils.checkIfAllowedList(req.getCallingUserId(), Arrays.asList(Role.ADMIN, Role.STUDENT_CARE), Boolean.FALSE);
        return islUserManager.getUsersRegisteredForISL(req);
    }

}
