package com.vedantu.user.controllers;

import com.vedantu.user.dao.MongoClientFactory;
import com.vedantu.user.entity.Testing;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/test")
public class TestingController {

    @Autowired
    private MongoClientFactory mongoClientFactory;

    @RequestMapping("/insert")
    public boolean insert(){
        Testing testing = new Testing();
        testing.setUserId("1");
        mongoClientFactory.getMongoOperations().insert(testing);
        return true;
    }

    @RequestMapping("/get")
    public List<Testing> get(){
        return mongoClientFactory.getMongoOperations().find(new Query(), Testing.class);
    }
}
