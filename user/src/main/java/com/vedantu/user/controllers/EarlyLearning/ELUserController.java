package com.vedantu.user.controllers.EarlyLearning;

import java.util.Arrays;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.vedantu.User.Role;
import com.vedantu.User.Pojo.ELTeachersProficiencies;
import com.vedantu.exception.VException;
import com.vedantu.subscription.enums.EarlyLearningCourseType;
import com.vedantu.user.managers.earlylearning.UserManager;
import com.vedantu.util.LogFactory;
import com.vedantu.util.security.HttpSessionUtils;

@RestController
@RequestMapping("eluser")
public class ELUserController {

	private Logger logger = LogFactory.getLogger(ELUserController.class);

	@Autowired
	@Qualifier("ELUserManager")
	private UserManager elUserManager;

	@Autowired
	HttpSessionUtils sessionUtils;

	@RequestMapping(value = "/getELTeacherProficiency", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ELTeachersProficiencies getEarlyLearningTeacherProficiency(
			@RequestParam(value = "type", required = false) EarlyLearningCourseType type) throws VException {
		sessionUtils.checkIfAllowedList(sessionUtils.getCallingUserId(), Arrays.asList(Role.ADMIN, Role.STUDENT), true);
		return elUserManager.getELTeachersProficiencies(type);
	}
}
