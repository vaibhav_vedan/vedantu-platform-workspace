package com.vedantu.user.controllers;


import com.google.gson.Gson;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.User.Role;
import com.vedantu.aws.pojo.AWSSNSSubscriptionRequest;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.VException;
import com.vedantu.user.managers.VQuizUserManager;
import com.vedantu.util.LogFactory;
import com.vedantu.util.PlatformBasicResponse;
import com.vedantu.util.StringUtils;
import com.vedantu.util.WebUtils;
import com.vedantu.util.enums.SNSTopic;
import com.vedantu.util.security.HttpSessionUtils;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.web.bind.annotation.*;

import java.io.UnsupportedEncodingException;
import java.util.Map;

/**
 * @author mnpk
 */

@EnableAsync
@RestController
@RequestMapping("/vquiz")
public class VQuizUserController {

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(VQuizUserController.class);

    @Autowired
    private HttpSessionUtils sessionUtils;

    @Autowired
    private VQuizUserManager vQuizUserManager;

    @RequestMapping(value = "/exportWinnersCsv", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse exportWinnersCsv(@RequestBody Map<String, String> requestParams) throws VException, UnsupportedEncodingException {
        logger.info("requestParams : " + requestParams);
        sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.FALSE);
        return vQuizUserManager.exportWinnersCsv(requestParams);
    }

}
