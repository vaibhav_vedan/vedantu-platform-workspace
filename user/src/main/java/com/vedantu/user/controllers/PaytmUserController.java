package com.vedantu.user.controllers;

import com.vedantu.User.request.PaytmSignInReq;
import com.vedantu.User.request.SignInByTokenReq;
import com.vedantu.User.request.SignInReq;
import com.vedantu.exception.VException;
import com.vedantu.user.helper.BotFilter;
import com.vedantu.user.managers.login.PaytmUserManager;
import com.vedantu.user.managers.login.UserLoginManager;
import com.vedantu.util.LogFactory;
import com.vedantu.util.security.HttpSessionData;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URISyntaxException;

@RestController
@RequestMapping("/paytm/")
public class PaytmUserController {

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(PaytmUserController.class);

    @Autowired
    private PaytmUserManager paytmUserManager;

    @Autowired
    private BotFilter botFilter;


    @RequestMapping(value = "/authenticateUser", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public HttpSessionData authenticateUser(@RequestBody PaytmSignInReq requestBody, HttpServletRequest request,
                                   HttpServletResponse response) throws Exception {
        requestBody.verify();
        try {
            String ipAddress = request.getHeader("X_FORWARDED_FOR");
            if (ipAddress == null) {
                ipAddress = request.getRemoteAddr();
            }
            if (ipAddress != null) {
                logger.info("ip address of the user trying to login with paytm:" + ipAddress);
            }
          //  requestBody.setIpAddress();
        } catch (Exception e) {
            logger.warn(e);
        }

        return paytmUserManager.authenticateUser(requestBody, request, response);



    }



}
