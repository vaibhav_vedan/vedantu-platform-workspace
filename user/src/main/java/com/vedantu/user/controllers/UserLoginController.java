package com.vedantu.user.controllers;

import com.vedantu.User.request.SignInByTokenReq;
import com.vedantu.User.request.SignInReq;
import com.vedantu.User.response.ProcessVerifyContactNumberResponse;
import com.vedantu.exception.VException;
import com.vedantu.user.helper.BotFilter;
import com.vedantu.user.managers.login.UserLoginManager;
import com.vedantu.user.requests.OTPVerificationReq;
import com.vedantu.util.LogFactory;
import com.vedantu.util.security.HttpSessionData;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URISyntaxException;

@RestController
@RequestMapping("/login")
public class UserLoginController {

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(UserLoginController.class);

    @Autowired
    private UserLoginManager userLoginManager;

    @Autowired
    private BotFilter botFilter;


    @RequestMapping(value = "/authenticateUser", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public HttpSessionData authenticateUser(@RequestBody SignInReq requestBody, HttpServletRequest request,
                                            HttpServletResponse response) throws VException, IOException, URISyntaxException {
        try {
            String ipAddress = request.getHeader("X_FORWARDED_FOR");
            if (ipAddress == null) {
                ipAddress = request.getRemoteAddr();
            }
            if (ipAddress != null) {
                logger.info("ip address of the user trying to login:" + ipAddress);
            }
        } catch (Exception e) {
            logger.warn(e);
        }
        return userLoginManager.authenticateUser(requestBody, request, response);
    }

    @RequestMapping(value = "/loginByToken", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public HttpSessionData loginByToken(@RequestBody SignInByTokenReq requestBody, HttpServletRequest request,
                                        HttpServletResponse response) throws VException, IOException, URISyntaxException {
        return userLoginManager.authenticateUserByToken(requestBody, request, response);
    }

    @RequestMapping(value = "/verifyOTP", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ProcessVerifyContactNumberResponse verifyOTP(@RequestBody OTPVerificationReq req, HttpServletRequest request,
                                                        HttpServletResponse response) throws VException, IOException, URISyntaxException {
        return userLoginManager.verifyOTP(req, request, response);
    }

}
