package com.vedantu.user.controllers;

import com.vedantu.User.Role;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.ForbiddenException;
import com.vedantu.exception.VException;
import com.vedantu.user.managers.UserReferralManager;
import com.vedantu.util.LogFactory;
import com.vedantu.util.enums.ReferralType;
import com.vedantu.util.security.HttpSessionData;
import com.vedantu.util.security.HttpSessionUtils;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

/**
 * @author MNPK
 */

@RestController
@RequestMapping("/referral")
public class UserReferralController {

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(UserReferralController.class);

    @Autowired
    private HttpSessionUtils sessionUtils;

    @Autowired
    private UserReferralManager userReferralManager;


    @RequestMapping(value = "/getLink", method = RequestMethod.GET)
    @ResponseBody
    public String getLink(@RequestParam(value = "type", required = false) ReferralType type) throws VException, NoSuchPaddingException, InvalidAlgorithmParameterException, NoSuchAlgorithmException, IllegalBlockSizeException, BadPaddingException, InvalidKeyException {
        HttpSessionData httpSessionData = sessionUtils.getCurrentSessionData();
        if (httpSessionData == null || !httpSessionData.getRole().equals(Role.STUDENT)) {
            throw new ForbiddenException(ErrorCode.ACTION_NOT_ALLOWED, "action not allowed, api only for students");
        }
        return userReferralManager.getLink(httpSessionData.getUserId(), type);
    }

}
