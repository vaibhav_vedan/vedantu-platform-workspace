package com.vedantu.user.controllers;

import com.vedantu.User.request.ChangePasswordReq;
import com.vedantu.User.request.ForgotPasswordReq;
import com.vedantu.User.request.GeneratePasswordReq;
import com.vedantu.User.request.ManualResetPasswordReq;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.VException;
import com.vedantu.user.managers.UserPasswordManager;
import com.vedantu.util.LogFactory;
import com.vedantu.util.PlatformBasicResponse;
import com.vedantu.util.security.HttpSessionData;
import com.vedantu.util.security.HttpSessionUtils;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;


@RestController
@RequestMapping("/password")
public class UserPasswordController {

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(UserPasswordController.class);

    @Autowired
    private UserPasswordManager userPasswordManager;

    @Autowired
    private HttpSessionUtils sessionUtils;


    @RequestMapping(value = "/forgotPassword", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse forgotPassword(@RequestBody ForgotPasswordReq req) throws VException, UnsupportedEncodingException {
        return userPasswordManager.forgotPassword(req);
    }

    @RequestMapping(value = "/changePassword", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String changePassword(@Valid @RequestBody ChangePasswordReq req, HttpServletRequest request, HttpServletResponse response) throws VException, IOException, URISyntaxException {
        sessionUtils.checkIfUserLoggedIn();
        req.setUserId(sessionUtils.getCurrentSessionData().getUserId());
        return userPasswordManager.changePassword(req,request,response);
    }

    @RequestMapping(value = "/generatePassword", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse generatePassword(@RequestBody GeneratePasswordReq req) throws VException, IOException {
        return userPasswordManager.generatePassword(req);
    }

    @RequestMapping(value = "/resetPassword", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse resetPassword(@RequestBody ManualResetPasswordReq req) throws VException, IOException {
        return userPasswordManager.resetPassword(req);
    }




}
