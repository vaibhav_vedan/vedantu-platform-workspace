package com.vedantu.user.controllers;

import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.VException;
import com.vedantu.user.managers.GAIdManager;
import com.vedantu.user.requests.GAIdRequest;
import com.vedantu.user.responses.GAIdResponse;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Ausaf
 */

@RestController
@RequestMapping("/GA_Id")
public class GAIdController
{
    @Autowired
    private GAIdManager gaIdManager;

    @Autowired
    private LogFactory logFactory;

    private final Logger logger = logFactory.getLogger(GAIdController.class);

    @RequestMapping(value = "/checkIfLoggingFirstTime", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GAIdResponse> checkIfLoggingFirstTime(@RequestBody GAIdRequest gaIdRequest) throws VException
    {
        GAIdResponse gaIdResponse = new GAIdResponse();
        try
        {
            logger.info("GAID Controller, method : checkIfLoggingFirstTime, GAIdRequest : {}", gaIdRequest);
            if (gaIdRequest == null)
                throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Null GAIdRequest");

            if (StringUtils.isEmpty(gaIdRequest.getUserId()) || StringUtils.isEmpty(gaIdRequest.getGa_id()) || StringUtils.isEmpty(gaIdRequest.getAppVersionCode()))
                throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "UserId or GAId or appVersion null/empty");

            gaIdResponse = gaIdManager.checkIfLoggingFirstTime(gaIdRequest.getUserId(), gaIdRequest.getGa_id(), gaIdRequest.getAppVersionCode());
        }
        catch (BadRequestException e)
        {
            logger.error("Bad Request " + e.toString());
            return new ResponseEntity(e.getErrorMessage(), HttpStatus.BAD_REQUEST);
        }
        catch (Exception e)
        {
            logger.error("Could not get logging info due to : {}", e.getMessage());
            return new ResponseEntity(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return ResponseEntity.ok(gaIdResponse);
    }
    
    
    @RequestMapping(value = "/appRegistrationCreationTime", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public String appRegistrationCreationTime(@RequestParam(value = "userId", required = true) String userId, @RequestParam(value = "gaid", required = false)
    	String gaid) throws VException{
    	String creationTime = "";
    	creationTime = gaIdManager.appRegistrationCreationTime(userId, gaid);
    	return creationTime;
    }
    
}
