package com.vedantu.user.controllers;

import com.vedantu.User.Role;
import com.vedantu.User.User;
import com.vedantu.exception.VException;
import com.vedantu.user.managers.LOAMAmbassadorUserManager;
import com.vedantu.user.requests.UserByIdReq;
import com.vedantu.user.utils.PojoUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.security.HttpSessionUtils;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/*
@Author: Sanket Jain
@Reviewer: Arun Dhwaj
@Reviewer_comment:
    i) Exposed API, can be reduced to, with help to post parameters.
    ii) Variable name should be enough to explicit the symantics.
    Like: ExposeEmail, can be rename to exposeContactInfo.
*/

@RestController
@RequestMapping("loam")
public class LOAMAmbassadorUserController
{
    @Autowired
    private PojoUtils pojoUtils;
    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(LOAMAmbassadorUserController.class);

    @Autowired
    private LOAMAmbassadorUserManager loamAmbassadorUserManager;

    @Autowired
    private HttpSessionUtils sessionUtils;


    @RequestMapping(value = "/loam_getUsersByIds", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<User> getUsersByIdMentor(@RequestBody UserByIdReq req, @RequestParam(value = "exposeContactInfo", required = false) boolean exposeContactInfo) throws VException
    {
        List<Role> allowedRoles = new ArrayList<>();
        allowedRoles.add(Role.ADMIN);
        allowedRoles.add(Role.STUDENT_CARE);
        allowedRoles.add(Role.TEACHER);
        sessionUtils.checkIfAllowedList(null, allowedRoles, true);
        return loamAmbassadorUserManager.loam_getUsersById(req.getUserIds(), exposeContactInfo, req.getCallerType());
    }

    @RequestMapping(value = "/loam_getUserByEmail", method = RequestMethod.GET)
    @ResponseBody
    public User getUserById(@RequestParam(value = "email") String email) throws VException {
        List<Role> allowedRoles = new ArrayList<>();
        allowedRoles.add(Role.ADMIN);
        allowedRoles.add(Role.STUDENT_CARE);
        allowedRoles.add(Role.TEACHER);
        sessionUtils.checkIfAllowedList(null, allowedRoles, true);
        com.vedantu.user.entity.User user = loamAmbassadorUserManager.loam_getUserByEmail(email);
        return pojoUtils.convertToUserPojo(user);
    }

    @Deprecated
    @RequestMapping(value = "/loam_getAcadMentorUsers", method = RequestMethod.GET)
    @ResponseBody
    public List<User> loam_getAcadMentorUsers() throws VException {
        logger.warn("Nowhere using API called. LOAMAmbassadorUserController API - loam/loam_getAcadMentorUsers");
        //return loamAmbassadorUserManager.loam_getAcadMentorUsers();
        return new ArrayList<>();
    }

    @RequestMapping(value = "/loam_getDoubtResolverData", method = RequestMethod.GET)
    @ResponseBody
    public List<User> loam_getDoubtResolverData(@RequestParam(value = "fromTime") Long fromTime, @RequestParam(value = "thruTime") Long thruTime,
                                                @RequestParam(value = "start") Integer start, @RequestParam(value = "size") Integer size) throws VException {
        return new ArrayList<>();
        //        return loamAmbassadorUserManager.loam_getDoubtResolverData(fromTime, thruTime, start, size);
    }

//    @RequestMapping(value = "/loam_updateTeacherRoleToSuperMentor", method = RequestMethod.GET)
//    @ResponseBody
//    public com.vedantu.user.entity.User loam_updateTeacherRoleToSuperMentor(@RequestParam(value = "userId") Long userId, @RequestParam(value = "callingUserId") Long callerId) throws VException {
//        return loamAmbassadorUserManager.loam_updateTeacherRoleToSuperMentor(userId, callerId);
//    }

}
