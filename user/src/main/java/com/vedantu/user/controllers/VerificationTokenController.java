/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.user.controllers;

import com.vedantu.User.Role;
import com.vedantu.User.VerificationTokenType;
import com.vedantu.User.response.ProcessVerificationResponse;
import com.vedantu.exception.VException;
import com.vedantu.user.entity.VerificationTokenII;
import com.vedantu.user.managers.VerificationTokenManager;
import com.vedantu.util.LogFactory;
import com.vedantu.util.security.HttpSessionUtils;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 *
 * @author somil
 */
@RestController
@RequestMapping("/token")
public class VerificationTokenController {

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(VerificationTokenController.class);

    @Autowired
    VerificationTokenManager tokenManager;

    @Autowired
    HttpSessionUtils sessionUtils;
    
    @RequestMapping(value = "/processToken", method = RequestMethod.GET)
    @ResponseBody
    public ProcessVerificationResponse processToken(@RequestParam(value = "tokenCode", required = false) String tokenCode,
            @RequestParam(value = "password", required = false) String password) throws VException {
        return tokenManager.processToken(tokenCode, password);
    }
    
    @RequestMapping(value = "/getEmailToken", method = RequestMethod.GET)
    @ResponseBody
    public String getEmailToken(@RequestParam(value = "userId") Long userId, @RequestParam(value = "email") String email, @RequestParam(value = "verificationTokenType") VerificationTokenType verificationTokenType, @RequestParam(value = "callingUserId") Long callingUserId) {
        return tokenManager.getEmailToken(userId, email, verificationTokenType, callingUserId);
    }

    @RequestMapping(value = "/getVerificationCodes", method = RequestMethod.GET)
    @ResponseBody
    public List<VerificationTokenII> getVerificationCodes(@RequestParam(value = "start") Integer start,
                                                          @RequestParam(value = "limit") Integer limit, @RequestParam(value = "emailIdOrContactNumber", required = false) String emailIdOrContactNumber ) throws VException {
        sessionUtils.checkIfAllowed(sessionUtils.getCallingUserId(), Role.ADMIN, true);
        return tokenManager.getVerificationCodes(start,limit,emailIdOrContactNumber);
    }



}
