/**
 * 
 */
package com.vedantu.user.managers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import com.google.gson.Gson;
import com.vedantu.util.enums.SNSTopic;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.byteowls.jopencage.model.JOpenCageResponse;
import com.byteowls.jopencage.model.JOpenCageReverseRequest;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.VException;
import com.vedantu.user.dao.RedisDAO;
import com.vedantu.user.dao.UserDAO;
import com.vedantu.user.dao.UserLocalesDAO;
import com.vedantu.user.entity.UserGPSLocales;
import com.vedantu.user.pojo.GPSLocalePojo;
import com.vedantu.user.requests.GpsLocationReq;
import com.vedantu.user.responses.GpsLocationResp;
import com.vedantu.user.responses.OpenCageResponse;
import com.vedantu.user.utils.OpenCageUtility;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.security.HttpSessionUtils;
/**
 * @author vedantu
 *
 */
@Service
public class UserLocalesManager {

	@Autowired
	private LogFactory logFactory;
	@SuppressWarnings("static-access")
	private final Logger logger = logFactory.getLogger(UserLocalesManager.class);

	@Autowired
	private UserDAO userDAO;

	@Autowired
	private UserLocalesDAO userLocalesDAO;

	@Autowired
	private RedisDAO redisDAO;

	@Autowired
	HttpSessionUtils sessionUtils;

	private static String openCageAPIKey =  ConfigUtils.INSTANCE.getStringValue("OPENCAGE_API_KEY");
	String getNoOfDaysFromConfig = ConfigUtils.INSTANCE.getStringValue("OPENCAGE_API_CALL_NO_OF_DAYS");


	@Autowired
	private AwsSNSManager awsSNSManager;


	private UserGPSLocales fetchUserGPSLocales(String userId) throws VException{
		UserGPSLocales userGPSLocales = new UserGPSLocales();
		if(StringUtils.isNotEmpty(userId)) {
			userGPSLocales = userLocalesDAO.fetchUserGPSLocales(userId);
		}else if(StringUtils.isNotEmpty(sessionUtils.getCallingUserId().toString())){
			userGPSLocales = userLocalesDAO.fetchUserGPSLocales(sessionUtils.getCallingUserId().toString());
		}else {
			throw new VException(ErrorCode.EMPTY_PATTERN, "User_Id cannot be null or empty");
		}
		return userGPSLocales;
	}


	private OpenCageResponse fetchFromOpenCage(GpsLocationReq gpsLocationReq) throws VException{

		//		TODO:handle error messgaes if any

		OpenCageUtility jOpenCageGeocoder = new OpenCageUtility(openCageAPIKey);
		OpenCageResponse openCageResponse = new OpenCageResponse();
		JOpenCageReverseRequest request = new JOpenCageReverseRequest(Double.valueOf(gpsLocationReq.getUserLocale().getLatitude())
				, Double.valueOf(gpsLocationReq.getUserLocale().getLongitude()));
		request.setLanguage("es"); // prioritize results in a specific language using an IETF format language code
		request.setNoDedupe(true); // don't return duplicate results
		request.setLimit(1); // only return the first 1 results (default is 10)
		request.setNoAnnotations(true); // exclude additional info such as calling code, timezone, and currency
		request.setMinConfidence(3); // restrict to results with a confidence rating of at least 3 (out of 10)

		JOpenCageResponse response = jOpenCageGeocoder.reverse(request);
		String formattedAddress = "";
		// get the formatted address of the first result:
		if(null != response) {
			if(null !=response.getFirstComponents() && null != response.getFirstComponents().getCity() && !response.getFirstComponents().getCity().isEmpty()) {
				logger.info("city from open cage = "+gpsLocationReq.getUserId()+" "+response.getFirstComponents().getCity());
				openCageResponse.setCity(response.getFirstComponents().getCity());
				if(null != response.getFirstComponents().getState() && !response.getFirstComponents().getState().isEmpty())
					openCageResponse.setState(response.getFirstComponents().getState());	
				if(null != response.getFirstComponents().getCountry() && !response.getFirstComponents().getCountry().isEmpty())
					openCageResponse.setCountry(response.getFirstComponents().getCountry());	

			}

		}else {
			openCageResponse.setCity("");
		}
		openCageResponse.setUserId(gpsLocationReq.getUserId());
		openCageResponse.setLatitude(gpsLocationReq.getUserLocale().getLatitude());
		openCageResponse.setLongitude(gpsLocationReq.getUserLocale().getLongitude());
		logger.info("openCageResponse for "+gpsLocationReq.getUserId()+" "+openCageResponse);
		return openCageResponse;

	}

	public static GpsLocationResp convert(OpenCageResponse response) {

		return GpsLocationResp.builder().
				latestLocale(convertGPSLocalePojo(response)).userId(response.getUserId()).build();
	}

	public static GPSLocalePojo convertGPSLocalePojo(OpenCageResponse response) {
		Long currentTime = System.currentTimeMillis();
		return GPSLocalePojo.builder()
				.latitude(response.getLatitude())
				.longitude(response.getLongitude())
				.city(response.getCity())
				.capturedTime(currentTime)
				.build();
	}


	public GpsLocationResp fetchCurrentLocale(GpsLocationReq gpsReq) throws VException {
		GpsLocationResp gpsLocationResp = new GpsLocationResp();
		OpenCageResponse openCageResp = new OpenCageResponse();
		Boolean setRetry = false;
		if(null != gpsReq && StringUtils.isNotEmpty(gpsReq.getUserId())
				&& StringUtils.isNotEmpty(gpsReq.getUserLocale().getLatitude())
				&& StringUtils.isNotEmpty(gpsReq.getUserLocale().getLongitude())){
			UserGPSLocales entity = new UserGPSLocales();
			entity = fetchUserGPSLocales(gpsReq.getUserId());
			UserGPSLocales newUserGPSLocales = new UserGPSLocales();
			GPSLocalePojo gpsLocalePojo = new GPSLocalePojo();
			if(null == entity || null == entity.getId() || entity.getId().isEmpty()) { // new user entry
				logger.info("This is a new user and first time logging lat long");
				openCageResp = fetchFromOpenCage(gpsReq);
				if(null != openCageResp && StringUtils.isNotEmpty(openCageResp.getCity())) {//open cage responded
					gpsLocalePojo = convertGPSLocalePojo(openCageResp);
					logger.info("gpsLocalePojo first time = "+gpsLocalePojo);
				}else {//open cage failed to set city
					logger.info("Could not fetch new data from Open Cage, cannot set previous values as not record found in DB");
					setRetry = true;
					openCageResp.setUserId(gpsReq.getUserId());
					openCageResp.setLatitude(gpsReq.getUserLocale().getLatitude());
					openCageResp.setLongitude(gpsReq.getUserLocale().getLongitude());
					openCageResp.setCity("");
					gpsLocalePojo = convertGPSLocalePojo(openCageResp);
				}
				gpsLocationResp = condition1(gpsLocalePojo,openCageResp);
				gpsLocationResp.setRetryRequired(setRetry);
			}else { // existing user entry
				logger.info("Existing user --------->");
				Long currTime = System.currentTimeMillis();
				Long previousTime = 0L;
				newUserGPSLocales.setId(entity.getId());
				newUserGPSLocales.setUserId(entity.getUserId());
				if(null != entity.getLastUpdated()) {//last update time found 
					logger.info("userGPSLocales details fetched : "+entity);
					previousTime = entity.getLastUpdated();
					Long diff = currTime - previousTime;
					Long days = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
					if(Integer.valueOf(getNoOfDaysFromConfig) < days.intValue()) { // outdated user location info
						openCageResp = fetchFromOpenCage(gpsReq);
						logger.info("outdated user ----------------->");	
						gpsLocationResp = condition2(newUserGPSLocales, openCageResp, entity);
					}else {// last data is updated less than 7 days before so ignore ---send prev data to UI
						gpsLocationResp = convert(entity);
					}
				}else { //Last updated time is missing rewrite old data
					openCageResp = fetchFromOpenCage(gpsReq);
					gpsLocationResp = condition2(newUserGPSLocales,openCageResp, entity);
				}
			}
		}
		return gpsLocationResp;
	}

	private static GpsLocationResp convert(UserGPSLocales entity) {

		return GpsLocationResp.builder().userId(entity.getUserId()).latestLocale(entity.getLatestLocale())
				.historicalLocales(entity.getHistoricalLocales()).build();

	}

	private void saveToDB(UserGPSLocales newUserGPSLocales) {

		userLocalesDAO.save(newUserGPSLocales);
		//this SNS calling only in prod if you want in local comment the below condition.
//		if(StringUtils.isNotEmpty(newUserGPSLocales.getUserId())
//		&& ConfigUtils.INSTANCE.properties.getProperty("environment").equals("PROD")
//		){
//			sendGPSLocalesToLSUsingSNS(newUserGPSLocales.getUserId(),newUserGPSLocales.getLatestLocale().getLatitude(), newUserGPSLocales.getLatestLocale().getLongitude(),newUserGPSLocales.getLatestLocale().getCity());
//		}
	}

	public void sendGPSLocalesToLSUsingSNS(String userId, String latitude, String longitude, String city){
		Map<String,String> payload = new HashMap<>();
		if(StringUtils.isNotEmpty(userId)){
			payload.put("userId",userId);
			payload.put("latitude",latitude);
			payload.put("longitude",longitude);
			payload.put("city",city);
			logger.info("calling sns for sendGPSLocalesToLSUsingSNS with : "+payload.toString());
			awsSNSManager.triggerSNS(SNSTopic.UPDATE_LEAD_GPS_TO_LS,SNSTopic.UPDATE_LEAD_GPS_TO_LS.name(),new Gson().toJson(payload));
		}
	}


	private GpsLocationResp condition1(GPSLocalePojo gpsLocalePojo, OpenCageResponse openCageResp) {
		GpsLocationResp gpsLocationResp = new GpsLocationResp();
		List<GPSLocalePojo> historicalLocales = new ArrayList<GPSLocalePojo>();
		UserGPSLocales newUserGPSLocales = new UserGPSLocales();
		historicalLocales.add(gpsLocalePojo);
		newUserGPSLocales.setUserId(openCageResp.getUserId());
		newUserGPSLocales.setLatestLocale(gpsLocalePojo);
		newUserGPSLocales.setHistoricalLocales(historicalLocales);
		logger.debug("newUserGPSLocales to save = "+newUserGPSLocales);
		saveToDB(newUserGPSLocales);
		gpsLocationResp = convert(newUserGPSLocales);
		return gpsLocationResp;
	}

	private GpsLocationResp condition2(UserGPSLocales newUserGPSLocales, OpenCageResponse openCageResp, UserGPSLocales entity) {

		GpsLocationResp gpsLocationResp = new GpsLocationResp();
		GPSLocalePojo gpsLocalePojo = new GPSLocalePojo();
		Boolean setRetry = false;
		Boolean cityMatches = false;
		if(null != openCageResp && null != openCageResp.getCity()) {
			gpsLocalePojo = convertGPSLocalePojo(openCageResp);
		}else {
			logger.info("Could not fetch new data from Open Cage,send blank city");
				openCageResp.setCity("");
				gpsLocalePojo = convertGPSLocalePojo(openCageResp);
			
			setRetry = true;
		}
		if(null != openCageResp && null != openCageResp.getCity() && null != entity.getLatestLocale().getCity()
				&& StringUtils.isNotEmpty(entity.getLatestLocale().getCity()) && StringUtils.isNotEmpty(openCageResp.getCity())) {
			cityMatches = openCageResp.getCity().equalsIgnoreCase(entity.getLatestLocale().getCity());
		}
		List<GPSLocalePojo> historicalLocales = new ArrayList<GPSLocalePojo>();
		if(null!= entity.getHistoricalLocales() && ArrayUtils.isNotEmpty(entity.getHistoricalLocales()))
			historicalLocales.addAll(entity.getHistoricalLocales());
		newUserGPSLocales.setLatestLocale(gpsLocalePojo);
		//if user's city is still same
		if(historicalLocales.size()>0 && cityMatches) {
			historicalLocales.remove((historicalLocales.size()-1)); 
		}
		historicalLocales.add(gpsLocalePojo);
		newUserGPSLocales.setHistoricalLocales(historicalLocales);
		logger.debug("newUserGPSLocales to save = "+newUserGPSLocales);
		saveToDB(newUserGPSLocales);
		gpsLocationResp = convert(newUserGPSLocales);
		gpsLocationResp.setRetryRequired(setRetry);
		return gpsLocationResp;
	}
	
	
}


