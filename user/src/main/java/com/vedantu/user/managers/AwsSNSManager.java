/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.user.managers;

import com.vedantu.aws.AbstractAwsSNSManager;
import com.vedantu.aws.pojo.CronTopic;
import com.vedantu.onetofew.enums.SNSTopicOTF;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.enums.SNSTopic;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author somil
 */
@Service
public class AwsSNSManager extends AbstractAwsSNSManager {

    @Autowired
    private LogFactory logFactory;
    public String environment;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(AwsSNSManager.class);

    public AwsSNSManager() {
        super();
        logger.info("initializing AwsSNSManager");
        environment = ConfigUtils.INSTANCE.getStringValue("environment");
    }

    @Override
    public void createSubscriptions() {
        if (StringUtils.isNotEmpty(environment) && environment.equalsIgnoreCase("PROD")) {
            createCronSubscription(CronTopic.CRON_CHIME_DAILY_3PM_IST, "/resetadminpasswords");
        }
        createCronSubscription(CronTopic.CRON_CHIME_2HOURLY, "/sendReviseJeeReminderSMS");
        createCronSubscription(SNSTopicOTF.ORDER_UPDATED, "/userOrderOperation");
        createCronSubscription(SNSTopic.UPDATE_MEMBER_COUNT, "/updateMemberCount");
        createCronSubscription(SNSTopic.VSAT_EVENT_UPDATED, "/vsat/changeRedisVsatEventDetails");

    }

    @Override
    public void createTopics() {
        //nothing
        //ORDER_UPDATED_PROD
        createSNSTopic(SNSTopicOTF.ORDER_UPDATED);
        createSNSTopic(SNSTopic.UPDATE_MEMBER_COUNT);
    }

}
