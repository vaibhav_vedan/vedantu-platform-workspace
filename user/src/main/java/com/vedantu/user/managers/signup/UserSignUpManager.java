package com.vedantu.user.managers.signup;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.User.FeatureSource;
import com.vedantu.User.Role;
import com.vedantu.User.UTMParams;
import com.vedantu.User.request.LeadSquaredRequest;
import com.vedantu.User.request.SocialSource;
import com.vedantu.User.request.SocialUserInfo;
import com.vedantu.exception.*;
import com.vedantu.user.dao.RedisDAO;
import com.vedantu.user.entity.User;
import com.vedantu.user.enums.ReferralStep;
import com.vedantu.user.managers.AwsSQSManager;
import com.vedantu.user.managers.CommunicationManager;
import com.vedantu.user.managers.UserManager;
import com.vedantu.user.requests.ProcessReferralBonusReq;
import com.vedantu.user.utils.PojoUtils;
import com.vedantu.util.*;
import com.vedantu.util.enums.LeadSquaredAction;
import com.vedantu.util.enums.LeadSquaredDataType;
import com.vedantu.util.enums.SQSMessageType;
import com.vedantu.util.enums.SQSQueue;
import com.vedantu.util.security.HttpSessionData;
import com.vedantu.util.security.HttpSessionUtils;
import org.apache.logging.log4j.Logger;
import org.dozer.DozerBeanMapper;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.charset.Charset;
import java.util.*;

@Service
public class UserSignUpManager {

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(UserSignUpManager.class);

    @Autowired
    private RedisDAO redisDAO;

    @Autowired
    @Resource(name = "googleManager")
    private ISocialManager googleManager;

    private final Gson gson = new Gson();

    @Autowired
    private UserManager userManager;

    @Autowired
    private HttpSessionUtils sessionUtils;

    @Autowired
    private PojoUtils pojoUtils;

    @Autowired
    private DozerBeanMapper mapper;

    @Autowired
    public AwsSQSManager awsSQSManager;

    @Autowired
    public CommunicationManager communicationManager;

    public static final String STATUS_SOCIAL_LOGIN = "/social/statusSocialLogin.jsp";

    public void authenticate(HttpServletRequest request, HttpServletResponse response, SocialSource socialSource) throws VException, ServletException, IOException, URISyntaxException {
        logger.info("state " + request.getParameter("state"));
        String generatedString = request.getParameter("state");
        String metaData = redisDAO.get(generatedString);
        logger.info("metaData: " + metaData);
        if (metaData == null) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "bad authenticate request");
        }
        String utmParams = null;
        String referrer = null;
        String signUpURL = null;
        String optIn = null;

        JSONObject json = new JSONObject(metaData);
        logger.info("jsonObject: " + json.toString());

        if (json.has("utmParams") && json.getString("utmParams") != null) {
            utmParams = json.getString("utmParams");
            logger.info("utmParams: " + utmParams);
        }
        if (json.has("signUpURL") && json.get("signUpURL") != null) {
            signUpURL = json.getString("signUpURL");
            logger.info("signUpURL: " + signUpURL);
        }
        if (json.has("referrer") && json.get("referrer") != null) {
            referrer = json.getString("referrer");
            logger.info("referrer: " + referrer);
        }
        if (json.has("optIn") && json.get("optIn") != null) {
            optIn = json.getString("optIn");
            logger.info("optIn: " + optIn);
        }

        UTMParams utm = gson.fromJson(utmParams, UTMParams.class);

        ISocialManager socialManager = getSocialManager(socialSource);
        String code = request.getParameter("code");
        logger.info("code: " + code);
        SocialUserInfo sociaUserInfo = socialManager.getUserInfo(code);
        JsonObject jsonObject = new JsonObject();
        request.setAttribute("data", jsonObject);
        request.setAttribute("source", socialSource.name());
        if (sociaUserInfo == null || StringUtils.isEmpty(sociaUserInfo.email)) {
            jsonObject.addProperty("error", "missing user email");
            request.getRequestDispatcher(STATUS_SOCIAL_LOGIN).forward(request, response);
            return;
        }
        // try to add user in db
        User user = userManager.getUserByEmail(sociaUserInfo.email);

        Boolean isSignUpURLSet = true;
        if (user != null && StringUtils.isNotEmpty(user.getSocialSource())) {
            if (StringUtils.isEmpty(user.getSignUpURL())) {
                isSignUpURLSet = false;
            } else {
                if (user.getSignUpURL().contains("google.com") || user.getSignUpURL().contains("facebook.com")) {
                    isSignUpURLSet = false;
                } else {
                    isSignUpURLSet = true;
                }
            }
        }

        if (user != null) {
            if (user.getProfileEnabled() != null && !user.getProfileEnabled()) {
                jsonObject.addProperty("error", ErrorCode.USER_BLOCKED.name());
                request.getRequestDispatcher(STATUS_SOCIAL_LOGIN).forward(request, response);
                return;
            }
            jsonObject.addProperty("login", true);
            jsonObject.addProperty("isSignUpURLSet", isSignUpURLSet);
            HttpSessionData sessionData = sessionUtils.setCookieAndHeaders(request, response, pojoUtils.convertToUserPojo(user));
            // createSession(request, response, user, request.getSession());
            jsonObject.add("userDetails", gson.toJsonTree(sessionData));
            // create the login session
        } else {
            /*
            //Not doing auto signup for new flow of signup--phone compulsory, email optional
            if (sessionUtils.getCurrentSessionData() == null) {

                // Redis check
                SignUpReq signUpReq = new SignUpReq();
                signUpReq.setEmail(sociaUserInfo.email);
                signUpReq.setFirstName(sociaUserInfo.firstName);
                signUpReq.setLastName(sociaUserInfo.lastName);
                if (sociaUserInfo.gender == null) {
                    signUpReq.setGender(Gender.UNKNOWN);
                } else {
                    signUpReq.setGender(sociaUserInfo.gender);
                }
                signUpReq.setPicUrl(sociaUserInfo.picUrl);
                signUpReq.setSocialSource(socialSource.toString());
                signUpReq.setTncVersion("v5");
                signUpReq.setRole(Role.STUDENT);
                if (utm != null) {
                    signUpReq.setUtm(utm);
                }
                signUpReq.setSignUpURL(signUpURL);
                signUpReq.setReferrer(referrer);
                if (StringUtils.isNotEmpty(optIn) && optIn.equals("true")) {
                    signUpReq.setOptIn(true);
                } else {
                    signUpReq.setOptIn(false);
                }

                SignUpRes sRes = signUpUser(signUpReq, request, response, socialSource.toString());
                sociaUserInfo.setUserId(sRes.getUserId().toString());
            }
             */
            // TODO: Check whether this works
            // String authToken = getAuthToke();
            // HttpSession session = request.getSession();
            // session.setAttribute(SignInReq.AUTH_TOKEN, authToken);
            // request.setAttribute(SignInReq.AUTH_TOKEN, authToken);
            // HttpSessionData sessionData = sessionUtils.getCurrentSessionData();
            /*if (sessionData != null) {
                sessionData.setProfilePicUrl(sociaUserInfo.picUrl);
                sessionUtils._setCookieAndHeaders(request, response, sessionData);
                // session.setAttribute(USER_PIC_URL, sociaUserInfo.picUrl);
            }*/
            jsonObject.add("response", gson.toJsonTree(sociaUserInfo));
        }
        request.getRequestDispatcher(STATUS_SOCIAL_LOGIN).forward(request, response);

    }

    private ISocialManager getSocialManager(SocialSource socialSource) {
        switch (socialSource) {
            case GOOGLE:
                return googleManager;
            default:
                return null;

        }
    }

    public void authorize(HttpServletResponse response, SocialSource socialSource, String utmParams, String signUpURL, String referrer, String optIn) throws IOException, InternalServerErrorException, BadRequestException {

        ISocialManager socialManager = getSocialManager(socialSource);

        byte[] array = new byte[7]; // length is bounded by 7
        new Random().nextBytes(array);
        String generatedString = new String(array, Charset.forName("UTF-8"));

        JSONObject json = new JSONObject();
        json.put("utmParams", utmParams);
        json.put("signUpURL", signUpURL);
        json.put("referrer", referrer);
        json.put("optIn", optIn);

        redisDAO.setex(generatedString, json.toString(), 3600);

        String authUrl = StringUtils.defaultIfEmpty(socialManager.getAuthorizeUrl(generatedString));
        if(StringUtils.isEmpty(authUrl)){
            throw new BadRequestException(ErrorCode.GOOGLE_SINGIN_REDIRECING_URL_NOT_GENERATED, "redirecting url not generated from social manager");
        }
        logger.info("auth url : " + authUrl);
        response.sendRedirect(authUrl);
    }

    public void signUpTasksFromQueue(User user){
        if(user == null){
            logger.warn("user Details not for proceeding the singup tasks");
            return;
        }

        logger.info("calling the platform api for referral bonus for singup");
        try {
            if (user.getReferrerCode() != null && !user.getRole().equals(Role.ADMIN)) {
                ProcessReferralBonusReq request = new ProcessReferralBonusReq();
                request.setReferralStep(ReferralStep.SIGNUP);
                request.setUserId(user.getId());
                String platformEndpoint = ConfigUtils.INSTANCE.getStringValue("PLATFORM_ENDPOINT");
                ClientResponse resp = WebUtils.INSTANCE.doCall(platformEndpoint + "/referral/processReferralBonusForSignUp",
                        HttpMethod.POST, new Gson().toJson(request));
                VExceptionFactory.INSTANCE.parseAndThrowException(resp);
            }
        } catch (Exception e) {
            logger.error("processReferralBonusForSignUp failed : ", e);
        }

        com.vedantu.User.User user1 = mapper.map(user, com.vedantu.User.User.class);

        logger.info("eleminate the creating the lead square if the user sign Up through ORG_USER_SIGNUP signup feature source");
        boolean createLead = true;
        if (null != user.getSignUpFeature() && FeatureSource.ORG_USER_SIGNUP.equals(user.getSignUpFeature())) {
            createLead = false;
        }

        //leadSquared post
        if(createLead){
            if (Role.STUDENT.equals(user.getRole())) {
                logger.info("triggering  the lead squared queue with user : "+user1);
                try {
                    LeadSquaredRequest req = new LeadSquaredRequest(LeadSquaredAction.POST_LEAD_DATA, LeadSquaredDataType.LEAD_UPSERT, user1, null, null);
                    if (LeadSquaredDataType.LEAD_UPSERT.equals(req.getDataType()) && req.getUser() != null && req.getUser().getId() != null) {
                        String messageGorupId = req.getUser().getId() + "_UPSERT";
                        awsSQSManager.sendToSQS(SQSQueue.LEADSQUARED_QUEUE, SQSMessageType.LS_TASK, gson.toJson(req), messageGorupId);
                    } else {
                        awsSQSManager.sendToSQS(SQSQueue.LEADSQUARED_QUEUE, SQSMessageType.LS_TASK, gson.toJson(req));
                    }
                } catch (Exception e) {
                    logger.error("Exception occured in pushing data to lead squared");
                }
            }
        }

        //Early Learning User Signup LeadSquared Entry
        logger.info("user.getSignUpURL : "+user.getSignUpURL());
        if(Role.STUDENT.equals(user.getRole()) && user.getSignUpURL() !=null && user.getSignUpURL().toLowerCase().contains("superkids")){
            Map<String,String> userLead = new HashMap();
            if(Objects.nonNull(user) && Objects.nonNull(user.getId())) {
                createEarlyLearningUserLeadsMap(userLead,user);
                logger.info("userLead : "+userLead);
                LeadSquaredRequest req = new LeadSquaredRequest(LeadSquaredAction.POST_LEAD_DATA, LeadSquaredDataType.LEAD_ACTIVITY_SUPERKIDS_SIGNUP, null, userLead, null);
                String messageGroupId = user.getId() + "_SIGNUP";
                awsSQSManager.sendToSQS(SQSQueue.LEADSQUARED_QUEUE, SQSMessageType.CUSTOMER_SIGNUP_SUPERKIDS, gson.toJson(req), messageGroupId);
            }
        }

        try {
            if (user1 != null && (FeatureSource.ISL.equals(user1.getSignUpFeature())
                    || FeatureSource.ISL_REGISTRATION_VIA_TOOLS.equals(user1.getSignUpFeature()))
                    || FeatureSource.VSAT.equals(user1.getSignUpFeature())
                    || FeatureSource.VOLT_2020_JAN.equals(user1.getSignUpFeature())
                    || FeatureSource.REVISEINDIA_2020_FEB.equals(user1.getSignUpFeature())
                    || FeatureSource.REVISE_JEE_2020_MARCH.equals(user1.getSignUpFeature())) {
                if (Role.STUDENT.equals(user1.getRole())) {
                    switch (user1.getSignUpFeature()){
                        case ISL:
                            communicationManager.sendRegistrationEmailForISL(user1);
                            break;
                        case ISL_REGISTRATION_VIA_TOOLS:
                            communicationManager.sendEmailRegistrationISLViaTools(user1);
                            break;
                        case VSAT:
                            communicationManager.sendRegistrationEmailForVSAT(user1);
                            break;
                        case VOLT_2020_JAN:
                            communicationManager.sendRegistrationEmailForVolt(user1);
                            break;
                        case REVISEINDIA_2020_FEB:
                            communicationManager.sendRegistrationEmailForReviseIndia(user1);
                            break;
                        case REVISE_JEE_2020_MARCH:
                            communicationManager.sendRegistrationEmailForReviseJee(user1);
                            break;
                    }
                }
            }
        }catch (Exception e) {
            logger.error("Exception occured in sending registration mail to user ");
        }
    }

    public void createEarlyLearningUserLeadsMap(Map<String,String> userLead, User user){

        if(Objects.nonNull(user.getStudentInfo())){
            if(ArrayUtils.isNotEmpty(user.getStudentInfo().getParentInfos())) {
                userLead.put(LeadSquaredRequest.Constants.PARENT_NAME,
                        Optional.ofNullable(user.getStudentInfo().getParentInfos().get(0).getFirstName()).orElse(""));
                userLead.put(LeadSquaredRequest.Constants.PARENT_EMAIL,
                        Optional.ofNullable(user.getStudentInfo().getParentInfos().get(0).getEmail()).orElse(""));
                userLead.put(LeadSquaredRequest.Constants.PARENT_PHONE,
                        Optional.ofNullable(user.getStudentInfo().getParentInfos().get(0).getContactNumber()).orElse(""));
            }
            userLead.put(LeadSquaredRequest.Constants.GRADE,Optional.ofNullable(user.getStudentInfo().getGrade()).orElse(""));
            userLead.put(LeadSquaredRequest.Constants.BOARD,Optional.ofNullable(user.getStudentInfo().getBoard()).orElse(""));
            if(Objects.nonNull(user.getStudentInfo().getDeviceType())) {
                userLead.put(LeadSquaredRequest.Constants.DEVICE_TYPE, Optional.ofNullable(user.getStudentInfo().getDeviceType().name()).orElse(""));
            }
            userLead.put(LeadSquaredRequest.Constants.CUSTOMER_STATUS,"Signed Up");
        }
        userLead.put(LeadSquaredRequest.Constants.CHILD_NAME,Optional.ofNullable(user.getFirstName()).orElse(""));
        userLead.put(LeadSquaredRequest.Constants.STUDENT_ID,String.valueOf(user.getId()));
    }

}
