package com.vedantu.user.managers;

import com.vedantu.User.Role;
import com.vedantu.User.User;
import com.vedantu.User.enums.SubRole;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ConflictException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.VException;
import com.vedantu.user.dao.LOAMAmbassadorUserDAO;
import com.vedantu.user.dao.UserDAO;
import com.vedantu.user.utils.PojoUtils;
import com.vedantu.util.CollectionUtils;
import com.vedantu.util.CustomValidator;
import com.vedantu.util.LogFactory;
import com.vedantu.util.dbentities.mongo.AbstractMongoEntity;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/*
@Author: Sanket Jain
@Reviewer: Arun Dhwaj
@Reviewer_comment:
    i) Variable name should be enough to explicit the symantics.
    Like: ExposeEmail, can be rename to exposeContactInfo.
*/

@Service
public class LOAMAmbassadorUserManager
{
    @Autowired
    private LOAMAmbassadorUserDAO loamAmbassadorUserDAO;

    @Autowired
    public PojoUtils pojoUtils;

    @Autowired
    private LogFactory logFactory;

    @Autowired
    private UserDAO userDAO;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(LOAMAmbassadorUserManager.class);

    public List<User> loam_getUsersById(List<Long> userIds, boolean exposeContactInfo, String callType) throws VException
    {
        if (userIds == null)
        {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "userIds null");
        }

        Set<String> fields = new HashSet<>();
        fields.add(User.Constants.ID);
        fields.add(User.Constants.FIRST_NAME);
        fields.add(User.Constants.LAST_NAME);
        fields.add(User.Constants.FULL_NAME);
        fields.add(User.Constants.EMAIL);
        fields.add(User.Constants.STUDENTINFO_GRADE);
        fields.add(User.Constants.STUDENTINFO_BOARD);
        fields.add(User.Constants.STUDENTINFO_EXAM_TARGET);
        fields.add(User.Constants.PROFILE_PIC_ID);
        fields.add(User.Constants.PROFILE_PIC_URL);
        fields.add(User.Constants.PROFILE_PIC_PATH);

        if(callType.equals("student"))
        {
            fields.add(User.Constants.GENDER);
            fields.add(User.Constants.LOCATION_INFO);
            fields.add(User.Constants.SOCIAL_INFO);
            fields.add(User.Constants.STUDENTINFO_PARENT_INFOS);
            fields.add(User.Constants.LANGUAGE_PREFS);
            fields.add(User.Constants.PHONE_CODE);
            fields.add(User.Constants.CONTACT_NUMBER);
        }

        List<com.vedantu.user.entity.User> users = loamAmbassadorUserDAO.loam_getUsersById(userIds, fields, true);
        List<com.vedantu.User.User> results = new ArrayList<>();

        if (!CollectionUtils.isEmpty(users))
        {
            users.stream().map((user) -> pojoUtils.convertToUserPojo(user)).map((u) -> {
                if (!exposeContactInfo)
                {
                    u.setContactNumber(null);
                    u.setPhoneCode(null);
                    u.setEmail(null);

                    if (Role.TEACHER.equals(u.getRole()) && u.getTeacherInfo() != null)
                    {
                        u.getTeacherInfo().setExtensionNumber(null);
                        u.getTeacherInfo().setPrimaryCallingNumberCode(null);
                    }
                }
                return u;
            }).forEachOrdered((u) -> {
                results.add(u);
            });
        }
        return results;
    }

    // Get User by Email. Added projections to get the teacher info.
    public com.vedantu.user.entity.User loam_getUserByEmail(String email) throws VException {
        if (!CustomValidator.validEmail(email)) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Email not valid");
        }
        Set<String> fields = new HashSet<>();
        fields.add(User.Constants.TEACHER_INFO);
        return loamAmbassadorUserDAO.loam_getUserByEmail(email,fields,true);
    }

    public List<User> loam_getAcadMentorUsers() throws VException
    {
        Set<String> fields = new HashSet<>();
        fields.add(User.Constants.ID);
        fields.add(User.Constants.FIRST_NAME);
        fields.add(User.Constants.LAST_NAME);
        fields.add(User.Constants.FULL_NAME);
        fields.add(User.Constants.EMAIL);
        fields.add(User.Constants.PROFILE_PIC_ID);
        fields.add(User.Constants.PROFILE_PIC_URL);
        fields.add(User.Constants.PROFILE_PIC_PATH);
        List<com.vedantu.user.entity.User> users = loamAmbassadorUserDAO.loam_getAcadMentorUsers(fields, true);
        List<com.vedantu.User.User> results = new ArrayList<>();

        if (!CollectionUtils.isEmpty(users))
        {
            users.stream().map((user) -> pojoUtils.convertToUserPojo(user)).forEachOrdered((u) -> {
                results.add(u);
            });
        }
        return results;
    }

    public List<User> loam_getDoubtResolverData(Long fromTime, Long thruTime, Integer start, Integer size) {
        Set<String> fields = new HashSet<>();
        fields.add(User.Constants.FIRST_NAME);
        fields.add(User.Constants.LAST_NAME);
        fields.add(User.Constants.FULL_NAME);
        loam_addAbstractMongoEntityFields(fields);

        List<com.vedantu.user.entity.User> users = loamAmbassadorUserDAO.loam_getDoubtResolverData(fromTime, thruTime, start, size,fields, true);
        List<com.vedantu.User.User> results = new ArrayList<>();

        if (!CollectionUtils.isEmpty(users))
        {
            users.stream().map((user) -> pojoUtils.convertToUserPojo(user)).forEachOrdered((u) -> {
                results.add(u);
            });
        }

        logger.info("Logger Results :" + results);
        return results;
    }

    private void loam_addAbstractMongoEntityFields(Set<String> requiredFields) {
        requiredFields.add(AbstractMongoEntity.Constants._ID);
        requiredFields.add(AbstractMongoEntity.Constants.CREATION_TIME);
        requiredFields.add(AbstractMongoEntity.Constants.CREATED_BY);
        requiredFields.add(AbstractMongoEntity.Constants.LAST_UPDATED);
        requiredFields.add(AbstractMongoEntity.Constants.ID);
        requiredFields.add(AbstractMongoEntity.Constants.LAST_UPDATED_BY);
        requiredFields.add(AbstractMongoEntity.Constants.ENTITY_STATE);
    }

    public com.vedantu.user.entity.User loam_updateTeacherRoleToSuperMentor(Long userId, Long callerId) throws ConflictException {
        com.vedantu.user.entity.User user = userDAO.getUserByUserId(userId);
        user.getTeacherInfo().getSubRole().add(SubRole.ACADMENTOR);
        userDAO.update(user, callerId);
        return user;
    }
}
