/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.user.managers.signup;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.vedantu.User.request.SocialSource;
import com.vedantu.User.request.SocialUserInfo;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.WebCommunicator;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author somil
 */
@Service
public class GoogleManager extends AbstractSocialManager {

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(GoogleManager.class);

    public GoogleManager() {
        super();
        httpParams.add(new BasicNameValuePair("response_type", "code"));
        httpParams.add(new BasicNameValuePair(
                "scope",
                "profile openid email"));
        super.init();

    }

//    @Override
//    protected void init() {
//        httpParams.add(new BasicNameValuePair("response_type", "code"));
//        httpParams.add(new BasicNameValuePair(
//                "scope",
//                "https://www.googleapis.com/auth/plus.login https://www.googleapis.com/auth/plus.me https://www.googleapis.com/auth/plus.profile.emails.read"));
//        super.init();
//
//    }

    @Override
    public String getClientId() {
        return CLIENT_ID;
    }

    @Override
    public String getClientSecret() {

        return CLIENT_SECRET;
    }

    @Override
    public String getAuthUrl() {

        return AUTH_URL;
    }

    @Override
    public String getTokenUrl(List<NameValuePair> httpParams) {
        return TOKEN_URL;
    }

    @Override
    public String getUserInfoUrl(String access_token) {
        StringBuilder sb = new StringBuilder();
        sb.append(USER_INFO_URL);
        sb.append("access_token=").append(access_token);
        return sb.toString();
    }

    @Override
    public SocialUserInfo getUserInfo(String code) {

        List<NameValuePair> httpParams = new ArrayList<NameValuePair>(
                this.httpParams);
        httpParams.remove(0);
        httpParams.add(new BasicNameValuePair("code", code));
        httpParams.add(new BasicNameValuePair("client_secret",
                getClientSecret()));
        httpParams.add(new BasicNameValuePair("grant_type",
                "authorization_code"));
        logger.info("loading authorization_code/token info for code: " + code);
        String tokenRes = WebCommunicator.fetchWithUrlFetchService(
                getTokenUrl(httpParams), httpParams, HttpMethod.POST);
        logger.info("tokenRes : " + tokenRes);

        if (StringUtils.isEmpty(tokenRes)) {
            return null;
        }

        String accessToken = null;
        JsonObject userJson = null;
        Gson gson = new Gson();
        JsonObject tokenJson = gson.fromJson(tokenRes, JsonObject.class);
        
        if (tokenJson.has("access_token") && tokenJson.get("access_token").getAsString() != null) {
            // Do something with object.
        	accessToken = tokenJson.get("access_token").getAsString();
        } else {
        	return null;
        }
        
        String userInfoString = WebCommunicator.fetchWithUrlFetchService(
                getUserInfoUrl(accessToken), null, HttpMethod.GET);
        logger.info("user info string : " + userInfoString);

        if (userInfoString == null) {
            logger.warn("can not get user data from google");
            return null;
        }

        userJson = gson.fromJson(userInfoString, JsonObject.class);

        return getSocialSource().getSocialUserInfo(userJson, accessToken);
    }

    @Override
    public SocialSource getSocialSource() {
        return SocialSource.GOOGLE;
    }

    @Override
    public SocialUserInfo getUserInfoUsingToken(String code) {
        throw new UnsupportedOperationException("Not supported for Google, only used for Facebook");
    }

}
