/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.user.managers;

import com.amazonaws.services.glue.model.AlreadyExistsException;
import com.vedantu.exception.*;
import com.vedantu.user.dao.OrgDAO;
import com.vedantu.user.dao.UserDAO;
import com.vedantu.user.entity.Org;
import com.vedantu.user.entity.User;
import com.vedantu.util.*;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

import static com.vedantu.exception.ErrorCode.*;

/**
 *
 * @author ajith
 */
@Service
public class OrgManager {

    @Autowired
    private OrgDAO orgDAO;

    @Autowired
    private UserDAO userDAO;

    @Autowired
    private FosUtils fosUtils;

    @Autowired
    private LogFactory logFactory;

    private final Logger logger = logFactory.getLogger(OrgManager.class);

    public Org add(Org org) throws ConflictException{
        org.setId(org.getId().toLowerCase());
        Org prevOrg=orgDAO.getOrgById(org.getId());
        if(prevOrg!=null){
            throw new ConflictException(ErrorCode.ORG_ALREADY_EXISTS,"Org already exists");
        }
        return orgDAO.save(org);
    }

    public Org getOrg(String orgId) {
        return orgDAO.getOrgById(orgId);
    }

    public List<Org> getOrgIds(Integer start, Integer limit) {
        return orgDAO.getOrgIds(start,limit);
    }

    public PlatformBasicResponse addEditOrgIdToUser(String orgId, Long userId) throws BadRequestException, NotFoundException, ConflictException, InternalServerErrorException {
        if (StringUtils.isEmpty(orgId)) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "orgid required");
        }
        if (userId == null) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "userId required");
        }
        Org org = orgDAO.getOrgById(orgId);
        if (org == null) {
            throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, "orgId not found");
        }
        User user = userDAO.getUserByUserId(userId);
        logger.info("userDetails  :" + user);
        if (user == null) {
            throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, "user not found with id" + userId);
        }
        user.setOrgId(orgId);
        userDAO.update(user, userId);
        fosUtils.deleteRedisUserBasicInfoByUserId(user.getId());
        return new PlatformBasicResponse();
    }
}
