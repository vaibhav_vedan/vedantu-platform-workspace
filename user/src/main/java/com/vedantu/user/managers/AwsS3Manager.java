package com.vedantu.user.managers;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.amazonaws.HttpMethod;
import com.amazonaws.metrics.AwsSdkMetrics;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.GeneratePresignedUrlRequest;
import com.vedantu.aws.pojo.UploadFileSourceType;
import com.vedantu.aws.response.UploadFileUrlRes;
import com.vedantu.exception.VException;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.LogFactory;
import javax.annotation.PreDestroy;

@Service
public class AwsS3Manager {

    private AmazonS3Client s3Client;

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(AwsS3Manager.class);

    @PostConstruct
    public void init() {

        s3Client = new AmazonS3Client();
    }

    public Boolean uploadFile(String bucket, String key, File contentFile) {
        Boolean isUploadContentFileSuccessfull = false;

        try {
            s3Client.putObject(bucket, key, contentFile);

            isUploadContentFileSuccessfull = true;
        } catch (Exception ex) {
            isUploadContentFileSuccessfull = false;
        }

        return isUploadContentFileSuccessfull;
    }

    public String getPublicBucketDownloadUrl(String bucket, String key) {
        return s3Client.getUrl(bucket, key).toExternalForm();
    }


    @PreDestroy
    public void cleanUp() {
        try {
            if (s3Client != null) {
                s3Client.shutdown();
            }
            AwsSdkMetrics.unregisterMetricAdminMBean();
        } catch (Exception e) {
            logger.error("Error in shutting down s3Client ", e);
        }
    }
}
