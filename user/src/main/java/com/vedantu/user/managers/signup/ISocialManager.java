package com.vedantu.user.managers.signup;



import com.vedantu.User.request.SocialUserInfo;
import java.util.List;
import org.apache.http.NameValuePair;

public interface ISocialManager {
    public String getClientId();

    public String getClientSecret();

    public String getAuthUrl();

    public String getRedirectUrl();

    public String getTokenUrl(List<NameValuePair> httpParams);

    public String getUserInfoUrl(String access_token);

    public String getAuthorizeUrl(String generatedString);

    public SocialUserInfo getUserInfo(String code);

    public SocialUserInfo getUserInfoUsingToken(String code);
}

