package com.vedantu.user.managers;

import com.google.gson.Gson;
import com.vedantu.User.Role;
import com.vedantu.User.UserBasicInfo;
import com.vedantu.User.request.*;
import com.vedantu.User.response.CreateUserResponse;
import com.vedantu.User.response.GeneratePasswordRes;
import com.vedantu.exception.*;
import com.vedantu.notification.enums.CommunicationType;
import com.vedantu.notification.requests.EmailRequest;
import com.vedantu.user.dao.UserDAO;
import com.vedantu.user.dao.UserLoginDataDao;
import com.vedantu.user.entity.User;
import com.vedantu.user.entity.temp.LoginData;
import com.vedantu.util.LogFactory;
import com.vedantu.util.PlatformBasicResponse;
import com.vedantu.util.StringUtils;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.mail.internet.InternetAddress;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


@Service
public class UserPasswordManager {

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(UserManager.class);

    @Autowired
    private UserManager userManager;

    @Autowired
    public PasswordEncoder passwordEncoder;

    @Autowired
    private UserDAO userDAO;

    @Autowired
    private UserLoginDataDao userLoginDataDao;

    @Autowired
    private CommunicationManager communicationManager;

    public PlatformBasicResponse forgotPassword(ForgotPasswordReq req) throws VException, UnsupportedEncodingException {
        CreateUserResponse createUserResponse = userManager.forgotPassword(req);
        communicationManager.sendVerificationTokenEmail(new UserBasicInfo(createUserResponse.getUser(), true), CommunicationType.FORGOT_PASSWORD, createUserResponse.getEmailTokenCode());
        return new PlatformBasicResponse();
    }

    public String changePassword(ChangePasswordReq req, HttpServletRequest request, HttpServletResponse response) throws VException, IOException, URISyntaxException {
        PlatformBasicResponse platformBasicResponse = userManager.changePassword(req,request,response);
        return new Gson().toJson(platformBasicResponse);
    }

    public PlatformBasicResponse generatePassword(GeneratePasswordReq req) throws IOException, VException {
        GeneratePasswordRes generatePasswordRes = userManager.generatePassword(req);
        String contactNumber = generatePasswordRes.getContactNumber();
        if (StringUtils.isNotEmpty(contactNumber)) {
            StringBuilder sb = new StringBuilder();
            for (int k = 0; k < contactNumber.length(); k++) {
                if (k < contactNumber.length() - 4) {
                    sb.append("x");
                } else {
                    sb.append(contactNumber.charAt(k));
                }
            }
            contactNumber = sb.toString();
        }
        return new PlatformBasicResponse(true, contactNumber, null);
    }

    public PlatformBasicResponse resetPassword(ManualResetPasswordReq req) throws VException, UnsupportedEncodingException {
        req.verify();
        if (!"4dYHF*v)Uu42Q*".equals(req.getSecretKey())) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Operation not allowed");
        }
        User user = null;

        if (null != req.getUserId()) {
            user = userManager.getUserByUserId(req.getUserId());
        } else if (null != req.getEmail()) {
            user = userManager.getUserByEmail(req.getEmail());
        }

        if (null == user) {
            return new PlatformBasicResponse(false, "", "Invalid user Id");
        }

        String newPassword = StringUtils.randomAlphaNumericString(6);
        String hashedPassword = passwordEncoder.encode(newPassword);
        user.setPassword(hashedPassword);
        user.setPasswordHashed(true);
        user.setPasswordChangedAt(System.currentTimeMillis());
        user.addRecentHashedPassword(hashedPassword);


        // Check if password already changed.
        LoginData loginData = userLoginDataDao.getPasswordChangedTime(user.getEmail());

        if (null != loginData) {
            return new PlatformBasicResponse(false, "", "Password already changed before manually");
        }

        try {
            EmailRequest emailRequest = new EmailRequest();
            emailRequest.setType(CommunicationType.MANUAL_PASSWORD_RESET);
            emailRequest.setRole(Role.STUDENT);
            Map<String, Object> bodyscopes = new HashMap<>();
            emailRequest.setBodyScopes(bodyscopes);
            emailRequest.getBodyScopes().put("password", newPassword);

            ArrayList<InternetAddress> toList = new ArrayList<>();
            toList.add(new InternetAddress(user.getEmail(), user.getFullName()));
            emailRequest.setTo(toList);

            communicationManager.sendEmail(emailRequest);
        } catch (Exception e) {
            logger.error("Error in sending Password reset email" + e.getMessage());
            return new PlatformBasicResponse(false, "error", e.getMessage());
        }

        userDAO.update(user, null);

        LoginData loginData1 = new LoginData();
        loginData1.setEmail(user.getEmail());
        loginData1.setPasswordChanged(true);
        loginData1.setLastPasswordChangedTime(System.currentTimeMillis());
        userLoginDataDao.save(loginData1);

        return new PlatformBasicResponse();
    }
}
