package com.vedantu.user.managers;

import com.google.gson.Gson;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.User.User;
import com.vedantu.User.UserInfo;
import com.vedantu.exception.VException;
import com.vedantu.exception.VExceptionFactory;
import com.vedantu.notification.enums.CommunicationType;
import com.vedantu.notification.requests.TextSMSRequest;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.WebUtils;
import com.vedantu.util.enums.SQSMessageType;
import com.vedantu.util.enums.SQSQueue;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

/**
 * @author MNPK
 */

@Service
public class SMSManager {
    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(SMSManager.class);

    @Autowired
    private AwsSQSManager awsSQSManager;

    private static final String FOS_ENDPOINT = ConfigUtils.INSTANCE.getStringValue("FOS_ENDPOINT");


    public String sendSMS(TextSMSRequest request) throws VException {
        if (StringUtils.isEmpty(request.getTo())) {
            logger.warn("To field is empty: " + request);
            return null;
        }
        awsSQSManager.sendToSQS(SQSQueue.NOTIFICATION_QUEUE, SQSMessageType.SEND_SMS, new Gson().toJson(request));
        return null;
    }

    public String sendSMSViaRest(TextSMSRequest request) throws VException {
        if (StringUtils.isEmpty(request.getTo())) {
            logger.warn("To field is empty: " + request);
            return null;
        }
        String notificationEndpoint = ConfigUtils.INSTANCE.getStringValue("NOTIFICATION_ENDPOINT");
        ClientResponse resp = WebUtils.INSTANCE.doCall(notificationEndpoint + "/SMS/sendSMS", HttpMethod.POST,
                new Gson().toJson(request));
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        logger.info("Response from notification-centre " + jsonString);
        return jsonString;
    }

    public boolean sendSMS(CommunicationType communicationType, UserInfo userInfo, Object... optionalEntities) throws VException {
        Map<String, Object> scopeParams = new HashMap<String, Object>();
        scopeParams.put(User.Constants.NAME, userInfo.getFirstName());
        if (!CommunicationType.PHONE_VERIFICATION.equals(communicationType) && !userInfo.getIsContactNumberVerified()) {
            logger.info("contact number is not verified for : " + userInfo.getEmail() + " number : " + userInfo.getContactNumber());
            return false;
        }
        Map<String, Object> bodyScopes = new HashMap<String, Object>();
        if (optionalEntities[0] != null) {
            try {
                if (optionalEntities[0] instanceof Map) {
                    bodyScopes = (Map<String, Object>) optionalEntities[0];
                }
            } catch (Exception ex) {
                logger.error("Exception in sendSMS", ex);
            }
        }
        if (bodyScopes != null && !bodyScopes.isEmpty()) {
            scopeParams.putAll(bodyScopes);
            logger.info("sendSMS", scopeParams);
        }
        String phoneNumber = userInfo.getContactNumber();
        String phoneCode = userInfo.getPhoneCode();
        switch (communicationType) {
            case PHONE_VERIFICATION:
                String bonusAmount = ConfigUtils.INSTANCE.getStringValue("freebies.rupees.join.bonus");
                String utm_campaign = (String) optionalEntities[3];
//                if (Role.STUDENT.equals(userInfo.getRole()) && !"bizalliance".equals(utm_campaign)) {
//                    scopeParams.put("VerficationText", " to verify your mobile number on Vedantu.");
//                } else {
//                    scopeParams.put("VerficationText", "for Vedantu.");
//                }
                phoneNumber = (String) optionalEntities[0];
                phoneCode = (String) optionalEntities[1];
                scopeParams.put("code", optionalEntities[2]);

                logger.info("Phonenumber: " + phoneNumber);
                logger.info("phoneCode: " + phoneCode);
                logger.info("code: " + optionalEntities[2]);

                if (StringUtils.isNotEmpty(phoneNumber)) {
                    TextSMSRequest request = new TextSMSRequest(phoneNumber, phoneCode, scopeParams, communicationType, userInfo.getRole());
                    sendSMSViaRest(request);
                }
                return true;
            case PHONE_VERIFICATION_SUCCESS:
                if (optionalEntities != null && optionalEntities.length > 0) {
                    phoneNumber = (String) optionalEntities[0];
                    phoneCode = (String) optionalEntities[1];
                }
                break;
            default:
                break;
        }
        logger.info("scopeParams: " + scopeParams);
        if (StringUtils.isNotEmpty(phoneNumber)) {
            TextSMSRequest request = new TextSMSRequest(phoneNumber, phoneCode, scopeParams, communicationType, userInfo.getRole());
            sendSMS(request);
        }
        return true;
    }

}
