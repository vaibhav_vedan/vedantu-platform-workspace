package com.vedantu.user.managers;

import com.google.gson.Gson;
import com.sun.xml.bind.v2.TODO;
import com.vedantu.User.UserBasicInfo;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.NotFoundException;
import com.vedantu.exception.VException;
import com.vedantu.user.dao.UserDAO;
import com.vedantu.user.entity.User;
import com.vedantu.util.*;
import com.vedantu.util.enums.ReferralType;
import com.vedantu.util.enums.SQSMessageType;
import com.vedantu.util.enums.SQSQueue;
import com.vedantu.util.security.EncryptionUtil;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.xml.bind.DatatypeConverter;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.*;

/**
 * @author MNPK
 */

@Service
public class UserReferralManager {

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(UserReferralManager.class);

    @Autowired
    private UserDAO userDAO;

    @Autowired
    private AwsSQSManager awsSQSManager;

    private final String fosEndpoint = ConfigUtils.INSTANCE.getStringValue("FOS_ENDPOINT");

    public String getLink(Long userId, ReferralType type) throws VException, NoSuchPaddingException, InvalidKeyException, NoSuchAlgorithmException, IllegalBlockSizeException, BadPaddingException, InvalidAlgorithmParameterException {
        User user = userDAO.getReferralCodeByUsingUserId(userId);
        if (user == null) {
            throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, "User not found with the given userId :" + userId);
        }
        if (type == null) {
            type = ReferralType.SIGNUP;
        }
        String code = user.getReferralCode() + "__" + user.getId() + "__" + type.name();
        logger.info("code : "+code);
        String link = fosEndpoint + "?referrelCode=" + EncryptionUtil.encrypt(code.getBytes());
        String shortUrl = WebUtils.INSTANCE.shortenUrl(link);
        return shortUrl;
    }

    public void singUpReferralFromQueue(Map<String, String> payload) throws NotFoundException {
        logger.info(payload);
        String signUpUserId = payload.get("userId");
        String referrerEncryptCode = payload.get("referrerEncryptCode");
        logger.info("signUpUserId : "+ signUpUserId + " referrerEncryptCode : "+referrerEncryptCode);
        String referrerDecryptCode = "";
        try {
            byte[] referrerBytes = DatatypeConverter.parseHexBinary(referrerEncryptCode);
            referrerDecryptCode = EncryptionUtil.decrypt(referrerBytes);
            logger.info("referrerDecryptCode : "+referrerDecryptCode);
        } catch (Exception ex) {
            logger.error("referrer Decrypt Code Error : " + ex);
        }
        if (StringUtils.isEmpty(referrerDecryptCode)) {
            logger.warn("Empty referrer Decrypt Code found");
            throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, "referrerDecryptCode Not found : " + referrerDecryptCode);
        }
        String[] referrerCodeList = referrerDecryptCode.split("__");
        logger.info("referrerCodeList : "+referrerCodeList);
        logger.info("referrerCodeList : "+referrerCodeList);
        if (referrerCodeList.length != 3) {
            logger.error("referrerCodeList not have three parts");
            return;
        }
        String referralCode = referrerCodeList[0];
        String referrerUserId = referrerCodeList[1];
        ReferralType referralType = ReferralType.valueOf(referrerCodeList[2]);
        logger.info("referralCode : " + referralCode + " referrerUserId : " + referrerUserId + " referralType : " + referralType + " signUpUserId : " + signUpUserId);
        //TODO: implement the referral bonus login according to the requirement
        if (referralType != null) {
            switch (referralType) {
                case SIGNUP:
                    break;
                case VQUIZ_SIGNUP:
                    vquizReferral(signUpUserId, referrerUserId);
                    break;
                default:
                    logger.warn("referralType not found!");
            }
        }
    }

    private void vquizReferral(String signUpUserId, String referrerUserId) throws NotFoundException {
        logger.info("referrerUserId : " + referrerUserId + "signUpUserId : " + signUpUserId);
        List<Long> userIds = new ArrayList<>();
        if (StringUtils.isEmpty(signUpUserId)) {
            throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, "signUpuserId not found in Referral Queue : " + signUpUserId);
        }
        userIds.add(Long.parseLong(signUpUserId));
        if (StringUtils.isEmpty(referrerUserId)) {
            throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, "referrerUserId not found in Referral Queue : " + referrerUserId);
        }
        userIds.add(Long.parseLong(referrerUserId));
        List<User> users = userDAO.getUserBasicInfoByIds(userIds, true);
        logger.info("users : " + users);
        if (ArrayUtils.isEmpty(users) || users.size() != 2) {
            throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, "Users not found in user DB by using userIds : " + userIds);
        }
        Map<String, Object> payload = new HashMap<>();
        for (User user : users) {
            if (signUpUserId.equals(user.getId().toString())) {
                payload.put("signUpUser", user);
            } else if (referrerUserId.equals(user.getId().toString())) {
                payload.put("referrerUser", user);
            }
        }
        logger.info("payload : " + payload);
        awsSQSManager.sendToSQS(SQSQueue.VQUIZ_REFERRAL_QUEUE, SQSMessageType.VQUIZ_REFERRAL_QUEUE_REQ, new Gson().toJson(payload));
    }


}
