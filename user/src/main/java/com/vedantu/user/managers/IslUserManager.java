package com.vedantu.user.managers;

import com.google.gson.Gson;
import com.vedantu.User.enums.VoltStatus;
import com.vedantu.User.request.GetUsersReq;
import com.vedantu.User.response.IsRegisteredForISLRes;
import com.vedantu.User.response.UserDetailsInfo;
import com.vedantu.exception.VException;
import com.vedantu.util.LogFactory;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author MNPK
 */

@Service
public class IslUserManager {

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(UserManager.class);

    @Autowired
    private UserManager userManager;


    public IsRegisteredForISLRes isRegisteredForISL(Long userId, String event) throws VException {
        return userManager.isRegisteredForISL(userId, event);
    }

    public IsRegisteredForISLRes isRegisteredForVolt(Long userId, String event) throws VException {
        UserDetailsInfo info = userManager.getUserDetailsForISLInfo(userId, "VOLT_2020_JAN", null);
        if (info != null) {
            return new IsRegisteredForISLRes(VoltStatus.REGISTERED == info.getVoltStatus(), userId, info.getVoltStatus());
        } else {
            return new IsRegisteredForISLRes(false, userId, null);
        }
    }

    public String getUsersRegisteredForISL(GetUsersReq req) throws VException {
        List<UserDetailsInfo> userDetailsInfos = userManager.getUsersRegisteredForISL(req);
        return new Gson().toJson(userDetailsInfos);
    }

}
