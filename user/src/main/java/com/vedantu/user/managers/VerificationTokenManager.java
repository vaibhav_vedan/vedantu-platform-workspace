/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.user.managers;

import com.vedantu.User.VerificationLinkStatus;
import com.vedantu.User.VerificationTokenType;
import com.vedantu.User.response.ProcessVerificationResponse;
import com.vedantu.User.response.VerificationStatus;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.VException;
import com.vedantu.user.entity.VerificationTokenII;
import com.vedantu.user.dao.VerificationTokenDAO;
import com.vedantu.user.utils.PojoUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import java.util.List;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author somil
 */
@Service
public class VerificationTokenManager {

    @Autowired
    private LogFactory logFactory;

    @Autowired
    private VerificationTokenDAO verificationTokenDAO;

    @Autowired
    private UserManager userManager;

    @Autowired
    private PojoUtils pojoUtils;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(VerificationTokenManager.class);

    String getMobileToken(Long userId, String contactNumber, String phoneCode, Long callingUserId) {
        VerificationTokenII smsToken = verificationTokenDAO
                .getActiveVerificationToken(userId, contactNumber, phoneCode);
        if (smsToken == null) {
            smsToken = new VerificationTokenII(userId, contactNumber, VerificationTokenType.SMS_VERIFICATION,
                    VerificationLinkStatus.ACTIVE);
            smsToken.setCode(StringUtils
                    .randomNumericString(VerificationTokenII.VERIFICATION_CODE_LENGTH));
            smsToken.setPhoneCode(phoneCode);
            verificationTokenDAO.create(smsToken, callingUserId);
        }
        return smsToken.getCode();
    }

    public String getEmailToken(Long userId, String email, VerificationTokenType verificationTokenType, Long callingUserId) {
        VerificationTokenII emailToken = verificationTokenDAO
                .getActiveVerificationToken(userId, email, null);
        if (emailToken == null) {
            emailToken = new VerificationTokenII(userId, email, verificationTokenType,
                    VerificationLinkStatus.ACTIVE);
            //emailToken.setCode(StringUtils
            //        .randomNumericString(VerificationToken.EMAIL_VERIFICATION_CODE_LENGTH));
            verificationTokenDAO.create(emailToken, callingUserId);
        }

        return emailToken.getId();
    }

    public ProcessVerificationResponse processToken(String tokenCode, String password) throws VException {
        if (StringUtils.isEmpty(tokenCode)) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "tokenCode empty");
        }
        ProcessVerificationResponse response = new ProcessVerificationResponse();
        VerificationTokenII token = verificationTokenDAO.getTokenByCode(tokenCode);
        if (token == null || (VerificationLinkStatus.USED).equals(token.getStatus())) {
            response.setStatus(VerificationStatus.INVALID_TOKEN);
            return response;
        }
        response.setUserId(token.getUserId());
        response.setStatus(VerificationStatus.VERIFIED);
        if (StringUtils.isNotEmpty(password)) {
            userManager.resetPassword(token.getUserId(), password, token.getUserId());
            token.setStatus(VerificationLinkStatus.USED);
            verificationTokenDAO.create(token, token.getUserId());
            if (VerificationTokenType.EMAIL_VERIFICATION.equals(token.getType())) {
                response.setSendWelcomeMail(true);
                //TODO: Look into this
                response.setUser(pojoUtils.convertToUserPojo(userManager.getUserByUserId(token.getUserId())));
            }
            response.setStatus(VerificationStatus.PASSWORD_RESET_SUCCESS);

        } else {
            Pair<Boolean, Boolean> verificationResponse;
            switch (token.getType()) {
                case EMAIL_VERIFICATION_SIGNUP:
                    verificationResponse = userManager.verifyEmail(token.getUserId(), token.getUserId());
                    if (verificationResponse.getRight()) {
                        response.setProcessReferralBonus(Boolean.TRUE);
                        response.setUser(pojoUtils.convertToUserPojo(userManager.getUserByUserId(token.getUserId())));
                    }
                    if (verificationResponse.getLeft()) {
                        token.setStatus(VerificationLinkStatus.USED);
                        verificationTokenDAO.create(token, token.getUserId());
                        response.setSendWelcomeMail(true);
                        response.setUser(pojoUtils.convertToUserPojo(userManager.getUserByUserId(token.getUserId())));
                        response.setStatus(VerificationStatus.VERIFIED);
                    } else {
                        response.setStatus(VerificationStatus.INVALID_TOKEN);
                    }
                    break;
                case EMAIL_VERIFICATION:
                    verificationResponse = userManager.verifyEmail(token.getUserId(), token.getUserId());
                    if (verificationResponse.getRight()) {
                        response.setProcessReferralBonus(Boolean.TRUE);
                        response.setUser(pojoUtils.convertToUserPojo(userManager.getUserByUserId(token.getUserId())));
                    }
                    if (!verificationResponse.getLeft()) {
                        response.setStatus(VerificationStatus.INVALID_TOKEN);
                        break;
                    }
                //No break here
                case RESET_PASSWORD:
                    response.setStatus(VerificationStatus.SHOW_PASSWORD_RESET);
                    break;

            }
        }

        return response;

    }

    String createResetPasswordToken(Long userId, String email, Long callingUserId) {

        VerificationTokenII emailToken = new VerificationTokenII(userId, email, VerificationTokenType.RESET_PASSWORD,
                VerificationLinkStatus.ACTIVE);
        verificationTokenDAO.create(emailToken, callingUserId);

        return emailToken.getId();
    }

    //For mobile
    public VerificationTokenII getVerificationTokenByCode(Long sessionUserId, String verificationCode) {
        return verificationTokenDAO.getTokenByCode(sessionUserId, verificationCode);
    }

    public void markUsed(VerificationTokenII token, Long callingUserId) {
        if (token != null) {
            token.setStatus(VerificationLinkStatus.USED);
            verificationTokenDAO.create(token, callingUserId);
        }
    }

    List<VerificationTokenII> getVerificationTokens(Long userId, VerificationTokenType verificationTokenType, VerificationLinkStatus verificationLinkStatus) {
        return verificationTokenDAO.getVerificationTokens(userId, verificationTokenType, verificationLinkStatus);
    }
    
    public VerificationTokenII getVerificationTokenById(String tokenId) {
        return verificationTokenDAO.getById(tokenId);
    }

    public List<VerificationTokenII> getVerificationCodes(Integer start, Integer limit, String emailIdOrContactNumber){
        start = (start == null) ? 0 : start;
        limit = ((limit == null) || (limit == 0)) ? 10 : limit;
        return verificationTokenDAO.getVerificationCodes(start,limit,emailIdOrContactNumber);
    }

}
