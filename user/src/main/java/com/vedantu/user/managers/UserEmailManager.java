package com.vedantu.user.managers;

import com.vedantu.User.UserBasicInfo;
import com.vedantu.User.VerificationTokenType;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.NotFoundException;
import com.vedantu.exception.VException;
import com.vedantu.notification.enums.CommunicationType;
import com.vedantu.user.requests.SendUserEmailReq;
import com.vedantu.util.LogFactory;
import com.vedantu.util.PlatformBasicResponse;
import com.vedantu.util.StringUtils;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.UnsupportedEncodingException;

/**
 * @author MNPK
 */

@Service
public class UserEmailManager {

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(UserManager.class);

    @Autowired
    private UserManager userManager;

    @Autowired
    private VerificationTokenManager verificationTokenManager;

    @Autowired
    private CommunicationManager communicationManager;


    public PlatformBasicResponse sendUserRelatedEmail(SendUserEmailReq req) throws VException, UnsupportedEncodingException {
        req.verify();
        VerificationTokenType verificationTokenType = null;
        if (CommunicationType.SIGNUP.equals(req.getEmailType())) {
            verificationTokenType = VerificationTokenType.EMAIL_VERIFICATION;
        }
        if (null == verificationTokenType) {
            return new PlatformBasicResponse(false, null, null);
        }
        UserBasicInfo user = userManager.getUserInfo(req.getUserId(), true);
        if (null == user) {
            throw new NotFoundException(ErrorCode.USER_NOT_FOUND, null);
        }
        if (user != null && StringUtils.isEmpty(user.getEmail())) {
            throw new NotFoundException(ErrorCode.EMAIL_NOT_FOUND, "User Email not found");
        }
        Long callingUserId = null;
        if (req.getCallingUserId() != null) {
            callingUserId = req.getCallingUserId();
        }
        String emailToken = verificationTokenManager.getEmailToken(req.getUserId(), user.getEmail(), verificationTokenType, callingUserId);
        logger.info("user : " + user);
        logger.info("type : " + req.getEmailType());
        logger.info("tokenCode : " + emailToken);
        if (StringUtils.isNotEmpty(emailToken)) {
            communicationManager.sendVerificationTokenEmail(user, req.getEmailType(), emailToken);
        }
        return new PlatformBasicResponse();
    }

}
