package com.vedantu.user.managers;

import com.google.gson.Gson;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.exception.BaseResponse;
import com.vedantu.exception.VException;
import com.vedantu.exception.VExceptionFactory;
import com.vedantu.notification.requests.CreateNotificationRequest;
import com.vedantu.notification.requests.MarkSeenReq;
import com.vedantu.notification.responses.MarkSeenRes;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.WebUtils;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

/**
 * @author MNPK
 */

@Service
public class NotificationManager {

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(NotificationManager.class);

    public BaseResponse createNotification(CreateNotificationRequest request) throws VException {
        request.verify();
        String notificationEndpoint = ConfigUtils.INSTANCE.getStringValue("NOTIFICATION_ENDPOINT");
        logger.info("CreateNotification request " + new Gson().toJson(request));
        ClientResponse resp = WebUtils.INSTANCE.doCall(notificationEndpoint + "/notification/createNotification", HttpMethod.POST, new Gson().toJson(request));
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        BaseResponse response = new Gson().fromJson(jsonString, BaseResponse.class);
        logger.info("Exiting: " + response.toString());
        return response;
    }

    public MarkSeenRes markSeenByType(MarkSeenReq req) throws VException {
        String notificationEndpoint = ConfigUtils.INSTANCE.getStringValue("NOTIFICATION_ENDPOINT");
        ClientResponse resp = WebUtils.INSTANCE.doCall(notificationEndpoint + "/notification/markSeenByType", HttpMethod.POST, new Gson().toJson(req));
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        return new Gson().fromJson(jsonString, MarkSeenRes.class);
    }

}
