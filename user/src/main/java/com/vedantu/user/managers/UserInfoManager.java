package com.vedantu.user.managers;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.vedantu.User.TeacherBasicInfo;
import com.vedantu.User.UserBasicInfo;
import com.vedantu.User.request.ExtensionNumberReq;
import com.vedantu.User.request.GetActiveTutorsReq;
import com.vedantu.User.request.GetUserSystemIntroReq;
import com.vedantu.User.response.GetActiveTutorsRes;
import com.vedantu.User.response.UserDetailsInfo;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.InternalServerErrorException;
import com.vedantu.exception.NotFoundException;
import com.vedantu.exception.VException;
import com.vedantu.user.dao.RedisDAO;
import com.vedantu.user.dao.UserDAO;
import com.vedantu.user.entity.User;
import com.vedantu.user.requests.UserPreBookingVerificationInfoReq;
import com.vedantu.user.responses.UserPreBookingVerificationInfoRes;
import com.vedantu.user.utils.PojoUtils;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.FosUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.PlatformBasicResponse;
import com.vedantu.util.StringUtils;

/**
 * @author MNPK
 */

@Service
public class UserInfoManager {

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(UserManager.class);

    @Autowired
    private FosUtils fosUtils;

    @Autowired
    private UserManager userManager;

    @Autowired
    private RedisDAO redisDAO;

    @Autowired
    public PojoUtils pojoUtils;

    @Autowired
    private UserDAO userDAO;

    private final Gson gson = new Gson();
    
    private static final String env = ConfigUtils.INSTANCE.getStringValue("environment");
    private static final Integer redisExpiry = ConfigUtils.INSTANCE.getIntValue("redis.setExpiry");


    public List<UserBasicInfo> getUserInfos(String userIdsList, boolean exposeEmail) throws VException {
        List<UserBasicInfo> response = new ArrayList<>();
        String[] ids = org.apache.commons.lang3.StringUtils.split(userIdsList, ",");
        if (ids != null) {
            List<Long> longIds = new ArrayList<>();
            for (String id : ids) {
                longIds.add(Long.parseLong(id));
            }
            if (ArrayUtils.isNotEmpty(longIds)) {
//                List<UserBasicInfo> userBasicInfos = fosUtils.getUserBasicInfosFromLongIds(longIds, exposeEmail);
                List<UserBasicInfo> userBasicInfos = userManager.getUserBasicInfos(longIds, exposeEmail);
                if (ArrayUtils.isNotEmpty(userBasicInfos)) {
                    response.addAll(userBasicInfos);
                }
            }
        }
        return response;
    }

    public PlatformBasicResponse checkUsername(String username) throws VException, UnsupportedEncodingException {
        if (StringUtils.isEmpty(username)) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "username missing");
        }
        User user = userManager.getUserByEmail(username);
        PlatformBasicResponse basicResp = new PlatformBasicResponse();
        if (user != null) {
            basicResp.setSuccess(Boolean.TRUE);
        } else {
            basicResp.setSuccess(Boolean.FALSE);
        }
        return basicResp;
    }

    public String getTeacherContactFromExtension(ExtensionNumberReq req)
            throws InternalServerErrorException, VException, IllegalArgumentException, IllegalAccessException {
        String CallSid = req.getCallSid();
        String number = redisDAO.get(CallSid);
        if (StringUtils.isNotEmpty(number)) {
            return number;
        }

       String phoneNumber = userManager.getTeacherContactFromExtension(req);
        redisDAO.set(req.getCallSid(), phoneNumber);
        return phoneNumber;
    }

    public UserDetailsInfo getUserDetailsForISLInfo(Long userId, String event) throws VException {
        UserDetailsInfo info = getUserDetailsInfo(userId, event);
        if (info != null) {
            info.setServerTime(System.currentTimeMillis());
            return info;
        }
        UserDetailsInfo response = userManager.getUserDetailsForISLInfo(userId, event);
        response.setServerTime(System.currentTimeMillis());
        return response;
    }

    public UserDetailsInfo getUserDetailsInfo(Long userId, String event) {

        String key = "PROD_USER_DETAILS_" + userId + "_EVENT_" + event;
        String keybasicInfo = "PROD_USER_BASIC_" + userId;
        try {
            String detailstring = redisDAO.get(key);
            if (StringUtils.isNotEmpty(detailstring)) {
                UserDetailsInfo info = gson.fromJson(detailstring, UserDetailsInfo.class);
                if (info != null) {
                    UserBasicInfo basicInfo = null;
                    String userString = redisDAO.get(keybasicInfo);
                    if (StringUtils.isNotEmpty(userString)) {
                        basicInfo = gson.fromJson(userString, UserBasicInfo.class);
                    } else {
                        basicInfo = fosUtils.getUserBasicInfo(userId, true);
                    }

                    info.setStudent(basicInfo);
                    return info;
                }
            }
        } catch (Exception e) {
            logger.info("Exception");
        }

        return null;
    }

    public String getUserSystemIntroData(GetUserSystemIntroReq req, HttpServletRequest request) throws NotFoundException, BadRequestException {
       return userManager.getUserSystemIntroDataRes(req);
    }

    public GetActiveTutorsRes getActiveTutorsList(HttpServletRequest httpRequest, GetActiveTutorsReq reqPojo) {
        List<User> users = userDAO.getTutorsWithProfilePic(reqPojo.getStart(), reqPojo.getSize());
        GetActiveTutorsRes respPojo = new GetActiveTutorsRes();
        if (users != null && reqPojo.getStart() != null && reqPojo.getSize() != null
                && users.size() == (reqPojo.getSize() - reqPojo.getStart())) {
            respPojo.setHasNext(Boolean.TRUE);
        }
        for (User user : users) {
            // log("user = "+user.getProfileEnabled()+", active = "+user.getTeacherInfo());
            Boolean profileEnabled = user.getProfileEnabled();
            if ((profileEnabled == null || profileEnabled) && user.getTeacherInfo() != null) {
                Boolean isActive = user.getTeacherInfo().getActive();
                if (isActive == null || isActive) {
                    respPojo.addUser(new TeacherBasicInfo(pojoUtils.convertToUserPojo(user), false));
                }
            }
        }
        return respPojo;
    }
    public UserBasicInfo getUserByContactNumber(String contactNumber) throws BadRequestException, InternalServerErrorException {
        UserBasicInfo user = userDAO.getUserByContactNumber(contactNumber);

        return user;
    }

    public UserPreBookingVerificationInfoRes preBookingVerification(UserPreBookingVerificationInfoReq req) {
        UserPreBookingVerificationInfoRes userPreBookingVerificationInfoRes = new UserPreBookingVerificationInfoRes();
        List<User> users = userDAO.getUsersByEmailOrPhone(req.getEmail(), req.getPhoneNumber(), req.getPhoneCode());
        logger.info("users - {}", users);
        users.stream().filter(Objects::nonNull).forEach(user -> {
            User verifyUser = user;
            if(req.getEmail().equalsIgnoreCase(verifyUser.getEmail())) {
                logger.info("user with email found");
                userPreBookingVerificationInfoRes.setEmailExists(Boolean.TRUE);
                if(verifyUser.getIsContactNumberVerified()) {
                    userPreBookingVerificationInfoRes.setUserHasVerifiedPhoneNumber(Boolean.TRUE);
                }
            }
            if(req.getPhoneNumber().equalsIgnoreCase(verifyUser.getContactNumber())) {
                logger.info("user with phoneNumber found");
                userPreBookingVerificationInfoRes.setPhoneNumberExists(Boolean.TRUE);
                if(org.apache.commons.lang3.StringUtils.isNotEmpty(verifyUser.getEmail())) {
                    userPreBookingVerificationInfoRes.setUserHasEmail(Boolean.TRUE);
                }
            }
        });
        if(userPreBookingVerificationInfoRes.getEmailExists() && userPreBookingVerificationInfoRes.getPhoneNumberExists() && users.size() == 1) {
            logger.info("userEmail Linked");
            userPreBookingVerificationInfoRes.setIsContactEmailLinked(Boolean.TRUE);
        }
        return userPreBookingVerificationInfoRes;
    }
    
    public List<TeacherBasicInfo> getTeacherBasicInfos(List<Long> teacherIdsList) {

    	Map<Long, TeacherBasicInfo> infoMap = new HashMap<Long, TeacherBasicInfo>();
    	infoMap = getTeacherBasicInfoFromRedis(teacherIdsList);
    	logger.debug("UserManager getTeacherBasicInfos infoMap -> "+infoMap);
    	Map<String, String> redisMap = new HashMap<>();

    	List<Long> fetchList = new ArrayList<>();
    	if (ArrayUtils.isNotEmpty(teacherIdsList)) {
    		if (!infoMap.isEmpty()) {
    			for (Long id : teacherIdsList) {
    				if (!infoMap.containsKey(id)) {
    					fetchList.add(id);
    				}
    			}
    		} else {
    			fetchList.addAll(teacherIdsList);
    		}
    	}

    	int LIMIT = 20;
    	int listSize = fetchList.size();
    	if (ArrayUtils.isNotEmpty(fetchList)) {
    		logger.info("Fetching teacherbasicInfos from DB : {}", fetchList);
    		for (int i = 0; i < listSize; i += LIMIT) {
    			List<Long> subList;
    			if (listSize > i + LIMIT) {
    				subList = fetchList.subList(i, (i + LIMIT));
    			} else {
    				subList = fetchList.subList(i, listSize);
    			}
    			List<User> users = new ArrayList<>();
    			if (ArrayUtils.isNotEmpty(fetchList)) {
    				users = userDAO.getTeacherBasicInfoByIds(fetchList, null, null);
    				logger.debug("UserManager getTeacherBasicInfos users -> "+users);
    			}
    			if (ArrayUtils.isNotEmpty(users)) {
    				for (User user : users) {
    					if (null != user) {
    						TeacherBasicInfo teacherBasicInfo = convertUserToTeacherBasicInfo(user);
    						redisMap.put(getTeacherBasicInfoKey(user.getId()), gson.toJson(teacherBasicInfo));
    						logger.debug("UserManager getTeacherBasicInfos teacherBasicInfo - > "+teacherBasicInfo);
    						infoMap.put(user.getId(), teacherBasicInfo);
    					}
    				}
    				if (!redisMap.isEmpty()) {
    					try {
    						redisDAO.setexKeyPairs(redisMap, 900);
    					} catch (Exception e) {
    						logger.info("Exception occured during setting redis key map ->"+e);
    					}
    				}
    			}
    		}
    	}
    	return new ArrayList<TeacherBasicInfo>(infoMap.values());
    }

    private TeacherBasicInfo convertUserToTeacherBasicInfo(User user) {
    	TeacherBasicInfo teacherBasicInfo = new TeacherBasicInfo();
    	teacherBasicInfo.setUserId(user.getId());
    	teacherBasicInfo.setFirstName(user.getFirstName());
    	teacherBasicInfo.setLastName(user.getLastName());
    	teacherBasicInfo.setFullName(user.getFullName());
    	if(null != user.getTeacherInfo()) {
    		teacherBasicInfo.setLatestEducation(user.getTeacherInfo().getLatestEducation());
    		//Fetch Actual Subject from subjectId-Not clear on logic 
    		teacherBasicInfo.setPrimarySubject(user.getTeacherInfo().getPrimarySubject());
    		teacherBasicInfo.setSalientPoints(user.getTeacherInfo().getSalientPoints());
    		teacherBasicInfo.setTeacherSmallImage(user.getTeacherInfo().getTeacherSmallImage());
    		teacherBasicInfo.setThumbnailUrlApp(user.getTeacherInfo().getThumbnailUrlApp());
    		teacherBasicInfo.setThumbnailUrlWeb(user.getTeacherInfo().getThumbnailUrlWeb());
    		teacherBasicInfo.setTransparentImage(user.getTeacherInfo().getTransparentImage());
    	}
    	return teacherBasicInfo;

    }

    private Map<Long, TeacherBasicInfo> getTeacherBasicInfoFromRedis(List<Long> teacherIdsList) {
    	Map<Long, TeacherBasicInfo> infoMap = new HashMap<Long, TeacherBasicInfo>();
    	if (ArrayUtils.isNotEmpty(teacherIdsList)) {
    		List<String> keys = new ArrayList<>();
    		for (Long id : teacherIdsList) {
    			if (null != id) {
    				keys.add(getTeacherBasicInfoKey(id));
    			}
    		}
    		Map<String, String> valueMap = new HashMap<>();
    		try {
    			valueMap = redisDAO.getValuesForKeys(keys);
    		} catch (Exception e) {
    			logger.info("Error in Redis getValuesForKeys "+e);
    		}

    		if (valueMap != null && !valueMap.isEmpty()) {
    			for (Long id : teacherIdsList) {
    				if (id != null) {
    					String value = valueMap.get(getTeacherBasicInfoKey(id));
    					if (StringUtils.isNotEmpty(value)) {
    						TeacherBasicInfo teacherBasicInfo = gson.fromJson(value, TeacherBasicInfo.class);
    						if (teacherBasicInfo != null) {
    							infoMap.put(id, teacherBasicInfo);
    						}
    					}
    				}
    			}
    		}
    	}

    	return infoMap;
    }

    public String getTeacherBasicInfoKey(Long id) {
    	return env + "_TEACHER_BASIC_INFO_" + id;
    }
}
