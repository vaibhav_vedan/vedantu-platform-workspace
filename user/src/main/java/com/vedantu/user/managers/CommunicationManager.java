/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.user.managers;

import com.google.gson.Gson;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.User.Gender;
import com.vedantu.User.Role;
import com.vedantu.User.UserBasicInfo;
import com.vedantu.User.UserInfo;
import com.vedantu.User.enums.VoltStatus;
import com.vedantu.User.request.GeneratePasswordReq;
import com.vedantu.User.response.GeneratePasswordRes;
import com.vedantu.User.response.UserDetailsInfo;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.NotFoundException;
import com.vedantu.exception.VException;
import com.vedantu.exception.VExceptionFactory;
import com.vedantu.notification.enums.CommunicationType;
import com.vedantu.notification.enums.EmailPriorityType;
import com.vedantu.notification.enums.EmailService;
import com.vedantu.notification.requests.EmailRequest;
import com.vedantu.notification.requests.TextSMSRequest;
import com.vedantu.user.entity.User;
import com.vedantu.util.*;
import com.vedantu.util.enums.SQSMessageType;
import com.vedantu.util.enums.SQSQueue;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.*;
import javax.mail.internet.InternetAddress;

import com.vedantu.util.pojo.VsatEventDetailsPojo;
import com.vedantu.util.pojo.VsatSchedule;
import com.vedantu.util.security.HttpSessionData;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

/**
 * @author jeet
 */
@Service
public class CommunicationManager {

    @Autowired
    private LogFactory logFactory;

    @Autowired
    private AwsSQSManager awsSQSManager;

    @Autowired
    private UserManager userManager;

    @Autowired
    private SMSManager smsManager;

    @Autowired
    private FosUtils userUtils;


    private Logger logger = logFactory.getLogger(CommunicationManager.class);
    private static final SimpleDateFormat sdfDate = new SimpleDateFormat("dd-MM-yyyy");
    private static final SimpleDateFormat sdfDay = new SimpleDateFormat("E");
    private static final SimpleDateFormat sdfTime = new SimpleDateFormat("hh:mma");

    final Gson gson = new Gson();

    public String sendEmail(EmailRequest request) throws VException {
        logger.info("Inside sendEmail" + gson.toJson(request));
        awsSQSManager.sendToSQS(SQSQueue.NOTIFICATION_QUEUE, SQSMessageType.SEND_EMAIL, gson.toJson(request));
        return null;
    }

    public String sendSMS(TextSMSRequest request) throws VException {
        if (StringUtils.isEmpty(request.getTo())) {
            logger.warn("To field is empty: " + request);
            return null;
        }

        awsSQSManager.sendToSQS(SQSQueue.NOTIFICATION_QUEUE, SQSMessageType.SEND_SMS, new Gson().toJson(request));
        return null;
    }

    public void sendGeneratePasswordEmailAndSMS(GeneratePasswordRes res) throws IOException, VException {
        CommunicationType emailType = CommunicationType.GENERATE_PASSWORD;
        HashMap<String, Object> bodyScopes = new HashMap<>();
        bodyScopes.put("password", res.getNewPassword());
        bodyScopes.put("email", res.getEmail());
        bodyScopes.put("fullName", res.getFullName());
        bodyScopes.put("baseUrl", ConfigUtils.INSTANCE.getStringValue("url.base"));

        ArrayList<InternetAddress> toList = new ArrayList<>();
        toList.add(new InternetAddress(res.getEmail(), res.getFullName()));
        EmailRequest request = new EmailRequest(toList, bodyScopes, bodyScopes, emailType, res.getRole());
        request.setPriorityType(EmailPriorityType.HIGH);
        sendEmailViaRest(request);
        TextSMSRequest textSMSRequest = new TextSMSRequest(res.getContactNumber(),
                res.getPhoneCode(), bodyScopes, emailType, res.getRole());
        sendSMSViaRest(textSMSRequest);
    }

    public String sendEmailViaRest(EmailRequest request) throws VException {
        String notificationEndpoint = ConfigUtils.INSTANCE.getStringValue("NOTIFICATION_ENDPOINT");
        ClientResponse resp = WebUtils.INSTANCE.doCall(notificationEndpoint + "/email/sendEmail", HttpMethod.POST,
                new Gson().toJson(request));

        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        logger.info("Response from notification-centre " + jsonString);
        return jsonString;
    }

    public String sendSMSViaRest(TextSMSRequest request) throws VException {
        if (StringUtils.isEmpty(request.getTo())) {
            logger.warn("To field is empty: " + request);
            return null;
        }
        String notificationEndpoint = ConfigUtils.INSTANCE.getStringValue("NOTIFICATION_ENDPOINT");
        ClientResponse resp = WebUtils.INSTANCE.doCall(notificationEndpoint + "/SMS/sendSMS", HttpMethod.POST,
                new Gson().toJson(request));
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        logger.info("Response from notification-centre " + jsonString);
        return jsonString;
    }

    public void sendKYCLink(Long userId) throws VException, UnsupportedEncodingException {
        CommunicationType communicationType = CommunicationType.KYC_UPDATE_LINK;
        User student = userManager.getUserByUserId(userId);
        if (StringUtils.isEmpty(student.getEmail())) {
            throw new BadRequestException(ErrorCode.EMAIL_NOT_FOUND, "Email not found to send kyclink");
        }
        logger.info("STUDENT EMAIL: " + student.getEmail());
        String studentProfileLink = null;

        if (student == null) {
            throw new NotFoundException(ErrorCode.USER_NOT_FOUND, "user not found " + userId);
        }

        HashMap<String, Object> scopeParams = new HashMap<>();
        scopeParams.put("studentName", student.getFullName());
        String fosEnvironment = ConfigUtils.INSTANCE.getStringValue("FOS_ENDPOINT");

        if (StringUtils.isEmpty(fosEnvironment)) {
            fosEnvironment = "https://www.vedantu.com";
        }

        studentProfileLink = fosEnvironment + "/v/user/edit/" + student.getId();
        scopeParams.put("studentProfileEditLink", studentProfileLink);

        ArrayList<InternetAddress> toList = new ArrayList<>();
        toList.add(new InternetAddress(student.getEmail(), student.getFullName()));
        EmailRequest request = new EmailRequest(toList, scopeParams, scopeParams, communicationType, Role.STUDENT);
        sendEmail(request);
        logger.info("EMAIL SENT DONE");
    }

    public void sendEmailUserDetailsCreatedViaTools(com.vedantu.User.User user) throws IOException, VException {
        CommunicationType emailType = CommunicationType.USER_DETAILS_CREATED_VIA_ISL_REGISTRATION_TOOLS;
        HashMap<String, Object> bodyScopes = new HashMap<>();
        bodyScopes.put("studentName", user._getFullName());
        bodyScopes.put("studentEmail", user.getEmail());
        if (user.getStudentInfo() != null) {
            bodyScopes.put("parentName", user.getStudentInfo().getParentFirstName());
        }
        String loginUrl = ConfigUtils.INSTANCE.getStringValue("web.baseUrl");
        bodyScopes.put("loginUrl", loginUrl);

        bodyScopes.put("baseUrl", ConfigUtils.INSTANCE.getStringValue("url.base"));
        UserDetailsInfo newInfo = userManager.getUserDetailsForISLInfo(user.getId(), "ISL_2017");
        if (newInfo != null) {
            String category = getISLCategoryString(newInfo);
            bodyScopes.put("category", category);
        }
        HashMap<String, Object> subjectScopes = new HashMap<>();
        subjectScopes.put("name", user.getFirstName());
        ArrayList<InternetAddress> toList = new ArrayList<>();
        toList.add(new InternetAddress(user.getEmail(), user._getFullName()));
        EmailRequest request = new EmailRequest(toList, subjectScopes, bodyScopes, emailType, user.getRole());
        request.setIncludeHeaderFooter(Boolean.FALSE);
        sendEmail(request);
        if (StringUtils.isNotEmpty(user.getContactNumber())) {
            TextSMSRequest textSMSRequest = new TextSMSRequest(user.getContactNumber(),
                    user.getPhoneCode(), bodyScopes, emailType, Role.STUDENT);
            smsManager.sendSMS(textSMSRequest);
        }
    }

    private String getISLCategoryString(UserDetailsInfo userDetailsInfo) {
        String category = null;
        if (null != userDetailsInfo.getCategory()) {
            switch (userDetailsInfo.getCategory()) {
                case "ISL_2017_CAT_3_5":
                    category = "In Category 1 : Grades 3-5";
                    break;
                case "ISL_2017_CAT_6_8":
                    category = "In Category 2 : Grades 6-8";
                    break;
                case "ISL_2017_CAT_9_10":
                    category = "In Category 3 : Grades 9-10";
                    break;
                default:
                    break;
            }
        }
        return category;
    }

    public void sendRegistrationEmailForISL(com.vedantu.User.User user) throws IOException, VException {
        logger.info("sending welcome email to : " + user.getEmail());
        CommunicationType emailType = CommunicationType.REGISTRATION_EMAIL_ISL;
        HashMap<String, Object> bodyScopes = new HashMap<>();
        bodyScopes.put("studentName", user._getFullName());
        bodyScopes.put("studentEmail", user.getEmail());
        if (user.getStudentInfo() != null) {
            bodyScopes.put("parentName", user.getStudentInfo().getParentFirstName());
        }
        String loginUrl = ConfigUtils.INSTANCE.getStringValue("web.baseUrl");
        bodyScopes.put("loginUrl", loginUrl);

        bodyScopes.put("baseUrl", ConfigUtils.INSTANCE.getStringValue("url.base"));

        UserDetailsInfo newInfo = userManager.getUserDetailsForISLInfo(user.getId(), "ISL_2017");
        if (newInfo != null) {
            String category = getISLCategoryString(newInfo);
            bodyScopes.put("category", category);
        }

        HashMap<String, Object> subjectScopes = new HashMap<>();
        subjectScopes.put("name", user.getFirstName());
        ArrayList<InternetAddress> toList = new ArrayList<>();
        toList.add(new InternetAddress(user.getEmail(), user._getFullName()));
        EmailRequest request = new EmailRequest(toList, subjectScopes, bodyScopes, emailType, user.getRole());

        request.setIncludeHeaderFooter(Boolean.FALSE);
        sendEmail(request);
    }

    public void sendRegistrationEmailForVSAT(com.vedantu.User.User user) throws IOException, VException {
        logger.info("sending welcome email to : " + user.getEmail());
        VsatEventDetailsPojo vsatEventDetailsPojo = userUtils.getVsatEventDetails();
        CommunicationType emailType = CommunicationType.REGISTRATION_EMAIL_VSAT;
        HashMap<String, Object> bodyScopes = new HashMap<>();
        bodyScopes.put("studentName", user._getFullName());
        bodyScopes.put("studentEmail", user.getEmail());
        if (user.getStudentInfo() != null) {
            bodyScopes.put("parentName", user.getStudentInfo().getParentFirstName());
        }
        if (StringUtils.isNotEmpty(user.getContactNumber())) {
            bodyScopes.put("contactNumber", user.getContactNumber());
        } else if (StringUtils.isNotEmpty(user.getTempContactNumber())) {
            bodyScopes.put("contactNumber", user.getTempContactNumber());
        } else {
            bodyScopes.put("contactNumber", user.getContactNumber());
        }

        String loginUrl = ConfigUtils.INSTANCE.getStringValue("web.baseUrl");
        bodyScopes.put("loginUrl", loginUrl);

        bodyScopes.put("baseUrl", ConfigUtils.INSTANCE.getStringValue("url.base"));

        //building VSAT start Date text
        List<String> dateStringsList = new ArrayList<>();
        String yearString = "";

        List<VsatSchedule> vsatSchedules = vsatEventDetailsPojo.getVsatSchedules();
        if (ArrayUtils.isNotEmpty(vsatSchedules)) {
            for (VsatSchedule vsatSchedule : vsatSchedules) {
                if (vsatSchedule.getStartTime() != null) {
                    SimpleDateFormat sdfForDateString = new SimpleDateFormat("EEEEE, dd MMM yyyy hh:mm a");
                    sdfForDateString.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));
                    dateStringsList.add(sdfForDateString.format(new Date(vsatSchedule.getStartTime())));
                }
            }
            SimpleDateFormat sdfForyearString = new SimpleDateFormat("yyyy");
            sdfForyearString.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));
            yearString = sdfForyearString.format(new Date(vsatSchedules.get(vsatSchedules.size() - 1).getStartTime()));
        }
        bodyScopes.put("dateString", String.join(" & ", dateStringsList));
        bodyScopes.put("yearString", yearString);

        UserDetailsInfo newInfo = userManager.getUserDetailsForISLInfo(user.getId(), vsatEventDetailsPojo.getEventName());

        // NEED TO MAKE CHANGES HERE
        if (newInfo != null) {
            bodyScopes.put("studentTarget", newInfo.getExam());
            if (null != newInfo.getGrade()) {
                bodyScopes.put("studentGrade", newInfo.getGrade().toString());
            }
        }

        HashMap<String, Object> subjectScopes = new HashMap<>();
        subjectScopes.put("name", user.getFirstName());
        ArrayList<InternetAddress> toList = new ArrayList<>();
        toList.add(new InternetAddress(user.getEmail(), user._getFullName()));
        EmailRequest request = new EmailRequest(toList, subjectScopes, bodyScopes, emailType, user.getRole());

        request.setIncludeHeaderFooter(Boolean.FALSE);
        request.setEmailService(EmailService.MANDRILL);
        sendEmailViaRest(request);
        TextSMSRequest textSMSRequest = new TextSMSRequest(user.getContactNumber(),
                user.getPhoneCode(), bodyScopes, emailType, user.getRole());
        smsManager.sendSMSViaRest(textSMSRequest);
    }

    public void sendRegistrationEmailForVolt(com.vedantu.User.User user) throws IOException, VException {
        logger.info("sending welcome email to : " + user.getEmail());
        HashMap<String, Object> bodyScopes = new HashMap<>();
        bodyScopes.put("studentName", user._getFullName());
        bodyScopes.put("studentEmail", user.getEmail());
        if (user.getStudentInfo() != null) {
            bodyScopes.put("parentName", user.getStudentInfo().getParentFirstName());
        }
        if (StringUtils.isNotEmpty(user.getContactNumber())) {
            bodyScopes.put("contactNumber", user.getContactNumber());
        } else if (StringUtils.isNotEmpty(user.getTempContactNumber())) {
            bodyScopes.put("contactNumber", user.getTempContactNumber());
        } else {
            bodyScopes.put("contactNumber", user.getContactNumber());
        }

        String loginUrl = ConfigUtils.INSTANCE.getStringValue("web.baseUrl");
        bodyScopes.put("loginUrl", loginUrl);

        bodyScopes.put("baseUrl", ConfigUtils.INSTANCE.getStringValue("url.base"));

        UserDetailsInfo newInfo = userManager.getUserDetailsForISLInfo(user.getId(), "VOLT_2020_JAN");
        CommunicationType emailType;
        if (newInfo.getVoltStatus() == VoltStatus.REGISTERED) {
            emailType = CommunicationType.REGISTRATION_EMAIL_VOLT;
        } else {
            emailType = CommunicationType.REQUEST_CONFIRM_EMAIL_VOLT;
        }

        // NEED TO MAKE CHANGES HERE
        if (newInfo != null) {
            bodyScopes.put("studentTarget", newInfo.getExam());
            if (null != newInfo.getGrade()) {
                bodyScopes.put("studentGrade", newInfo.getGrade().toString());
            }
        }

        HashMap<String, Object> subjectScopes = new HashMap<>();
        subjectScopes.put("name", user.getFirstName());
        ArrayList<InternetAddress> toList = new ArrayList<>();
        toList.add(new InternetAddress(user.getEmail(), user._getFullName()));
        EmailRequest request = new EmailRequest(toList, subjectScopes, bodyScopes, emailType, user.getRole());

        request.setIncludeHeaderFooter(Boolean.FALSE);
        request.setEmailService(EmailService.MANDRILL);
        sendEmailViaRest(request);
        if (emailType == CommunicationType.REGISTRATION_EMAIL_VOLT) {
            TextSMSRequest textSMSRequest = new TextSMSRequest(user.getContactNumber(),
                    user.getPhoneCode(), bodyScopes, emailType, user.getRole());
            smsManager.sendSMSViaRest(textSMSRequest);
        }
    }

    public void sendRegistrationEmailForJRP(com.vedantu.User.User user, UserDetailsInfo info) throws IOException, VException {
        logger.info("sending welcome email to : " + user.getEmail());
        CommunicationType emailType = CommunicationType.REGISTERED_JRP;
        HashMap<String, Object> bodyScopes = new HashMap<>();
        bodyScopes.put("studentName", info.getStudentName());
        bodyScopes.put("email", info.getEmail());
        bodyScopes.put("contactNumber", info.getStudentPhoneNo());
        bodyScopes.put("grade", info.getGrade().toString());

        String loginUrl = ConfigUtils.INSTANCE.getStringValue("web.baseUrl");
        bodyScopes.put("loginUrl", loginUrl);

        bodyScopes.put("baseUrl", ConfigUtils.INSTANCE.getStringValue("url.base"));

        bodyScopes.put("link1", ConfigUtils.INSTANCE.getStringValue("web.baseUrl") + "/v/studentprofile");
        bodyScopes.put("link2", ConfigUtils.INSTANCE.getStringValue("web.baseUrl") + "/iit-jee/jee-online-mock-test");

        HashMap<String, Object> subjectScopes = new HashMap<>();
        subjectScopes.put("name", user.getFirstName());
        subjectScopes.put("studentName", info.getStudentName());
        ArrayList<InternetAddress> toList = new ArrayList<>();
        toList.add(new InternetAddress(user.getEmail(), user._getFullName()));
        EmailRequest request = new EmailRequest(toList, subjectScopes, bodyScopes, emailType, Role.STUDENT);

        request.setIncludeHeaderFooter(Boolean.FALSE);
//        request.setEmailService(EmailService.MANDRILL);
        sendEmail(request);
        TextSMSRequest textSMSRequest = new TextSMSRequest(info.getStudentPhoneNo(),
                info.getStudentPhoneCode(), bodyScopes, CommunicationType.REGISTERED_JRP, Role.STUDENT);
        smsManager.sendSMS(textSMSRequest);

    }

    public void sendRegistrationEmailForReviseIndia(com.vedantu.User.User user) throws UnsupportedEncodingException, VException {
        logger.info("sending welcome email to : " + user.getEmail());
        HashMap<String, Object> bodyScopes = new HashMap<>();
        bodyScopes.put("studentName", user._getFullName());
        bodyScopes.put("studentEmail", user.getEmail());
        if (user.getStudentInfo() != null) {
            bodyScopes.put("parentName", user.getStudentInfo().getParentFirstName());
        }
        if (StringUtils.isNotEmpty(user.getContactNumber())) {
            bodyScopes.put("contactNumber", user.getContactNumber());
        } else if (StringUtils.isNotEmpty(user.getTempContactNumber())) {
            bodyScopes.put("contactNumber", user.getTempContactNumber());
        } else {
            bodyScopes.put("contactNumber", user.getContactNumber());
        }

        String loginUrl = ConfigUtils.INSTANCE.getStringValue("web.baseUrl");
        bodyScopes.put("loginUrl", loginUrl);

        bodyScopes.put("baseUrl", ConfigUtils.INSTANCE.getStringValue("url.base"));

        UserDetailsInfo newInfo = userManager.getUserDetailsForISLInfo(user.getId(), "REVISEINDIA_2020_FEB");
        CommunicationType emailType = CommunicationType.REGISTRATION_EMAIL_REVISEINDIA;

//        // NEED TO MAKE CHANGES HERE
//        if (newInfo != null) {
//            bodyScopes.put("studentTarget", newInfo.getExam());
//            if (null != newInfo.getGrade()) {
//                bodyScopes.put("studentGrade", newInfo.getGrade().toString());
//            }
//        }

        HashMap<String, Object> subjectScopes = new HashMap<>();
        subjectScopes.put("name", user.getFirstName());
        ArrayList<InternetAddress> toList = new ArrayList<>();
        toList.add(new InternetAddress(user.getEmail(), user._getFullName()));
        EmailRequest request = new EmailRequest(toList, subjectScopes, bodyScopes, emailType, user.getRole());

        request.setIncludeHeaderFooter(Boolean.FALSE);
        request.setEmailService(EmailService.MANDRILL);
        sendEmailViaRest(request);
        if (emailType == CommunicationType.REGISTRATION_EMAIL_REVISEINDIA) {
            CommunicationType communicationType = CommunicationType.REGISTRATION_SMS_REVISEINDIA;
            TextSMSRequest textSMSRequest = new TextSMSRequest(user.getContactNumber(),
                    user.getPhoneCode(), bodyScopes, communicationType, user.getRole());
            smsManager.sendSMSViaRest(textSMSRequest);
        }
    }

    public void sendRegistrationEmailForReviseJee(com.vedantu.User.User user) throws UnsupportedEncodingException, VException {
        logger.info("sending welcome email to : " + user.getEmail());
        HashMap<String, Object> bodyScopes = new HashMap<>();
        bodyScopes.put("studentName", user._getFullName());
        bodyScopes.put("studentEmail", user.getEmail());
        if (user.getStudentInfo() != null) {
            bodyScopes.put("parentName", user.getStudentInfo().getParentFirstName());
        }
        if (StringUtils.isNotEmpty(user.getContactNumber())) {
            bodyScopes.put("contactNumber", user.getContactNumber());
        } else if (StringUtils.isNotEmpty(user.getTempContactNumber())) {
            bodyScopes.put("contactNumber", user.getTempContactNumber());
        } else {
            bodyScopes.put("contactNumber", user.getContactNumber());
        }

        String loginUrl = ConfigUtils.INSTANCE.getStringValue("web.baseUrl");
        bodyScopes.put("loginUrl", loginUrl);

        bodyScopes.put("baseUrl", ConfigUtils.INSTANCE.getStringValue("url.base"));

        UserDetailsInfo newInfo = userManager.getUserDetailsForISLInfo(user.getId(), "REVISE_JEE_2020_MARCH");
        CommunicationType emailType = CommunicationType.REGISTRATION_EMAIL_REVISEJEE;

//        // NEED TO MAKE CHANGES HERE
//        if (newInfo != null) {
//            bodyScopes.put("studentTarget", newInfo.getExam());
//            if (null != newInfo.getGrade()) {
//                bodyScopes.put("studentGrade", newInfo.getGrade().toString());
//            }
//        }

        HashMap<String, Object> subjectScopes = new HashMap<>();
        subjectScopes.put("name", user.getFirstName());
        ArrayList<InternetAddress> toList = new ArrayList<>();
        toList.add(new InternetAddress(user.getEmail(), user._getFullName()));
        EmailRequest request = new EmailRequest(toList, subjectScopes, bodyScopes, emailType, user.getRole());

        request.setIncludeHeaderFooter(Boolean.FALSE);
        request.setEmailService(EmailService.MANDRILL);
        sendEmailViaRest(request);
        if (emailType == CommunicationType.REGISTRATION_EMAIL_REVISEJEE) {
            CommunicationType communicationType = CommunicationType.REGISTRATION_SMS_REVISEJEE;
            TextSMSRequest textSMSRequest = new TextSMSRequest(user.getContactNumber(),
                    user.getPhoneCode(), bodyScopes, communicationType, user.getRole());
            smsManager.sendSMSViaRest(textSMSRequest);
        }
    }

    public void sendVerificationTokenEmail(UserBasicInfo user, CommunicationType emailType, String emailTokenCode) throws VException, UnsupportedEncodingException {
        if (StringUtils.isEmpty(user.getEmail())) {
            return;
        }
        logger.info("Sending verification email" + user);
        HashMap<String, Object> bodyScopes = new HashMap<>();
        bodyScopes.put("name", user.getFirstName());
        bodyScopes.put("role", user.getRole());
        String pronoun = (null != user.getGender() && null != user.getGender().getPronoun())
                ? user.getGender().getPronoun()
                : Gender.OTHER.getPronoun();
        bodyScopes.put("pronoun", pronoun);
        bodyScopes.put("Pronoun", StringUtils.captialize(pronoun));
        String verifyUrl = ConfigUtils.INSTANCE.getStringValue("web.baseUrl")
                + ConfigUtils.INSTANCE.getStringValue("url.verify.token") + emailTokenCode;
        bodyScopes.put("link", verifyUrl);

        String signupBonus = ConfigUtils.INSTANCE.getStringValue("freebies.rupees.join.bonus");
        bodyScopes.put("signupBonus", signupBonus);

        HashMap<String, Object> subjectScopes = new HashMap<>();
        subjectScopes.put("name", user.getFirstName());
        subjectScopes.put("rupees", signupBonus);
        ArrayList<InternetAddress> toList = new ArrayList<>();
        toList.add(new InternetAddress(user.getEmail(), user.getFullName()));
        EmailRequest request = new EmailRequest(toList, subjectScopes, bodyScopes, emailType, user.getRole());

        sendEmail(request);
    }

    public void sendBlockUserToAdmin(Long callingUserId, Long userId, String reason, HttpSessionData callerUser)
            throws VException, UnsupportedEncodingException {
        String emailAddrSC = ConfigUtils.INSTANCE.getStringValue("email.ADDRESS.STUDENT_CARE");
        String emailAddrTC = ConfigUtils.INSTANCE.getStringValue("email.ADDRESS.TEACHER_CARE");
        String emailAddrAtom = ConfigUtils.INSTANCE.getStringValue("email.display.emailId");
        UserInfo blockedUser = userManager.getUserInfo(userId, true);
        // HttpSessionData callerUser = sessionUtils.getCurrentSessionData();
        logger.info("sendBlockUserToAdmin", blockedUser._getFullName(), callerUser.getFirstName());
        logger.info("sending block user email to : " + emailAddrSC + ", " + emailAddrTC + ", " + emailAddrAtom);
        CommunicationType emailType = CommunicationType.BLOCKED_USER;
        HashMap<String, Object> bodyScopes = new HashMap<>();
        HashMap<String, Object> subjectScopes = new HashMap<>();

        String callerFullName = callerUser.getFirstName() + " "
                + (!StringUtils.isEmpty(callerUser.getLastName()) ? callerUser.getLastName() : "");
        String blockedUserFullName = blockedUser.getFirstName() + " "
                + (!StringUtils.isEmpty(blockedUser.getLastName()) ? blockedUser.getLastName() : "");

        bodyScopes.put("callerUserRole", callerUser.getRole().name().toLowerCase());
        bodyScopes.put("callerUserName", callerFullName);
        bodyScopes.put("callerUserId", callerUser.getUserId());

        bodyScopes.put("blockedUserRole", blockedUser.getRole().name().toLowerCase());
        bodyScopes.put("blockedUserName", blockedUserFullName);
        bodyScopes.put("blockedUserId", blockedUser.getUserId());
        bodyScopes.put("reason", reason);

        subjectScopes.put("callerUserRole", callerUser.getRole().name().toLowerCase());
        subjectScopes.put("callerUserName", callerFullName);

        subjectScopes.put("blockedUserRole", blockedUser.getRole().name().toLowerCase());
        subjectScopes.put("blockedUserName", blockedUserFullName);

        ArrayList<InternetAddress> toList = new ArrayList<>();
        toList.add(new InternetAddress(emailAddrSC, "Student Care"));
        toList.add(new InternetAddress(emailAddrTC, "Teacher Care"));
        toList.add(new InternetAddress(emailAddrAtom, "Atom"));
        EmailRequest request = new EmailRequest(toList, subjectScopes, bodyScopes, emailType, Role.ADMIN);
        sendEmail(request);
    }

    public void sendEmailRegistrationISLViaTools(com.vedantu.User.User user) throws IOException, VException {
        CommunicationType emailType = CommunicationType.SIGNUP_VIA_ISL_REGISTRATION_TOOLS;
        HashMap<String, Object> bodyScopes = new HashMap<>();
        bodyScopes.put("studentName", user._getFullName());
        bodyScopes.put("studentEmail", user.getEmail());
        if (user.getStudentInfo() != null) {
            bodyScopes.put("parentName", user.getStudentInfo().getParentFirstName());
        }
        String loginUrl = ConfigUtils.INSTANCE.getStringValue("web.baseUrl");
        bodyScopes.put("loginUrl", loginUrl);
        bodyScopes.put("baseUrl", ConfigUtils.INSTANCE.getStringValue("url.base"));
        UserDetailsInfo newInfo = userManager.getUserDetailsForISLInfo(user.getId(), "ISL_2017");
        GeneratePasswordReq generatePasswordReq = new GeneratePasswordReq();
        generatePasswordReq.setEmail(user.getEmail());
        GeneratePasswordRes response = userManager.generatePassword(generatePasswordReq);
        String password = response.getNewPassword();
        bodyScopes.put("password", password);

//        String passwordResetLink = ConfigUtils.INSTANCE.getStringValue("web.baseUrl")
//                + ConfigUtils.INSTANCE.getStringValue("url.verify.token") + tokenCode;
//        bodyScopes.put("passwordResetLink", passwordResetLink);

        if (newInfo != null) {
            String category = getISLCategoryString(newInfo);
            bodyScopes.put("category", category);
        }
        HashMap<String, Object> subjectScopes = new HashMap<>();
        subjectScopes.put("name", user.getFirstName());
        ArrayList<InternetAddress> toList = new ArrayList<>();
        toList.add(new InternetAddress(user.getEmail(), user._getFullName()));
        EmailRequest request = new EmailRequest(toList, subjectScopes, bodyScopes, emailType, user.getRole());
        request.setIncludeHeaderFooter(Boolean.FALSE);
        sendEmail(request);
        if (StringUtils.isNotEmpty(user.getContactNumber())) {
            TextSMSRequest textSMSRequest = new TextSMSRequest(user.getContactNumber(),
                    user.getPhoneCode(), bodyScopes, emailType, Role.STUDENT);
            smsManager.sendSMS(textSMSRequest);
        }
    }


}
