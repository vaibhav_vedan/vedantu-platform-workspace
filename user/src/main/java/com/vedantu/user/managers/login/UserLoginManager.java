package com.vedantu.user.managers.login;

import com.google.gson.Gson;
import com.vedantu.User.LocationInfo;
import com.vedantu.User.Role;
import com.vedantu.User.VerificationLinkStatus;
import com.vedantu.User.request.SignInByTokenReq;
import com.vedantu.User.request.SignInReq;
import com.vedantu.User.response.ProcessVerifyContactNumberResponse;
import com.vedantu.async.AsyncTaskFactory;
import com.vedantu.async.AsyncTaskParams;
import com.vedantu.exception.*;
import com.vedantu.user.async.AsyncTaskName;
import com.vedantu.user.dao.*;
import com.vedantu.user.entity.User;
import com.vedantu.user.entity.UserLoginToken;
import com.vedantu.user.entity.VerificationTokenII;
import com.vedantu.user.entity.temp.ISPData;
import com.vedantu.user.entity.temp.LoginData;
import com.vedantu.user.enums.VerifiedType;
import com.vedantu.user.helper.BotFilter;
import com.vedantu.user.pojo.UserPreLoginDataPojo;
import com.vedantu.user.requests.OTPVerificationReq;
import com.vedantu.user.utils.PojoUtils;
import com.vedantu.user.utils.TestAccontPasswordFactory;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.IPUtil;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.security.HttpSessionData;
import com.vedantu.util.security.HttpSessionUtils;
import com.vedantu.util.security.LoginType;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;

@Service
public class UserLoginManager {

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(UserLoginManager.class);

    @Autowired
    private UserDAO userDAO;

    @Autowired
    public RedisDAO redisDAO;

    @Autowired
    public VerificationTokenDAO verificationTokenDAO;

    @Autowired
    public PasswordEncoder passwordEncoder;

    @Autowired
    public PojoUtils pojoUtils;

    @Autowired
    private HttpSessionUtils sessionUtils;

    @Autowired
    private UserLoginTokenDAO userLoginTokenDAO;

    @Autowired
    private UserLoginDataDao loginDataDao;

    @Autowired
    private BotFilter botFilter;

    @Autowired
    private AsyncTaskFactory asyncTaskFactory;

    @Autowired
    private IPUtil ipUtil;

    @Autowired
    private UserIspDataDao userIspDataDao;

    private static final int DUMMY_NUMBER_LENGTH = 8;
    private static Integer limit = Integer.parseInt(ConfigUtils.INSTANCE.properties.getProperty("otp.login.limit"));
    private final Gson gson = new Gson();

    public HttpSessionData authenticateUser(@RequestBody SignInReq signInReq, HttpServletRequest request,
                                            HttpServletResponse response) throws VException, IOException, URISyntaxException {
        signInReq.verify();
        User user = null;
        if (null != signInReq.getEmail()) {
            user = userDAO.getUserByEmail(signInReq.getEmail());
        } else if (null != signInReq.getPhone()) {
            user = userDAO.getUserByPhoneNumberAndCode(signInReq.getPhone(), signInReq.getPhoneCode());
        }
        if (null == user) {
            throw new ForbiddenException(ErrorCode.EMAIL_NOT_REGISTERED, "No user found for the specified email id.");
        }

        if (!passwordEncoder.matches(signInReq.getPassword(), user.getPassword())) {
            LoginData loginData = loginDataDao.getPasswordChangedTime(signInReq.getEmail());
            botFilter.blockIpAddress(signInReq.getIpAddress(), signInReq.getEmail());
            if(null != loginData){
                logger.warn("PASSWORD_DOES_NOT_MATCH_LOGIN_DATA : " + signInReq.getEmail());
                //throw new UnauthorizedException(ErrorCode.PASSWORD_DOES_NOT_MATCH_LOGIN_DATA, "Incorrect credentials.");
            }
            throw new UnauthorizedException(ErrorCode.PASSWORD_DOES_NOT_MATCH, "Incorrect credentials.");
        }
        Role signInReqUserRole = user.getRole();

        boolean exceptionEmailNotExist = true;
        String[] exceptionEmails = ConfigUtils.INSTANCE.getStringValue("google.sigin.in.exceptional.emails").split(",");
        if (signInReq.getEmail() != null) {
            for (String exEmail : exceptionEmails) {
                if (exEmail.equals(signInReq.getEmail())) {
                    exceptionEmailNotExist = false;
                    break;
                }
            }
        }

        if (signInReq.getEmail() != null && exceptionEmailNotExist) {
            if (Role.ADMIN.equals(signInReqUserRole) || Role.SEO_MANAGER.equals(signInReqUserRole) || Role.STUDENT_CARE.equals(signInReqUserRole)) {
                if (ConfigUtils.INSTANCE.getStringValue("environment").equals("PROD")) {
                    throw new BadRequestException(ErrorCode.ADMIN_USER_NON_SOCIAL_LOGIN, "Sign-in with Google's social API credentials");
                }
            }
        }

        if (Boolean.FALSE.equals(user.getProfileEnabled())) {
            throw new ForbiddenException(ErrorCode.USER_BLOCKED, "User is blocked.");
        }

        try {
            Map<String, Object> payload = new HashMap<>();
            payload.put("email", user.getEmail());
            payload.put("ipAddress", signInReq.getIpAddress());
            AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.SAVE_USER_LOGIN_DATA, payload);
            asyncTaskFactory.executeTask(params);
        }catch (Exception e){
            logger.error("Unable to update user login data");
        }

        com.vedantu.User.User userPojo = pojoUtils.convertToUserPojo(user);
        logger.info("authenticateUser - user - userAllowedToTakeOnboarding - {}", userPojo.isUserInProcessOfOnboarding());

        logger.info("authenticateUser - user - user data received for onboarding - {}",userPojo.isUserInProcessOfOnboarding());
        HttpSessionData sessionData = sessionUtils.setCookieAndHeaders(request, response, userPojo);
        sessionData.setPasswordAutogenerated(user.getPasswordAutogenerated());
        logger.info("authenticateUser - user - userAllowedToTakeOnboarding - {}",sessionData.isUserInProcessOfOnboarding());
        return sessionData;
    }

    public HttpSessionData authenticateUserByToken(SignInByTokenReq signInReq, HttpServletRequest request,
                                                    HttpServletResponse response) throws VException, IOException, URISyntaxException {
        signInReq.verify();
        UserLoginToken userLoginToken = userLoginTokenDAO.getById(signInReq.getToken());

        if (null == userLoginToken) {
            throw new ForbiddenException(ErrorCode.USER_LOGIN_TOKEN_INVALID, "Login token is not valid.");
        }
        Long diff = userLoginToken.getExpirationTime() - System.currentTimeMillis();
        if (diff <= 0) {
            throw new ForbiddenException(ErrorCode.USER_LOGIN_TOKEN_EXPIRED, "Login token is already expired.");
        }

        User user = userDAO.getUserByUserId(userLoginToken.getUserId());
        if (null == user) {
            throw new ForbiddenException(ErrorCode.EMAIL_NOT_REGISTERED, "No user found for the specified email id.");
        }

        if (Boolean.FALSE.equals(user.getProfileEnabled())) {
            throw new ForbiddenException(ErrorCode.USER_BLOCKED, "User is blocked.");
        }
        com.vedantu.User.User responseUser = pojoUtils.convertToUserPojo(user);
        responseUser.setUserId(userLoginToken.getUserId().toString());
        HttpSessionData sessionData = sessionUtils.setCookieAndHeaders(request, response, responseUser, LoginType.TOKEN_BASED);
        return sessionData;
    }

    public ProcessVerifyContactNumberResponse verifyOTP(OTPVerificationReq req, HttpServletRequest request, HttpServletResponse response) throws VException, IOException, URISyntaxException {
        if (null == req.getOtp()) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "OTP cannot be empty");
        }

        VerificationTokenII emailToken = null;
        VerificationTokenII mobileToken = null;

        String userEmail = "";
        String userPhone = "";
        String userPhoneCode = "";

        mobileToken = verificationTokenDAO.getPhoneLoginVerificationCode(req.getPhoneNumber(), req.getPhoneCode());
        emailToken = verificationTokenDAO.getEmailLoginVerificationCode(req.getEmail());

        if (null != req.getEmail() && StringUtils.isNotEmpty(req.getEmail())) {
            User u = userDAO.getUserByEmail(req.getEmail());
            if (null != u && null != u.getContactNumber() && u.getContactNumber().length() != DUMMY_NUMBER_LENGTH) {
                userPhone = u.getContactNumber();
                userPhoneCode = u.getPhoneCode();
                userEmail = u.getEmail();
                mobileToken = verificationTokenDAO.getPhoneLoginVerificationCode(u.getContactNumber(), u.getPhoneCode());
            }
        }

        if (StringUtils.isNotEmpty(req.getPhoneCode()) && StringUtils.isNotEmpty(req.getPhoneNumber())) {
            boolean isTestSignup = TestAccontPasswordFactory.isTestSignupAccount(req.getPhoneNumber());
            User u = null;
            if (!isTestSignup) {
                u = userDAO.getUserByPhoneNumberAndCode(req.getPhoneNumber(), req.getPhoneCode());
            }

            if (null != u && null != u.getEmail()) {
                userPhone = u.getContactNumber();
                userPhoneCode = u.getPhoneCode();
                userEmail = u.getEmail();
                emailToken = verificationTokenDAO.getEmailLoginVerificationCode(u.getEmail());
            }
        }

        if (emailToken == null && mobileToken == null) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "OTP expired");
        }

        // filter login
        loginRateFilter(userEmail, userPhone, userPhoneCode);

        boolean phone = false;
        boolean email = false;

        String emailTokenCode = (null != emailToken) ? emailToken.getCode() : null;
        String mobileTokenCode = (null != mobileToken) ? mobileToken.getCode() : null;

        String otp = req.getOtp();

        VerificationTokenII token;

        if (null != emailTokenCode && emailTokenCode.equals(otp)) {
            email = true;
            emailToken.setStatus(VerificationLinkStatus.USED);
            token = emailToken;
            verificationTokenDAO.create(emailToken, req.getCallingUserId());
        } else if (null != mobileTokenCode && mobileTokenCode.equals(otp)) {
            String testPhone = (null != req.getPhoneNumber()) ? req.getPhoneNumber() : userPhone;
            if (TestAccontPasswordFactory.isTestAccount(testPhone)) {
                if (sessionUtils.getCurrentSessionData() == null && null == req.getEmail() && !TestAccontPasswordFactory.canLoginThroughOtp(req.getPhoneNumber())) {
                    throw new ForbiddenException(ErrorCode.BAD_REQUEST_ERROR, "Invalid OTP");
                }
            }
            phone = true;
            token = mobileToken;
            mobileToken.setStatus(VerificationLinkStatus.USED);
            verificationTokenDAO.create(mobileToken, req.getCallingUserId());
        } else {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Incorrect OTP entered");
        }

        if (email) {
            return verifyOTPEmail(userEmail, req, request, response);
        } else if (phone) {
            return verifyOTPPhone(userPhone, userPhoneCode, req, request, response);
        } else {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Incorrect details are entered");
        }
    }

    private void loginRateFilter(String email, String contactNumber, String phoneCode) {
        if (StringUtils.isNotEmpty(email)) {
            String key = "OTP_LOGIN_" + email;
            try {
                String value = redisDAO.get(key);
                if (!StringUtils.isEmpty(value)) {
                    Integer count = gson.fromJson(value, Integer.class);
                    incrementRedisValue(key);
                    if (count > limit) {
                        // expire verification token
                        VerificationTokenII verificationTokenII = verificationTokenDAO.getEmailLoginVerificationCode(email);
                        verificationTokenII.setStatus(VerificationLinkStatus.EXPIRED);
                        verificationTokenDAO.create(verificationTokenII, null);
                        resetRedisValue(key);
                    }
                } else {
                    updateRedisValue(key);
                }
            } catch (Exception e) {
                logger.warn("Error in getting value from redis" + e.getMessage());
            }
        }

        if (StringUtils.isNotEmpty(contactNumber)) {
            String key = "OTP_LOGIN_" + contactNumber;
            try {
                String value = redisDAO.get(key);
                if (!StringUtils.isEmpty(value)) {
                    Integer count = gson.fromJson(value, Integer.class);
                    incrementRedisValue(key);
                    if (count > limit) {
                        // expire verification token
                        VerificationTokenII verificationTokenII = verificationTokenDAO.getPhoneLoginVerificationCode(contactNumber, phoneCode);
                        verificationTokenII.setStatus(VerificationLinkStatus.EXPIRED);
                        verificationTokenDAO.create(verificationTokenII, null);
                        resetRedisValue(key);
                    }
                } else {
                    updateRedisValue(key);
                }
            } catch (Exception e) {
                logger.warn("Error in getting value from redis" + e.getMessage());
            }
        }
    }

    private void resetRedisValue(String key) {
        try {
            redisDAO.del(key);
        } catch (Exception e) {
            logger.warn("Error in updating value to redis" + e.getMessage());
        }

    }

    private void updateRedisValue(String key) {
        try {
            // System.currentTimeMillis gives milli seconds from epoch
            // but we need unix time here for expireAt in redis,  which takes no of seconds from epoch
            // that's why milliSeconds /1000 + 300 (for 5 min)
            redisDAO.incrAndExpireAt(key, 300 + System.currentTimeMillis() / 1000);
        } catch (Exception e) {
            logger.warn("Error in updating value to redis" + e.getMessage());
        }
    }

    private void incrementRedisValue(String key) {
        try {
            redisDAO.incr(key);
        } catch (Exception e) {
            logger.warn("Error in incrementing value to redis" + e.getMessage());
        }
    }

    private ProcessVerifyContactNumberResponse verifyOTPEmail(String userEmail, OTPVerificationReq req, HttpServletRequest request, HttpServletResponse response) throws ForbiddenException, IOException, URISyntaxException, ConflictException {
        ProcessVerifyContactNumberResponse processVerifyContactNumberResponse = new ProcessVerifyContactNumberResponse();
        User user = null;
        if (StringUtils.isNotEmpty(userEmail)) {
            user = userDAO.getUserByEmail(userEmail);
        } else {
            user = userDAO.getUserByEmail(req.getEmail());
        }
        processVerifyContactNumberResponse.setVerified(true);
        if (null == user) {
            return processVerifyContactNumberResponse;
        } else {

            if (!user.getIsEmailVerified()) {
                user.setIsEmailVerified(true);
                userDAO.update(user, req.getCallingUserId());
            }
            if (Boolean.FALSE.equals(user.getProfileEnabled())) {
                throw new ForbiddenException(ErrorCode.USER_BLOCKED, "User is blocked.");
            }

            com.vedantu.User.User user1 = pojoUtils.convertToUserPojo(user);
            HttpSessionData sessionData = sessionUtils.setCookieAndHeaders(request, response, user1);
            sessionData.setPasswordAutogenerated(user.getPasswordAutogenerated());
            processVerifyContactNumberResponse.setUserDetails(sessionData);
            processVerifyContactNumberResponse.setUser(user1);
        }
        return processVerifyContactNumberResponse;
    }

    private ProcessVerifyContactNumberResponse verifyOTPPhone(String userPhone, String userPhoneCode, OTPVerificationReq req, HttpServletRequest request, HttpServletResponse response) throws IOException, URISyntaxException, VException {
        ProcessVerifyContactNumberResponse processVerifyContactNumberResponse = new ProcessVerifyContactNumberResponse();
        User user = null;

        if (null == req.getPhoneNumber() && userPhone.equals("")) {
            throw new ForbiddenException(ErrorCode.FORBIDDEN_ERROR, "Invalid OTP");
        }

        if (userPhone.equals("") && null == req.getEmail() && !TestAccontPasswordFactory.canLoginThroughOtp(req.getPhoneNumber())) {
            throw new ForbiddenException(ErrorCode.FORBIDDEN_ERROR, "Invalid OTP");
        }

        if (null != req.getEmail() && req.getEmail().endsWith("@vedantu.com")) {
            User user1 = userDAO.getUserByEmail(req.getEmail());
            if (null != user1 && null != user1.getRole() && !Role.TEACHER.equals(user1.getRole()) && !Role.STUDENT.equals(user1.getRole())) {
                throw new ForbiddenException(ErrorCode.FORBIDDEN_ERROR, "Login not allowed");
            }
        }

        String testPhoneNumber = (null != req.getPhoneNumber()) ? req.getPhoneNumber() : userPhone;
        if (!TestAccontPasswordFactory.isTestSignupAccount(testPhoneNumber)) {
            if (StringUtils.isNotEmpty(userPhone) && StringUtils.isNotEmpty(userPhoneCode)) {
                user = userDAO.getUserByPhoneNumberAndCode(userPhone, userPhoneCode);
            } else {
                user = userDAO.getUserByPhoneNumberAndCode(req.getPhoneNumber(), req.getPhoneCode());
            }
        }

        processVerifyContactNumberResponse.setVerified(true);
        if (null == user) {
            return processVerifyContactNumberResponse;
        } else {
            if (Boolean.FALSE.equals(user.getProfileEnabled())) {
                throw new ForbiddenException(ErrorCode.USER_BLOCKED, "User is blocked.");
            }

            if (!user.getIsContactNumberVerified()) {
                user.setIsContactNumberVerified(true);
                user.setContactNumberVerifiedBy(VerifiedType.OTP);
                userDAO.update(user, req.getCallingUserId());
            }

            com.vedantu.User.User user1 = pojoUtils.convertToUserPojo(user);
            HttpSessionData sessionData = sessionUtils.setCookieAndHeaders(request, response, user1);
            sessionData.setPasswordAutogenerated(user.getPasswordAutogenerated());

            processVerifyContactNumberResponse.setUserDetails(sessionData);
            processVerifyContactNumberResponse.setUser(user1);
            processVerifyContactNumberResponse.setNumber(req.getPhoneNumber());
            processVerifyContactNumberResponse.setNumber(req.getPhoneCode());
            return processVerifyContactNumberResponse;
        }
    }

    public void savePreLoginData(UserPreLoginDataPojo data) {
        ISPData ispData = new ISPData();
        if(null == data.getIpAddress()){
            ispData.setLoginParam(data.getLoginParam());
        }
        LocationInfo locationInfo = ipUtil.getLocationFromIp(data.getIpAddress());
        if(null != locationInfo){
            ispData.setCountry(locationInfo.getCountry());
            ispData.setIpAddress(data.getIpAddress());
            ispData.setLoginParam(data.getLoginParam());
            ispData.setIsp(locationInfo.getIsp());
        }
        userIspDataDao.save(ispData);
    }
}
