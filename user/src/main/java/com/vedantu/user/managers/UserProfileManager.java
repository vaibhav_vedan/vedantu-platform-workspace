package com.vedantu.user.managers;

import com.google.gson.Gson;
import com.vedantu.User.*;
import com.vedantu.User.request.*;
import com.vedantu.User.response.EditProfileRes;
import com.vedantu.User.response.EditUserProfileRes;
import com.vedantu.User.response.GetActiveTutorsRes;
import com.vedantu.User.response.UserDetailsInfo;
import com.vedantu.exception.*;
import com.vedantu.user.dao.RedisDAO;
import com.vedantu.user.dao.UserDAO;
import com.vedantu.user.dao.UserHistoryDAO;
import com.vedantu.user.entity.User;
import com.vedantu.user.entity.UserHistoryII;
import com.vedantu.user.requests.UserPreBookingVerificationInfoReq;
import com.vedantu.user.responses.UserPreBookingVerificationInfoRes;
import com.vedantu.user.utils.PojoUtils;
import com.vedantu.util.*;
import com.vedantu.util.security.HttpSessionUtils;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.Logger;
import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class UserProfileManager {

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(UserManager.class);

    @Autowired
    private DozerBeanMapper mapper;

    @Autowired
    public UserHistoryDAO userHistoryDAO;

    @Autowired
    public PojoUtils pojoUtils;

    @Autowired
    public HttpSessionUtils sessionUtils;

    @Autowired
    private UserDAO userDAO;

    public EditUserProfileRes editUserProfile(@Valid EditUserProfileReq req) throws NotFoundException, ForbiddenException, BadRequestException, ConflictException {
        req.verify();
        EditUserProfileRes res;
        User user;
        User requestingUser = userDAO.getUserByUserId(req.getCallingUserId());
        if (requestingUser == null) {
            throw new NotFoundException(ErrorCode.USER_NOT_FOUND,
                    "no user found with callingUserId:" + req.getCallingUserId(), Level.ERROR);
        }
        if (req.getUserId().longValue() == sessionUtils.getCallingUserId().longValue()) {
            user = requestingUser;
        } else {
                throw new ForbiddenException(ErrorCode.FORBIDDEN_ERROR,
                        "action not allowed for userId:" + req.getCallingUserId());
        }
        UserHistoryII userHistory = mapper.map(user, UserHistoryII.class);
        userHistory.setId(null);
        userHistory.setUserId(user.getId());
        userHistoryDAO.create(userHistory, req.getCallingUserId());
        logger.info("user email - {} ", user.getEmail());
        if(StringUtils.isNotEmpty(req.getEmail()) && StringUtils.isEmpty(user.getEmail())){

            if(CommonUtils.isValidEmailId(req.getEmail())) {
                logger.info("Updating user email");
                user.setEmail(req.getEmail());
            }
            else{
                throw new BadRequestException(ErrorCode.INVALID_EMAILID, "email id is not valid");
            }
        }
        if (StringUtils.isNotEmpty(req.getParentEmail())) {
            logger.info("Updating parent email");
            ParentInfo parentInfo = new ParentInfo();
            parentInfo.setEmail(req.getParentEmail());
            if (Objects.nonNull(user.getStudentInfo())) {
                if (CollectionUtils.isNotEmpty(user.getStudentInfo().getParentInfos())) {
                    List<ParentInfo> parentInfos = user.getStudentInfo().getParentInfos().
                            stream().
                            filter(Objects::nonNull).
                            filter(parInfo ->  req.getParentEmail().equalsIgnoreCase(parentInfo.getEmail())).
                            collect(Collectors.toList());
                    if(CollectionUtils.isEmpty(parentInfos)) {
                        logger.info("Updating parent email, when parentInfoList present without the requested email");
                        user.getStudentInfo().getParentInfos().add(parentInfo);
                    }
                } else {
                    logger.info("Updating parent email, when parentInfoList not present");
                    List<ParentInfo> parentInfoList = new ArrayList<>();
                    parentInfoList.add(parentInfo);
                    user.getStudentInfo().setParentInfos(parentInfoList);
                }
            } else {
                logger.info("Updating parent email, when studentInfo not present");
                StudentInfo studentInfo = new StudentInfo();
                List<ParentInfo> parentInfoList = new ArrayList<>();
                parentInfoList.add(parentInfo);
                studentInfo.setParentInfos(parentInfoList);
                user.setStudentInfo(studentInfo);
            }
        }
        if(Objects.nonNull(req.getDeviceType())) {
            if(Objects.nonNull(user.getStudentInfo())) {
                user.getStudentInfo().setDeviceType(req.getDeviceType());
            } else {
                StudentInfo studentInfo = new StudentInfo();
                studentInfo.setDeviceType(req.getDeviceType());
            }
        }
        if(StringUtils.isNotEmpty(req.getContactNumber()) && StringUtils.isNotEmpty(req.getPhoneCode()) && !user.getIsContactNumberVerified()) {
            user.setContactNumber(req.getContactNumber());
            user.setPhoneCode(req.getPhoneCode());
        }

        userDAO.update(user, req.getCallingUserId());

        res = new EditUserProfileRes(pojoUtils.convertToUserPojo(user));
        return res;
    }
}
