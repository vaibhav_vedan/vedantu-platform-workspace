package com.vedantu.user.managers.earlylearning;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vedantu.User.Pojo.ELProficiencyUserPojo;
import com.vedantu.User.Pojo.ELTeachersProficiencies;
import com.vedantu.User.Pojo.TeacherProficiency;
import com.vedantu.User.enums.TeacherCategory.ProficiencyType;
import com.vedantu.exception.VException;
import com.vedantu.subscription.enums.EarlyLearningCourseType;
import com.vedantu.user.dao.UserDAO;
import com.vedantu.util.LogFactory;

@Service("ELUserManager")
//TODO: Is this more of a Teacher Manager rather then UserManager?
public class UserManager {

	private Logger logger = LogFactory.getLogger(UserManager.class);

	@Autowired
	private UserDAO userDAO;

	public UserManager() {
	}

	public ELTeachersProficiencies getELTeachersProficiencies(EarlyLearningCourseType earlyLeaningCourseType)
			throws VException {

		ELTeachersProficiencies elTeachersProficiencies = new ELTeachersProficiencies();

		List<TeacherProficiency> proficiencyIds = userDAO.getTeacherProficiency(earlyLeaningCourseType);

		Map<ProficiencyType, List<String>> proficiencyToTeacherMap = proficiencyIds.stream()
				.collect(Collectors.groupingBy(TeacherProficiency::getProficiencyType,
						Collectors.mapping(TeacherProficiency::get_id, Collectors.toList())));

		// TODO : Check if getELTeachersProficiency requires any changes
		List<ELProficiencyUserPojo> proficiencyUserPojos = proficiencyToTeacherMap.entrySet().stream()
				.map(x -> new ELProficiencyUserPojo(x.getKey(), x.getValue())).collect(Collectors.toList());

		proficiencyUserPojos.forEach(pojo -> elTeachersProficiencies.setTeachersForProficiency(pojo));

		logger.info("elTeachersProficiencies : {}", elTeachersProficiencies);
		return elTeachersProficiencies;
	}

}
