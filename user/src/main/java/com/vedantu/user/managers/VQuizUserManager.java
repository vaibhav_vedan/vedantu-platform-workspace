package com.vedantu.user.managers;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.User.UserBasicInfo;
import com.vedantu.async.AsyncTaskFactory;
import com.vedantu.async.AsyncTaskParams;
import com.vedantu.exception.*;
import com.vedantu.notification.enums.CommunicationType;
import com.vedantu.notification.requests.EmailAttachment;
import com.vedantu.notification.requests.EmailRequest;
import com.vedantu.onetofew.pojo.OTFAttendance;
import com.vedantu.user.async.AsyncTaskName;
import com.vedantu.user.controllers.VQuizUserController;
import com.vedantu.user.dao.UserDAO;
import com.vedantu.user.entity.User;
import com.vedantu.util.*;
import com.vedantu.util.enums.SNSTopic;
import com.vedantu.util.security.HttpSessionData;
import com.vedantu.util.security.HttpSessionUtils;
import io.swagger.models.auth.In;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

import javax.mail.internet.InternetAddress;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalField;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author mnpk
 */

@Service
public class VQuizUserManager {

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(VQuizUserManager.class);

    @Autowired
    private HttpSessionUtils sessionUtils;

    @Autowired
    private AsyncTaskFactory asyncTaskFactory;

    @Autowired
    private UserDAO userDAO;

    @Autowired
    private FosUtils fosUtils;

    @Autowired
    private CommunicationManager communicationManager;


    public PlatformBasicResponse exportWinnersCsv(Map<String, String> requestParams) throws VException {
        logger.info("requestParams : " + requestParams);
        if (requestParams == null) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "request object not found!");
        }
        if (StringUtils.isEmpty(requestParams.get("quizId"))) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "quizId required");
        }

        //for checking the quiz by using quizId
        Map<String, Object> result = getVquizByUsingId(requestParams.get("quizId"));
        if (result.get("status") == null || result.get("prizeAmount") == null) {
            throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, "quiz does not have the status or prizeAmount");
        }
        String status = (String) result.get("status");
        if (!status.equals("ENDED")) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "quiz status not in ENDED state");
        }
        Double prizeAmount = (Double) result.get("prizeAmount");
        if (prizeAmount == null) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "quiz prizeAmount not found");
        }
        int amount = (int) (prizeAmount * 1);
        requestParams.put("prizeAmount", String.valueOf(amount));

        HttpSessionData httpSessionData = sessionUtils.getCurrentSessionData();
        requestParams.put("userId", Long.toString(httpSessionData.getUserId()));
        logger.info("requestParams : " + requestParams);

        //checking for result existed or not
        Map<String, Object> respObj = getWinnersList(requestParams.get("quizId"), 0, 10);
        logger.info("respObj : " + respObj);
        List<Object> result1 = (List<Object>) respObj.get("result");
        if (ArrayUtils.isEmpty(result1)) {
            throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, "quizId result not found for id " + requestParams.get("quizId"));
        }
        logger.info("result : " + result1);

        Map<String, Object> payload = new HashMap<>();
        payload.put("requestParams", requestParams);
        AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.EXPORT_VQUIZ_WINNERS, payload);
        asyncTaskFactory.executeTask(params);

        return new PlatformBasicResponse();
    }


    public void exportWinnersCsvFromAsync(Map<String, String> requestParams) throws VException, UnsupportedEncodingException {
        logger.info("requestParams : " + requestParams);
        if (StringUtils.isEmpty(requestParams.get("quizId"))) {
            logger.error("quizId not found for exportWinners");
            return;
        }
        logger.info("totalAmount : " + requestParams.get("prizeAmount"));

        List<String> headers = Arrays.asList("userId", "name", "contactNumber", "email", "quizTotalAmount", "prizeAmount", "quizDate");
        List<String> exportEntries = new ArrayList<>();

        Integer skip = 0, limit = 100, totalWinnersCount = 0;
        while (true) {
            Map<String, Object> respObj = getWinnersList(requestParams.get("quizId"), skip, limit);
            logger.info("respObj : " + respObj);

            List<Object> result = (List<Object>) respObj.get("result");
            logger.info("result  ( List ) : " + result);

            List<Long> userIds = getUserIdsFromRespObj(result);
            logger.info("userIds : " + userIds);

            List<User> users = userDAO.getUserByUsingUserIdsForVquizWinnersExport(userIds);
            logger.info("users : " + users);

            List<String> resultEntriesList = resultEntriesList(result, users, requestParams);
            logger.info("resultEntriesList : " + resultEntriesList);
            exportEntries.addAll(resultEntriesList);

            //while loop breaking...
            if (ArrayUtils.isEmpty(result) || result.size() != limit) {
                logger.info("########### while loop break ###########");
                break;
            }

            skip = skip + limit;
        }
        sendWinnersCsvEmail(requestParams, headers, exportEntries);
    }

    private Map<String, Object> getVquizByUsingId(String quizId) throws VException {
        logger.info("quizId : " + quizId);
        //example url :: https://vquiz-dev.vedantu.com/quiz/winners/get-list?quizId=<quizId>
        String VQUIZ_ENDPOINT = ConfigUtils.INSTANCE.getStringValue("VQUIZ_ENDPOINT");
        String url = VQUIZ_ENDPOINT + "quiz/" + quizId;
        ClientResponse resp = WebUtils.INSTANCE.doCall(url, HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        logger.info("response of get quiz By id : " + jsonString);
        Map<String, Object> obj = new Gson().fromJson(jsonString, new TypeToken<Map<String, Object>>() {
        }.getType());
        if (obj == null || obj.get("result") == null) {
            throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, "quizId result not found for id " + quizId);
        }
        logger.info("obj : " + obj);
        Map<String, Object> result = (Map<String, Object>) obj.get("result");
        if (result == null) {
            throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, "quizId result not found for id " + quizId);
        }
        logger.info("result : " + result);
        return result;
    }

    private Map<String, Object> getWinnersList(String quizId, Integer skip, Integer limit) throws VException {
        logger.info("quizId : " + quizId + " skip : " + skip + " limit : " + limit);
        //        https://vquiz-dev.vedantu.com/quiz/winners/get-list?quizId=<quizId>&skip=<num>&limit=<num>
        String VQUIZ_ENDPOINT = ConfigUtils.INSTANCE.getStringValue("VQUIZ_ENDPOINT");
        String url = "quiz/winners/get-list?quizId=" + quizId + "&skip=" + skip + "&limit=" + limit;
        ClientResponse resp = WebUtils.INSTANCE.doCall(VQUIZ_ENDPOINT + url, HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        logger.info("response of vquiz winners list : " + jsonString);
        Map<String, Object> respObj = new Gson().fromJson(jsonString, new TypeToken<Map<String, Object>>() {
        }.getType());
        logger.info("obj : " + respObj);
        if (respObj == null || respObj.get("result") == null) {
            logger.error("quizId result not found for id : " + quizId);
        }
        return respObj;
    }

    private List<Long> getUserIdsFromRespObj(List<Object> req) {
        logger.info("req : " + req);
        List<Long> userIds = new ArrayList<>();
        if (ArrayUtils.isNotEmpty(req)) {
            for (int i = 0; i < req.size(); i++) {
                logger.info(i + " : " + req.get(i));
                Map<String, Object> eachObj = (Map<String, Object>) req.get(i);
                if (eachObj != null && eachObj.get("user") != null) {
                    Double userIdDouble = (Double) eachObj.get("user");
                    long userId = (long) (userIdDouble * 1);
                    logger.info("userId : " + userId);
                    userIds.add(userId);
                }
            }
        }
        logger.info("userIds : " + userIds);
        return userIds;
    }


    private List<String> resultEntriesList(List<Object> result, List<User> users, Map<String, String> requestParams) {
        Map<Long, User> userMap = getUserIdUserMap(users);
        List<String> exportEntries = new ArrayList<>();
        if (result != null) {
            for (int i = 0; i < result.size(); i++) {
                List<String> resultEntries = new ArrayList<>();
                logger.info(i + " : " + result.get(i));
                Map<String, Object> eachObj = (Map<String, Object>) result.get(i);
                if (eachObj != null) {
                    long userId = 0;
                    if (eachObj.get("user") != null) {
                        Double userIdDouble = (Double) eachObj.get("user");
                        userId = (long) (userIdDouble * 1);
                        logger.info("userId : " + userId);
                        resultEntries.add(escapeCommas(Long.toString(userId)));
                    } else {
                        resultEntries.add("");
                    }

                    if (userId != 0 && userMap != null && userMap.get(userId) != null) {
                        logger.info("userMap.get(userId) : " + userMap.get(userId));
                        resultEntries.add((StringUtils.isNotEmpty(userMap.get(userId).getFullName())) ? (escapeCommas(userMap.get(userId).getFullName())) : "");
                        resultEntries.add((StringUtils.isNotEmpty(userMap.get(userId).getContactNumber())) ? (escapeCommas(userMap.get(userId).getContactNumber())) : "");
                        resultEntries.add((StringUtils.isNotEmpty(userMap.get(userId).getEmail())) ? (escapeCommas(userMap.get(userId).getEmail())) : "");
                    } else {
                        resultEntries.add("");
                        resultEntries.add("");
                        resultEntries.add("");
                    }
                    resultEntries.add((requestParams != null && StringUtils.isNotEmpty(requestParams.get("prizeAmount"))) ? escapeCommas(requestParams.get("prizeAmount")) : "");

                    if (eachObj.get("prizeAmount") != null) {
                        Double prizeAmount = (Double) eachObj.get("prizeAmount");
                        int amount = (int) (prizeAmount * 1);
//                        resultEntries.add(String.valueOf(amount));
                        resultEntries.add(Double.toString(prizeAmount));
                    } else {
                        resultEntries.add("");
                    }

                    if (eachObj.get("createdAt") != null) {
                        String createdAt = (String) eachObj.get("createdAt");
                        logger.info("createdAt : " + createdAt);
                        DateTimeFormatter f = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSSz");
                        LocalDateTime localDateTime = LocalDateTime.parse(createdAt, f);
                        long epoch = localDateTime.atZone(ZoneId.systemDefault()).toInstant().getEpochSecond();
                        logger.info("epoch : " + epoch);
                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss a");
                        sdf.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));
                        String dateTime = sdf.format(new Date(epoch * 1000));
                        logger.info("dateTime : " + dateTime);
                        resultEntries.add(dateTime);
                    } else {
                        resultEntries.add("");
                    }

                    exportEntries.add(String.join(",", resultEntries));
                } else {
                    logger.error("winner object not found");
                }
            }
        }
        logger.info("exportEntries : " + exportEntries);
        return exportEntries;
    }


    private Map<Long, User> getUserIdUserMap(List<User> users) {
        Map<Long, User> result = new HashMap<>();
        if (ArrayUtils.isNotEmpty(users)) {
            result = users.stream().filter(Objects::nonNull)
                    .collect(Collectors.toMap(User::getId, b -> b));
        }
        logger.info("userId UserMap : " + result);
        return result;
    }

    private String escapeCommas(String input) {
        if (StringUtils.isNotEmpty(input)) {
            input = input.replace(",", "-");
            input = input.replace("\n", " ");
            return input;
        } else {
            return "";
        }
    }

    private void sendWinnersCsvEmail(Map<String, String> requestParams, List<String> headers, List<String> exportEntries) throws UnsupportedEncodingException, VException {
        logger.info("requestParams : " + requestParams + " headers : " + headers + " exportEntries : " + exportEntries);
        if (requestParams != null && ArrayUtils.isNotEmpty(headers) && ArrayUtils.isNotEmpty(exportEntries)) {
            UserBasicInfo callingUser = fosUtils.getUserBasicInfo(requestParams.get("userId"), true);
            if (callingUser == null || StringUtils.isEmpty(callingUser.getEmail())) {
                logger.warn("admin email not found");
                return;
            }
            EmailRequest email = new EmailRequest();
            String subject = "VQuiz Result - " + requestParams.get("quizId");
            email.setSubject(subject);
            String body = "Please find the attached vquiz winners file!";
            email.setBody(body);
            ArrayList<InternetAddress> toList = new ArrayList<>();
            toList.add(new InternetAddress(callingUser.getEmail(), "Admin"));
            email.setTo(toList);
            email.setType(CommunicationType.VQUIZ_WINNERS_EXPORT_EMAIL);
            EmailAttachment attachment = new EmailAttachment();
            attachment.setApplication("application/csv");
            String exportData = String.join(",", headers) + "\n" + String.join("\n", exportEntries);
            attachment.setAttachmentData(exportData);
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yy HH:mm:ss a");
            sdf.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));
            String dateTime = sdf.format(System.currentTimeMillis());
            attachment.setFileName("VQuiz_" + dateTime + ".csv");
            email.setAttachment(attachment);
            logger.info("email : " + email);
            communicationManager.sendEmailViaRest(email);
        }
    }

}
