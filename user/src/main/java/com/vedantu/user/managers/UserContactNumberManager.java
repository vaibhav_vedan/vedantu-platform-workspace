package com.vedantu.user.managers;

import com.google.gson.Gson;
import com.vedantu.User.Role;
import com.vedantu.User.UserInfo;
import com.vedantu.User.request.AddOrUpdatePhoneNumberReq;
import com.vedantu.User.request.EditContactNumberReq;
import com.vedantu.User.request.ReSendContactNumberVerificationCodeReq;
import com.vedantu.User.request.SetBlockStatusForUserReq;
import com.vedantu.User.response.EditContactNumberRes;
import com.vedantu.User.response.GetUserPhoneNumbersRes;
import com.vedantu.User.response.PhoneNumberRes;
import com.vedantu.User.response.ReSendContactNumberVerificationCodeRes;
import com.vedantu.exception.VException;
import com.vedantu.notification.enums.CommunicationType;
import com.vedantu.notification.enums.NotificationType;
import com.vedantu.notification.requests.CreateNotificationRequest;
import com.vedantu.notification.requests.MarkSeenReq;
import com.vedantu.util.LogFactory;
import com.vedantu.util.PlatformBasicResponse;
import com.vedantu.util.StringUtils;
import com.vedantu.util.security.HttpSessionData;
import com.vedantu.util.security.HttpSessionUtils;
import com.vedantu.util.security.UserRedisManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;

/**
 * @author MNPK
 */

@Service
public class UserContactNumberManager {

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(UserManager.class);

    @Autowired
    private UserManager userManager;

    @Autowired
    private HttpSessionUtils sessionUtils;

    @Autowired
    private SMSManager smsManager;

    @Autowired
    private UserRedisManager userRedisManager;

    @Autowired
    private CommunicationManager communicationManager;

    @Autowired
    private NotificationManager notificationManager;


    public EditContactNumberRes editContactNumber(EditContactNumberReq request, HttpServletRequest httpRequest, HttpServletResponse httpResponse) throws VException, IOException, URISyntaxException {
        request.verify();
        if (request.getUserId() == null) {
            request.setUserId(sessionUtils.getCallingUserId());
        }
        EditContactNumberRes editContactNumberRes = userManager.editContactNumber(request);
        HttpSessionData sessionData = sessionUtils.getCurrentSessionData();
        sessionData.setIsContactNumberDND(editContactNumberRes.isDND());
        // sessionData.setIsContactNumberWhitelisted(response.isWhitelisted());
        sessionData.setIsContactNumberVerified(editContactNumberRes.isIsVerified());
        sessionData.setContactNumber(editContactNumberRes.getContactNumber());
        sessionData.setPhoneCode(editContactNumberRes.getPhoneCode());
        sessionUtils._setCookieAndHeaders(httpRequest, httpResponse, sessionData);

        if (!StringUtils.isEmpty(editContactNumberRes.getMobileTokenCode())) {
            logger.info("EDIT-CONTACT-NUMBER:SEND-SMS");
            logger.info(editContactNumberRes);
            logger.info(editContactNumberRes.toString());
            sendContactNumberVerificationSMS(editContactNumberRes.getUserInfo(), editContactNumberRes.getContactNumber(), editContactNumberRes.getPhoneCode(), editContactNumberRes.getMobileTokenCode(), editContactNumberRes.getUtm_campaign());
        }
        // Must do it
        editContactNumberRes.setUtm_campaign(null);
        editContactNumberRes.setMobileTokenCode(null);
        editContactNumberRes.setUserInfo(null);
        return editContactNumberRes;
    }

    public ReSendContactNumberVerificationCodeRes reSendContactNumberVerificationCode(ReSendContactNumberVerificationCodeReq request) throws VException {
        request.verify();
        ReSendContactNumberVerificationCodeRes reSendContactNumberVerificationCodeRes = userManager.reSendContactNumberVerificationCode(request);
        UserInfo userInfo = reSendContactNumberVerificationCodeRes.getUserInfo();
        boolean isPrimaryNumber = StringUtils.isEmpty(request.getNumber());
        String number = isPrimaryNumber ? userInfo.getTempContactNumber() : request.getNumber();
        String phoneCode = isPrimaryNumber ? userInfo.getTempPhoneCode() : request.getPhoneCode();
        logger.info("RESPONSE-MOBILETOKENCODE: " + reSendContactNumberVerificationCodeRes.getMobileTokenCode());
        logger.info("Number: " + number);
        if (!StringUtils.isEmpty(reSendContactNumberVerificationCodeRes.getMobileTokenCode()) && StringUtils.isNotEmpty(number)) {
            sendContactNumberVerificationSMS(userInfo, number, phoneCode, reSendContactNumberVerificationCodeRes.getMobileTokenCode(), reSendContactNumberVerificationCodeRes.getUtm_campaign());
        }
        // Must do it
        reSendContactNumberVerificationCodeRes.setUtm_campaign(null);
        reSendContactNumberVerificationCodeRes.setMobileTokenCode(null);
        reSendContactNumberVerificationCodeRes.setUserInfo(null);
        return reSendContactNumberVerificationCodeRes;
    }

    public PhoneNumberRes updatePhoneNumber(AddOrUpdatePhoneNumberReq req) throws VException {
        req.verify();
        PhoneNumberRes phoneNumberRes = userManager.updatePhoneNumber(req);
        if (!StringUtils.isEmpty(phoneNumberRes.getMobileTokenCode())) {
            sendContactNumberVerificationSMS(phoneNumberRes.getUserInfo(), phoneNumberRes.getNumber(), phoneNumberRes.getPhoneCode(), phoneNumberRes.getMobileTokenCode(), phoneNumberRes.getUtm_campaign());
        }
        // Must do it
        phoneNumberRes.setUtm_campaign(null);
        phoneNumberRes.setMobileTokenCode(null);
        phoneNumberRes.setUserInfo(null);
        return phoneNumberRes;
    }

    public PhoneNumberRes updateAndMarkActivePhoneNumber(AddOrUpdatePhoneNumberReq req) throws VException {
        req.verify();
        PhoneNumberRes phoneNumberRes = userManager.updateAndMarkActivePhoneNumber(req);
//        if (!StringUtils.isEmpty(phoneNumberRes.getMobileTokenCode())) {
//            sendContactNumberVerificationSMS(phoneNumberRes.getUserInfo(), phoneNumberRes.getNumber(), phoneNumberRes.getPhoneCode(), phoneNumberRes.getMobileTokenCode(),null);
//        }
        phoneNumberRes.setMobileTokenCode(null);
        phoneNumberRes.setUserInfo(null);
        return phoneNumberRes;

    }

    public String getUserPhoneNumbers(Long userId) throws VException {
        GetUserPhoneNumbersRes getUserPhoneNumbersRes = userManager.getUserPhoneNumbers(userId);
        return new Gson().toJson(getUserPhoneNumbersRes);
    }

    public PlatformBasicResponse markBlockStatus(SetBlockStatusForUserReq setBlockStatusForUserReq) throws VException, UnsupportedEncodingException {
        setBlockStatusForUserReq.verify();
        Boolean profileEnabled = setBlockStatusForUserReq.isProfileEnabled();
        if (!profileEnabled) {
            CreateNotificationRequest request = new CreateNotificationRequest(setBlockStatusForUserReq.getUserId(), NotificationType.BLOCKED, null, Role.STUDENT);
            notificationManager.createNotification(request);
        } else {
            MarkSeenReq markSeenReq = new MarkSeenReq();
            markSeenReq.setUserId(setBlockStatusForUserReq.getUserId());
            markSeenReq.setCallingUserId(setBlockStatusForUserReq.getCallingUserId());
            markSeenReq.setNotificationType(NotificationType.BLOCKED);
            notificationManager.markSeenByType(markSeenReq);
        }
        PlatformBasicResponse platformBasicResponse = userManager.markBlockStatus(setBlockStatusForUserReq);
        userRedisManager.addUserStatus(setBlockStatusForUserReq.getUserId(), setBlockStatusForUserReq.isProfileEnabled());
        if (!profileEnabled) {
            String reason = setBlockStatusForUserReq.getReason();
            // emailManager.sendBlockUserToAdmin(setBlockStatusForUserReq.getCallingUserId(),
            // setBlockStatusForUserReq.getUserId(), reason);
            Map<String, Object> payload = new HashMap<String, Object>();
            payload.put("callingUserId", setBlockStatusForUserReq.getCallingUserId());
            payload.put("userId", setBlockStatusForUserReq.getUserId());
            payload.put("reason", reason);
            payload.put("callerUser", sessionUtils.getCurrentSessionData());
            communicationManager.sendBlockUserToAdmin(setBlockStatusForUserReq.getCallingUserId(), setBlockStatusForUserReq.getUserId(), reason, sessionUtils.getCurrentSessionData());
        }
        return platformBasicResponse;
    }

    public void sendContactNumberVerificationSMS(UserInfo userInfo, String number, String phoneCode, String mobileTokenCode, String utm_campaign) throws VException {
        smsManager.sendSMS(CommunicationType.PHONE_VERIFICATION, userInfo, number, phoneCode, mobileTokenCode, utm_campaign);
    }


}
