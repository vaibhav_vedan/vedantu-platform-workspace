package com.vedantu.user.managers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.amazonaws.services.sns.AmazonSNSAsync;
import com.amazonaws.services.sns.AmazonSNSAsyncClientBuilder;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;

import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.sns.model.PublishRequest;
import com.amazonaws.services.sns.model.PublishResult;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.User.Role;
import com.vedantu.User.UserEvents;
import com.vedantu.User.enums.UserLeadsActivityType;
import com.vedantu.async.AsyncTaskFactory;
import com.vedantu.async.AsyncTaskParams;
import com.vedantu.User.response.UserLeadInfo;

import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.NotFoundException;
import com.vedantu.exception.VException;
import com.vedantu.exception.VExceptionFactory;
import com.vedantu.lms.cmds.pojo.KeyValueObject;
import com.vedantu.user.async.AsyncTaskName;
import com.vedantu.user.dao.UserDAO;
import com.vedantu.user.dao.UserLeadActivityDAO;
import com.vedantu.user.dao.UserLeadDAO;
import com.vedantu.user.entity.UserLead;
import com.vedantu.user.entity.UserLeadActivity;
import com.vedantu.user.requests.AddEditUserLeadReq;
import com.vedantu.user.requests.GetUserLeadReq;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.WebUtils;
import com.vedantu.util.fos.request.ReqLimits;
import java.lang.reflect.Type;
import javax.annotation.PreDestroy;
import org.dozer.DozerBeanMapper;
import org.springframework.http.HttpMethod;

@Service
public class UserLeadManager {

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(UserLeadManager.class);

    @Autowired
    public UserDAO userDAO;

    @Autowired
    public UserLeadDAO userLeadsDAO;

    @Autowired
    private AsyncTaskFactory asyncTaskFactory;

    @Autowired
    public UserLeadActivityDAO userLeadsActivityDAO;

    @Autowired
    private DozerBeanMapper mapper;

    public static String arn = ConfigUtils.INSTANCE.getStringValue("aws.arn");
    public static String env = ConfigUtils.INSTANCE.getStringValue("environment");
    private static final String IP_LOCATION_URL_BUILDER = ConfigUtils.INSTANCE.getStringValue("ip_location.apigurus.api");

    private final AmazonSNSAsync snsClient;
    
    public UserLeadManager() {
        snsClient = AmazonSNSAsyncClientBuilder.standard()
                .withRegion(Regions.AP_SOUTHEAST_1)
                .build();
    }

    
    private String getMaskedEmail(String email) {
        return email.replaceAll("(^[^@]{3}|(?!^)\\G)[^@]", "$1*");
    }
    public UserLeadInfo addEditUserLeads(AddEditUserLeadReq req) throws BadRequestException, IllegalArgumentException, IllegalAccessException {

        logger.info("userLeads :req" + req);
        if(StringUtils.isEmpty(req.getEmail()) || StringUtils.isEmpty(req.getContactNumber()) || StringUtils.isEmpty(req.getSource())){
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "missing email||contactno||source");
        }
        req.setEmail(req.getEmail().toLowerCase());
        UserLead oldLead = userLeadsDAO.getUserLeadByPhone(req.getContactNumber(), req.getSource());
        if(oldLead != null){
           if(!oldLead.getEmail().equalsIgnoreCase(req.getEmail())){
                UserLeadInfo info = mapper.map(oldLead, UserLeadInfo.class);
                info.setEmail(getMaskedEmail(info.getEmail()));
                return info;
            }
        }
        oldLead = userLeadsDAO.getUserLeadByEmail(req.getEmail(), req.getSource());
        boolean sendSMS =true;
        UserLead userLead;
        if(oldLead == null){
           userLead = mapper.map(req, UserLead.class);
        } else {
            userLead=oldLead;
            if(StringUtils.isNotEmpty(userLead.getContactNumber())&&userLead.getContactNumber().equalsIgnoreCase(req.getContactNumber())){
                sendSMS = false;
            }
            userLead.setPhoneCode(req.getPhoneCode());
            userLead.setContactNumber(req.getContactNumber());
            userLead.setFirstName(req.getFirstName());
            userLead.setLastName(req.getLastName());
            userLead.setFullName(req.getFullName());
            userLead.setGender(req.getGender());
            userLead.setGrade(req.getGrade());
        }
        userLead.setIpAddress(req.getIpAddress());
        userLeadsDAO.save(userLead, null);
        UserLeadInfo info = mapper.map(userLead, UserLeadInfo.class);
        info.setUserLeadsId(userLead.getId());
        Map<String, Object> payload = new HashMap<>();
        payload.put("userLead", userLead);
        payload.put("sendSMS", sendSMS);
        AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.USER_LEAD_FILL_LOCATION, payload);
        asyncTaskFactory.executeTask(params);
        return info;
//        if (userLead.getActivity() == null && ArrayUtils.isEmpty(userLead.getLeadMetadata()) && !Boolean.TRUE.equals(req.getSkipMessage())) {
//            Map<String, Object> payload = new HashMap<>();
//            payload.put("userLead", userLead);
//            AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.USER_LEAD_EVENT, payload);
//            asyncTaskFactory.executeTask(params);
//        }

//		Update update = new Update();
//		logger.info("lead query "+query);
//		query.addCriteria(criteria);
//		query.with(new Sort(Direction.ASC, UserLead.Constants.CREATION_TIME));
//		List<UserLead> userleads = userLeadsDAO.runQuery(query, UserLead.class);
//		if(ArrayUtils.isEmpty(userleads)){
//			UserLead userLead = mapper.map(req, UserLead.class);
//			userLeadsDAO.save(userLead, null);
//			logger.info("update new  "+userLead);
//			return;
//		}else {
//			UserLead userLead = userleads.get(0);
//			
//			if(StringUtils.isNotEmpty(req.getEmail())){
//				userLead.setEmail(req.getEmail());
//			}
//			if(StringUtils.isNotEmpty(req.getContactNumber())){
//				userLead.setContactNumber(req.getContactNumber());
//			}
//			if(StringUtils.isNotEmpty(req.getPhoneCode())){
//				userLead.setPhoneCode(req.getPhoneCode());
//			}
//			if(StringUtils.isNotEmpty(req.getCity())){
//				userLead.setCity(req.getCity());
//			}
//			if(StringUtils.isNotEmpty(req.getFirstName())){
//				userLead.setFirstName(req.getFirstName());
//				
//			}
//			if(StringUtils.isNotEmpty(req.getLastName())){
//				userLead.setLastName(req.getLastName());
//				
//			}
//			if(StringUtils.isNotEmpty(req.getFullName())){
//				userLead.setFullName(req.getFullName());
//			}
//			if(StringUtils.isNotEmpty(req.getGrade())){
//				userLead.setGrade(req.getGrade());
//				
//			}
//			if(StringUtils.isNotEmpty(req.getCity())){
//				userLead.setCity(req.getCity());
//				
//			}
//			if(StringUtils.isNotEmpty(req.getCountry())){
//				userLead.setCountry(req.getCountry());
//				
//			}
//			if(StringUtils.isNotEmpty(req.getSchool())){
//				userLead.setSchool(req.getSchool());
//				
//			}
//			if(StringUtils.isNotEmpty(req.getBoard())){
//				userLead.setBoard(req.getBoard());
//				
//			}
//			if(req.getGender() != null){
//				userLead.setGender(req.getGender());
//			}
//			if(req.getRole() != null){
//				userLead.setRole(req.getRole());
//			}
//			if(req.getSource() != null){
//				userLead.setSource(req.getSource());
//			}
//			userLeadsDAO.save(userLead, null);
//			logger.info("old update"+userLead);
//		}
    }

    public void addEditUserLeads(String jsonString) throws BadRequestException {
        if (StringUtils.isEmpty(jsonString)) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "No data");
        }
        JSONObject jsonObject = new JSONObject(jsonString);
        addEditUserLeads(jsonObject);
    }

    public void addEditUserLeads(JSONObject jsonObject) throws BadRequestException {

        Query query = new Query();
        Update update = new Update();

        UserLead userLead = new UserLead();
        String email = "";
        String contactNumber = "";
        UserLead lead = new UserLead();
        Criteria criteria = Criteria.where(UserLead.Constants._ID).is("null");
        Criteria criteriaNumber = null;
        Criteria criteriaEmail = null;

        if (jsonObject.has("email")) {
            userLead.setEmail(jsonObject.getJSONArray("email").getString(0).toLowerCase());
        }
        if (jsonObject.has("contactNumber")) {
            userLead.setContactNumber(jsonObject.getJSONArray("contactNumber").getString(0));
        }
        if (jsonObject.has("phoneCode")) {
            userLead.setPhoneCode(jsonObject.getJSONArray("phoneCode").getString(0));
        }
        if (jsonObject.has("fullName")) {
            userLead.setFullName(jsonObject.getJSONArray("fullName").getString(0));
        }
        if (jsonObject.has("firstName")) {
            userLead.setFirstName(jsonObject.getJSONArray("firstName").getString(0));
        }
        if (jsonObject.has("lastName")) {
            userLead.setLastName(jsonObject.getJSONArray("lastName").getString(0));
        }
        if (jsonObject.has("grade")) {
            userLead.setGrade(jsonObject.getJSONArray("grade").getString(0));
        }
        if (jsonObject.has("utm_source")) {
            userLead.setUtm_source(jsonObject.getJSONArray("utm_source").getString(0));
        }
        if (jsonObject.has("utm_medium")) {
            userLead.setUtm_medium(jsonObject.getJSONArray("utm_medium").getString(0));
        }
        if (jsonObject.has("utm_campaign")) {
            userLead.setUtm_campaign(jsonObject.getJSONArray("utm_campaign").getString(0));
        }
        if (jsonObject.has("utm_term")) {
            userLead.setUtm_term(jsonObject.getJSONArray("utm_term").getString(0));
        }
        if (jsonObject.has("utm_content")) {
            userLead.setUtm_content(jsonObject.getJSONArray("utm_content").getString(0));
        }
        if (jsonObject.has("channel")) {
            userLead.setChannel(jsonObject.getJSONArray("channel").getString(0));
        }
        if (jsonObject.has("ipAddress")) {
            userLead.setIpAddress(jsonObject.getJSONArray("ipAddress").getString(0));
        }
        if (jsonObject.has("school")) {
            userLead.setSchool(jsonObject.getJSONArray("school").getString(0));
        }
        if (jsonObject.has("city")) {
            userLead.setCity(jsonObject.getJSONArray("city").getString(0));
        }
        if (jsonObject.has("country")) {
            userLead.setCountry(jsonObject.getJSONArray("country").getString(0));
        }
        if (jsonObject.has("board")) {
            userLead.setBoard(jsonObject.getJSONArray("board").getString(0));
        }
        if (jsonObject.has("role")) {
            try {
                Role role = Role.valueOf(jsonObject.getJSONArray("city").getString(0).toUpperCase());
                userLead.setRole(role);

            } catch (Exception e) {
                logger.error("incorrect Role" + " " + e);
            }
        }
        if (jsonObject.has("source")) {
        	userLead.setSource(jsonObject.getJSONArray("source").getString(0));
        }
        if (jsonObject.has("activity")) {
            String activity = jsonObject.getJSONArray("activity").getString(0);
            if (StringUtils.isNotEmpty(activity)) {
                UserLeadsActivityType type = UserLeadsActivityType.valueOf(activity.toUpperCase());
                userLead.setActivity(type);
            } else {
                logger.info("No activity" + activity);
            }
        }

        if (jsonObject.has("contextType")) {
            userLead.setContextType(jsonObject.getJSONArray("contextType").getString(0));
        }
        if (jsonObject.has("contextId")) {
            userLead.setContextId(jsonObject.getJSONArray("contextId").getString(0));
        }
        if (jsonObject.has("joined")) {
            String joinString = (jsonObject.getJSONArray("joined").getString(0));
            if ("TRUE".equalsIgnoreCase(joinString)) {
                userLead.setJoined(true);
            } else if ("FALSE".equalsIgnoreCase(joinString)) {
                userLead.setJoined(false);
            }
        }
        List<KeyValueObject> objects = new ArrayList<>();
        int count = 0;
        for (String key : jsonObject.keySet()) {
            if (key.contains("newKey")) {
                String value = jsonObject.getJSONArray("key").getString(0);
                String[] keyValues = key.split("#");
                if (keyValues.length > 1 && StringUtils.isNotEmpty(value)) {
                    if (value.length() > ReqLimits.NAME_TYPE_MAX) {
                        throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Too long string" + value.length() + " " + value);
                    }
                    String keyValue = keyValues[1];
                    KeyValueObject obj = new KeyValueObject();
                    obj.setKey(keyValue);
                    obj.setValue(value);
                    objects.add(obj);
                    count++;
                }
                if (count > 50) {
                    throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Too many new Keys");
                }
            }
        }
        if (ArrayUtils.isNotEmpty(objects)) {
            userLead.setLeadMetadata(objects);
        }

        userLeadsDAO.save(userLead, null);
        if (userLead.getActivity() == null && ArrayUtils.isEmpty(userLead.getLeadMetadata())) {
            Map<String, Object> payload = new HashMap<>();
            payload.put("userLead", userLead);
            AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.USER_LEAD_EVENT, payload);
            asyncTaskFactory.executeTask(params);
        }
//		if (jsonObject.has("firstName")) {
//			String name = jsonObject.getJSONArray("firstName").getString(0);
//			update.set(UserLeads.Constants.FIRST_NAME, name);
//			lead.setFirstName(name);
//		}
//		if (jsonObject.has("lastName")) {
//			String name = jsonObject.getJSONArray("lastName").getString(0);
//			update.set(UserLeads.Constants.LAST_NAME, name);
//			lead.setLastName(name);
//		}
//		if (jsonObject.has("emailId")) {
//			String email = jsonObject.getJSONArray("emailId").getString(0);
//			if (StringUtils.isNotEmpty(email)) {
//
//				update.set(UserLeads.Constants.EMAIL, email.toLowerCase());
//				lead.setEmail(email.toLowerCase());
//				emailExists = true;
//			}
//
//		}
//		if (jsonObject.has("phoneCode")) {
//			String code = jsonObject.getJSONArray("phoneCode").getString(0);
//			update.set(UserLeads.Constants.PHONE_CODE, code);
//			lead.setPhoneCode(code);
//		}
//		if (jsonObject.has("contactNumber")) {
//			String nummber = jsonObject.getJSONArray("contactNumber").getString(0);
//			update.set(UserLeads.Constants.CONTACT_NUMBER, nummber);
//			lead.setContactNumber(nummber);
//		}
//
//		if (jsonObject.has("utm_source")) {
//			String utm_source = jsonObject.getJSONArray("utm_source").getString(0);
//			update.set(UserLeads.Constants.UTM_SOURCE, utm_source);
//			lead.setUtm_source(utm_source);
//		}
//		if (jsonObject.has("utm_campaign")) {
//			String utm_campaign = jsonObject.getJSONArray("utm_campaign").getString(0);
//			update.set(UserLeads.Constants.UTM_CAMPAIGN, utm_campaign);
//			lead.setUtm_campaign(utm_campaign);
//
//		}
//		if (jsonObject.has("utm_medium")) {
//			String utm_medium = jsonObject.getJSONArray("utm_medium").getString(0);
//			update.set(UserLeads.Constants.UTM_MEDIUM, utm_medium);
//			lead.setUtm_medium(utm_medium);
//
//		}

    }
    
    public void addUserLeadActivity(UserLeadActivity req) throws BadRequestException {
        if(StringUtils.isEmpty(req.getUserLeadsId()) || StringUtils.isEmpty(req.getSource()) || StringUtils.isEmpty(req.getEventName()) || StringUtils.isEmpty(req.getEventValue())){
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "missing leadId|| source || name ||value");
        }
        UserLeadActivity past = userLeadsActivityDAO.getLatestPastUserLeadActivity(req.getUserLeadsId(), req.getSource(), req.getEventName(), req.getEventValue());
        if(past == null){
            userLeadsActivityDAO.save(req, null);
        }
    }

    public void addEditActivity(String jsonString) throws BadRequestException {
        if (StringUtils.isEmpty(jsonString)) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "No data");
        }
        JSONObject jsonObject = new JSONObject(jsonString);
        addEditActivity(jsonObject);
    }

    public void addEditActivity(JSONObject jsonObject) throws BadRequestException {

        String number = "";
        String email = "";

        Query query = new Query();

        Criteria criteria = new Criteria();
        Criteria criteriaNumber = null;
        Criteria criteriaEmail = null;
        if (jsonObject.has("contactNumber")) {
            number = jsonObject.getJSONArray("contactNumber").getString(0);
            criteriaNumber = (Criteria.where(UserLead.Constants.CONTACT_NUMBER).is(number));
        }
        if (jsonObject.has("email")) {
            email = jsonObject.getJSONArray("email").getString(0);
            if (StringUtils.isNotEmpty(email)) {
                criteriaEmail = Criteria.where(UserLead.Constants.EMAIL).is(email.toLowerCase());

            }
        }

        if (StringUtils.isEmpty(email) && StringUtils.isEmpty(number)) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "No email or contact number");
        }
        if (criteriaEmail != null && criteriaNumber != null) {
            criteria = criteria.orOperator(criteriaEmail, criteriaNumber);
        } else if (criteriaEmail != null) {
            criteria = criteriaEmail;
        } else {
            criteria = criteriaNumber;
        }
        query.addCriteria(criteria);
        List<UserLead> leads = userLeadsDAO.runQuery(query, UserLead.class);
        UserLead exLead = null;
        if (ArrayUtils.isNotEmpty(leads)) {
            for (UserLead lead : leads) {
                if (StringUtils.isNotEmpty(lead.getEmail())) {
                    addEditActivity(jsonObject, lead);
                    return;
                }
                exLead = lead;
            }
            addEditActivity(jsonObject, exLead);
        } else {
            addEditUserLeads(jsonObject);
        }
    }

    public void addEditActivity(JSONObject jsonObject, UserLead userLeads) throws BadRequestException {

        UserLeadActivity leadsActivity = new UserLeadActivity();
        if (userLeads == null) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "No user Leads available");
        }
        leadsActivity.setUserLeadsId(userLeads.getId());
        if (jsonObject.has("activity")) {
            String activity = jsonObject.getJSONArray("activity").getString(0);
            if (StringUtils.isNotEmpty(activity)) {
                UserLeadsActivityType type = UserLeadsActivityType.valueOf(activity.toUpperCase());
                leadsActivity.setActivity(type);
            } else {
                logger.info("No activity" + activity);
                return;
            }
        } else {
            logger.info("No activity");
            return;
        }

        if (jsonObject.has("utm_source")) {
            String utm_source = jsonObject.getJSONArray("utm_source").getString(0);
            leadsActivity.setUtm_source(utm_source);
        }

        if (jsonObject.has("utm_campaign")) {

            String utm_campaign = jsonObject.getJSONArray("utm_campaign").getString(0);
            leadsActivity.setUtm_campaign(utm_campaign);
        }
        if (jsonObject.has("utm_medium")) {
            String utm_medium = jsonObject.getJSONArray("utm_medium").getString(0);
            leadsActivity.setUtm_medium(utm_medium);
        }
        if (jsonObject.has("ip_address")) {
            String ip_address = jsonObject.getJSONArray("ip_address").getString(0);
            leadsActivity.setIpAddress(ip_address);
        }

        int count = 0;
        Map<String, String> metadata = new HashMap<>();
        for (String key : jsonObject.keySet()) {
            if (key.contains("newKey")) {
                String value = jsonObject.getJSONArray("key").getString(0);
                String[] keyValues = key.split("#");
                if (keyValues.length > 1 && StringUtils.isNotEmpty(value)) {
                    if (value.length() > ReqLimits.NAME_TYPE_MAX) {
                        throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Too long string" + value.length() + " " + value);
                    }
                    String keyValue = keyValues[1];
                    metadata.put(keyValue, value);
                    count++;
                }
                if (count > 50) {
                    throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Too many new Keys");
                }
            }
        }

        leadsActivity.setMetadata(metadata);

        userLeadsActivityDAO.save(leadsActivity, null);

    }

    public List<UserLead> getUserLeads(GetUserLeadReq req) {

        return userLeadsDAO.getUserLeads(req);
    }
    
    public void fillLocation(UserLead userLead, boolean sendSMS) throws VException {
        String ipAddress=userLead.getIpAddress();
        if (StringUtils.isNotEmpty(ipAddress)) {
            String url=String.format(IP_LOCATION_URL_BUILDER,ipAddress);
            ClientResponse resp = WebUtils.INSTANCE.doCall(url, HttpMethod.GET, null, false);
            VExceptionFactory.INSTANCE.parseAndThrowException(resp);
            Type type = new TypeToken<Map<String, Object>>() {
            }.getType();
            String jsonString = resp.getEntity(String.class);
            Map<String, Object> response = new Gson().fromJson(jsonString, type);
            if(response.containsKey("geolocation_data")){
                Map<String, Object> test= (Map)response.get("geolocation_data");
                if(test.containsKey("country_name")){
                    String country=(String)test.get("country_name");
                    userLead.setCountry(country);
                }
                if(test.containsKey("city")){
                    String city=(String)test.get("city");
                    userLead.setCity(city);
                }
                userLeadsDAO.save(userLead, null);
            }
        }
        if(sendSMS){
            checkAndSendSMS(userLead);
        }
    }

    public void checkAndSendSMS(UserLead userLead) {
        if (StringUtils.isNotEmpty(userLead.getContactNumber())) {
            publishSNS(mapper.map(userLead, UserLeadInfo.class));
//            Query query = new Query();
//            query.addCriteria(Criteria.where(UserLead.Constants.CONTACT_NUMBER).is(userLead.getContactNumber()));
//
//            List<UserLead> userLeads = userLeadsDAO.runQuery(query, UserLead.class);
//            publishSNS(mapper.map(userLeads.get(0), UserLeadInfo.class));
//            if (ArrayUtils.isNotEmpty(userLeads) && userLeads.size() > 1) {
//                return;
//            } else {
//                publishSNS(mapper.map(userLeads.get(0), UserLeadInfo.class));
//            }
        }
    }

    public void publishSNS(UserLeadInfo info) {        
        String topicArn = "arn:aws:sns:ap-southeast-1:" + arn + ":USER_EVENTS_" + env.toUpperCase();
        // String msg = "{\"sessionId\":\"" + sessionId + "\"}";
        String msg = new Gson().toJson(info);
        String snsSubject = UserEvents.USER_LEAD_SMS.toString();
        PublishRequest publishRequest = new PublishRequest(topicArn, msg, snsSubject);
        PublishResult publishResult = snsClient.publish(publishRequest);
        logger.info("Published SNS, MessageId - " + publishResult.getMessageId());
    }

    public UserLeadInfo fetchUserLead(GetUserLeadReq req) throws NotFoundException, BadRequestException {
        Query query = new Query();
        boolean check = false;
        if(StringUtils.isEmpty(req.getSource())){
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "source should not be empty");
        }
        query.addCriteria(Criteria.where(UserLead.Constants.SOURCE).is(req.getSource()));
        if (StringUtils.isNotEmpty(req.getEmail())) {
            query.addCriteria(Criteria.where(UserLead.Constants.EMAIL).is(req.getEmail()));
            check = true;
        }
        if (StringUtils.isNotEmpty(req.getContactNumber())) {
            query.addCriteria(Criteria.where(UserLead.Constants.CONTACT_NUMBER).is(req.getContactNumber()));
            check = true;
        }
        if (!check) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "no input params");
        }
        logger.info("fetchUserLead: query " + query);
//        query.with(new Sort(Direction.DESC, UserLead.Constants.CREATION_TIME));

        UserLead lead = userLeadsDAO.findOne(query, UserLead.class);
        logger.info("fetchUserLead: lead " + lead);
        if (lead!=null) {
            UserLeadInfo info = mapper.map(lead, UserLeadInfo.class);
            info.setUserLeadsId(lead.getId());
            return info;
        } else {
            throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, "Lead not found for phone" + req.getContactNumber()
                    + " and email " + req.getEmail());
        }

    }

    public Set<String> getUserLeadsNumber() {

        int start = 0;
        int size = 500;
        Set<String> userNumbers = new HashSet();
        Query queryCount = new Query();
        Long count = userLeadsDAO.queryCount(queryCount, UserLead.class);
        while (true) {
            Query query = new Query();
            userLeadsDAO.setFetchParameters(query, start, size);
            query.with(Sort.by(Direction.ASC, UserLead.Constants.CREATION_TIME));
            List<UserLead> leadsTemp = userLeadsDAO.runQuery(query, UserLead.class);
            if (ArrayUtils.isEmpty(leadsTemp)) {
                break;
            }
            start = start + leadsTemp.size();
            for (UserLead userLead : leadsTemp) {
                if (StringUtils.isNotEmpty(userLead.getContactNumber())) {
                    userNumbers.add(userLead.getContactNumber());
                }
            }
            if (count != null && start > count.intValue()) {
                break;
            }
        }
        return userNumbers;
    }

    @PreDestroy
    public void cleanUp() {
        try {
            if (snsClient != null) {
                snsClient.shutdown();
            }
        } catch (Exception e) {
            logger.error("Error in closing snsclient connection ", e);
        }
    }    
}
