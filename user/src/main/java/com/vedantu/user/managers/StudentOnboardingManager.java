package com.vedantu.user.managers;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.vedantu.user.enums.ABTestingType;
import com.vedantu.util.*;
import org.apache.logging.log4j.Logger;
import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.User.LocationInfo;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.ForbiddenException;
import com.vedantu.exception.InternalServerErrorException;
import com.vedantu.exception.VException;
import com.vedantu.exception.VExceptionFactory;
import com.vedantu.subscription.pojo.BundleInfo;
import com.vedantu.user.dao.RedisDAO;
import com.vedantu.user.dao.StudentOnboardingDAO;
import com.vedantu.user.entity.StudentOnboarding;
import com.vedantu.user.entity.User;
import com.vedantu.user.enums.OnboardingCompletionStatus;
import com.vedantu.user.enums.OnboardingStep;
import com.vedantu.user.enums.SAMIntroductionStatus;
import com.vedantu.user.enums.SalesVerificationStatus;
import com.vedantu.user.pojo.onboarding.CourseEnroll;
import com.vedantu.user.pojo.onboarding.Introduction;
import com.vedantu.user.pojo.onboarding.ProfileCreation;
import com.vedantu.user.pojo.onboarding.SAMIntroductionInfo;
import com.vedantu.user.pojo.onboarding.SalesVerificationInfo;
import com.vedantu.user.pojo.onboarding.StudentDetails;
import com.vedantu.user.requests.SalesVerificationAndSAMDataSetupRequest;
import com.vedantu.user.requests.SalesVerificationAndSAMInformationRequest;
import com.vedantu.user.requests.StudentOnboardingRequest;
import com.vedantu.user.responses.StudentOnboardingCompletionStatusResp;
import com.vedantu.user.responses.StudentOnboardingResponse;
import com.vedantu.user.utils.PojoUtils;
import com.vedantu.util.pojo.DeviceDetails;
import com.vedantu.util.pojo.DeviceType;
import com.vedantu.util.security.HttpSessionData;
import com.vedantu.util.security.HttpSessionUtils;

import nl.basjes.parse.useragent.UserAgent;

@Service
public class StudentOnboardingManager {

    @Autowired
    private StudentOnboardingDAO studentOnboardingDAO;

    @Autowired
    private HttpSessionUtils httpSessionUtils;

    @Autowired
    private UserManager userManager;

    @Autowired
    private DozerBeanMapper mapper;

    @Autowired
    private PojoUtils pojoUtils;

    @Autowired
    private IPUtil ipUtil;

    @Autowired
    private UserAgentParser userAgentParser;

    @Autowired
    private RedisDAO redisDAO;

    private final Logger logger = LogFactory.getLogger(StudentOnboardingManager.class);

    private static final String SUBSCRIPTION_ENDPOINT = ConfigUtils.INSTANCE.getStringValue("SUBSCRIPTION_ENDPOINT");
    private static final Gson gson = new Gson();
    private static final String ONBOARDING_KEY="ONBOARDING_KEY";

    public StudentOnboardingResponse getOnboardingData(StudentOnboardingRequest studentOnboardingRequest, HttpServletRequest request, HttpServletResponse response) throws VException, IOException, URISyntaxException {
        Long currentUser = httpSessionUtils.getCallingUserId();
        if (!currentUser.equals(studentOnboardingRequest.getStudentId())) {
            throw new ForbiddenException(ErrorCode.FORBIDDEN_ERROR, "Malicious request from user id " + currentUser);
        }
        User user = userManager.getUserByUserId(currentUser);
        changeSessionData(request, response, user);
        return getStudentOnboardingResponseData(studentOnboardingRequest.getStudentId());
    }

    private StudentOnboardingResponse getStudentOnboardingResponseData(Long currentUser) throws VException {
        StudentOnboarding studentOnboarding = studentOnboardingDAO.getOnboardingDataUsingStudentId(currentUser);
        if (Objects.isNull(studentOnboarding) || OnboardingCompletionStatus.SKIPPED.equals(studentOnboarding.getCompletionStatus())) {
            throw new BadRequestException(ErrorCode.ONBOARDING_NOT_ALLOWED, "Student already onboarded or no information available for user id " + currentUser);
        } else {
            if(OnboardingCompletionStatus.PENDING.equals(studentOnboarding.getCompletionStatus())){
                studentOnboardingDAO.setOnboardingStateAsStarted(currentUser);
            }
            logger.info("getStudentOnboardingResponseData - studentOnboarding - {}", studentOnboarding);
        }
        return getStudentOnboardingResponse(currentUser, studentOnboarding);
    }

    private StudentOnboardingResponse getStudentOnboardingResponse(Long currentUser, StudentOnboarding studentOnboarding) throws VException {
        StudentOnboardingResponse studentOnboardingResponse = new StudentOnboardingResponse(studentOnboarding);
        if (OnboardingCompletionStatus.PENDING.equals(studentOnboardingResponse.getProfileCreation().getStepCompletionStatus())) {
            User user = userManager.getUserByUserId(currentUser);
            if (Objects.isNull(user)) {
                throw new BadRequestException(ErrorCode.USER_NOT_AVAILABLE, "No user available for user id " + currentUser);
            }
            studentOnboardingResponse.setDefaultProfileInformation(user);
        }
        return studentOnboardingResponse;
    }


    public StudentOnboardingResponse setOnboardingData(StudentOnboardingRequest studentOnboardingRequest, HttpServletRequest request, HttpServletResponse response)
            throws VException, IOException, URISyntaxException {

        Long currentUser = httpSessionUtils.getCallingUserId();
        if (!currentUser.equals(studentOnboardingRequest.getCallingUserId())) {
            throw new ForbiddenException(ErrorCode.FORBIDDEN_ERROR, "Malicious request from user id " + currentUser);
        }
        User user = userManager.getUserByUserId(currentUser);
        changeSessionData(request, response, user);
        if(ArrayUtils.isEmpty(user.getPendingBundleIdForOnboarding())){
            throw new BadRequestException(ErrorCode.FORBIDDEN_ERROR,"User can't go for onboarding as there's nothing to onboard for");
        }

        // Validate on current onboarding step type
        OnboardingStep currentOnboardingStep = studentOnboardingRequest.getOnboardingStepType();
        if (Objects.isNull(currentOnboardingStep)) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "[onboardingStepType] value is missing.");
        }

        // Check if that onboarding step is supposed to be completed or not
        String bundleId = studentOnboardingRequest.getBundleId();
        if (!isCurrentOnboardingStepPermissible(currentOnboardingStep, studentOnboardingRequest.getStudentId(), bundleId)) {
            throw new BadRequestException(ErrorCode.ONBOARDING_STEP_ALREADY_DONE, "Current onboarding step is already done by the user");
        }

        // After all validations are in place then complete the insertion step
        Update update = new Update();
        switch (currentOnboardingStep) {
            case INTRODUCTION: {
                Introduction introduction = studentOnboardingRequest.getIntroduction();
                if(studentOnboardingRequest.isIntroductionCompleted()) {
                    introduction.markCurrentStepCompleted();
                    String ipAddress = studentOnboardingRequest.getIpAddress();
                    ipAddress = ipAddress.split(", ")[0]; // can be more than 1 IP
                    String userAgent = studentOnboardingRequest.getUserAgent();
                    if(Objects.nonNull(studentOnboardingRequest.getRequestSource())) {
                        update.set(StudentOnboarding.Constants.REQUEST_SOURCE, studentOnboardingRequest.getRequestSource());
                    }
                    update.set(StudentOnboarding.Constants.IP_ADDRESS,ipAddress);
                    if(StringUtils.isNotEmpty(studentOnboardingRequest.getGeoCountry())) {
                        update.set(StudentOnboarding.Constants.GEO_COUNTRY, studentOnboardingRequest.getGeoCountry());
                    }
                    update.set(StudentOnboarding.Constants.USER_AGENT,userAgent);
                    if(StringUtils.isNotEmpty(studentOnboardingRequest.getAppVersionCode())) {
                        update.set(StudentOnboarding.Constants.APP_VERSION_CODE, studentOnboardingRequest.getAppVersionCode());
                    }
                    if(StringUtils.isNotEmpty(studentOnboardingRequest.getAppDeviceId())) {
                        update.set(StudentOnboarding.Constants.APP_DEVICE_ID, studentOnboardingRequest.getAppDeviceId());
                    }
                    update.set(StudentOnboarding.Constants.LOCATION_INFO,ipUtil.getLocationFromIp(ipAddress));
                    update.set(StudentOnboarding.Constants.DEVICE_DETAILS,getDeviceDetails(userAgent));
                }
                if (introduction.checkDataValidity()) {
                    throw new BadRequestException(ErrorCode.INVALID_ONBOARDING_DATA, "Invalid data for " + currentOnboardingStep + " step to save");
                }
                update.set(StudentOnboarding.Constants.INTRODUCTION, introduction);
                update.set(StudentOnboarding.Constants.COMPLETION_STATUS, OnboardingCompletionStatus.ONGOING);
                break;
            }

            case PROFILE_CREATION: {
                ProfileCreation profileCreation = studentOnboardingRequest.getProfileCreation();
                profileCreation.markCurrentStepCompleted();
                if (profileCreation.checkDataValidity()) {
                    throw new BadRequestException(ErrorCode.INVALID_ONBOARDING_DATA, "Invalid data for " + currentOnboardingStep + " step to save");
                }
                updateProfileCreationUserData(profileCreation, currentUser, request, response);
                update.set(StudentOnboarding.Constants.PROFILE_CREATION, profileCreation);
                break;
            }

            case COURSE_ENROLL: {
                CourseEnroll courseEnroll = studentOnboardingRequest.getCourseEnroll();
                courseEnroll.markCurrentStepCompleted();
                if (courseEnroll.checkDataValidity()) {
                    throw new BadRequestException(ErrorCode.INVALID_ONBOARDING_DATA, "Invalid data for " + currentOnboardingStep + " step to save");
                }
                update.set(StudentOnboarding.Constants.COMPLETION_STATUS, OnboardingCompletionStatus.COMPLETED);
                update.set(StudentOnboarding.Constants.COURSE_ENROLL, courseEnroll);

                // update student's profile
                updateProfileCompletionUserData(currentUser, bundleId);

                // change session data
                changeSessionData(request, response, user);
                break;
            }

            case NONE:
            default:
                break;
        }

        studentOnboardingDAO.updateRequiredData(update, currentUser, bundleId);

        StudentOnboarding studentOnboarding = studentOnboardingDAO.getDataForUserId(currentUser);

        return getStudentOnboardingResponse(currentUser, studentOnboarding);
    }

    private void updateProfileCompletionUserData(Long currentUser, String bundleId) throws VException {
        User student = userManager.getUserByUserId(currentUser);

        Set<String> pendingBundleIdForOnboarding = new HashSet<>();
        Set<String> completedBundleIdForOnboarding = new HashSet<>();

        if (ArrayUtils.isNotEmpty(student.getPendingBundleIdForOnboarding())) {
            pendingBundleIdForOnboarding = student.getPendingBundleIdForOnboarding();
        }

        if (ArrayUtils.isNotEmpty(student.getCompletedBundleIdForOnboarding())) {
            completedBundleIdForOnboarding = student.getCompletedBundleIdForOnboarding();
        }

        if (pendingBundleIdForOnboarding.contains(bundleId)) {
            pendingBundleIdForOnboarding.remove(bundleId);
            completedBundleIdForOnboarding.add(bundleId);
            userManager.updateBundleInfoForUserOnboarding(student, currentUser, pendingBundleIdForOnboarding, completedBundleIdForOnboarding);
        } else {
            if (!completedBundleIdForOnboarding.contains(bundleId)) {
                throw new BadRequestException(ErrorCode.INVALID_BUNDLE_ID_FOR_ONBOARDING_COMPLETION, "For [studentId] "
                        + currentUser + " and [bundleId] " + bundleId + " onboarding completion is not possible");
            }
        }
    }

    private void updateProfileCreationUserData(ProfileCreation profileCreation, Long currentUser, HttpServletRequest request, HttpServletResponse response) throws VException, IOException, URISyntaxException {
        User user = userManager.getUserByUserId(currentUser);
        if (Objects.isNull(user)) {
            throw new BadRequestException(ErrorCode.USER_NOT_AVAILABLE, "No user available for user id " + currentUser);
        }

        // Set student details
        StudentDetails studentDetails = profileCreation.getStudentDetails();
        user.setFirstName(studentDetails.getFirstName());
        user.setLastName(studentDetails.getLastName());

        if(StringUtils.isNotEmpty(studentDetails.getEmail())){
            if(CommonUtils.isValidEmailId(studentDetails.getEmail())) {
                user.setEmail(studentDetails.getEmail());
            }
            else{
                throw new BadRequestException(ErrorCode.INVALID_EMAILID, "email id is not valid");
            }
        }
        user.setPhones(studentDetails.getPhoneNumbers());
        user.setDob(studentDetails.getDob());
        user.setLanguagePrefs(studentDetails.getPreferredLanguages());
        user.setGender(studentDetails.getGender());

        // Set parent infos
        user.getStudentInfo().setParentInfos(profileCreation.getParentDetails());

        // Update all the information
        userManager.updateUser(user, currentUser);

        // Change session data
        changeSessionData(request, response, user);
    }

    private boolean isCurrentOnboardingStepPermissible(OnboardingStep currentOnboardingStep, Long studentId, String bundleId) throws BadRequestException {
        StudentOnboarding onboardingData = studentOnboardingDAO.getOnBoardingDataUsingStudentAndBundleId(studentId, bundleId);
        if (Objects.isNull(onboardingData)) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "No onboarding data found for [studentId] " + studentId + " [bundleId] " + bundleId);
        }

        if (Objects.isNull(currentOnboardingStep)) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "[currentOnboardingStep] cann't be null");
        }
        OnboardingCompletionStatus previousStepCompletionStatus = null;
        switch (currentOnboardingStep) {
            case INTRODUCTION: {
                previousStepCompletionStatus = null;
                break;
            }

            case PROFILE_CREATION: {
                previousStepCompletionStatus = onboardingData.getIntroduction().getStepCompletionStatus();
                break;
            }

            case COURSE_ENROLL: {
                previousStepCompletionStatus = onboardingData.getProfileCreation().getStepCompletionStatus();
                break;
            }

            case NONE:
            default:
                logger.error("This should never happen, need to check for onboarding step {}", currentOnboardingStep);
                break;
        }

        return OnboardingCompletionStatus.COMPLETED.equals(previousStepCompletionStatus) || Objects.isNull(previousStepCompletionStatus);
    }

    public PlatformBasicResponse insertInitialOnboardingInformation(StudentOnboardingRequest studentOnboardingRequest, HttpServletRequest request, HttpServletResponse response) throws VException, IOException, URISyntaxException {

        Long studentId = studentOnboardingRequest.getStudentId();
        Long callingUserId = studentOnboardingRequest.getCallingUserId();
        if (Objects.isNull(studentId) || Objects.isNull(studentOnboardingRequest.getBundleId()) || Objects.isNull(studentOnboardingRequest.getBundleName())) {
            throw new BadRequestException(ErrorCode.MISSING_PARAMETER, "Some of the required parameters are missing, kindly check again");
        }

        User student = userManager.getUserByUserId(studentId);

        if (Objects.isNull(student)) {
            logger.error("no user exist for userId: " + studentId);
            return new PlatformBasicResponse(false, "", "student does not exist");
        }

        StudentOnboarding studentOnboardingDB = studentOnboardingDAO.getDataForUserId(studentId);
        if (Objects.nonNull(studentOnboardingDB)) {
            logger.info("student onboarding already exists");
            return new PlatformBasicResponse(false, "", "student onboarding already exists");
        }

        String checkUrl = SUBSCRIPTION_ENDPOINT + "/bundle/getBundleInfoForOnboarding?userId=" + studentId + "&bundleId=" + studentOnboardingRequest.getBundleId();
        ClientResponse checkResp = WebUtils.INSTANCE.doCall(checkUrl, HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(checkResp);
        String bundleInfoString = checkResp.getEntity(String.class);
        BundleInfo bundleInfo = gson.fromJson(bundleInfoString, BundleInfo.class);
        if (bundleInfo == null) {
            logger.info("no enrollment available in db for onboarding");
            return new PlatformBasicResponse(false, "", "no enrollment available in db for onboarding");
        }

        if (StringUtils.isNotEmpty(student.getEmail())) {
            studentOnboardingRequest.setStudentEmail(student.getEmail());
        }
        if (student.getStudentInfo() != null && StringUtils.isNotEmpty(student.getStudentInfo().getGrade())) {
            studentOnboardingRequest.setStudentGrade(student.getStudentInfo().getGrade());
        }
        if (StringUtils.isNotEmpty(student.getFullName())) {
            studentOnboardingRequest.setStudentName(student.getFullName());
        }

        studentOnboardingRequest.setDownPayment((long) bundleInfo.getPrice());
        studentOnboardingRequest.setBookingPotential((long) bundleInfo.getCutPrice());

        StudentOnboarding studentOnboarding = getOnboardingObject(studentOnboardingRequest);
        boolean newOnboardingToBeDone=allowUserForOnboarding();
        if(newOnboardingToBeDone){
            studentOnboarding.setAbTestingType(ABTestingType.B);
        }
        studentOnboardingDAO.save(studentOnboarding);

        Set<String> onboardingBundleIds = new HashSet<>();
        if (ArrayUtils.isNotEmpty(student.getPendingBundleIdForOnboarding())) {
            onboardingBundleIds = student.getPendingBundleIdForOnboarding();
        }
        onboardingBundleIds.add(studentOnboardingRequest.getBundleId());
        userManager.updateBundleInfoForUserOnboarding(student, studentId, onboardingBundleIds, null);

        //For session data changing
        student.setPendingBundleIdForOnboarding(onboardingBundleIds);

        // Taking care of manual enrollment case
        if(Objects.nonNull(callingUserId) && callingUserId.equals(studentId)) {
            changeSessionData(request, response, student);
        }
        return new PlatformBasicResponse();
    }

    private boolean allowUserForOnboarding() throws BadRequestException, InternalServerErrorException {
//        String key = redisDAO.get(ONBOARDING_KEY);
//        if(StringUtils.isEmpty(key)){
//            key="0";
//        }
//        long count=Long.parseLong(key)+1;
//        redisDAO.set(ONBOARDING_KEY,Long.toString(count));
        return true;
    }

    private StudentOnboarding getOnboardingObject(StudentOnboardingRequest studentOnboardingRequest) {

        StudentOnboarding studentOnboarding = new StudentOnboarding();

        studentOnboarding.setStudentId(studentOnboardingRequest.getStudentId());
        studentOnboarding.setStudentName(studentOnboardingRequest.getStudentName());
        studentOnboarding.setStudentEmail(studentOnboardingRequest.getStudentEmail());
        studentOnboarding.setStudentGrade(studentOnboardingRequest.getStudentGrade());

        studentOnboarding.setBundleId(studentOnboardingRequest.getBundleId());
        studentOnboarding.setBundleName(studentOnboardingRequest.getBundleName());
        studentOnboarding.setEntityType(studentOnboardingRequest.getEntityType());

        studentOnboarding.setPaymentType(studentOnboardingRequest.getPaymentType());
        studentOnboarding.setDownPayment(studentOnboardingRequest.getDownPayment());
        studentOnboarding.setBookingPotential(studentOnboardingRequest.getBookingPotential());

        return studentOnboarding;
    }

    private DeviceDetails getDeviceDetails(String userAgent) {
        DeviceDetails deviceDetails = new DeviceDetails();
        if (StringUtils.isNotEmpty(userAgent)) {
            UserAgent details = userAgentParser.getUserAgentDetails(userAgent);
            if (StringUtils.isNotEmpty(details.getValue("DeviceClass"))) {
                deviceDetails.setDeviceType(DeviceType.valueOfKey(details.getValue("DeviceClass")));
            }
            if (StringUtils.isNotEmpty(details.getValue("DeviceName"))) {
                deviceDetails.setDeviceName(details.getValue("DeviceName"));
            }

            if (StringUtils.isNotEmpty(details.getValue("DeviceBrand"))) {
                deviceDetails.setDeviceBrand(details.getValue("DeviceBrand"));
            }
            if (StringUtils.isNotEmpty(details.getValue("AgentName"))) {
                deviceDetails.setBrowserName(details.getValue("AgentName"));
            }
            if (StringUtils.isNotEmpty(details.getValue("AgentVersion"))) {
                deviceDetails.setBrowserVersion(details.getValue("AgentVersion"));
            }
            if (StringUtils.isNotEmpty(details.getValue("AgentVersionMajor"))) {
                deviceDetails.setBrowserVersionMajor(details.getValue("AgentVersionMajor"));
            }
        }
        return deviceDetails;
    }


    private void changeSessionData(HttpServletRequest request, HttpServletResponse response, User user) throws IOException, URISyntaxException {
        com.vedantu.User.User userToModify = pojoUtils.convertToUserPojo(user);
        logger.info("userToModify - {}", userToModify);
        HttpSessionData httpSessionData = httpSessionUtils.setCookieAndHeaders(request, response, userToModify);
        logger.info("httpSessionData - changeSessionData - {}", httpSessionData);
        logger.info("httpSessionData - currentSessionData - {}", httpSessionUtils.getCurrentSessionData());
    }

    public List<StudentOnboardingResponse> getSalesVerificationAndSAMData(SalesVerificationAndSAMInformationRequest salesVerificationAndSAMInformationRequest) {
        List<StudentOnboarding> onboardingData = studentOnboardingDAO.getOnboardingData(salesVerificationAndSAMInformationRequest);
        return Optional.ofNullable(onboardingData)
                .map(Collection::stream)
                .orElseGet(Stream::empty)
                .map(StudentOnboardingResponse::new)
                .collect(Collectors.toList());
    }

    public PlatformBasicResponse setSalesVerificationAndSAMData(SalesVerificationAndSAMDataSetupRequest salesVerificationAndSAMInformationRequest) throws VException {

        String onboardingId = salesVerificationAndSAMInformationRequest.getOnboardingId();
        StudentOnboarding studentOnboarding = studentOnboardingDAO.getExistingOnboardingDataForSalesVerification(onboardingId);
        Long callingUserId = salesVerificationAndSAMInformationRequest.getCallingUserId();
        SalesVerificationInfo salesVerificationInfo = salesVerificationAndSAMInformationRequest.getSalesVerificationInfo();
        SalesVerificationInfo currentVerificationInfo = studentOnboarding.getSalesVerificationInfo();
        SAMIntroductionInfo samIntroductionInfo = salesVerificationAndSAMInformationRequest.getSamIntroductionInfo();

        if (Objects.nonNull(salesVerificationInfo)) {
            if (Objects.nonNull(currentVerificationInfo) &&
                    !SalesVerificationStatus.VERIFICATION_PENDING.equals(currentVerificationInfo.getSalesVerificationStatus())) {
                throw new BadRequestException(ErrorCode.SALES_VERIFICATION_ALREADY_COMPLETED, "Sales verification already completed, so can't do it again");
            }
            User user = userManager.getUserByUserId(callingUserId);
            salesVerificationInfo.setSalesVerifierId(user.getId());
            salesVerificationInfo.setSalesVerifierEmail(user.getEmail());
            salesVerificationInfo.setSalesVerificationTime(System.currentTimeMillis());
            studentOnboardingDAO.updateSalesVerificationInfo(onboardingId, salesVerificationInfo, callingUserId);
        } else if (Objects.nonNull(samIntroductionInfo)) {
            if (Objects.isNull(currentVerificationInfo) ||
                    SalesVerificationStatus.VERIFICATION_PENDING.equals(currentVerificationInfo.getSalesVerificationStatus())) {
                throw new BadRequestException(ErrorCode.SAM_ACTION_NOT_ALLOWED, "No activity from SAM is allowed unless and until sales verification is completed or failed");
            }
            if (Objects.nonNull(studentOnboarding.getSamIntroductionInfo()) &&
                    SAMIntroductionStatus.COMEPLTED.equals(studentOnboarding.getSamIntroductionInfo().getSamIntroductionStatus())) {
                throw new BadRequestException(ErrorCode.SAM_INTRO_ALREADY_DONE, "SAM introduction is already done.");
            }
            samIntroductionInfo.setSamIntroductionTime(System.currentTimeMillis());
            studentOnboardingDAO.updateSAMIntroductionInfo(onboardingId, samIntroductionInfo, callingUserId);
        } else {
            logger.error("Something went wrong, please look into the request payload - {}", salesVerificationAndSAMInformationRequest);
        }

        return new PlatformBasicResponse();
    }

    public void migrateDeviceAndLocationData() {
        List<StudentOnboarding> studentOnboardings=studentOnboardingDAO.getOnboardingDataForDeviceDataAndLocationMigration();

        while(ArrayUtils.isNotEmpty(studentOnboardings)){
            studentOnboardings.stream().peek(logger::info).forEach(this::updateDeviceAndLocationData);
            studentOnboardings=studentOnboardingDAO.getOnboardingDataForDeviceDataAndLocationMigration();
        }
    }

    private void updateDeviceAndLocationData(StudentOnboarding studentOnboarding) {
        LocationInfo locationInfo=ipUtil.getLocationFromIp(studentOnboarding.getIpAddress().split(", ")[0]);
        DeviceDetails deviceDetails=getDeviceDetails(studentOnboarding.getUserAgent());
        studentOnboardingDAO.updateDeviceAndLocationData(studentOnboarding.getId(),deviceDetails,locationInfo);
    }

    public StudentOnboardingCompletionStatusResp getCompletionStatus(Long userId) {
        return studentOnboardingDAO.getCompletionStatus(userId);
    }
}
