package com.vedantu.user.managers.login;

import com.google.gson.Gson;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.core.util.MultivaluedMapImpl;
import com.vedantu.User.FeatureSource;
import com.vedantu.User.Role;
import com.vedantu.User.StudentInfo;
import com.vedantu.User.request.PaytmAccessTokenResp;
import com.vedantu.User.request.PaytmSignInReq;
import com.vedantu.User.request.PaytmUserProfileResp;
import com.vedantu.async.AsyncTaskFactory;
import com.vedantu.exception.*;
import com.vedantu.user.dao.RedisDAO;
import com.vedantu.user.dao.UserDAO;
import com.vedantu.user.dao.UserLoginDataDao;
import com.vedantu.user.dao.UserLoginTokenDAO;
import com.vedantu.user.entity.User;
import com.vedantu.user.enums.VerifiedType;
import com.vedantu.user.helper.BotFilter;
import com.vedantu.user.managers.UserManager;
import com.vedantu.user.requests.SignUpUserReq;
import com.vedantu.user.responses.SignUpUserRes;
import com.vedantu.user.utils.PojoUtils;
import com.vedantu.util.*;
import com.vedantu.util.security.HttpSessionData;
import com.vedantu.util.security.HttpSessionUtils;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.http.HttpMethod;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.MultivaluedMap;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.*;

@Service
public class PaytmUserManager {

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(PaytmUserManager.class);

    @Autowired
    private UserDAO userDAO;

    @Autowired
    public PasswordEncoder passwordEncoder;

    @Autowired
    public PojoUtils pojoUtils;

    @Autowired
    private HttpSessionUtils sessionUtils;

    @Autowired
    private UserLoginTokenDAO userLoginTokenDAO;

    @Autowired
    private UserManager userManager;
    @Autowired
    private UserLoginDataDao loginDataDao;

    @Autowired
    private BotFilter botFilter;

    @Autowired
    private RedisDAO redisDao;

    @Autowired
    private AsyncTaskFactory asyncTaskFactory;


    private static final String CLIENT_ID = ConfigUtils.INSTANCE.getStringValue("paytm.mini.client_id");
    private static final String CLIENT_SECRET = ConfigUtils.INSTANCE.getStringValue("paytm.mini.client_secret");
    private static final String PAYTM_ACCOUNT_URL = ConfigUtils.INSTANCE.getStringValue("paytm.mini.account_url");

    public HttpSessionData authenticateUser(PaytmSignInReq requestBody, HttpServletRequest request,
                                            HttpServletResponse response ) throws Exception {

        com.vedantu.User.User userPojo = new com.vedantu.User.User();


        Boolean isEmailConflicted = false;
        PaytmAccessTokenResp accessTokenReq =getAccessToken(requestBody);
        PaytmUserProfileResp paytmUserProfileResp =  getUserInfo(accessTokenReq);


        if(paytmUserProfileResp  == null ||  paytmUserProfileResp.getPhoneInfo() == null || StringUtils.isEmpty(  paytmUserProfileResp.getPhoneInfo().getPhoneNumber()) || StringUtils.isEmpty( paytmUserProfileResp.getPhoneInfo().getCountryCode())){
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Invalid Paytm request");
        }

        logger.info("paytm user "+ paytmUserProfileResp);
//       1) Check if User is present with Paytm PhoneNumber

        User user = userDAO.getUserByPhoneNumberAndCode(paytmUserProfileResp.getPhoneInfo().getPhoneNumber(), paytmUserProfileResp.getPhoneInfo().getCountryCode());


//        2) if existing user dont have emailId then update it with paytm Email Id

        if(user != null && StringUtils.isNotEmpty( paytmUserProfileResp.getEmail() ) &&  StringUtils.isEmpty(user.getEmail()) ) {
            if(userDAO.getUserByEmail(paytmUserProfileResp.getEmail()) == null) {

                if(CommonUtils.isValidEmailId(paytmUserProfileResp.getEmail())) {
                    user.setEmail(paytmUserProfileResp.getEmail());
                }
                else{
                    throw new BadRequestException(ErrorCode.INVALID_EMAILID, "email id is not valid");
                }
                userDAO.update(user, user.getId());
            }
            else {
                isEmailConflicted=true;
            }
        }


//        3) Check if User is present with Paytm Email Id

        if(user == null && StringUtils.isNotEmpty( paytmUserProfileResp.getEmail() ) ){
                user=userDAO.getUserByEmail(paytmUserProfileResp.getEmail());
                logger.info("user by mail "+ user);
                if(user != null) {
//                    4) Check if User  Email Id is verified
//                            if not then dont consider this account
//                    if (!user.getIsEmailVerified()) {
//                        user = null;
//                        paytmUserProfileResp.setEmail(null);
//                        isEmailConflicted=true;
//                    } else {
//                     5)   if User  Email Id is verified
//                            contact number is not verified then use paytm number
                        if(!user.getIsContactNumberVerified()) {
                            user.setContactNumber(paytmUserProfileResp.getPhoneInfo().getPhoneNumber());
                            user.setPhoneCode(paytmUserProfileResp.getPhoneInfo().getCountryCode());
                            user.setIsContactNumberVerified(true);
                            user.setContactNumberVerifiedBy(VerifiedType.PAYTM);
                            userDAO.update(user, user.getId());
                            userPojo = pojoUtils.convertToUserPojo(user);
                        }
                        else{
                                user = null;
                        paytmUserProfileResp.setEmail(null);
                        isEmailConflicted=true;

                            }

                    }
                }


//        6)if email and number both are absent then crate new user
       if(user == null) {
                SignUpUserReq signUpUserReq = new SignUpUserReq();
                if(StringUtils .isNotEmpty(requestBody.getIpAddress() )) {
                    signUpUserReq.setIpAddress(requestBody.getIpAddress());
                }
                signUpUserReq.setPhoneCode(paytmUserProfileResp.getPhoneInfo().getCountryCode());
                signUpUserReq.setPhoneNumber(paytmUserProfileResp.getPhoneInfo().getPhoneNumber());
                signUpUserReq.setFullName(paytmUserProfileResp.getProfileInfo().getDisplayName());
                StudentInfo studentInfo = new StudentInfo();
                if (StringUtils.isNotEmpty(paytmUserProfileResp.getEmail())  && CommonUtils.isValidEmailId(paytmUserProfileResp.getEmail())) {
                    signUpUserReq.setEmail(paytmUserProfileResp.getEmail());
                }
                studentInfo.setGrade(requestBody.getGrade());
                if(Arrays.asList("CBSE","ICSE","IGCSE","IB").contains( requestBody.getTarget().toUpperCase())) {
                    studentInfo.setBoard(requestBody.getTarget().toUpperCase());
                }
                else{
                    studentInfo.setBoard("STATE BOARD");

                }
                if(Arrays.asList("NEET","JEE","OTHERS").contains( requestBody.getTarget().toUpperCase())) {
                studentInfo.setTarget(requestBody.getTarget().toUpperCase());
                }
                signUpUserReq.setStudentInfo(studentInfo);

                signUpUserReq.setSignUpFeature(FeatureSource.PAYTM_MINI);
                redisDao.setex("PAYTM_" + paytmUserProfileResp.getPhoneInfo().getPhoneNumber(), accessTokenReq.getAccess_token(), 300);
                signUpUserReq.setPaytmToken(accessTokenReq.getAccess_token());
                SignUpUserRes signUpUserRes = userManager.signUpUser(signUpUserReq, request, response);
                userPojo = signUpUserRes.getUser();

        }
        else{

            userPojo  = pojoUtils.convertToUserPojo(user);
        }



        logger.info("authenticateUser - user - userAllowedToTakeOnboarding - {}", userPojo.isUserInProcessOfOnboarding());
    userPojo.setIsEmailConflict(isEmailConflicted);
        logger.info("authenticateUser - user - user data received for onboarding - {}",userPojo.isUserInProcessOfOnboarding());
        HttpSessionData sessionData = sessionUtils.setCookieAndHeaders(request, response, userPojo);
        return sessionData;
    }

    public PaytmAccessTokenResp getAccessToken(PaytmSignInReq requestBody) throws VException, IOException, URISyntaxException {
        Map<String, String> headers = new HashMap<>();
        MultivaluedMap formData = new MultivaluedMapImpl();
        formData.add("grant_type","authorization_code");
        formData.add("code",requestBody.getAuthCode());
        formData.add("client_id",CLIENT_ID);
        formData.add("scope","basic");
        headers.put("Authorization",CLIENT_SECRET);
        //      headers.put("Content-Type","application/x-www-form-urlencoded");

//        headers.put("Content-Length", Integer.toString(reqString.length() ));
        ClientResponse response = WebUtils.INSTANCE.doPostFormEncoded(PAYTM_ACCOUNT_URL + "/oauth2/v2/token",  formData,headers);
        VExceptionFactory.INSTANCE.parseAndThrowException(response);

        String jsonString = response.getEntity(String.class);
        PaytmAccessTokenResp accessTokenReq = new Gson().fromJson(jsonString, PaytmAccessTokenResp.class);
        return accessTokenReq;
    }

    public PaytmUserProfileResp getUserInfo(PaytmAccessTokenResp req) throws VException, IOException, URISyntaxException {
       Map<String, String> headers = new HashMap<>();


        headers.put("Authorization",CLIENT_SECRET);
        headers.put("verification_type", "oauth_token");
        headers.put("data", req.getAccess_token());


        ClientResponse response = WebUtils.INSTANCE.doCall(PAYTM_ACCOUNT_URL +"/v2/user?fetch_strategy=profile_info,phone_number,email", HttpMethod.GET,null,  headers);
        VExceptionFactory.INSTANCE.parseAndThrowException(response);

        String jsonString = response.getEntity(String.class);
        PaytmUserProfileResp paytmAccessTokenResp = new Gson().fromJson(jsonString, PaytmUserProfileResp.class);
        return paytmAccessTokenResp;
    }




    }
