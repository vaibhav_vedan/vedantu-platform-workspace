package com.vedantu.user.managers;

import com.google.gson.Gson;
import com.mongodb.MongoWriteException;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.async.AsyncTaskFactory;
import com.vedantu.async.AsyncTaskParams;
import com.vedantu.exception.VException;
import com.vedantu.exception.VExceptionFactory;
import com.vedantu.lms.response.VGroupBasicResponse;
import com.vedantu.user.async.AsyncTaskName;
import com.vedantu.user.dao.GAIdDAO;
import com.vedantu.user.dao.RedisDAO;
import com.vedantu.user.dao.UserDAO;
import com.vedantu.user.entity.GAId;
import com.vedantu.user.entity.User;
import com.vedantu.user.responses.GAIdResponse;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.WebUtils;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 *
 * @author Ausaf
 */
@Service
public class GAIdManager
{
    @Autowired
    private LogFactory logFactory;

    private Logger logger = logFactory.getLogger(GAIdManager.class);

    @Autowired
    private GAIdDAO gaIdDao;

    @Autowired
    private UserDAO userDAO;

    @Autowired
    private AsyncTaskFactory asyncTaskFactory;

    @Autowired
    private RedisDAO redisDAO;

    private final String LMS_ENDPOINT = ConfigUtils.INSTANCE.getStringValue("LMS_ENDPOINT");

    public GAIdResponse checkIfLoggingFirstTime(String userId, String gaid, String appVersion)
    {
        logger.info("method : checkIfLoggingFirstTime, userId : {}, gaid : {}, appVersion : {}", userId, gaid, appVersion);
        if (!StringUtils.isEmpty(userId) && !StringUtils.isEmpty(gaid) && !StringUtils.isEmpty(appVersion))
            return saveGaid(userId, gaid, appVersion);
        return null;
    }

    public GAIdResponse saveGaid(String userId, String gaId, String appVersion)
    {
        GAIdResponse gaIdResponse = new GAIdResponse();

        if (!StringUtils.isEmpty(userId) && !StringUtils.isEmpty(gaId) && !StringUtils.isEmpty(appVersion))
        {
            GAId gaid = gaIdDao.findByUserId(userId, null);
            logger.info("GA_Id fetched from db : {}", gaid);
            if (gaid == null) {
                gaid = new GAId();
                Set<String> gaids = new HashSet<>();
                gaids.add(gaId);
                gaid.setGaids(gaids);
                gaid.setUserId(userId);
                Map<String, String> appVersionMap = new HashMap<>();
                appVersionMap.put(gaId, appVersion);
                gaid.setAppVersionMap(appVersionMap);
                gaIdResponse.setFirstTimeInstall(true);

//                try {
//                    List<User> users = userDAO.getUserBasicInfoByIds(Collections.singletonList(Long.parseLong(userId)), false);
//
//                    logger.info("User fetched : {}", users);
//
//                    if (users != null && !users.isEmpty()) {
//                        String grade = users.get(0).getStudentInfo().getGrade();
//
//                        Map<String, Object> vgroupMap = new HashMap<>();
//                        vgroupMap.put("userId", userId);
//                        vgroupMap.put("grade", grade);
//                        AsyncTaskParams asyncTaskParams = new AsyncTaskParams(AsyncTaskName.REGISTER_USER_VGROUP, vgroupMap);
//                        asyncTaskFactory.executeTask(asyncTaskParams);
//                    }
//                }
//                catch (Exception e)
//                {
//                    logger.info("Couldn't initiate VGroup registration for user : {} due to : {}", userId, e.toString());
//                }
            }
            else
            {
                gaIdResponse.setFirstTimeInstall(false);
                if (gaid.getGaids() == null)
                    gaid.setGaids(new HashSet<>());

                if (gaid.getAppVersionMap() == null)
                    gaid.setAppVersionMap(new HashMap<>());

                if (gaid.getGaids().contains(gaId))
                    gaIdResponse.setAdIdRegistered(true);
                else
                {
                    gaid.getGaids().add(gaId);
                    gaid.getAppVersionMap().put(gaId, appVersion);
                }
            }

            try {
                if (!gaIdResponse.isAdIdRegistered()) {
                    logger.info("Saving GA_Id in db : {}", gaid);
                    gaIdDao.save(gaid);
                }
            }
            catch (Exception e)
            {
                //
            }
        }
        else
        {
            logger.error("Invalid GA_Id request, userId : {}, ga_id : {}, appVersion : {}", userId, gaId, appVersion);
            gaIdResponse.setAdIdRegistered(true);
        }
        return gaIdResponse;
    }

    public VGroupBasicResponse registerUserInVGroups(String userId, String grade)
    {
        VGroupBasicResponse vGroupBasicResponse = new VGroupBasicResponse();
        try
        {
                logger.info("method : registerUserInVGroups for userId : {}, grade : {}", userId, grade);
                String url = LMS_ENDPOINT + "VChat/registerUserInVGroups?userId=" + userId + "&grade=" + grade;
                ClientResponse response = WebUtils.INSTANCE.doCall(url, HttpMethod.GET, null, true);
                VExceptionFactory.INSTANCE.parseAndThrowException(response);
                String resString = response.getEntity(String.class);
                vGroupBasicResponse = new Gson().fromJson(resString, VGroupBasicResponse.class);
                logger.info(vGroupBasicResponse);
                return vGroupBasicResponse;
        }
        catch (VException e) {
            //
        }

        vGroupBasicResponse.setResponse("Couldn't register user in VGroups.");
        vGroupBasicResponse.setStatus(HttpStatus.INTERNAL_SERVER_ERROR);
        return vGroupBasicResponse;
    }

	public String appRegistrationCreationTime(String userId, String gaid) throws VException{
		
		String creationTime = "";
		GAId gaId =  gaIdDao.findByUserIdAndGaid(userId, gaid);
		logger.debug("gaId in isAppRegisteredForUser -> "+gaId);
		if(null != gaId && null != gaId.getCreationTime()) {
			creationTime = gaId.getCreationTime().toString();
			logger.debug("creationTime in isAppRegisteredForUser -> "+creationTime);
		}
		return creationTime;
		
		
	}
}
