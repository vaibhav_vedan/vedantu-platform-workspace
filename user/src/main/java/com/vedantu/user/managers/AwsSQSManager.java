/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.user.managers;

import com.amazonaws.services.sqs.model.CreateQueueRequest;
import com.amazonaws.services.sqs.model.GetQueueAttributesRequest;
import com.amazonaws.services.sqs.model.GetQueueAttributesResult;
import com.amazonaws.services.sqs.model.QueueAttributeName;
import com.amazonaws.services.sqs.model.SetQueueAttributesRequest;
import com.vedantu.aws.AbstractAwsSQSManagerNew;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.enums.SQSQueue;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;


@Service
public class AwsSQSManager extends AbstractAwsSQSManagerNew {

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(AwsSQSManager.class);

    private String env;

    public AwsSQSManager() {
        super();
        logger.info("initializing AwsSQSManager - User");
        try {
            env = ConfigUtils.INSTANCE.getStringValue("environment");
            if (!(StringUtils.isEmpty(env) || env.equals("LOCAL"))) {
                CreateQueueRequest cqr = new CreateQueueRequest(SQSQueue.NOTIFICATION_QUEUE
                        .getQueueName(env));
                //cqr.addAttributesEntry(QueueAttributeName.ContentBasedDeduplication.name(), "true");
                sqsClient.createQueue(cqr);
                logger.info("created queue for notification");
                
                /*CreateQueueRequest cleverTapQueue = new CreateQueueRequest(SQSQueue.CLEVERTAP_QUEUE
                        .getQueueName(env));
                cleverTapQueue.addAttributesEntry(QueueAttributeName.VisibilityTimeout.name(), SQSQueue.CLEVERTAP_QUEUE.getVisibilityTimeout());
                sqsClient.createQueue(cleverTapQueue);
                logger.info("created queue:" + SQSQueue.CLEVERTAP_QUEUE.getQueueName(env));*/

                logger.info("creating queue for user city update");
                CreateQueueRequest userCityUpdateQueue = new CreateQueueRequest(SQSQueue.USER_CITY_UPDATE_QUEUE.getQueueName(env));
                userCityUpdateQueue.addAttributesEntry(QueueAttributeName.VisibilityTimeout.name(), SQSQueue.USER_CITY_UPDATE_QUEUE.getVisibilityTimeout());
                sqsClient.createQueue(userCityUpdateQueue);
                logger.info("created queue for user Update");

                logger.info("creating dead letter queue user city update");
                CreateQueueRequest userCityUpdateDl = new CreateQueueRequest(SQSQueue.USER_CITY_UPDATE_DL_QUEUE
                        .getQueueName(env));
                sqsClient.createQueue(userCityUpdateDl);
                logger.info("dead letter queue for user city update created" + SQSQueue.USER_CITY_UPDATE_DL_QUEUE.getQueueName(env));

                assignDeadLetterQueue(SQSQueue.USER_CITY_UPDATE_QUEUE.getQueueName(env), SQSQueue.USER_CITY_UPDATE_DL_QUEUE
                        .getQueueName(env), 4);


                logger.info("creating queue for user signup tasks : "+SQSQueue.SIGNUP_TASKS_QUEUE.getQueueName(env));
                CreateQueueRequest userSignUpTasksQueue = new CreateQueueRequest(SQSQueue.SIGNUP_TASKS_QUEUE.getQueueName(env));
                userSignUpTasksQueue.addAttributesEntry(QueueAttributeName.FifoQueue.name(), "true");
                userSignUpTasksQueue.addAttributesEntry(QueueAttributeName.VisibilityTimeout.name(), SQSQueue.SIGNUP_TASKS_QUEUE.getVisibilityTimeout());
                sqsClient.createQueue(userSignUpTasksQueue);
                logger.info("created queue for user signup tasks : "+SQSQueue.SIGNUP_TASKS_QUEUE.getQueueName(env));

                logger.info("creating dead letter queue user signup tasks : "+SQSQueue.SIGNUP_TASKS_DL_QUEUE.getQueueName(env));
                CreateQueueRequest userSignUpTasksQueueDl = new CreateQueueRequest(SQSQueue.SIGNUP_TASKS_DL_QUEUE.getQueueName(env));
                userSignUpTasksQueueDl.addAttributesEntry(QueueAttributeName.FifoQueue.name(), "true");
                sqsClient.createQueue(userSignUpTasksQueueDl);
                logger.info("dead letter queue for user signup tasks created" + SQSQueue.SIGNUP_TASKS_DL_QUEUE.getQueueName(env));

                assignDeadLetterQueue(SQSQueue.SIGNUP_TASKS_QUEUE.getQueueName(env), SQSQueue.SIGNUP_TASKS_DL_QUEUE.getQueueName(env), 4);


                logger.info("creating queue for VQuiz Referral  : "+SQSQueue.VQUIZ_REFERRAL_QUEUE.getQueueName(env));
                CreateQueueRequest vQuizReferralQueue = new CreateQueueRequest(SQSQueue.VQUIZ_REFERRAL_QUEUE.getQueueName(env));
//                vQuizReferralQueue.addAttributesEntry(QueueAttributeName.FifoQueue.name(), "true");
                vQuizReferralQueue.addAttributesEntry(QueueAttributeName.VisibilityTimeout.name(), SQSQueue.VQUIZ_REFERRAL_QUEUE.getVisibilityTimeout());
                sqsClient.createQueue(vQuizReferralQueue);
                logger.info("created queue for VQuiz Refferral : "+SQSQueue.VQUIZ_REFERRAL_QUEUE.getQueueName(env));

                logger.info("creating dead letter queue VQuiz Referral : "+SQSQueue.VQUIZ_REFERRAL_DL_QUEUE.getQueueName(env));
                CreateQueueRequest vQuizReferralQueueDl = new CreateQueueRequest(SQSQueue.VQUIZ_REFERRAL_DL_QUEUE.getQueueName(env));
//                vQuizReferralQueueDl.addAttributesEntry(QueueAttributeName.FifoQueue.name(), "true");
                sqsClient.createQueue(vQuizReferralQueueDl);
                logger.info("dead letter queue for vquiz referral queue created" + SQSQueue.VQUIZ_REFERRAL_DL_QUEUE.getQueueName(env));

                assignDeadLetterQueue(SQSQueue.VQUIZ_REFERRAL_QUEUE.getQueueName(env), SQSQueue.VQUIZ_REFERRAL_DL_QUEUE.getQueueName(env), 4);



                logger.info("creating queue for VQuiz Reminder  : "+SQSQueue.VQUIZ_REMINDER_QUEUE.getQueueName(env));
                CreateQueueRequest vQuizReminderQueue = new CreateQueueRequest(SQSQueue.VQUIZ_REMINDER_QUEUE.getQueueName(env));
                vQuizReminderQueue.addAttributesEntry(QueueAttributeName.VisibilityTimeout.name(), SQSQueue.VQUIZ_REMINDER_QUEUE.getVisibilityTimeout());
                sqsClient.createQueue(vQuizReminderQueue);
                logger.info("created queue for VQuiz Reminder : "+SQSQueue.VQUIZ_REMINDER_QUEUE.getQueueName(env));

                logger.info("creating dead letter queue VQuiz Reminder : "+SQSQueue.VQUIZ_REMINDER_DL_QUEUE.getQueueName(env));
                CreateQueueRequest vQuizReminderQueueDl = new CreateQueueRequest(SQSQueue.VQUIZ_REMINDER_DL_QUEUE.getQueueName(env));
                sqsClient.createQueue(vQuizReminderQueueDl);
                logger.info("dead letter queue for vquiz reminder queue created" + SQSQueue.VQUIZ_REMINDER_DL_QUEUE.getQueueName(env));

                assignDeadLetterQueue(SQSQueue.VQUIZ_REMINDER_QUEUE.getQueueName(env), SQSQueue.VQUIZ_REMINDER_DL_QUEUE.getQueueName(env), 1);


                logger.info("creating dead letter queue prelogin data : "+SQSQueue.PRE_LOGIN_DATA_QUEUE_DL.getQueueName(env));
                CreateQueueRequest userPreLoginDataQueueDl = new CreateQueueRequest(SQSQueue.PRE_LOGIN_DATA_QUEUE_DL.getQueueName(env));
                sqsClient.createQueue(userPreLoginDataQueueDl);
                logger.info("dead letter queue for user prelogin data created" + SQSQueue.PRE_LOGIN_DATA_QUEUE_DL.getQueueName(env));

                logger.info("creating queue for user prelogin data : "+SQSQueue.PRE_LOGIN_DATA_QUEUE.getQueueName(env));
                CreateQueueRequest userPreLoginDataQueue = new CreateQueueRequest(SQSQueue.PRE_LOGIN_DATA_QUEUE.getQueueName(env));
                userPreLoginDataQueue.addAttributesEntry(QueueAttributeName.VisibilityTimeout.name(), SQSQueue.PRE_LOGIN_DATA_QUEUE.getVisibilityTimeout());
                sqsClient.createQueue(userPreLoginDataQueue);
                logger.info("created queue for user prelogin data : "+SQSQueue.PRE_LOGIN_DATA_QUEUE.getQueueName(env));


                assignDeadLetterQueue(SQSQueue.PRE_LOGIN_DATA_QUEUE.getQueueName(env), SQSQueue.PRE_LOGIN_DATA_QUEUE_DL.getQueueName(env), 1);

            }
        } catch (Exception e) {
            logger.error("Error in initializing AwsSQSManager " + e.getMessage());
        }

    }

    @PostConstruct
    public void postQueueCreation(){

    }

    private void assignDeadLetterQueue(String src_queue_name, String dl_queue_name, Integer maxReceive) {
        if (maxReceive == null) {
            maxReceive = 4;
        }
        // Get dead-letter queue ARN
        String dl_queue_url = sqsClient.getQueueUrl(dl_queue_name)
                .getQueueUrl();

        GetQueueAttributesResult queue_attrs = sqsClient.getQueueAttributes(
                new GetQueueAttributesRequest(dl_queue_url)
                        .withAttributeNames("QueueArn"));

        String dl_queue_arn = queue_attrs.getAttributes().get("QueueArn");

        // Set dead letter queue with redrive policy on source queue.
        String src_queue_url = sqsClient.getQueueUrl(src_queue_name)
                .getQueueUrl();

        SetQueueAttributesRequest request = new SetQueueAttributesRequest()
                .withQueueUrl(src_queue_url)
                .addAttributesEntry("RedrivePolicy",
                        "{\"maxReceiveCount\":\"" + maxReceive + "\", \"deadLetterTargetArn\":\""
                                + dl_queue_arn + "\"}");

        sqsClient.setQueueAttributes(request);
        logger.info("assigned dead letter queue: " + dl_queue_name + "  to:" + src_queue_name);

    }

}
