/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.user.managers;

import static java.time.ZoneOffset.UTC;
import static java.util.stream.Collectors.toList;
import static org.apache.commons.lang3.ObjectUtils.defaultIfNull;
import static org.apache.commons.lang3.StringEscapeUtils.escapeCsv;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.security.KeyFactory;
import java.security.PublicKey;
import java.security.Signature;
import java.security.spec.X509EncodedKeySpec;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Random;
import java.util.Set;
import java.util.UUID;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import javax.annotation.PreDestroy;
import javax.mail.internet.InternetAddress;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.DatatypeConverter;

import com.vedantu.user.dao.HomePageUserBucketDAO;
import com.vedantu.user.entity.HomePageUserBucket;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.Logger;
import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.http.HttpMethod;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.amazonaws.regions.Regions;
import com.amazonaws.services.sns.AmazonSNSAsync;
import com.amazonaws.services.sns.AmazonSNSAsyncClientBuilder;
import com.amazonaws.services.sns.model.PublishRequest;
import com.amazonaws.services.sns.model.PublishResult;
import com.google.api.client.util.Base64;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.User.CountryToPhonePrefix;
import com.vedantu.User.DexInfo;
import com.vedantu.User.FeatureSource;
import com.vedantu.User.LocationInfo;
import com.vedantu.User.Role;
import com.vedantu.User.SaleClosed;
import com.vedantu.User.SaleClosedUser;
import com.vedantu.User.StudentInfo;
import com.vedantu.User.TeacherBasicInfo;
import com.vedantu.User.TeacherInfo;
import com.vedantu.User.UserBasicInfo;
import com.vedantu.User.UserDetailsForSNS;
import com.vedantu.User.UserEvents;
import com.vedantu.User.UserInfo;
import com.vedantu.User.VerificationLinkStatus;
import com.vedantu.User.VerificationTokenType;
import com.vedantu.User.enums.EntityModel;
import com.vedantu.User.enums.VoltStatus;
import com.vedantu.User.request.AcceptTncReq;
import com.vedantu.User.request.AddOrUpdatePhoneNumberReq;
import com.vedantu.User.request.AddUserAddressReq;
import com.vedantu.User.request.AddUserSystemIntroReq;
import com.vedantu.User.request.ChangePasswordReq;
import com.vedantu.User.request.CreateUserAuthenticationTokenReq;
import com.vedantu.User.request.EditContactNumberReq;
import com.vedantu.User.request.EditProfileReq;
import com.vedantu.User.request.EditUserDetailsReq;
import com.vedantu.User.request.ExistingUserISLRegReq;
import com.vedantu.User.request.ExtensionNumberReq;
import com.vedantu.User.request.ForgotPasswordReq;
import com.vedantu.User.request.GeneratePasswordReq;
import com.vedantu.User.request.GetActiveTutorsReq;
import com.vedantu.User.request.GetDexTeachersReq;
import com.vedantu.User.request.GetNonUsersReq;
import com.vedantu.User.request.GetOTPReq;
import com.vedantu.User.request.GetUserInfosByEmailReq;
import com.vedantu.User.request.GetUserSystemIntroReq;
import com.vedantu.User.request.GetUsersReq;
import com.vedantu.User.request.LeadSquaredRequest;
import com.vedantu.User.request.ManualResetPasswordReq;
import com.vedantu.User.request.ReSendContactNumberVerificationCodeReq;
import com.vedantu.User.request.RegistrationEmailReq;
import com.vedantu.User.request.ResetPasswordReq;
import com.vedantu.User.request.SetBlockStatusForUserReq;
import com.vedantu.User.request.SetProfilePicReq;
import com.vedantu.User.request.SignInByTokenReq;
import com.vedantu.User.request.SignInReq;
import com.vedantu.User.request.SignUpForISLReq;
import com.vedantu.User.request.SignUpReq;
import com.vedantu.User.request.VerifyContactNumberReq;
import com.vedantu.User.response.CreateUserAuthenticationTokenRes;
import com.vedantu.User.response.CreateUserResponse;
import com.vedantu.User.response.EditContactNumberRes;
import com.vedantu.User.response.EditProfileRes;
import com.vedantu.User.response.GeneratePasswordRes;
import com.vedantu.User.response.GetActiveTutorsRes;
import com.vedantu.User.response.GetUserPhoneNumbersRes;
import com.vedantu.User.response.GetUserProfileRes;
import com.vedantu.User.response.GetUserSystemIntroDataListRes;
import com.vedantu.User.response.GetUsersRes;
import com.vedantu.User.response.IsRegisteredForISLRes;
import com.vedantu.User.response.PhoneNumberRes;
import com.vedantu.User.response.ProcessVerifyContactNumberResponse;
import com.vedantu.User.response.ReSendContactNumberVerificationCodeRes;
import com.vedantu.User.response.ReferralInfoRes;
import com.vedantu.User.response.SignUpForISLRes;
import com.vedantu.User.response.StatusCounterResponse;
import com.vedantu.User.response.TeacherListWithStatusRes;
import com.vedantu.User.response.UserBasicResponse;
import com.vedantu.User.response.UserDetailsInfo;
import com.vedantu.User.response.UserSystemIntroRes;
import com.vedantu.async.AsyncTaskFactory;
import com.vedantu.async.AsyncTaskParams;
import com.vedantu.dinero.pojo.OrderedItem;
import com.vedantu.dinero.pojo.Orders;
import com.vedantu.dinero.response.GetUserDashboardAccountInfoRes;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ConflictException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.ForbiddenException;
import com.vedantu.exception.InternalServerErrorException;
import com.vedantu.exception.NotFoundException;
import com.vedantu.exception.UnauthorizedException;
import com.vedantu.exception.VException;
import com.vedantu.exception.VExceptionFactory;
import com.vedantu.lms.cmds.pojo.AttemptedUserIds;
import com.vedantu.notification.enums.CommunicationType;
import com.vedantu.notification.enums.EmailPriorityType;
import com.vedantu.notification.enums.EmailService;
import com.vedantu.notification.requests.EmailRequest;
import com.vedantu.notification.requests.TextSMSRequest;
import com.vedantu.scheduling.response.session.GetUserDashboardSessionInfoRes;
import com.vedantu.session.pojo.EntityType;
import com.vedantu.subscription.enums.EarlyLearningCourseType;
import com.vedantu.subscription.request.PremiumSubscriptionRequest;
import com.vedantu.user.async.AsyncTaskName;
import com.vedantu.user.dao.CounterService;
import com.vedantu.user.dao.HomePageUserBucketDAO;
import com.vedantu.user.dao.OrgDAO;
import com.vedantu.user.dao.RedisDAO;
import com.vedantu.user.dao.UserDAO;
import com.vedantu.user.dao.UserDetailsDAO;
import com.vedantu.user.dao.UserHistoryDAO;
import com.vedantu.user.dao.UserIspDataDao;
import com.vedantu.user.dao.UserLoginDataDao;
import com.vedantu.user.dao.UserLoginTokenDAO;
import com.vedantu.user.dao.UserSystemIntroDataDAO;
import com.vedantu.user.dao.VerificationTokenDAO;
import com.vedantu.user.entity.HomePageUserBucket;
import com.vedantu.user.entity.Org;
import com.vedantu.user.entity.PhoneNumber;
import com.vedantu.user.entity.TruecallerEntity;
import com.vedantu.user.entity.User;
import com.vedantu.user.entity.UserDetails;
import com.vedantu.user.entity.UserHistoryII;
import com.vedantu.user.entity.UserLoginToken;
import com.vedantu.user.entity.UserSystemIntroData;
import com.vedantu.user.entity.VerificationTokenII;
import com.vedantu.user.entity.temp.LoginData;
import com.vedantu.user.enums.CouponType;
import com.vedantu.user.enums.ReferralStep;
import com.vedantu.user.enums.VerifiedType;
import com.vedantu.user.helper.BotFilter;
import com.vedantu.user.pojo.MemberCountPojo;
import com.vedantu.user.pojo.TeachersTagsUpdateReq;
import com.vedantu.user.pojo.UserCityUpdate;
import com.vedantu.user.pojo.UserPreLoginDataPojo;
import com.vedantu.user.requests.CreateUserDetailsReq;
import com.vedantu.user.requests.GetUserDashboardReq;
import com.vedantu.user.requests.GetVoltUserDetailsReq;
import com.vedantu.user.requests.GetXVedTokenByUserDetailsReq;
import com.vedantu.user.requests.NewPhoneVerificationReq;
import com.vedantu.user.requests.OTPVerificationReq;
import com.vedantu.user.requests.ProcessCouponReq;
import com.vedantu.user.requests.ProcessReferralBonusReq;
import com.vedantu.user.requests.ProfileBuilderPostLoginReq;
import com.vedantu.user.requests.SignUpUserReq;
import com.vedantu.user.requests.TruecallerLoginReq;
import com.vedantu.user.requests.UserAddDummyNumberReq;
import com.vedantu.user.requests.UserDetailsVoltApproveReq;
import com.vedantu.user.requests.UserEmailUpdateReq;
import com.vedantu.user.requests.UserPreLoginVerificationInfoReq;
import com.vedantu.user.requests.UserPreSignUpVerificationInfoReq;
import com.vedantu.user.responses.GetDexAndTheirStatusRes;
import com.vedantu.user.responses.GetTestAttemptRes;
import com.vedantu.user.responses.GetUserDashboardRes;
import com.vedantu.user.responses.NewPhoneVerificationRes;
import com.vedantu.user.responses.ProfileBuilderPostLoginRes;
import com.vedantu.user.responses.SignUpUserRes;
import com.vedantu.user.responses.TeacherInfoMinimal;
import com.vedantu.user.responses.TruecallerLoginRes;
import com.vedantu.user.responses.UserParentInfoRes;
import com.vedantu.user.responses.UserPreLoginVerificationInfoRes;
import com.vedantu.user.responses.UserPreSignUpVerificationInfoRes;
import com.vedantu.user.utils.PojoUtils;
import com.vedantu.user.utils.TestAccontPasswordFactory;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.CollectionUtils;
import com.vedantu.util.CommonUtils;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.Constants;
import com.vedantu.util.CustomValidator;
import com.vedantu.util.DateTimeUtils;
import com.vedantu.util.FosUtils;
import com.vedantu.util.IPUtil;
import com.vedantu.util.LogFactory;
import com.vedantu.util.PlatformBasicResponse;
import com.vedantu.util.StringUtils;
import com.vedantu.util.WebUtils;
import com.vedantu.util.dbentities.mongo.AbstractTargetTopicEntity;
import com.vedantu.util.enums.LeadSquaredAction;
import com.vedantu.util.enums.LeadSquaredDataType;
import com.vedantu.util.enums.LocationAddedBy;
import com.vedantu.util.enums.RequestSource;
import com.vedantu.util.enums.SQSMessageType;
import com.vedantu.util.enums.SQSQueue;
import com.vedantu.util.pojo.VsatEventDetailsPojo;
import com.vedantu.util.redis.RateLimitingRedisDao;
import com.vedantu.util.security.HttpSessionData;
import com.vedantu.util.security.HttpSessionUtils;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.Logger;
import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.http.HttpMethod;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.annotation.PreDestroy;
import javax.mail.internet.InternetAddress;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.DatatypeConverter;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.security.KeyFactory;
import java.security.PublicKey;
import java.security.Signature;
import java.security.spec.X509EncodedKeySpec;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Random;
import java.util.Set;
import java.util.UUID;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static java.time.ZoneOffset.UTC;
import static java.util.stream.Collectors.toList;
import static org.apache.commons.lang3.ObjectUtils.defaultIfNull;
import static org.apache.commons.lang3.StringEscapeUtils.escapeCsv;


/**
 * @author somil
 */
@Service
public class UserManager {

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(UserManager.class);

    @Autowired
    public UserDAO userDAO;

    @Autowired
    private CounterService counterService;

    @Autowired
    public UserHistoryDAO userHistoryDAO;

    @Autowired
    public VerificationTokenManager tokenManager;

    @Autowired
    public UserLoginTokenDAO userLoginTokenDAO;

    @Autowired
    public UserSystemIntroDataDAO userSystemIntroDataDAO;

    @Autowired
    public PojoUtils pojoUtils;

    @Autowired
    public PasswordEncoder passwordEncoder;

    @Autowired
    public UserDetailsDAO userDetailsDAO;

    @Autowired
    private AsyncTaskFactory asyncTaskFactory;

    @Autowired
    public VerificationTokenDAO verificationTokenDAO;

    @Autowired
    public AwsSQSManager awsSQSManager;

    @Autowired
    public RedisDAO redisDAO;

    @Autowired
    private FosUtils fosUtils;

    @Autowired
    HttpSessionUtils sessionUtils;

    @Autowired
    private IPUtil ipUtil;

    @Autowired
    private AwsS3Manager awsS3Manager;

    @Autowired
    public CommunicationManager communicationManager;

    @Autowired
    private UserLoginDataDao loginDataDao;

    @Autowired
    private RateLimitingRedisDao rateLimitingRedisDao;

    @Autowired
    private UserPasswordManager userPasswordManager;

    @Autowired
    private OrgDAO orgDAO;

    @Autowired
    private BotFilter botFilter;

    @Autowired
    private UserIspDataDao ispDataDao;

    @Autowired
    private HomePageUserBucketDAO homePageUserBucketDAO;

    private final String dineroEndpoint = ConfigUtils.INSTANCE.getStringValue("DINERO_ENDPOINT");

    private static Integer limit = Integer.parseInt(ConfigUtils.INSTANCE.properties.getProperty("otp.login.limit"));
    private static Integer preLoginLimit = Integer.parseInt(ConfigUtils.INSTANCE.properties.getProperty("otp.preLogin.limit"));

    private String SALT;

    public static String arn;
    public static String env;
    public static Integer redisExpiry;

    private static List<String> allowedCountriesList = new ArrayList(Arrays.asList("India", "Saudi Arabia", "singapore", "United Arab Emirates", "Kuwait", "Oman", "Qatar", "Bahrain",  "Nigeria", "New Zealand", "Kazakhstan"));

    @Autowired
    private DozerBeanMapper mapper;
    private final Gson gson = new Gson();

    private static final String DINERO_ENDPOINT = ConfigUtils.INSTANCE.getStringValue("DINERO_ENDPOINT");
    private static final String SCHEDULING_ENDPOINT = ConfigUtils.INSTANCE.getStringValue("SCHEDULING_ENDPOINT");
    private static final Set<EntityType> entityTypes = new HashSet<>(Arrays.asList(EntityType.OTF_BATCH_REGISTRATION,
            EntityType.OTF_BATCH_REGISTRATION, EntityType.COURSE_PLAN_REGISTRATION, EntityType.OTO_COURSE_REGISTRATION,
            EntityType.OTM_BUNDLE_REGISTRATION, EntityType.BUNDLE_TRIAL));

    private static final int DUMMY_NUMBER_LENGTH = 8;

    private final AmazonSNSAsync snsClient;

    private static Pattern pattern;
    static final String phoneRegex = "^[0-9]{6,14}$";
    static final String phoneCodeRegex = "^[0-9]{1,3}$";
    private static final List<String> resetPasswordExceptions = new ArrayList<>(Arrays.asList(ConfigUtils.INSTANCE.getStringValue("user.resetPasswordExceptions").split(","))).stream()
            .map(e -> {
                return e.toLowerCase().trim();
            })
            .collect(Collectors.toList());
    private static final Set<String> restrictedResetPasswordAccounts = Arrays.stream(ConfigUtils.INSTANCE.getStringValue("user.restrict.reset.password")
            .split(","))
            .filter(Objects::nonNull)
            .map(String::trim)
            .filter(e -> !e.isEmpty())
            .map(String::toLowerCase)
            .collect(Collectors.toSet());

    public static final long ONE_DAY_IN_MILLIS = 86400000;
    public static final long TWO_HOUR_IN_MILLIS = 7200000;
    private static final String EARLY_LEARNING_TEACHERS_SUPER_CODER = "EARLY_LEARNING_TEACHERS_SUPER_CODER";
    private static final String EARLY_LEARNING_TEACHERS_SUPER_READER = "EARLY_LEARNING_TEACHERS_SUPER_READER";
    private static final String SUBSCRIPTION_ENDPOINT = ConfigUtils.INSTANCE.getStringValue("SUBSCRIPTION_ENDPOINT");
    private static final String IS_SUBSCRIPTION_ACTIVE ="ISSUBSCRIPTIONACTIVE";
    private static final String AUTOSALES_PB_ON_APP_ALLOWED = "AUTOSALES_PB_ON_APP_ALLOWED";
    private static final String HOMEPAGE_BUCKET_COUNTER_KEY = "HOMEPAGE_BUCKET_COUNTER";
    private static final int NEW_HOMEPAGE_BUCKETS = 2;

    public UserManager() {
        SALT = ConfigUtils.INSTANCE.getStringValue("user.referral.salt");
        env = ConfigUtils.INSTANCE.getStringValue("environment");
        arn = ConfigUtils.INSTANCE.getStringValue("aws.arn");
        redisExpiry = ConfigUtils.INSTANCE.getIntValue("redis.setExpiry");
        snsClient = AmazonSNSAsyncClientBuilder.standard()
                .withRegion(Regions.AP_SOUTHEAST_1)
                .build();
    }

    public CreateUserResponse createUser(SignUpReq signUpReq) throws Exception {
        signUpReq.verify();

        if (StringUtils.isEmpty(signUpReq.getContactNumber())) {
            Long phone = counterService.getNextSequence(User.Constants.DUMMY_NUMBER, 1);
            signUpReq.setContactNumber(String.valueOf(phone));
        }

        if (StringUtils.isNotEmpty(signUpReq.getContactNumber())) {
            if (StringUtils.isEmpty(signUpReq.getPhoneCode())) {
                signUpReq.setPhoneCode("91");
            }
            User phoneUser = userDAO.getUserByPhoneNumberAndCode(signUpReq.getContactNumber(), signUpReq.getPhoneCode());
            if (null != phoneUser) {
                throw new ConflictException(ErrorCode.USER_ALREADY_EXISTS, "User already exists with phone[" + signUpReq.getContactNumber()
                        + "]");
            }
        }

        if (StringUtils.isNotEmpty(signUpReq.getFirstName()) && signUpReq.getFirstName().equals("dsfsdfsdf")) {
            throw new ConflictException(ErrorCode.USER_ALREADY_EXISTS, "User already exists with email[" + signUpReq.getEmail()
                    + "]");
        }

        // Mobile App related experiment
        if (signUpReq.getEmail().contains("-app@vedantu.com")) {
            if (StringUtils.isNotEmpty(signUpReq.getContactNumber())) {
                List<User> users = userDAO.getUsersByPhoneNumberAndCode(signUpReq.getContactNumber(), signUpReq.getContactNumber(), false);
                if (ArrayUtils.isNotEmpty(users)) {
                    throw new ConflictException(ErrorCode.USER_ALREADY_EXISTS, "User already exists with contact[" + signUpReq.getContactNumber()
                            + "]");
                }
            }
        }

        logger.info("Request for createUser " + signUpReq);
        Boolean hasPassword = !StringUtils.isEmpty(signUpReq.getPassword());
        User user = new User(signUpReq);
        // BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        // String hashedPassword = passwordEncoder.encode(user.getPassword());
        // user.setPassword(hashedPassword);

        if (Role.ADMIN.equals(signUpReq.getRole()) || Role.STUDENT_CARE.equals(signUpReq.getRole())) {
            List<String> passwordErrors = StringUtils.isValidAdminPassword(signUpReq.getPassword());
            if (ArrayUtils.isNotEmpty(passwordErrors)) {
                throw new BadRequestException(ErrorCode.INVALID_ADMIN_PASSWORD, passwordErrors.toString());
            }
        }

        if (StringUtils.isNotEmpty(signUpReq.getReferrer())) {
            user.setReferrer(signUpReq.getReferrer());
        }

        if (StringUtils.isNotEmpty(signUpReq.getSignUpURL())) {
            user.setSignUpURL(signUpReq.getSignUpURL());
        }

        if (StringUtils.isNotEmpty(user.getPassword())) {
            String hashedPassword = passwordEncoder.encode(user.getPassword());
            user.setPassword(hashedPassword);
            user.setPasswordHashed(true);
            user.setPasswordChangedAt(System.currentTimeMillis());
            user.addRecentHashedPassword(hashedPassword);
        }
        if (StringUtils.isEmpty(user.getPhoneCode())) {
            user.setPhoneCode("91");
        }

        if (user.getAppId() != null && user.getAppId() > 0) {
            user.setTncVersion("v7");
        }

        if (StringUtils.isNotEmpty(user.getContactNumber())) {
            PhoneNumber phoneNumber = new PhoneNumber(user.getContactNumber(), user.getPhoneCode(), false, false, false,
                    false);
            List<PhoneNumber> numbers = new ArrayList<>();
            numbers.add(phoneNumber);
            user.setPhones(numbers);
        }

        try {
            user.setReferralCode(getReferralCode(user));
        } catch (Exception ex) {
            logger.error("Error generating referral code for user " + user, ex);
        }

        if (signUpReq.getTeacherInfo() != null) {
            if (signUpReq.getTeacherInfo().getDexInfo() != null) {
                if (signUpReq.getTeacherInfo().getDexInfo().getTargetGrades() != null && signUpReq.getTeacherInfo().getDexInfo().gettTopics() != null) {
                    AbstractTargetTopicEntity atte = new AbstractTargetTopicEntity();
                    atte.setTargetGrades(signUpReq.getTeacherInfo().getDexInfo().getTargetGrades());
                    atte.settTopics(signUpReq.getTeacherInfo().getDexInfo().gettTopics());
                    AbstractTargetTopicEntity topicTargetTagsPojo = fosUtils.setAndGenerateTags(atte);
                    // If anything will be null setAndGenerateTags will throw error
                    if (topicTargetTagsPojo != null) {
                        signUpReq.getTeacherInfo().getDexInfo().setMainTags(topicTargetTagsPojo.getMainTags());
                        signUpReq.getTeacherInfo().getDexInfo().settSubjects(topicTargetTagsPojo.gettSubjects());
                        signUpReq.getTeacherInfo().getDexInfo().settTargets(topicTargetTagsPojo.gettTargets());
                        signUpReq.getTeacherInfo().getDexInfo().settGrades(topicTargetTagsPojo.gettGrades());
                        signUpReq.getTeacherInfo().getDexInfo().setTags(topicTargetTagsPojo);
                        user.getTeacherInfo().setDexInfo(signUpReq.getTeacherInfo().getDexInfo());
                    }
                }
            }
        }

        if(StringUtils.isNotEmpty(signUpReq.getOrgId())){
            Org org = orgDAO.getOrgById(signUpReq.getOrgId());
            if(org != null){
                user.setOrgId(signUpReq.getOrgId());
            }else{
                throw new BadRequestException(ErrorCode.ORG_NOT_FOUND, "org not found");
            }
        }
        userDAO.create(user, signUpReq.getCallingUserId());

        UserDetails userDetail = null;

        VsatEventDetailsPojo vsatEventDetailsPojo = fosUtils.getVsatEventDetails();

        if (user.getRole().equals(Role.STUDENT)
                && (FeatureSource.ISL.equals(user.getSignUpFeature())
                || FeatureSource.ISL_REGISTRATION_VIA_TOOLS.equals(user.getSignUpFeature())
                || FeatureSource.VSAT.equals(user.getSignUpFeature())
                || FeatureSource.VOLT_2020_JAN.equals(user.getSignUpFeature()))
                || FeatureSource.REVISEINDIA_2020_FEB.equals(user.getSignUpFeature())
                || FeatureSource.REVISE_JEE_2020_MARCH.equals(user.getSignUpFeature())
                || FeatureSource.TARGET_JEE_NEET.equals(user.getSignUpFeature())) {

            String event = null;
            switch (user.getSignUpFeature()) {
                case ISL_REGISTRATION_VIA_TOOLS:
                case ISL:
                    event = "ISL_2017";
                    break;
                case VSAT:
                    event = vsatEventDetailsPojo.getEventName();
                    break;
//                case JRP_SEP_2018:
//                    event = "JRP_SEP_2018";
//                    break;
                case REVISEINDIA_2020_FEB:
                    event = "REVISEINDIA_2020_FEB";
                    break;

                case VOLT_2020_JAN:
                    event = "VOLT_2020_JAN";
                    break;

                case REVISE_JEE_2020_MARCH:
                    event = "REVISE_JEE_2020_MARCH";
                    break;
                case TARGET_JEE_NEET:
                    event = "TARGET_JEE_NEET";
                    break;
            }

            userDetail = createUserDetailsForEvent(user, event,
                    signUpReq.getCallingUserId(), signUpReq.getAgentName(), signUpReq.getAgentEmail(), signUpReq.getExamName(), signUpReq.getVoltId(),
                    signUpReq.getAchievementDocments(), signUpReq.getAchievements(), signUpReq.getIdproofs(), signUpReq.getLocationInfo(), new HashMap<>());
        }

//      String mobileToken = tokenManager.getMobileToken(user.getId(), user.getContactNumber(), user.getPhoneCode(),
//                signUpReq.getCallingUserId());
        VerificationTokenType verificationTokenType = StringUtils.isEmpty(user.getSocialSource()) || !hasPassword
                ? VerificationTokenType.EMAIL_VERIFICATION
                : VerificationTokenType.EMAIL_VERIFICATION_SIGNUP;

        String emailToken = tokenManager.getEmailToken(user.getId(), user.getEmail(), verificationTokenType,
                signUpReq.getCallingUserId());

        CreateUserResponse response = new CreateUserResponse(pojoUtils.convertToUserPojo(user), null,
                emailToken);
        if (userDetail != null) {
            response.setUserDetailCreated(Boolean.TRUE);
        }

        if (user.getLocationInfo() == null) {
            try {
                String ipAddress = signUpReq.getIpAddress();
                if (ipAddress != null) {

                    if (StringUtils.isNotEmpty(ipAddress)) {
                        String[] ips = ipAddress.split(",");
                        logger.info("IPADDRESS: " + ips[0]);

                        UserCityUpdate userCityUpdate = new UserCityUpdate(user, ips[0],signUpReq.getCallingUserId());
                        awsSQSManager.sendToSQS(SQSQueue.USER_CITY_UPDATE_QUEUE, SQSMessageType.USER_CITY_UPDATE, new Gson().toJson(userCityUpdate));
//                        AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.USER_CITY_UPDATE, payload1);
//                        asyncTaskFactory.executeTask(params);

                    }
                }
            } catch (Exception e) {
                logger.warn(e);
            }
        }

        if (Role.TEACHER.equals(user.getRole())) {
            redisDAO.evictELTeachersKeys();
        }

        logger.info("Response of CreateUser" + response);
        return response;
    }

    private void addLocationFromIp(User user) {
        try {
            String ipAddress = user.getIpAddress();
            if (ipAddress != null) {
                if (StringUtils.isNotEmpty(ipAddress)) {
                    String[] ips = ipAddress.split(",");
                    logger.info("IPADDRESS: " + ips[0]);

                    UserCityUpdate userCityUpdate = new UserCityUpdate(user, ips[0],sessionUtils.getCallingUserId());
                    awsSQSManager.sendToSQS(SQSQueue.USER_CITY_UPDATE_QUEUE, SQSMessageType.USER_CITY_UPDATE, new Gson().toJson(userCityUpdate));
//                    AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.USER_CITY_UPDATE, payload1);
//                    asyncTaskFactory.executeTask(params);
                }
            }
        } catch (Exception e) {
            logger.warn(e);
        }
    }

    public String getReferralCode(User user) {
        String email = (user.getEmail() != null) ? user.getEmail() : StringUtils.randomAlphaNumericString(10);
        String stringSecret = DigestUtils.sha256Hex(user.getFullName() + email + SALT);
        String refCode = "";
        String name = user.getFirstName();

        if (StringUtils.isEmpty(name)) {
            name = "";
        }
        name = name.replaceAll("\\s", "");
        if (name.length() < 4) {
            refCode = name.toLowerCase() + stringSecret.substring(3, 11 - name.length());
        } else {
            refCode = name.substring(0, 4).toLowerCase() + stringSecret.substring(3, 7);
        }
        return refCode;
    }

    void resetPassword(Long userId, String password, Long callingUserId) throws VException {
        User user = getUserByUserId(userId);
        if (user == null) {
            throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, "User does not exist");
        }
        if (StringUtils.isNotEmpty(user.getEmail()) && restrictedResetPasswordAccounts.contains(user.getEmail().toLowerCase().trim())) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Trying to change password for restricted account");
        }

        if (Role.ADMIN.equals(user.getRole()) || Role.STUDENT_CARE.equals(user.getRole())) {
            List<String> passwordErrors = StringUtils.isValidAdminPassword(password);
            if (ArrayUtils.isNotEmpty(passwordErrors)) {
                throw new BadRequestException(ErrorCode.INVALID_ADMIN_PASSWORD, passwordErrors.toString());
            }
            List<String> recentHashes = user.getRecentHashedPasswords();
            for (String hash : recentHashes) {
                if (passwordEncoder.matches(password, hash)) {
                    throw new BadRequestException(ErrorCode.PASSWORD_CANNOT_BE_ONE_OF_LAST_2_PASSWORDS, "Password cannot be same as one of last 5 passwords");
                }
            }
        }
        String hashedPassword = passwordEncoder.encode(password);
        user.setPassword(hashedPassword);
        user.setPasswordHashed(true);
        user.setPasswordAutogenerated(false);
        user.setPasswordChangedAt(System.currentTimeMillis());
        user.addRecentHashedPassword(hashedPassword);
        userDAO.update(user, callingUserId);
    }

    public User getUserByUserId(Long userId) throws VException {
        if (userId == null) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "userId null");
        }
        return userDAO.getUserByUserId(userId);
    }

    public Pair<Boolean, Boolean> verifyEmail(Long userId, Long callingUserId) {
        Boolean verified = false;
        Boolean processReferralBonus = false;

        try {
            User user = getUserByUserId(userId);
            if (user.getIsEmailVerified()) { // Avoid verifying email Id
                // multiple time via mutiple
                // process, this also avoid
                // multiple freebies
                verified = true;
                return Pair.of(verified, processReferralBonus);
            } else {
                user.setIsEmailVerified(true);
                userDAO.update(user, callingUserId);
            }
            verified = user != null && user.getIsEmailVerified();

            /*
             * try { if (user.getReferrerCode() != null) {
             * ReferralManager.processReferralBonus(ReferralStep.VEMAIL, user); } } catch
             * (Exception e) { logger.error("Exception in processReferralBonus", e); }
             */
            processReferralBonus = true;

        } catch (Exception e) {
            logger.error("Exception in verifyEmail", e);
        }
        return Pair.of(verified, processReferralBonus);
    }

    public CreateUserResponse forgotPassword(ForgotPasswordReq req) throws BadRequestException, NotFoundException {
        req.verify();
        String email = req.getEmailId();
        if (!CustomValidator.validEmail(email)) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "No valid email found");
        }
        if (StringUtils.isNotEmpty(email) && restrictedResetPasswordAccounts.contains(email.toLowerCase().trim())) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Trying to change password for restricted account");
        }
        User user = userDAO.getUserByEmail(email);
        if (null == user) {
            throw new NotFoundException(ErrorCode.EMAIL_NOT_REGISTERED, "The email is not registered");
        }
        String tokenCode = tokenManager.createResetPasswordToken(user.getId(), email, req.getCallingUserId());
        CreateUserResponse response = new CreateUserResponse(pojoUtils.convertToUserPojo(user), null, tokenCode);
        return response;
    }

    public void resetPasswordOtpRequest(ResetPasswordReq req) throws VException, UnsupportedEncodingException {
        String email = req.getEmailId();
        String contactNumber = req.getContactNumber();
        Long callingUserId = sessionUtils.getCallingUserId();
        if (!CustomValidator.validEmail(email) && !CustomValidator.validPhoneNumber(contactNumber) && callingUserId == null) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "No valid email and contact number found");
        }
        if (StringUtils.isNotEmpty(email) && restrictedResetPasswordAccounts.contains(email.toLowerCase().trim())) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Trying to change password for restricted account");
        }
        User user;
        if (callingUserId != null) {
            user = userDAO.getUserByUserId(callingUserId);
        } else {
            user = userDAO.getUserByPhoneNumberAndCode(req.getContactNumber(), req.getPhoneCode());
        }
        if (null == user) {
            throw new NotFoundException(ErrorCode.EMAIL_NOT_REGISTERED, "The contact number is not registered");
        }

        email = email != null ? email : user.getEmail();
        contactNumber = contactNumber != null ? contactNumber : user.getContactNumber();

        if (StringUtils.isNotEmpty(email)) {
            sendOTPToEmail(email, true);
        }

        String formattedPhoneCode = (null != req.getPhoneCode()) ? req.getPhoneCode().replace("+", "") : "91";
        if (StringUtils.isNotEmpty(contactNumber)) {
            sendOTPToPhone(contactNumber, formattedPhoneCode, true);
        }

    }

    public PlatformBasicResponse changePassword(ChangePasswordReq req, HttpServletRequest request, HttpServletResponse response) throws VException, IOException, URISyntaxException {
        //req.verify();
        Long userId = req.getUserId();

        if (StringUtils.isEmpty(req.getPassword()) || req.getUserId() == null || req.getCallingUserId() == null) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Invalid request");
        }

        logger.info("==================new change");

        if (StringUtils.isEmpty(req.getOldPassword())) {
            return setOrChangePasswordWithOtp(req, request, response);
        }

        User userDetails = userDAO.getUserByUserId(userId);
        if (userDetails == null) {
            throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, "User not found", Level.WARN);
        }
        if (StringUtils.isNotEmpty(userDetails.getEmail()) && restrictedResetPasswordAccounts.contains(userDetails.getEmail().toLowerCase().trim())) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Trying to change password for restricted account");
        }

        // if (!userDetails.getEmailVerified()) {
        // throw new ConflictException(ErrorCode.EMAIL_NOT_VERIFIED, "Email Not
        // verified");
        // }
        User user = userDAO.authenticateUser(userId, req.getOldPassword());

        if (null == user) {
            throw new ConflictException(ErrorCode.PASSWORD_DOES_NOT_MATCH, "Password does not match");
        }

        if (Role.ADMIN.equals(user.getRole()) || Role.STUDENT_CARE.equals(user.getRole())) {
            List<String> passwordErrors = StringUtils.isValidAdminPassword(req.getPassword());
            if (ArrayUtils.isNotEmpty(passwordErrors)) {
                throw new BadRequestException(ErrorCode.INVALID_ADMIN_PASSWORD, passwordErrors.toString());
            }
        }

        List<String> recentHashes = user.getRecentHashedPasswords();
        for (String hash : recentHashes) {
            if (passwordEncoder.matches(req.getPassword(), hash)) {
                throw new BadRequestException(ErrorCode.PASSWORD_CANNOT_BE_ONE_OF_LAST_2_PASSWORDS, "New Password cannot be same as one of last 2 Passwords");
            }
        }

        String hashedPassword = passwordEncoder.encode(req.getPassword());
        user.setPassword(hashedPassword);
        user.setPasswordHashed(true);
        user.setPasswordAutogenerated(false);
        user.setPasswordChangedAt(System.currentTimeMillis());
        user.addRecentHashedPassword(hashedPassword);
        userDAO.update(user, req.getCallingUserId());
        return new PlatformBasicResponse();

    }

    public PlatformBasicResponse setOrChangePasswordWithOtp(ChangePasswordReq req, HttpServletRequest request, HttpServletResponse response) throws VException, IOException, URISyntaxException {
        Long userId = req.getUserId();
        String otp = req.getOtp();

        User user = userDAO.getUserByUserId(userId);
        if (user == null) {
            throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, "User not found", Level.WARN);
        }
        if (StringUtils.isNotEmpty(user.getEmail()) && restrictedResetPasswordAccounts.contains(user.getEmail().toLowerCase().trim())) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Trying to change password for restricted account");
        }

        boolean isFirstTimePasswordSet = user.getPassword() == null;

        VerificationTokenII mobiletoken = null;
        VerificationTokenII emailtoken = null;
        boolean phoneOtpVerified = false;
        boolean emailOtpVerified = false;
        if (!isFirstTimePasswordSet) {
            mobiletoken = verificationTokenDAO.getPhoneLoginVerificationCode(user.getContactNumber(), user.getPhoneCode());
            emailtoken = verificationTokenDAO.getEmailLoginVerificationCode(user.getEmail());
            if (mobiletoken == null && emailtoken == null) {
                throw new NotFoundException(ErrorCode.INVALID_VERIFICATION_CODE,
                        "Invalid verification code : " + otp);
            }

            if ((mobiletoken != null && VerificationLinkStatus.USED.equals(mobiletoken.getStatus())) && (emailtoken != null && VerificationLinkStatus.USED.equals(emailtoken.getStatus()))) {
                throw new ConflictException(ErrorCode.VERIFICATION_CODE_ALREADY_USED,
                        "Verification code : " + otp + " already used");
            }

            if (mobiletoken != null && !VerificationLinkStatus.USED.equals(mobiletoken.getStatus()) && mobiletoken.getCode().equals(otp)) {
                phoneOtpVerified = true;
            } else if (emailtoken != null && !VerificationLinkStatus.USED.equals(emailtoken.getStatus()) && emailtoken.getCode().equals(otp)) {
                emailOtpVerified = true;
            } else {
                throw new NotFoundException(ErrorCode.INVALID_VERIFICATION_CODE,
                        "Invalid verification code : " + otp);
            }

        }

        if (Role.ADMIN.equals(user.getRole()) || Role.STUDENT_CARE.equals(user.getRole())) {
            List<String> passwordErrors = StringUtils.isValidAdminPassword(req.getPassword());
            if (ArrayUtils.isNotEmpty(passwordErrors)) {
                throw new BadRequestException(ErrorCode.INVALID_ADMIN_PASSWORD, passwordErrors.toString());
            }
        }

        List<String> recentHashes = user.getRecentHashedPasswords();
        for (String hash : recentHashes) {
            if (passwordEncoder.matches(req.getPassword(), hash)) {
                throw new BadRequestException(ErrorCode.PASSWORD_CANNOT_BE_ONE_OF_LAST_2_PASSWORDS, "New Password cannot be same as one of last 2 Passwords");
            }
        }

        String hashedPassword = passwordEncoder.encode(req.getPassword());
        user.setPassword(hashedPassword);
        user.setPasswordHashed(true);
        user.setPasswordAutogenerated(false);
        user.setPasswordChangedAt(System.currentTimeMillis());
        user.addRecentHashedPassword(hashedPassword);

        if (phoneOtpVerified) {
            user.setIsContactNumberVerified(true);
            if (CollectionUtils.isNotEmpty(user.getPhones())) {
                user.getPhones().get(0).setIsVerified(true);
            }
        } else if (emailOtpVerified) {
            user.setIsEmailVerified(true);
        }

        userDAO.update(user, req.getCallingUserId());

        if (phoneOtpVerified) {
            tokenManager.markUsed(mobiletoken, req.getCallingUserId());
        } else if (emailOtpVerified) {
            tokenManager.markUsed(emailtoken, req.getCallingUserId());
        }

        return new PlatformBasicResponse();

    }

    public com.vedantu.User.User authenticateUserByOTP(SignInReq signInReq) throws VException {
        signInReq.verify();
//        User user = userDAO.getUserByEmail(signInReq.getEmail() + "-app@vedantu.com");
        User user = userDAO.getUserByContactNumberAndOTPPassword(signInReq.getEmail());

        if (null == user) {
            throw new ForbiddenException(ErrorCode.EMAIL_NOT_REGISTERED, "No user found for the specified email id.");
        }

        logger.info("GET-PASSWORD: " + signInReq.getPassword());
        logger.info("USER-PASSWORD: " + user.getOtpPassword());

        if (!user.getOtpPassword().equals(signInReq.getPassword())) {
            throw new UnauthorizedException(ErrorCode.PASSWORD_DOES_NOT_MATCH, "Incorrect credentials.");
        }

        if (user.getIsContactNumberVerified().equals(false)) {
            user.setIsContactNumberVerified(true);
            userDAO.update(user, user.getId());
        }

        if (Boolean.FALSE.equals(user.getProfileEnabled())) {
            throw new ForbiddenException(ErrorCode.USER_BLOCKED, "User is blocked.");
        }
        return pojoUtils.convertToUserPojo(user);
    }

    public com.vedantu.User.User authenticateUser(SignInReq signInReq) throws VException {
        signInReq.verify();
        User user = null;
        if (null != signInReq.getEmail()) {
            user = userDAO.getUserByEmail(signInReq.getEmail());
        } else if (null != signInReq.getPhone()) {
            user = userDAO.getUserByPhoneNumberAndCode(signInReq.getPhone(), signInReq.getPhoneCode());
        }
        if (null == user) {
            throw new ForbiddenException(ErrorCode.EMAIL_NOT_REGISTERED, "No user found for the specified email id.");
        }

        if (!passwordEncoder.matches(signInReq.getPassword(), user.getPassword())) {
            LoginData loginData = loginDataDao.getPasswordChangedTime(signInReq.getEmail());
            botFilter.blockIpAddress(signInReq.getIpAddress(), signInReq.getEmail());
            throw new UnauthorizedException(ErrorCode.PASSWORD_DOES_NOT_MATCH, "Incorrect credentials.");
        }
        Role signInReqUserRole = user.getRole();

        boolean exceptionEmailNotExist = true;
        String[] exceptionEmails = ConfigUtils.INSTANCE.getStringValue("google.sigin.in.exceptional.emails").split(",");
        if (signInReq.getEmail() != null) {
            for (String exEmail : exceptionEmails) {
                if (exEmail.equals(signInReq.getEmail())) {
                    exceptionEmailNotExist = false;
                    break;
                }
            }
        }

        if (signInReq.getEmail() != null && exceptionEmailNotExist) {
            if (Role.ADMIN.equals(signInReqUserRole) || Role.SEO_MANAGER.equals(signInReqUserRole) || Role.STUDENT_CARE.equals(signInReqUserRole)) {
                if (ConfigUtils.INSTANCE.getStringValue("environment").equals("PROD")) {
                    throw new BadRequestException(ErrorCode.ADMIN_USER_NON_SOCIAL_LOGIN, "Sign-in with Google's social API credentials");
                }
            }
        }

        if (Boolean.FALSE.equals(user.getProfileEnabled())) {
            throw new ForbiddenException(ErrorCode.USER_BLOCKED, "User is blocked.");
        }
        try {
            Map<String, Object> payload = new HashMap<>();
            payload.put("email", user.getEmail());
            payload.put("ipAddress", signInReq.getIpAddress());
            AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.SAVE_USER_LOGIN_DATA, payload);
            asyncTaskFactory.executeTask(params);
        }catch (Exception e){
            logger.info("Unable to update user login data");
        }
        com.vedantu.User.User userPojo = pojoUtils.convertToUserPojo(user);
        logger.info("authenticateUser - user - userAllowedToTakeOnboarding - {}", userPojo.isUserInProcessOfOnboarding());
        return userPojo;
    }

    public com.vedantu.User.User authenticateUserByToken(SignInByTokenReq signInReq) throws VException {
        signInReq.verify();
        UserLoginToken userLoginToken = userLoginTokenDAO.getById(signInReq.getToken());

        if (null == userLoginToken) {
            throw new ForbiddenException(ErrorCode.USER_LOGIN_TOKEN_INVALID, "Login token is not valid.");
        }
        Long diff = userLoginToken.getExpirationTime() - System.currentTimeMillis();
        if (diff <= 0) {
            throw new ForbiddenException(ErrorCode.USER_LOGIN_TOKEN_EXPIRED, "Login token is already expired.");
        }

        User user = userDAO.getUserByUserId(userLoginToken.getUserId());
        if (null == user) {
            throw new ForbiddenException(ErrorCode.EMAIL_NOT_REGISTERED, "No user found for the specified email id.");
        }

        if (Boolean.FALSE.equals(user.getProfileEnabled())) {
            throw new ForbiddenException(ErrorCode.USER_BLOCKED, "User is blocked.");
        }
        com.vedantu.User.User responseUser = pojoUtils.convertToUserPojo(user);
        responseUser.setUserId(userLoginToken.getUserId().toString());
        return responseUser;
    }

    public CreateUserAuthenticationTokenRes createUserAuthenticationToken(CreateUserAuthenticationTokenReq createUserAuthenticationTokenReq) throws BadRequestException {
        createUserAuthenticationTokenReq.verify();
        if (createUserAuthenticationTokenReq.getExpireAfterMinutes() == null) {
            createUserAuthenticationTokenReq.setExpireAfterMinutes(60l);
        }
        Long expirationTime = Date.from(LocalDateTime.now(UTC).plusMinutes(createUserAuthenticationTokenReq.getExpireAfterMinutes()).toInstant(UTC)).getTime();
        UserLoginToken userLoginToken = new UserLoginToken(createUserAuthenticationTokenReq.getUserId(), createUserAuthenticationTokenReq.getContextType(), createUserAuthenticationTokenReq.getContextId(), expirationTime);
        if (createUserAuthenticationTokenReq.getTestLink() != null) {
            userLoginToken.setTestLink(createUserAuthenticationTokenReq.getTestLink());
        }
        userLoginTokenDAO.create(userLoginToken, createUserAuthenticationTokenReq.getCallingUserId());
        CreateUserAuthenticationTokenRes res = new CreateUserAuthenticationTokenRes(userLoginToken.getContextType(), userLoginToken.getContextId(), userLoginToken.getExpirationTime(), userLoginToken.getUserId());
        res.setLoginToken(userLoginToken.getId());
        return res;
    }

    public GetUserProfileRes getUserProfile(Long userId, boolean exposeEmail) throws VException {
        GetUserProfileRes userProfile = null;
        if (userId == null) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "userId null");
        }
        com.vedantu.session.pojo.CumilativeRating ratingPojo = null;
        com.vedantu.User.User userPojo = null;

        User user = userDAO.getUserInfoWithPhones(userId);

        if (user != null) {
            userPojo = pojoUtils.convertToUserPojo(user);
            // CumilativeRating rating =
            // cumilativeRatingDAO.getCumulativeRating(user.getId(), EntityType.USER);
            // if (rating != null) {
            // ratingPojo = rating.toRatingPojo();
            // }
            userProfile = new GetUserProfileRes(userPojo, null, exposeEmail);
            userProfile.setPhones(getUserPhoneNumberRes(user));

            if (Role.STUDENT.equals(user.getRole())) {
                StudentInfo studentInfo = user.getStudentInfo();
                Double days = Double.parseDouble(ConfigUtils.INSTANCE.getStringValue("user.studentInfo.updateDays")) * DateTimeUtils.MILLIS_PER_DAY;
                Long refreshTime = days.longValue();
                if (studentInfo == null) {
                    studentInfo = new StudentInfo();
                    studentInfo.setUpdateNeeded(true);
                } else if (studentInfo.getLastUpdated() == null || (System.currentTimeMillis() - studentInfo.getLastUpdated()) > refreshTime) {
                    studentInfo.setUpdateNeeded(true);
                } else {
                    studentInfo.setUpdateNeeded(false);
                }
                userProfile.setStudentInfo(studentInfo);
                userProfile.setInfo(studentInfo);
            }

            if (Role.TEACHER.equals(user.getRole())) {
                userProfile.setPrimaryCallingNumberForTeacher(ConfigUtils.INSTANCE.getStringValue(
                        "teacher.call.extension." + user.getTeacherInfo().getPrimaryCallingNumberCode()));
                if (user.getTeacherInfo().getDexInfo() != null) {
                    userProfile.setTeacherPoolType(user.getTeacherInfo().getDexInfo().getTeacherPoolType());
                }
            }

        }

        if (null != userProfile && !exposeEmail) {
            userProfile.setPhones(null);
            userProfile.setContactNumber(null);
            userProfile.setPhoneCode(null);
            userProfile.setEmail(null);
            userProfile.setSocialInfo(null);
            userProfile.setFirstName(null);
            userProfile.setLastName(null);
            userProfile.setFullName(null);

            if (userProfile.getStudentInfo() != null) {
                userProfile.getStudentInfo().setParentInfos(null);
            }
            if (userProfile.getInfo() != null && userProfile.getInfo() instanceof StudentInfo) {
                ((StudentInfo) userProfile.getInfo()).setParentInfos(null);
            }
        }

        if(null != userProfile && null != userProfile.getRole() && userProfile.getRole().equals(Role.STUDENT)){
            if(null != sessionUtils.getCurrentSessionData()
                    && null != sessionUtils.getCurrentSessionData().getRole()
                    && sessionUtils.getCurrentSessionData().getRole().equals(Role.STUDENT)
                    && !sessionUtils.getCallingUserId().equals(userId)){
                throw new ForbiddenException(ErrorCode.FORBIDDEN_ERROR, "User is forbidden to use this API");
            }
        }


        return userProfile;
    }

    public Map<String, Boolean> isPasswordSet(Long userId) throws ForbiddenException {

        User user = userDAO.getUserByUserId(userId);
        if (null == user) {
            throw new ForbiddenException(ErrorCode.EMAIL_NOT_REGISTERED, "No user found for the specified id.");
        }
        boolean isPasswordSet = StringUtils.isNotEmpty(user.getPassword());
        Map<String, Boolean> response = new HashMap<>();
        response.put("isPasswordSet", isPasswordSet);
        return response;

    }

    public List<PhoneNumberRes> getUserPhoneNumberRes(User user) {
        List<PhoneNumberRes> phones = new ArrayList<>();
        boolean isActiveForCallFound = false;
        boolean isPrimaryFound = false;
        List<PhoneNumber> userPhones = user.getPhones();
        try {
            if (userPhones != null && !userPhones.isEmpty()) {
                for (PhoneNumber phone : userPhones) {
                    if (user.getContactNumber() != null && user.getContactNumber().equals(phone.getNumber())) {
                        isPrimaryFound = true;
                    }
                    phones.add(new PhoneNumberRes(pojoUtils.convertToPhonePojo(phone)));
                    if (!isActiveForCallFound) {
                        if (phone.getIsActiveForCall() != null) {
                            isActiveForCallFound = phone.getIsActiveForCall();
                        }
                    }
                }
            }
        } catch (Exception ex) {
            logger.error("Error in getUserPhoneNumberRes", ex);
        }

        if (!isPrimaryFound && StringUtils.isNotEmpty(user.getContactNumber())) {
            phones.add(0, new PhoneNumberRes(user.getContactNumber(), user.getPhoneCode(), user.getIsContactNumberDND(),
                    user.getIsContactNumberWhitelisted(), user.getIsContactNumberVerified(), false));
        }
        return phones;
    }

    public PlatformBasicResponse acceptTnc(AcceptTncReq req) throws VException {
        req.verify();
        Long userId = req.getUserId();

        User userDetails = userDAO.getUserByUserId(userId);
        if (userDetails == null) {
            throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, "User not found", Level.WARN);
        }
        userDetails.setTncVersion(req.getTncVersion());
        userDAO.update(userDetails, req.getCallingUserId());

        return new PlatformBasicResponse();

    }

    public void editProfileLocation(LocationInfo req, Long userId) throws NotFoundException, ConflictException {
        User user = null;
        User requestingUser = userDAO.getUserByUserId(userId);
        if (requestingUser == null) {
            throw new NotFoundException(ErrorCode.USER_NOT_FOUND,
                    "no user found with callingUserId:" + userId.toString(), Level.ERROR);
        }
        LocationInfo currentLocationInfo = requestingUser.getLocationInfo();
        if (currentLocationInfo == null || currentLocationInfo.getAddedBy() == null) {
            if (req != null) {
                requestingUser.setLocationInfo(req);
                userDAO.update(requestingUser, userId);
            }
        } else {
            if (LocationAddedBy.SYSTEM.equals(currentLocationInfo.getAddedBy())) {
                req.setAddedBy(LocationAddedBy.SYSTEM);
                requestingUser.setLocationInfo(req);
                userDAO.update(requestingUser, userId);
            }
        }
    }

    public EditProfileRes editProfile(EditProfileReq req) throws Exception {
        req.verify();
        EditProfileRes res = null;
        User user = null;
        User requestingUser = userDAO.getUserByUserId(req.getCallingUserId());
        if (requestingUser == null) {
            throw new NotFoundException(ErrorCode.USER_NOT_FOUND,
                    "no user found with callingUserId:" + req.getCallingUserId(), Level.ERROR);
        }
        boolean isSelf = (req.getUserId().longValue() == req.getCallingUserId().longValue());
        if (isSelf) {
            user = requestingUser;
        } else {
            if (!Role.ADMIN.equals(requestingUser.getRole())) {
                throw new ForbiddenException(ErrorCode.FORBIDDEN_ERROR,
                        "action not allowed for userId:" + req.getCallingUserId());
            }
            user = userDAO.getUserByUserId(req.getUserId());
        }

        if (user == null) {
            throw new NotFoundException(ErrorCode.USER_NOT_FOUND, "no user found with id:" + req.getUserId(),
                    Level.WARN);
        }
        UserHistoryII userHistory = mapper.map(user, UserHistoryII.class);
        userHistory.setId(null);
        userHistory.setUserId(user.getId());
        userHistoryDAO.create(userHistory, req.getCallingUserId());

        if(StringUtils.isNotEmpty(req.getFirstName())) {
            user.setFirstName(req.getFirstName());
        }
//        if(StringUtils.isNotEmpty(req.getLastName())) {
//            user.setLastName(req.getLastName());
//        }

        if(null !=  req.getLastName()){
            if(StringUtils.isEmpty(req.getLastName())){
                logger.info("******** last name edited by the user is empty");
            }
            user.setLastName(req.getLastName());
        }

        String firstName = req.getFirstName();
        String lastName = req.getLastName();
        StringBuffer sb = new StringBuffer();

        if (null != firstName) {
            sb.append(firstName);
        }
//        if (null != lastName) {
//            sb.append(" ");
//            sb.append(lastName);
//        }

        sb.append(" ");
        sb.append(lastName);
        String fullName = sb.toString();

        if (!StringUtils.isEmpty(fullName)) {
            user.setFullName(fullName);
        }

        if(Objects.nonNull(req.getGender())) {
            user.setGender(req.getGender());
        }
        if(!ArrayUtils.isEmpty(req.getLanguagePrefs())) {
            user.setLanguagePrefs(req.getLanguagePrefs());
        }
        if(Objects.nonNull(req.getParentRegistration())) {
            user.setParentRegistration(req.getParentRegistration());
        }
        req.getLocationInfo().setAddedBy(LocationAddedBy.USER);
        user.setLocationInfo(req.getLocationInfo());

        user.setSocialInfo(req.getSocialInfo());
        if (req.getStudentInfo() != null && !req.getStudentInfo().isEmpty()) {
            StudentInfo studentInfo = req.getStudentInfo();
            studentInfo.setLastUpdated(System.currentTimeMillis());
            user.setStudentInfo(studentInfo);
        }
        if(Role.STUDENT.equals(user.getRole()) && req.getStudentInfo() != null && req.getStudentInfo().getDeviceType()!=null) {
            user.getStudentInfo().setDeviceType(req.getStudentInfo().getDeviceType());
        }
        TeacherInfo prevTeacherInfo = user.getTeacherInfo();

        boolean isTeacher = user.getRole().equals(Role.TEACHER);
        if (isTeacher) {
            TeacherInfo tInfo = req.getTeacherInfo();
            // don't allow the UI to edit the user payout rate on edit Profile,
            // it should be handled on a separate call
            tInfo.setPayoutRate(user.getTeacherInfo().getPayoutRate());
            user.setTeacherInfo(tInfo);
            if (req.getTeacherPoolType() != null) {
                DexInfo dexInfo = tInfo.getDexInfo();
                AbstractTargetTopicEntity atte = new AbstractTargetTopicEntity();
                atte.setTargetGrades(dexInfo.getTargetGrades());
                atte.settTopics(dexInfo.gettTopics());

                AbstractTargetTopicEntity abstractTargetTopicEntity = fosUtils.setAndGenerateTags(atte);

                dexInfo.setTags(abstractTargetTopicEntity);
                // Enhancement as per GA-1269 : Paid doubts will only be visible to some specific Dex
                // As per GA-1341 : Also FOS type doubts need to be prioritised is addition to PAID, UNPAID
                dexInfo.setViewerAccesses(req.getTeacherInfo().getDexInfo().getViewerAccesses());
                user.getTeacherInfo().setDexInfo(dexInfo);
            }

            redisDAO.evictELTeachersKeys();
            // TODO, make checks for changes which admin can make and unlisted
            // teachers can make
            // right now it is implemented from front end.
            redisDAO.del(getUserBasicInfoKey(req.getCallingUserId()));
        }
        userDAO.update(user, req.getCallingUserId());
        // if (isTeacher) {
        // Migrated to platform
        // this.updateTeacherBoardMapping(user);
        // }
        res = new EditProfileRes(pojoUtils.convertToUserPojo(user));
        res.setPrevTeacherInfo(prevTeacherInfo);
        return res;

    }

    public EditProfileRes editStudentInfo(EditProfileReq req) throws VException {
        if (req.getUserId() == null) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "UserId not valid");
        }
        EditProfileRes res = null;
        User user = null;
        User requestingUser = userDAO.getUserByUserId(req.getCallingUserId());
        if (requestingUser == null) {
            throw new NotFoundException(ErrorCode.USER_NOT_FOUND,
                    "no user found with callingUserId:" + req.getCallingUserId(), Level.ERROR);
        }
        boolean isSelf = (req.getUserId().longValue() == req.getCallingUserId().longValue());
        if (isSelf) {
            user = requestingUser;
        } else {
            if (!Role.ADMIN.equals(requestingUser.getRole())) {
                throw new ForbiddenException(ErrorCode.FORBIDDEN_ERROR,
                        "action not allowed for userId:" + req.getCallingUserId());
            }
            user = userDAO.getUserByUserId(req.getUserId());
        }

        if (user == null) {
            throw new NotFoundException(ErrorCode.USER_NOT_FOUND, "no user found with id:" + req.getUserId(),
                    Level.WARN);
        }
        if (req.getStudentInfo() != null && !req.getStudentInfo().isEmpty()) {
            UserHistoryII userHistory = mapper.map(user, UserHistoryII.class);
            userHistory.setUserId(user.getId());
            userHistory.setId(null);
            userHistoryDAO.create(userHistory, req.getCallingUserId());
            StudentInfo studentInfo = req.getStudentInfo();
            studentInfo.setLastUpdated(System.currentTimeMillis());
            user.setStudentInfo(studentInfo);
            userDAO.update(user, req.getCallingUserId());
        }
        res = new EditProfileRes(pojoUtils.convertToUserPojo(user));
        return res;

    }

    public GetUsersRes getUsers(GetUsersReq req) throws VException {
        req.verify();
        GetUsersRes res = new GetUsersRes();

        List<User> users = userDAO.getUsersInfo(req.getRole(), req.getSubRole(), req.getQuery(), req.getStart(), req.getSize(),
                req.getFromTime(), req.getTillTime(),req.getTeacherCategoryType());
        if (users != null) {
            for (User user : users) {
                com.vedantu.session.pojo.CumilativeRating ratingPojo = null;
                // if (Role.TEACHER.equals(user.getRole())) {
                // CumilativeRating rating =
                // cumilativeRatingDAO.getCumulativeRating(user.getId(), EntityType.USER);
                // if (rating != null) {
                // ratingPojo = rating.toRatingPojo();
                // }
                // }
                UserInfo userInfo = new UserInfo(pojoUtils.convertToUserPojo(user), ratingPojo, true);
                if (Role.TEACHER.equals(user.getRole())) {
                    userInfo.setPrimaryCallingNumberForTeacher(ConfigUtils.INSTANCE.getStringValue(
                            "teacher.call.extension." + user.getTeacherInfo().getPrimaryCallingNumberCode()));

                    if (userInfo.getTeacherInfo() != null) {
                        if (userInfo.getTeacherInfo().getDexInfo() != null) {
                            userInfo.getTeacherInfo().getDexInfo().setTeacherPoolType(user.getTeacherInfo().getDexInfo().getTeacherPoolType());
                        }
                    }
                }
                logger.info("User teacherpooltype: " + userInfo.getTeacherPoolType());
                res.addUser(userInfo);
            }
        }
        return res;
    }

    public GetUsersRes getNonStudentUsers(GetNonUsersReq req) throws VException {
        logger.info("GetNonUsersReq : " + req);
        req.verify();
        GetUsersRes res = new GetUsersRes();
        List<User> users = userDAO.getNonStudentUsersInfo(req.getQuery(), req.getStart(), req.getSize());
        if (users != null) {
            for (User user : users) {
                com.vedantu.session.pojo.CumilativeRating ratingPojo = null;
                UserInfo userInfo = new UserInfo(pojoUtils.convertToUserPojo(user), ratingPojo, true);
                res.addUser(userInfo);
            }
        }
        return res;
    }

    public EditContactNumberRes editContactNumber(EditContactNumberReq req) throws VException {
        req.verify();
        EditContactNumberRes res = new EditContactNumberRes();
        User user = null;
        if (req.getUserId() != null) {
            user = userDAO.getUserByUserId(req.getUserId());
        } else {
            user = userDAO.getUserByUserId(req.getCallingUserId());
        }
        if (user == null) {
            throw new NotFoundException(ErrorCode.USER_NOT_FOUND, "no user found with id:" + req.getCallingUserId(),
                    Level.WARN);
        }

        logger.info("editContactUser: " + user.toString());

        String formattedPhoneCode = req.getPhoneCode().replace("+", "");
        req.setPhoneCode(formattedPhoneCode);

        String formattedPhoneNumber = req.getContactNumber().replace(" ", "");
        req.setContactNumber(formattedPhoneNumber);

        String number = req.getContactNumber();
        String phoneCode = req.getPhoneCode();

        // check if phone already exists.
        if (StringUtils.isNotEmpty(number) && StringUtils.isNotEmpty(phoneCode)) {
            User phoneUser = userDAO.getUserByPhoneNumberAndCode(number, phoneCode);
            if (null != phoneUser) {
                throw new ConflictException(ErrorCode.USER_ALREADY_EXISTS, "User already exists with phone[" + number
                        + "]");
            }
        }

        if (StringUtils.isEmpty(phoneCode)) {
            phoneCode = "91";
        }

        pattern = Pattern.compile(phoneCodeRegex);

        if (!pattern.matcher(phoneCode).matches()) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Invalid Phone Code");
        }

        pattern = Pattern.compile(phoneRegex);

        if (!pattern.matcher(number).matches()) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Invalid Phone Number");
        }

        String[] testingNumbers = ConfigUtils.INSTANCE.getStringValue("testing.mobileNumbers").split(",");
        List<String> testingNumbersList = new ArrayList<String>();
        for (String testingNumber : testingNumbers) {
            testingNumbersList.add(testingNumber.trim());
        }

        if (!testingNumbersList.contains(number)) {
            List<User> usersByNumber = userDAO.getUsersByPhoneNumberAndCode(number, phoneCode, false);
            int maxAllowed = ConfigUtils.INSTANCE.getIntValue("user.maxAllowedAccountsWithSameNumber");
            if (null != usersByNumber && maxAllowed > 0 && usersByNumber.size() >= maxAllowed) {
                throw new ConflictException(ErrorCode.CONTACT_NUMBER_EXISTS,
                        "More than allowed users already exist with contact number");
            }
        }

        Boolean isDNDNumber = user.getIsContactNumberDND();
        Boolean isVerified = false;

        logger.info("editContactNumber-check");

        logger.info("Number: " + number);
        logger.info("getContactNumber: " + user.getContactNumber());
        logger.info("phonecode: " + phoneCode);
        logger.info("userPhoneCode: " + user.getPhoneCode());
//        if (!(number.equals(user.getContactNumber()) && phoneCode.equals(user.getPhoneCode()))) {
        logger.info("editContactNumber-check-if");
        PhoneNumber phone = null;
        List<PhoneNumber> phones = user.getPhones();
        if (phones != null && !phones.isEmpty()) {
            for (PhoneNumber phoneNumber : phones) {
                if (number.equals(phoneNumber.getNumber()) && phoneCode.equals(phoneNumber.getPhoneCode())) {
                    phone = phoneNumber;
                    break;
                }
            }
        }
        Boolean isWhitelisted = false;
        if (phone != null && phone.getIsVerified() != null && phone.getIsVerified()) {

            logger.info("editContactNumber-check-if-if");

            isDNDNumber = phone.getIsDND();
            isWhitelisted = phone.getIsWhitelisted();
            isVerified = phone.getIsVerified();
        } else {
            // Set mobileToken so platform can send the verificationSMS
            // sendContactNumberVerificationSMS(user, number, phoneCode, null, mgr);
            String mobileToken = tokenManager.getMobileToken(user.getId(), req.getContactNumber(),
                    req.getPhoneCode(), req.getCallingUserId());

            logger.info("editContactNumber-check-if-else-mobile: " + mobileToken);

            res.setMobileTokenCode(mobileToken);
            res.setUserInfo(new UserInfo(pojoUtils.convertToUserPojo(user), null, true));
            res.setUtm_campaign(user.getUtm_campaign());
        }

        if (phone == null) {
            phone = new PhoneNumber(number, phoneCode, false, false, false, false);
            if (phones == null) {
                user.setPhones(new ArrayList<>());
            }
            user.getPhones().add(phone);
        }

        // Teachers can have only one phone number, editing contact number means
        // editing phones list and removing the old contact number
        if (Role.TEACHER.equals(user.getRole())) {
            List<PhoneNumber> _phones = user.getPhones();
            String oldContactNumber = user.getContactNumber();
            String oldPhoneCode = user.getPhoneCode();

            if (ArrayUtils.isNotEmpty(_phones)) {
                for (PhoneNumber _phPhoneNumber : _phones) {
                    if (_phPhoneNumber != null && _phPhoneNumber.getNumber().equals(oldContactNumber)
                            && _phPhoneNumber.getPhoneCode().equals(oldPhoneCode)) {
                        _phones.remove(_phPhoneNumber);
                        break;
                    }
                }
            }
        }

//            user.setContactNumber(number);
        user.setTempContactNumber(number);
        user.setTempPhoneCode(phoneCode);
//            user.setPhoneCode(phoneCode);
        user.setIsContactNumberDND(isDNDNumber);
        user.setIsContactNumberVerified(isVerified);
        user.setIsContactNumberWhitelisted(isWhitelisted);
        userDAO.update(user, req.getCallingUserId());
//        }
        res.setContactNumber(number);
        res.setPhoneCode(phoneCode);
        res.setIsDND(isDNDNumber);
        res.setIsVerified(isVerified);
        return res;
    }

    public ReSendContactNumberVerificationCodeRes requestOTPPassword(GetOTPReq req) throws VException {
        req.verify();

        List<User> users = userDAO.getUserByVerifiedContactNumber(req.getNumber());

        if (ArrayUtils.isEmpty(users)) {
            throw new NotFoundException(ErrorCode.USER_NOT_FOUND, "no user found with id:" + sessionUtils.getCallingUserId());
        }

        User user = users.get(0);

        for (User userCheck : users) {
            if (userCheck.getIsContactNumberVerified().equals(true)) {
                user = userCheck;
                break;
            }
        }

        if (StringUtils.isEmpty(req.getPhoneCode())) {
            req.setPhoneCode("91");
        }

        String formattedPhoneCode = req.getPhoneCode().replace("+", "");
        req.setPhoneCode(formattedPhoneCode);

        String formattedPhoneNumber = req.getNumber().replace(" ", "");
        req.setNumber(formattedPhoneNumber);

        pattern = Pattern.compile(phoneCodeRegex);

        if (!pattern.matcher(formattedPhoneCode).matches()) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Invalid Phone Code");
        }

        pattern = Pattern.compile(phoneRegex);

        if (!pattern.matcher(req.getNumber()).matches()) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Invalid Phone Number");
        }

//        String[] testingNumbers = ConfigUtils.INSTANCE.getStringValue("testing.mobileNumbers").split(",");
//        List<String> testingNumbersList = new ArrayList<String>();
//        for (String testingNumber : testingNumbers) {
//            testingNumbersList.add(testingNumber.trim());
//        }
//        if (!testingNumbersList.contains(req.getNumber())) {
//            List<User> usersByNumber = userDAO.getUsersByPhoneNumberAndCode(req.getNumber(), req.getPhoneCode());
//            int maxAllowed = ConfigUtils.INSTANCE.getIntValue("user.maxAllowedAccountsWithSameNumber");
//
//            if (null != usersByNumber && usersByNumber.size() >= 1) {
//                String email = usersByNumber.get(0).getEmail();
//                throw new ConflictException(ErrorCode.CONTACT_NUMBER_EXISTS,
//                        "M.No. verified with " + email.replaceAll("(?<=.{3}).(?=.*@)", "*")
//                                + ". Login with your verified account or signup with new M.No.");
//            }
//
////            if (null != usersByNumber && maxAllowed > 0 && usersByNumber.size() >= maxAllowed) {
////                throw new ConflictException(ErrorCode.CONTACT_NUMBER_EXISTS,
////                        "More than allowed users already exist with contact number");
////            }
//        }
//        user.setTempContactNumber(req.getNumber());
//        user.setTempPhoneCode(req.getPhoneCode());
        ReSendContactNumberVerificationCodeRes res = new ReSendContactNumberVerificationCodeRes();
        res.setSended(true);
        UserInfo userInfo = new UserInfo(pojoUtils.convertToUserPojo(user), null, true);
        userInfo.setTempContactNumber(req.getNumber());
        userInfo.setTempPhoneCode(req.getPhoneCode());
        res.setUserInfo(userInfo);

        String otpToken = StringUtils.randomNumericString(4);
        user.setOtpPassword(otpToken);
        user.setOtpPasswordChangedAt(System.currentTimeMillis());
        userDAO.update(user, user.getId());

//        String mobileToken = tokenManager.getMobileToken(user.getId(), req.getNumber(), req.getPhoneCode(), req.getCallingUserId());
        Map<String, Object> scopeParams = new HashMap<String, Object>();
        scopeParams.put("code", otpToken);
        TextSMSRequest textSMSRequest = new TextSMSRequest(req.getNumber(), req.getPhoneCode(), scopeParams, CommunicationType.PHONE_VERIFICATION, user.getRole());
        communicationManager.sendSMSViaRest(textSMSRequest);
        return res;
    }

    public ReSendContactNumberVerificationCodeRes requestOTP(GetOTPReq req) throws VException {
        req.verify();
        User user = userDAO.getUserByUserId(sessionUtils.getCallingUserId());

        if (user == null) {
            throw new NotFoundException(ErrorCode.USER_NOT_FOUND, "no user found with id:" + sessionUtils.getCallingUserId());
        }

        if (StringUtils.isEmpty(req.getPhoneCode())) {
            req.setPhoneCode("91");
        }

        String formattedPhoneCode = req.getPhoneCode().replace("+", "");
        req.setPhoneCode(formattedPhoneCode);

        String formattedPhoneNumber = req.getNumber().replace(" ", "");
        req.setNumber(formattedPhoneNumber);

        pattern = Pattern.compile(phoneCodeRegex);

        if (!pattern.matcher(formattedPhoneCode).matches()) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Invalid Phone Code");
        }

        pattern = Pattern.compile(phoneRegex);

        if (!pattern.matcher(req.getNumber()).matches()) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Invalid Phone Number");
        }

        String[] testingNumbers = ConfigUtils.INSTANCE.getStringValue("testing.mobileNumbers").split(",");
        List<String> testingNumbersList = new ArrayList<String>();
        for (String testingNumber : testingNumbers) {
            testingNumbersList.add(testingNumber.trim());
        }

        if (!testingNumbersList.contains(req.getNumber())) {
            List<User> usersByNumber = userDAO.getUsersByPhoneNumberAndCode(req.getNumber(), req.getPhoneCode(), false);
            int maxAllowed = ConfigUtils.INSTANCE.getIntValue("user.maxAllowedAccountsWithSameNumber");

            if (null != usersByNumber && usersByNumber.size() >= 1) {
                String email = usersByNumber.get(0).getEmail();
                if(null == email){
                    email = "";
                }
                throw new ConflictException(ErrorCode.CONTACT_NUMBER_EXISTS,
                        "M.No. verified with " + email.replaceAll("(?<=.{3}).(?=.*@)", "*")
                        + ". Login with your verified account or signup with new M.No.");
            }

//            if (null != usersByNumber && maxAllowed > 0 && usersByNumber.size() >= maxAllowed) {
//                throw new ConflictException(ErrorCode.CONTACT_NUMBER_EXISTS,
//                        "More than allowed users already exist with contact number");
//            }
        }

        user.setTempContactNumber(req.getNumber());
        user.setTempPhoneCode(req.getPhoneCode());

        userDAO.update(user, req.getCallingUserId());
        ReSendContactNumberVerificationCodeRes res = new ReSendContactNumberVerificationCodeRes();
        res.setSended(true);
        UserInfo userInfo = new UserInfo(pojoUtils.convertToUserPojo(user), null, true);
        userInfo.setTempContactNumber(req.getNumber());
        userInfo.setTempPhoneCode(req.getPhoneCode());
        res.setUserInfo(userInfo);
        String mobileToken = tokenManager.getMobileToken(user.getId(), req.getNumber(), req.getPhoneCode(), req.getCallingUserId());
        Map<String, Object> scopeParams = new HashMap<String, Object>();
        scopeParams.put("code", mobileToken);
        TextSMSRequest textSMSRequest = new TextSMSRequest(req.getNumber(), req.getPhoneCode(), scopeParams, CommunicationType.PHONE_VERIFICATION, user.getRole());

        communicationManager.sendSMSViaRest(textSMSRequest);

        return res;

    }

    public ReSendContactNumberVerificationCodeRes reSendContactNumberVerificationCode(
            ReSendContactNumberVerificationCodeReq req) throws VException {
        req.verify();
        User user = userDAO.getUserByUserId(req.getCallingUserId());
        if (user == null) {
            throw new NotFoundException(ErrorCode.USER_NOT_FOUND, "no user found with id:" + req.getCallingUserId());
        }

        boolean isPrimaryNumber = StringUtils.isEmpty(req.getNumber());

        if (isPrimaryNumber && user.getIsContactNumberVerified()) {
            throw new ConflictException(ErrorCode.ALREADY_VERIFIED, "contact number already verified");
        }

        if (StringUtils.isEmpty(req.getPhoneCode())) {
            req.setPhoneCode("91");
        }

        String number = isPrimaryNumber ? user.getTempContactNumber() : req.getNumber();
        String phoneCode = isPrimaryNumber ? user.getTempPhoneCode() : req.getPhoneCode();

        user.setTempContactNumber(number);
        user.setTempPhoneCode(phoneCode);
        userDAO.update(user, req.getCallingUserId());

        if (!isPrimaryNumber) {

            boolean isPhoneNumberAdded = false;
            try {
                List<PhoneNumber> phones = user.getPhones();
                if (phones != null && !phones.isEmpty()) {
                    for (PhoneNumber phoneNumber : phones) {
                        if (number.equals(phoneNumber.getNumber()) && phoneCode.equals(phoneNumber.getPhoneCode())) {
                            isPhoneNumberAdded = true;
                            break;
                        }
                    }
                }
            } catch (Exception ex) {
                logger.error("reSendContactNumberVerificationCode", ex);
            }
            if (!isPhoneNumberAdded) {
                throw new NotFoundException(ErrorCode.PHONE_NUMBER_NOT_ADDED,
                        "phone number[" + number + "]  not added in user[" + user.getId() + "] profile", Level.WARN);
            }
        }

        ReSendContactNumberVerificationCodeRes res = new ReSendContactNumberVerificationCodeRes();
        res.setSended(true);

        UserInfo userInfo = new UserInfo(pojoUtils.convertToUserPojo(user), null, true);
        userInfo.setTempContactNumber(number);
        userInfo.setTempPhoneCode(phoneCode);
        logger.info("USER-INFO: " + userInfo.toString());
        res.setUserInfo(userInfo);
        String mobileToken = tokenManager.getMobileToken(user.getId(), number, phoneCode, req.getCallingUserId());
        res.setMobileTokenCode(mobileToken);
        res.setUtm_campaign(user.getUtm_campaign());
        return res;
    }

    public PhoneNumberRes updatePhoneNumber(AddOrUpdatePhoneNumberReq req) throws VException {
        req.verify();
        User user = userDAO.getUserByUserId(req.getCallingUserId());
        if (user == null) {
            throw new NotFoundException(ErrorCode.USER_NOT_FOUND, "no user found with id:" + req.getCallingUserId());
        }

        String number = req.getNumber();
        String phoneCode = req.getPhoneCode();

        // check if phone already exists.
        if (StringUtils.isNotEmpty(number) && StringUtils.isNotEmpty(phoneCode)) {
            User phoneUser = userDAO.getUserByPhoneNumberAndCode(number, phoneCode);
            if (null != phoneUser) {
                throw new ConflictException(ErrorCode.USER_ALREADY_EXISTS, "User already exists with phone[" + number
                        + "]");
            }
        }

        if (StringUtils.isEmpty(phoneCode)) {
            phoneCode = "91";
        }

        boolean alreadyAdded = false;
        if (number.equals(user.getContactNumber()) && phoneCode.equals(user.getPhoneCode())) {
            alreadyAdded = true;
        }

        if (req.isRemove() && alreadyAdded) {
            throw new ForbiddenException(ErrorCode.ACTION_NOT_ALLOWED, "Phone Number[" + number
                    + "] can not be removed as it is set as primary contact number for user[" + user.getId() + "]");
        }

        List<PhoneNumber> phones = user.getPhones();

        if (!alreadyAdded && phones != null) {
            int matchedIndex = -1;
            for (int index = 0; index < phones.size(); index++) {
                PhoneNumber phoneNumber = phones.get(index);
                if (number.equals(phoneNumber.getNumber()) && phoneCode.equals(phoneNumber.getPhoneCode())) {
                    alreadyAdded = true;
                    matchedIndex = index;
                    break;
                }
            }

            if (req.isRemove()) {
                if (!alreadyAdded || matchedIndex < 0) {
                    throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR,
                            "number[" + number + "] is not added in user[" + req.getCallingUserId() + "] profile",
                            Level.WARN);
                } else {
                    logger.info("======== matchedIndex ====== " + matchedIndex);
                    PhoneNumber removeNumber = phones.get(matchedIndex);
                    logger.info("removeNumber ====== " + removeNumber);
                    boolean deleted = phones.remove(removeNumber);
                    if (deleted) {
                        userDAO.update(user, req.getCallingUserId());
                        return new PhoneNumberRes(pojoUtils.convertToPhonePojo(removeNumber));
                    }
                }
            }
        }

        if (alreadyAdded) {
            throw new ConflictException(ErrorCode.PHONE_NUMBER_ALREADY_ADDED,
                    "Phone Number[" + number + "] already added for user[" + user.getId() + "]");
        }

        boolean isDND = false;
        boolean isWhitelisted = false;
        /*
         * if("91".equals(phoneCode)) { isDND =
         * SMSManager.cloudPhoneCallManager.isDNDNumber(number); if (isDND) {
         * isWhitelisted = SMSManager.cloudPhoneCallManager.isWhitelistedNumber(number);
         * } }
         */
        PhoneNumber phone = new PhoneNumber(number, phoneCode, false, isDND, isWhitelisted, false);
        phones.add(phone);
        userDAO.update(user, req.getCallingUserId());

        PhoneNumberRes res = new PhoneNumberRes(pojoUtils.convertToPhonePojo(phone));
        res.setUserInfo(new UserInfo(pojoUtils.convertToUserPojo(user), null, true));
        String mobileToken = tokenManager.getMobileToken(user.getId(), number, phoneCode, req.getCallingUserId());
        res.setUtm_campaign(user.getUtm_campaign());
        res.setMobileTokenCode(mobileToken);
        return res;
    }

    public PhoneNumberRes updateAndMarkActivePhoneNumber(AddOrUpdatePhoneNumberReq req) throws VException {
        req.verify();
        User user = userDAO.getUserByUserId(req.getCallingUserId());
        if (user == null) {
            throw new NotFoundException(ErrorCode.USER_NOT_FOUND, "no user found with id:" + req.getCallingUserId());
        }

        String number = req.getNumber();
        String phoneCode = req.getPhoneCode();

        if (StringUtils.isEmpty(phoneCode)) {
            phoneCode = "91";
        }

        if (!"91".equals(phoneCode)) {
            throw new ForbiddenException(ErrorCode.ACTION_NOT_ALLOWED, "Non-indian numbers not allowed");
        }

        boolean alreadyAdded = false;

        List<PhoneNumber> phones = user.getPhones();
        PhoneNumber phone = null;
        if (!alreadyAdded && phones != null) {
            int matchedIndex = -1;
            for (int index = 0; index < phones.size(); index++) {
                PhoneNumber phoneNumber = phones.get(index);
                if (number.equals(phoneNumber.getNumber()) && phoneCode.equals(phoneNumber.getPhoneCode())) {
                    alreadyAdded = true;
                    matchedIndex = index;
                    phoneNumber.setIsActiveForCall(true);
                    phone = phoneNumber;
                } else {
                    phoneNumber.setIsActiveForCall(false);
                }
            }
        }

        if (!alreadyAdded) {
            boolean isDND = false;
            boolean isWhitelisted = false;
            /*
             * if ("91".equals(phoneCode)) { isDND =
             * SMSManager.cloudPhoneCallManager.isDNDNumber(number); if (isDND) {
             * isWhitelisted = SMSManager.cloudPhoneCallManager
             * .isWhitelistedNumber(number); } }
             */
            phone = new PhoneNumber(number, phoneCode, false, isDND, isWhitelisted, true);
            phones.add(phone);
        }
        userDAO.update(user, req.getCallingUserId());
        PhoneNumberRes res = new PhoneNumberRes(pojoUtils.convertToPhonePojo(phone));
        return res;
    }

    public ProcessVerifyContactNumberResponse verifyContactNumber(VerifyContactNumberReq req) throws VException {
        req.verify();
        ProcessVerifyContactNumberResponse response = new ProcessVerifyContactNumberResponse();
        // Boolean checkOtpOnly = req.isOtpOnly();
        User user;

        // try{
        // user = userDAO.getUserByIdFromWriteServer(req.getCallingUserId());
        // }catch(Exception ex){
        // user = userDAO.getUserById(req.getCallingUserId(),mgr);
        // }
        user = userDAO.getUserByUserId(req.getCallingUserId());
        if (user == null) {
            throw new NotFoundException(ErrorCode.USER_NOT_FOUND, "no user found with id:" + req.getCallingUserId());
        }

        VerificationTokenII token = tokenManager.getVerificationTokenByCode(req.getCallingUserId(),
                req.getVerificationCode());
        if (token == null) {
            throw new NotFoundException(ErrorCode.INVALID_VERIFICATION_CODE,
                    "Invalid verification code : " + req.getVerificationCode());
        }

        if (VerificationLinkStatus.USED.equals(token.getStatus())) {
            throw new ConflictException(ErrorCode.VERIFICATION_CODE_ALREADY_USED,
                    "Verification code : " + req.getVerificationCode() + " already used");
        }

        String number = req.getNumber();
        String phoneCode = req.getPhoneCode();
        if (StringUtils.isEmpty(number)) {
//            number = user.getContactNumber();
            number = user.getTempContactNumber();
            phoneCode = user.getTempPhoneCode();
        }

        if (StringUtils.isEmpty(phoneCode)) {
            phoneCode = "91";
        }

        String numberAssociatedWithToken = token.getEmailId();
        String phoneCodeAssociatedWithToken = token.getPhoneCode();
        String numberRequestedToVerify = number;
        String phoneCodeRequestedToVerify = phoneCode;

        if (StringUtils.isEmpty(token.getPhoneCode())) {
            phoneCodeAssociatedWithToken = "91";
        }

        if (!(numberAssociatedWithToken.equals(numberRequestedToVerify)
                && phoneCodeAssociatedWithToken.equals(phoneCodeRequestedToVerify))) {
            throw new NotFoundException(ErrorCode.INVALID_VERIFICATION_CODE,
                    "Invalid verification code : " + req.getVerificationCode());
        }

        // email id == contact numberAssociatedWithToken
        boolean isDND = false;
        boolean isWhitelisted = false;

        /*
         * if("91".equals(phoneCodeAssociatedWithToken)) {
         *
         * isDND = SMSManager.cloudPhoneCallManager
         * .isDNDNumber(numberAssociatedWithToken); if (isDND) { isWhitelisted =
         * SMSManager.cloudPhoneCallManager.isWhitelistedNumber(
         * numberAssociatedWithToken); if (!isWhitelisted) {
         * SMSManager.cloudPhoneCallManager.addToDNDWhitelist(numberAssociatedWithToken)
         * ; if (!checkOtpOnly || Role.TEACHER.equals(user.getRole())) { throw
         * VExceptionFactory.INSTANCE.conflictException(ErrorCode.
         * CONTACT_NUMBER_NOT_WHITELISTED,
         * "DND contact number & not subscribed to whitelist", null); } } } }
         */
        boolean isPrimaryNumber = number.equals(user.getTempContactNumber());
        boolean isPhoneNumerMatched = false;

        LocationInfo lInfo = user.getLocationInfo();
        if (lInfo != null && lInfo.getCity() != null && !LocationAddedBy.SYSTEM.equals(lInfo.getAddedBy())) {
            logger.info("City: " + lInfo.getCity());
        } else {
            try {
                String ipAddress = req.getIpAddress();
                if (ipAddress != null) {

                    if (StringUtils.isNotEmpty(ipAddress)) {
                        String[] ips = ipAddress.split(",");
                        logger.info("IPADDRESS: " + ips[0]);

                        if (user.getIpAddress() != null && user.getIpAddress().contains("127.0.0.1") && !ips[0].contains("127.0.0.1")) {
                            user.setIpAddress(ips[0]);
                        }

                        UserCityUpdate userCityUpdate = new UserCityUpdate(user, ips[0],req.getCallingUserId());
                        awsSQSManager.sendToSQS(SQSQueue.USER_CITY_UPDATE_QUEUE, SQSMessageType.USER_CITY_UPDATE, new Gson().toJson(userCityUpdate));
//                        AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.USER_CITY_UPDATE, payload);
//                        asyncTaskFactory.executeTask(params);

                    }
                }
            } catch (Exception e) {
                logger.warn(e);
            }
        }

        // check if phone already exists.
        if (StringUtils.isNotEmpty(numberAssociatedWithToken) && StringUtils.isNotEmpty(phoneCodeAssociatedWithToken)) {
            User phoneUser = userDAO.getUserByPhoneNumberAndCode(numberAssociatedWithToken, phoneCodeAssociatedWithToken);
            if (null != phoneUser) {
                throw new ConflictException(ErrorCode.USER_ALREADY_EXISTS, "User already exists with phone[" + phoneCodeAssociatedWithToken
                        + "]");
            }
        }

        if (isPrimaryNumber) {
            user.setContactNumber(numberAssociatedWithToken);
            user.setPhoneCode(phoneCodeAssociatedWithToken);
            user.setIsContactNumberDND(isDND);
            user.setIsContactNumberWhitelisted(isWhitelisted);
            user.setIsContactNumberVerified(true);
            user.setTempContactNumber(null);
            user.setTempPhoneCode(null);
            isPhoneNumerMatched = true;

            PhoneNumber newPh = new PhoneNumber();
            newPh.setNumber(numberAssociatedWithToken);
            newPh.setPhoneCode(phoneCodeAssociatedWithToken);
            newPh.setIsDND(isDND);
            newPh.setIsWhitelisted(isWhitelisted);
            newPh.setIsVerified(true);

            List<PhoneNumber> phones = user.getPhones();
            if (phones != null && !phones.isEmpty()) {
                boolean numberPresent = false;
                for (PhoneNumber phone : phones) {
                    if (phone.getNumber() != null && phone.getNumber().equals(numberAssociatedWithToken)) {
                        numberPresent = true;
                        phone.setIsVerified(true);
                    }
                }
                if (numberPresent == false) {
                    phones.add(newPh);
                }
            } else {
                phones = new ArrayList<>();
                phones.add(newPh);
            }
            user.setPhones(phones);
        }

        if (!isPhoneNumerMatched) {
            throw new BadRequestException(ErrorCode.NOT_FOUND_ERROR, "phone number[" + number
                    + "] does not match with any number added by user[" + user.getId() + "] in profile");
        }

        userDAO.update(user, req.getCallingUserId());

        UserDetails userDetails = userDetailsDAO.getUserDetailsByUserId(user.getId(), null);
        if (userDetails != null) {
            userDetails.setStudentPhoneNo(numberAssociatedWithToken);
            userDetails.setStudentPhoneCode(phoneCodeAssociatedWithToken);
            userDetails.setParentPhoneNo(numberAssociatedWithToken);
            userDetails.setParentPhoneCode(phoneCodeAssociatedWithToken);
            userDetailsDAO.save(userDetails, user.getId());
        }

        tokenManager.markUsed(token, req.getCallingUserId());

        response.setNumber(numberAssociatedWithToken);
        response.setPhoneCode(phoneCodeAssociatedWithToken);
        response.setDnd(isDND);
        response.setVerified(true);
        response.setWhitelisted(isWhitelisted);

        HttpSessionData hsd = pojoUtils.convertToUserSessionPojo(user);
        hsd.setUserId(user.getId());

        response.setUserDetails(hsd);

        response.setUser(pojoUtils.convertToUserPojo(user));

        response.setAllowFreebies(allowFreebies(user));

        // processing Referral
        response.setProcessReferralBonus(true);

        if (response.isVerified()) {
            TextSMSRequest textSMSRequest = new TextSMSRequest(response.getNumber(), response.getPhoneCode(), null, CommunicationType.PHONE_VERIFICATION_SUCCESS, user.getRole());
            communicationManager.sendSMSViaRest(textSMSRequest);
        }

        return response;
    }

    private Boolean allowFreebies(User user) {
        return false;// product asked to remove the freebies on sms verification
        /*
         * if (!Role.STUDENT.equals(user.getRole())) { return false; }
         * Set<VerificationToken> vTokensSet = new HashSet<VerificationToken>();
         *
         * List<VerificationToken> verificationTokens =
         * tokenManager.getVerificationTokens(user.getId(),
         * VerificationTokenType.SMS_VERIFICATION, VerificationLinkStatus.USED); if
         * (verificationTokens != null) { vTokensSet.addAll(verificationTokens); } if
         * (vTokensSet.size() > 1) { return false; } else { return true; }
         */
    }

    public GetUserPhoneNumbersRes getUserPhoneNumbers(Long userId) throws VException {
        // req.verify();
        // Long userId = req.getUserId() == null ? req.getCallingUserId() :
        // req.getUserId();

        User user = userDAO.getUserByUserId(userId);
        if (user == null) {
            throw new NotFoundException(ErrorCode.USER_NOT_FOUND, "no user found with id:" + userId);
        }

        GetUserPhoneNumbersRes res = new GetUserPhoneNumbersRes();
        res.setList(getUserPhoneNumberRes(user));
        return res;
    }

    // public User updateUser(User user) throws VException {
    // if (user == null) {
    // throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "User null");
    // }
    // userDAO.update(user);
    //
    // return user;
    // }
    public User getUserByEmail(String email) throws VException {
        if (!CustomValidator.validEmail(email)) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Email not valid");
        }
        return userDAO.getUserByEmail(email);
    }

    public UserBasicInfo getUserBasicInfoByEmail(String email, boolean exposeEmail) throws VException {
        if (!CustomValidator.validEmail(email)) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Email " + email + " not valid");
        }
        return new UserBasicInfo(pojoUtils.convertToUserPojo(userDAO.getUserBasicInfoByEmail(email, true)), exposeEmail);
    }

    public List<com.vedantu.User.User> getUsersById(List<Long> userIds, boolean exposeEmail) throws VException {
        if (userIds == null) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "userIds null");
        }
        List<User> users = userDAO.getUsersById(userIds);
        List<com.vedantu.User.User> results = new ArrayList<>();
        if (!CollectionUtils.isEmpty(users)) {
            users.stream().map((user) -> pojoUtils.convertToUserPojo(user)).map((u) -> {
                if (!exposeEmail) {
                    u.setContactNumber(null);
                    u.setPhoneCode(null);
                    u.setEmail(null);
                    if (Role.TEACHER.equals(u.getRole()) && u.getTeacherInfo() != null) {
                        u.getTeacherInfo().setExtensionNumber(null);
                        u.getTeacherInfo().setPrimaryCallingNumberCode(null);
                    }
                }
                return u;
            }).forEachOrdered((u) -> {
                results.add(u);
            });
        }
        return results;
    }

    public List<com.vedantu.User.User> getUsersByContactNumber(String contactNumber) throws VException {
        List<User> users = userDAO.getUsersByContactNumber(contactNumber);
        List<com.vedantu.User.User> results = new ArrayList<>();
        if (!CollectionUtils.isEmpty(users)) {
            users.forEach(u -> results.add(pojoUtils.convertToUserPojo(u)));
        }
        return results;
    }

    // public List<?> getEntityInfoByIds(String type, String userIdsList) throws
    // VException {
    // List<?> resp = null;
    // if (StringUtils.isEmpty(userIdsList)) {
    // return resp;
    // }
    // String[] ids = org.apache.commons.lang3.StringUtils.split(userIdsList, ",");
    // if (ids != null) {
    // List<Long> longIds = new ArrayList<Long>();
    // for (String id : ids) {
    // longIds.add(Long.parseLong(id));
    // }
    // switch (type) {
    // case "user":
    // resp = getUserBasicInfoByIds(longIds);
    // break;
    // case "teacher":
    // //TODO: Implement this
    // //resp = (List<EsTeacherInfo>)
    // OTFManager.INSTANCE.getTeacherInfoByIds(request);
    // break;
    // case "student":
    // resp = getUserBasicInfoByIds(longIds);
    // break;
    // //case "board":
    // //resp = (List<BoardInfoRes>) OTFManager.INSTANCE.getBoardInfo(request);
    // default:
    // break;
    // }
    // }
    // return resp;
    // }
    public List<UserBasicInfo> getUserBasicInfos(String userIdsList, boolean exposeEmail) {
        List<UserBasicInfo> response = new ArrayList<>();
        String[] ids = org.apache.commons.lang3.StringUtils.split(userIdsList, ",");
        if (ids != null) {
            List<Long> longIds = new ArrayList<>();
            for (String id : ids) {
                longIds.add(Long.parseLong(id));
            }
            response = getUserBasicInfos(longIds, exposeEmail);
        }
        return response;
    }

    public List<UserBasicInfo> getUserBasicInfos(List<Long> longIds, boolean exposeEmail) {
        List<UserBasicInfo> response = new ArrayList<>();

        // this way of duplicating is wrong but the other
        // subsystems are expecting the response in this format
        // TODO remove this duplicacy
        Map<Long, UserBasicInfo> infoMap = getUserBasicInfosMap(longIds, exposeEmail);
        for (Long id : longIds) {
            if (infoMap.containsKey(id)) {
                response.add(infoMap.get(id));
            } else {
                response.add(null);
            }
        }
        logger.info("response" + response);
        return response;
    }

    public PlatformBasicResponse setProfilePic(SetProfilePicReq req) throws VException {
        req.verify();
        User user = userDAO.getUserByUserId(req.getUserId());
        if (null == user) {
            throw new NotFoundException(ErrorCode.USER_NOT_FOUND, null);
        }

        user.setProfilePicId(req.getFileId());
        user.setProfilePicPath(req.getPath());
        user.setProfilePicUrl(req.getUrl());

        userDAO.update(user, req.getCallingUserId());

        // try {
        // if (Role.TEACHER.equals(user.getRole())) {
        // user.setProfilePicUrl(req.getUrl());
        // /*
        // CreateElasticSearchTask.INSTANCE.createTask(
        // ElasticSearchTask.UPDATE_TEACHER_DATA,
        // (new Gson().toJson(user)));
        // */
        // eSManager.updateTeacherData(user);
        // }
        // } catch (Exception e) {
        // logger.error(e);
        // }
        return new PlatformBasicResponse();
    }

    public Role getRole(Long userId) throws VException {
        Role role = null;
        if (userId != null) {
            role = userDAO.getRole(userId);
        }
        return role;
    }

    public PlatformBasicResponse addSessionDuration(Long teacherId, Long duration, Long callingUserId)
            throws NotFoundException, ConflictException {
        PlatformBasicResponse response = new PlatformBasicResponse();
        User teacher = userDAO.getUserByUserId(teacherId);
        if (teacher == null) {
            throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, "teacherId does not exist " + teacherId,
                    Level.ERROR);
        }

        TeacherInfo teacherInfo = teacher.getTeacherInfo();
        teacherInfo.setSessionHours(teacherInfo.getSessionHours() + duration);
        teacherInfo.setSessions(teacherInfo.getSessions() + 1);
        teacher.setTeacherInfo(teacherInfo);
        userDAO.update(teacher, callingUserId, false);
        return response;
    }

    public PlatformBasicResponse markBlockStatus(SetBlockStatusForUserReq setBlockStatusForUserReq)
            throws ForbiddenException, BadRequestException, com.vedantu.exception.InternalServerErrorException {

        PlatformBasicResponse response = new PlatformBasicResponse();

        // User callingUser = userDAO.getUserByUserId(callingUserId);
        // if (callingUser == null || (!Role.ADMIN.equals(callingUser.getRole())
        // && !Role.TEACHER.equals(callingUser.getRole()))) {
        // throw new ForbiddenException(ErrorCode.FORBIDDEN_ERROR, "Operation not
        // allowed to non-admin user["
        // + callingUser.getId() + "]");
        // }
        try {
            Boolean profileEnabled = setBlockStatusForUserReq.isProfileEnabled();
            // if (!profileEnabled) {
            // //TODO send an email
            //// NotificationManager.INSTANCE.createNotification(setBlockStatusForUserReq.getUserId(),
            //// NotificationType.BLOCKED, null, null);
            // }

            User user = userDAO.getUserByUserId(setBlockStatusForUserReq.getUserId());
            if (user == null) {
                throw new NotFoundException(ErrorCode.USER_NOT_FOUND,
                        "no user found with id:" + setBlockStatusForUserReq.getUserId());
            }
            user.setProfileEnabled(profileEnabled);
            user.setBlockedAt(System.currentTimeMillis());
            user.setBlockedBy(setBlockStatusForUserReq.getCallingUserId());
            user.setBlockedReason(setBlockStatusForUserReq.getReason());
            user.setBlockedReasonType(setBlockStatusForUserReq.getReasonType());

            userDAO.update(user, setBlockStatusForUserReq.getCallingUserId());

            // if (!profileEnabled) {
            // String reason = setBlockStatusForUserReq.getReason();
            // //TODO send email to admin
            //// EmailManager.INSTANCE.sendBlockUserToAdmin(callingUser, user, reason);
            // }
            response.setSuccess(true);

        } catch (Throwable e) {
            throw new com.vedantu.exception.InternalServerErrorException(ErrorCode.SERVICE_ERROR, e.getMessage());
        }
        return response;
    }

    public UserInfo getUserInfo(Long userId, boolean exposeEmail) throws VException {
        if (userId == null) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "userId null");
        }
        User user = userDAO.getUserInfo(userId);
        if (user == null) {
            return null;
        }
        com.vedantu.User.User userPojo = pojoUtils.convertToUserPojo(user);
        UserInfo userInfo = new UserInfo(userPojo, null, exposeEmail);
        return userInfo;
    }

    public ReferralInfoRes getReferralInfo(Long userId) throws VException {
        UserInfo userInfo = null;
        UserInfo referrerUserInfo = null;
        User user = getUserByUserId(userId);
        if (user == null) {
            throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, "No User found with id " + userId, Level.WARN);
        }
        userInfo = new UserInfo(pojoUtils.convertToUserPojo(user), null, false);
        if (StringUtils.isNotEmpty(user.getReferrerCode())) {
            referrerUserInfo = userDAO.getUserInfoByReferralCode(user.getReferrerCode());
        }
        return new ReferralInfoRes(userInfo, referrerUserInfo);
    }

    public StatusCounterResponse getMemberCount(Role role, Boolean verifiedCustomersOnly) {
        StatusCounterResponse resp = new StatusCounterResponse();
        Long memberCount = userDAO.getMemberCount(role, verifiedCustomersOnly);
        logger.info("getMemberCount result : " + memberCount);
        resp.setMemberCount(memberCount);
        return resp;
    }

    public String getUserSystemIntroDataRes(GetUserSystemIntroReq reqPojo)
            throws BadRequestException, NotFoundException {
        if (!reqPojo.isAllData()) {
            reqPojo.verify();
        }
        String response;
        if (reqPojo.isAllData()) {
            response = new Gson().toJson(getUserSystemIntroDataList(reqPojo));
        } else {
            response = new Gson().toJson(getUserSystemIntroData(reqPojo));
        }
        return response;
    }

    public UserSystemIntroRes getUserSystemIntroData(GetUserSystemIntroReq req) throws NotFoundException {
        UserSystemIntroData userSystemIntroData = userSystemIntroDataDAO.getUserSystemIntroData(req.getUserId(),
                req.getType(), req.getIdentifier());
        if (userSystemIntroData == null) {
            throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, "No UserSystemIntroData found for user : " + req);
        }
        UserSystemIntroRes userSystemIntroRes = userSystemIntroData.toUserSystemIntroRes();
        return userSystemIntroRes;
    }

    public GetUserSystemIntroDataListRes getUserSystemIntroDataList(GetUserSystemIntroReq req) {

        GetUserSystemIntroDataListRes res = new GetUserSystemIntroDataListRes();

        List<UserSystemIntroData> results = userSystemIntroDataDAO.getUserSystemIntroDataList(req.getUserId(),
                req.getType());
        for (UserSystemIntroData uData : results) {
            res.addItem(uData.toUserSystemIntroRes());
        }
        return res;
    }

    public UserSystemIntroRes addUserSystemIntroData(AddUserSystemIntroReq addUserSystemIntroReq) {
        UserSystemIntroData userSystemIntroData = new UserSystemIntroData(addUserSystemIntroReq.getUserId(),
                addUserSystemIntroReq.getType(), addUserSystemIntroReq.getStatus(), addUserSystemIntroReq.getData(),
                addUserSystemIntroReq.getIdentifier());
        UserSystemIntroData prevUserSystemIntroData = userSystemIntroDataDAO.getUserSystemIntroData(
                addUserSystemIntroReq.getUserId(), addUserSystemIntroReq.getType(),
                addUserSystemIntroReq.getIdentifier());
        if (prevUserSystemIntroData != null) {
            userSystemIntroData.setId(prevUserSystemIntroData.getId());
        }
        userSystemIntroDataDAO.create(userSystemIntroData, addUserSystemIntroReq.getCallingUserId());
        UserSystemIntroRes userSystemIntroRes = userSystemIntroData.toUserSystemIntroRes();
        if ("SYSTEM_CHECK".equals(userSystemIntroData.getIdentifier())
                && "HEALTH_CHECK".equals(userSystemIntroData.getType())) {
            // ZohoCRMManager.INSTANCE.updateUserSystemInfoInCRM(userSystemIntroData,
            // false); //TODO
        }
        return userSystemIntroRes;
    }

    public PlatformBasicResponse incSubscriptionRequestCount(Long userId) throws VException {
        Query query = new Query();
        query.addCriteria(Criteria.where(User.Constants._ID).is(userId));
        Update update = new Update();
        update.inc(User.Constants.SUBSCRIPTION_REQUEST_COUNT, 1);
        userDAO.updateFirst(query, update);
        return new PlatformBasicResponse();
    }

    public void triggerUserUpdationSNS(User user) {
        // user.setPassword(null);
        String topicArn = "arn:aws:sns:ap-southeast-1:" + arn + ":USER_EVENTS_" + env.toUpperCase();
        // String msg = "{\"sessionId\":\"" + sessionId + "\"}";
        String msg = new Gson().toJson(user);
        String snsSubject = UserEvents.USER_UPDATED.toString();
        PublishRequest publishRequest = new PublishRequest(topicArn, msg, snsSubject);
        PublishResult publishResult = snsClient.publish(publishRequest);
        logger.info("Published SNS, MessageId - " + publishResult.getMessageId());

        try {
            redisDAO.del(getUserBasicInfoKey(user.getId()));
        } catch (Exception e) {
            // TODO Auto-generated catch block
            logger.info("Error in deleting cache from redis");
        }
    }

    public GetActiveTutorsRes getActiveTutorsList(GetActiveTutorsReq reqPojo) {
        List<User> users = userDAO.getTutorsWithProfilePic(reqPojo.getStart(), reqPojo.getSize());
        GetActiveTutorsRes respPojo = new GetActiveTutorsRes();
        if (users != null && reqPojo.getStart() != null && reqPojo.getSize() != null
                && users.size() == (reqPojo.getSize() - reqPojo.getStart())) {
            respPojo.setHasNext(Boolean.TRUE);
        }
        for (User user : users) {
            // log("user = "+user.getProfileEnabled()+", active = "+user.getTeacherInfo());
            Boolean profileEnabled = user.getProfileEnabled();
            if ((profileEnabled == null || profileEnabled) && user.getTeacherInfo() != null) {
                Boolean isActive = user.getTeacherInfo().getActive();
                if (isActive == null || isActive) {
                    respPojo.addUser(new TeacherBasicInfo(pojoUtils.convertToUserPojo(user), false));
                }
            }
        }
        return respPojo;
    }

    public String getTeacherContactFromExtension(ExtensionNumberReq req) throws VException {
        int primaryNumberCode = 1;
        if (req.getTo() != null) {
            while (true) {
                String keyVal = ConfigUtils.INSTANCE.getStringValue("teacher.call.extension." + primaryNumberCode);
                if (StringUtils.isEmpty(keyVal) || keyVal.equals(req.getTo())) {
                    break;
                }
                primaryNumberCode++;
            }
        }
        // Optimization can be done
        User user = userDAO.getUserViaExtension(primaryNumberCode,
                Integer.parseInt(req.getDigits().replace("\"", "").trim()));
        if (user == null) {
            throw new NotFoundException(ErrorCode.USER_NOT_FOUND, "User not found");
        }
        String phoneNumber = user.getContactNumber();
        return phoneNumber;
    }

    public PlatformBasicResponse incOtfSubscriptionCount(Long userId) {
        Query query = new Query();
        query.addCriteria(Criteria.where(User.Constants._ID).is(userId));
        Update update = new Update();
        update.inc(User.Constants.OTF_SUBSCRIPTION_COUNT, 1);
        update.set(User.Constants.LAST_UPDATED, System.currentTimeMillis());
        userDAO.updateFirst(query, update);
        return new PlatformBasicResponse();
    }

    public List<TeacherListWithStatusRes> getActiveTeacherIdList() {
        int start = 0;
        int limit = 200;
        List<TeacherListWithStatusRes> teacherRes = new ArrayList<>();
        while (true) {
            Query query = new Query();
            query.addCriteria(Criteria.where(User.Constants.ROLE).is(Role.TEACHER));
            query.addCriteria(Criteria.where(User.Constants.PROFILE_ENABLED).is(Boolean.TRUE));
            query.fields().include(User.Constants.ID);
            query.fields().include(User.Constants._ID);
            query.fields().include(User.Constants.CURRENT_STATUS);
            query.fields().include(User.Constants.PRIMARY_SUBJECT);
            // query.fields().include(User.Constants.TEACHER_INFO);
            query.with(Sort.by(Sort.Direction.DESC, User.Constants.CREATION_TIME));
            userDAO.setFetchParameters(query, start, limit);
            List<User> users = userDAO.runQuery(query, User.class);

            if (ArrayUtils.isNotEmpty(users)) {
                for (User user : users) {
                    if (user.getId() != null) {
                        TeacherListWithStatusRes res = new TeacherListWithStatusRes();
                        res.setTeacherId(user.getId());
                        res.setCurrentStatus(user.getTeacherInfo().getCurrentStatus());
                        res.setPrimarySubject(user.getTeacherInfo().getPrimarySubject());
                        teacherRes.add(res);
                    }
                }
            } else {
                break;
            }
            start += limit;
        }
        return teacherRes;
    }

    public void migratePassword(Integer start, Integer size, Boolean all) {

        while (true) {
            List<User> users = userDAO.getUsersByCreationTime(start, size);
            logger.info("start: " + start + " size: " + size + " beginning time: " + System.currentTimeMillis());
            if (users == null || users.isEmpty()) {
                break;
            }

            for (User user : users) {
                try {
                    logger.info("Start User Id: " + user.getId() + " time: " + System.currentTimeMillis());
                    if (!Boolean.TRUE.equals(user.getPasswordHashed()) && StringUtils.isNotEmpty(user.getPassword())) {

                        String hashedPassword = passwordEncoder.encode(user.getPassword());
                        user.setPassword(hashedPassword);
                        user.setPasswordHashed(true);
                        userDAO.updateUserWithoutTrigger(user);
                    }

                    logger.info("End User Id: " + user.getId() + " time: " + System.currentTimeMillis());
                } catch (Exception ex) {
                    logger.error("Exception in User ID: " + user.getId(), ex);
                }

            }
            start += size;
            if (!all) {
                break;
            }

            logger.info("end time: " + System.currentTimeMillis());
        }
    }

    public void addAddress(AddUserAddressReq addUserAddressReq) throws NotFoundException, ConflictException {
        User user = userDAO.getUserByUserId(addUserAddressReq.getUserId());
        if (user == null) {
            throw new NotFoundException(ErrorCode.USER_NOT_FOUND,
                    "No user found for id : "
                    + addUserAddressReq.getUserId());
        }

        LocationInfo locationInfo;
        if (user.getLocationInfo() == null) {
            logger.info("location info pre update: NO location info found");
            locationInfo = new LocationInfo();
        } else {
            logger.info("location info pre update:" + user.getLocationInfo().toString());
            locationInfo = user.getLocationInfo();
        }
        locationInfo.setCity(addUserAddressReq.getCity());
        locationInfo.setCountry(addUserAddressReq.getCountry());
        locationInfo.setPincode(addUserAddressReq.getPincode());
        locationInfo.setState(addUserAddressReq.getState());
        locationInfo.setStreetAddress(addUserAddressReq.getStreetAddress());
        locationInfo.setStreetAddressLine2(addUserAddressReq.getStreetAddressLine2());
        locationInfo.setAddedBy(LocationAddedBy.USER);
        user.setLocationInfo(locationInfo);
        userDAO.update(user, addUserAddressReq.getCallingUserId());
    }

    public UserDetails createUserDetailsForEvent(User user, String event,
            Long callingUserId, String agentName, String agentEmail, String exam,
            String voltId, List<String> achievementDocments, List<String> achievements,
            List<Map<String, String>> idproofs, LocationInfo locationInfo, Map<String, String> userInfo) throws VException {

        if (user != null && StringUtils.isNotEmpty(event)) {
            UserDetails userDetail = userDetailsDAO.getUserDetailsByUserId(user.getId(), event);
            if (userDetail == null) {
                userDetail = new UserDetails(user);
                if (user.getSignUpFeature() == FeatureSource.VOLT_2020_JAN) {
                    if (StringUtils.isNotEmpty(voltId)) {
                        if (isValidVoltId(voltId)) {
                            if (isVoltIdUsed(voltId)) {
                                throw new ConflictException(ErrorCode.USED_VOLT_ID, "VOLT ID ALREADY USED");
                            }
                            userDetail.setVoltId(voltId);
                            userDetail.setVoltStatus(VoltStatus.REGISTERED);
                        } else {
                            throw new ConflictException(ErrorCode.INVALID_VOLT_ID, "VOLT ID IS INVALID");
                        }
                    } else if (ArrayUtils.isEmpty(achievementDocments) || ArrayUtils.isEmpty(achievements)) {
                        logger.info("ACHIEVEMENT SELECTED: " + achievements);
                        logger.info("ACHIEVEMENT DOCUMENTS: " + achievementDocments);
                        throw new ConflictException(ErrorCode.ACIEVEMENT_DOCUMENTS_VOLT_EMPTY, "ACHIEVEMENT DOCUMENTS CANT BE EMPTY");
                    } else {
                        userDetail.setAchievementDocments(achievementDocments);
                        userDetail.setAchievements(achievements);
                        userDetail.setVoltStatus(VoltStatus.PENDING);
                    }
                    userDetail.setIdproofs(idproofs);
                }
                userDetail.setEvent(event);
                String category = null;

                StudentInfo studentInfo = user.getStudentInfo();
                if (studentInfo != null) {
                    if (StringUtils.isNotEmpty(studentInfo.getGrade())) {
                        try {
                            Integer grade = Integer.parseInt(studentInfo.getGrade());
                            userDetail.setGrade(grade);
                        } catch (NumberFormatException e) {
                            //
                            throw new BadRequestException(ErrorCode.USER_GRADE_NOT_FOUND, "User Grade in Improper format");
                        }
                    }
                }
                int grade = 0;
                if (userDetail.getGrade() != null) {
                    userDetail.setCategory(getEventCategoryForGrade(userDetail.getGrade(), event, exam));
                } else if (StringUtils.isNotEmpty(event) && event.equals("REVISE_JEE_2020_MARCH")) {
                    userDetail.setCategory("REVISE_JEE_2020_MARCH_1");
                } else if (StringUtils.isNotEmpty(event) && event.equals("TARGET_JEE_NEET")) {
                    userDetail.setCategory("TARGET_JEE_NEET");
                } else {
                    throw new BadRequestException(ErrorCode.USER_GRADE_NOT_FOUND, "User Grade in Not Found");
                }
                userDetail.setExam(exam);
                userDetail.setBoard(studentInfo.getBoard());
                userDetail.setAgentEmail(agentEmail);
                userDetail.setAgentName(agentName);
                if (locationInfo != null) {
                    userDetail.setCity(locationInfo.getCity());
                    userDetail.setState(locationInfo.getState());
                    userDetail.setCountry(locationInfo.getCountry());
                    String address = "";
                    if (StringUtils.isNotEmpty(locationInfo.getStreetAddress())) {
                        address += locationInfo.getStreetAddress().trim();
                    }
                    if (StringUtils.isNotEmpty(locationInfo.getStreetAddressLine2())) {
                        address += " " + locationInfo.getStreetAddressLine2();
                    }
                    if (StringUtils.isNotEmpty(address.trim())) {
                        userDetail.setAddress(address);
                    }
                }
                userDetail.setSchool(studentInfo.getSchool());

                if (user.getSignUpFeature() == FeatureSource.REVISE_JEE_2020_MARCH) {
                    if (userInfo.containsKey("jeeApplicationNumber")) {
                        userDetail.setJeeApplicationNumber(userInfo.get("jeeApplicationNumber"));
                    }
                    if (userInfo.containsKey("dob")) {
                        userDetail.setDob(userInfo.get("dob"));
                    }
                }else if (user.getSignUpFeature() == FeatureSource.TARGET_JEE_NEET) {
                    if (userInfo.containsKey("jeeApplicationNumber")) {
                        userDetail.setJeeApplicationNumber(userInfo.get("jeeApplicationNumber"));
                    }
                }
                userDetailsDAO.save(userDetail, callingUserId);
                triggerSNSUserDetails(userDetail);
                return userDetail;
            } else {
                throw new ConflictException(ErrorCode.ALREADY_REGISTERED, "User Already Registered for ISL");
            }
        }
        return null;
    }

    public boolean isVoltIdUsed(String voltId) {
        List<UserDetails> details = userDetailsDAO.getByVoltId(voltId);
        return ArrayUtils.isNotEmpty(details);
    }

    public boolean isValidVoltId(String voltId) throws VException {
        logger.info("VOLT ID: " + voltId);
        if (StringUtils.isEmpty(voltId)) {
            return false;
        }
        String key = "VOLT_" + voltId;
        if (StringUtils.isNotEmpty(redisDAO.get(key))) {
            logger.info("VOLT ID IN REDIS");
            return true;
        }
        String url = ConfigUtils.INSTANCE.getStringValue("ISL_ENDPOINT") + "/volt/validate/" + voltId;
        ClientResponse response = WebUtils.INSTANCE.doCall(url, HttpMethod.GET, null);
        try {
            String entity = response.getEntity(String.class);
            logger.info("VOLT ID STATUS: " + entity);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (response.getStatus() == HttpServletResponse.SC_OK) {
            logger.info("VOLT ID IN ISL");
            redisDAO.set(key, voltId);
            return true;
        } else {
            return false;
        }
    }

    public UserDetailsInfo createUserDetailsForExistingUserAndSendEmail(ExistingUserISLRegReq req) throws VException {
        UserDetailsInfo userDetailsInfo = createUserDetailsForExistingUser(req);
        //sendEmailAndSMS
        try {
            com.vedantu.User.User user = new com.vedantu.User.User();
            user.setFirstName(userDetailsInfo.getStudentName());
            if (userDetailsInfo.getStudent() == null || (StringUtils.isEmpty(userDetailsInfo.getStudent().getEmail())
                    && StringUtils.isEmpty(userDetailsInfo.getStudent().getContactNumber()))) {
                logger.error("User BasicInfo or email and contact number is null");
            }

            user.setEmail(userDetailsInfo.getStudent().getEmail());
            StudentInfo studentInfo = new StudentInfo();
            user.setStudentInfo(studentInfo);
            user.setRole(Role.STUDENT);
            user.setId(req.getUserId());
            user.setContactNumber(userDetailsInfo.getParentPhoneNo());
            user.setPhoneCode(userDetailsInfo.getParentPhoneCode());
            if (StringUtils.isEmpty(user.getContactNumber())) {
                user.setContactNumber(req.getContactNumber());
                user.setPhoneCode(req.getPhoneCode());
            }
            user.setTempContactNumber(userDetailsInfo.getStudentPhoneNo());

            Map<String, Object> payload = new HashMap<>();
            payload.put("user", user);
            payload.put("userDetails", userDetailsInfo);
            AsyncTaskParams params = null;

            if (null != req.getFeatureSource()) {
                switch (req.getFeatureSource()) {
                    case ISL_REGISTRATION_VIA_TOOLS:
                        params = new AsyncTaskParams(AsyncTaskName.USER_DETAILS_CREATED_VIA_ISL_REGISTRATION_TOOLS, payload);
                        break;
                    case ISL:
                        params = new AsyncTaskParams(AsyncTaskName.REGISTRATION_EMAIL_ISL, payload);
                        break;
                    case VSAT:
                        params = new AsyncTaskParams(AsyncTaskName.REGISTRATION_EMAIL_VSAT, payload);
                        break;
                    case VOLT_2020_JAN:
                        params = new AsyncTaskParams(AsyncTaskName.REGISTRATION_EMAIL_VOLT, payload);
                        break;
                    case JRP_SEP_2019:
                        params = new AsyncTaskParams(AsyncTaskName.REGISTERED_EMAIL_JRP, payload);
                        break;
                }
            }
            else if(null != req.getSignUpFeature()) {
                switch (req.getSignUpFeature()) {
                    case REVISEINDIA_2020_FEB:
                        params = new AsyncTaskParams(AsyncTaskName.REGISTRATION_EMAIL_REVISEINDIA, payload);
                        break;
                    case REVISE_JEE_2020_MARCH:
                        params = new AsyncTaskParams(AsyncTaskName.REGISTRATION_EMAIL_REVISEJEE, payload);
                        break;
                    case VSAT:
                        params = new AsyncTaskParams(AsyncTaskName.REGISTRATION_EMAIL_VSAT, payload);
                        break;
                }
            }
            if(params != null){
                asyncTaskFactory.executeTask(params);
            }
        } catch (Exception e) {
            logger.error("Error sending Registration mail");
        }
        return userDetailsInfo;
    }

    public UserDetailsInfo createUserDetailsForExistingUser(ExistingUserISLRegReq req) throws VException {
        req.verify();
        UserDetails userDetails = userDetailsDAO.getUserDetailsByUserId(req.getUserId(), req.getEvent());
        if (userDetails != null) {
            throw new ConflictException(ErrorCode.ALREADY_REGISTERED, "User Already Registered for ISL");
        }

        if(req.getUserId() == null){
            req.setUserId(sessionUtils.getCallingUserId());
        }
        User user = userDAO.getUserByUserId(req.getUserId());
        if (user == null) {
            throw new NotFoundException(ErrorCode.USER_NOT_FOUND, "No user found for userId : " + req.getUserId());
        }
        if (!Role.STUDENT.equals(user.getRole())) {
            throw new ForbiddenException(ErrorCode.ACTION_NOT_ALLOWED, "Only students can register");
        }
        //public UserDetails createUserDetailsForExistingUser(ExistingUserISLRegReq req)
//        userDetails = new UserDetails(user);
        userDetails = new UserDetails();
        userDetails.setUserId(user.getId());
        if(CommonUtils.isValidEmailId(user.getEmail())) {
            userDetails.setEmail(user.getEmail());
            userDetails.setStudentEmail(user.getEmail());
        }
        else{
            throw new BadRequestException(ErrorCode.INVALID_EMAILID, "email id is not valid");
        }

        userDetails.setStudentFirstName(req.getStudentFirstName());
        userDetails.setStudentLastName(req.getStudentLastName());

        String studentName = (req.getStudentFirstName() + " " + StringUtils.defaultIfEmpty(req.getStudentLastName())).trim();
        userDetails.setStudentName(studentName);
        StudentInfo studentInfo = req.getStudentInfo();
        userDetails.setStudentPhoneCode(req.getPhoneCode());
        userDetails.setStudentPhoneNo(user.getContactNumber());
        if (req.getEvent().equals("VOLT_2020_JAN")) {
            if (StringUtils.isNotEmpty(req.getVoltId())) {
                if (isValidVoltId(req.getVoltId())) {
                    if (isVoltIdUsed(req.getVoltId())) {
                        throw new ConflictException(ErrorCode.USED_VOLT_ID, "VOLT ID ALREADY USED");
                    }
                    userDetails.setVoltId(req.getVoltId());
                    userDetails.setVoltStatus(VoltStatus.REGISTERED);
                } else {
                    throw new ConflictException(ErrorCode.INVALID_VOLT_ID, "VOLT ID IS INVALID");
                }
            } else if (ArrayUtils.isEmpty(req.getAchievementDocments()) || ArrayUtils.isEmpty(req.getAchievements())) {
                logger.info("ACHIEVEMENT SELECTED: " + req.getAchievements());
                logger.info("ACHIEVEMENT DOCUMENTS: " + req.getAchievementDocments());
                throw new ConflictException(ErrorCode.ACIEVEMENT_DOCUMENTS_VOLT_EMPTY, "ACHIEVEMENT DOCUMENTS CANT BE EMPTY");
            } else {
                userDetails.setAchievementDocments(req.getAchievementDocments());
                userDetails.setAchievements(req.getAchievements());
                userDetails.setVoltStatus(VoltStatus.PENDING);
            }
            userDetails.setIdproofs(req.getIdproofs());
        }

        if (studentInfo != null) {
            List<String> parentEmails = new ArrayList<>(studentInfo.getParentEmails(user.getEmail()));
            if (ArrayUtils.isNotEmpty(parentEmails)
                    && StringUtils.isNotEmpty(parentEmails.get(0))) {

                if(CommonUtils.isValidEmailId(parentEmails.get(0))){
                    user.setEmail(parentEmails.get(0));
                }
                else{
                    throw new BadRequestException(ErrorCode.INVALID_EMAILID, "email id is not valid");
                }

            }

            if (StringUtils.isNotEmpty(studentInfo.getGrade())) {
                try {
                    Integer grade = Integer.parseInt(studentInfo.getGrade());
                    userDetails.setGrade(grade);
                } catch (NumberFormatException e) {
                    //
                    throw new BadRequestException(ErrorCode.USER_GRADE_NOT_FOUND, "User Grade in Not Found in correct format");
                }
            } else if (req.getSignUpFeature() != null && req.getSignUpFeature().equals(FeatureSource.REVISE_JEE_2020_MARCH)) {
                logger.info("REVISEJEE set grade");
            } else {
                throw new BadRequestException(ErrorCode.USER_GRADE_NOT_FOUND, "User Grade in Not Found");
            }
            if (StringUtils.isNotEmpty(studentInfo.getParentFirstName())) {
                userDetails.setParentFirstName(studentInfo.getParentFirstName());
            }
            if (StringUtils.isNotEmpty(studentInfo.getParentLastName())) {
                userDetails.setParentLastName(studentInfo.getParentLastName());
            }
            if (StringUtils.isNotEmpty(studentInfo.getParentFirstName())) {
                String parentName = (studentInfo.getParentFirstName() + " " + StringUtils.defaultIfEmpty(studentInfo.getParentLastName())).trim();
                userDetails.setParentName(parentName);
            }

            userDetails.setBoard(studentInfo.getBoard());
//            if (StringUtils.isNotEmpty(studentInfo.getParentContactNo())) {
//                userDetails.setParentPhoneNo(studentInfo.getParentContactNo());
//            }
            userDetails.setSchool(studentInfo.getSchool());
//            userDetails.setStudentEmail(user.getEmail());
        }

        if (req.getUtm() != null) {
            userDetails.setUtm_campaign(req.getUtm().getUtm_campaign());
            userDetails.setUtm_medium(req.getUtm().getUtm_medium());
            userDetails.setUtm_source(req.getUtm().getUtm_source());
            userDetails.setUtm_term(req.getUtm().getUtm_term());
            userDetails.setUtm_content(req.getUtm().getUtm_content());
            userDetails.setChannel(req.getUtm().getChannel());
        }
        LocationInfo locationInfo = req.getLocationInfo();
        if (locationInfo != null) {
            userDetails.setCity(locationInfo.getCity());
            userDetails.setState(locationInfo.getState());
            userDetails.setCountry(locationInfo.getCountry());
            String address = "";
            if (StringUtils.isNotEmpty(locationInfo.getStreetAddress())) {
                address += locationInfo.getStreetAddress().trim();
            }
            if (StringUtils.isNotEmpty(locationInfo.getStreetAddressLine2())) {
                address += " " + locationInfo.getStreetAddressLine2();
            }
            if (StringUtils.isNotEmpty(address.trim())) {
                userDetails.setAddress(address);
            }
        }

//        LocationInfo userLocationInfo = user.getLocationInfo();
//        if (userLocationInfo != null) {
//            userDetails.setCity(userLocationInfo.getCity());
//        }
        userDetails.setEvent(req.getEvent());
        userDetails.setCategory(getEventCategoryForGrade(userDetails.getGrade(), req.getEvent(), req.getExam()));
        userDetails.setAgentEmail(req.getAgentEmail());
        userDetails.setAgentName(req.getAgentName());
        userDetails.setExamPreps(req.getExamPreps());
        userDetails.setRegisterUrl(req.getRegisterUrl());
        userDetails.setExam(req.getExam());
        userDetails.setSocialSource(req.getSocialSource());
        userDetails.setDob(req.getDob());
        userDetails.setJeeApplicationNumber(req.getJeeApplicationNumber());
        userDetailsDAO.save(userDetails, req.getCallingUserId());
        UserBasicInfo userBasicInfo = new UserBasicInfo(pojoUtils.convertToUserPojo(user), true);

        UserDetailsInfo userInfo = gson.fromJson(gson.toJson(userDetails), UserDetailsInfo.class);
//        UserDetailsInfo userInfo = mapper.map(userDetails, UserDetailsInfo.class);
        userInfo.setStudent(userBasicInfo);
//        UserDetailsInfo userInfo = userDetails.toUserDetailsInfo(userDetails, userBasicInfo);
        triggerSNSUserDetails(userInfo);
        return userInfo;
    }

    public UserDetailsInfo getUserDetailsForISLInfo(Long userId, String event) throws ConflictException, NotFoundException {
        return getUserDetailsForISLInfo(userId, event, null);
    }

    public UserDetailsInfo getUserDetailsForISLInfo(Long userId, String event, String category) throws ConflictException, NotFoundException {
        UserDetails userDetails = userDetailsDAO.getUserDetailsByUserId(userId, event, category);
        if (userDetails == null || !userDetails.getEvent().equals(event)) {
            throw new NotFoundException(ErrorCode.NOT_REGISTERED, "User Not Registered for ISL userId : " + userId);
        }
        List<UserBasicInfo> users = getUserBasicInfos(Arrays.asList(userId), false);
        UserBasicInfo userBasicInfo = null;

        if (ArrayUtils.isNotEmpty(users)) {
            userBasicInfo = users.get(0);
        }
        //UserBasicInfo userBasicInfo = new UserBasicInfo(pojoUtils.convertToUserPojo(user),true);
        UserDetailsInfo userInfo = userDetails.toUserDetailsInfo(userDetails, userBasicInfo);
        return userInfo;
    }

    public IsRegisteredForISLRes isRegisteredForISL(Long userId, String event) {
        if (StringUtils.isEmpty(event)) {
            event = "ISL_2017";
        }
        UserDetails userDetails = userDetailsDAO.getUserDetailsByUserId(userId, event);
        return new IsRegisteredForISLRes((userDetails != null), userId);
    }

    public SignUpForISLRes signUpForISL(SignUpForISLReq req) throws VException {
        VerificationTokenII token = tokenManager.getVerificationTokenById(req.getToken());
        if (token == null) {
            throw new NotFoundException(ErrorCode.INVALID_TOKEN, "Invalid token");
        }
        if (token.getExpiryTime() != null && System.currentTimeMillis() > token.getExpiryTime()) {
            throw new ForbiddenException(ErrorCode.EXPIRED_TOKEN, "Expired token");
        }
        Long userId = token.getUserId();
        String event = req.getEvent();
        if (StringUtils.isEmpty(event)) {
            event = "ISL_2017";
        }
        UserDetails userDetails = userDetailsDAO.getUserDetailsByUserId(userId, event);
        if (userDetails != null) {
            throw new BadRequestException(ErrorCode.ALREADY_REGISTERED_FOR_ISL, "already registered for isl 2017");
        }

        SignUpForISLRes res = new SignUpForISLRes();
        res.setUserId(userId);
        User user = userDAO.getUserByUserId(userId);
        if (Boolean.FALSE.equals(user.getProfileEnabled())) {
            throw new ForbiddenException(ErrorCode.USER_BLOCKED, "User is blocked.");
        }
        if (!user.getEmail().equals(req.getEmail())) {
            res.setResult(SignUpForISLRes.SignUpForISLResResult.EMAILS_NOT_MATCHED);
        } else if (!(passwordEncoder.matches(req.getPassword(), user.getPassword())
                || req.getPassword().equals(user.getPassword()))) {
            if (req.isResetPassword()) {
                String hashedPassword = passwordEncoder.encode(req.getPassword());
                user.setPassword(hashedPassword);
                user.setPasswordHashed(true);
                userDAO.update(user, null);
                res.setResult(SignUpForISLRes.SignUpForISLResResult.PASSWORD_RESET_DONE);
            } else {
                res.setResult(SignUpForISLRes.SignUpForISLResResult.PASSWORDS_NOT_MATCHED);
            }
        } else {
            res.setResult(SignUpForISLRes.SignUpForISLResResult.PASSWORDS_MATCHED);
        }

        return res;
    }

    public List<UserDetailsInfo> getUsersRegisteredForISL(GetUsersReq req) {
        List<UserDetailsInfo> userDetailsInfos = new ArrayList<>();
        List<UserDetails> userDetails = userDetailsDAO.getUserDetails(req.getQuery(), req.getStart(), req.getSize(), req.getFromTime(), req.getTillTime());
        List<Long> userIds = new ArrayList<>();
        if (ArrayUtils.isNotEmpty(userDetails)) {
            for (UserDetails userDetail : userDetails) {
                userIds.add(userDetail.getUserId());
            }
            Map<Long, UserBasicInfo> userMap = getUserBasicInfosMap(userIds, true);

            for (UserDetails userDetail : userDetails) {
                if (userDetail.getUserId() != null && userMap.containsKey(userDetail.getUserId())) {
                    UserDetailsInfo userInfo = userDetail.toUserDetailsInfo(userDetail, userMap.get(userDetail.getUserId()));
                    userDetailsInfos.add(userInfo);
                }
            }
        }
        return userDetailsInfos;
    }

    public Map<Long, UserBasicInfo> getUserBasicInfosMap(List<Long> userIds, boolean exposeEmail) {

        Map<Long, UserBasicInfo> infoMap = getUserBasicInfoFromRedis(userIds);
        Map<String, String> redisMap = new HashMap<>();
        if (infoMap == null) {
            infoMap = new HashMap<>();
        }
        List<Long> fetchList = new ArrayList<>();
        if (ArrayUtils.isNotEmpty(userIds)) {
//        	longIds.removeAll(infoMap.keySet());
            if (!infoMap.isEmpty()) {
                for (Long id : userIds) {
                    if (!infoMap.containsKey(id)) {
                        fetchList.add(id);
                    }
                }
            } else {
                fetchList.addAll(userIds);
            }
        }
        List<User> users = new ArrayList<>();
        if (ArrayUtils.isNotEmpty(fetchList)) {
            users = userDAO.getUserBasicInfoByIds(fetchList, true);
        }
        if (users != null) {
            for (User user : users) {
                if (user != null) {
                    com.vedantu.User.User userPojo = pojoUtils.convertToUserPojo(user);
                    redisMap.put(getUserBasicInfoKey(user.getId()), gson.toJson(new UserBasicInfo(userPojo, true)));

                    UserBasicInfo userBasicInfo = new UserBasicInfo(userPojo, exposeEmail);
                    if (exposeEmail && Role.TEACHER.equals(user.getRole())) {
                        userBasicInfo.setPrimaryCallingNumberForTeacher(ConfigUtils.INSTANCE.getStringValue(
                                "teacher.call.extension." + user.getTeacherInfo().getPrimaryCallingNumberCode()));
                    }

                    if (Role.TEACHER.equals(user.getRole())) {
                        if (user.getTeacherInfo() != null && user.getTeacherInfo().getDexInfo() != null) {
                            userBasicInfo.setTeacherPoolType(user.getTeacherInfo().getDexInfo().getTeacherPoolType());
                        }
                        if (user.getTeacherInfo() != null && user.getTeacherInfo().getSliderPicUrl() != null) {
                            userBasicInfo.setTeacherSliderPicUrl(user.getTeacherInfo().getSliderPicUrl());
                        }
                    }

                    infoMap.put(user.getId(), userBasicInfo);
                }
            }
            if (!redisMap.isEmpty()) {
                try {
                    redisDAO.setexKeyPairs(redisMap, redisExpiry);
                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    logger.info("Problem in redis set");
                }
            }
        }
        //The above logic is too complicated, just looping throught the infomap once again to ensure
        //sensitive info is not sent
        if (!infoMap.isEmpty() && !exposeEmail) {
            for (UserBasicInfo uInfo : infoMap.values()) {
                uInfo.purgeEmail();
            }
        }
        return infoMap;
    }

    public List<AttemptedUserIds> getUserIdsRegisteredForISL() {

        List<AttemptedUserIds> categoryUserIds = userDetailsDAO.getUsersRegisteredForISL();
        if (categoryUserIds == null) {
            categoryUserIds = new ArrayList<>();
        }
        return categoryUserIds;
    }

    public UserDetailsInfo editUserDetailsForEvent(EditUserDetailsReq req) throws VException {
        UserDetailsInfo userDetailsInfo = new UserDetailsInfo();

        if(req.getUserId() == null)
            req.setUserId(sessionUtils.getCallingUserId());

        UserDetails userDetails = userDetailsDAO.getUserDetailsByUserId(req.getUserId(), req.getEvent());
        if (userDetails == null) {
            throw new NotFoundException(ErrorCode.NOT_REGISTERED, "User Not Registered for ISL userId : " + req.getUserId());
        }

        User user = userDAO.getUserByUserId(req.getUserId());
        if (user == null) {
            throw new NotFoundException(ErrorCode.USER_NOT_FOUND, "No user found for userId : " + req.getUserId());
        }
        Boolean userChange = Boolean.FALSE;
        if (StringUtils.isNotEmpty(req.getEmail())) {
            User emailUser = userDAO.getUserByEmail(req.getEmail());
            if (null != emailUser && null == user.getEmail()) {
                throw new BadRequestException(ErrorCode.EMAIL_ALREADY_EXISTS, "Email already exists");
            }
            if(StringUtils.isNotEmpty(req.getEmail())) {
                if(CommonUtils.isValidEmailId(req.getEmail())) {
                    user.setEmail(req.getEmail());
                    userDetails.setStudentEmail(req.getEmail());

                }
                else{
                    throw new BadRequestException(ErrorCode.INVALID_EMAILID, "email id is not valid");
                }
            }
            userChange = Boolean.TRUE;
        }
        if (StringUtils.isNotEmpty(req.getStudentName())) {
            userDetails.setStudentName(req.getStudentName());
            user.setFirstName(req.getStudentName());
            user.setLastName("");
            user.setFullName(req.getStudentName());
            userChange = Boolean.TRUE;
        }
        if (req.getGrade() != null && !userDetails.getGrade().equals(req.getGrade())) {
            logger.info("userDetail grade modified for " + userDetails.getUserId() + " from "
                    + userDetails.getGrade() + " to " + req.getGrade());
            userDetails.setGrade(req.getGrade());

        }
        if (StringUtils.isNotEmpty(req.getSchoolName())) {
            userDetails.setSchool(req.getSchoolName());
            if (user.getStudentInfo() == null) {
                user.setStudentInfo(new StudentInfo());
            }
            user.getStudentInfo().setSchool(req.getSchoolName());
            userChange = Boolean.TRUE;
        }
        if (StringUtils.isNotEmpty(req.getPhoneCode())) {
            userDetails.setParentPhoneCode(req.getPhoneCode());
            userDetails.setStudentPhoneCode(req.getPhoneCode());
        }

        if (StringUtils.isNotEmpty(req.getParentPhoneNo())) {
            userDetails.setParentPhoneNo(req.getParentPhoneNo());
            userDetails.setStudentPhoneNo(req.getParentPhoneNo());
        }

        if (StringUtils.isNotEmpty(req.getCity())) {
            userDetails.setCity(req.getCity());
        }

        if (StringUtils.isNotEmpty(req.getState())) {
            userDetails.setState(req.getState());
        }

        if (StringUtils.isNotEmpty(req.getCountry())) {
            userDetails.setCountry(req.getCountry());
        }

        if (StringUtils.isNotEmpty(req.getBoard())) {
            userDetails.setBoard(req.getBoard());
            if (user.getStudentInfo() == null) {
                user.setStudentInfo(new StudentInfo());
            }
            user.getStudentInfo().setBoard(req.getBoard());
            userChange = Boolean.TRUE;
        }

        if (StringUtils.isNotEmpty(req.getParentFirstName())) {
            userDetails.setParentFirstName(req.getParentFirstName());
            if (user.getStudentInfo() == null) {
                user.setStudentInfo(new StudentInfo());
            }
            //user.getStudentInfo().setParentFirstName(req.getParentFirstName());
            userChange = Boolean.TRUE;
        }
        if (StringUtils.isNotEmpty(req.getParentLastName())) {
            userDetails.setParentLastName(req.getParentLastName());
            if (user.getStudentInfo() == null) {
                user.setStudentInfo(new StudentInfo());
            }
            //user.getStudentInfo().setParentLastName(req.getParentLastName());
            userChange = Boolean.TRUE;
        }
        if (StringUtils.isNotEmpty(req.getParentFirstName()) || StringUtils.isNotEmpty(req.getParentLastName())) {
            String parentName = (userDetails.getParentFirstName() + " " + StringUtils.defaultIfEmpty(userDetails.getParentLastName())).trim();
            if (StringUtils.isNotEmpty(parentName)) {
                userDetails.setParentName(parentName);
            }
        }
        if (req.getExamPreps() != null) {
            userDetails.setExamPreps(req.getExamPreps());
        }
        if (StringUtils.isNotEmpty(req.getExam())) {
            userDetails.setExam(req.getExam());
        }

        if (ArrayUtils.isNotEmpty(req.getParentInfo())) {
            userDetails.setParentInfo(req.getParentInfo());
        }

        if (req.getGender() != null) {
            userDetails.setGender(req.getGender());
        }

        userDetails.setAchievementDocments(req.getAchievementDocments());
        userDetails.setAchievements(req.getAchievements());

        if (ArrayUtils.isNotEmpty(req.getIdproofs())) {
            userDetails.setIdproofs(req.getIdproofs());
        }

        if (req.getJeeMainPercentile() != null) {
            userDetails.setJeeMainPercentile(req.getJeeMainPercentile());
        }

        userDetails.setCategory(getEventCategoryForGrade(userDetails.getGrade(), userDetails.getEvent(), userDetails.getExam()));

        logger.info("userDetail modified for " + userDetails.getUserId());
        userDetailsDAO.save(userDetails, req.getCallingUserId());
        if (userChange) {
            logger.info("userTable modified for " + user.getId());
            userDAO.update(user, req.getCallingUserId());
        }
        List<UserBasicInfo> users = getUserBasicInfos(Arrays.asList(userDetails.getUserId()), true);
        UserBasicInfo userBasicInfo = null;

        if (ArrayUtils.isNotEmpty(users)) {
            userBasicInfo = users.get(0);
        }
        userDetailsInfo = userDetails.toUserDetailsInfo(userDetails, userBasicInfo);
        triggerSNSUserDetails(userDetailsInfo);

        return userDetailsInfo;
    }

    public void triggerSNSUserDetails(UserDetails userDetails) {
        List<UserBasicInfo> users = getUserBasicInfos(Arrays.asList(userDetails.getUserId()), true);
        UserBasicInfo userBasicInfo = null;

        if (ArrayUtils.isNotEmpty(users)) {
            userBasicInfo = users.get(0);
        }
        UserDetailsInfo userDetailsInfo = userDetails.toUserDetailsInfo(userDetails, userBasicInfo);
        triggerSNSUserDetails(userDetailsInfo);
    }

    public void triggerSNSUserDetails(UserDetailsInfo userInfo) {
        UserDetailsForSNS user = new UserDetailsForSNS();
        UserBasicInfo userBasicInfo = userInfo.getStudent();
        if (userBasicInfo != null) {
            user.setId(userBasicInfo.getUserId());
            user.setEmail(userBasicInfo.getEmail());
            user.setFullName(userBasicInfo.getFullName());
            user.setFirstName(userBasicInfo.getFirstName());
            user.setLastName(userBasicInfo.getLastName());
            user.setCategory(userInfo.getCategory());

            Map<String, Object> payload = new HashMap<>();
            payload.put("user", user);
            AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.USER_UPDATION_TRIGGER_ISL, payload);
            asyncTaskFactory.executeTask(params);
        }
        //awsSQSManager.sendToSQS(SQSQueue.CLEVERTAP_QUEUE, SQSMessageType.USER_DETAILS_REGISTRATION, gson.toJson(userInfo));
    }

    public void triggerUserUpdationSNSForISL(UserDetailsForSNS user) {

        // Commenting for now to not trigger SNS
        /*

        AmazonSNSClient snsClient = new AmazonSNSClient();
        snsClient.setRegion(Region.getRegion(Regions.AP_SOUTHEAST_1));
        String topicArn = "arn:aws:sns:ap-southeast-1:" + arn + ":USER_EVENTS_" + env.toUpperCase();
        // String msg = "{\"sessionId\":\"" + sessionId + "\"}";
        String msg = new Gson().toJson(user);
        String snsSubject = UserEvents.USER_UPDATED_ISL.toString();
        PublishRequest publishRequest = new PublishRequest(topicArn, msg, snsSubject);
        PublishResult publishResult = snsClient.publish(publishRequest);
        logger.info("Published SNS, MessageId - " + publishResult.getMessageId());
         */
    }

    public String getEventCategoryForGrade(Integer grade, String event, String exam) throws VException {
        if (grade == null) {
            throw new NotFoundException(ErrorCode.USER_CATEGORY_NOT_FOUND, "Category not found for grade :" + grade);
        }

        if(StringUtils.isEmpty(event)){
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR,"event code not found");
        }

        Set<String> allowedExams = new HashSet<>(Arrays.asList("JEE", "NEET"));

        VsatEventDetailsPojo vsatEventDetailsPojo = fosUtils.getVsatEventDetails();
        if(vsatEventDetailsPojo.getEventName().equals(event)){
            if (grade == 4 || grade == 5) {
                return event + "_CAT_4_5";
            }
            else if (grade == 6 || grade == 7 || grade == 8) {
                return event + "_CAT_6_8";
            }
            else if (grade == 9 || grade == 10) {
                return event + "_CAT_9_10";
            }
            else if (StringUtils.isEmpty(exam) || !allowedExams.contains(exam)) {
                throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Exam name is null/not allowed");
            }
            else if (grade == 11 || grade == 12 || grade == 13) {
                return event + "_CAT_" + exam + "_" + grade;
            }
            else {
                throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Grade Not Allowed");
            }
        }
        else {
            switch (event) {
                case "ISL_2017":
                    if (grade >= 3 && grade <= 5) {
                        return ("ISL_2017_CAT_3_5");
                    } else if (grade >= 6 && grade <= 8) {
                        return ("ISL_2017_CAT_6_8");
                    } else if (grade >= 9 && grade <= 10) {
                        return ("ISL_2017_CAT_9_10");
                    } else {
                        throw new NotFoundException(ErrorCode.USER_CATEGORY_NOT_FOUND, "Category not found for grade :" + grade);
                    }
                case "VOLT_2020_JAN":
                    if (grade >= 4 && grade <= 10) {
                        return ("VOLT_2020_JAN_CAT_" + grade);
                    } else if ((StringUtils.isEmpty(exam) || !allowedExams.contains(exam)) && (grade == 11 || grade == 12)) {
                        return ("VOLT_2020_JAN_CAT_" + grade);
                    } else if (grade >= 11 && grade <= 12) {
                        return ("VOLT_2020_JAN_CAT_" + exam + "_" + grade);
                    } else {
                        throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Grade Not Allowed");
                    }
                case "REVISEINDIA_2020_FEB":
                    return "REVISEINDIA_2020_FEB_1";
                case "REVISE_JEE_2020_MARCH":
                    return "REVISE_JEE_2020_MARCH_1";
                case "TARGET_JEE_NEET":
                    return "TARGET_JEE_NEET";
                case "JRP_SEP_2019":
                    return ("JRP_SEP_2019_CAT_" + grade);
                default:
                    throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "unsupported event");
            }
        }
    }

    public GetUsersRes getPasswordEmptyUsersForISL(Integer start,
            Integer size) throws VException {
        GetUsersRes res = new GetUsersRes();
        List<User> users = userDAO.getPasswordEmptyUsersForISL(start, size);
        if (users != null) {
            List<Long> userIds = new ArrayList<>();
            for (User user : users) {
                userIds.add(user.getId());
            }
            Map<Long, String> tokensMap = verificationTokenDAO.getPasswordResetTokens(userIds);
            for (User user : users) {
                UserInfo userInfo = new UserInfo(pojoUtils.convertToUserPojo(user), null, false);
                userInfo.setUserId(user.getId());
                //hacky but will prevent from creating another extra pojo
                userInfo.setReferralCode(tokensMap.get(user.getId()));
                res.addUser(userInfo);
            }
        }
        return res;
    }

    public UserDetailsForSNS loginISL(SignInReq req, String event) throws VException {
        com.vedantu.User.User user = authenticateUser(req);
        UserDetailsForSNS userInfo = mapper.map(user, UserDetailsForSNS.class);
        if (StringUtils.isEmpty(event)) {
            event = "ISL_2017";
        }
        UserDetails userDetails = userDetailsDAO.getUserDetailsByUserId(user.getId(), event);
        if (userDetails != null) {
            userInfo.setCategory(userDetails.getCategory());
            if (userInfo.getGrade() != null) {
                userInfo.setGrade(userInfo.getGrade());
            }
        }
        if (StringUtils.isEmpty(userInfo.getGrade()) && user != null && user.getStudentInfo() != null && StringUtils.isNotEmpty(user.getStudentInfo().getGrade())) {
            userInfo.setGrade(user.getStudentInfo().getGrade());
        }
        return userInfo;
    }

    public UserDetailsForSNS getUserByIdForISL(Long userId, String event) throws VException {
        User user = getUserByUserId(userId);
        if (user == null) {
            throw new NotFoundException(ErrorCode.USER_NOT_FOUND,
                    "no user found with userId:" + userId);
        }
        if (StringUtils.isEmpty(event)) {
            event = "ISL_2017";
        }
        UserDetailsForSNS userInfo = mapper.map(user, UserDetailsForSNS.class);
        UserDetails userDetails = userDetailsDAO.getUserDetailsByUserId(user.getId(), event);
        if (userDetails != null) {
            userInfo.setCategory(userDetails.getCategory());
            if (userInfo.getGrade() != null) {
                userInfo.setGrade(userInfo.getGrade());
            }
        }
        if (StringUtils.isEmpty(userInfo.getGrade()) && user.getStudentInfo() != null && StringUtils.isNotEmpty(user.getStudentInfo().getGrade())) {
            userInfo.setGrade(user.getStudentInfo().getGrade());
        }
        return userInfo;
    }

    public GeneratePasswordRes generatePassword(GeneratePasswordReq req) throws VException, IOException {
        req.verify();
        if (StringUtils.isNotEmpty(req.getEmail())
                && restrictedResetPasswordAccounts.contains(req.getEmail().toLowerCase().trim())
                && !"J2TucPn2wZnRnPEU".equals(req.getAdminSecurity())) {
            throw new BadRequestException(ErrorCode.ACTION_NOT_ALLOWED, "Trying to change password for restricted account");
        }
        User user = getUserByEmail(req.getEmail());
        if (user == null) {
            throw new NotFoundException(ErrorCode.EMAIL_NOT_REGISTERED, "The email is not registered");
        }
        return _generatePassword(user);
    }

    public GeneratePasswordRes _generatePassword(User user) throws VException, IOException {
        String newPassword = StringUtils.randomAlphaNumericString(6);
        String hashedPassword = passwordEncoder.encode(newPassword);
        user.setPassword(hashedPassword);
        user.setPasswordHashed(true);
        user.setPasswordAutogenerated(Boolean.TRUE);
        user.setPasswordChangedAt(System.currentTimeMillis());
        user.addRecentHashedPassword(hashedPassword);
        userDAO.update(user, null);
        GeneratePasswordRes res = new GeneratePasswordRes();
        res.setNewPassword(newPassword);
        res.setEmail(user.getEmail());
        res.setContactNumber(user.getContactNumber());
        res.setPhoneCode(user.getPhoneCode());
        res.setFullName(user.getFullName());
        res.setRole(user.getRole());
        Map<String, Object> payload = new HashMap<>();
        payload.put("generatePasswordRes", res);
        AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.GENERATE_PASSWORD_EMAIL, payload);
        asyncTaskFactory.executeTask(params);
        return res;
    }

    public PlatformBasicResponse getUserISLStatus(String email, String event) throws VException {
        User user = getUserByEmail(email);
        String response;
        if (user == null) {
            response = "NOT_REGISTERED_ON_VEDANTU";
        } else {
            if (StringUtils.isEmpty(event)) {
                event = "ISL_2017";
            }
            UserDetails userDetails = userDetailsDAO.getUserDetailsByUserId(user.getId(), event);
            if (userDetails == null) {
                response = "NOT_REGISTERED_FOR_ISL";
            } else {
                response = "REGISTERED_FOR_ISL";
            }
        }
        return new PlatformBasicResponse(true, response, null);
    }

    public UserDetailsForSNS getUserDetailsFromISL(String email, String event) throws ForbiddenException {
        User user = userDAO.getUserByEmail(email);

        if (null == user) {
            throw new ForbiddenException(ErrorCode.EMAIL_NOT_REGISTERED, "No user found for the specified email id.");
        }
        if (StringUtils.isEmpty(event)) {
            event = "ISL_2017";
        }
        //com.vedantu.User.User userPojo = pojoUtils.convertToUserPojo(user);
        UserDetailsForSNS userInfo = mapper.map(user, UserDetailsForSNS.class);
        UserDetails userDetails = userDetailsDAO.getUserDetailsByUserId(user.getId(), event);
        if (userDetails != null) {
            userInfo.setCategory(userDetails.getCategory());
            if (userInfo.getGrade() != null) {
                userInfo.setGrade(userInfo.getGrade());
            }
        }
        if (StringUtils.isEmpty(userInfo.getGrade()) && user != null && user.getStudentInfo() != null && StringUtils.isNotEmpty(user.getStudentInfo().getGrade())) {
            userInfo.setGrade(user.getStudentInfo().getGrade());
        }
        return userInfo;
    }

    public Map<Long, UserBasicInfo> getUserBasicInfoFromRedis(List<Long> ids) {

        Map<Long, UserBasicInfo> infoMap = new HashMap<>();
        if (ArrayUtils.isNotEmpty(ids)) {
            List<String> keys = new ArrayList<>();
            for (Long id : ids) {
                if (id != null) {
                    keys.add(getUserBasicInfoKey(id));
                }
            }
            Map<String, String> valueMap = new HashMap<>();
            try {
                valueMap = redisDAO.getValuesForKeys(keys);
            } catch (Exception e) {
                // TODO Auto-generated catch block
                logger.info("Error in Redis getValuesForKeys");
            }

            if (valueMap != null && !valueMap.isEmpty()) {
                for (Long id : ids) {
                    if (id != null) {
                        String value = valueMap.get(getUserBasicInfoKey(id));
                        if (StringUtils.isNotEmpty(value)) {
                            UserBasicInfo userBasicInfo = gson.fromJson(value, UserBasicInfo.class);
                            if (userBasicInfo != null) {
                                infoMap.put(id, userBasicInfo);
                            }
                        }
                    }
                }
            }
        }

        return infoMap;
    }

    public String getUserBasicInfoKey(Long id) {
        return env + "_USER_BASIC_" + id;
    }

    public GetUserDashboardRes getUserDashboard(GetUserDashboardReq req) throws VException {
        req.verify();
        GetUserDashboardRes userDashboard = new GetUserDashboardRes();
        UserInfo userInfo;
        if (req.getUserId() != null) {
            userInfo = getUserInfo(req.getUserId(), true);
        } else {
            User u = getUserByEmail(req.getEmail());
            userInfo = new UserInfo(pojoUtils.convertToUserPojo(u), null, true);
        }
        userDashboard.setUserInfo(userInfo);

        ClientResponse resDinero = WebUtils.INSTANCE.doCall(DINERO_ENDPOINT + "/account/getUserDashboardAccountInfo?userId=" + req.getUserId(), HttpMethod.GET,
                null, true);
        VExceptionFactory.INSTANCE.parseAndThrowException(resDinero);
        String jsonStringDinero = resDinero.getEntity(String.class);
        GetUserDashboardAccountInfoRes getUserDashboardAccountInfoRes = gson.fromJson(jsonStringDinero, GetUserDashboardAccountInfoRes.class);
        logger.info("Response from dinero " + getUserDashboardAccountInfoRes);
        userDashboard.setAccountInfo(getUserDashboardAccountInfoRes);

        ClientResponse respScheduling = WebUtils.INSTANCE.doCall(SCHEDULING_ENDPOINT + "/session/getUserDashboardSessionInfo?userId=" + req.getUserId(), HttpMethod.GET,
                null, true);
        VExceptionFactory.INSTANCE.parseAndThrowException(respScheduling);
        String jsonStringScheduling = respScheduling.getEntity(String.class);
        GetUserDashboardSessionInfoRes getUserDashboardSessionInfoRes = gson.fromJson(jsonStringScheduling, GetUserDashboardSessionInfoRes.class);
        logger.info("Response from scheduling " + getUserDashboardSessionInfoRes);
        userDashboard.setSessionInfo(getUserDashboardSessionInfoRes);

        return userDashboard;
    }

    public PlatformBasicResponse getRandomUserName() {
        int skip = new Random().nextInt(1000) + 1;
        User user = userDAO.getUserWithSkip(skip);
        PlatformBasicResponse res = new PlatformBasicResponse();
        if (user != null) {
            res.setResponse(user.getFirstName());
        }
        return res;
    }

    public List<UserInfo> getUserInfosByEmail(GetUserInfosByEmailReq req) throws BadRequestException {
        if (ArrayUtils.isEmpty(req.getEmailIds()) && ArrayUtils.isEmpty(req.getUserIds())) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "No valid input found");
        }
        Set<String> emailIds = new HashSet<>();
        Query query = new Query();
        if (ArrayUtils.isNotEmpty(req.getEmailIds())) {
            for (String mail : req.getEmailIds()) {
                if (!CustomValidator.validEmail(mail)) {
                    throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Some Invalid mail found");
                }
                emailIds.add(mail.trim().toLowerCase());
            }
            query.addCriteria(Criteria.where(User.Constants.EMAIL).in(emailIds));

        }
        if (ArrayUtils.isNotEmpty(req.getUserIds())) {
            query.addCriteria(Criteria.where(User.Constants.ID).in(req.getUserIds()));
        }
        List<User> users = userDAO.runQueryUserInfo(query);
        List<UserInfo> userInfos = new ArrayList<>();
        if (ArrayUtils.isNotEmpty(users)) {
            for (User user : users) {
                UserInfo info = new UserInfo(pojoUtils.convertToUserPojo(user), null, true);
                info.setTeacherInfo(user.getTeacherInfo());
                userInfos.add(info);
            }
        }
        return userInfos;
    }

    public List<com.vedantu.User.User> fetchUsersByCreationTime(GetUsersReq req) {

        Query query = new Query();
        List<Sort.Order> orderList = new ArrayList<>();
        orderList.add(new Sort.Order(Sort.Direction.ASC, User.Constants.CREATION_TIME));
        query.with(Sort.by(orderList));
        Long fromTime = req.getFromTime();
        Long tillTime = req.getTillTime();

        if ((fromTime != null && fromTime > 0)
                || (tillTime != null && tillTime > 0)) {

            Criteria criteria = null;
            if (fromTime != null && fromTime > 0) {
                criteria = Criteria.where(User.Constants.CREATION_TIME).gte(fromTime);
            }

            if (tillTime != null && tillTime > 0) {
                if (criteria != null) {
                    criteria = criteria.andOperator(Criteria.where(User.Constants.CREATION_TIME).lt(tillTime));
                } else {
                    criteria = Criteria.where(User.Constants.CREATION_TIME).lt(tillTime);
                }
            }
            query.addCriteria(criteria);
        }
        if (req.getRole() != null) {
            query.addCriteria(Criteria.where(User.Constants.ROLE).is(req.getRole()));
        }

        userDAO.setFetchParameters(query, req.getStart(), req.getSize());

        List<User> users = userDAO.runQuery(query, User.class);
        List<com.vedantu.User.User> results = new ArrayList<>();
        if (!CollectionUtils.isEmpty(users)) {
            users.forEach(u -> results.add(pojoUtils.convertToUserPojo(u)));
        }
        return results;
    }

    public void updateSalesClosedInfo(String orderString) throws ConflictException {
        Orders order = null;
        if (StringUtils.isNotEmpty(orderString)) {
            order = gson.fromJson(orderString, Orders.class);
        }

        if (order != null && order.getUserId() != null) {
            Long userId = order.getUserId();

            Integer promotionalPaid = order.getPromotionalAmountPaid();
            Integer nonPromotionalPaid = order.getNonPromotionalAmountPaid();

            if (nonPromotionalPaid == null || nonPromotionalPaid <= 0) {
                nonPromotionalPaid = order.getNonPromotionalAmount();
                promotionalPaid = order.getPromotionalAmount();
            }
            if (nonPromotionalPaid == null || nonPromotionalPaid <= 0) {
                logger.info("nonPromotionalPaid is empty in Order:" + " userId: " + userId);
                return;
            }
            logger.info("NP Paid : " + nonPromotionalPaid + " P Paid : " + promotionalPaid);
            EntityModel model = null;
            List<OrderedItem> items = order.getItems();
            if (ArrayUtils.isNotEmpty(items)) {
                for (OrderedItem item : items) {
                    if (entityTypes.contains(item.getEntityType())) {
                        logger.info("Registration Order: Type " + item.getEntityType()
                                + " entityId: " + item.getEntityId() + " userId: " + userId);
                        return;
                    }
                }
                User user = userDAO.getUserByUserId(userId);
                if (user != null) {
                    if (user.getSaleClosed() != null && user.getSaleClosed().getClosedTime() != null) {
                        logger.info("Sale already closed for userId:" + userId);
                        return;
                    }
                    Set<EntityType> otmTypes = new HashSet<>();
                    otmTypes.add(EntityType.BUNDLE);
                    otmTypes.add(EntityType.BUNDLE_PACKAGE);
                    otmTypes.add(EntityType.OTF);
                    otmTypes.add(EntityType.OTM_BUNDLE);
                    otmTypes.add(EntityType.OTM_BUNDLE_ADVANCE_PAYMENT);
                    if (otmTypes.contains(items.get(0).getEntityType())) {
                        model = EntityModel.OTM;
                    } else {
                        model = EntityModel.OTO;
                    }
                    SaleClosed sale = new SaleClosed(order.getCreationTime(), model, promotionalPaid, nonPromotionalPaid,
                            items.get(0).getEntityType(), items.get(0).getEntityId(), order.getId());
                    sale.setVedantuDiscount(items.get(0).getVedantuDiscountAmount());

                    user.setSaleClosed(sale);
                    userDAO.update(user, null);
                    logger.info("Sales closed updated for user : " + userId + " SalesClosed : " + sale.toString());
                    user.setPassword(null);
                    // not async as it is generated from sns
                    awsSQSManager.sendToSQS(SQSQueue.LEADSQUARED_QUEUE, SQSMessageType.SALE_CLOSED, gson.toJson(user), user.getId().toString() + "_SALE_CLOSED");
                }
            }
        } else {
            logger.info("order is null :" + orderString);
        }

    }

    public PlatformBasicResponse updateUserSaleClosed(SaleClosedUser req) throws ConflictException {
        logger.info("UPDATEUSERSALECLOSED");
        logger.info(req);
        if (StringUtils.isNotEmpty(req.getUserEmail())) {
            User user = userDAO.getUserByEmail(req.getUserEmail());
            SaleClosed sc = user.getSaleClosed();
            if (sc == null) {
                sc = new SaleClosed();
            }
            sc.setOwnerName(req.getOwnerName());
            sc.setOwnerEmail(req.getOwnerEmail());
            sc.setLastWebinarAttended(req.getLastWebinarAttended());
            user.setSaleClosed(sc);
            userDAO.update(user, user.getId());
            logger.info(user);
        }
        return new PlatformBasicResponse();
    }

    public void updateUserLocationFrommIp(User user, String ip, Long callingUser) throws ConflictException {
        LocationInfo lInfo = ipUtil.getLocationFromIp(ip);
        if (lInfo != null) {
            user.setLocationInfo(lInfo);
            userDAO.update(user, callingUser);
        }
    }

    public PlatformBasicResponse resetadminpasswordsAsync() {
        AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.RESET_ADMIN_PASSWORDS, new HashMap<>());
        asyncTaskFactory.executeTask(params);
        return new PlatformBasicResponse();
    }

    public void resetadminpasswords() {
        List<User> users = userDAO.getAdminsForPasswordReset();
        if (ArrayUtils.isNotEmpty(users)) {
            logger.info("resetting password for " + users.size());
            for (User user : users) {
                if (!resetPasswordExceptions.contains(user.getEmail())) {

                    logger.info("resetting password for " + user.getEmail());
                    try {
                        GeneratePasswordRes res = _generatePassword(user);
                        logger.info(res);
                    } catch (Exception e) {
                        logger.error("error in resetting password for admin " + user.getEmail());
                    }
                }
            }
        }
    }

    public void markTelegramLinked(Long userId) throws ConflictException {
        User user = userDAO.getUserByUserId(userId);
        user.setTelegramLinked(true);
        userDAO.update(user, userId);
    }

    public List<com.vedantu.User.User> getDexTeachers(GetDexTeachersReq req) {
        List<User> users = userDAO.getDexUsers(req);

        List<com.vedantu.User.User> result = new ArrayList<>();
        for (User user : users) {
            result.add(pojoUtils.convertToUserPojo(user));
        }

        return result;

    }

    public List<GetDexAndTheirStatusRes> getDexAndTheirStatus(GetDexTeachersReq req) {
        return userDAO.getDexAndTheirStatus(req);
    }

    public PlatformBasicResponse getXVedToken(String email) throws VException, IOException,
            URISyntaxException {
        com.vedantu.User.User user = mapper.map(userDAO.getUserByEmail(email), com.vedantu.User.User.class);
        String token = sessionUtils.getXVedToken(user);
        PlatformBasicResponse res = new PlatformBasicResponse();
        res.setResponse(token);
        return res;
    }

    public List<UserBasicInfo> getAllAcadMentors(Integer start, Integer size) {
        List<User> users = userDAO.getAllAcadMentors(start, size);
        List<UserBasicInfo> results = new ArrayList<>();
        if (!CollectionUtils.isEmpty(users)) {
            users.forEach(u -> results.add(new UserBasicInfo(pojoUtils.convertToUserPojo(u), true)));
        }
        return results;
    }

    public List<User> getAllAcadMentorsUserObject(Integer start, Integer size) {
        List<User> users = userDAO.getAllAcadMentors(start, size);

        return users;
    }

    @PreDestroy
    public void cleanUp() {
        try {
            if (snsClient != null) {
                snsClient.shutdown();
            }
        } catch (Exception e) {
            logger.error("Error in closing snsclient connection ", e);
        }
    }

    public List<TeacherInfoMinimal> getBasicTeacherInfoByIds(List<Long> userIds) {
        return userDAO.getBasicTeacherInfoByIds(userIds);
    }

    public UserLoginToken getTestLinkByuserLoginTokenId(String userLoginTokenId) throws NotFoundException {
        return userLoginTokenDAO.getById(userLoginTokenId);
    }

    public PlatformBasicResponse editUserRoleByUserId(Long userId, Role role) throws VException {
        User user = getUserByUserId(userId);
        if (user == null) {
            throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, "User does not exist");
        }
        if (user.getRole() != role) {
            List<Role> roleList = Arrays.asList(Role.ADMIN, Role.STUDENT_CARE, Role.SEO_MANAGER);
            if (role != null && user.getRole() != null && roleList.contains(role) && roleList.contains(user.getRole())) {
                user.setRole(role);
                userDAO.update(user, null);
            } else {
                throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Not Allowed to change " + user.getRole() + " role to " + role);
            }
        }
        return new PlatformBasicResponse();
    }

    public PlatformBasicResponse changeEmailByUserId(Long userId, String email) throws VException {
        User user = getUserByUserId(userId);
        if (user == null) {
            throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, "User does not exist");
        }
        if (StringUtils.isNotEmpty(email) && CustomValidator.validEmail(email.trim())) {
            User emailExistsUser = userDAO.getUserByEmail(email.trim());
            if(emailExistsUser == null){
                user.setEmail(email);
                user.setIsEmailVerified(false);
                userDAO.update(user, null);
            }else{
                throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "given Email : " + email + " already Exist");
            }
        } else {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Email : " + email + " not valid");
        }
        return new PlatformBasicResponse();
    }

    /**
     * Pre Login Verification
     */
    public UserPreLoginVerificationInfoRes preLoginVerification(UserPreLoginVerificationInfoReq req,
           HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws VException, UnsupportedEncodingException {

        Boolean sendOTP = true;

        // get auth token, if a web request
        if (null == req.getRequestSource() || req.getRequestSource().equals(RequestSource.WEB)) {
            String authToken = getAuthTokenFromReq(httpServletRequest);
            if(!verifyLoginAuthToken(authToken)){
                logger.info("auth token cookie value is, {}", authToken);
                //sendOTP = false;
            }
        }

        req.verify();
        UserPreLoginVerificationInfoRes userPreLoginVerificationInfoRes = new UserPreLoginVerificationInfoRes();

        logger.info("PreLoginVerification Req : {}", req);
        if(null == req.getRequestSource() || !(req.getRequestSource().equals(RequestSource.IOS) || req.getRequestSource().equals(RequestSource.ANDROID))){
                if(null == req.getVersion() || !req.getVersion().equals("2")){
                            logger.info("Not sending the OTP " +  req);
                            sendOTP = false;
                }
        }

        if(sendOTP) {
            try {
                UserPreLoginDataPojo data = new UserPreLoginDataPojo();
                data.setIpAddress(req.getIpAddress());
                data.setLoginParam(req.getEmail() + "" + req.getPhoneCode() + "" + req.getPhoneNumber());
                awsSQSManager.sendToSQS(SQSQueue.PRE_LOGIN_DATA_QUEUE, SQSMessageType.PRE_LOGIN_DATA, new Gson().toJson(data));
            } catch (Exception e) {
                logger.info("Error in sending login data" + e);
            }

            // verify google recaptcha
            if (null == req.getRequestSource() || req.getRequestSource().equals(RequestSource.WEB)) {
                boolean allowLogin = botFilter.verifyGoogleRecaptcha(req.getToken(), req.getIpAddress());
                if (!allowLogin) {
                    logger.info("Recaptcha Verification Failed for req : {} ", req);
                }
            }


            // isp verify check
            LocationInfo locationInfo = null;
            if (sendOTP) {
                try {
                    String ispCheck = redisDAO.get("ISP_CHECK_PRE_LOGIN");
                    if (null != ispCheck) {
                        locationInfo = ipUtil.getLocationFromIp(req.getIpAddress());
                        String isp = locationInfo.getIsp();
                        if (null != isp && null != locationInfo.getCountry() && !locationInfo.getCountry().equalsIgnoreCase("India")) {
                            String noOTPExceptAllowedCountries = redisDAO.get("NO_OTP_EXCEPT_ALLOWED_COUNTRIES");
                            if (null == noOTPExceptAllowedCountries) {
                                String loginParam = req.getEmail() + "" + req.getPhoneCode() + "" + req.getPhoneNumber();
                                sendOTP = botFilter.sendPreLoginOTP(loginParam, isp);
                            } else {
                                if (!allowedCountriesList.contains(locationInfo.getCountry())) {
                                    sendOTP = false;
                                }
                            }
                        }
                    }
                } catch (Exception e) {
                    // swallow
                }
            }


            try {
                //login rate limit
                boolean exceeded = preLoginRateFilter(req.getEmail(), req.getPhoneNumber(), req.getPhoneCode());

                if (exceeded) {
                    String key = "PRE_LOGIN_BLOCKED_ISP_NEW";
                    if (null != locationInfo && null != locationInfo.getIsp() && !"India".equalsIgnoreCase(locationInfo.getCountry())) {
                        redisDAO.addToSet(key, locationInfo.getIsp());
                    }
                    throw new BadRequestException(ErrorCode.RATE_LIMIT_EXCEEDED, "Tried logging in too many times");
                }
            } catch (Exception e) {
                // swallow
            }
        }

        boolean userLoginWithPhone = false;
        boolean userLoginWithEmail = false;

        // If both email and phone are available, send response as per phone.
        if (null != req.getPhoneCode() && null != req.getPhoneNumber()
                && !StringUtils.isEmpty(req.getPhoneCode()) && !StringUtils.isEmpty(req.getPhoneNumber())) {
            userLoginWithPhone = true;
        } else if (null != req.getEmail() || StringUtils.isNotEmpty(req.getEmail())) {
            userLoginWithEmail = true;
        }

        if (req.getCouncellerAgent()) {
            userLoginWithPhone = false;
        }

        if (userLoginWithPhone) {
            userPreLoginVerificationInfoRes =  preLoginVerificationPhone(req, sendOTP);
            if(!userPreLoginVerificationInfoRes.getEmailSent() && !userPreLoginVerificationInfoRes.getSmsSent()){
                throw new BadRequestException(ErrorCode.SERVICE_ERROR, "Something went wrong");
            }
        } else if (userLoginWithEmail) {
            userPreLoginVerificationInfoRes =   preLoginVerificationEmail(req, sendOTP);
            if(!userPreLoginVerificationInfoRes.getEmailSent() && !userPreLoginVerificationInfoRes.getSmsSent()){
                throw new BadRequestException(ErrorCode.SERVICE_ERROR, "Something went wrong");
            }
        }


        return userPreLoginVerificationInfoRes;
    }

    private boolean verifyLoginAuthToken(String authToken) {
        if(StringUtils.isNotEmpty(authToken)){
            return true;
        }
        return false;
    }

    private String getAuthTokenFromReq(HttpServletRequest req) {
        Cookie cookie = sessionUtils.getCookie(req, "auth-token");
        if (cookie != null) {
            return cookie.getValue();
        } else {
            return null;
        }
    }

    private UserPreLoginVerificationInfoRes preLoginVerificationPhone(UserPreLoginVerificationInfoReq req, Boolean sendOTP) throws VException, UnsupportedEncodingException {
        UserPreLoginVerificationInfoRes userPreLoginVerificationInfoRes = new UserPreLoginVerificationInfoRes();

        String testSignupNumber = TestAccontPasswordFactory.getTestContactNumber(TestAccontPasswordFactory.Constants.TEST_SIGNUP_NUMBER);

        String testLoginInternalNumber = TestAccontPasswordFactory.getTestContactNumber(TestAccontPasswordFactory.Constants.TEST_LOGIN_INTERNAL_NUMBER);

        List<User> userList = new ArrayList<>();

        if (!testSignupNumber.equals(req.getPhoneNumber())) {
            userList = userDAO.getUsersByPhoneNumberAndCode(req.getPhoneNumber(), req.getPhoneCode(), true);
        }

        if (userList.size() > 1 || testLoginInternalNumber.equals(req.getPhoneNumber())) {
            userPreLoginVerificationInfoRes.setMultipleUsersExists(true);
            return userPreLoginVerificationInfoRes;
        }
        User user = null;
        if (userList.size() > 0) {
            user = userList.get(0);
        }

        if (!req.getVsatVerification()) {
            try {
                if(sendOTP) {
                    sendOTPToPhone(req.getPhoneNumber(), req.getPhoneCode(), false);
                }
            }catch (VException ex){
                logger.info("Error in sending otp to phone {} {}", req, ex);
                userPreLoginVerificationInfoRes.setSmsSent(false);
            }
        }

        if (null != user) {
            checkIfUserIsBlocked(user);
            mobileAppAllowStudentOnly(user, req);
            if (req.getVsatVerification()) {
                try {
                    if(sendOTP) {
                        sendOTPToPhone(req.getPhoneNumber(), req.getPhoneCode(), false);
                    }
                }catch(VException ex){
                    logger.info("Error in sending otp to phone {} {}", req, ex);
                    userPreLoginVerificationInfoRes.setSmsSent(false);
                }
            }
            userPreLoginVerificationInfoRes.setContactExist(true);
            if (null != user.getPassword()) {
                userPreLoginVerificationInfoRes.setHasPassword(true);
            }

            if (null != user.getEmail() && StringUtils.isNotEmpty(user.getEmail()) && user.getIsContactNumberVerified()) {
                userPreLoginVerificationInfoRes.setEmailExists(true);
                userPreLoginVerificationInfoRes.setEmail(maskEmail(user.getEmail()));
                try {
                    if(sendOTP){
                        sendOTPToEmail(user.getEmail(), false);
                    }
                }catch(VException ex){
                    logger.info("Error in sending email {} {}", req, ex);
                    userPreLoginVerificationInfoRes.setEmailSent(false);
                }
            }

            if (null != user.getIsContactNumberVerified()) {
                userPreLoginVerificationInfoRes.setPhoneVerified(user.getIsContactNumberVerified());
            }
            if (null != user.getIsEmailVerified()) {
                userPreLoginVerificationInfoRes.setEmailVerified(user.getIsEmailVerified());
            }

        }
        return userPreLoginVerificationInfoRes;
    }

    private UserPreLoginVerificationInfoRes preLoginVerificationEmail(UserPreLoginVerificationInfoReq req, Boolean sendOTP) throws VException, UnsupportedEncodingException {
        UserPreLoginVerificationInfoRes userPreLoginVerificationInfoRes = new UserPreLoginVerificationInfoRes();

        List<User> userList = userDAO.getUsersByEmail(req.getEmail());

        if (userList.size() > 1) {
            userPreLoginVerificationInfoRes.setMultipleUsersExists(true);
            return userPreLoginVerificationInfoRes;
        }

        User user = null;
        if (userList.size() > 0) {
            user = userList.get(0);
        }

        if (req.getCouncellerAgent()) {
            return counselorPreLoginVerificationEmail(req, user, false);
        }

        if (null != user) {
            checkIfUserIsBlocked(user);
            mobileAppAllowStudentOnly(user, req);
            try {
                if(sendOTP) {
                    sendOTPToEmail(req.getEmail(), false);
                }
            }catch(VException ex){
                logger.info("Error in sending otp to email {} {}", req, ex);
                userPreLoginVerificationInfoRes.setEmailSent(false);
            }
            userPreLoginVerificationInfoRes.setEmailExists(true);

            if (null != user.getContactNumber() && StringUtils.isNotEmpty(user.getContactNumber())) {
                userPreLoginVerificationInfoRes.setContactExist(true);
                if (user.getIsContactNumberVerified()) {
                    userPreLoginVerificationInfoRes.setPhoneVerified(true);
                    try {
                        if(sendOTP) {
                            sendOTPToPhone(user.getContactNumber(), user.getPhoneCode(), false);
                        }
                    }catch(VException ex){
                        logger.info("Error in sending otp to phone {} {}", req, ex);
                        userPreLoginVerificationInfoRes.setSmsSent(false);
                    }
                }
                if (user.getContactNumber().length() >= DUMMY_NUMBER_LENGTH) {
                    userPreLoginVerificationInfoRes.setPhone(maskPhone(user.getPhoneCode(), user.getContactNumber()));
                }
            }

            if (null != user.getPassword()) {
                userPreLoginVerificationInfoRes.setHasPassword(true);
            }
            if (null != user.getIsContactNumberVerified()) {
                userPreLoginVerificationInfoRes.setPhoneVerified(user.getIsContactNumberVerified());
            }
            if (null != user.getIsEmailVerified()) {
                userPreLoginVerificationInfoRes.setEmailVerified(user.getIsEmailVerified());
            }
        }

        return userPreLoginVerificationInfoRes;
    }

    private UserPreLoginVerificationInfoRes counselorPreLoginVerificationEmail(UserPreLoginVerificationInfoReq req, User user, boolean resend) throws UnsupportedEncodingException, VException {

        UserPreLoginVerificationInfoRes userPreLoginVerificationInfoRes = new UserPreLoginVerificationInfoRes();

        String userName = "Counselor";
        if (user != null) {
            userName = StringUtils.isNotEmpty(user.getFullName()) ? user.getFullName() : userName;
            userPreLoginVerificationInfoRes.setEmailExists(true);
        }

        sendCounsellerOTPToEmail(userName, req.getEmail(), resend);

        return userPreLoginVerificationInfoRes;
    }

    private void checkIfUserIsBlocked(User user) throws ForbiddenException {
        if (null != user && null != user.getProfileEnabled() && Boolean.FALSE.equals(user.getProfileEnabled())) {
            throw new ForbiddenException(ErrorCode.USER_BLOCKED, "User is blocked.");
        }
    }

    private void mobileAppAllowStudentOnly(User user, UserPreLoginVerificationInfoReq req) throws ForbiddenException {
        if (null != user && null != req && null != user.getRole() && null != req.getRequestSource() && !user.getRole().equals(Role.STUDENT)) {
            if ((req.getRequestSource().equals(RequestSource.ANDROID)) || req.getRequestSource().equals(RequestSource.IOS)) {
                throw new ForbiddenException(ErrorCode.ACCESS_NOT_ALLOWED, "Only Student login is  allowed ");
            }
        }
    }

    public PlatformBasicResponse resendPreLoginVerificationOTP(UserPreLoginVerificationInfoReq req) throws VException, UnsupportedEncodingException {
        boolean userLoginWithPhone = false;
        boolean userLoginWithEmail = false;

        boolean sendOTP = true;

//        String ipAddress = req.getIpAddress();
//
//        LocationInfo locationInfo = ipUtil.getLocationFromIp(ipAddress);
//
//        if(null != locationInfo && "China".equalsIgnoreCase(locationInfo.getCountry())){
//            throw new ForbiddenException(ErrorCode.FORBIDDEN_ERROR, "Operation not allowed");
//        }

        try{
            // send login data to queue
            UserPreLoginDataPojo data = new UserPreLoginDataPojo();
            data.setIpAddress(req.getIpAddress());
            data.setLoginParam(req.getEmail() + "" + req.getPhoneCode() + "" + req.getPhoneNumber());
            awsSQSManager.sendToSQS(SQSQueue.PRE_LOGIN_DATA_QUEUE, SQSMessageType.PRE_LOGIN_DATA, new Gson().toJson(data));
        }
        catch(Exception e){
            // swallow
        }

        LocationInfo locationInfo = null;
        if (sendOTP) {
            try {
                String ispCheck = "True";
                if (null != ispCheck) {
                    locationInfo = ipUtil.getLocationFromIp(req.getIpAddress());
                    String isp = locationInfo.getIsp();
                    if (null != isp && null != locationInfo.getCountry() && !locationInfo.getCountry().equalsIgnoreCase("India")) {
                        String noOTPExceptAllowedCountries = null;
                        if (null == noOTPExceptAllowedCountries) {
                            String loginParam = req.getEmail() + "" + req.getPhoneCode() + "" + req.getPhoneNumber();
                            sendOTP = botFilter.sendPreLoginOTP(loginParam, isp);
                        } else {
                            if (!allowedCountriesList.contains(locationInfo.getCountry())) {
                                sendOTP = false;
                            }
                        }
                    }
                }
            } catch (Exception e) {
                // swallow
            }
        }

        // verify google recaptcha
        if (null == req.getRequestSource() || req.getRequestSource().equals(RequestSource.WEB)) {
            boolean allowLogin = botFilter.verifyGoogleRecaptcha(req.getToken(), req.getIpAddress());
            if (!allowLogin) {
                logger.info("Recaptcha Verification Failed for req : {} ", req);
            }
        }

        if (null != req.getPhoneCode() && null != req.getPhoneNumber()
                && !StringUtils.isEmpty(req.getPhoneCode()) && !StringUtils.isEmpty(req.getPhoneNumber())) {
            userLoginWithPhone = true;
        } else if (null != req.getEmail() || StringUtils.isNotEmpty(req.getEmail())) {
            userLoginWithEmail = true;
        }

        if (userLoginWithPhone) {
            User user = userDAO.getUserByPhoneNumberAndCode(req.getPhoneNumber(), req.getPhoneCode());
            checkIfUserIsBlocked(user);
            sendOTPToPhone(req.getPhoneNumber(), req.getPhoneCode(), sendOTP);
            if (null != user && null != user.getEmail() && StringUtils.isNotEmpty(user.getEmail()) && user.getIsContactNumberVerified()) {
                sendOTPToEmail(user.getEmail(), sendOTP);
            }
        } else if (userLoginWithEmail) {
            User user = userDAO.getUserByEmail(req.getEmail());

            if (req.getCouncellerAgent()) {
                counselorPreLoginVerificationEmail(req, user, sendOTP);
                return new PlatformBasicResponse();
            }

            checkIfUserIsBlocked(user);
            if (null != user) {
                sendOTPToEmail(user.getEmail(), sendOTP);
                if (null != user.getContactNumber() && StringUtils.isNotEmpty(user.getContactNumber()) && user.getIsContactNumberVerified()) {
                    sendOTPToPhone(user.getContactNumber(), user.getPhoneCode(), true);
                }
            }
        }
        return new PlatformBasicResponse();
    }

    private void sendOTPToPhone(String phoneNumber, String phoneCode, boolean resend) throws VException {
        VerificationTokenII verificationTokenII = new VerificationTokenII();

        String OTP;
        if (TestAccontPasswordFactory.isTestAccount(phoneNumber)) {
            OTP = TestAccontPasswordFactory.getTestDefaultOTP(phoneNumber);
        } else {
            OTP = StringUtils.randomNumericString(4);
        }

        verificationTokenII.setCode(OTP);
        verificationTokenII.setStatus(VerificationLinkStatus.ACTIVE);
        verificationTokenII.setEmailId(phoneNumber);

        String formattedPhoneCode = (null != phoneCode) ? phoneCode.replace("+", "") : "91";
        verificationTokenII.setPhoneCode(formattedPhoneCode);

        CommunicationType communicationType = CommunicationType.PHONE_VERIFICATION;

        verificationTokenII.setExpiryTime(ConfigUtils.INSTANCE.getLongValue("verification_code_expiry"));
        verificationTokenII.setType(VerificationTokenType.SMS_VERIFICATION);

        if (resend) {
            VerificationTokenII token = verificationTokenDAO.getPhoneLoginVerificationCode(phoneNumber, formattedPhoneCode);
            if (null != token) {
                OTP = token.getCode();
                verificationTokenII.setCode(OTP);
            }
        }

        verificationTokenDAO.create(verificationTokenII, null);

        Map<String, Object> scopeParams = new HashMap<String, Object>();
        scopeParams.put("code", OTP);

        TextSMSRequest textSMSRequest = null;

//        if (resend) {
//            int random = (int) Math.round(Math.random());
//            textSMSRequest = new TextSMSRequest(phoneNumber, formattedPhoneCode, scopeParams,
//                    communicationType, Role.STUDENT, random);
//        } else {
//            textSMSRequest = new TextSMSRequest(phoneNumber, formattedPhoneCode, scopeParams,
//                    communicationType, Role.STUDENT);
//        }
        textSMSRequest = new TextSMSRequest(phoneNumber, formattedPhoneCode, scopeParams,
                communicationType, Role.STUDENT);

        if (!isLocalEnvironment() && !TestAccontPasswordFactory.isTestAccount(phoneNumber)) {
            communicationManager.sendSMSViaRest(textSMSRequest);
        }
    }

    private Boolean isLocalEnvironment() {
        return "LOCAL".equalsIgnoreCase(env);
    }

    private void sendOTPToEmail(String email, boolean resend) throws VException, UnsupportedEncodingException {
        VerificationTokenII verificationTokenII = new VerificationTokenII();
        String OTP = StringUtils.randomNumericString(4);
        verificationTokenII.setCode(OTP);
        verificationTokenII.setStatus(VerificationLinkStatus.ACTIVE);
        verificationTokenII.setEmailId(email.toLowerCase());
        verificationTokenII.setExpiryTime(ConfigUtils.INSTANCE.getLongValue("verification_code_expiry"));
        verificationTokenII.setType(VerificationTokenType.EMAIL_VERIFICATION);

        if (resend) {
            VerificationTokenII token = verificationTokenDAO.getEmailLoginVerificationCode(email);
            if (null != token) {
                OTP = token.getCode();
                verificationTokenII.setCode(OTP);
            }
        }

        verificationTokenDAO.create(verificationTokenII, null);

        CommunicationType emailType = CommunicationType.EMAIL_VERIFICATION;
        HashMap<String, Object> bodyScopes = new HashMap<>();
        bodyScopes.put("OTP", OTP);

        ArrayList<InternetAddress> toList = new ArrayList<>();
        toList.add(new InternetAddress(email, ""));
        EmailRequest emailRequest = new EmailRequest(toList, bodyScopes, bodyScopes, emailType, Role.STUDENT);
        emailRequest.setPriorityType(EmailPriorityType.HIGH);
        if (!isLocalEnvironment()) {
            communicationManager.sendEmailViaRest(emailRequest);
        }
    }

    private void sendCounsellerOTPToEmail(String userName, String email, boolean resend) throws VException, UnsupportedEncodingException {

        if (StringUtils.isEmpty(email) || !email.toLowerCase().endsWith("vedantu.com")) {
            return;
        }

        VerificationTokenII verificationTokenII = new VerificationTokenII();
        String OTP = StringUtils.randomNumericString(4);
        verificationTokenII.setCode(OTP);
        verificationTokenII.setStatus(VerificationLinkStatus.ACTIVE);
        verificationTokenII.setEmailId(email.toLowerCase());
        verificationTokenII.setExpiryTime(ConfigUtils.INSTANCE.getLongValue("verification_code_expiry"));
        verificationTokenII.setType(VerificationTokenType.EMAIL_VERIFICATION);

        if (resend) {
            VerificationTokenII token = verificationTokenDAO.getEmailLoginVerificationCode(email);
            if (null != token) {
                OTP = token.getCode();
                verificationTokenII.setCode(OTP);
            }
        }

        verificationTokenDAO.create(verificationTokenII, null);

        CommunicationType emailType = CommunicationType.EMAIL_VERIFICATION_COUNSELLER;
        HashMap<String, Object> bodyScopes = new HashMap<>();
        bodyScopes.put("userName", userName);
        bodyScopes.put("OTP", OTP);

        ArrayList<InternetAddress> toList = new ArrayList<>();
        toList.add(new InternetAddress(email, ""));
        EmailRequest emailRequest = new EmailRequest(toList, bodyScopes, bodyScopes, emailType, Role.STUDENT_CARE);

        if (!isLocalEnvironment()) {
            communicationManager.sendEmailViaRest(emailRequest);
        }
    }

    public ProcessVerifyContactNumberResponse verifyOTP(OTPVerificationReq req, HttpServletRequest request, HttpServletResponse response) throws VException, IOException, URISyntaxException {

        if (null == req.getOtp()) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "OTP cannot be empty");
        }

        VerificationTokenII emailToken = null;
        VerificationTokenII mobileToken = null;

        String userEmail = "";
        String userPhone = "";
        String userPhoneCode = "";

        mobileToken = verificationTokenDAO.getPhoneLoginVerificationCode(req.getPhoneNumber(), req.getPhoneCode());
        emailToken = verificationTokenDAO.getEmailLoginVerificationCode(req.getEmail());


        HttpSessionData currentSessionData = sessionUtils.getCurrentSessionData();
        if (null != currentSessionData && currentSessionData.getUserId() != null) {
            return verifyPhonePostLogin(req, currentSessionData.getUserId(), mobileToken, request, response);
        }

        if (null != req.getEmail() && StringUtils.isNotEmpty(req.getEmail())) {
            User u = userDAO.getUserByEmail(req.getEmail());
            if (null != u && null != u.getContactNumber() && u.getContactNumber().length() != DUMMY_NUMBER_LENGTH) {
                userPhone = u.getContactNumber();
                userPhoneCode = u.getPhoneCode();
                userEmail = u.getEmail();
                mobileToken = verificationTokenDAO.getPhoneLoginVerificationCode(u.getContactNumber(), u.getPhoneCode());
            }
        }

        if (StringUtils.isNotEmpty(req.getPhoneCode()) && StringUtils.isNotEmpty(req.getPhoneNumber())) {
            boolean isTestSignup = TestAccontPasswordFactory.isTestSignupAccount(req.getPhoneNumber());
            User u = null;
            if (!isTestSignup) {
                u = userDAO.getUserByPhoneNumberAndCode(req.getPhoneNumber(), req.getPhoneCode());
            }

            if (null != u && null != u.getEmail()) {
                userPhone = u.getContactNumber();
                userPhoneCode = u.getPhoneCode();
                userEmail = u.getEmail();
                emailToken = verificationTokenDAO.getEmailLoginVerificationCode(u.getEmail());
            }
        }

        if (emailToken == null && mobileToken == null) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "OTP expired");
        }

        // filter login
        loginRateFilter(userEmail, userPhone, userPhoneCode);

        boolean phone = false;
        boolean email = false;

        String emailTokenCode = (null != emailToken) ? emailToken.getCode() : null;
        String mobileTokenCode = (null != mobileToken) ? mobileToken.getCode() : null;

        String otp = req.getOtp();

        VerificationTokenII token;

        if (null != emailTokenCode && emailTokenCode.equals(otp)) {
            email = true;
            emailToken.setStatus(VerificationLinkStatus.USED);
            token = emailToken;
            verificationTokenDAO.create(emailToken, req.getCallingUserId());
        } else if (null != mobileTokenCode && mobileTokenCode.equals(otp)) {
            String testPhone = (null != req.getPhoneNumber()) ? req.getPhoneNumber() : userPhone;
            if (TestAccontPasswordFactory.isTestAccount(testPhone)) {
                if (sessionUtils.getCurrentSessionData() == null && null == req.getEmail() && !TestAccontPasswordFactory.canLoginThroughOtp(req.getPhoneNumber())) {
                    throw new ForbiddenException(ErrorCode.BAD_REQUEST_ERROR, "Invalid OTP");
                }
            }
            phone = true;
            token = mobileToken;
            mobileToken.setStatus(VerificationLinkStatus.USED);
            verificationTokenDAO.create(mobileToken, req.getCallingUserId());
        } else {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Incorrect OTP entered");
        }

        if (email) {
            return verifyOTPEmail(userEmail, req, request, response);
        } else if (phone) {
            return verifyOTPPhone(userPhone, userPhoneCode, req, request, response);
        } else {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Incorrect details are entered");
        }
    }

    private boolean preLoginRateFilter(String email, String contactNumber, String phoneCode) {
        if(StringUtils.isNotEmpty(email)){
            String key = "OTP_PRE_LOGIN_" + email;
            try {
                String value = redisDAO.get(key);
                if (!StringUtils.isEmpty(value)) {
                    Integer count = gson.fromJson(value, Integer.class);
                    incrementRedisValue(key);
                    if (count >= preLoginLimit) {
                        return true;
                    }
                } else {
                    updateRedisValue(key);
                    return false;
                }
            } catch (Exception e) {
                logger.info("Error in getting value from redis" + e.getMessage());
            }
        }

        if(StringUtils.isNotEmpty(contactNumber)){
            String key = "OTP_PRE_LOGIN_" + contactNumber;
            try {
                String value = redisDAO.get(key);
                if (!StringUtils.isEmpty(value)) {
                    Integer count = gson.fromJson(value, Integer.class);
                    incrementRedisValue(key);
                    if (count >= preLoginLimit) {
                       return true;
                    }
                } else {
                    updateRedisValue(key);
                    return false;
                }
            } catch (Exception e) {
                logger.info("Error in getting value from redis" + e.getMessage());
            }
        }
        return false;
    }

    private void loginRateFilter(String email, String contactNumber, String phoneCode) {
        if(StringUtils.isNotEmpty(email)){
            String key = "OTP_LOGIN_" + email;
            try {
                String value = redisDAO.get(key);
                if (!StringUtils.isEmpty(value)) {
                    Integer count = gson.fromJson(value, Integer.class);
                    incrementRedisValue(key);
                    if (count > limit) {
                        // expire verification token
                        VerificationTokenII verificationTokenII = verificationTokenDAO.getEmailLoginVerificationCode(email);
                        verificationTokenII.setStatus(VerificationLinkStatus.EXPIRED);
                        verificationTokenDAO.create(verificationTokenII, null);
                        resetRedisValue(key);
                    }
                } else {
                    updateRedisValue(key);
                }
            } catch (Exception e) {
                logger.info("Error in getting value from redis" + e.getMessage());
            }
        }

        if(StringUtils.isNotEmpty(contactNumber)){
            String key = "OTP_LOGIN_" + contactNumber;
            try {
                String value = redisDAO.get(key);
                if (!StringUtils.isEmpty(value)) {
                    Integer count = gson.fromJson(value, Integer.class);
                    incrementRedisValue(key);
                    if (count >= limit) {
                        // expire verification token
                        VerificationTokenII verificationTokenII = verificationTokenDAO.getPhoneLoginVerificationCode(contactNumber, phoneCode);
                        verificationTokenII.setStatus(VerificationLinkStatus.EXPIRED);
                        verificationTokenDAO.create(verificationTokenII, null);
                        resetRedisValue(key);
                    }
                } else {
                    updateRedisValue(key);
                }
            } catch (Exception e) {
                logger.info("Error in getting value from redis" + e.getMessage());
            }
        }
    }

    private void resetRedisValue(String key) { try {
        redisDAO.del(key);
    } catch (Exception e) {
        logger.info("Error in updating value to redis" + e.getMessage());
    }

    }

    private void updateRedisValue(String key) {
        try {
            // System.currentTimeMillis gives milli seconds from epoch
            // but we need unix time here for expireAt in redis,  which takes no of seconds from epoch
            // that's why milliSeconds /1000 + 300 (for 5 min)
            redisDAO.incrAndExpireAt(key, 300 + System.currentTimeMillis() / 1000);
        } catch (Exception e) {
            logger.info("Error in updating value to redis" + e.getMessage());
        }
    }

    private void incrementRedisValue(String key) {
        try {
            redisDAO.incrAndExpireAt(key, 300 + System.currentTimeMillis() / 1000);
        } catch (Exception e) {
            logger.info("Error in incrementing value to redis" + e.getMessage());
        }
    }


    public ProcessVerifyContactNumberResponse verifyCounsellerOTP(OTPVerificationReq req, HttpServletRequest request, HttpServletResponse response) throws VException, IOException, URISyntaxException {

        if (null == req.getOtp()) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "OTP cannot be empty");
        }

        VerificationTokenII emailToken = null;

        emailToken = verificationTokenDAO.getEmailLoginVerificationCode(req.getEmail());

        if (emailToken == null) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "OTP expired");
        }

        String emailTokenCode = (null != emailToken) ? emailToken.getCode() : "";

        String otp = req.getOtp();

        if (emailTokenCode.equals(otp)) {
            emailToken.setStatus(VerificationLinkStatus.USED);
            verificationTokenDAO.create(emailToken, req.getCallingUserId());
        } else {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Incorrect OTP entered");
        }

        ProcessVerifyContactNumberResponse processVerifyContactNumberResponse = new ProcessVerifyContactNumberResponse();
        processVerifyContactNumberResponse.setEmailVerifed(true);

        return processVerifyContactNumberResponse;
    }

    private ProcessVerifyContactNumberResponse verifyOTPPhone(String userPhone, String userPhoneCode, OTPVerificationReq req, HttpServletRequest request, HttpServletResponse response) throws IOException, URISyntaxException, VException {
        ProcessVerifyContactNumberResponse processVerifyContactNumberResponse = new ProcessVerifyContactNumberResponse();
        User user = null;

        if (null == req.getPhoneNumber() && userPhone.equals("")) {
            throw new ForbiddenException(ErrorCode.FORBIDDEN_ERROR, "Invalid OTP");
        }

        if (userPhone.equals("") && null == req.getEmail() && !TestAccontPasswordFactory.canLoginThroughOtp(req.getPhoneNumber())) {
            throw new ForbiddenException(ErrorCode.FORBIDDEN_ERROR, "Invalid OTP");
        }

        if (null != req.getEmail() && req.getEmail().endsWith("@vedantu.com")) {
            User user1 = userDAO.getUserByEmail(req.getEmail());
            if (null != user1 && null != user1.getRole() && !Role.TEACHER.equals(user1.getRole()) && !Role.STUDENT.equals(user1.getRole())) {
                throw new ForbiddenException(ErrorCode.FORBIDDEN_ERROR, "Login not allowed");
            }
        }

        String testPhoneNumber = (null != req.getPhoneNumber()) ? req.getPhoneNumber() : userPhone;
        if (!TestAccontPasswordFactory.isTestSignupAccount(testPhoneNumber)) {
            if (StringUtils.isNotEmpty(userPhone) && StringUtils.isNotEmpty(userPhoneCode)) {
                user = userDAO.getUserByPhoneNumberAndCode(userPhone, userPhoneCode);
            } else {
                user = userDAO.getUserByPhoneNumberAndCode(req.getPhoneNumber(), req.getPhoneCode());
            }
        }

        processVerifyContactNumberResponse.setVerified(true);
        if (null == user) {
            return processVerifyContactNumberResponse;
        } else {
            if (Boolean.FALSE.equals(user.getProfileEnabled())) {
                throw new ForbiddenException(ErrorCode.USER_BLOCKED, "User is blocked.");
            }

            if (!user.getIsContactNumberVerified()) {
                user.setIsContactNumberVerified(true);
                user.setContactNumberVerifiedBy(VerifiedType.OTP);
                userDAO.update(user, req.getCallingUserId());
            }

            com.vedantu.User.User user1 = pojoUtils.convertToUserPojo(user);
            HttpSessionData sessionData = sessionUtils.setCookieAndHeaders(request, response, user1);
            sessionData.setPasswordAutogenerated(user.getPasswordAutogenerated());

            processVerifyContactNumberResponse.setUserDetails(sessionData);
            processVerifyContactNumberResponse.setUser(user1);
            processVerifyContactNumberResponse.setNumber(req.getPhoneNumber());
            processVerifyContactNumberResponse.setNumber(req.getPhoneCode());
            return processVerifyContactNumberResponse;
        }
    }

    private ProcessVerifyContactNumberResponse verifyOTPEmail(String userEmail, OTPVerificationReq req, HttpServletRequest request, HttpServletResponse response) throws ForbiddenException, IOException, URISyntaxException, ConflictException {
        ProcessVerifyContactNumberResponse processVerifyContactNumberResponse = new ProcessVerifyContactNumberResponse();
        User user = null;
        if (StringUtils.isNotEmpty(userEmail)) {
            user = userDAO.getUserByEmail(userEmail);
        } else {
            user = userDAO.getUserByEmail(req.getEmail());
        }
        processVerifyContactNumberResponse.setVerified(true);
        if (null == user) {
            return processVerifyContactNumberResponse;
        } else {

            if (!user.getIsEmailVerified()) {
                user.setIsEmailVerified(true);
                userDAO.update(user, req.getCallingUserId());
            }
            if (Boolean.FALSE.equals(user.getProfileEnabled())) {
                throw new ForbiddenException(ErrorCode.USER_BLOCKED, "User is blocked.");
            }

            com.vedantu.User.User user1 = pojoUtils.convertToUserPojo(user);
            HttpSessionData sessionData = sessionUtils.setCookieAndHeaders(request, response, user1);
            sessionData.setPasswordAutogenerated(user.getPasswordAutogenerated());
            processVerifyContactNumberResponse.setUserDetails(sessionData);
            processVerifyContactNumberResponse.setUser(user1);
        }
        return processVerifyContactNumberResponse;
    }

    private ProcessVerifyContactNumberResponse verifyPhonePostLogin(OTPVerificationReq req, Long userId, VerificationTokenII token, HttpServletRequest request, HttpServletResponse response) throws BadRequestException, IOException, URISyntaxException, ConflictException {
//        User user = userDAO.getUserByUserId(userId);
        User user = userDAO.getUserByPhoneNumberAndCode(req.getPhoneNumber(), req.getPhoneCode());

        if (null == user) {
            user = userDAO.getUserByUserId(userId);
        }

        if (null == token) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Incorrect OTP entered");
        }

        String tokenCode = token.getCode();
        String otp = req.getOtp();

        if (!tokenCode.equals(otp)) {
            //return otp not matched.
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Incorrect OTP entered");
        }

        if (null == req.getPhoneNumber() || null == req.getPhoneCode()) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "phone number cannot be empty");
        }

        if (null != req.getPhoneCode() && req.getPhoneCode().startsWith("+")) {
            user.setPhoneCode(req.getPhoneCode().replace("+", ""));
        } else if (null != req.getPhoneCode()) {
            user.setPhoneCode(req.getPhoneCode());
        }

        if (null != req.getPhoneNumber()) {
            user.setContactNumberVerifiedBy(VerifiedType.OTP);
            user.setContactNumber(req.getPhoneNumber());
            user.setIsContactNumberVerified(true);
        }
        userDAO.update(user, req.getCallingUserId());

        com.vedantu.User.User user1 = mapper.map(user, com.vedantu.User.User.class);
        HttpSessionData sessionData = sessionUtils.setCookieAndHeaders(request, response, user1);
        sessionData.setPasswordAutogenerated(user.getPasswordAutogenerated());

        ProcessVerifyContactNumberResponse processVerifyContactNumberResponse = new ProcessVerifyContactNumberResponse();
        processVerifyContactNumberResponse.setUserDetails(sessionData);
        processVerifyContactNumberResponse.setUser(user1);
        processVerifyContactNumberResponse.setVerified(true);

        return processVerifyContactNumberResponse;
    }

    public NewPhoneVerificationRes newPhoneVerification(NewPhoneVerificationReq req) throws VException {
        req.verify();
        NewPhoneVerificationRes newPhoneVerificationRes = new NewPhoneVerificationRes();
        User user = null;
        if (!TestAccontPasswordFactory.isTestSignupAccount(req.getPhoneNumber())) {
            user = userDAO.getUserByPhoneNumberAndCode(req.getPhoneNumber(), req.getPhoneCode());
        }

        HttpSessionData sessionData = sessionUtils.getCurrentSessionData();

        long userId = 0L;

        if (null != sessionData && null != sessionData.getUserId()) {
            userId = sessionData.getUserId();
        }

        if (null != user && userId != user.getId()) {
            newPhoneVerificationRes.setPhoneNumberExists(true);
            String email = user.getEmail();
            if (null != email) {
                newPhoneVerificationRes.setLinkedEmail(maskEmail(email));
            }
            return newPhoneVerificationRes;
        }

        if (TestAccontPasswordFactory.isTestAccount(req.getPhoneNumber())) {

            String email = req.getEmail();
            if (sessionUtils.getCallingUserId() != null) {
                User loggedInUser = userDAO.getUserByUserId(sessionUtils.getCallingUserId());
                email = loggedInUser.getEmail();
            }

            if (null != email) {
                if (StringUtils.isEmpty(email) || !email.endsWith("@vedantu.com")) {
                    throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Not permited to register this contact number");
                }
            }
        }

        sendOTPToPhone(req.getPhoneNumber(), req.getPhoneCode(), false);

        newPhoneVerificationRes.setPhoneNumberExists(false);
        return newPhoneVerificationRes;
    }

    private String maskEmail(String email) {
        return email.replaceAll("(?<=.)[^@](?=[^@]*?@)|(?:(?<=@.)|(?!^)\\G(?=[^@]*$)).(?=.*\\.)", "*");
    }

    private String maskPhone(String phoneCode, String contactNumber) {
        int len = contactNumber.length();

        String sub1 = contactNumber.substring(0, 2);
        String sub2 = contactNumber.substring(len - 2, len);

        String mask = addMask(len - 4);

        if (null == phoneCode) {
            phoneCode = "";
        }

        if (!phoneCode.startsWith("+")) {
            phoneCode = "+" + phoneCode;
        }

        String phone = phoneCode + sub1 + mask + sub2;

        return phone;
    }

    private String addMask(int len) {
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < len; i++) {
            sb.append("*");
        }
        return sb.toString();
    }

    public SignUpUserRes signUpUser(SignUpUserReq req, HttpServletRequest request, HttpServletResponse response) throws Exception {

        //set Phonenumber and Phone code
        TruecallerEntity truecallerEntity;
        if (null != req.getTruecallerToken() && null != req.getTruecallerToken().getSignature() && !StringUtils.isEmpty(req.getTruecallerToken().getSignature())) {
            truecallerEntity = getTruecallerData(req.getTruecallerToken().getPayload());
            String mobile = truecallerEntity.getPhoneNumber();
            CountryToPhonePrefix countryToPhonePrefix = new CountryToPhonePrefix();
            String phoneCodeTemp = countryToPhonePrefix.prefixFor(truecallerEntity.getCountryCode());
            req.setPhoneCode(phoneCodeTemp);
            req.setPhoneNumber(mobile.substring(mobile.indexOf(phoneCodeTemp) + phoneCodeTemp.length()));
            logger.debug(req);
        }

        req.verify();

        HttpSessionData localsessionData = sessionUtils.getCurrentSessionData();

        if (null != localsessionData && null != localsessionData.getUserId() && null != localsessionData.getRole()) {
            Role role = localsessionData.getRole();
            if (!role.equals(Role.STUDENT)) {
                throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Cannot sign up new user");
            }
        }

        String phoneNumber = req.getPhoneNumber();
        String phoneCode = req.getPhoneCode();
        if(req.getSignUpFeature() != null && FeatureSource.PAYTM_MINI.equals(req.getSignUpFeature())) {
            String token =   redisDAO.get("PAYTM_" + req.getPhoneNumber());
            if(StringUtils.isEmpty(token) || !req.getPaytmToken().equals(token)){
                throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Invalid Paytm request");
            }
        }else
        if (null == req.getTruecallerToken() || null == req.getTruecallerToken().getSignature() || StringUtils.isEmpty(req.getTruecallerToken().getSignature())) {
            // re-verify otp
            String verificationToken = req.getVerificationToken();
            VerificationTokenII token = verificationTokenDAO.getUsedVerificationToken(phoneNumber, phoneCode, verificationToken);
            if (token == null) {
                throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Invalid request");
            }
        } else {

            //re-Verify Truecaller
            boolean isVerified = truecallerVerify(req.getTruecallerToken().getPayload(), req.getTruecallerToken().getSignature(), req.getTruecallerToken().getSignatureAlgorithm());
            truecallerEntity = getTruecallerData(req.getTruecallerToken().getPayload());
            long timeDifference = System.currentTimeMillis() / 1000 - truecallerEntity.getRequestTime();
            if (isVerified == false || timeDifference > 900) {
                throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Invalid Truecaller request");
            }

        }

        // check if email already exists.
        if (null != req.getEmail() && StringUtils.isNotEmpty(req.getEmail())) {
            User user = getUserByEmail(req.getEmail().trim());
            if (null != user) {
                if (Role.STUDENT.equals(req.getRole()) && (FeatureSource.SEM_PAGES.equals(req.getSignUpFeature())
                        || FeatureSource.SEO_PAGES.equals(req.getSignUpFeature()))) {
                    Map<String, String> lsParams = new HashMap<>();
                    lsParams.put("interestType", req.getSignUpFeature().name());
                    com.vedantu.User.User user1 = mapper.map(user, com.vedantu.User.User.class);
                    LeadSquaredRequest leadReq = new LeadSquaredRequest(LeadSquaredAction.POST_LEAD_DATA,
                            LeadSquaredDataType.LEAD_ACTIVITY_SHOW_INTEREST, user1, lsParams,
                            null);
                    String platformEndpoint = ConfigUtils.INSTANCE.getStringValue("PLATFORM_ENDPOINT");
                    ClientResponse resp = WebUtils.INSTANCE.doCall(platformEndpoint + "/leadsquared/postLeadSquaredData",
                            HttpMethod.POST, new Gson().toJson(leadReq));
                }
                throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Enter a different email as this is already in use");
            }
        }

        // check if phone already exists.
        if (StringUtils.isNotEmpty(req.getPhoneNumber()) && StringUtils.isNotEmpty(req.getPhoneCode())) {
            User phoneUser = userDAO.getUserByPhoneNumberAndCode(req.getPhoneNumber(), req.getPhoneCode());
            if (null != phoneUser) {
                throw new ConflictException(ErrorCode.USER_ALREADY_EXISTS, "User already exists with phone[" + req.getPhoneNumber()
                        + "]");
            }
        }

        // create user
        User user = createUserByVerificationCode(req, request, response);
        com.vedantu.User.User user1 = mapper.map(user, com.vedantu.User.User.class);
        if(req.getSignUpFeature() == null || !FeatureSource.PAYTM_MINI.equals(req.getSignUpFeature())) {
            HttpSessionData sessionData = sessionUtils.setCookieAndHeaders(request, response, user1);
        }

        SignUpUserRes signUpUserRes = new SignUpUserRes(user1);
        signUpUserRes.setIsContactNumberVerified(true);
        signUpUserRes.setUser(user1);

        if (StringUtils.isNotEmpty(req.getCouponCode())) {
            try {
                ProcessCouponReq processCouponReq = new ProcessCouponReq(req.getCouponCode(), user.getId(), CouponType.CREDIT);
                Object appliedCoupon = processCoupon(processCouponReq);
                signUpUserRes.setAppliedCoupon(appliedCoupon != null);
            } catch (VException ex) {
                if (ex instanceof InternalServerErrorException) {
                    logger.error(ex);
                }
            }
        }

        String resString = null;

        try {
            resString = redisDAO.get(AUTOSALES_PB_ON_APP_ALLOWED);
        } catch (Exception e) {
            logger.error("ERROR Occured while fetching data from Cache = " + e.getMessage());
        }

        // todo-- set appropriate vals for key
        if (StringUtils.isEmpty(resString)) {
            resString = "TRUE";
        }

        if (!resString.isEmpty() && Boolean.FALSE.toString().equalsIgnoreCase(resString)) {
            return signUpUserRes;
        }
        
        if(null != req.getSignUpFeature() && req.getSignUpFeature().equals(FeatureSource.DOUBT_APP) && StringUtils.isNotEmpty(req.getAppVersionCode())
        		&& req.getAppVersionCode().compareTo("1.7.4") >= 0 && !user.getStudentInfo().getGrade().equalsIgnoreCase("1")
        		&& !user.getStudentInfo().getGrade().equalsIgnoreCase("2") && !user.getStudentInfo().getGrade().equalsIgnoreCase("3")
        		&& !user.getStudentInfo().getGrade().equalsIgnoreCase("4") && !user.getStudentInfo().getGrade().equalsIgnoreCase("5")) {
        	try {
        		logger.debug("Calling Going to enroll user to premium bundles for new app sign up ->");
        		enrollNewUserToPremiumBundles(user);
        	}catch(VException v){
        		logger.error("Could not enroll Mobile APP signing user to a premium Subscription due to -> "+v);
        	}
        }

        return signUpUserRes;
    }

    private String processCoupon(ProcessCouponReq req) throws VException {
        logger.info("ENTRY: " + req);
        String dineroEndpoint = ConfigUtils.INSTANCE.getStringValue("DINERO_ENDPOINT");
        ClientResponse resp = WebUtils.INSTANCE.doCall(dineroEndpoint + "/coupons/processCoupon",
                HttpMethod.POST, new Gson().toJson(req));
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        logger.info("EXIT:" + jsonString);
        return jsonString;
    }

    private User createUserByVerificationCode(SignUpUserReq req, HttpServletRequest request, HttpServletResponse response) throws VException, UnsupportedEncodingException {
        if (request != null && null == req.getSignUpURL()) {
            String refererUrl = request.getHeader("referer");
            if (StringUtils.isEmpty(refererUrl)) {
                refererUrl = request.getHeader("Referer");
                if (StringUtils.isEmpty(refererUrl) && !StringUtils.isEmpty(request.getParameter("refererUrl"))) {
                    refererUrl = request.getParameter("refererUrl");
                }
            }
            req.setSignUpURL(refererUrl);
        }

        User user = new User(req);
        user.setRole(Role.STUDENT);

        String password = StringUtils.randomNumericString(4);
        if (req.getSignUpFeature() != null && req.getSignUpFeature().equals(FeatureSource.ORG_USER_SIGNUP)) {
            user.setPassword(password);
        }

        if (StringUtils.isNotEmpty(user.getPassword())) {
            String hashedPassword = passwordEncoder.encode(user.getPassword());
            user.setPassword(hashedPassword);
            user.setPasswordHashed(true);
            user.setPasswordChangedAt(System.currentTimeMillis());
            user.addRecentHashedPassword(hashedPassword);
        }

        if (StringUtils.isNotEmpty(req.getReferrer())) {
            user.setReferrer(req.getReferrer());
        }

        if (StringUtils.isNotEmpty(req.getSignUpURL())) {
            user.setSignUpURL(req.getSignUpURL());
        }

        if (StringUtils.isEmpty(user.getPhoneCode())) {
            user.setPhoneCode("91");
        }

        if (user.getAppId() != null && user.getAppId() > 0) {
            user.setTncVersion("v7");
        }

        if (StringUtils.isNotEmpty(user.getContactNumber())) {
            PhoneNumber phoneNumber = new PhoneNumber(user.getContactNumber(), user.getPhoneCode(), true, false, false,
                    false);
            List<PhoneNumber> numbers = new ArrayList<>();
            numbers.add(phoneNumber);
            user.setPhones(numbers);
        }

        try {
            user.setReferralCode(getReferralCode(user));
        } catch (Exception ex) {
            logger.error("Error generating referral code for user " + user, ex);
        }
        if(FeatureSource.PAYTM_MINI.equals(req.getSignUpFeature())){
            user.setIsContactNumberVerified(true);
            user.setContactNumberVerifiedBy(VerifiedType.PAYTM);
        }
        else if (null != req.getTruecallerToken() && null != req.getTruecallerToken().getSignature() && !StringUtils.isEmpty(req.getTruecallerToken().getSignature())) {
            user.setIsContactNumberVerified(true);
            user.setContactNumberVerifiedBy(VerifiedType.TRUECALLER);
        } else {
            user.setIsContactNumberVerified(true);
            user.setContactNumberVerifiedBy(VerifiedType.OTP);
        }

        if (FeatureSource.ORG_USER_SIGNUP.equals(req.getSignUpFeature())) {
            user.setIsContactNumberVerified(false);
        }

        // create user
        userDAO.create(user, req.getCallingUserId());

        UserDetails userDetail = null;

        Map<String, String> userInfo = new HashMap<>();
        if (user.getRole().equals(Role.STUDENT)
                && (FeatureSource.ISL.equals(user.getSignUpFeature())
                || FeatureSource.ISL_REGISTRATION_VIA_TOOLS.equals(user.getSignUpFeature())
                || FeatureSource.VSAT.equals(user.getSignUpFeature())
                || FeatureSource.VOLT_2020_JAN.equals(user.getSignUpFeature())
                || FeatureSource.REVISEINDIA_2020_FEB.equals(user.getSignUpFeature())
                || FeatureSource.REVISE_JEE_2020_MARCH.equals(user.getSignUpFeature())
                || FeatureSource.TARGET_JEE_NEET.equals(user.getSignUpFeature()))) {
            String event = null;
            switch (user.getSignUpFeature()) {
                case ISL_REGISTRATION_VIA_TOOLS:
                case ISL:
                    event = "ISL_2017";
                    break;
                case VSAT:
                    VsatEventDetailsPojo vsatEventDetailsPojo = fosUtils.getVsatEventDetails();
                    event = vsatEventDetailsPojo.getEventName();
                    break;
                case VOLT_2020_JAN:
                    event = "VOLT_2020_JAN";
                    break;
                case REVISEINDIA_2020_FEB:
                    event = "REVISEINDIA_2020_FEB";
                    break;
                case REVISE_JEE_2020_MARCH:
                    event = "REVISE_JEE_2020_MARCH";
                    userInfo.put("jeeApplicationNumber", req.getjeeApplicationNumber());
                    userInfo.put("dob", req.getDob());
                    break;
                case TARGET_JEE_NEET:
                    event = "TARGET_JEE_NEET";
                    userInfo.put("jeeApplicationNumber", req.getjeeApplicationNumber());
                    break;
            }

            userDetail = createUserDetailsForEvent(user, event,
                    req.getCallingUserId(), req.getAgentName(), req.getAgentEmail(), req.getExamName(), req.getVoltId(),
                    req.getAchievementDocments(), req.getAchievements(), req.getIdproofs(), req.getLocationInfo(), userInfo);
        }

        // User location update
        if (null != user.getLocationInfo() && null == user.getLocationInfo().getCity()) {
            try {
                String ipAddress = req.getIpAddress();
                if (ipAddress != null) {

                    if (StringUtils.isNotEmpty(ipAddress)) {
                        String[] ips = ipAddress.split(",");
                        logger.info("IPADDRESS: " + ips[0]);


                        UserCityUpdate userCityUpdate = new UserCityUpdate(user, ips[0],req.getCallingUserId());
                        awsSQSManager.sendToSQS(SQSQueue.USER_CITY_UPDATE_QUEUE, SQSMessageType.USER_CITY_UPDATE, new Gson().toJson(userCityUpdate));
//                        AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.USER_CITY_UPDATE, payload1);
//                        asyncTaskFactory.executeTask(params);
                    }
                }
            } catch (Exception e) {
                logger.warn(e);
            }
        }
        // creating account
        try {
            createUserAccount(user);
        } catch (VException e1) {
            logger.error("Error in creating account while signUp", e1);
        }

        // post sign up tasks
//        Map<String, Object> payload = new HashMap<>();
//        payload.put("user", user);
//        payload.put("callingUserId", req.getCallingUserId());
//        AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.SIGNUP_TASKS, payload);
//        asyncTaskFactory.executeTask(params);
        logger.info("sending the SIGNUP_TASKS_REQ with user : "+user);
        awsSQSManager.sendToSQS(SQSQueue.SIGNUP_TASKS_QUEUE, SQSMessageType.SIGNUP_TASKS_REQ, new Gson().toJson(user));
        logger.info("SIGNUP_TASKS_REQ  queue triggered");

        //email for bulk user creation
        if (req.getSignUpFeature() != null && req.getSignUpFeature().equals(FeatureSource.ORG_USER_SIGNUP)) {
            logger.info("sending welcome email to : " + user.getEmail());
            HashMap<String, Object> bodyScopes = new HashMap<>();
            bodyScopes.put("studentName", user.getFullName());
            bodyScopes.put("password", password);

            CommunicationType emailType = CommunicationType.ORG_USER_SIGNUP;

            HashMap<String, Object> subjectScopes = new HashMap<>();

            ArrayList<InternetAddress> toList = new ArrayList<>();
            toList.add(new InternetAddress(user.getEmail(), user.getFullName()));
            EmailRequest emailRequest = new EmailRequest(toList, subjectScopes, bodyScopes, emailType, user.getRole());
            communicationManager.sendEmail(emailRequest);
            if (emailType == CommunicationType.ORG_USER_SIGNUP) {
                CommunicationType communicationType = CommunicationType.ORG_USER_SIGNUP;
                TextSMSRequest textSMSRequest = new TextSMSRequest(user.getContactNumber(),
                        user.getPhoneCode(), bodyScopes, communicationType, user.getRole());
                communicationManager.sendSMS(textSMSRequest);
            }
        }

        if (StringUtils.isNotEmpty(req.getReferrerEncryptCode()) && user.getId() != null) {
            Map<String, String> payload = new HashMap<>();
            payload.put("userId", user.getId().toString());
            payload.put("referrerEncryptCode", req.getReferrerEncryptCode());
            awsSQSManager.sendToSQS(SQSQueue.SIGNUP_TASKS_QUEUE, SQSMessageType.SIGNUP_TASKS_REFERRAL_REQ, new Gson().toJson(payload));
        }

        return user;
    }

    // create user account on sign up
    private void createUserAccount(User user) throws VException {
        if (user != null && user.getId() != null) {
            logger.info("Request received to create User Account : " + user.getId());
//            try {
//                logger.info("calling the dinero api for creating new account");
//                ClientResponse resp = WebUtils.INSTANCE.doCall(dineroEndpoint + "/account/createUserAccount", HttpMethod.POST,
//                        gson.toJson(user));
//                VExceptionFactory.INSTANCE.parseAndThrowException(resp);
////            String jsonString = resp.getEntity(String.class);
//                logger.info("Account has been created successfully : " + resp);

                logger.info("triggering the Account creation queue CREATE_NEW_USER_ACCOUNT_QUEUE with user : " + user);
                awsSQSManager.sendToSQS(SQSQueue.CREATE_NEW_USER_ACCOUNT_QUEUE, SQSMessageType.CREATE_NEW_USER_ACCOUNT_REQ, new Gson().toJson(user), String.valueOf(user.getId()));
                logger.info("sqs queue triggered successfully");

//            } catch (VException ex) {
//
//            }
       }
        else {
            logger.error("user object not found at creating the new Account");
        }
    }

    // perform sign up tasks
    public void performSignUpTasks(User user, Long callingUserId) {
        try {
            //
            if (user.getReferrerCode() != null && !user.getRole().equals(Role.ADMIN)) {
                // call platform
                ProcessReferralBonusReq request = new ProcessReferralBonusReq();
                request.setReferralStep(ReferralStep.SIGNUP);
                request.setUserId(user.getId());
                String platformEndpoint = ConfigUtils.INSTANCE.getStringValue("PLATFORM_ENDPOINT");
                ClientResponse resp = WebUtils.INSTANCE.doCall(platformEndpoint + "/referral/processReferralBonusForSignUp",
                        HttpMethod.POST, new Gson().toJson(request));
                //handle error correctly
                VExceptionFactory.INSTANCE.parseAndThrowException(resp);

            }
        } catch (Exception e) {
            logger.error("processReferralBonus", e);
        }

        com.vedantu.User.User user1 = mapper.map(user, com.vedantu.User.User.class);
        boolean createLead = true;
        if (null != user.getSignUpFeature() && FeatureSource.ORG_USER_SIGNUP.equals(user.getSignUpFeature())) {
            createLead = false;
        }
        if (Role.STUDENT.equals(user.getRole()) && createLead) {
            try {
                LeadSquaredRequest req = new LeadSquaredRequest(LeadSquaredAction.POST_LEAD_DATA,
                        LeadSquaredDataType.LEAD_UPSERT, user1, null, null);
                String platformEndpoint = ConfigUtils.INSTANCE.getStringValue("PLATFORM_ENDPOINT");
                ClientResponse resp = WebUtils.INSTANCE.doCall(platformEndpoint + "/leadsquared/postLeadSquaredData",
                        HttpMethod.POST, new Gson().toJson(req));
            } catch (Exception e) {
                logger.error("Exception occured in pushing data to lead squared");
            }
        }

        //Early Learning User Signup LeadSquared Entry
        logger.info("user.getSignUpURL : "+user.getSignUpURL());
        if(Role.STUDENT.equals(user.getRole()) && user.getSignUpURL() !=null && user.getSignUpURL().toLowerCase().contains("superkids")){
            Map<String,String> userLead = new HashMap();

            if(Objects.nonNull(user) && Objects.nonNull(user.getId())) {
                createEarlyLearningUserLeadsMap(userLead,user);
                logger.info("userLead : "+userLead);
                LeadSquaredRequest req = new LeadSquaredRequest(LeadSquaredAction.POST_LEAD_DATA, LeadSquaredDataType.LEAD_ACTIVITY_SUPERKIDS_SIGNUP, null, userLead, null);
                String messageGroupId = user.getId() + "_SIGNUP";
                awsSQSManager.sendToSQS(SQSQueue.LEADSQUARED_QUEUE, SQSMessageType.CUSTOMER_SIGNUP_SUPERKIDS, gson.toJson(req), messageGroupId);
            }
        }



        // send registration mail
        try {
            if (user != null && (FeatureSource.ISL.equals(user.getSignUpFeature())
                    || FeatureSource.ISL_REGISTRATION_VIA_TOOLS.equals(user.getSignUpFeature()))
                    || FeatureSource.VSAT.equals(user.getSignUpFeature())
                    || FeatureSource.VOLT_2020_JAN.equals(user.getSignUpFeature())
                    || FeatureSource.REVISEINDIA_2020_FEB.equals(user.getSignUpFeature())
                    || FeatureSource.REVISE_JEE_2020_MARCH.equals(user.getSignUpFeature())) {
                if (Role.STUDENT.equals(user.getRole())) {

                    com.vedantu.User.User vedantuUser = mapper.map(user, com.vedantu.User.User.class);

                    if (FeatureSource.ISL.equals(user.getSignUpFeature())) {
                        RegistrationEmailReq emailReq = new RegistrationEmailReq(vedantuUser);
                        String notificationEndpoint = ConfigUtils.INSTANCE.getStringValue("PLATFORM_ENDPOINT");
                        ClientResponse resp = WebUtils.INSTANCE.doCall(notificationEndpoint + "/notification-centre/email/sendRegistrationEmailForISL",
                                HttpMethod.POST, new Gson().toJson(emailReq));
                    } else if (FeatureSource.ISL_REGISTRATION_VIA_TOOLS.equals(user.getSignUpFeature())) {
                        RegistrationEmailReq emailReq = new RegistrationEmailReq(vedantuUser);
                        String notificationEndpoint = ConfigUtils.INSTANCE.getStringValue("PLATFORM_ENDPOINT");
                        ClientResponse resp = WebUtils.INSTANCE.doCall(notificationEndpoint + "/notification-centre/email/sendEmailRegistrationISLViaTools",
                                HttpMethod.POST, new Gson().toJson(emailReq));
                    } else if (FeatureSource.VSAT.equals(user.getSignUpFeature())) {
                        RegistrationEmailReq emailReq = new RegistrationEmailReq(vedantuUser);
                        String notificationEndpoint = ConfigUtils.INSTANCE.getStringValue("PLATFORM_ENDPOINT");
                        ClientResponse resp = WebUtils.INSTANCE.doCall(notificationEndpoint + "/notification-centre/email/sendVSATRegistrationEmail",
                                HttpMethod.POST, new Gson().toJson(emailReq));
                    } else if (FeatureSource.VOLT_2020_JAN.equals(user.getSignUpFeature())) {
                        RegistrationEmailReq emailReq = new RegistrationEmailReq(vedantuUser);
                        String notificationEndpoint = ConfigUtils.INSTANCE.getStringValue("PLATFORM_ENDPOINT");
                        ClientResponse resp = WebUtils.INSTANCE.doCall(notificationEndpoint + "/notification-centre/email/sendVoltRegistrationEmail",
                                HttpMethod.POST, new Gson().toJson(emailReq));
                    } else if (FeatureSource.REVISEINDIA_2020_FEB.equals(user.getSignUpFeature())) {
                        RegistrationEmailReq emailReq = new RegistrationEmailReq(vedantuUser);
                        String notificationEndpoint = ConfigUtils.INSTANCE.getStringValue("PLATFORM_ENDPOINT");
                        ClientResponse resp = WebUtils.INSTANCE.doCall(notificationEndpoint + "/notification-centre/email/sendRegistrationEmailForReviseIndia",
                                HttpMethod.POST, new Gson().toJson(emailReq));
                    } else if (FeatureSource.REVISE_JEE_2020_MARCH.equals(user.getSignUpFeature())) {
                        RegistrationEmailReq emailReq = new RegistrationEmailReq(vedantuUser);
                        String notificationEndpoint = ConfigUtils.INSTANCE.getStringValue("PLATFORM_ENDPOINT");
                        ClientResponse resp = WebUtils.INSTANCE.doCall(notificationEndpoint + "/notification-centre/email/sendRegistrationEmailForReviseJee",
                                HttpMethod.POST, new Gson().toJson(emailReq));
                    }
                }
            }
        } catch (Exception e) {
            logger.error("Exception occured in sending registration mail to user ");
        }

    }

    public void createEarlyLearningUserLeadsMap(Map<String,String> userLead, User user){

        if(Objects.nonNull(user.getStudentInfo())){
            if(ArrayUtils.isNotEmpty(user.getStudentInfo().getParentInfos())) {
                userLead.put(LeadSquaredRequest.Constants.PARENT_NAME,
                        Optional.ofNullable(user.getStudentInfo().getParentInfos().get(0).getFirstName()).orElse(""));
                userLead.put(LeadSquaredRequest.Constants.PARENT_EMAIL,
                        Optional.ofNullable(user.getStudentInfo().getParentInfos().get(0).getEmail()).orElse(""));
                userLead.put(LeadSquaredRequest.Constants.PARENT_PHONE,
                        Optional.ofNullable(user.getStudentInfo().getParentInfos().get(0).getContactNumber()).orElse(""));
            }
            userLead.put(LeadSquaredRequest.Constants.GRADE,Optional.ofNullable(user.getStudentInfo().getGrade()).orElse(""));
            userLead.put(LeadSquaredRequest.Constants.BOARD,Optional.ofNullable(user.getStudentInfo().getBoard()).orElse(""));
            if(Objects.nonNull(user.getStudentInfo().getDeviceType())) {
                userLead.put(LeadSquaredRequest.Constants.DEVICE_TYPE, Optional.ofNullable(user.getStudentInfo().getDeviceType().name()).orElse(""));
            }
            userLead.put(LeadSquaredRequest.Constants.CUSTOMER_STATUS,"Signed Up");
        }
        userLead.put(LeadSquaredRequest.Constants.CHILD_NAME,Optional.ofNullable(user.getFirstName()).orElse(""));
        userLead.put(LeadSquaredRequest.Constants.STUDENT_ID,String.valueOf(user.getId()));
    }

    public ProfileBuilderPostLoginRes profileBuilderPostLogin(ProfileBuilderPostLoginReq req, HttpServletRequest request, HttpServletResponse httpResponse) throws VException, IOException, URISyntaxException {
        ProfileBuilderPostLoginRes response = new ProfileBuilderPostLoginRes();

        logger.info("ProfileBuilderPostLoginReq req = "+req);

        HttpSessionData sessionData = sessionUtils.getCurrentSessionData();

        if (null == sessionData) {
            throw new ForbiddenException(ErrorCode.NOT_LOGGED_IN, "not logged in");
        }

        if (null != req.getEmail()) {
            User user = getUserByEmail(req.getEmail());
            if (null != user && null != sessionData && null != sessionData.getUserId() && !user.getId().equals(sessionData.getUserId())) {
                throw new ForbiddenException(ErrorCode.BAD_REQUEST_ERROR, "Enter a different email as this is already in use");
            }
        }

        User user = userDAO.getUserByUserId(sessionData.getUserId());

        if (null != user && StringUtils.isNotEmpty(req.getEmail()) && StringUtils.isNotEmpty(user.getEmail()) && !(req.getEmail().trim().equals(user.getEmail().trim()))) {
            throw new ForbiddenException(ErrorCode.BAD_REQUEST_ERROR, "Email cannot be changed");
        }

        if (StringUtils.isNotEmpty(req.getEmail())) {

            if(CommonUtils.isValidEmailId(req.getEmail())) {
                user.setEmail(req.getEmail());
            }
            else{
                throw new BadRequestException(ErrorCode.INVALID_EMAILID, "email id is not valid");
            }

        }

        if (null != req.getFullName()) {
            user.setFullName(req.getFullName());
        }

        String[] name = (null != req.getFullName()) ? req.getFullName().trim().split("\\s+") : null;

        if (null != name) {
            user.setFirstName(name[0]);
        }

        if (null != name && name.length > 1) {
            StringBuffer sb = new StringBuffer();

            for (int i = 1; i < name.length; i++) {
                sb.append(name[i] + " ");
            }

            String lastName = sb.toString();

            user.setLastName(lastName);
        }

        if (null != user.getStudentInfo()) {
        	logger.info("user.getStudentInfo() is not null");
            if (null != req.getGrade()) {
                user.getStudentInfo().setGrade(req.getGrade());
            }

            if(req.getDeviceType() != null) {
                user.getStudentInfo().setDeviceType(req.getDeviceType());
            }

            if (null != req.getBoard()) {
                user.getStudentInfo().setBoard(req.getBoard());
            }

            if (null != req.getTarget()) {
                user.getStudentInfo().setTarget(req.getTarget());
            }

            if (null != req.getTarget()) {
                if (null != user.getStudentInfo().getExamTargets()) {
                    // Handle for multiple targets. split and add to exam targets.
                    List<String> targets = getTargetsFromTargetString(req.getTarget());
                    targets.addAll(user.getStudentInfo().getExamTargets());

                    // Add unique values only
                    Set<String> targetSet = new HashSet<>(targets);
                    List<String> targetList = new ArrayList<>(targetSet);
                    user.getStudentInfo().setExamTargets(targetList);
                } else {
                    List<String> targets = new ArrayList<>();
                    targets.add(req.getTarget());
                    user.getStudentInfo().setExamTargets(targets);
                }
            }

            if (StringUtils.isNotEmpty(req.getSchool())) {
                user.getStudentInfo().setSchool(req.getSchool());
            }

            if(null != req.getStream()) {
            	logger.info("req has Stream");
            	user.getStudentInfo().setStream(req.getStream());
            }

        } else {
        	logger.info("user.getStudentInfo() is not null");
            StudentInfo studentInfo = new StudentInfo();

            if (null != req.getBoard()) {
                studentInfo.setBoard(req.getBoard());
            }

            if (null != req.getGrade()) {
                studentInfo.setGrade(req.getGrade());
            }

            if (null != req.getTarget()) {
                studentInfo.setTarget(req.getTarget());
            }

            if(req.getDeviceType() != null) {
                user.getStudentInfo().setDeviceType(req.getDeviceType());
            }

            if (null != req.getTarget()) {
                if (null != studentInfo.getExamTargets()) {
                    studentInfo.getExamTargets().add(req.getTarget());
                } else {
                    List<String> targets = new ArrayList<>();
                    targets.add(req.getTarget());
                    studentInfo.setExamTargets(targets);
                }
            }

            if (StringUtils.isNotEmpty(req.getSchool())) {
                user.getStudentInfo().setSchool(req.getSchool());
            }

            if(null != req.getStream()) {
            	logger.info("req has Stream");
            	studentInfo.setStream(req.getStream());
            }

            user.setStudentInfo(studentInfo);
        }

        if (null != req.getLocationInfo()) {
            user.setLocationInfo(req.getLocationInfo());
        }

        userDAO.update(user, req.getCallingUserId());
        com.vedantu.User.User user1 = mapper.map(user, com.vedantu.User.User.class);
        sessionUtils.setCookieAndHeaders(request, httpResponse, user1);
        response.setUser(user1);

        if (null != req.getEvent()) {
            UserDetails userDetails = userDetailsDAO.getUserDetailsByUserId(user.getId(), req.getEvent());

            if (userDetails != null) {
                if (StringUtils.isNotEmpty(req.getEmail())) {

                    if(CommonUtils.isValidEmailId(req.getEmail())) {
                        userDetails.setEmail(req.getEmail());
                    }
                    else{
                        throw new BadRequestException(ErrorCode.INVALID_EMAILID, "email id is not valid");
                    }

                    if(CommonUtils.isValidEmailId(req.getEmail())) {
                        userDetails.setEmail(req.getEmail());
                    }
                    else{
                        throw new BadRequestException(ErrorCode.INVALID_EMAILID, "email id is not valid");
                    }

                }

                if (StringUtils.isNotEmpty(req.getSchool())) {
                    userDetails.setSchool(req.getSchool());
                }

                if (null != req.getLocationInfo()) {
                    if (StringUtils.isNotEmpty(req.getLocationInfo().getStreetAddress())) {
                        userDetails.setAddress(req.getLocationInfo().getStreetAddress());
                    }
                    if (StringUtils.isNotEmpty(req.getLocationInfo().getCity())) {
                        userDetails.setCity(req.getLocationInfo().getCity());
                    }
                    if (StringUtils.isNotEmpty(req.getLocationInfo().getCountry())) {
                        userDetails.setCountry(req.getLocationInfo().getCountry());
                    }

                }

                userDetailsDAO.save(userDetails, user.getId());
            }
        }

        String key = env + "_USER_BASIC_" + sessionData.getUserId();

        try {
            redisDAO.del(key);
        } catch (Exception e) {
            logger.warn("Could not delete redis key : {}", key);
        }

        return response;
    }

    private List<String> getTargetsFromTargetString(String target) {
        List<String> examTargets = new ArrayList<>();
        if (null != target) {
            if (target.contains(",")) {
                examTargets = Arrays.asList(target.split(","));
            } else {
                examTargets.add(target);
            }
        }
        return examTargets;
    }

    public PlatformBasicResponse addEmailToUser(Long userId, String email) throws NotFoundException, ConflictException, BadRequestException {
        PlatformBasicResponse res = new PlatformBasicResponse();
        Long loggedInUserId = sessionUtils.getCallingUserId();
        if (Role.ADMIN.equals(sessionUtils.getCallingUserRole())) {
            if (StringUtils.isNotEmpty(email)) {
                if (ArrayUtils.isEmpty(userDAO.getUsersByEmail(email))) {
                    User user = userDAO.getUserByUserId(userId);
                    if (null == user) {
                        throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, "user does not exist for " + userId,
                                Level.ERROR);
                    }
                    if(StringUtils.isNotEmpty(email)){

                        if(CommonUtils.isValidEmailId(email)) {
                            user.setEmail(email);
                        }
                        else{
                            throw new BadRequestException(ErrorCode.INVALID_EMAILID, "email id is not valid");
                        }
                    }
                    userDAO.update(user, loggedInUserId);
                    res.setResponse("Email added successfully");
                    res.setSuccess(true);
                } else {
                    res.setErrorCode(String.valueOf(ErrorCode.DUPLICATE_ENTRY));
                    res.setSuccess(false);
                    res.setErrorMessage("Enter a different email as this is already in use");
                    res.setResponse("Enter a different email as this is already in use");
                }
            }

        } else {
            res.setErrorCode(String.valueOf(ErrorCode.ACTION_NOT_ALLOWED));
            res.setSuccess(false);
            res.setErrorMessage("Insufficient Priviledges");
            res.setResponse("Insufficient Priviledges");
        }

        return res;
    }

    public Long getIdByContactNumber(String phoneNumber) {
        Long studentId = userDAO.getIdByContactNumber(phoneNumber);
        logger.info("Student id sending from User : {}", studentId);
        return studentId;
    }

    public UserPreSignUpVerificationInfoRes preSignUpVerification(UserPreSignUpVerificationInfoReq req) throws VException {
        UserPreSignUpVerificationInfoRes response = new UserPreSignUpVerificationInfoRes();

        String phoneCode = req.getPhoneCode();
        String phone = req.getPhoneNumber();
        String email = req.getEmail();

        Boolean emailUnique = false;
        Boolean phoneUnique = false;

        // check if email is unique
        User emailuser = null;
        if (StringUtils.isNotEmpty(email)) {
            emailuser = userDAO.getUserByEmail(email);
        }
        if (null == emailuser) {
            emailUnique = true;
            response.setEmailUnique(true);
        }

        // check if phone is unique
        User phoneUser = userDAO.getUserByPhoneNumberAndCode(phone, phoneCode);
        if (null == phoneUser) {
            phoneUnique = true;
            response.setPhoneUnique(true);
        }

        String testLoginInternalNumber = TestAccontPasswordFactory.getTestContactNumber(TestAccontPasswordFactory.Constants.TEST_SIGNUP_NUMBER);

        if (testLoginInternalNumber.equals(phone)) {
            phoneUnique = true;
            response.setPhoneUnique(true);
        }

        if (phoneUnique && emailUnique) {
            sendOTPToPhone(req.getPhoneNumber(), req.getPhoneCode(), false);
        }
        return response;
    }

    public com.vedantu.User.User addUserEmailSeoPdf(String email) throws ForbiddenException, ConflictException, BadRequestException {
        Long userId = sessionUtils.getCallingUserId();

        if(!CommonUtils.isValidEmailId(email)){
            throw new BadRequestException(ErrorCode.INVALID_EMAILID, "email id is not valid");
        }
        User user = userDAO.getUserByUserId(userId);

        if (StringUtils.isNotEmpty(user.getEmail())) {
            throw new ForbiddenException(ErrorCode.FORBIDDEN_ERROR, "Operation Not Allowed");
        }

        User existingUser = userDAO.getUserByEmail(email);
        if (null != existingUser) {
            throw new ForbiddenException(ErrorCode.FORBIDDEN_ERROR, "Email already exists");
        }

        if(StringUtils.isNotEmpty(email)) {
            if(CommonUtils.isValidEmailId(email)) {
                user.setEmail(email);
            }
            else{
                throw new BadRequestException(ErrorCode.INVALID_EMAILID, "email id is not valid");
            }
        }

        try {
            userDAO.update(user, userId);
        }catch(Exception e){
            logger.info("Error in updating user email " + e.getMessage());
        }

        return pojoUtils.convertToUserPojo(user);
    }

    public List<UserDetailsInfo> getUserDetailsForVolt(GetVoltUserDetailsReq req) {
        List<UserDetails> details = userDetailsDAO.getUserDetailsForVolt(req, false);
        return getUserDetailsInfos(details);
    }

    private List<UserDetailsInfo> getUserDetailsInfos(List<UserDetails> details) {
        List<Long> ids = details.stream()
                .filter(Objects::nonNull)
                .map(UserDetails::getUserId)
                .filter(Objects::nonNull)
                .collect(toList());
        List<UserBasicInfo> userBasicInfos = getUserBasicInfos(ids, false);
        Map<Long, UserBasicInfo> basicInfoMap = userBasicInfos.stream()
                .filter(Objects::nonNull)
                .collect(Collectors.toMap(UserBasicInfo::getUserId, e -> e));
        List<UserDetailsInfo> detailsInfos = new ArrayList<>();
        for (UserDetails detail : details) {
            UserDetailsInfo info = detail.toUserDetailsInfo(detail, basicInfoMap.get(detail.getUserId()), true);
            detailsInfos.add(info);
        }
        return detailsInfos;
    }

    public String getUserDetailsForVoltCsv(GetVoltUserDetailsReq req) throws IOException {
        List<UserDetails> details = new ArrayList<>();
        req.setSize(25);
        String header = String.join(",", "Profile Pic Link", "Name", "EmailId", "Phone No", "Grade", "State", "City",
                "School Name", "VOLT ID", "Date of Registration", "Time of Registration", "Date of Application", "Time of Application",
                "Registration Status", "Reason",
                System.lineSeparator());

        Path page = Files.createTempFile("page", ".csv");
        Files.write(page, header.getBytes(), StandardOpenOption.CREATE, StandardOpenOption.APPEND);

        int start = 0;
        do {
            StringBuilder builder = new StringBuilder();
            req.setStart(start);
            start = start + req.getSize();
            details = userDetailsDAO.getUserDetailsForVolt(req, true);
            List<UserDetailsInfo> infos = getUserDetailsInfos(details);
            for (UserDetailsInfo info : infos) {

                ZonedDateTime created = DateTimeUtils.getZonedDateTimeIst(info.getCreationTime());
                UserBasicInfo student = info.getStudent();
                String profilePic = student == null ? "" : student.getProfilePicUrl();
                ZonedDateTime approvedTime = info.getApprovedTime() == null ? created : DateTimeUtils.getZonedDateTimeIst(info.getApprovedTime());
                builder.append(defaultIfNull(profilePic, "")).append(",")
                        .append(escapeCsv(defaultIfNull(info.getStudentName(), ""))).append(",")
                        .append(escapeCsv(defaultIfNull(info.getEmail(), ""))).append(",")
                        .append(escapeCsv(defaultIfNull(info.getStudentPhoneNo(), ""))).append(",")
                        .append(escapeCsv(String.valueOf(defaultIfNull(info.getGrade(), "")))).append(",")
                        .append(escapeCsv(defaultIfNull(info.getState(), ""))).append(",")
                        .append(escapeCsv(defaultIfNull(info.getCity(), ""))).append(",")
                        .append(escapeCsv(defaultIfNull(info.getSchool(), ""))).append(",")
                        .append(escapeCsv(defaultIfNull(info.getVoltId(), ""))).append(",")
                        .append(escapeCsv(String.valueOf(approvedTime.toLocalDate()))).append(",")
                        .append(escapeCsv(String.valueOf(approvedTime.toLocalTime()))).append(",")
                        .append(escapeCsv(String.valueOf(created.toLocalDate()))).append(",")
                        .append(escapeCsv(String.valueOf(created.toLocalTime()))).append(",")
                        .append(escapeCsv(info.getVoltStatus().name())).append(",")
                        .append(escapeCsv(defaultIfNull(info.getApprovalReason(), ""))).append(",").append(",").append(System.lineSeparator());
            }
            Files.write(page, builder.toString().getBytes(), StandardOpenOption.CREATE, StandardOpenOption.APPEND);
        } while (details.size() == req.getSize());

        String key = ConfigUtils.INSTANCE.getEnvironmentSlug() + "/volt-csv/" + UUID.randomUUID() + ".csv";
        awsS3Manager.uploadFile(ConfigUtils.INSTANCE.getStringValue("aws.bucket.isl"), key, page.toFile());
        logger.info(page.toAbsolutePath());
        page.toFile().delete();
        return awsS3Manager.getPublicBucketDownloadUrl(ConfigUtils.INSTANCE.getStringValue("aws.bucket.isl"), key);
    }

    public void approveActionForUserDetails(UserDetailsVoltApproveReq req) throws VException, UnsupportedEncodingException {
        if (req.getStatus() == null) {
            throw new BadRequestException(ErrorCode.SERVICE_ERROR, "volt status to be set can't be null");
        }

        if (req.getStatus() == VoltStatus.REJECTED && StringUtils.isEmpty(req.getReason())) {
            throw new BadRequestException(ErrorCode.SERVICE_ERROR, "reason can't be empty");
        }

        UserDetails entity = userDetailsDAO.getEntityById(req.getId(), UserDetails.class);

        if (!entity.getEvent().equals("VOLT_2020_JAN")) {
            throw new BadRequestException(ErrorCode.SERVICE_ERROR, "Requested user is not registered for volt");
        }

        entity.setVoltStatus(req.getStatus());
        entity.setApprovalReason(req.getReason());
        entity.setApprovedTime(System.currentTimeMillis());

        userDetailsDAO.save(entity, sessionUtils.getCallingUserId());

        sendActionEmailForVolt(req, entity);

    }

    private void sendActionEmailForVolt(UserDetailsVoltApproveReq req, UserDetails entity) throws UnsupportedEncodingException, VException {
        User user = userDAO.getUserByUserId(entity.getUserId());
        logger.info("sending welcome email to : " + user.getEmail());
        HashMap<String, Object> bodyScopes = new HashMap<>();
        bodyScopes.put("studentName", user._getFullName());
        bodyScopes.put("studentEmail", user.getEmail());
        if (user.getStudentInfo() != null) {
            bodyScopes.put("parentName", user.getStudentInfo().getParentFirstName());
        }
        if (StringUtils.isNotEmpty(user.getContactNumber())) {
            bodyScopes.put("contactNumber", user.getContactNumber());
        } else if (StringUtils.isNotEmpty(user.getTempContactNumber())) {
            bodyScopes.put("contactNumber", user.getTempContactNumber());
        } else {
            bodyScopes.put("contactNumber", user.getContactNumber());
        }

        String loginUrl = ConfigUtils.INSTANCE.getStringValue("web.baseUrl");
        bodyScopes.put("loginUrl", loginUrl);

        bodyScopes.put("baseUrl", ConfigUtils.INSTANCE.getStringValue("url.base"));

        CommunicationType emailType;
        if (entity.getVoltStatus() == VoltStatus.REGISTERED) {
            emailType = CommunicationType.APPROVED_EMAIL_VOLT;
        } else if (entity.getVoltStatus() == VoltStatus.REJECTED) {
            emailType = CommunicationType.REJECTED_EMAIL_VOLT;
        } else {
            return;
        }

        // NEED TO MAKE CHANGES HERE
        if (entity != null) {
            bodyScopes.put("studentTarget", entity.getExam());
            if (null != entity.getGrade()) {
                bodyScopes.put("studentGrade", entity.getGrade().toString());
            }
        }

        HashMap<String, Object> subjectScopes = new HashMap<>();
        subjectScopes.put("name", user.getFirstName());
        ArrayList<InternetAddress> toList = new ArrayList<>();
        toList.add(new InternetAddress(user.getEmail(), user._getFullName()));
        EmailRequest request = new EmailRequest(toList, subjectScopes, bodyScopes, emailType, user.getRole());

        request.setIncludeHeaderFooter(Boolean.FALSE);
        request.setEmailService(EmailService.MANDRILL);
        communicationManager.sendEmailViaRest(request);
        TextSMSRequest textSMSRequest = new TextSMSRequest(user.getContactNumber(),
                user.getPhoneCode(), bodyScopes, emailType, user.getRole());
        communicationManager.sendSMSViaRest(textSMSRequest);

    }

    public void addDummyNumber(UserAddDummyNumberReq req) throws BadRequestException, ForbiddenException, ConflictException {

        if (null == req.getUserId()) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "userId cannot be null");
        }

        User user = userDAO.getUserByUserId(req.getUserId());
        if (null == user) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "User does not exist");
        }

        PhoneNumber phoneNumber = new PhoneNumber();

        if (null != user.getContactNumber()) {
            phoneNumber.setNumber(user.getContactNumber());
            phoneNumber.setIsVerified(user.getIsContactNumberVerified());
            user.getPhones().add(phoneNumber);
        }

        String dummyNumber = getDummyNumber();
        user.setContactNumber(dummyNumber);
        user.setIsContactNumberVerified(false);

        userDAO.update(user, req.getCallingUserId());
    }

    private String getDummyNumber() {
        Long dummyNumber = counterService.getNextSequence(User.Constants.DUMMY_NUMBER, 1);
        return String.valueOf(dummyNumber);
    }

    public void setVoltTestAttemptStatus(Long userId, String testId) {
        UserDetails details = userDetailsDAO.getUserDetailsByUserId(userId, "VOLT_2020_JAN");
        if(null != details) {
            details.setTestAttempted(true);
            details.setTestId(testId);
            userDetailsDAO.save(details, sessionUtils.getCallingUserId());
        }
    }

    public void setReviseJeeTestAttemptStatus(Long userId, String testId, String event) {
        UserDetails details = userDetailsDAO.getUserDetailsByUserId(userId, event);
        details.setTestAttempted(true);
        details.setTestId(testId);
        userDetailsDAO.save(details, sessionUtils.getCallingUserId());
    }

    public GetTestAttemptRes getUserDetailForVolt(Long id) {
        UserDetails details = userDetailsDAO.getUserDetailsByUserId(id, "VOLT_2020_JAN");
        GetTestAttemptRes res = new GetTestAttemptRes();
        if(null == details){
            res.setAttempted(false);
            return res;
        }
        boolean attempt = details.getTestAttempted() == null ? false : details.getTestAttempted();
        res.setAttempted(attempt);
        res.setTestId(details.getTestId());
        return res;
    }

    public boolean truecallerVerify(final String payload, final String signedString, final String signatureAlgorithm) throws Exception {

        String keyType = ConfigUtils.INSTANCE.getStringValue("truecaller.keyType");
        String publicKeyString = ConfigUtils.INSTANCE.getStringValue("truecaller.key");

        final byte[] publicKeyBytes = DatatypeConverter.parseBase64Binary(publicKeyString);
        final PublicKey publicKey = KeyFactory.getInstance(keyType).generatePublic(new X509EncodedKeySpec(publicKeyBytes));

        final byte[] signatureByteArray = Base64.decodeBase64(signedString.getBytes(StandardCharsets.UTF_8));
        final byte[] payloadArray = payload.getBytes(StandardCharsets.UTF_8);

        Signature vSignature = Signature.getInstance(signatureAlgorithm);
        vSignature.initVerify(publicKey);
        vSignature.update(payloadArray);

        return vSignature.verify(signatureByteArray);
    }

    public TruecallerEntity getTruecallerData(String payload) {
        String jsonData = new String(DatatypeConverter.parseBase64Binary(payload));
        Type _type = new TypeToken<TruecallerEntity>() {
        }.getType();
        TruecallerEntity te = new Gson().fromJson(jsonData, _type);
        return te;

    }

    public TruecallerLoginRes truecallerLogin(TruecallerLoginReq req, HttpServletRequest request, HttpServletResponse response) throws Exception {
        boolean isVerified;
        try {
            isVerified = truecallerVerify(req.getPayload(), req.getSignature(), req.getSignatureAlgorithm());
        } catch (Exception e) {
            logger.info("Exception in Truecaller Verification " + e.getMessage());
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Error in Verication");
        }

        TruecallerEntity truecallerEntity = getTruecallerData(req.getPayload());
        String mobile = truecallerEntity.getPhoneNumber();
        long timeDifference = System.currentTimeMillis() / 1000 - truecallerEntity.getRequestTime();
        CountryToPhonePrefix countryToPhonePrefix = new CountryToPhonePrefix();
        String phoneCode = countryToPhonePrefix.prefixFor(truecallerEntity.getCountryCode());
        String phoneNumber = mobile.substring(mobile.indexOf(phoneCode) + phoneCode.length());
        TruecallerLoginRes trueRes = new TruecallerLoginRes();
        if (isVerified == true && null != phoneNumber && null != phoneCode && StringUtils.isNotEmpty(phoneCode) && StringUtils.isNotEmpty(phoneNumber)) {
            if (timeDifference < 900) {
                User u = userDAO.getUserByPhoneNumberAndCode(phoneNumber, phoneCode);
                if (null != u) {
                    if (!u.getRole().equals(Role.STUDENT)) {
                        throw new ForbiddenException(ErrorCode.ACTION_NOT_ALLOWED, "User is not Student");
                    }
                    if (!u.getIsContactNumberVerified()) {
                        u.setIsContactNumberVerified(true);
                        u.setContactNumberVerifiedBy(VerifiedType.TRUECALLER);
                        userDAO.update(u, req.getCallingUserId());
                    }
                    if (Boolean.FALSE.equals(u.getProfileEnabled())) {
                        throw new ForbiddenException(ErrorCode.USER_BLOCKED, "User is blocked.");
                    }

                    com.vedantu.User.User user1 = mapper.map(u, com.vedantu.User.User.class);
                    HttpSessionData sessionData = sessionUtils.setCookieAndHeaders(request, response, user1);
                    sessionData.setPasswordAutogenerated(u.getPasswordAutogenerated());
                    trueRes.setUser(user1);
                    trueRes.setUserDetails(sessionData);
                    trueRes.setSignatureVerified(true);

                } else {
                    trueRes.setSignatureVerified(true);
                    trueRes.setUserDetails(null);
                }
            } else {
                throw new BadRequestException(ErrorCode.VERIFICATION_CODE_EXPIRED, "Verification Expired");
            }
        } else {
//            trueRes.setSignatureVerified(false);
//            trueRes.setUserDetails(null);
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Error in Verication");
        }

        return trueRes;

    }

    public void updateDummyNumberForUsers(UserEmailUpdateReq req) throws ConflictException {
        List<Long> userIds = req.getUserIds();
        for (Long userId : userIds) {
            User user = userDAO.getUserByUserId(userId);
            String phone = getDummyNumber();
            user.setContactNumber(phone);
            user.setIsContactNumberVerified(false);
            userDAO.update(user, req.getCallingUserId());
        }
    }

    public void rectifyPhoneCode(Integer start, Integer size) throws ConflictException {
        List<User> users = userDAO.getUsersIncorrectPhoneCode(start, size);
        for (User user : users) {
            user.setPhoneCode("91");
            for (PhoneNumber phoneNumber : user.getPhones()) {
                phoneNumber.setPhoneCode("91");
            }
            userDAO.update(user, null);
        }
    }

    public void updateUserDetailsForVsat(CreateUserDetailsReq req) throws VException {
        List<Long> userIds = req.getUserIds();
        List<User> users = userDAO.getUsersById(userIds);

        String event1 = "VSAT_2020_JAN_4";
        String event2 = "VSAT_2020_FEB";
        String event3 = "VSAT_2020_FEB_2";
        String event4 = "VSAT_2020_FEB_3";

        for (User user : users) {
            UserDetailsInfo userDetails = checkUserDetailsForVSAT(user.getId(), event1, event2, event3, event4);
            if (userDetails == null) {
                continue;
            }
            LocationInfo locationInfo = new LocationInfo();
            locationInfo.setCity(userDetails.getCity());
            locationInfo.setCountry(userDetails.getCountry());
            locationInfo.setStreetAddress(userDetails.getAddress());

            createUserDetailsForEvent(user, event4, req.getCallingUserId(), userDetails.getAgentName(), userDetails.getAgentEmail(),
                    userDetails.getExam(), null, null, null, null, locationInfo, new HashMap<>());
        }
    }

    private UserDetailsInfo checkUserDetailsForVSAT(Long id, String event1, String event2, String event3, String event4) throws ConflictException, NotFoundException {
        UserDetailsInfo userDetails = null;

        if (isRegisteredForISL(id, event4).isIsRegistered()) {
            return null;
        } else if (isRegisteredForISL(id, event3).isIsRegistered()) {
            return getUserDetailsForISLInfo(id, event3);
        } else if (isRegisteredForISL(id, event2).isIsRegistered()) {
            return getUserDetailsForISLInfo(id, event2);
        } else if (isRegisteredForISL(id, event1).isIsRegistered()) {
            return getUserDetailsForISLInfo(id, event1);
        }

        return userDetails;
    }

    public List<UserInfo> getUserLocationInfoForUsers(String userIds) {
        List<User> users = new ArrayList<>();
        List<UserInfo> userInfos = new ArrayList<>();
        String[] userIdsArr = org.apache.commons.lang3.StringUtils.split(userIds, ",");
        if (userIdsArr != null) {
            List<Long> userIdsList = new ArrayList<>();
            for (String id : userIdsArr) {
                userIdsList.add(Long.parseLong(id));
            }
            if (ArrayUtils.isNotEmpty(userIdsList)) {
                users = userDAO.getUserLocationInfoForUsers(userIdsList);
            }

            if (ArrayUtils.isNotEmpty(users)) {
                for (User user : users) {
                    logger.info(user.toString());
                    String firstName = user.getFirstName();
                    String lastName = user.getLastName();
                    UserInfo info = new UserInfo(pojoUtils.convertToUserPojo(user), null, false);
                    info.setFirstName(firstName);
                    info.setLastName(lastName);
                    userInfos.add(info);
                }
            }
        }
        return userInfos;
    }

    public void updateBundleInfoForUserOnboarding(User user, Long callingUserId, Set<String> pendingBundleIdForOnboarding, Set<String> completedBundleIdForOnboarding) throws ConflictException {

        UserHistoryII userHistory = mapper.map(user, UserHistoryII.class);
        userHistory.setId(null);
        userHistory.setUserId(user.getId());
        userHistoryDAO.create(userHistory, callingUserId);

        userDAO.updateOnboardingData(callingUserId, pendingBundleIdForOnboarding, completedBundleIdForOnboarding);
    }

    public void updateUser(User user, Long currentUser) throws ConflictException {
        userDAO.update(user, currentUser);
    }

    public List<UserParentInfoRes> getUsersParentInfoByIds(List<Long> userIds, Integer start, Integer size) {
        if (size == null) {
            size = 200;
        }
        if (start == null) {
            start = 0;
        }

        List<User> usersInfo = userDAO.getUsersParentInfoByIds(userIds, start, size);
        List<UserParentInfoRes> usersParentInfo = new ArrayList<>();

        if (ArrayUtils.isEmpty(usersInfo)) {
            return usersParentInfo;
        }

        for (User userInfo : usersInfo) {
            usersParentInfo.add(new UserParentInfoRes(userInfo.getId(), userInfo.getEmail(), userInfo.getFirstName(), userInfo.getStudentInfo()));
        }
        logger.info("usersParentInfo:" + usersParentInfo);
        return usersParentInfo;

    }

    public void sendReviseJeeReminderSMS() {
        Long currentTime = System.currentTimeMillis();

        Long afterTime = currentTime - ONE_DAY_IN_MILLIS - TWO_HOUR_IN_MILLIS;
        Long beforeTime = currentTime - ONE_DAY_IN_MILLIS;

        List<UserDetails> userDetailsList = userDetailsDAO.getUserDetailForReviseJee(afterTime, beforeTime, "REVISE_JEE_2020_MARCH");

        for (UserDetails userDetail : userDetailsList) {
            String phone = userDetail.getStudentPhoneNo();
            String phoneCode = userDetail.getStudentPhoneCode();
            Map<String, Object> scopeParams = new HashMap<String, Object>();
            scopeParams.put("studentName", userDetail.getStudentName());

            try {
                TextSMSRequest textSMSRequest = new TextSMSRequest(phone, phoneCode,
                        scopeParams, CommunicationType.REMINDER_SMS_REVISEJEE, Role.STUDENT);
                communicationManager.sendSMS(textSMSRequest);
            } catch (Exception e) {
                logger.info("Error in sending SMS" + e.getMessage());
            }

        }
    }

    public PlatformBasicResponse orgUserSignUp(SignUpUserReq req) throws VException, UnsupportedEncodingException {
        // check if phone already exists
        req.verify();
        Org org = orgDAO.getOrgById(req.getOrgId());
        if (org == null) {
            throw new BadRequestException(ErrorCode.ORG_NOT_FOUND, "org not found");
        }
        if (null != req.getPhoneNumber()) {
            if (null == req.getPhoneCode()) {
                req.setPhoneCode("91");
            }
            User user = userDAO.getUserByPhoneNumberAndCode(req.getPhoneNumber(), req.getPhoneCode());
            if (null != user) {
                if (StringUtils.isNotEmpty(req.getOrgId()) && StringUtils.isNotEmpty(user.getOrgId())
                        && !req.getOrgId().equalsIgnoreCase(user.getOrgId())) {
                    throw new ForbiddenException(ErrorCode.USER_PART_OF_ANOTHER_ORG, "user already part of another org");
                }
                user.setOrgId(req.getOrgId());
                if (StringUtils.isEmpty(user.getEmail()) && StringUtils.isNotEmpty(req.getEmail())) {
                    if(CommonUtils.isValidEmailId(req.getEmail())) {
                        user.setEmail(req.getEmail());
                        userDAO.update(user, null);
                        return new PlatformBasicResponse();
                    }
                    else{
                        throw new BadRequestException(ErrorCode.INVALID_EMAILID, "email id is not valid");
                    }

                } else {
                    userDAO.update(user, null);
                    return new PlatformBasicResponse();
                }
            } else {
                if (StringUtils.isNotEmpty(req.getEmail())) {
                    User user2 = userDAO.getUserByEmail(req.getEmail().toLowerCase());
                    if (null != user2) {
                        if (StringUtils.isNotEmpty(req.getOrgId()) && StringUtils.isNotEmpty(user2.getOrgId())
                                && !req.getOrgId().equalsIgnoreCase(user2.getOrgId())) {
                            throw new ForbiddenException(ErrorCode.USER_PART_OF_ANOTHER_ORG, "user already part of another org");
                        }
                        user2.setOrgId(req.getOrgId());
                        user2.setContactNumber(req.getPhoneNumber());
                        user2.setPhoneCode(req.getPhoneCode());
                        PhoneNumber phoneNumber = new PhoneNumber(user2.getContactNumber(), user2.getPhoneCode(), false, false, false,
                                false);
                        List<PhoneNumber> numbers = new ArrayList<>();
                        numbers.add(phoneNumber);
                        user2.setPhones(numbers);
                        userDAO.update(user2, null);
                        return new PlatformBasicResponse();
                    }
                }
            }
        }

        createUserByVerificationCode(req, null, null);

        return new PlatformBasicResponse();
    }




    public List<String> getEarlyLearningTeachers(String earlyLearningCourseType) {
        String key = EARLY_LEARNING_TEACHERS_SUPER_CODER;
        //final String EARLY_LEARNING_TEACHERS_KEY = "EARLY_LEARNING_TEACHERS";
        List<String> teacherIds;

        try {
            if(EarlyLearningCourseType.SUPER_READER.toString().equals( earlyLearningCourseType )) {
                key = EARLY_LEARNING_TEACHERS_SUPER_READER;
            }
            String teacherIdsListJson = redisDAO.get(key);
            // retrieve list from redis -- else fallback-userDAO call
            if (null != teacherIdsListJson) {
                Type type = new TypeToken<List<String>>() {
                }.getType();
                teacherIds = gson.fromJson(teacherIdsListJson, type);
                return teacherIds;
            }
        } catch (BadRequestException | InternalServerErrorException e) {
            logger.error("Error while fetching values for redis key {}", key);
        }

        List<User> users = userDAO.getEarlyLearningTeachers(earlyLearningCourseType);
        teacherIds = Optional.ofNullable(users).orElseGet(ArrayList::new)
                .stream()
                .filter(Objects::nonNull)
                .map(user -> String.valueOf(pojoUtils.convertToUserPojo(user).getId()))
                .collect(Collectors.toList());

        try {
            // TODO update redis key when new early learning teachers are added
            redisDAO.set(key, gson.toJson(teacherIds));
        } catch (InternalServerErrorException e) {
            logger.error("Error while setting values for redis key {}", key);
        }

        return teacherIds;
    }

    public List<UserBasicInfo> getUserByOrgId(String orgId, int start, int limit) throws BadRequestException {
        if (StringUtils.isEmpty((orgId))) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "orgId is not found");
        }

        List<User> users = userDAO.getUsersByOrgId(orgId, start, limit);

        List<UserBasicInfo> userBasicInfoList = new ArrayList<>();

        if (users != null && ArrayUtils.isNotEmpty(users)) {
            for (User user : users) {
                if (user != null) {
                    com.vedantu.User.User userPojo = pojoUtils.convertToUserPojo(user);
                    UserBasicInfo userBasicInfo = new UserBasicInfo(userPojo, true);
                    userBasicInfoList.add(userBasicInfo);
                }
            }
        }
        return userBasicInfoList;
    }

    public void syncUserAndLead(List<String> phoneList) {

        List<User> users = userDAO.getUsersByContactNumbers(phoneList);

        for(User user : users) {
            Map<String, Object> payload = new HashMap<String, Object>();
            payload.put("user", user);
            AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.USER_UPDATION_TRIGGER, payload);
            asyncTaskFactory.executeTask(params);
        }
    }


    public UserBasicResponse getUserInfoByContactNumbers(Set<String> contactNumbers, Role role) {
        logger.info("ContactNumbers : {} for role : {}", contactNumbers, role);
        List<User> users = userDAO.getUserInfoByContact(contactNumbers, role);
        logger.info("Users fetched : {}", users);
        List<com.vedantu.User.User> userList = new ArrayList<>();

        if (!users.isEmpty()) {
            for (User user : users) {
                com.vedantu.User.User u = new com.vedantu.User.User();
                u.setId(user.getId());
                u.setContactNumber(user.getContactNumber());
                userList.add(u);
            }
        }
        UserBasicResponse userBasicResponse = new UserBasicResponse();
        if (userList.isEmpty()) {
            userBasicResponse.setSuccess(false);
            userBasicResponse.setErrorMessage("No user present with given contactNumbers.");
        } else {
            userBasicResponse.setSuccess(true);
            userBasicResponse.setUsers(userList);
        }
        return userBasicResponse;
    }

    public PlatformBasicResponse getXVedTokenUsingUserDetails(GetXVedTokenByUserDetailsReq req) throws IOException, URISyntaxException, BadRequestException, ConflictException {
        logger.info("req : " + req);
        com.vedantu.User.User user = null;
        if (StringUtils.isEmpty(req.getPhoneCode())) {
            req.setPhoneCode("91");
        }
        User userByPhone = userDAO.getUserByPhoneNumberAndCode(req.getContactNumber(), req.getPhoneCode());
        if (userByPhone != null) {
            logger.info("user By Phone : " + userByPhone);
            user = mapper.map(userByPhone, com.vedantu.User.User.class);
        } else {
            logger.info("user By Phone not found");
            User userByEmail = null;
            if (StringUtils.isNotEmpty(req.getEmail())) {
                userByEmail = userDAO.getUserByEmail(req.getEmail());
                if (userByEmail != null) {
                    logger.info("user By Email : " + userByEmail);
                    user = mapper.map(userByEmail, com.vedantu.User.User.class);
                }
            }
            if (userByEmail == null) {
                logger.info("USER not found with contact number and email : " + req);
                User createdUser = createUserByXVedTokenUserDetailsRequest(req);
                user = mapper.map(createdUser, com.vedantu.User.User.class);
            }
        }
        logger.info("before createing token user : " + user);
        String token = sessionUtils.getXVedToken(user);
        return new PlatformBasicResponse(true, token, "");
    }

    public User createUserByXVedTokenUserDetailsRequest(GetXVedTokenByUserDetailsReq req) throws ConflictException, BadRequestException {
        logger.info("creating user using req : " + req);
        if (StringUtils.isEmpty(req.getContactNumber())) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "contactNumber Required");
        }

//        if we want to create the user even contact number is emapty
//        if(StringUtils.isEmpty(req.getContactNumber()) && StringUtils.isNotEmpty(req.getEmail())){
//            Long phone = counterService.getNextSequence(User.Constants.DUMMY_NUMBER, 1);
//            req.setContactNumber(String.valueOf(phone));
//        }

        if (StringUtils.isEmpty(req.getFirstName())) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "firstName Required");
        }

        User user = new User(req.getEmail(), req.getContactNumber(), req.getFirstName(), req.getLastName(), req.getPassword(), req.getGender(), Role.STUDENT, req.getLocationInfo());
        if (StringUtils.isEmpty(req.getPassword())) {
            user.setPassword(StringUtils.randomNumericString(8));
        }
        if (StringUtils.isNotEmpty(user.getPassword())) {
            String hashedPassword = passwordEncoder.encode(user.getPassword());
            user.setPassword(hashedPassword);
            user.setPasswordHashed(true);
            user.setPasswordChangedAt(System.currentTimeMillis());
            user.addRecentHashedPassword(hashedPassword);
        }
        user.setPhoneCode(req.getPhoneCode());
        if (StringUtils.isNotEmpty(user.getContactNumber())) {
            PhoneNumber phoneNumber = new PhoneNumber(user.getContactNumber(), user.getPhoneCode(), false, false, false,
                    false);
            List<PhoneNumber> numbers = new ArrayList<>();
            numbers.add(phoneNumber);
            user.setPhones(numbers);
        }

        try {
            user.setReferralCode(getReferralCode(user));
        } catch (Exception ex) {
            logger.error("Error generating referral code for user " + user, ex);
        }
        user.setSignUpFeature(FeatureSource.INSTA_SOLVER);

        logger.info("creating user : " + user);
        userDAO.create(user, req.getCallingUserId());
        return user;
    }

    public PlatformBasicResponse migrationOfELTeachers(TeachersTagsUpdateReq teacherUpdateReq) throws ConflictException {


        Map<Long, List<EarlyLearningCourseType>> teacherTagMap = Optional.ofNullable(teacherUpdateReq.getTeacherTagsInformation())
                .orElseGet(ArrayList::new)
                .stream()
                .collect(Collectors.toMap(
                        x -> Long.valueOf(x.getTeacherId()),
                        x-> x.getEarlyLearningTag()
                ));

        if(!CollectionUtils.isEmpty(teacherTagMap.keySet())) {

            List<User> teacherData = userDAO.getEarlyLearningTeachersById(teacherTagMap.keySet());

            for(User teacher : teacherData) {

                try {
                    TeacherInfo teacherInfo = teacher.getTeacherInfo();
                    if(teacherInfo != null) {
                        List<EarlyLearningCourseType> earlyLearningCourses = teacherTagMap.get(teacher.getId());
                        teacherInfo.setEarlyLearningCourses(earlyLearningCourses);
                        userDAO.update(teacher, sessionUtils.getCallingUserId());
                    }
                }
                catch (Exception e){
                    logger.error("Update failed for the User : "+teacher.getId()+"  "+e.getMessage());
                }
            }
        }
        return new PlatformBasicResponse();
    }

    public List<UserBasicInfo> getEarlyLearningTeachersWithSearchString(List<String> earlyLearningteacherIds,String nameQuery) throws BadRequestException{

        List<UserBasicInfo> availableTeachers = new ArrayList<>();

        if(ArrayUtils.isEmpty(earlyLearningteacherIds)) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR,"earlyLearningteacherIds Can not be null or empty");
        }
        if(earlyLearningteacherIds.size() > 500) {
            logger.error("Change the logic of getEarlyLearningTeachersWithSearchString");
        }
        List<Long> teacherIds = earlyLearningteacherIds.stream().filter(StringUtils::isNotEmpty).map(Long::valueOf).collect(Collectors.toList());
        List<User> userInfos =userDAO.getEarlyLearningTeachersWithSearchString(nameQuery, teacherIds);
        logger.info("result : "+userInfos);

        if (ArrayUtils.isNotEmpty(userInfos)) {
            for (User user : userInfos) {
                com.vedantu.User.User userPojo = pojoUtils.convertToUserPojo(user);
                UserBasicInfo userBasicInfo = new UserBasicInfo(userPojo, true);
                if(teacherIds.contains(userBasicInfo.getUserId())) {
                    availableTeachers.add(userBasicInfo);
                }
            }
        }

        return availableTeachers;
    }

    public Boolean checkGivenUserIdsValidOrNot(List<Long> userIds) {
        if (ArrayUtils.isNotEmpty(userIds)) {
            List<User> users = userDAO.getUserIdsByUsingUserIds(userIds);
            if (users != null && users.size() == userIds.size()) {
                return true;
            } else if (users != null) {
                List<String> fetchedUserIds = new ArrayList<>();
                users.forEach(user -> fetchedUserIds.add(Long.toString(user.getId())));
                userIds.forEach(userId -> {
                    if (!fetchedUserIds.contains(userId)) {
                        logger.info("userId does not Exist in users : " + userId);
                    }
                });
            }
        }
        return false;
    }

    public void updateMemberCount(String message) {
        MemberCountPojo memberCount = null;
        if (StringUtils.isNotEmpty(message)) {
            memberCount = gson.fromJson(message, MemberCountPojo.class);
        }
        userDAO.incrementMemberCount(memberCount.getRole(), memberCount.getEmailConfirmed());
    }

    public PlatformBasicResponse getLoginTokenForTeacher(String email, String secret, HttpServletRequest request, HttpServletResponse response) throws IOException, URISyntaxException, ForbiddenException, NotFoundException {

        User user = userDAO.getUserByEmail(email);

        if(user == null)
            throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, "User not found with the given email address :" + email);

        if (StringUtils.isEmpty(secret) || !secret.equals("9GTsaDy4GCp2tShe")) {
            throw new ForbiddenException(ErrorCode.INVALID_SECRET, "invalid secret key");
        }
        com.vedantu.User.User user1 = mapper.map(user, com.vedantu.User.User.class);
        HttpSessionData sessionData = sessionUtils.setCookieAndHeaders(request, response, user1);
        sessionData.setPasswordAutogenerated(user.getPasswordAutogenerated());

        String token = response.getHeader("X-Ved-Token");
        return new PlatformBasicResponse(true, token, "");

    }

    public void updateLoginData(String email, String ipAddress) throws UnsupportedEncodingException, VException {
        LocationInfo locationFromIp = ipUtil.getLocationFromIp(ipAddress);
        String countryFromIp = "";
        if (locationFromIp != null) {
            countryFromIp = locationFromIp.getCountry();
            String isp = locationFromIp.getIsp();

            LoginData loginData = new LoginData();
            loginData.setCountry(countryFromIp);
            loginData.setEmail(email);
            loginData.setIpAddress(ipAddress);
            loginData.setLastLoginTime(System.currentTimeMillis());
            loginData.setIsp(isp);

            loginDataDao.save(loginData);
        }

        boolean sendManualPasswordEmail = false;

        List<String> allowedCountriesListFromRedis;
        String redisKey = "ALLOWED_COUNTRIES_LIST";
        try{
            String redisCountriesString = rateLimitingRedisDao.get(redisKey);
            String[] redisCountriesArr = redisCountriesString.split(",");
            allowedCountriesListFromRedis = new ArrayList(Arrays.asList(redisCountriesArr));
            allowedCountriesList = allowedCountriesListFromRedis;
            for(String country : allowedCountriesList){
                if(countryFromIp.equalsIgnoreCase(country)){
                    return;
                }
            }
            sendManualPasswordEmail = true;
        }catch (Exception e){
            logger.error("Error in getting key from redis " + redisKey);
        }

        if(sendManualPasswordEmail){
            ManualResetPasswordReq req = new ManualResetPasswordReq();
            req.setSecretKey("4dYHF*v)Uu42Q*");
            req.setEmail(email);
            userPasswordManager.resetPassword(req);
        }

    }

    private void enrollNewUserToPremiumBundles(User user1) throws VException{
//    	String subscriptionStatus = "";
    	String responseString = "";
//    	try {
//			subscriptionStatus = redisDAO.get(IS_SUBSCRIPTION_ACTIVE);
//			logger.debug("subscriptionStatus -> "+subscriptionStatus);
//		} catch (InternalServerErrorException | BadRequestException e) {
//			logger.error("Subscription Module Health Status retrieval from Redis Failed");
//		}

//    	if(null != subscriptionStatus && !subscriptionStatus.isEmpty() && subscriptionStatus.equalsIgnoreCase("true")) {
    		PremiumSubscriptionRequest req = new PremiumSubscriptionRequest();
    		String url = SUBSCRIPTION_ENDPOINT + "bundle/enrollNewUserToPremiumBundles?userId=" + user1.getId();
			if(StringUtils.isNotEmpty(user1.getStudentInfo().getGrade())) {
				req.setGrade(user1.getStudentInfo().getGrade());
			}
			if(StringUtils.isNotEmpty(user1.getStudentInfo().getBoard())) {
				req.setBoard(user1.getStudentInfo().getBoard());
			}
			if(StringUtils.isNotEmpty(user1.getStudentInfo().getStream())) {
				req.setStream(user1.getStudentInfo().getStream());
			}
			if(StringUtils.isNotEmpty(user1.getStudentInfo().getTarget())) {
				req.setTarget(user1.getStudentInfo().getTarget());
			}
			String query = new Gson().toJson(req);
			logger.debug("Sending request for enrolling user to Premium Bundles for user = "+user1.getId()+" " +query);
			ClientResponse responseFromSystem = WebUtils.INSTANCE.doCall(url.toString(), HttpMethod.POST, query, true, true);
			VExceptionFactory.INSTANCE.parseAndThrowException(responseFromSystem);
			responseString = responseFromSystem.getEntity(String.class);
			logger.debug("Response from Subscription for enrollNewUserToPremiumBundles for user = "+user1.getId()+" " +responseString.toString());


//    	}else {
//    		logger.error("Could not call Subscription for auto enrolling user to Premium Bundle as Module is down");
//    	}
    }

    public Boolean getHomepageAccessBucket(Long userId) throws InternalServerErrorException {
        if (Objects.isNull(userId)) {
            return Boolean.FALSE;
        }

        long modCounter = 0L;

        // get info from redis - and return if available
        List<Long> cachedAccessInfo = redisDAO.getHomepageAccessInfo(userId);
        if (CollectionUtils.isNotEmpty(cachedAccessInfo) && cachedAccessInfo.get(0) == 1) {
            logger.info("found set in cache -- returning resp : {}", cachedAccessInfo);
            return cachedAccessInfo.get(1) == 1;
        }

        HomePageUserBucket fetched = homePageUserBucketDAO.getUserBucket(userId);

        if (Objects.isNull(fetched) || Objects.isNull(fetched.getId())) {

            try {
                modCounter = redisDAO.incr(HOMEPAGE_BUCKET_COUNTER_KEY);
            } catch (InternalServerErrorException e) {
                logger.warn("error while fetching HOMEPAGE_BUCKET_COUNTER from cache {}", e.getErrorMessage());
            }

            HomePageUserBucket userBucket = HomePageUserBucket.builder()
                    .userId(userId)
                    .canAccessNewHomepage((modCounter - 1) % NEW_HOMEPAGE_BUCKETS == 0)
                    .build();
            homePageUserBucketDAO.create(userBucket);

            redisDAO.setHomepageAccessInfo(userId, String.valueOf(userBucket.getCanAccessNewHomepage() ? 1 : 0));
            return userBucket.getCanAccessNewHomepage();
        }

        redisDAO.setHomepageAccessInfo(userId, String.valueOf(fetched.getCanAccessNewHomepage() ? 1 : 0));
        return fetched.getCanAccessNewHomepage();
    }

    public PlatformBasicResponse removeEmailUserId(Long userId) throws ConflictException {
        User user = userDAO.getUserByUserId(userId);
        user.setEmail("");
        userDAO.update(user, sessionUtils.getCallingUserId());
        return new PlatformBasicResponse();
    }
    public VsatEventDetailsPojo getRedisVsatEventDetails() throws BadRequestException, InternalServerErrorException {
        String vsatEventDetailsStr = redisDAO.get(Constants.VSAT_EVENT_DETAILS);
        VsatEventDetailsPojo vsatEventDetailsPojo = gson.fromJson(vsatEventDetailsStr, VsatEventDetailsPojo.class);
        return vsatEventDetailsPojo;
    }

    public void clearRedisVsatEventDetails() throws BadRequestException, InternalServerErrorException {
        redisDAO.del(Constants.VSAT_EVENT_DETAILS);
    }

    public void setRedisVsatEventDetails(String message) throws InternalServerErrorException, BadRequestException {
        redisDAO.set(Constants.VSAT_EVENT_DETAILS, message);
    }

    public PlatformBasicResponse getAuthTokenLogin(UserPreLoginVerificationInfoReq request, HttpServletRequest req, HttpServletResponse res) {
        String token = "8Hq63zA7QQVD8MmU";
        Cookie tokenCookie = new Cookie("auth-token", token);
        tokenCookie.setMaxAge(-1);
        tokenCookie.setDomain(ConfigUtils.INSTANCE.getStringValue("auth.cookie.domain"));
        tokenCookie.setSecure(ConfigUtils.INSTANCE.getBooleanValue("auth.cookie.secure"));
        tokenCookie.setPath("/");
        tokenCookie.setHttpOnly(true);
        res.addCookie(tokenCookie);
        return new PlatformBasicResponse();
    }
}
