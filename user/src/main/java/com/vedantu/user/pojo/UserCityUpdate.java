package com.vedantu.user.pojo;


import com.vedantu.user.entity.User;

public class UserCityUpdate {

    private User user;
    private String ip;
    private Long callingUserId;

    public UserCityUpdate() {
    }

    public UserCityUpdate(User user, String ip, Long callingUserId) {
        this.user = user;
        this.ip = ip;
        this.callingUserId = callingUserId;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public Long getCallingUserId() {
        return callingUserId;
    }

    public void setCallingUserId(Long callingUserId) {
        this.callingUserId = callingUserId;
    }
}
