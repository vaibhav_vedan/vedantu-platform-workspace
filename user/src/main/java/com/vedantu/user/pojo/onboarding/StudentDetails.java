package com.vedantu.user.pojo.onboarding;

import com.vedantu.User.Gender;
import com.vedantu.user.entity.PhoneNumber;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class StudentDetails {
    private String firstName;
    private String lastName;
    private String email;
    private List<PhoneNumber> phoneNumbers;
    private Long dob;
    private List<String> preferredLanguages;
    private Gender gender;
}
