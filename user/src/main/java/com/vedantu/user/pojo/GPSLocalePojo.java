/**
 * 
 */
package com.vedantu.user.pojo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * @author vedantu
 *
 */

@Data
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class GPSLocalePojo {

	private String latitude;

	private String longitude;

	private String city;
	
	private Long capturedTime;




}
