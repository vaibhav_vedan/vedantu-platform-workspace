package com.vedantu.user.pojo;

import lombok.Data;


public class UserPreLoginDataPojo {
    String ipAddress;
    String loginParam;

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public String getLoginParam() {
        return loginParam;
    }

    public void setLoginParam(String loginParam) {
        this.loginParam = loginParam;
    }
}
