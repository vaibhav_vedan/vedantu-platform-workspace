package com.vedantu.user.pojo.onboarding;

import com.vedantu.User.ParentInfo;
import com.vedantu.user.enums.OnboardingCompletionStatus;
import com.vedantu.user.enums.OnboardingStep;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.StringUtils;
import lombok.Data;

import java.util.List;

@Data
public class ProfileCreation extends PageBasicDetials {
    private StudentDetails studentDetails;
    private List<ParentInfo> parentDetails;
    // private EducationalDetails educationalDetails;
    // private LocationInfo locationInfo;
    public ProfileCreation(){
        super(1, OnboardingStep.PROFILE_CREATION, OnboardingCompletionStatus.PENDING);
    }

    @Override
    public boolean checkDataValidity() {
        return super.checkDataValidity() ||
                StringUtils.isEmpty(studentDetails.getFirstName()) ||
                StringUtils.isEmpty(studentDetails.getEmail()) ||
                ArrayUtils.isEmpty(studentDetails.getPhoneNumbers()) ||
                ArrayUtils.isEmpty(parentDetails) ||
                StringUtils.isEmpty(parentDetails.get(0).getEmail()) ||
                StringUtils.isEmpty(parentDetails.get(0).getFirstName()) ||
                StringUtils.isEmpty(parentDetails.get(0).getContactNumber());
    }
}
