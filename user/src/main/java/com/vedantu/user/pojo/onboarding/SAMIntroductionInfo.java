package com.vedantu.user.pojo.onboarding;

import com.vedantu.user.enums.SAMIntroductionStatus;
import lombok.Data;

@Data
public class SAMIntroductionInfo {
    private Long idOfSAM;
    private String assignedSAM;
    private SAMIntroductionStatus samIntroductionStatus;
    private String samComment;
    private Long samIntroductionTime;

    public SAMIntroductionInfo(){
        this.samIntroductionStatus=SAMIntroductionStatus.PENDING;
    }
}
