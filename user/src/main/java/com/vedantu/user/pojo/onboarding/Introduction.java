package com.vedantu.user.pojo.onboarding;

import com.vedantu.user.enums.OnboardingCompletionStatus;
import com.vedantu.user.enums.OnboardingStep;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Objects;

@Data
public class Introduction extends PageBasicDetials {

    // New onboarding
    private Double allAboutYourPurchase=0d;
    private Double whereToFindYourCourses=0d;
    private Double howToAttendClasses=0d;
    private Double whatToDoInTheClasses=0d;
    private Double yourTestsAndAssignments=0d;

    // Old onboarding
    private Double introductionPercentage;
    private Double introductionPercentageForMobile;
    private Double waveDemoPercentage;
    private Double questionsAndLeaderboardDemoPercentage;
    private Double hotspotVideoPercentage;
    private Double doubtsVideoPercentage;
    public Introduction(){
        super(0, OnboardingStep.INTRODUCTION, OnboardingCompletionStatus.PENDING);
    }
}
