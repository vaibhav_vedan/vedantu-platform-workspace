package com.vedantu.user.pojo;


import com.vedantu.User.FeatureSource;
import com.vedantu.User.User;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.CustomValidator;
import com.vedantu.util.StringUtils;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TeachersTagsUpdateReq extends AbstractFrontEndReq {

    List<TeacherTagsInformation> teacherTagsInformation;

    @Override
    protected List<String> collectVerificationErrors() {

        List<String> errors = super.collectVerificationErrors();
        if (ArrayUtils.isEmpty(teacherTagsInformation)) {
            errors.add("Empty teacherTagsInformation");
        }

      String requestTeacherIdCheck = Optional.ofNullable(teacherTagsInformation)
              .orElseGet(ArrayList::new)
              .stream()
              .map(x->x.getTeacherId())
              .filter(Objects::isNull)
              .findFirst()
              .orElse("");
        if(Objects.isNull(requestTeacherIdCheck)) {
            errors.add("Some of the teacherIds is null");
        }

        return errors;
    }
}
