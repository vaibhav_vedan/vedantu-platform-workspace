package com.vedantu.user.pojo.onboarding;

import com.vedantu.user.enums.SalesVerificationStatus;
import lombok.Data;

@Data
public class SalesVerificationInfo {

    private Long salesVerifierId;
    private String salesVerifierEmail;
    private SalesVerificationStatus salesVerificationStatus;
    private String salesComment;
    private Long salesVerificationTime;

    public SalesVerificationInfo(){
        this.salesVerificationStatus=SalesVerificationStatus.VERIFICATION_PENDING;
    }
}
