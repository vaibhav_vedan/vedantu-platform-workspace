package com.vedantu.user.pojo.onboarding;

import com.vedantu.user.enums.OnboardingCompletionStatus;
import com.vedantu.user.enums.OnboardingStep;
import lombok.Data;

import java.util.List;

@Data
public class CourseEnroll extends PageBasicDetials {
    private List<String> enrolledEntityIds;
    public CourseEnroll(){
        super(2, OnboardingStep.COURSE_ENROLL, OnboardingCompletionStatus.PENDING);
    }
}
