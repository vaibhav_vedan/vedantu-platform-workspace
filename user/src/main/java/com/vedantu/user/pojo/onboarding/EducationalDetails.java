package com.vedantu.user.pojo.onboarding;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class EducationalDetails {
    private String board;
    private String grade;
    private String previousGradeScore;
    private String schoolName;
    private List<String> targetExams; // Multiple values present in User collection, so need to take one
    private List<String> preferredTimeForLiveClasses; // Should be a list
}
