package com.vedantu.user.pojo.onboarding;

import com.vedantu.user.enums.OnboardingCompletionStatus;
import com.vedantu.user.enums.OnboardingStep;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Objects;

@Getter
@Setter
@NoArgsConstructor
public class PageBasicDetials{
    private Integer stepNo;
    private OnboardingStep onboardingStepType;
    private OnboardingCompletionStatus stepCompletionStatus;
    private Long stepCompletionTime;

    public boolean checkDataValidity(){
        return Objects.isNull(stepNo) || Objects.isNull(onboardingStepType) || Objects.isNull(stepCompletionStatus);
    }

    public void markCurrentStepCompleted(){
        this.stepCompletionStatus=OnboardingCompletionStatus.COMPLETED;
        this.stepCompletionTime=System.currentTimeMillis();
    }

    public PageBasicDetials(Integer stepNo, OnboardingStep onboardingStepType, OnboardingCompletionStatus stepCompletionStatus) {
        this.stepNo = stepNo;
        this.onboardingStepType = onboardingStepType;
        this.stepCompletionStatus = stepCompletionStatus;
    }
}
