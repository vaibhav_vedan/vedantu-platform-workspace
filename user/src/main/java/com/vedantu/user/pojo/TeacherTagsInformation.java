package com.vedantu.user.pojo;


import com.vedantu.subscription.enums.EarlyLearningCourseType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TeacherTagsInformation {

    private String teacherId;
    private List<EarlyLearningCourseType> earlyLearningTag;
}
